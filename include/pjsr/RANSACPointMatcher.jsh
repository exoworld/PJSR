//     ____       __ _____  ____
//    / __ \     / // ___/ / __ \
//   / /_/ /__  / / \__ \ / /_/ /
//  / ____// /_/ / ___/ // _, _/   PixInsight JavaScript Runtime
// /_/     \____/ /____//_/ |_|    PJSR Version 1.0
// ----------------------------------------------------------------------------
// pjsr/RANSACPointMatcher.jsh - Released 2023-06-12T18:03:39Z
// ----------------------------------------------------------------------------
// This file is part of the PixInsight JavaScript Runtime (PJSR).
// PJSR is an ECMA-262-5 compliant framework for development of scripts on the
// PixInsight platform.
//
// Copyright (c) 2003-2023 Pleiades Astrophoto S.L. All Rights Reserved.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __PJSR_RANSACPointMatcher_jsh
#define __PJSR_RANSACPointMatcher_jsh

/*
 * Minimum number of RANSAC iterations for a meaningful outlier rejection in
 * the homography estimation task. 35 iterations is the minimum required to
 * ensure success with 99.99% probability and a 30% of outliers.
 */
#define RANSAC_PM_MIN_ITERATIONS 35

/*
 * RANSAC robust point matching and homography estimation.
 */
function RANSACPointMatcher( a_P1, a_P2, tolerance, maxIterations, klen, kovl, kreg, krms )
{
   this.__base__ = Object;
   this.__base__();

   if ( !(a_P1 instanceof Array) || !(a_P2 instanceof Array) )
      throw new Error( "RANSACPointMatcher: expected two Array objects." );

   let P1 = [], P2 = [];
   let N = 0;
   for ( let i = 0; i < a_P1.length && i < a_P2.length; ++i )
      if ( a_P1[i] && a_P2[i] )
      {
         let p1 = a_P1[i];
         p1.index = i;
         P1.push( p1 );
         let p2 = a_P2[i];
         p2.index = i;
         P2.push( p2 );
         ++N;
      }
   if ( N < 8 )
      throw new Error( "RANSACPointMatcher: need at least 8 point pairs." );

   this.H = null; // Estimated homography matrix
   this.I   = []; // Matched target point indexes in P1 and P2
   this.M1  = []; // Best matched point set, reference
   this.M2  = []; // Best matched point set, target
   this.RMS = 0;  // Root mean square error in pixels
   this.EDV = 0;  // Average RMS error deviation
   this.PEX = 0;  // Peak error in pixels, X-axis
   this.PEY = 0;  // Peak error in pixels, Y-axis
   this.INL = 0;  // Fraction of inliers
   this.OVL = 0;  // Overlapping area in fractional units, [0,1] range
   this.REG = 0;  // Regularity in the 2-D distribution of point matches, in the [0,1] range
   this.QLT = 0;  // Model quality in the [0,1] range (the minimum of INL, OVL, REG)
   this.NIT = 0;  // Number of iterations performed
   this.MIT = 0;  // Best model iteration

   /*
    * Default tolerance in pixels.
    */
   if ( tolerance === undefined || tolerance <= 0 )
      tolerance = 2;

   /*
    * Default maximum number of iterations.
    */
   if ( maxIterations === undefined || maxIterations <= 0 )
      maxIterations = RANSAC_PM_MIN_ITERATIONS;
   else
      maxIterations = Math.max( RANSAC_PM_MIN_ITERATIONS, maxIterations );

   /*
    * Default optimization factors.
    */
   if ( klen === undefined || klen < 0 )
      klen = 1;
   if ( kovl === undefined || kovl < 0 )
      kovl = 1;
   if ( kreg === undefined || kreg < 0 )
      kreg = 1;
   if ( krms === undefined || krms < 0 )
      krms = 1;

   /*
    * Scale optimization factors to form a unit vector.
    */
   {
      let ks = klen + kovl + kreg + krms;
      if ( 1 + ks == 1 ) // !?
         klen = kovl = kreg = krms = 1.0/4;
      else
      {
         klen /= ks;
         kovl /= ks;
         kreg /= ks;
         krms /= ks;
      }
   }

   /*
    * Model rectangle
    */
   let rect = new Rect( Math.MAX, Math.MAX, Math.MIN, Math.MIN );
   for ( let i = 0; i < N; ++i )
   {
      if ( P1[i].x < rect.x0 )
         rect.x0 = P1[i].x;
      if ( P1[i].y < rect.y0 )
         rect.y0 = P1[i].y;
      if ( P1[i].x > rect.x1 )
         rect.x1 = P1[i].x;
      if ( P1[i].y > rect.y1 )
         rect.y1 = P1[i].y;
   }
   if ( !rect.isRect ) // !? this should never happen
      return;
   rect.x0 = Math.trunc( rect.x0 );
   rect.y0 = Math.trunc( rect.y0 );
   rect.x1 = Math.trunc( rect.x1 );
   rect.y1 = Math.trunc( rect.y1 );

   let area = rect.area;

   do
   {
      /*
       * The consensus set
       */
      let I0 = [];          // initial pair indexes
      let J = [];           // matched pair indexes
      let Q1 = [], Q2 = []; // matched points

      /*
       * Extract random samples
       */
      for ( let i = 0; i < 4; ++i )
      {
         let j;
         do
            j = Math.trunc( Math.random() * N );
         while ( I0.indexOf( j ) >= 0 );
         I0.push( j );
         Q1.push( P1[j] );
         Q2.push( P2[j] );
      }

      /*
       * Fit a model hypothesis
       */
      let HQ;
      try
      {
         HQ = Math.homography( Q1, Q2 );
      }
      catch ( x )
      {
         // In the unlikely case we get a singular matrix here, silently
         // eat it and ignore this iteration.
         continue;
      }

      /*
       * Get a set of putative inliers satisfying the model hypothesis
       */
      for ( let i = 0; i < N; ++i )
      {
         // Exclude points used to build the model
         if ( I0.indexOf( i ) >= 0 )
            continue;

         // The model predicts a target point
         let p2 = HQ.apply( P1[i] );

         // Let's see if the model confirms this putative match
         let dx = Math.abs( P2[i].x - p2.x );
         if ( dx <= tolerance )
         {
            let dy = Math.abs( P2[i].y - p2.y );
            if ( dy <= tolerance )
            {
               // If the model confirms an existing putative match, we
               // have found a putative inlier.
               Q1.push( P1[i] );
               Q2.push( P2[i] );
            }
         }
      }

      /*
       * Now verify model likelihood from all putative inliers, and estimate
       * mean and peak errors in both axes.
       */
      if ( Q1.length > 4 )
      {
         HQ = Math.homography( Q1, Q2 );

         Q1 = [];
         Q2 = [];
         let E = []; // point errors

         let ssd = 0; // sum of squared distances
         let pdx = 0; // peak error, X-axis
         let pdy = 0; // peak error, Y-axis
         let rgn = new Rect( Math.MAX, Math.MAX, Math.MIN, Math.MIN ); // overlapping area

         for ( let i = 0; i < N; ++i )
         {
            let p2 = HQ.apply( P1[i] );

            let dx = Math.abs( P2[i].x - p2.x );
            if ( dx <= tolerance )
            {
               let dy = Math.abs( P2[i].y - p2.y );
               if ( dy <= tolerance )
               {
                  Q1.push( P1[i] );
                  Q2.push( P2[i] );
                  J.push( P1[i].index );

                  let e2 = dx*dx + dy*dy;
                  ssd += e2;
                  E.push( Math.sqrt( e2 ) );

                  if ( pdx < dx )
                     pdx = dx;
                  if ( pdy < dy )
                     pdy = dy;

                  if ( P1[i].x < rgn.x0 )
                     rgn.x0 = P1[i].x;
                  if ( P1[i].y < rgn.y0 )
                     rgn.y0 = P1[i].y;
                  if ( P1[i].x > rgn.x1 )
                     rgn.x1 = P1[i].x;
                  if ( P1[i].y > rgn.y1 )
                     rgn.y1 = P1[i].y;
               }
            }
         }

         /*
          * If we find an acceptable model that is better than the current
          * model, grab it as the best model so far.
          */
         if ( Q1.length >= 8 ) // a valid model has at least eight inliers
         {
            /*
             * Root mean square error
             */
            let rms = Math.sqrt( ssd/Q1.length );

            /*
             * MAD of RMS errors, consistent with a normal distribution.
             */
            let edv = 1.4826*Math.MAD( E );

            /*
             * Fractional overlapping area.
             */
            let ovl = rgn.area/area;

            /*
             * Regularity in the 2-D distribution of point matches.
             */
            let reg = 0;
            if ( kreg > 0 )
            {
               for ( let j = 0; j < 2; ++j )
               {
                  let size = (j ? rect.height : rect.width) + 1;
                  let h = new Array( size );
                  for ( let i = 0; i < size; ++i )
                     h[i] = 0;
                  for ( let i = 0; i < Q1.length; ++i )
                     h[j ? Math.trunc( Q1[i].y ) - rect.y0 : Math.trunc( Q1[i].x ) - rect.x0] = 1;
                  let z = 0;
                  for ( let i = 0; i < size; )
                     if ( h[i++] == 0 )
                     {
                        let z1 = 1;
                        for ( ; i != size && h[i] == 0; ++i )
                           ++z1;
                        if ( z1 > z )
                           z = z1;
                     }
                  let rz = z/size;
                  if ( rz > reg )
                     reg = rz;
               }
               reg = 1 - reg;
            }

            /*
             * Model quality normalized to [0,1].
             * For 16 or less points, we favor point inclusion exclusively.
             */
            let qlt;
            if ( Q1.length > 16 )
               qlt = klen*Q1.length/N + kovl*ovl + kreg*reg + krms*(tolerance - rms)/tolerance;
            else
               qlt = Q1.length/N;

            /*
             * If we have found a model of more quality than the current one,
             * fetch it as the best model so far.
             */
            if ( qlt > this.QLT )
            {
               this.H   = Math.homography( Q1, Q2 );
               this.I   = J;
               this.M1  = Q1;
               this.M2  = Q2;
               this.RMS = rms;
               this.EDV = edv;
               this.PEX = pdx;
               this.PEY = pdy;
               this.OVL = ovl;
               this.REG = reg;
               this.QLT = qlt;
               this.MIT = this.NIT + 1;

               /*
                * Stop in case we have found a model that includes all point
                * pairs available.
                */
               if ( this.M1.length == N )
                  break;
            }
         }
      }
   }
   while ( ++this.NIT < maxIterations );

   // Compute the fraction of inliers
   this.INL = 1 - (N - this.M1.length)/N;
}

RANSACPointMatcher.prototype = new Object;

#endif   // __PJSR_RANSACPointMatcher_jsh

// ----------------------------------------------------------------------------
// EOF pjsr/RANSACPointMatcher.jsh - Released 2023-06-12T18:03:39Z

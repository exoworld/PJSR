//     ____       __ _____  ____
//    / __ \     / // ___/ / __ \
//   / /_/ /__  / / \__ \ / /_/ /
//  / ____// /_/ / ___/ // _, _/   PixInsight JavaScript Runtime
// /_/     \____/ /____//_/ |_|    PJSR Version 1.0
// ----------------------------------------------------------------------------
// pjsr/LinearPatternSubtraction.jsh - Released 2023-06-12T18:03:39Z
// ----------------------------------------------------------------------------
// This file is part of the PixInsight JavaScript Runtime (PJSR).
// PJSR is an ECMA-262-5 compliant framework for development of scripts on the
// PixInsight platform.
//
// Copyright (c) 2003-2023 Pleiades Astrophoto S.L. All Rights Reserved.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __PJSR_LinearPatternSubtraction_jsh
#define __PJSR_LinearPatternSubtraction_jsh

#include <pjsr/DataType.jsh>
#include <pjsr/ImageOp.jsh>
#include <pjsr/ReadTextOptions.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/SampleType.jsh>

#define LPS_DEFAULT_INPUT_FILES                    []
#define LPS_DEFAULT_TARGET_IS_ACTIVE_IMAGE         false
#define LPS_DEFAULT_CLOSE_FORMER_WORKING_IMAGES    false
#define LPS_DEFAULT_OUTPUT_DIR                     ""
#define LPS_DEFAULT_CORRECT_COLUMNS                true
#define LPS_DEFAULT_CORRECT_ENTIRE_IMAGE           false
#define LPS_DEFAULT_DEFECT_TABLE_FILE_PATH         ""
#define LPS_DEFAULT_POSTFIX                        "_lps"
#define LPS_DEFAULT_LAYERS_TO_REMOVE               9
#define LPS_DEFAULT_REJECTION_LIMIT                3
#define LPS_DEFAULT_GLOBAL_REJECTION               true
#define LPS_DEFAULT_GLOBAL_REJECTION_LIMIT         5
#define LPS_DEFAULT_BACKGROUND_REFERENCE_LEFT      0
#define LPS_DEFAULT_BACKGROUND_REFERENCE_TOP       0
#define LPS_DEFAULT_BACKGROUND_REFERENCE_WIDTH     512
#define LPS_DEFAULT_BACKGROUND_REFERENCE_HEIGHT    512
#define LPS_DEFAULT_DEFECT_COLUMN_ROW              undefined
#define LPS_DEFAULT_DEFECT_START_PIXEL             undefined
#define LPS_DEFAULT_DEFECT_END_PIXEL               undefined

/*
 * LinearPatternSubtraction
 *
 * Corrects residual column or row patterns in a list of images.
 *
 * This procedure reuses some code written by Georg Viehoever in the
 * CanonBandingReduction script and applies the column or row correction in the
 * multiscale context. The engine also includes outlier rejection for a more
 * robust statistical evaluation.
 *
 * The procedure corrects entire rows or columns, but it can also correct
 * partial rows or columns by reading a defect table generated by
 * CosmeticCorrection. Thus, if you want to correct partial rows or columns,
 * you should first run CosmeticCorrection, manually create the defect list,
 * and save it to a text file.
 */
function LPSEngine()
{
   // set defaults
   this.inputFiles = LPS_DEFAULT_INPUT_FILES;
   this.targetIsActiveImage = LPS_DEFAULT_TARGET_IS_ACTIVE_IMAGE;
   this.closeFormerWorkingImages = LPS_DEFAULT_CLOSE_FORMER_WORKING_IMAGES;
   this.outputDir = LPS_DEFAULT_OUTPUT_DIR;
   this.correctColumns = LPS_DEFAULT_CORRECT_COLUMNS;
   this.correctEntireImage = LPS_DEFAULT_CORRECT_ENTIRE_IMAGE;
   this.defectTableFilePath = LPS_DEFAULT_DEFECT_TABLE_FILE_PATH;
   this.postfix = LPS_DEFAULT_POSTFIX;
   this.layersToRemove = LPS_DEFAULT_LAYERS_TO_REMOVE;
   this.rejectionLimit = LPS_DEFAULT_REJECTION_LIMIT;
   this.globalRejection = LPS_DEFAULT_GLOBAL_REJECTION;
   this.globalRejectionLimit = LPS_DEFAULT_GLOBAL_REJECTION_LIMIT;
   this.backgroundReferenceLeft = LPS_DEFAULT_BACKGROUND_REFERENCE_LEFT;
   this.backgroundReferenceTop = LPS_DEFAULT_BACKGROUND_REFERENCE_TOP;
   this.backgroundReferenceWidth = LPS_DEFAULT_BACKGROUND_REFERENCE_WIDTH;
   this.backgroundReferenceHeight = LPS_DEFAULT_BACKGROUND_REFERENCE_HEIGHT;
   this.defectColumnOrRow = LPS_DEFAULT_DEFECT_COLUMN_ROW;
   this.defectStartPixel = LPS_DEFAULT_DEFECT_START_PIXEL;
   this.defectEndPixel = LPS_DEFAULT_DEFECT_END_PIXEL;

   // ----------------------------------------------------------------------------

   /*
    * These are the image windows and images that will be used by the script
    * engine.
    */
   this.defineWindowsAndImages = ( globalRejection ) =>
   {
      let WI = {};

      // Force the active window to be in 32-bit float format.
      ImageWindow.activeWindow.setSampleFormat( 32, true );

      WI.targetImageWindow = ImageWindow.activeWindow;

      WI.targetImage = new Image( WI.targetImageWindow.mainView.image.width,
         WI.targetImageWindow.mainView.image.height,
         WI.targetImageWindow.mainView.image.numberOfChannels,
         WI.targetImageWindow.mainView.image.colorSpace,
         WI.targetImageWindow.mainView.image.bitsPerSample,
         SampleType_Real );

      WI.targetImage.apply( WI.targetImageWindow.mainView.image );

      /*
       * The large-scale component image.
       * The initial image is a clone of the target image. The contents of this
       * image will be generated by the MultiscaleIsolation function.
       */
      WI.LSImageWindow = new ImageWindow( WI.targetImage.width,
         WI.targetImage.height,
         WI.targetImage.numberOfChannels,
         32, true, false, "LS" );

      WI.LSImage = new Image( WI.targetImageWindow.mainView.image.width,
         WI.targetImageWindow.mainView.image.height,
         WI.targetImageWindow.mainView.image.numberOfChannels,
         WI.targetImageWindow.mainView.image.colorSpace,
         WI.targetImageWindow.mainView.image.bitsPerSample,
         SampleType_Real );

      /*
       * The small-scale component image.
       * We'll get the column statistics from this image. We copy the targetImage
       * content to this image; the large-scale components of this image will be
       * removed by the MultiscaleIsolation function.
       */
      WI.SSImageWindow = new ImageWindow( WI.targetImage.width,
         WI.targetImage.height,
         WI.targetImage.numberOfChannels,
         32, true, false, "SS" );

      WI.SSImage = new Image( WI.targetImage.width,
         WI.targetImage.height,
         WI.targetImage.numberOfChannels,
         WI.targetImage.colorSpace,
         WI.targetImage.bitsPerSample,
         SampleType_Real );

      WI.SSImage.apply( WI.targetImageWindow.mainView.image );

      /*
       * The image containing the line pattern.
       * The pattern is calculated and subtracted from the target image by the
       * PatternSubtraction function.
       */
      WI.patternImageWindow = new ImageWindow( WI.targetImage.width,
         WI.targetImage.height,
         WI.targetImage.numberOfChannels,
         32, true, false, "pattern" );

      WI.patternImage = new Image( WI.targetImage.width,
         WI.targetImage.height,
         WI.targetImage.numberOfChannels,
         WI.targetImage.colorSpace,
         WI.targetImage.bitsPerSample,
         SampleType_Real );

      WI.patternImage.fill( 0 );

      if ( globalRejection )
      {
         WI.clippingMask = new Image( WI.targetImage.width,
            WI.targetImage.height,
            WI.targetImage.numberOfChannels,
            WI.targetImage.colorSpace,
            WI.targetImage.bitsPerSample,
            SampleType_Real );

         WI.clippingMask.apply( WI.targetImageWindow.mainView.image );
      }

      return WI;
   };

   // ----------------------------------------------------------------------------

   /*
    * This function calculates the median and the MAD of a background area.
    * These values will be used by the MultiscaleIsolation and GlobalRejection
    * functions in LinearPatternSubtraction.
    */
   this.backgroundStatistics = ( targetImage,
      backgroundReferenceLeft, backgroundReferenceTop,
      backgroundReferenceWidth, backgroundReferenceHeight ) =>
   {
      let BS = {};
      let backgroundRectangle = new Rect( backgroundReferenceWidth, backgroundReferenceHeight );
      backgroundRectangle.moveTo( backgroundReferenceLeft, backgroundReferenceTop );
      BS.median = targetImage.median( backgroundRectangle );
      BS.MAD = targetImage.MAD( BS.median, backgroundRectangle );
      console.writeln( format( "<end><cbr>Background level: %9.7f +- %9.7f", BS.median, BS.MAD ) );
      if ( BS.median < 0.0000001 )
         throw new Error( "Background level is zero. Please select a background area " +
            "with a higher signal level. We recommend checking your image " +
            "since it could have a large proportion of clipped data. " );
      return BS;
   };

   // ----------------------------------------------------------------------------

   /*
    * Function to subtract the large-scale components from an image using the
    * median wavelet transform.
    */
   this.multiscaleIsolation = ( image, LSImage, layersToRemove ) =>
   {
      // Generate the large-scale components image.
      // First we generate the array that defines
      // the states (enabled / disabled) of the scale layers.
      let scales = new Array;
      for ( let i = 0; i < layersToRemove; ++i )
         scales.push( 1 );

      // The scale layers are an array of images.
      // We use the medianWaveletTransform. This algorithm is less prone
      // to show vertical patterns in the large-scale components.
      let multiscaleTransform = new Array;
      multiscaleTransform = image.medianWaveletTransform( layersToRemove - 1, 0, scales );
      // We subtract the last layer to the image.
      // Please note that this image has negative pixel values.
      image.apply( multiscaleTransform[ layersToRemove - 1 ], ImageOp_Sub );
      // Generate a large-scale component image
      // if the respective input image is not null.
      if ( LSImage != null )
         LSImage.apply( multiscaleTransform[ layersToRemove - 1 ] );
      // Remove the multiscale layers from memory.
      for ( let i = 0; i < multiscaleTransform.length; ++i )
         multiscaleTransform[ i ].free();
   };

   // ----------------------------------------------------------------------------

   /*
    * Function to perform a pixel rejection in the small-scale image based on the
    * pixel values of the target image. The rejected pixel values are set to 1 in
    * the small-scale image; this way, they will be all rejected when performing
    * the in-line pixel rejection in the PatternSubtraction function since the
    * rejection high value will be always below 1.
    */
   this.executeGlobalRejection = ( targetImage, SSImage, LSImage, clippingMask,
      backgroundMedian, backgroundMAD, globalRejectionLimit ) =>
   {
      let globalRejectionHigh = backgroundMedian + ( backgroundMAD * this.globalRejectionLimit );
      clippingMask.binarize( globalRejectionHigh );
      // Before applying the maximum operation between the clipping mask and
      // the small-scale component image, we need to add the median of
      // the sky background to the latter, since the median value of that image
      // is zero because we already subtracted the large-scale components.
      SSImage.apply( backgroundMedian, ImageOp_Add );
      SSImage.apply( clippingMask, ImageOp_Max );
      // After applying the maximum operation, we should subtract again the sky
      // background pedestal to keep the median value of the small-component image
      // to zero. This is needed because this image is used specifically
      // to calculate the defect deviations that should be removed from
      // the target image.
      SSImage.apply( backgroundMedian, ImageOp_Sub );
      console.writeln( format( "Global rejection high value: %9.7f", globalRejectionHigh ) );
   };

   // ----------------------------------------------------------------------------

   /*
    * Generate the pattern image and subtract it from the target image.
    */
   this.patternSubtraction = ( correctColumns, lines, targetImage, SSImage, patternImage, rejectionLimit ) =>
   {
      // Generate the line-patterned image.
      for ( let i = 0; i < lines.columnOrRow.length; ++i )
      {
         // Select the line specified in the line list.
         if ( correctColumns )
         {
            var lineRect = new Rect( 1, lines.endPixel[ i ] - lines.startPixel[ i ] + 1 );
            lineRect.moveTo( lines.columnOrRow[ i ], lines.startPixel[ i ] );
         }
         else
         {
            var lineRect = new Rect( lines.endPixel[ i ] - lines.startPixel[ i ] + 1, 1 );
            lineRect.moveTo( lines.startPixel[ i ], lines.columnOrRow[ i ] );
         }

         // Calculate the line statistics.
         var lineStatistics = this.iterativeStatistics( SSImage, lineRect, this.rejectionLimit );
         // Store line median value into patternImage.
         // Please note that we apply the same line selection to this image,
         // so we fill only that line with its median value.
         patternImage.apply( lineStatistics.median, 1, lineRect );
      }
      // Finally we subtract the pattern image from the target image.
      targetImage.apply( patternImage, ImageOp_Sub );
   };

   // ----------------------------------------------------------------------------

   /*
    * Function to calculate the median and MAD of a selected image area with
    * iterative outlier rejection in the high end of the distribution. Useful to
    * reject bright objects in a background-dominated image, especially if the
    * input image is the output image of MultiscaleIsolation.
    */
   this.iterativeStatistics = ( image, rectangle, rejectionLimit ) =>
   {
      let IS = {};

      image.selectedRect = rectangle;
      let formerHighRejectionLimit = 1000;
      // The initial currentHighRejectionLimit value is set to 0.99 because
      // the global rejection sets the rejected pixels to 1. This way, those
      // pixels are already rejected in the first iteration.
      let currentHighRejectionLimit = 0.99;
      let j = 0;
      while ( formerHighRejectionLimit / currentHighRejectionLimit > 1.001 || j < 10 )
      {
         // Construct the statistics object to rectangle statistics.
         // These statistics are updated with the new high rejection limit
         // calculated at the end of the iteration.
         let iterativeRectangleStatistics = new ImageStatistics;
         with( iterativeRectangleStatistics )
         {
            medianEnabled = true;
            lowRejectionEnabled = false;
            highRejectionEnabled = true;
            rejectionHigh = currentHighRejectionLimit;
         }
         iterativeRectangleStatistics.generate( image );
         IS.median = iterativeRectangleStatistics.median;
         IS.MAD = iterativeRectangleStatistics.mad;
         formerHighRejectionLimit = currentHighRejectionLimit;
         currentHighRejectionLimit = parseFloat( IS.median + ( iterativeRectangleStatistics.mad * 1.4826 * rejectionLimit ) );
         ++j;
      }
      image.resetSelections();

      return IS;
   };

   // ----------------------------------------------------------------------------

   /*
    * Function to create a list of vertical or horizontal lines in an image. It
    * can combine entire rows or columns and fragmented ones, if an array of
    * partial sections is specified in the input parameters. This list is used to
    * input the selected regions in the IterativeStatistics function.
    */
   this.lineList = ( correctEntireImage, partialColumnOrRow, partialStartPixel, partialEndPixel, maxPixelPara, maxPixelPerp ) =>
   {
      let LL = {};

      LL.columnOrRow = new Array;
      LL.startPixel = new Array;
      LL.endPixel = new Array;

      if ( !correctEntireImage )
      {
         LL.columnOrRow = partialColumnOrRow;
         LL.startPixel = partialStartPixel;
         LL.endPixel = partialEndPixel;
      }
      else
      {
         if ( partialColumnOrRow.length == 0 )
            partialColumnOrRow.push( maxPixelPerp + 1 );

         let iPartial = 0;
         for ( let i = 0; i <= maxPixelPerp; ++i )
         {
            if ( iPartial < partialColumnOrRow.length )
            {
               if ( i < partialColumnOrRow[ iPartial ] && correctEntireImage )
               {
                  LL.columnOrRow.push( i );
                  LL.startPixel.push( 0 );
                  LL.endPixel.push( maxPixelPara );
               }
               else
               {
                  // Get the partial column or row.
                  LL.columnOrRow.push( partialColumnOrRow[ iPartial ] );
                  LL.startPixel.push( partialStartPixel[ iPartial ] );
                  LL.endPixel.push( partialEndPixel[ iPartial ] );
                  if ( partialStartPixel[ iPartial ] > 0 )
                  {
                     LL.columnOrRow.push( partialColumnOrRow[ iPartial ] );
                     LL.startPixel.push( 0 );
                     LL.endPixel.push( partialStartPixel[ iPartial ] - 1 );
                  }
                  if ( partialEndPixel[ iPartial ] < maxPixelPara )
                  {
                     LL.columnOrRow.push( partialColumnOrRow[ iPartial ] );
                     LL.startPixel.push( partialEndPixel[ iPartial ] + 1 );
                     LL.endPixel.push( maxPixelPara );
                  }
                  // In some cases, there can be more than one section of
                  // the same column or row in the partial defect list.
                  // In that case, i (which is the current column or row number)
                  // shouldn't increase because we are repeating
                  // the same column or row.
                  i = partialColumnOrRow[ iPartial ];
                  ++iPartial;
               }
            }
            else if ( correctEntireImage )
            {
               LL.columnOrRow.push( i );
               LL.startPixel.push( 0 );
               LL.endPixel.push( maxPixelPara );
            }
         }
      }

      return LL;
   };

   // ----------------------------------------------------------------------------

   this.executeOnActiveImage = ( outputFilePath ) =>
   {
      // start the measuring executed time
      let engineTime = new ElapsedTime;

      // Define the needed image windows and images for the entire engine.
      var WI = this.defineWindowsAndImages( this.globalRejection );

      // Check if the defined background area is inside the image.
      if ( this.backgroundReferenceLeft + this.backgroundReferenceWidth > WI.targetImage.width ||
         this.backgroundReferenceTop + this.backgroundReferenceHeight > WI.targetImage.height ) {
         console.criticalln( "Background reference area is out of bounds of the image" );
         return false;
      }

      // Calculate the median and MAD values of the background reference area.
      var background = this.backgroundStatistics( WI.targetImage,
         this.backgroundReferenceLeft, this.backgroundReferenceTop,
         this.backgroundReferenceWidth, this.backgroundReferenceHeight );

      // Generate the small and large-scale component images.
      console.writeln( "Isolating multiscale components" );
      console.flush();
      this.multiscaleIsolation( WI.SSImage, WI.LSImage, this.layersToRemove );

      // Apply a small scale normalization to the already processed
      // small-scale component image. This way we adjust the local contrast
      // of the columns or rows, depending on the local illumination level
      // of the large-scale component image. This is needed because
      // the line structures lose contrast in higher illuminated areas,
      // thus affecting the statistics of the line.
      WI.SSImage.apply( WI.LSImage, ImageOp_Mul );
      WI.SSImage.apply( background.median, ImageOp_Div );

      // Perform a pixel rejection in the entire image
      // based on the pixel value of the target image.
      if ( this.globalRejection )
         this.executeGlobalRejection( WI.targetImage, WI.SSImage,
            WI.LSImage, WI.clippingMask,
            background.median, background.MAD, this.globalRejectionLimit );

      // Build the list of lines to be corrected.
      // This list includes both entire and partial columns or rows.
      console.writeln( "Building the list of pattern lines" );
      console.flush();
      if ( this.correctColumns )
      {
         var maxPixelPara = WI.targetImage.height - 1;
         var maxPixelPerp = WI.targetImage.width - 1;
      }
      else
      {
         var maxPixelPara = WI.targetImage.width - 1;
         var maxPixelPerp = WI.targetImage.height - 1;
      }

      // Read the defect table.
      this.defectColumnOrRow = [];
      this.defectStartPixel = [];
      this.defectEndPixel = [];

      if ( this.defectTableFilePath != ( "" && undefined ) )
      {
         let tableLines = File.readLines( this.defectTableFilePath, ReadTextOptions_RemoveEmptyLines | ReadTextOptions_TrimLines );
         for ( let iTable = 0; iTable < tableLines.length; ++iTable )
         {
            let tokens = tableLines[ iTable ].split( " " );
            this.defectColumnOrRow.push( parseInt( tokens[ 1 ] ) );
            this.defectStartPixel.push( parseInt( tokens[ 2 ] ) );
            this.defectEndPixel.push( parseInt( tokens[ 3 ] ) );
         }
      }
      // Create the list of lines to be corrected.
      var lines = this.lineList( this.correctEntireImage,
         this.defectColumnOrRow, this.defectStartPixel, this.defectEndPixel,
         maxPixelPara, maxPixelPerp )

      // Generate the line pattern and subtract it from the target image.
      console.writeln( "Correcting the line pattern" );
      console.flush();
      this.patternSubtraction( this.correctColumns, lines,
         WI.targetImage, WI.SSImage, WI.patternImage,
         this.rejectionLimit );

      // Show the working images if the target image is the active image.
      if ( outputFilePath == undefined )
      {
         // Add the median pixel value of the large-scale component image
         // to the small-scale, the pattern and the target images.
         // This ensures that the median signal intensity in the product
         // images is the same than the original image before the script
         // execution. This also prevents having pixels values out of the [0,1]
         // range in these images, which is necessary for a correct visualization
         // once they are applied to their corresponding image windows.
         let LSImageMedian = WI.LSImage.median();
         WI.SSImage.apply( LSImageMedian, ImageOp_Add );
         WI.SSImage.truncate( 0, 1 );
         WI.patternImage.apply( LSImageMedian, ImageOp_Add );
         WI.patternImage.truncate( 0, 1 );
         var targetImageMedian = WI.targetImage.median();
         WI.targetImage.apply( targetImageMedian, ImageOp_Sub );
         WI.targetImage.apply( LSImageMedian, ImageOp_Add );
         WI.targetImage.truncate( 0, 1 );

         // Apply the working images to their corresponding image windows.
         WI.targetImageWindow.mainView.beginProcess();
         WI.targetImageWindow.mainView.image.apply( WI.targetImage );
         WI.targetImageWindow.mainView.endProcess();
         WI.SSImageWindow.mainView.beginProcess( UndoFlag_NoSwapFile );
         WI.SSImageWindow.mainView.image.apply( WI.SSImage );
         WI.SSImageWindow.mainView.endProcess();
         WI.LSImageWindow.mainView.beginProcess( UndoFlag_NoSwapFile );
         WI.LSImageWindow.mainView.image.apply( WI.LSImage );
         WI.LSImageWindow.mainView.endProcess();
         WI.patternImageWindow.mainView.beginProcess( UndoFlag_NoSwapFile );
         WI.patternImageWindow.mainView.image.apply( WI.patternImage );
         WI.patternImageWindow.mainView.endProcess();
      }
      else
      {
         // Save the processed image if the target is an image list.
         WI.targetImageWindow.mainView.beginProcess( UndoFlag_NoSwapFile );
         WI.targetImageWindow.mainView.image.apply( WI.targetImage );
         WI.targetImageWindow.mainView.endProcess();
         if (!this.closeFormerWorkingImages) {
            WI.SSImageWindow.mainView.beginProcess( UndoFlag_NoSwapFile );
            WI.SSImageWindow.mainView.image.apply( WI.SSImage );
            WI.SSImageWindow.mainView.endProcess();
            WI.LSImageWindow.mainView.beginProcess( UndoFlag_NoSwapFile );
            WI.LSImageWindow.mainView.image.apply( WI.LSImage );
            WI.LSImageWindow.mainView.endProcess();
            WI.patternImageWindow.mainView.beginProcess( UndoFlag_NoSwapFile );
            WI.patternImageWindow.mainView.image.apply( WI.patternImage );
            WI.patternImageWindow.mainView.endProcess();
         }
         console.writeln( "Saving processed image to disk" );
         console.flush();

         WI.targetImageWindow.saveAs( outputFilePath, false, false, false, false );

         if ( File.exists( outputFilePath ) )
         {
            this.output.push(outputFilePath);
            console.writeln( "Image saved to " + outputFilePath );
            console.flush();
         }
         else {
            console.criticalln( "Could not save image to " + outputFilePath );
            return false;
         }

         if (this.closeFormerWorkingImages) {
            WI.targetImageWindow.forceClose();
         }
      }

      // Release the working images.
      WI.LSImage.free();
      WI.SSImage.free();
      WI.patternImage.free();
      WI.targetImage.free();
      if ( this.globalRejection )
            WI.clippingMask.free();

      // Clean working images and close image windows.
         if (this.closeFormerWorkingImages) {
            WI.LSImageWindow.forceClose();
            WI.SSImageWindow.forceClose();
            WI.patternImageWindow.forceClose();
         } else {
            WI.LSImageWindow.show();
            WI.SSImageWindow.show();
            WI.patternImageWindow.show();
         }

      console.writeln( "<end><cbr><br>Engine processing time: " + engineTime.text );
      return true;
   };

   // ----------------------------------------------------------------------------

   this.execute = () =>
   {
      // output list of processed file paths
      this.output = [];

      // close former working images if needed
      if ( this.closeFormerWorkingImages )
      {
         if ( !ImageWindow.windowById( "pattern" ).isNull )
            ImageWindow.windowById( "pattern" ).forceClose();
         if ( !ImageWindow.windowById( "SS" ).isNull )
            ImageWindow.windowById( "SS" ).forceClose();
         if ( !ImageWindow.windowById( "LS" ).isNull )
            ImageWindow.windowById( "LS" ).forceClose();
         if ( !ImageWindow.windowById( "clipping_mask" ).isNull )
            ImageWindow.windowById( "clipping_mask" ).forceClose();
      }

      if ( this.targetIsActiveImage )
      {
         /*
          * Process only the active image.
          */
         console.noteln( "<end><cbr><br>* Processing active image." );
         console.flush();

         this.executeOnActiveImage();
      }
      else
      {
         /*
          * Process each image in the input files list.
          */
         for ( let i = 0; i < this.inputFiles.length; ++i )
         {
            console.noteln( format( "<end><cbr><br>* Processing image %d of %d", i + 1, this.inputFiles.length ) );
            console.flush();

            // Get file name and build the output path
            // of the image to be processed by the engine.
            let fileName = File.extractName( this.inputFiles[ i ] );
            let outputDir = this.outputDir;
            if ( outputDir.length == 0 )
               outputDir = File.extractDrive( this.inputFiles[ i ] ) + File.extractDirectory( this.inputFiles[ i ] );
            let outputFilePath = outputDir + '/' + fileName + this.postfix + ".xisf";

            // open source image
            ImageWindow.open( this.inputFiles[ i ] )[ 0 ].show();

            this.executeOnActiveImage( outputFilePath );

            processEvents();

            if ( console.abortRequested )
            {
               console.criticalln( "<end><cbr>&lt;* abort *&gt;" );
               return;
            }
         }
      }
   };
}

#endif // __PJSR_LinearPatternSubtraction_jsh

// ----------------------------------------------------------------------------
// EOF pjsr/LinearPatternSubtraction.jsh - Released 2023-06-12T18:03:39Z

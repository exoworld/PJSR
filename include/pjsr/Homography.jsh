//     ____       __ _____  ____
//    / __ \     / // ___/ / __ \
//   / /_/ /__  / / \__ \ / /_/ /
//  / ____// /_/ / ___/ // _, _/   PixInsight JavaScript Runtime
// /_/     \____/ /____//_/ |_|    PJSR Version 1.0
// ----------------------------------------------------------------------------
// pjsr/Homography.jsh - Released 2023-06-12T18:03:39Z
// ----------------------------------------------------------------------------
// This file is part of the PixInsight JavaScript Runtime (PJSR).
// PJSR is an ECMA-262-5 compliant framework for development of scripts on the
// PixInsight platform.
//
// Copyright (c) 2003-2023 Pleiades Astrophoto S.L. All Rights Reserved.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#ifndef __PJSR_Homography_jsh
#define __PJSR_Homography_jsh

/*
 * Homography geometric transformation.
 *
 * A two-dimensional projective transformation, or homography, is a
 * line-preserving geometric transformation between two sets of points in the
 * plane. More formally, if P represents the set of points in the plane, a
 * homography is an invertible mapping H from P^2 to itself such that three
 * points p1, p2, p3 are collinear if and only if H(p1), H(p2), H(p3) do.
 *
 * Homographies have important practical applications in the field of computer
 * vision. On the PixInsight platform, this class is an essential component of
 * image registration and astrometry processes.
 *
 * A Homography object can be constructed in three ways:
 *
 * new Homography( Array P1, Array P2 )
 *
 *    Constructor from two 2D point lists. Each element in the P1 and P2 arrays
 *    must have x and y numeric properties. There can be undefined array
 *    elements, but at least 4 defined point pairs are required.
 *
 *    This constructor computes a homography transformation to generate a list
 *    P2 of transformed points from a list P1 of original points. In other
 *    words, the computed homography H works as follows:
 *
 *    P2 = H( P1 )
 *
 *    The transformation matrix is calculated by the Direct Linear
 *    Transformation (DLT) method (see Reference 1). Both point lists must
 *    contain at least four points. If one of the specified point lists
 *    contains less than four points, or if no homography can be estimated from
 *    the specified point lists (which leads to a singular transformation
 *    matrix), this constructor throws an Error exception.
 *
 * new Homography( Matrix H )
 *
 *    Constructs a homography with a prescribed transformation matrix H.
 *
 * new Homography()
 *
 *    Constructs an uninitialized object.
 *
 * References:
 *
 * 1. R. Hartley, In defense of the eight-point algorithm. IEEE Transactions on
 *    Pattern Analysis and Machine Intelligence, vol. 19, pp. 580–593, June
 *    1997.
 */
function Homography( P1, P2 )
{
   this.__base__ = Object;
   this.__base__();

   // -------------------------------------------------------------------------
   // Private implementation
   // -------------------------------------------------------------------------

   /*
    * Implementation of the Direct Linear Transformation (DLT) method to
    * compute a normalized homography matrix.
    */
   function DLT( P1, P2 )
   {
      function DLTNormalizedPoints( points )
      {
         /*
          * Calculate the centroid of the input set of points.
          */
         let cx = 0, cy = 0;
         for ( let i = 0; i < points.length; ++i )
         {
            cx += points[i].x;
            cy += points[i].y;
         }
         cx /= points.length;
         cy /= points.length;

         /*
          * Calculate mean centroid distance and move origin to the centroid.
          */
         this.N = []; // the normalized points
         let d0 = 0;
         for ( let i = 0; i < points.length; ++i )
         {
            let dx = points[i].x - cx;
            let dy = points[i].y - cy;
            this.N.push( { x: dx, y: dy } );
            d0 += Math.sqrt( dx*dx + dy*dy );
         }
         d0 /= points.length;

         /*
          * Scale point coordinates.
          */
         let scale = Math.SQRT2/d0;
         for ( let i = 0; i < this.N.length; ++i )
         {
            this.N[i].x *= scale;
            this.N[i].y *= scale;
         }

         /*
          * Form the normalization matrix.
          */
         this.T = new Matrix( scale, 0.0,   -scale*cx,
                              0.0,   scale, -scale*cy,
                              0.0,   0.0,   1.0 );
      }

      /*
       * Gather all defined point pairs in both lists.
       */
      let Q1 = [], Q2 = [];
      let n = 0;
      for ( let i = 0; i < P1.length && i < P2.length; ++i )
         if ( P1[i] && P2[i] )
         {
            Q1.push( P1[i] );
            Q2.push( P2[i] );
            ++n;
         }
      if ( n < 4 )
         throw new Error( "Homography::DLT(): less than four points specified." );

      /*
       * Normalize all points.
       */
      let p1 = new DLTNormalizedPoints( Q1 );
      let p2 = new DLTNormalizedPoints( Q2 );

      /*
       * Setup cross product matrix.
       */
      let A = new Matrix( 2*n, 9 );
      for ( let i = 0; i < n; ++i )
      {
         let x1 = p1.N[i].x;
         let y1 = p1.N[i].y;
         let x2 = p2.N[i].x;
         let y2 = p2.N[i].y;
         let i0 = 2*i;
         let i1 = 2*i + 1;

         A.at( i0, 0,  0 );
         A.at( i0, 1,  0 );
         A.at( i0, 2,  0 );
         A.at( i0, 3, -x1 );
         A.at( i0, 4, -y1 );
         A.at( i0, 5, -1 );
         A.at( i0, 6,  y2*x1 );
         A.at( i0, 7,  y2*y1 );
         A.at( i0, 8,  y2 );

         A.at( i1, 0,  x1 );
         A.at( i1, 1,  y1 );
         A.at( i1, 2,  1 );
         A.at( i1, 3,  0 );
         A.at( i1, 4,  0 );
         A.at( i1, 5,  0 );
         A.at( i1, 6, -x2*x1 );
         A.at( i1, 7, -x2*y1 );
         A.at( i1, 8, -x2 );
      }

      /*
       * SVD of cross product matrix.
       */
      let SVD = Math.svd( A ); // [U,W,V]
      let W = SVD[1];
      let V = SVD[2];

      /*
       * For sanity, set to zero all insignificant singular values.
       */
      for ( let i = 0; i < W.length; ++i )
         if ( Math.abs( W.at( i ) ) <= Math.EPSILON )
            W.at( i, 0 );

      /*
       * Locate the smallest nonzero singular value.
       */
      let i = 0;
      while ( W.at( i ) == 0 )
         if ( ++i == W.length )
            throw new Error( "Homography.DLT(): invalid SVD result." );
      for ( let j = i; ++j < W.length; )
         if ( W.at( j ) != 0 )
            if ( W.at( j ) < W.at( i ) )
               i = j;

      /*
       * The components of the homography matrix are those of the smallest
       * eigenvector, i.e. the column vector of V corresponding to the smallest
       * singular value.
       */
      let H = new Matrix( V.at( 0, i ), V.at( 1, i ), V.at( 2, i ),
                          V.at( 3, i ), V.at( 4, i ), V.at( 5, i ),
                          V.at( 6, i ), V.at( 7, i ), V.at( 8, i ) );

      if ( Math.abs( H.at( 2, 2 ) ) <= Math.EPSILON )
         throw new Error( "Homography.DLT(): singular matrix." );

      /*
       * Denormalize matrix components.
       */
      H = p2.T.inverse().mul( H ).mul( p1.T );

      /*
       * Normalize matrix so that H[2,2] = 1.
       */
      H.div( H.at( 2, 2 ) );

      return H;
   }

   // -------------------------------------------------------------------------
   // Public interface.
   // -------------------------------------------------------------------------

   if ( P1 && P2 )
   {
      // Constructed from two point lists: DLT algorithm.
      if ( !(P1 instanceof Array) || !(P2 instanceof Array) )
         throw new Error( "Homography( P1, P2 ): expected two Array objects." );
      this.H = DLT( P1, P2 );
   }
   else if ( P1 )
   {
      // Constructed with a prescribed transformation matrix.
      if ( !(P1 instanceof Matrix) )
         throw new Error( "Homography( M ): expected a Matrix object." );
      this.H = new Matrix( P1 );
   }
   else
   {
      // Constructed uninitialized.
      this.H = new Matrix;
   }

   /*
    * Coordinate transformation. Applies this homography to the specified point
    * p, which must have x and y numeric properties. Returns a new point with
    * the transformed x and y coordinates.
    */
   this.apply = function( p )
   {
      let w = this.H.at( 2, 0 )*p.x + this.H.at( 2, 1 )*p.y + this.H.at( 2, 2 );
      if ( 1 + w == 1 )
         throw new Error( "Homography.apply(): Invalid transformation matrix" );
      return { x: (this.H.at( 0, 0 )*p.x + this.H.at( 0, 1 )*p.y + this.H.at( 0, 2 ))/w,
               y: (this.H.at( 1, 0 )*p.x + this.H.at( 1, 1 )*p.y + this.H.at( 1, 2 ))/w };
   };

   /*
    * Returns the inverse of this transformation. If this transformation has
    * been computed from two point lists P1 and P2:
    *
    * P2 = H( P1 )
    *
    * then this function returns a transformation H1 such that:
    *
    * P1 = H1( P2 )
    */
   this.inverse = function()
   {
      return new Homography( this.H.inverse() );
   };

   /*
    * Returns true iff this transformation has been initialized.
    */
   this.isValid = function()
   {
      return !this.H.isEmpty();
   };

   /*
    * Returns true iff this is an affine homography transformation.
    *
    * An affine homography is a special type of a general homography where the
    * last row of the 3x3 transformation matrix is equal to (0, 0, 1). This
    * function verifies that this property holds for the current transformation
    * matrix (if it is valid) up to the machine epsilon for the 64-bit floating
    * point type.
    */
   this.isAffine = function()
   {
      return     this.isValid()
          &&     Math.abs( this.H.at( 2, 0 ) ) <= Math.EPSILON
          &&     Math.abs( this.H.at( 2, 1 ) ) <= Math.EPSILON
          && 1 - Math.abs( this.H.at( 2, 2 ) ) <= Math.EPSILON;
   };
}

Homography.prototype = new Object;

#endif   // __PJSR_Homography_jsh

// ----------------------------------------------------------------------------
// EOF pjsr/Homography.jsh - Released 2023-06-12T18:03:39Z

// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// SubmitCPDMain.js - Released 2022-04-13T18:28:34Z
// ----------------------------------------------------------------------------
//
// This file is part of PixInsight CPD Submission Utility Script 1.0
//
// Copyright (c) 2021-2022 Pleiades Astrophoto S.L.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/*
 * PixInsight CPD Submission Utility
 *
 * Copyright (C) 2021-2022 Pleiades Astrophoto. All Rights Reserved.
 * Written by Juan Conejero (PTeam)
 *
 * Script entry point.
 */

#include <pjsr/StdButton.jsh>
#include <pjsr/StdIcon.jsh>

function main()
{
   g_workingData.loadSettings();

   let dialog = new SubmitCPDDialog();
   for ( ;; )
   {
      try
      {
         console.show();

         if ( dialog.execute() )
         {
            if ( g_workingData.useKeysFile )
            {
               if ( g_workingData.keysFile.length == 0 )
               {
                  (new MessageBox( "No signing keys file has been specified.",
                                 TITLE, StdIcon_Error, StdButton_Ok )).execute();
                  continue;
               }

               if ( !File.exists( g_workingData.keysFile ) )
               {
                  (new MessageBox( "The specified signing keys file does not exist.",
                                 TITLE, StdIcon_Error, StdButton_Ok )).execute();
                  continue;
               }

               /*
                * N.B. The password ByteArray will be securely wiped out with
                * zeros by Security.submitCertifiedDeveloperData().
                */
               let password = dialog.password_Edit.utf8;
               Security.submitCertifiedDeveloperDataWithSigningKeysFile( g_workingData.keysFile, password,
                                                      g_workingData.contactEmail.trim(),
                                                      g_workingData.publicEmail.trim(),
                                                      g_workingData.url.trim(),
                                                      g_workingData.name.trim(),
                                                      g_workingData.info.trim() );
            }
            else
            {
               if ( g_workingData.developerId.trim().length == 0 )
               {
                  (new MessageBox( "No developer identifier has been specified.",
                                 TITLE, StdIcon_Error, StdButton_Ok )).execute();
                  continue;
               }

               if ( !dialog.publicKeyHex_Edit.isValid )
               {
                  (new MessageBox( "<p>Not a valid public signing key in hexadecimal format.</p>" +
                                 "<p>A public signing key must be specified as 64 hexadecimal digits (0-9, a-z or A-Z).</p>",
                                 TITLE, StdIcon_Error, StdButton_Ok )).execute();
                  continue;
               }

               if ( g_workingData.contactEmail.trim().length == 0 )
               {
                  (new MessageBox( "No contact email address has been specified.",
                                 TITLE, StdIcon_Error, StdButton_Ok )).execute();
                  continue;
               }

               let publicKey = ByteArray.fromHex( g_workingData.publicKeyHex );
               if ( publicKey.length != 32 )
               {
                  // This should not happen, since we are using a regexp to validate the Edit control.
                  (new MessageBox( "Not a valid public key. Must have 32 bytes (64 hexadecimal digits).",
                                 TITLE, StdIcon_Error, StdButton_Ok )).execute();
                  continue;
               }

               Security.submitCertifiedDeveloperData( g_workingData.developerId.trim(), publicKey,
                                                      g_workingData.contactEmail.trim(),
                                                      g_workingData.publicEmail.trim(),
                                                      g_workingData.url.trim(),
                                                      g_workingData.name.trim(),
                                                      g_workingData.info.trim() );
            }

            console.noteln( "* CPD data submitted successfully." );

            (new MessageBox( "<p><b>Your Certified PixInsight Developer data has been submitted.</b></p>" +
                             "<p>Thank you for helping us maintain our code security system. If necessary, " +
                             "we'll contact you as soon as possible regarding your request. " +
                             "If/when appropriate, the data you have provided will be released as an update, " +
                             "including a possibly updated public code signing key.</p>",
                             "PixInsight", StdIcon_Information, StdButton_Ok )).execute();

            g_workingData.saveSettings();
         }

         console.hide();

         break;
      }
      catch ( x )
      {
         console.criticalln( "<end><cbr><raw>" + x.toString() + "</raw>" );
      }
   }
}

// ----------------------------------------------------------------------------
// EOF SubmitCPDMain.js - Released 2022-04-13T18:28:34Z

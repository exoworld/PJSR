// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// MakGenGenerators.js - Released 2023-04-12T09:25:12Z
// ----------------------------------------------------------------------------
//
// This file is part of PixInsight Makefile Generator Script version 1.129
//
// Copyright (c) 2009-2023 Pleiades Astrophoto S.L.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/*
 * PixInsight Makefile Generator
 *
 * Automatic generation of PCL makefiles and projects for FreeBSD, Linux,
 * macOS and Windows platforms.
 *
 * Copyright (c) 2009-2023, Pleiades Astrophoto S.L. All Rights Reserved.
 * Written by Juan Conejero (PTeam)
 *
 * Makefile generation
 */

function writeSeparator()
{
   console.writeln( "<end><cbr><br>" + '='.repeat( 70 ) );
}

function GenerateAll( files, parameters )
{
   parameters.architecture = "x64";

   parameters.platform = "Host";
   GnuCxx( files, parameters );
   GnuCxxAll( files, parameters );

   parameters.platform = "FreeBSD";
   GnuCxx( files, parameters );
   GnuCxxAll( files, parameters );

   parameters.platform = "Linux";
   GnuCxx( files, parameters );
   GnuCxxAll( files, parameters );

   parameters.platform = "MacOSX";
   GnuCxx( files, parameters );
   GnuCxxAll( files, parameters );

   parameters.platform = "Windows";
   MSVCxxAll( files, parameters );
}

/*
 * Makefiles and projects for PixInsight Class Library
 */
function GeneratePCLMakefiles( compatibility )
{
   writeSeparator();
   console.writeln( "Generating makefiles for PixInsight Class Library" );
   console.flush();

   let files = new FileLists( PCLSRCDIR + "/pcl" );

   let parameters = new GeneratorParameters();
   parameters.id = "PCL";
   parameters.type = "StaticLibrary";
   parameters.official = true;
   parameters.gccOptimization = "3";
   parameters.hidden = true;

   GenerateAll( files, parameters );

   if ( compatibility )
   {
      parameters.compatibility = true;
      GenerateAll( files, parameters );
   }
}

/*
 * Makefiles and projects for PixInsight Core executable
 */
function GenerateCoreMakefiles( compatibility )
{
   writeSeparator();
   console.writeln( "Generating makefiles for PixInsight Core executable" );
   console.flush();

   let files = new FileLists( PCLSRCDIR + "/core", true/*noImages*/ );

   let parameters = new GeneratorParameters();
   parameters.id = "PixInsight";
   parameters.type = "Core";
   parameters.official = true;
   parameters.gccOptimization = "3";

   GenerateAll( files, parameters );

   if ( compatibility )
   {
      parameters.compatibility = true;
      GenerateAll( files, parameters );
   }
}

/*
 * Makefiles and projects for the updater1 program
 */
function GenerateUpdater1Makefiles( compatibility )
{
   writeSeparator();
   console.writeln( "Generating makefiles for PixInsight updater1 program" );
   console.flush();

   let files = new FileLists( PCLSRCDIR + "/core-aux/updater1" );

   let parameters = new GeneratorParameters();
   parameters.id = "PixInsightUpdater";
   parameters.type = "CoreExecutable";
   parameters.official = true;
   parameters.gccOptimization = "s";
   parameters.useAVX2 = false;
   parameters.uacAdmin = true;  // updater1 requires administrative privileges on Windows

   GenerateAll( files, parameters );

   if ( compatibility )
   {
      parameters.compatibility = true;
      GenerateAll( files, parameters );
   }
}

/*
 * Makefiles and projects for the updater2 program
 */
function GenerateUpdater2Makefiles( compatibility )
{
   writeSeparator();
   console.writeln( "Generating makefiles for PixInsight updater2 program" );
   console.flush();

   let files = new FileLists( PCLSRCDIR + "/core-aux/updater2" );

   let parameters = new GeneratorParameters();
   parameters.id = "updater2";
   parameters.type = "CoreExecutable";
   parameters.official = true;
   parameters.gccOptimization = "s";
   parameters.useAVX2 = false;

   GenerateAll( files, parameters );

   if ( compatibility )
   {
      parameters.compatibility = true;
      GenerateAll( files, parameters );
   }
}

/*
 * Makefiles and projects for the updater3 program
 */
function GenerateUpdater3Makefiles( compatibility )
{
   writeSeparator();
   console.writeln( "Generating makefiles for PixInsight updater3 program" );
   console.flush();

   let files = new FileLists( PCLSRCDIR + "/core-aux/updater3" );

   let parameters = new GeneratorParameters();
   parameters.id = "updater3";
   parameters.type = "CoreAux";
   parameters.official = true;
   parameters.gccOptimization = "s";
   parameters.useAVX2 = false;

   GenerateAll( files, parameters );

   if ( compatibility )
   {
      parameters.compatibility = true;
      GenerateAll( files, parameters );
   }
}

/*
 * Makefiles and projects for all the updater programs
 */
function GenerateUpdaterMakefiles( compatibility )
{
   GenerateUpdater1Makefiles( compatibility );
   GenerateUpdater2Makefiles( compatibility );
   GenerateUpdater3Makefiles( compatibility );
}

/*
 * Makefiles and projects for the X11 UNIX/Linux installer program
 */
function GenerateX11InstallerMakefiles( compatibility )
{
   writeSeparator();
   console.writeln( "Generating makefiles for PixInsight X11 UNIX/Linux installer program" );
   console.flush();

   let files = new FileLists( PCLSRCDIR + "/installer/x11" );

   let parameters = new GeneratorParameters();
   parameters.id = "installer";
   parameters.type = "X11Installer";
   parameters.official = true;
   parameters.gccOptimization = "s";
   parameters.useAVX2 = false;

   parameters.platform = "Host";
   GnuCxx( files, parameters );
   GnuCxxAll( files, parameters );

   parameters.platform = "Linux";
   GnuCxx( files, parameters );
   GnuCxxAll( files, parameters );

   parameters.platform = "FreeBSD";
   GnuCxx( files, parameters );
   GnuCxxAll( files, parameters );

   if ( compatibility )
   {
      parameters.compatibility = true;

      parameters.platform = "Host";
      GnuCxx( files, parameters );
      GnuCxxAll( files, parameters );

      parameters.platform = "Linux";
      GnuCxx( files, parameters );
      GnuCxxAll( files, parameters );

      parameters.platform = "FreeBSD";
      GnuCxx( files, parameters );
      GnuCxxAll( files, parameters );
   }
}

/*
 * Makefiles and projects for the PCLH code maintenance utility
 */
function GeneratePCLHMakefiles()
{
   writeSeparator();
   console.writeln( "Generating makefiles for PixInsight pclh utility" );
   console.flush();

   let files = new FileLists( PCLSRCDIR + "/utils/pclh" );

   let parameters = new GeneratorParameters();
   parameters.id = "pclh";
   parameters.type = "Executable";
   parameters.official = true;
   parameters.gccOptimization = "2"; // pclh is *extremely* critical code
   parameters.useAVX2 = false;

   GenerateAll( files, parameters );
}

/*
 * Makefiles and projects for a standard PixInsight module
 */
function GenerateStandardModuleMakefiles( module, compatibility,
                                          extraDefinitions, winExtraDefinitions, extraIncludeDirs,
                                          extraLibDirs, extraLibraries,
                                          winExtraLibraries, winExtraIncludeDirs, linuxExtraLibraries )
{
   writeSeparator();
   console.writeln( "Generating makefiles for PixInsight module: " + module );
   console.flush();

   let files = new FileLists( PCLSRCDIR + "/modules/" + module );

   let parameters = new GeneratorParameters();
   parameters.id = File.extractName( module );
   parameters.type = "Module";
   parameters.official = true;
   parameters.gccOptimization = "3";
   parameters.winExtraDefinitions.push( "_CRT_SECURE_NO_WARNINGS" );
   if ( extraDefinitions )
      for ( let i = 0; i < extraDefinitions.length; ++i )
         parameters.extraDefinitions.push( extraDefinitions[i] );
   if ( winExtraDefinitions )
      for ( let i = 0; i < winExtraDefinitions.length; ++i )
         parameters.winExtraDefinitions.push( winExtraDefinitions[i] );
   if ( extraIncludeDirs )
      for ( let i = 0; i < extraIncludeDirs.length; ++i )
         parameters.extraIncludeDirs.push( extraIncludeDirs[i] );
   if ( extraLibDirs )
      for ( let i = 0; i < extraLibDirs.length; ++i )
         parameters.extraLibDirs.push( extraLibDirs[i] );
   if ( extraLibraries )
      for ( let i = 0; i < extraLibraries.length; ++i )
         parameters.extraLibraries.push( extraLibraries[i] );
   if ( winExtraLibraries )
      for ( let i = 0; i < winExtraLibraries.length; ++i )
         parameters.winExtraLibraries.push( winExtraLibraries[i] );
   if ( winExtraIncludeDirs )
      for ( let i = 0; i < winExtraIncludeDirs.length; ++i )
         parameters.winExtraIncludeDirs.push( winExtraIncludeDirs[i] );
   if ( linuxExtraLibraries )
      for ( let i = 0; i < linuxExtraLibraries.length; ++i )
         parameters.linuxExtraLibraries.push( linuxExtraLibraries[i] );

   GenerateAll( files, parameters );

   if ( compatibility )
   {
      parameters.compatibility = true;
      GenerateAll( files, parameters );
   }
}

/*
 * Makefiles and projects for a standard PixInsight dynamic library
 */
function GenerateStandardDynamicLibraryMakefiles( library, compatibility,
                                                  winDefFile, extraDefinitions, winExtraDefinitions,
                                                  extraIncludeDirs, extraLibDirs, extraLibraries )
{
   writeSeparator();
   console.writeln( "Generating makefiles for dynamic library: " + library );
   console.flush();

   let files = new FileLists( PCLSRCDIR + '/' + library );

   let parameters = new GeneratorParameters();
   parameters.id = File.extractName( library );
   parameters.type = "DynamicLibrary";
   parameters.official = true;
   parameters.winDefFile = winDefFile;
   parameters.winExtraDefinitions.push( "_CRT_SECURE_NO_WARNINGS" );
   for ( let i = 0; i < extraDefinitions.length; ++i )
      parameters.extraDefinitions.push( extraDefinitions[i] );
   for ( let i = 0; i < winExtraDefinitions.length; ++i )
      parameters.winExtraDefinitions.push( winExtraDefinitions[i] );
   for ( let i = 0; i < extraIncludeDirs.length; ++i )
      parameters.extraIncludeDirs.push( extraIncludeDirs[i] );
   for ( let i = 0; i < extraLibDirs.length; ++i )
      parameters.extraLibDirs.push( extraLibDirs[i] );
   for ( let i = 0; i < extraLibraries.length; ++i )
      parameters.extraLibraries.push( extraLibraries[i] );

   GenerateAll( files, parameters );

   if ( compatibility )
   {
      parameters.compatibility = true;
      GenerateAll( files, parameters );
   }
}

/*
 * Makefiles and projects for a standard PixInsight static library
 */
function GenerateStandardStaticLibraryMakefiles( library, compatibility,
                                                 extraDefinitions, winExtraDefinitions, extraIncludeDirs )
{
   writeSeparator();
   console.writeln( "Generating makefiles for static library: " + library );
   console.flush();

   let files = new FileLists( PCLSRCDIR + '/' + library );

   let parameters = new GeneratorParameters();
   parameters.id = File.extractName( library );
   parameters.type = "StaticLibrary";
   parameters.official = true;
   parameters.winExtraDefinitions.push( "_CRT_SECURE_NO_WARNINGS" );
   if ( extraDefinitions )
      for ( let i = 0; i < extraDefinitions.length; ++i )
         parameters.extraDefinitions.push( extraDefinitions[i] );
   if ( winExtraDefinitions )
      for ( let i = 0; i < winExtraDefinitions.length; ++i )
         parameters.winExtraDefinitions.push( winExtraDefinitions[i] );
   if ( extraIncludeDirs )
      for ( let i = 0; i < extraIncludeDirs.length; ++i )
         parameters.extraIncludeDirs.push( extraIncludeDirs[i] );

   GenerateAll( files, parameters );

   if ( compatibility )
   {
      parameters.compatibility = true;
      GenerateAll( files, parameters );
   }
}

function GenerateStandardFileFormatModuleMakefiles( compatibility )
{
   GenerateStandardModuleMakefiles( "file-formats/BMP",        compatibility );
   GenerateStandardModuleMakefiles( "file-formats/FITS",       compatibility, null, null, null, null, ["cfitsio-pxi"] );
   GenerateStandardModuleMakefiles( "file-formats/JPEG",       compatibility, null, null, null, null, ["jpeg-pxi"] );
   GenerateStandardModuleMakefiles( "file-formats/JPEG2000",   compatibility, null, null, null, null, ["jasper-pxi"] );
   GenerateStandardModuleMakefiles( "file-formats/RAW",        compatibility, null, ["WIN32","LIBRAW_NODLL"], null, null, ["libraw_r-pxi"] );
   GenerateStandardModuleMakefiles( "file-formats/RAW-compat", compatibility, null, ["WIN32","LIBRAW_NODLL"], null, null, ["libraw_r-compat-pxi"] );
   GenerateStandardModuleMakefiles( "file-formats/TIFF",       compatibility, null, null, null, null, ["libtiff-pxi", "jpeg-pxi"] );
   GenerateStandardModuleMakefiles( "file-formats/XISF",       compatibility );
}

function GenerateStandardProcessModuleMakefiles( compatibility )
{
   GenerateStandardModuleMakefiles( "processes/APASS",                             compatibility );
   GenerateStandardModuleMakefiles( "processes/BackgroundModelization",            compatibility );
   GenerateStandardModuleMakefiles( "processes/CloneStamp",                        compatibility );
   GenerateStandardModuleMakefiles( "processes/ColorCalibration",                  compatibility );
   GenerateStandardModuleMakefiles( "processes/ColorManagement",                   compatibility );
   GenerateStandardModuleMakefiles( "processes/ColorSpaces",                       compatibility );
   GenerateStandardModuleMakefiles( "processes/Compatibility",                     compatibility );
   GenerateStandardModuleMakefiles( "processes/Convolution",                       compatibility );
   GenerateStandardModuleMakefiles( "processes/Deconvolution",                     compatibility );
   GenerateStandardModuleMakefiles( "processes/EphemerisGeneration",               compatibility );
   GenerateStandardModuleMakefiles( "processes/FindingChart",                      compatibility );
   GenerateStandardModuleMakefiles( "processes/Flux",                              compatibility );
   GenerateStandardModuleMakefiles( "processes/Fourier",                           compatibility );
   GenerateStandardModuleMakefiles( "processes/Gaia",                              compatibility );
   GenerateStandardModuleMakefiles( "processes/Geometry",                          compatibility );
   GenerateStandardModuleMakefiles( "processes/Global",                            compatibility );
   GenerateStandardModuleMakefiles( "processes/GREYCstoration",                    compatibility );
   GenerateStandardModuleMakefiles( "processes/Image",                             compatibility, null, ["CMINPACK_NO_DLL"] );
   GenerateStandardModuleMakefiles( "processes/ImageCalibration",                  compatibility );
   GenerateStandardModuleMakefiles( "processes/ImageIntegration",                  compatibility );
   GenerateStandardModuleMakefiles( "processes/ImageRegistration",                 compatibility );
   GenerateStandardModuleMakefiles( "processes/IntensityTransformations",          compatibility );
   GenerateStandardModuleMakefiles( "processes/MaskGeneration",                    compatibility );
   GenerateStandardModuleMakefiles( "processes/Morphology",                        compatibility );
   GenerateStandardModuleMakefiles( "processes/MultiscaleProcessing",              compatibility );
   GenerateStandardModuleMakefiles( "processes/NetworkService",                    compatibility );
   GenerateStandardModuleMakefiles( "processes/NoiseGeneration",                   compatibility );
   GenerateStandardModuleMakefiles( "processes/NoiseReduction",                    compatibility );
   GenerateStandardModuleMakefiles( "processes/NoOperation",                       compatibility );
   GenerateStandardModuleMakefiles( "processes/Obsolete",                          compatibility );
   GenerateStandardModuleMakefiles( "processes/PixelMath",                         compatibility );
   GenerateStandardModuleMakefiles( "processes/RestorationFilters",                compatibility );
   GenerateStandardModuleMakefiles( "processes/Sandbox",                           compatibility );
//   GenerateStandardModuleMakefiles( "processes/StarGenerator",                     compatibility );
   GenerateStandardModuleMakefiles( "processes/TGV",                               compatibility );
   GenerateStandardModuleMakefiles( "processes/contrib/cleger/SubframeSelector",   compatibility, null, ["CMINPACK_NO_DLL"] );
   GenerateStandardModuleMakefiles( "processes/contrib/gviehoever/GradientDomain", compatibility );
   GenerateStandardModuleMakefiles( "processes/contrib/kkretzschmar/INDIClient",   compatibility, null, null, ["$(INDIGODIR)/indigo_libs"],
                                                                     ["$(INDIGODIR)/build/lib"], ["indigo"], ["indigo_client","libpthreadVC3"],
                                                                     ["$(INDIGODIR)/indigo_windows/indigo_client/externals/pthreads4w/include"],
                                                                     ["dns_sd"]/*linuxExtraLibraries*/ );
   GenerateStandardModuleMakefiles( "processes/contrib/nvolkov/Blink",                       compatibility );
   GenerateStandardModuleMakefiles( "processes/contrib/nvolkov/CometAlignment",              compatibility );
   GenerateStandardModuleMakefiles( "processes/contrib/nvolkov/CosmeticCorrection",          compatibility );
   GenerateStandardModuleMakefiles( "processes/contrib/nvolkov/SplitCFA",                    compatibility );
   GenerateStandardModuleMakefiles( "processes/contrib/sharkmelley/ArcsinhStretch",          compatibility );
   GenerateStandardModuleMakefiles( "processes/contrib/spool/Debayer",                       compatibility );
   GenerateStandardModuleMakefiles( "processes/contrib/zvrastil/Annotation",                 compatibility );
   GenerateStandardModuleMakefiles( "processes/contrib/zvrastil/AssistedColorCalibration",   compatibility );
   GenerateStandardModuleMakefiles( "processes/contrib/zvrastil/LocalHistogramEqualization", compatibility );
}

function GeneratePixInsightPlatformMakefiles( compatibility )
{
   // PixInsight Class Library
   GeneratePCLMakefiles( compatibility );

   // CFITSIO FITS format support library - version >= 3.37
   GenerateStandardStaticLibraryMakefiles( "3rdparty/cfitsio",  compatibility, ["f2cFortran"], ["_MBCS", "FF_NO_UNISTD_H"] );

   // CMINPACK library
   GenerateStandardStaticLibraryMakefiles( "3rdparty/cminpack", compatibility, null, ["CMINPACK_NO_DLL"] );

   // JasPer JPEG2000 format support library
   GenerateStandardStaticLibraryMakefiles( "3rdparty/jasper",   compatibility );

   // JPEG format support library
   GenerateStandardStaticLibraryMakefiles( "3rdparty/jpeg",     compatibility );

   // Little CMS engine
   GenerateStandardStaticLibraryMakefiles( "3rdparty/lcms",     compatibility, ["CMS_NO_REGISTER_KEYWORD=1"] );

   // LibTIFF TIFF format support library
   GenerateStandardStaticLibraryMakefiles( "3rdparty/libtiff",  compatibility, null, null, ["$(PCLSRCDIR)/3rdparty/jpeg", "$(PCLSRCDIR)/3rdparty/zlib"] );

   // LZ4 data compression library
   GenerateStandardStaticLibraryMakefiles( "3rdparty/lz4",      compatibility );

   // RFC6234 cryptographic hashing library
   GenerateStandardStaticLibraryMakefiles( "3rdparty/RFC6234",  compatibility );

   // ZLIB data compression library
   GenerateStandardStaticLibraryMakefiles( "3rdparty/zlib",     compatibility );

   // PixInsight Core application
   GenerateCoreMakefiles( compatibility );

   // Updater programs
   GenerateUpdaterMakefiles( compatibility );

   // X11 install program
   GenerateX11InstallerMakefiles( compatibility );

   // Standard file format modules
   GenerateStandardFileFormatModuleMakefiles( compatibility );

   // Standard process modules
   GenerateStandardProcessModuleMakefiles( compatibility );

   // PCL source code maintenance utility
   GeneratePCLHMakefiles();
}

// ----------------------------------------------------------------------------
// EOF MakGenGenerators.js - Released 2023-04-12T09:25:12Z

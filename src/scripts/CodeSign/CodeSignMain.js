// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// CodeSignMain.js - Released 2022-04-13T15:13:33Z
// ----------------------------------------------------------------------------
//
// This file is part of PixInsight Code Signing Utility Script 1.0
//
// Copyright (c) 2021-2022 Pleiades Astrophoto S.L.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/*
 * PixInsight Code Signing Utility
 *
 * Copyright (C) 2021-2022 Pleiades Astrophoto. All Rights Reserved.
 * Written by Juan Conejero (PTeam)
 *
 * Script entry point.
 */

#include <pjsr/StdButton.jsh>
#include <pjsr/StdIcon.jsh>

function main()
{
   g_workingData.loadSettings();

   let dialog = new CodeSigningDialog();
   for ( ;; )
   {
      try
      {
         console.show();

         if ( dialog.execute() )
         {
            if ( g_workingData.keysFile.length == 0 )
            {
               (new MessageBox( "No signing keys file has been specified.",
                              TITLE, StdIcon_Error, StdButton_Ok )).execute();
               continue;
            }

            if ( !File.exists( g_workingData.keysFile ) )
            {
               (new MessageBox( "The specified signing keys file does not exist.",
                              TITLE, StdIcon_Error, StdButton_Ok )).execute();
               continue;
            }

            if ( g_workingData.targetFiles.length == 0 )
            {
               (new MessageBox( "No target files have been specified.",
                              TITLE, StdIcon_Error, StdButton_Ok )).execute();
               continue;
            }

            let password = dialog.password_Edit.utf8;
            if ( password.isEmpty )
            {
               (new MessageBox( "No password has been specified.",
                              TITLE, StdIcon_Error, StdButton_Ok )).execute();
               continue;
            }

            /*
             * N.B. The password ByteArray will be securely wiped out by
             * Security.loadSigningKeysFile().
             */
            let keys = Security.loadSigningKeysFile( g_workingData.keysFile, password );
            if ( !keys.valid )
            {
               keys.publicKey.secureFill();
               keys.privateKey.secureFill();
               (new MessageBox( "Invalid signing keys file.",
                              TITLE, StdIcon_Error, StdButton_Ok )).execute();
               continue;
            }

            let success = 0;

            for ( let i in g_workingData.targetFiles )
            {
               try
               {
                  let filePath = g_workingData.targetFiles[i];
                  console.writeln( "<end><cbr><br><raw>" + filePath + "</raw>" );

                  let ext = File.extractExtension( filePath );
                  if ( ext == ".js" || ext == ".scp" )
                  {
                     let signaturePath = File.changeExtension( filePath, ".xsgn" );
                     Security.generateScriptSignatureFile(
                                    signaturePath,
                                    filePath,
                                    g_workingData.entitlements,
                                    keys.developerId,
                                    keys.publicKey,
                                    keys.privateKey );

                     console.noteln( "* Script signature generated: <raw>" + signaturePath + "</raw>" );
                  }
                  else if ( ext == ".jsh" )
                  {
                     let signaturePath = File.changeExtension( filePath, ".xsgn" );
                     Security.generateCodeSignatureFile(
                                    File.changeExtension( filePath, ".xsgn" ),
                                    filePath,
                                    keys.developerId,
                                    keys.publicKey,
                                    keys.privateKey );

                     console.noteln( "* Code signature generated: <raw>" + signaturePath + "</raw>" );
                  }
                  else if ( ext == ".xri" )
                  {
                     Security.generateXMLSignature(
                                    filePath,
                                    keys.developerId,
                                    keys.publicKey,
                                    keys.privateKey );

                     console.noteln( "* Signed XML file: <raw>" + filePath + "</raw>" );
                  }
                  else
                  {
                     throw new Error( "Internal error: unknown file type for signing: " + filePath );
                  }

                  ++success;
               }
               catch ( x )
               {
                  console.criticalln( "<end><cbr><raw>" + x.toString() + "</raw>" );
               }
            }

            keys.publicKey.secureFill();
            keys.privateKey.secureFill();

            console.noteln( format(
               "<end><cbr><br>===== codesign: %u succeeded, %u failed =====",
               success, g_workingData.targetFiles.length-success ) );
            console.writeln();
            console.flush();

            g_workingData.saveSettings();

            if ( (new MessageBox( "Do you want to generate more code signatures?",
                                 TITLE, StdIcon_Question, StdButton_Yes, StdButton_No )).execute() == StdButton_Yes )
               continue;
         }

         console.hide();

         break;
      }
      catch ( x )
      {
         console.criticalln( "<end><cbr><raw>" + x.toString() + "</raw>" );
      }
   }
}

// ----------------------------------------------------------------------------
// EOF CodeSignMain.js - Released 2022-04-13T15:13:33Z

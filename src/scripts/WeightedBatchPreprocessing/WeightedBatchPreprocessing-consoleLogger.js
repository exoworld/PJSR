// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// WeightedBatchPreprocessing-consoleLogger.js - Released 2023-06-15T09:00:12Z
// ----------------------------------------------------------------------------
//
// This file is part of Weighted Batch Preprocessing Script version 2.5.10
//
// Copyright (c) 2019-2023 Roberto Sartori
// Copyright (c) 2020-2021 Adam Block
// Copyright (c) 2019 Tommaso Rubechi
// Copyright (c) 2012 Kai Wiechen
// Copyright (c) 2012-2023 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/**
 *
 *
 */
function ConsoleLogger()
{

   this.logFile = undefined;

   /**
    * Console logging inializer
    *
    */
   this.initialize = function( fileName )
   {
      this.logFile = fileName;
      console.beginLog();
      this.headerLength = console.logText().length;
      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "WeightedBatchPreprocessing " + VERSION );
      console.noteln( SEPARATOR );
   }

   /**
    * Appends the logs to the output file.
    *
    */
   this.flush = function()
   {
      console.flush();
      let logData = console.endLog();
      console.beginLog();
      this._appendToFile( logData );
   }

   /**
    * Append the last logs and terminate the logging.
    *
    */
   this.stopLogging = function()
   {
      console.flush();
      let logData = console.endLog();
      this._appendToFile( logData );
      this.logFile = undefined;
   }

   /**
    * Appends the given logs to the current logs file.
    *
    * @param {*} logData
    * @return {*}
    */
   this._appendToFile = function( logData )
   {
      let
      {
         existingDirectory,
      } = WBPPUtils.shared();

      if ( this.logFile == undefined )
         return;

      let logPath = existingDirectory( engine.outputDirectory + "/logs" ) +
         "/" + engine.executionTimestamp + ".log";
      try
      {
         let file;
         if ( File.exists( logPath ) )
         {
            file = File.openFile( logPath );
            file.seekEnd();
            // remove the main logs header to avoid the many repetitions along the logs file
            logData.remove( 0, this.headerLength );
         }
         else
            file = File.createFileForWriting( logPath );

         file.write( logData );
         file.close();
      }
      catch ( x )
      {
         if ( !engine.automationMode )
            ( new MessageBox( x.message, TITLE + " " + VERSION, StdIcon_Error, StdButton_Ok ) ).execute();
      }
   }

}

// ----------------------------------------------------------------------------
// EOF WeightedBatchPreprocessing-consoleLogger.js - Released 2023-06-15T09:00:12Z

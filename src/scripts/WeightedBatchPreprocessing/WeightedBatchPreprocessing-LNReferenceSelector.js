// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// WeightedBatchPreprocessing-LNReferenceSelector.js - Released 2023-06-15T09:00:12Z
// ----------------------------------------------------------------------------
//
// This file is part of Weighted Batch Preprocessing Script version 2.5.10
//
// Copyright (c) 2019-2023 Roberto Sartori
// Copyright (c) 2020-2021 Adam Block
// Copyright (c) 2019 Tommaso Rubechi
// Copyright (c) 2012 Kai Wiechen
// Copyright (c) 2012-2023 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/* beautify ignore:start */
#include "WeightedBatchPreprocessing-PreviewControl.js"
/* beautify ignore:end */

/**
 * This control manages the list of files, the corresponsing state and measurements
 * and handles the interaction events.
 *
 */
function WBPPLNFilesTable( parent )
{
   this.__base__ = Control;
   this.__base__( parent );

   this.framesTree = new TreeBox( this );
   this.framesTree.alternateRowColor = true;
   this.framesTree.onCurrentNodeUpdated = ( node ) =>
   {
      // the case where no node is selected is not handled since this case should neved occur.
      if ( node )
         this.onframeSelection( node.__activeFrame__ );
   }

   // sizers

   this.sizer = new VerticalSizer;
   this.sizer.add( this.framesTree );

   // aux functions

   /**
    * Node selection handler. This function must be invoked when the current node tree changes because of the user interacting
    * with the treeBox or when a node is programmatically selected.
    *
    * @param {*} activeFrame
    */
   this.onframeSelection = ( activeFrame ) =>
   {
      this.selectedFrame = activeFrame;
      if ( !this.STFLocked )
      {
         this.STFReference = this.selectedFrame
         this.updateSTFReferenceIcon();
      }
      if ( this.onFrameSelected )
         this.onFrameSelected( activeFrame );
   }

   //

   this.activeFrames = []; // the list of frames of the group
   this.bestFrames = {}; // the single best frame or the set of best frames depending on the reference frame generation mode
   this.selectedFrame = undefined; // the file path of the currently selected frame
   this.STFReference = undefined; // the file path of the file used for the current STF transformation
   this.STFLocked = false; // FALSE if STF parameters are updated on frame selction, TRUE if STF is freezed

   // work methods

   let numberOfDigits = function( value )
   {
      let xlog = Math.log10( value );
      let maxDecimalDigis = 3;
      let retVal;
      if ( xlog < 0 )
         retVal = Math.floor( -xlog ) + maxDecimalDigis;
      else
      {
         let N = Math.floor( xlog );
         retVal = Math.max( 0, maxDecimalDigis - N );
      }
      return retVal;
   }

   let formatFloat = function( value, digits )
   {
      return format( "%0." + digits + "f", value );
   }

   this.columnsDescriptor = [
   {
      label: "STF #",
      alignment: TextAlign_Right,
      content: ( activeFrame, i ) =>
      {
         return " " + i;
      }
   },
   {
      label: "Best",
      alignment: TextAlign_Center,
      content: () =>
      {
         return "";
      }
   },
   {
      label: "File name",
      alignment: TextAlign_Left,
      toolTip: ( activeFrame ) => ( activeFrame.current ),
      content: ( activeFrame ) =>
      {
         return File.extractNameAndExtension( activeFrame.current )
      }
   },
   {
      label: "PSF SW",
      alignment: TextAlign_Right,
      nDigits: () => ( Math.max.apply( null, this.activeFrames.map( f => numberOfDigits( f.descriptor.PSFSignalWeight ) ) ) ),
      content: ( activeFrame, i, N ) => ( formatFloat( activeFrame.descriptor.PSFSignalWeight, N ) ),
      associatedMetric: WBPPLocalNormalizationReferenceFrameMetric.PSFSW

   },
   {
      label: "PSF SNR",
      alignment: TextAlign_Right,
      nDigits: () => ( Math.max.apply( null, this.activeFrames.map( f => numberOfDigits( f.descriptor.SNR ) ) ) ),
      content: ( activeFrame, i, N ) => ( formatFloat( activeFrame.descriptor.SNR, N ) ),
      associatedMetric: WBPPLocalNormalizationReferenceFrameMetric.PSFSNR
   },
   {
      label: "M*",
      alignment: TextAlign_Right,
      nDigits: () => ( Math.max.apply( null, this.activeFrames.map( f => numberOfDigits( f.descriptor.Mstar ) ) ) ),
      content: ( activeFrame, i, N ) => ( formatFloat( activeFrame.descriptor.Mstar, N ) ),
      associatedMetric: WBPPLocalNormalizationReferenceFrameMetric.MSTAR
   },
   {
      label: "Median",
      alignment: TextAlign_Right,
      nDigits: () => ( Math.max.apply( null, this.activeFrames.map( f => numberOfDigits( f.descriptor.median ) ) ) ),
      content: ( activeFrame, i, N ) => ( formatFloat( activeFrame.descriptor.median, N ) ),
      associatedMetric: WBPPLocalNormalizationReferenceFrameMetric.MEDIAN
   },
   {
      label: "# stars",
      alignment: TextAlign_Right,
      content: ( activeFrame ) => ( "" + activeFrame.descriptor.numberOfStars ),
      associatedMetric: WBPPLocalNormalizationReferenceFrameMetric.STARS
   },
   {
      label: "", // dummy column for layout purposes
      alignment: TextAlign_Right,
      content: () => ( "" )
   } ]

   this.setHeaders = () =>
   {
      for ( let i = 0; i < this.columnsDescriptor.length; i++ )
      {
         this.framesTree.setHeaderText( i, this.columnsDescriptor[ i ].label );
         this.framesTree.setHeaderAlignment( i, this.columnsDescriptor[ i ].alignment );
         if ( this.columnsDescriptor[ i ].associatedMetric != undefined && this.columnsDescriptor[ i ].associatedMetric == engine.localNormalizationBestReferenceSelectionMethod )
            this.framesTree.setHeaderIcon( i, this.scaledResource( ":/icons/sort-down.png" ) )
         else
            this.framesTree.setHeaderIcon( i, null )
      }
   }

   this.update = () =>
   {
      let treeBox = this.framesTree;
      let frames = this.activeFrames;
      let best = this.bestFrames;

      treeBox.clear();
      this.setHeaders();

      // prepare the number of digits for each column
      let digits = [];
      for ( let i = 0; i < this.columnsDescriptor.length; i++ )
         digits[ i ] = this.columnsDescriptor[ i ].nDigits ? this.columnsDescriptor[ i ].nDigits() : 0;

      for ( let i = 0; i < frames.length; i++ )
      {
         let node = new TreeBoxNode();
         node.__activeFrame__ = frames[ i ];
         node.__index__ = i;
         let isIntegrated = node.__activeFrame__.__integrated__;

         // cell contents
         for ( let j = 0; j < this.columnsDescriptor.length; j++ )
         {
            node.setText( j, this.columnsDescriptor[ j ].content( frames[ i ], i, digits[ j ] ) )
            node.setAlignment( j, this.columnsDescriptor[ j ].alignment );
            // tootips
            if ( this.columnsDescriptor[ j ].toolTip )
               node.setToolTip( j, this.columnsDescriptor[ j ].toolTip( frames[ i ] ) );
         }

         // fill the Best column icons

         if ( isIntegrated )
            node.setIcon( 1, this.scaledResource( ":/icons/ok-button.png" ) );
         else if ( node.__activeFrame__.__disabled__ )
            node.setIcon( 1, this.scaledResource( ":/browser/disabled.png" ) );
         else if ( best[ node.__activeFrame__.current ] )
            node.setIcon( 1, this.scaledResource( ":/browser/enabled.png" ) );

         treeBox.add( node );

         if ( this.selectedFrame != undefined )
         {
            if ( frames[ i ].current == this.selectedFrame.current )
               treeBox.currentNode = node;
         }
      }

      this.updateSTFReferenceIcon();

      // initially update the column widths
      if ( this.treeInitialized == undefined )
      {
         this.treeInitialized = true;
         this.makeAllBold();
         for ( let j = 0; j < this.columnsDescriptor.length; j++ )
            treeBox.adjustColumnWidthToContents( j );
         this.makeAllBold( false );
      }

      this.setFonts();
      this.updateMetricColumnWidth();
   }

   this.updateSTFReferenceIcon = () =>
   {
      for ( let i = 0; i < this.framesTree.numberOfChildren; i++ )
      {
         let node = this.framesTree.child( i );
         if ( this.STFReference != undefined && node.__activeFrame__.current == this.STFReference.current )
            node.setIcon( 0, this.scaledResource( ":/toolbar/image-stf-auto.png" ) );
         else
            node.clearIcon( 0 );
      }
      this.framesTree.adjustColumnWidthToContents( 0 );
   }

   this.updateMetricColumnWidth = () =>
   {
      for ( let j = 0; j < this.columnsDescriptor.length; j++ )
         if ( this.columnsDescriptor[ j ].associatedMetric != undefined )
         {
            this.framesTree.adjustColumnWidthToContents( j );
         }
   }

   this.makeAllBold = ( bold ) =>
   {
      for ( let i = 0; i < this.framesTree.numberOfChildren; i++ )
      {
         let node = this.framesTree.child( i );
         for ( let j = 0; j < this.columnsDescriptor.length; j++ )
            this.makeColumnBold( node, j, ( bold != undefined ? bold : true ) );
      }
   }

   this.makeColumnBold = ( node, i, bold ) =>
   {
      let font = node.font( i );
      font.bold = ( bold != undefined ? bold : true );
      node.setFont( i, font );
   }

   this.makeColumnItalic = ( node, i, italic ) =>
   {
      let font = node.font( i );
      font.italic = ( italic != undefined ? italic : true );
      node.setFont( i, font );
   }

   this.setFonts = () =>
   {
      for ( let i = 0; i < this.framesTree.numberOfChildren; i++ )
      {
         let node = this.framesTree.child( i );

         // metric column
         for ( let j = 0; j < this.columnsDescriptor.length; j++ )
            if ( this.columnsDescriptor[ j ].associatedMetric != undefined && this.columnsDescriptor[ j ].associatedMetric == engine.localNormalizationBestReferenceSelectionMethod )
            {
               this.makeColumnBold( node, j )
               node.setTextColor( j, 0x000000FF )
            }

         // integrated frames
         if ( node.__activeFrame__.__integrated__ != undefined )
            for ( let j = 0; j < this.columnsDescriptor.length; j++ )
               this.makeColumnItalic( node, j )

         // best frames
         if ( this.bestFrames[ node.__activeFrame__.current ] )
            for ( let j = 0; j <= 2; j++ )
               this.makeColumnBold( node, j );
      }
   }

   this.setFrames = ( activeFrames, bestFrames ) =>
   {
      if ( activeFrames )
         this.activeFrames = activeFrames;
      if ( bestFrames )
         this.bestFrames = bestFrames.reduce( ( acc, item ) =>
         {
            acc[ item.current ] = true;
            return acc;
         },
         {} );
      this.update();
   }

   this.selectFrame = ( frame ) =>
   {
      this.selectedFrame = frame;
      if ( !this.STFLocked )
         this.STFReference = this.selectedFrame;
      this.update();
   }

   this.lockAutostretchReference = () =>
   {
      this.STFLocked = true;
   }

   this.clearAutostretchReference = () =>
   {
      this.STFLocked = false;
      this.STFReference = this.selectedFrame;
      this.updateSTFReferenceIcon();
   }

   // init
   this.clearAutostretchReference();
   this.setHeaders();
}

WBPPLNFilesTable.prototype = new Control;

/**
 * This control shows the assigned bmp image and allows to scroll and zoom in/out.
 *
 * @param {*} parent
 */
function WBPPLNPreview( parent )
{

   let
   {
      computeSTF
   } = WBPPUtils.shared();

   this.__base__ = Control;
   this.__base__( parent );

   // autostretch option, if true the current autostretch will update accordingly to the setted image
   this.autoStretch = true;
   // the current active frame shown
   this.dummyActiveFrame = {
      current: "",
      descriptor:
      {
         median: 0.1,
         mad: 0.1
      }
   };
   // the current STF to be applied to the set image
   this.currentSTF = [
      [ 0, 0.5, 1, 0, 1 ],
      [ 0, 0.5, 1, 0, 1 ],
      [ 0, 0.5, 1, 0, 1 ],
      [ 0, 0.5, 1, 0, 1 ],
      [ 0, 0.5, 1, 0, 1 ]
   ];
   // map between file paths and windows
   this.windows = {};
   // the BMP cache
   this.cache = {};
   // stf cache
   this.autostretchSTFcache = {};
   // mutiplicative coefficient of the c0 term in STF
   this.c0Coeff = 0;
   // mutiplicative coefficient of the m term in STF
   this.mCoeff = 0;

   //

   this.preview = new PreviewControl( this );
   this.preview.scaledImageProvider = ( image, scale ) =>
   {
      // pick from the cache the current image at the given scale
      let bmp = this.getFromCache( this.dummyActiveFrame.current, this.currentSTF, scale );
      if ( bmp == null )
      {
         bmp = image.scaled( scale );
         this.storeToCache( bmp, this.dummyActiveFrame.current, this.currentSTF, scale );
      }
      return bmp;
   };
   //

   this.sizer = new HorizontalSizer;
   this.sizer.add( this.preview );

   // Preview extra controls

   this.lockSFTCheckbox = new CheckBox( this.preview );
   this.lockSFTCheckbox.text = "Lock stretch";
   this.lockSFTCheckbox.checked = true
   this.lockSFTCheckbox.adjustToContents();
   this.lockSFTCheckbox.setFixedWidth( this.lockSFTCheckbox.width )
   this.lockSFTCheckbox.onCheck = ( checked ) =>
   {
      this.autoStretch = !checked;
      this.set( this.dummyActiveFrame );
      if ( this.onLockStretchChange )
         this.onLockStretchChange( checked );
   };
   this.preview.appendTopControl( this.lockSFTCheckbox );

   // autostretch adjustments

   let adjustmentsParent = new Control( this.preview );
   adjustmentsParent.sizer = new HorizontalSizer;
   adjustmentsParent.sizer.spacing = 4;


   this.c0StretchCoeffNumericControl = new NumericControl( adjustmentsParent );
   this.c0StretchCoeffNumericControl.label.text = "Shadows";
   this.c0StretchCoeffNumericControl.setRange( -1, 1 );
   this.c0StretchCoeffNumericControl.slider.setRange( 0, 100000 );
   this.c0StretchCoeffNumericControl.slider.setFixedWidth( 280 );
   this.c0StretchCoeffNumericControl.edit.visible = false;
   this.c0StretchCoeffNumericControl.setPrecision( 5 );
   this.c0StretchCoeffNumericControl.setValue( 0 );
   this.c0StretchCoeffNumericControl.slider.onMousePress = () =>
   {
      this.c0StretchCoeffNumericControl.mouseDownValue = this.c0StretchCoeffNumericControl.value;
   }
   this.c0StretchCoeffNumericControl.slider.onMouseRelease = () =>
   {
      if ( this.c0StretchCoeffNumericControl.mouseDownValue != this.c0StretchCoeffNumericControl.value )
      {
         this.c0Coeff = Math.pow( this.c0StretchCoeffNumericControl.value, 5 );
         this.set( this.dummyActiveFrame );
      }
   }

   this.mStretchCoeffNumericControl = new NumericControl( adjustmentsParent );
   this.mStretchCoeffNumericControl.label.text = "Midtones";
   this.mStretchCoeffNumericControl.setRange( -1, 1 );
   this.mStretchCoeffNumericControl.slider.setRange( 0, 100000 );
   this.mStretchCoeffNumericControl.slider.setFixedWidth( 280 );
   this.mStretchCoeffNumericControl.edit.visible = false;
   this.mStretchCoeffNumericControl.setPrecision( 5 );
   this.mStretchCoeffNumericControl.setValue( 0 );
   this.mStretchCoeffNumericControl.slider.onMousePress = () =>
   {
      this.mStretchCoeffNumericControl.mouseDownValue = this.mStretchCoeffNumericControl.value;
   }
   this.mStretchCoeffNumericControl.slider.onMouseRelease = () =>
   {
      if ( this.mStretchCoeffNumericControl.mouseDownValue != this.mStretchCoeffNumericControl.value )
      {
         this.mCoeff = Math.pow( this.mStretchCoeffNumericControl.value, 5 );
         this.set( this.dummyActiveFrame );
      }
   }

   this.c0Reset = new PushButton( this.preview );
   this.c0Reset.text = "Reset"
   this.c0Reset.toolTip = "<p>Resets the shadows adjustment.</p>"
   this.c0Reset.onClick = () =>
   {
      this.c0StretchCoeffNumericControl.setValue( 0 );
      this.c0Coeff = 0;
      this.set( this.dummyActiveFrame );
   }

   this.mReset = new PushButton( this.preview );
   this.mReset.text = "Reset"
   this.mReset.toolTip = "<p>Resets the midtones adjustment.</p>"
   this.mReset.onClick = () =>
   {
      this.mStretchCoeffNumericControl.setValue( 0 );
      this.mCoeff = 0;
      this.set( this.dummyActiveFrame );
   }

   this.autostretchOngoingLabel = new Label( adjustmentsParent );
   this.autostretchOngoingLabel.useRichText = true;
   this.autostretchOngoingLabel.textAlignment = TextAlign_VertCenter | TextAlign_Left;

   this.autostretchOngoingIcon = new ToolButton( adjustmentsParent );
   this.autostretchOngoingIcon.setScaledFixedSize( 20, 20 );
   this.autostretchOngoingIcon.icon = this.scaledResource( ":/icons/clock.png" );

   let autostretchOngoingSizer = new HorizontalSizer;
   autostretchOngoingSizer.spacing = 4;
   autostretchOngoingSizer.add( this.autostretchOngoingIcon );
   autostretchOngoingSizer.add( this.autostretchOngoingLabel );

   let resetButtonsSizer = new VerticalSizer;
   resetButtonsSizer.spacing = 4;
   resetButtonsSizer.add( this.c0Reset );
   resetButtonsSizer.add( this.mReset );

   let adjustmentsSizer = new VerticalSizer;
   adjustmentsSizer.spacing = 4;
   adjustmentsSizer.add( this.c0StretchCoeffNumericControl );
   adjustmentsSizer.add( this.mStretchCoeffNumericControl );

   adjustmentsParent.sizer.add( adjustmentsSizer );
   adjustmentsParent.sizer.add( resetButtonsSizer );
   adjustmentsParent.sizer.addSpacing( 8 );
   adjustmentsParent.sizer.add( autostretchOngoingSizer );
   adjustmentsParent.sizer.addStretch();

   this.preview.appendBottomControl( adjustmentsParent );

   //

   // Preview extra controls DONE

   //

   this.setStatusActiveFrameInfo = ( activeFrame ) =>
   {
      this.setStatusMessage( "<b>" + File.extractNameAndExtension( activeFrame.current ) + "</b>" );
   };

   this.setStatusMessage = ( message ) =>
   {
      this.preview.setStatusMessage( message );
   };

   this.set = ( activeFrame ) =>
   {
      this.dummyActiveFrame.current = activeFrame.current;
      this.dummyActiveFrame.descriptor = activeFrame.descriptor;
      // we first get the mapped window (load if needed)
      let window = this.getWindow( this.dummyActiveFrame.current );
      if ( window == null ) return;
      this.autostretchOngoingLabel.text = "<b>Autostretching...</b>"
      this.autostretchOngoingIcon.visible = true;
      // the STF parameters need to be updated to the current view if autostretch is active
      let cacheKey = this.dummyActiveFrame.current + "_" + this.c0Coeff + "_" + this.mCoeff;
      if ( this.autoStretch )
      {
         if ( this.autostretchSTFcache[ cacheKey ] != undefined )
            this.currentSTF = this.autostretchSTFcache[ cacheKey ];
         else
         {
            let c0Add = -this.c0Coeff;
            let mAdd = -this.mCoeff;
            this.currentSTF = computeSTF( window.mainView, this.dummyActiveFrame.descriptor, false /* forSTF */ , c0Add, mAdd );
            this.autostretchSTFcache[ cacheKey ] = this.currentSTF;
         }
      }

      // get from the cache the BMP corresponding to the current image, STF and scale = 1 (original scale)
      let cachedBMP = this.getFromCache( cacheKey, this.currentSTF, 1 );
      if ( cachedBMP != null )
      {
         this.preview.SetImage( cachedBMP );
         this.autostretchOngoingLabel.text = "";
         this.autostretchOngoingIcon.visible = false;
         return;
      }

      // not in cached, generate the STF image bmp
      if ( this.backupImage == undefined )
         this.backupImage = new Image(
            window.mainView.image.width,
            window.mainView.image.height,
            window.mainView.image.numberOfChannels,
            window.mainView.image.colorSpace,
            window.mainView.image.bitsPerSample,
            window.mainView.image.sampleType
         );

      this.backupImage.apply( window.mainView.image );
      var HT = new HistogramTransformation;
      HT.H = this.currentSTF;
      HT.executeOn( window.mainView, false ); // no swap file
      let bmp = new Bitmap( window.mainView.image.width, window.mainView.image.height );
      bmp.assign( window.mainView.image.render() );
      window.mainView.beginProcess();
      window.mainView.image.apply( this.backupImage );
      window.mainView.endProcess();

      // cache the BMP with current STF and scale = 1
      this.storeToCache( bmp, cacheKey, this.currentSTF, 1 );

      // update the preview
      this.preview.SetImage( bmp );

      this.autostretchOngoingLabel.text = "";
      this.autostretchOngoingIcon.visible = false;
   };

   this.update = () =>
   {
      this.lockSFTCheckbox.checked = !this.autoStretch
   };

   this.update();

   //------------------------------
   // IMAGE HANDLING

   this.getCacheID = ( filePath, stf, scale ) => ( filePath + "_" + stf + "_" + scale );

   this.getFromCache = ( filePath, stf, scale ) =>
   {
      let id = this.getCacheID( filePath, stf, scale );
      if ( this.cache[ id ] != undefined )
         return this.cache[ id ];
      else
         return null;
   };

   this.storeToCache = ( bmp, filePath, stf, scale ) =>
   {
      this.cache[ this.getCacheID( filePath, stf, scale ) ] = bmp;
   };

   this.getWindow = ( filePath ) =>
   {
      // return the window if already cached
      if ( this.windows[ filePath ] ) return this.windows[ filePath ];

      // we are interested only in the first window in case the file contain multiuple windows
      let windows = ImageWindow.open( filePath );
      if ( !windows || windows.length == 0 )
         return null;
      for ( let i = 1; i < windows.length; i++ )
         windows[ i ].forceClose();
      this.windows[ filePath ] = windows[ 0 ];
      return windows[ 0 ];
   };

   this.clear = () =>
   {
      this.cache = {};
      let keys = Object.keys( this.windows );
      for ( let i = 0; i < keys.length; i++ )
         this.windows[ keys[ i ] ].forceClose();
   };
}

WBPPLNPreview.prototype = new Control;

/**
 * This window handles a set of frames to
 *
 * @param {*} frames
 */
function WBPPLocalNormalizationReferenceSelector( frameGroup )
{
   this.__base__ = Dialog;
   this.__base__();
   this.windowTitle = "Local Normalization Selector";

   let spacing = 8;

   // Window size and position
   this.initialSizeAndPos = () =>
   {
      this.width = this.availableScreenRect.width * 0.9;
      this.height = this.availableScreenRect.height * 0.9;
      let origin = new Point(
         ( this.availableScreenRect.width - this.width ) / 2,
         ( this.availableScreenRect.height - this.height ) / 2 );
      this.move( origin );
   };
   this.initialSizeAndPos();

   // local normalization parameters

   // NOTE: we set this control to work in interactive mode, this way the option "Interactive"
   // is not be listed and only single best or integrate best frames are selectable.
   // We assign the default value to the reference frame generation method.
   this.localNormalizationParametersControl = new LocalNormalizationControl( this, false, true /* forInteractiveMode */ );

   // backup the Local Normalization parameters and
   this.localNormalizationParametersControl.updateControls();
   this.localNormalizationParametersControl.onReferenceFrameGenerationChange = () =>
   {
      // updates the best frames keeping the excluded frames
      let currentFrame = this.framesTable.selectedFrame;
      this.resetBestFrames( true /* resetN */ );
      this.update();
      this.selectFrame( currentFrame );
   };
   this.localNormalizationParametersControl.onEvaluationCriteriaChanged = () =>
   {
      // updates the best frames keeping the excluded frames
      let currentFrame = this.framesTable.selectedFrame;
      this.resetBestFrames();
      this.update();
      this.selectFrame( currentFrame );
   };
   this.localNormalizationParametersControl.adjustToContents();

   // Main Label
   this.mainTitleLabel = new Label( this );
   this.mainTitleLabel.useRichText = true;
   this.mainTitleLabel.textAlignment = TextAlign_Center;
   this.mainTitleLabel.styleSheet = this.scaledStyleSheet(
      "QWidget#" + this.mainTitleLabel.uniqueId + " {" +
      "font-size: 11pt;" +
      "border-style: solid;" +
      "border-color: #aaaaaa;" +
      "border-width: 1pt;" +
      "padding: 0.5em;" +
      "}" );

   // FILE LIST CONTROL

   // file tree
   this.framesTable = new WBPPLNFilesTable( this );
   this.framesTable.onFrameSelected = ( activeFrame ) =>
   {
      this.preview.set( activeFrame );
      this.preview.setStatusActiveFrameInfo( activeFrame );
      this.updateWithSelectedFrame();
   };

   // CENTRAL CONTROLS

   let separator = () =>
   {
      let control = new Control( this );
      control.styleSheet = this.scaledStyleSheet(
         "QWidget#" + control.uniqueId + " {" +
         "border-style: solid;" +
         "border-color: #aaaaaa;" +
         "border-width: 1pt;" +
         "}" );
      control.setFixedHeight( 1 );
      control.setVariableWidth();
      return control;
   };

   this.resetButton = new PushButton( this );
   this.resetButton.text = " Reset";
   this.resetButton.toolTip = "<p>Resets the frames table and sets the number of best frames to be integrated " +
      "to the suggested value.</p>"
   this.resetButton.icon = this.scaledResource( ":/icons/reload.png" );
   this.resetButton.onClick = () =>
   {
      // Re-enable all frames, reload data, select the first frame
      try
      {
         this.enableAllFrames();
         this.resetBestFrames( true /* resetN */ , false /* remove integrated */ )
         this.update();
         this.selectFirstFrame();
      }
      catch ( e )
      {
         console.noteln( e )
      }
   };

   //

   this.excludeFrameButton = new PushButton( this );
   this.excludeFrameButton.text = " Exclude";
   this.excludeFrameButton.toolTip = "<p>Exclude or re-include the selected frame.</p>" +
      "<p>By excluding a frame, we ensure that it is never selected as either the single best reference frame or " +
      "as part of the frames that will be integrated to generate the normalization reference image.</p>"
   this.excludeFrameButton.icon = this.scaledResource( ":/file-explorer/delete.png" );
   this.excludeFrameButton.onClick = () =>
   {
      // enable/disable the current frame, recompute the best frames
      this.switchFrameActivationStatus( this.framesTable.selectedFrame );
      this.updateBestFrames();
      this.update();
   };

   //

   let numberOfFramesToolTip = "<p>The number of frames to integrate.</p>" +
      "<p>This control is relevant only when the reference frame is generated by integrating the best frames.</p>";
   this.numberOfFramesLabel = new Label( this );
   this.numberOfFramesLabel.text = "Frames:"
   this.numberOfFramesLabel.toolTip = numberOfFramesToolTip;
   this.numberOfFramesLabel.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.numberOfFramesSpinBox = new SpinBox( this );
   this.numberOfFramesSpinBox.minValue = 3;
   this.numberOfFramesSpinBox.maxValue = 3;
   this.numberOfFramesSpinBox.toolTip = numberOfFramesToolTip;
   this.numberOfFramesSpinBox.onValueUpdated = ( value ) =>
   {
      this.N = value;
      this.updateBestFrames();
      this.update();
   };
   this.numberOfFramesSpinBox.update = () =>
   {
      this.numberOfFramesSpinBox.enabled = this.N >= 3 && engine.localNormalizationRerenceFrameGenerationMethod == WBPPLocalNormalizationReferenceFrameMethod.INTEGRATION_BEST_FRAMES;
      this.numberOfFramesSpinBox.maxValue = this.activeFrames.length;
      if ( !this.numberOfFramesSpinBox.enabled )
      {
         this.numberOfFramesSpinBox.minValue = 1;
         this.numberOfFramesSpinBox.value = 1;
      }
      else
      {
         this.numberOfFramesSpinBox.minValue = 3;
         this.numberOfFramesSpinBox.value = this.N;
      }
   };

   // blink animation

   this.backButton = new ToolButton( this );
   this.backButton.icon = this.scaledResource( ":/icons/step-backward.png" );
   this.backButton.setScaledFixedSize( 24, 24 );
   this.backButton.toolTip = "<p>Previous frame</p>"
   this.backButton.onMousePress = () =>
   {
      this.selectNextBestFrame( false /* forward */ );
   };

   this.playButton = new ToolButton( this );
   this.playButton.icon = this.scaledResource( ":/icons/play.png" );
   this.playButton.setScaledFixedSize( 24, 24 );
   this.playButton.toolTip = "<p>Play</p>"
   this.playButton.onMousePress = () =>
   {
      if ( this.blinkAnimationTimer == undefined )
      {
         this.startBlinkingAnimation( 0.5 );
         this.playButton.icon = this.scaledResource( ":/icons/stop.png" );
         this.playFastButton.enabled = false;
      }
      else
      {
         this.stopBlinkingAnimation()
         this.playButton.icon = this.scaledResource( ":/icons/play.png" );
         this.playFastButton.enabled = true;
      }
      this.updateWithTimer();
   };

   this.playFastButton = new ToolButton( this );
   this.playFastButton.icon = this.scaledResource( ":/icons/forward.png" );
   this.playFastButton.setScaledFixedSize( 24, 24 );
   this.playFastButton.toolTip = "<p>Play fast</p>"
   this.playFastButton.onMousePress = () =>
   {
      if ( this.blinkAnimationTimer == undefined )
      {
         this.startBlinkingAnimation( 0.05 );
         this.playFastButton.icon = this.scaledResource( ":/icons/stop.png" );
         this.playButton.enabled = false;
      }
      else
      {
         this.stopBlinkingAnimation()
         this.playFastButton.icon = this.scaledResource( ":/icons/forward.png" );
         this.playButton.enabled = true;
      }
      this.updateWithTimer();
   };

   this.forwardButton = new ToolButton( this );
   this.forwardButton.icon = this.scaledResource( ":/icons/step-forward.png" );
   this.forwardButton.setScaledFixedSize( 24, 24 );
   this.forwardButton.toolTip = "<p>Next frame</p>"
   this.forwardButton.onMousePress = () =>
   {
      this.selectNextBestFrame( true /* forward */ );
   };

   this.blinkBestCheckBox = new CheckBox( this );
   this.blinkBestCheckBox.text = "Best frames";
   this.blinkBestCheckBox.toolTip = "<p>If checked, the blink buttons loop through the set of best frames only.</p>"
   this.blinkBestCheckBox.textAlignment = TextAlign_Center;
   this.blinkBestCheckBox.checked = true;
   this.blinkBestCheckBox.onCheck = ( checked ) =>
   {
      this.bestFramesOnly = checked;
   };
   this.bestFramesOnly = true;

   let blinkButtonsSizer = new HorizontalSizer;
   blinkButtonsSizer.add( this.backButton )
   blinkButtonsSizer.add( this.playButton )
   blinkButtonsSizer.add( this.playFastButton )
   blinkButtonsSizer.add( this.forwardButton )

   let blinkSizer = new VerticalSizer;
   blinkSizer.add( blinkButtonsSizer );
   blinkSizer.addSpacing( 4 );
   blinkSizer.add( this.blinkBestCheckBox );

   //

   this.generateButton = new PushButton( this );
   this.generateButton.text = "Generate";
   this.generateButton.toolTip = "<p>Runs the integration of the current best frames to generate a local normalization " +
      "reference image candidate.</p>" +
      "<p>The integration is performed applying the additive+scaling global output normalization.</p>" +
      "<p>For pixel rejection, local normalization is applied to the subset of best frames with the single best frame as " +
      "normalization reference.</p>"
   this.generateButton.onClick = () =>
   {
      this.generateReferenceFrame();
   };

   //

   this.proceedButton = new PushButton( this );
   this.proceedButton.text = "Select";
   this.proceedButton.toolTip = "<p>Select the current active frame as the reference and continue.</p>";
   this.proceedButton.onClick = () =>
   {
      // exit and select the current frame as reference
      this.referenceFrame = this.framesTable.selectedFrame.current;
      this.ok();
   };

   // preview control

   this.preview = new WBPPLNPreview( this );
   this.preview.onLockStretchChange = ( checked ) =>
   {
      if ( checked )
         this.framesTable.lockAutostretchReference();
      else
         this.framesTable.clearAutostretchReference();
      this.preview.c0StretchCoeffNumericControl.enabled = !checked;
      this.preview.mStretchCoeffNumericControl.enabled = !checked;
      this.preview.c0Reset.enabled = !checked;
      this.preview.mReset.enabled = !checked;
   };
   //
   let numberOfFramesSizer = new HorizontalSizer;
   numberOfFramesSizer.spacing = spacing;
   numberOfFramesSizer.add( this.numberOfFramesLabel );
   numberOfFramesSizer.add( this.numberOfFramesSpinBox );

   let buttonsSizer = new VerticalSizer;
   buttonsSizer.spacing = spacing;
   buttonsSizer.add( this.resetButton );
   buttonsSizer.add( separator() );
   buttonsSizer.add( numberOfFramesSizer );
   buttonsSizer.add( this.excludeFrameButton );
   buttonsSizer.add( separator() );
   buttonsSizer.add( blinkSizer );
   buttonsSizer.add( separator() );
   buttonsSizer.add( this.generateButton );
   buttonsSizer.add( this.proceedButton );
   buttonsSizer.addStretch();

   let tableAndControlsSizer = new HorizontalSizer;
   tableAndControlsSizer.spacing = spacing;
   tableAndControlsSizer.add( this.framesTable )
   tableAndControlsSizer.add( buttonsSizer )

   let leftSizer = new VerticalSizer;
   leftSizer.spacing = spacing;
   leftSizer.addSpacing( 8 );
   leftSizer.add( this.localNormalizationParametersControl );
   leftSizer.add( this.mainTitleLabel );
   leftSizer.add( tableAndControlsSizer );

   //

   this.adjustMaxSizes = () =>
   {
      // the left column width is given by the current LN control width
      let leftPanelMaxWidth = this.width / 3;

      this.localNormalizationParametersControl.setMaxWidth( leftPanelMaxWidth );
      this.mainTitleLabel.setMaxWidth( leftPanelMaxWidth );

      this.proceedButton.adjustToContents();
      let buttonsMaxWidth = this.proceedButton.width;

      this.resetButton.setMaxWidth( buttonsMaxWidth );
      this.excludeFrameButton.setMaxWidth( buttonsMaxWidth );
      this.numberOfFramesSpinBox.setMaxWidth( buttonsMaxWidth );
      this.generateButton.setMaxWidth( buttonsMaxWidth );
      this.proceedButton.setMaxWidth( buttonsMaxWidth );

      this.framesTable.setMaxWidth( leftPanelMaxWidth - buttonsMaxWidth - spacing );
   };

   //

   this.sizer = new HorizontalSizer;
   this.sizer.margin = spacing;
   this.sizer.spacing = spacing;
   this.sizer.add( leftSizer );
   this.sizer.add( this.preview );

   this.adjustMaxSizes();

   // helpers and GUI handlers

   //

   this.onReturn = ( retVal ) =>
   {
      // clean up
      this.preview.clear();
      this.removeIntegratedImagesFromDisk( this.referenceFrame || "" /* except the best reference frame */ );
      this.saveCache();
   };

   /**
    * Update the entire window and children controls
    *
    */
   this.update = () =>
   {
      let lbl = engine.readableLNReferenceSelectionMethod();

      if ( engine.localNormalizationRerenceFrameGenerationMethod == WBPPLocalNormalizationReferenceFrameMethod.SINGLE_BEST )
         this.mainTitleLabel.text = "Select the singe reference frame with <b>" + lbl + "</b>";
      else
         this.mainTitleLabel.text = "Integrate the frames with <b>" + lbl + "</b>";

      this.preview.update();
      this.framesTable.setFrames( this.activeFrames, this.bestFrames );
      this.updateWithSelectedFrame();
   };

   /**
    * Handles the status of the  controls that depend on the selected frame
    *
    */
   this.updateWithSelectedFrame = () =>
   {
      let itemSelected = this.framesTable.selectedFrame != undefined
      // disable switch not enabled for integrated frames (only if not blinking)
      if ( this.blinkAnimationTimer == undefined )
      {
         this.excludeFrameButton.enabled = itemSelected && this.framesTable.selectedFrame.__integrated__ == undefined;
         // proceed is enabled only if we have to select a single frame or if we have selected an integrated one
         this.proceedButton.enabled = true;

         let blinkEnabled = engine.localNormalizationRerenceFrameGenerationMethod == WBPPLocalNormalizationReferenceFrameMethod.INTEGRATION_BEST_FRAMES;
         this.playButton.enabled = blinkEnabled
         this.playFastButton.enabled = blinkEnabled
         this.blinkBestCheckBox.enabled = blinkEnabled
      }
      else
      {
         this.proceedButton.enabled = false;
      }

      if ( itemSelected && this.framesTable.selectedFrame.__disabled__ )
      {
         this.excludeFrameButton.icon = this.scaledResource( ":/browser/enabled.png" );
         this.excludeFrameButton.text = " Include";
      }
      else
      {
         this.excludeFrameButton.icon = this.scaledResource( ":/file-explorer/delete.png" );
         this.excludeFrameButton.text = " Exclude";
      }
   };

   this.updateWithTimer = () =>
   {
      let enabled = this.blinkAnimationTimer == undefined;

      this.localNormalizationParametersControl.enabled = enabled;
      this.preview.lockSFTCheckbox.enabled = enabled;
      this.resetButton.enabled = enabled;
      this.excludeFrameButton.enabled = enabled;
      this.backButton.enabled = enabled;
      this.generateButton.enabled = enabled;

      this.updateWithSelectedFrame();
   };

   /**
    * Recalculate the best frames list depending on the Local Normalization control parameters.
    *
    * @param {*} resetN true if the current number of best frames has to be reset
    * @param {*} removeIntegrated true if the integrated frames must be removed
    */
   this.resetBestFrames = ( resetN, removeIntegrated ) =>
   {
      if ( removeIntegrated != undefined && removeIntegrated )
      {
         // delete the integrated images from disk and filter out them from the list
         this.removeIntegratedImagesFromDisk();
         this.activeFrames = this.activeFrames.filter( f => ( f.__integrated__ == undefined ) );
      }

      // first store the filepaths of the integrated frames
      let integrated = this.activeFrames.reduce( ( acc, f ) =>
      {
         if ( f.__integrated__ )
            acc[ f.current ] = true;
         return acc;
      },
      {} );
      // we compute the selection
      let
      {
         N,
         activeFrames
      } = engine.sortFramesForLocalNormalizationReference( frameGroup.cloneWithActiveItems( this.activeFrames ) );
      if ( resetN )
         this.N = N;
      // mark the integrated frames
      this.activeFrames = activeFrames.map( f =>
      {
         if ( integrated[ f.current ] )
            f.__integrated__ = true;
         return f;
      } )
      this.updateBestFrames();
      this.numberOfFramesSpinBox.update();
   };

   /**
    * Regenerate the list of best frames accordingly to the current frames status and the
    * current number of frames to be marked as best
    */
   this.updateBestFrames = () =>
   {
      // select the best frames excluding the integrated reference frames (only in integration mode)
      this.bestFrames = new Array;
      for ( let i = 0; i < this.activeFrames.length; i++ )
         if ( this.activeFrames[ i ].__integrated__ == undefined )
         {
            let disabled = this.disabledFrames[ this.activeFrames[ i ].current ];
            this.activeFrames[ i ].__disabled__ = disabled;
            if ( !disabled && this.bestFrames.length < this.N )
            {
               this.activeFrames[ i ].__best__ = true;
               this.bestFrames.push( this.activeFrames[ i ] );
            }
         }

      // once updated we disable the Generate button if less than 3 items are available
      this.generateButton.enabled = this.bestFrames.length >= 3;
   };

   /**
    * Programmatically selects the first frame in the list
    *
    */
   this.selectFirstFrame = () =>
   {
      this.selectFrame( this.activeFrames[ 0 ] );
   };

   /**
    * Programmatically select the given frame
    *
    * @param {*} frame
    */
   this.selectFrame = ( frame ) =>
   {
      this.preview.set( frame );
      this.framesTable.selectFrame( frame );
      this.preview.setStatusActiveFrameInfo( frame );
      this.updateWithSelectedFrame();
   };

   /**
    * MArks all frames as enabled
    *
    */
   this.enableAllFrames = () =>
   {
      this.disabledFrames = this.activeFrames.reduce( ( acc, item ) =>
      {
         acc[ item.current ] = false;
         return acc;
      },
      {} );
   };

   /**
    * Mark/unmark a frame as disabled
    *
    * @param {*} frame
    */
   this.switchFrameActivationStatus = ( frame ) =>
   {
      this.disabledFrames[ frame.current ] = !this.disabledFrames[ frame.current ];
   };

   /**
    * Generates the integrated reference frame with the current list of best frames.
    *
    */
   this.generateReferenceFrame = () =>
   {
      this.enabled = false;
      try
      {
         this.preview.setStatusMessage( "<b>GENERATING THE REFERENCE FRAME...</b>" )
         let fname = frameGroup.folderName( false /* sanitized */ ) + "_" + this.bestFrames.length + "_of_" + engine.readableLNReferenceSelectionMethod( true /* sanitized */ );
         let
         {
            lnReferenceFilePath,
            cached
         } = engine.generateLNReference(
            frameGroup,
            this.bestFrames /* use the current best frames for integration */ ,
            false, /* do not log into the process logger */
            fname /* desired file name */
         );
         let cachedSelected = false;
         if ( cached )
         {
            // select the cached reference frame
            for ( let i = 0; i < this.activeFrames.length; i++ )
               if ( this.activeFrames[ i ].current == lnReferenceFilePath )
               {
                  this.selectFrame( this.activeFrames[ i ] );
                  cachedSelected = true;
                  break;
               }
         }
         if ( !cachedSelected )
         {
            this.preview.setStatusMessage( "<b>MEASURING THE REFERENCE FRAME...</b>" );
            let activeFrame = ActiveFrame.dummy( lnReferenceFilePath, this.activeFrames[ 0 ] /* clone data from the first active frame */ );
            activeFrame.__integrated__ = true;
            engine.computeDescriptors( [ activeFrame ] );
            this.activeFrames.unshift( activeFrame );
            this.update();
            this.selectFrame( activeFrame );
         }
      }
      catch ( error )
      {
         console.warningln( "*** error occurred: ", error );
      }
      this.enabled = true;
   };

   /**
    * Programmatically move to the next best frame starting from the selected frame in the frames table
    *
    * @return {*}
    */
   this.selectNextBestFrame = ( forward ) =>
   {
      if ( this.selectingBestFrame )
         return;

      this.selectingBestFrame = true;

      if ( this.framesTable.selectedFrame == undefined )
      {
         this.selectFirstFrame();
         return;
      }
      let bestFramesMap = this.bestFrames.reduce( ( acc, item ) =>
      {
         acc[ item.current ] = true;
         return acc;
      },
      {} );

      let currentFound = false;
      let i = 0;
      let count = 0;
      // include a double check on the total amount of iterations in case of issues to avoid
      // an infinite loop
      while ( count < this.activeFrames.length * 2 )
      {
         if ( !currentFound )
         {
            if ( this.activeFrames[ i ].current == this.framesTable.selectedFrame.current )
               currentFound = true;
         }
         else
         {
            let enabled = !this.disabledFrames[ this.activeFrames[ i ].current ];
            if ( enabled )
               if (
                  engine.localNormalizationRerenceFrameGenerationMethod == WBPPLocalNormalizationReferenceFrameMethod.SINGLE_BEST ||
                  !this.bestFramesOnly ||
                  ( this.bestFramesOnly && bestFramesMap[ this.activeFrames[ i ].current ] ) )
               {
                  this.selectFrame( this.activeFrames[ i ] )
                  this.selectingBestFrame = false;
                  return;
               }
         }

         i = ( ( i + ( forward ? 1 : this.activeFrames.length - 1 ) ) % this.activeFrames.length );
         count++;
      }

      this.selectingBestFrame = false;
   };

   // ------------------
   // animation handling
   // ------------------

   /**
    * Starts the blink animation with the given period.
    *
    * @param {*} period the time elapsed between the next image
    */
   this.startBlinkingAnimation = ( period ) =>
   {
      this.blinkAnimationTimer = new Timer
      this.blinkAnimationTimer.interval = period;
      this.blinkAnimationTimer.periodic = true;
      this.blinkAnimationTimer.dialog = this;
      this.blinkAnimationTimer.onTimeout = () =>
      {
         this.selectNextBestFrame( true /* forward */ );
         // refresh the progress report
         processEvents();
         gc();
      };
      this.blinkAnimationTimer.start();
   };

   /**
    * Stops the blink animation.
    *
    */
   this.stopBlinkingAnimation = () =>
   {
      if ( this.blinkAnimationTimer != undefined )
      {
         this.blinkAnimationTimer.stop();
         this.blinkAnimationTimer = undefined;
      }
   };

   // --------------------
   // best frames handling
   // --------------------

   /**
    * Deleted the integrated images from disk, optionally avoid the deletion of the file provided.
    *
    * @param {*} exceptFramePath the path of the file to keep
    */
   this.removeIntegratedImagesFromDisk = ( exceptFramePath ) =>
   {
      if ( exceptFramePath == undefined )
         exceptFramePath = "";
      // clean up
      for ( let i = 0; i < this.activeFrames.length; i++ )
         if ( this.activeFrames[ i ].__integrated__ != undefined &&
            this.activeFrames[ i ].__cached__ != true &&
            this.activeFrames[ i ].current != exceptFramePath )
            // delete the file
            this.removeImageFromDisk( this.activeFrames[ i ].current );
   };

   this.removeImageFromDisk = ( filePath ) =>
   {
      // delete the file
      File.remove( filePath )
   };

   this.cacheKey = () =>
   {
      return "LNInteractiveGUI_" + engine.outputDirectory + " - " + frameGroup.folderName();
   }

   this.hasCache = () =>
   {
      return engine.executionCache.hasCacheForKey( this.cacheKey() );
   }

   this.restoreFromCache = () =>
   {
      if ( !this.hasCache() )
         return;

      // add the integrated frames
      let cacheData = engine.executionCache.cacheForKey( this.cacheKey() );

      // select the ative selected item
      let itemsToAdd = [];
      if ( cacheData.integratedFrames )
      {
         for ( let i = cacheData.integratedFrames.length - 1; i >= 0; i-- )
            if ( File.exists( cacheData.integratedFrames[ i ].current ) )
            {
               this.activeFrames.unshift( cacheData.integratedFrames[ i ] );
               this.activeFrames[ 0 ].__cached__ = true;
            }
      }

      this.update();

      let selected = false;
      if ( cacheData.selectedFrame )
      {
         for ( let i = 0; i < this.activeFrames.length; i++ )
            if ( cacheData.selectedFrame.current == this.activeFrames[ i ].current )
            {
               this.selectFrame( this.activeFrames[ i ] );
               selected = true;
               break;
            }
      }

      if ( !selected )
         this.selectFirstFrame();

   }

   this.saveCache = () =>
   {

      let cacheKey = "LNInteractiveGUI_" + engine.outputDirectory + " - " + frameGroup.folderName();
      let cacheData = {};
      // save the list of generated farmes
      cacheData.integratedFrames = [];
      for ( let i = 0; i < this.activeFrames.length; i++ )
         if ( this.activeFrames[ i ].__integrated__ && File.exists( this.activeFrames[ i ].current ) )
            cacheData.integratedFrames.push( this.activeFrames[ i ] );

      // save the selected frame
      cacheData.selectedFrame = this.framesTable.selectedFrame;

      engine.executionCache.setCache( cacheKey, cacheData );
   }

   /**
    * GUI data initialization
    *
    */
   this.initialize = () =>
   {
      let
      {
         N,
         activeFrames
      } = engine.sortFramesForLocalNormalizationReference( frameGroup );
      this.N = N;
      this.activeFrames = activeFrames.slice();
      this.numberOfFramesSpinBox.update();
      this.enableAllFrames();
      this.updateBestFrames();
      if ( this.hasCache() )
      {
         this.restoreFromCache();
      }
      else
      {
         this.update();
         this.selectFirstFrame();
      }
   };

   this.initialize();
}

WBPPLocalNormalizationReferenceSelector.prototype = new Dialog;

// ----------------------------------------------------------------------------
// EOF WeightedBatchPreprocessing-LNReferenceSelector.js - Released 2023-06-15T09:00:12Z

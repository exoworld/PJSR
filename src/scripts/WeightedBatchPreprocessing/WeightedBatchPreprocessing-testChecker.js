// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// WeightedBatchPreprocessing-testChecker.js - Released 2023-06-15T09:00:12Z
// ----------------------------------------------------------------------------
//
// This file is part of Weighted Batch Preprocessing Script version 2.5.10
//
// Copyright (c) 2019-2023 Roberto Sartori
// Copyright (c) 2020-2021 Adam Block
// Copyright (c) 2019 Tommaso Rubechi
// Copyright (c) 2012 Kai Wiechen
// Copyright (c) 2012-2023 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

let trackableOperations = engine.operationQueue.operations.filter( o => o && o.operation && !!o.operation.triggersEventScript );
let trackableOperationsCount = trackableOperations.length;
let trackableIndex = 0;
let terminate = false;

if ( env.event == "start" || env.event == "end" )
{
   // find the trackable operationindex
   for ( let i = 0; i < trackableOperationsCount; i++ )
      if ( trackableOperations[ i ].operation.__index__ == env.operationIndex )
      {
         trackableIndex = i;
         break;
      }
}

// -------------------------
// ---  PIPELINE STARTED
// -------------------------

if ( env.event == "pipeline start" && !engine.recordTest )
{
   if ( engine.executionStatus == undefined )
   {
      cout( "\nEngine does not contain the test execution status object" );
      cout( "\nTEST FAILED" );
      // interrupt the test execution
      engine.operationQueue.requestInterruption();
      engine.operationQueue.__interruptedFromTestCheckedScript__ = true;
      terminate = true;
   }

   if ( engine.executionStatus.ops == undefined )
   {
      cout( "\nEngine does not contain the list of operations array, wrong object structure" );
      cout( "\nTEST FAILED" );
      // interrupt the test execution
      engine.operationQueue.requestInterruption();
      engine.operationQueue.__interruptedFromTestCheckedScript__ = true;
      terminate = true;
   }

   //the number of trackable operations must be the same
   let expectedTrackableOperationsCount = engine.executionStatus && engine.executionStatus.ops && engine.executionStatus.ops.filter( o => !!o.triggersEventScript ) || 0;
   if ( trackableOperationsCount != expectedTrackableOperationsCount )
   {
      cout( "\nNumber of trackable operations: " + trackableOperationsCount + ", but got " + expectedTrackableOperationsCount );
      cout( "\nTEST FAILED" );
      // interrupt the test execution
      engine.operationQueue.requestInterruption();
      engine.operationQueue.__interruptedFromTestCheckedScript__ = true;
      terminate = true;
   }
}

// -------------------------
// ---  PIPELINE TERMINATAED
// -------------------------

if ( !terminate && env.event == "pipeline end" )
{
   if ( !engine.recordTest )
   {
      // all steps must be successfully executed
      let success = true;
      let i = 0;
      for ( ; success && i < engine.operationQueue.operations.length; i++ )
      {
         let status = engine.operationQueue.operations[ i ].status;
         success = status == undefined || status == OperationBlockStatus.DONE;
      }

      if ( !success )
         cout( "\nTEST FAILED: pipeline execution ended but operation #" + i + " is not done" );
   }

   cout( "\nTEST EXECUTION COMPLETED" );
}

// -------------------------
// ---  STEP START
// -------------------------

if ( !terminate && env.event == "start" )
{
   // provide information on the current progress
   cout( "\nPROGRESS: " + ( trackableIndex + 1 ) + " / " + trackableOperationsCount );
}

// -------------------------
// ---  STEP DONE
// -------------------------

// we check when an operation is done
if ( !terminate && env.event == "done" && !engine.recordTest )
{
   // check against result and current execution
   let testOperation = engine.executionStatus.ops[ env.operationIndex ];
   let operation = env.operation;

   cout( "\nenv.operationIndex: " + env.operationIndex );
   cout( "\ntestOperation: " + testOperation );

   // the index, result and notes must be the same
   let success = true;
   let keys = Object.keys( testOperation );
   for ( let i = 0; success && i < keys.length; i++ )
   {
      let key = keys[ i ];
      let testValue = testOperation[ key ];
      if ( typeof( testValue ) == "string" && testValue != operation[ key ] )
      {

         let msg = "expected field " + key + " with test value of\n[" + testValue + "]\nbut got\n[" + operation[ key ] + "]";
         cout( "\nSTEP #" + ( trackableIndex + 1 ) + " failed: " + msg );
         engine.operationQueue.requestInterruption();
         engine.operationQueue.__interruptedFromTestCheckedScript__ = true;
         success = false;
      }
   }
   if ( success )
      cout( "\nSTEP #" + ( trackableIndex + 1 ) + " success" );
}

cout( "\nend event: " + env.event );

cflush();
sleep( 1 );

// ----------------------------------------------------------------------------
// EOF WeightedBatchPreprocessing-testChecker.js - Released 2023-06-15T09:00:12Z

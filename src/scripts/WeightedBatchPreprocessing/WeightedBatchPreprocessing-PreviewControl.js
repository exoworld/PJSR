// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// WeightedBatchPreprocessing-PreviewControl.js - Released 2023-06-15T09:00:12Z
// ----------------------------------------------------------------------------
//
// This file is part of Weighted Batch Preprocessing Script version 2.5.10
//
// Copyright (c) 2019-2023 Roberto Sartori
// Copyright (c) 2020-2021 Adam Block
// Copyright (c) 2019 Tommaso Rubechi
// Copyright (c) 2012 Kai Wiechen
// Copyright (c) 2012-2023 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/*
 * Preview Control
 *
 * This file is part of the AnnotateImage script
 *
 * Copyright (C) 2013-2020, Andres del Pozo
 * Contributions (C) 2019-2020, Juan Conejero (PTeam)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/* beautify ignore:start */
#include <pjsr/ButtonCodes.jsh>
#include <pjsr/StdCursor.jsh>
/* beautify ignore:end */

function PreviewControl( parent )
{
   this.__base__ = Frame;
   this.__base__( parent );

   this.SetImage = function( image )
   {
      if ( this.image != undefined && this.image == image )
      {
         return;
      }

      let resetView = this.image == undefined;
      this.image = image;
      this.scaledImage = null;
      if ( resetView )
      {
         this.SetZoomOutLimit();
         this.UpdateZoom( this.zoomOutLimit );
      }
      else
      {
         this.UpdateZoom( this.zoom );
      }
   };

   this.UpdateZoom = function( newZoom, refPoint )
   {
      newZoom = Math.max( this.zoomOutLimit, Math.min( 1, newZoom ) );

      if ( newZoom == this.zoom && this.scaledImage )
         return;

      if ( refPoint == null )
         refPoint = new Point( this.scrollbox.viewport.width / 2, this.scrollbox.viewport.height / 2 );

      let imgx = null;
      if ( this.scrollbox.maxHorizontalScrollPosition > 0 )
         imgx = ( refPoint.x + this.scrollbox.horizontalScrollPosition ) / this.scale;

      let imgy = null;
      if ( this.scrollbox.maxVerticalScrollPosition > 0 )
         imgy = ( refPoint.y + this.scrollbox.verticalScrollPosition ) / this.scale;

      this.zoom = newZoom;
      this.scaledImage = null;
      gc( true );

      if ( this.zoom > 0 )
      {
         this.scale = this.zoom;
         this.zoomVal_Label.text = format( "%d:1", this.zoom );
      }
      else
      {
         this.scale = 1 / ( -this.zoom + 2 );
         this.zoomVal_Label.text = format( "1:%d", -this.zoom + 2 );
      }

      if ( this.image )
      {
         if ( this.scaledImageProvider )
            this.scaledImage = this.scaledImageProvider( this.image, this.scale );
         else
            this.scaledImage = this.image.scaled( this.scale );
      }
      else
         this.scaledImage = {
            width: this.image.width * this.scale,
            height: this.image.height * this.scale
         };

      this.scrollbox.maxHorizontalScrollPosition = Math.max( 0, this.scaledImage.width - this.scrollbox.viewport.width );
      this.scrollbox.maxVerticalScrollPosition = Math.max( 0, this.scaledImage.height - this.scrollbox.viewport.height );

      if ( this.scrollbox.maxHorizontalScrollPosition > 0 && imgx != null )
         this.scrollbox.horizontalScrollPosition = imgx * this.scale - refPoint.x;
      if ( this.scrollbox.maxVerticalScrollPosition > 0 && imgy != null )
         this.scrollbox.verticalScrollPosition = imgy * this.scale - refPoint.y;

      this.scrollbox.viewport.update();
   };

   this.zoomIn_Button = new ToolButton( this );
   this.zoomIn_Button.icon = this.scaledResource( ":/icons/zoom-in.png" );
   this.zoomIn_Button.setScaledFixedSize( 24, 24 );
   this.zoomIn_Button.toolTip = "Zoom In";
   this.zoomIn_Button.onMousePress = function()
   {
      this.parent.UpdateZoom( this.parent.zoom + 1 );
   };

   this.zoomOut_Button = new ToolButton( this );
   this.zoomOut_Button.icon = this.scaledResource( ":/icons/zoom-out.png" );
   this.zoomOut_Button.setScaledFixedSize( 24, 24 );
   this.zoomOut_Button.toolTip = "Zoom Out";
   this.zoomOut_Button.onMousePress = function()
   {
      this.parent.UpdateZoom( this.parent.zoom - 1 );
   };

   this.zoom11_Button = new ToolButton( this );
   this.zoom11_Button.icon = this.scaledResource( ":/icons/zoom-1-1.png" );
   this.zoom11_Button.setScaledFixedSize( 24, 24 );
   this.zoom11_Button.toolTip = "Zoom 1:1";
   this.zoom11_Button.onMousePress = function()
   {
      this.parent.UpdateZoom( 1 );
   };

   this.statusMessage = new Label( this );
   this.statusMessage.textAlignment = TextAlign_Center;
   this.statusMessage.useRichText = true;
   this.statusMessage.setVariableWidth();

   this.setStatusMessage = ( text ) =>
   {
      this.statusMessage.text = text;
   }

   this.buttons_Sizer = new HorizontalSizer;
   this.buttons_Sizer.margin = 4;
   this.buttons_Sizer.spacing = 4;
   this.buttons_Sizer.add( this.zoomIn_Button );
   this.buttons_Sizer.add( this.zoomOut_Button );
   this.buttons_Sizer.add( this.zoom11_Button );
   this.buttons_Sizer.add( this.statusMessage );

   this.appendTopControl = ( control ) =>
   {
      this.buttons_Sizer.insert( this.buttons_Sizer.numberOfItems - 1, control );
   }

   this.appendBottomControl = ( control ) =>
   {
      this.bottomControl_Sizer.add( control );
   }

   this.setScaledMinSize( 600, 400 );
   this.zoom = 1;
   this.scale = 1;
   this.zoomOutLimit = -5;
   this.scrollbox = new ScrollBox( this );
   this.scrollbox.autoScroll = true;
   this.scrollbox.tracking = true;
   this.scrollbox.cursor = new Cursor( StdCursor_UpArrow );

   this.scroll_Sizer = new HorizontalSizer;
   this.scroll_Sizer.add( this.scrollbox );

   this.SetZoomOutLimit = function()
   {
      let scaleX = Math.ceil( this.image.width / this.scrollbox.viewport.width );
      let scaleY = Math.ceil( this.image.height / this.scrollbox.viewport.height );
      let scale = Math.max( scaleX, scaleY );
      this.zoomOutLimit = -scale + 2;
   };

   this.scrollbox.onHorizontalScrollPosUpdated = function( newPos )
   {
      this.viewport.update();
   };

   this.scrollbox.onVerticalScrollPosUpdated = function( newPos )
   {
      this.viewport.update();
   };

   this.forceRedraw = function()
   {
      this.scrollbox.viewport.update();
   };

   this.scrollbox.viewport.onMouseWheel = function( x, y, delta, buttonState, modifiers )
   {
      let preview = this.parent.parent;
      preview.UpdateZoom( preview.zoom + ( ( delta > 0 ) ? -1 : 1 ), new Point( x, y ) );
   };

   this.scrollbox.viewport.onMousePress = function( x, y, button, buttonState, modifiers )
   {
      let preview = this.parent.parent;
      if ( preview.scrolling || button != MouseButton_Left )
         return;
      preview.scrolling = {
         orgCursor: new Point( x, y ),
         orgScroll: new Point( preview.scrollbox.horizontalScrollPosition, preview.scrollbox.verticalScrollPosition )
      };
      this.cursor = new Cursor( StdCursor_ClosedHand );
   };

   this.scrollbox.viewport.onMouseMove = function( x, y, buttonState, modifiers )
   {
      let preview = this.parent.parent;
      if ( preview.scrolling )
      {
         preview.scrollbox.horizontalScrollPosition = preview.scrolling.orgScroll.x - ( x - preview.scrolling.orgCursor.x );
         preview.scrollbox.verticalScrollPosition = preview.scrolling.orgScroll.y - ( y - preview.scrolling.orgCursor.y );
      }

      let ox = ( this.parent.maxHorizontalScrollPosition > 0 ) ?
         -this.parent.horizontalScrollPosition : ( this.width - preview.scaledImage.width ) / 2;
      let oy = ( this.parent.maxVerticalScrollPosition > 0 ) ?
         -this.parent.verticalScrollPosition : ( this.height - preview.scaledImage.height ) / 2;
      let coordPx = new Point( ( x - ox ) / preview.scale, ( y - oy ) / preview.scale );
      if ( coordPx.x < 0 ||
         coordPx.x > preview.image.width ||
         coordPx.y < 0 ||
         coordPx.y > preview.image.height )
      {
         preview.Xval_Label.text = "---";
         preview.Yval_Label.text = "---";
      }
      else
      {
         preview.Xval_Label.text = format( "%8.2f", coordPx.x );
         preview.Yval_Label.text = format( "%8.2f", coordPx.y );
      }
   };

   this.scrollbox.viewport.onMouseRelease = function( x, y, button, buttonState, modifiers )
   {
      let preview = this.parent.parent;
      if ( preview.scrolling && button == MouseButton_Left )
      {
         preview.scrollbox.horizontalScrollPosition = preview.scrolling.orgScroll.x - ( x - preview.scrolling.orgCursor.x );
         preview.scrollbox.verticalScrollPosition = preview.scrolling.orgScroll.y - ( y - preview.scrolling.orgCursor.y );
         preview.scrolling = null;
         this.cursor = new Cursor( StdCursor_UpArrow );
      }
   };

   this.scrollbox.viewport.onResize = function( wNew, hNew, wOld, hOld )
   {
      let preview = this.parent.parent;
      if ( preview.metadata && preview.scaledImage )
      {
         this.parent.maxHorizontalScrollPosition = Math.max( 0, preview.scaledImage.width - wNew );
         this.parent.maxVerticalScrollPosition = Math.max( 0, preview.scaledImage.height - hNew );
         preview.SetZoomOutLimit();
         preview.UpdateZoom( preview.zoom );
      }
      this.update();
   };

   this.scrollbox.viewport.onPaint = function( x0, y0, x1, y1 )
   {
      let preview = this.parent.parent;
      let graphics = new VectorGraphics( this );

      graphics.fillRect( x0, y0, x1, y1, new Brush( 0xff202020 ) );

      let offsetX = ( this.parent.maxHorizontalScrollPosition > 0 ) ?
         -this.parent.horizontalScrollPosition : ( this.width - preview.scaledImage.width ) / 2;
      let offsetY = ( this.parent.maxVerticalScrollPosition > 0 ) ?
         -this.parent.verticalScrollPosition : ( this.height - preview.scaledImage.height ) / 2;
      graphics.translateTransformation( offsetX, offsetY );

      if ( preview.image )
         graphics.drawBitmap( 0, 0, preview.scaledImage );
      else
         graphics.fillRect( 0, 0, preview.scaledImage.width, preview.scaledImage.height, new Brush( 0xff000000 ) );

      graphics.pen = new Pen( 0xffffffff, 0 );
      graphics.drawRect( -1, -1, preview.scaledImage.width + 1, preview.scaledImage.height + 1 );

      if ( preview.onCustomPaint )
      {
         graphics.antialiasing = true;
         graphics.scaleTransformation( preview.scale, preview.scale );
         preview.onCustomPaint.call( preview.onCustomPaintScope, graphics, x0, y0, x1, y1 );
      }

      graphics.end();
   };

   this.zoomLabel_Label = new Label( this );
   this.zoomLabel_Label.text = "Zoom:";
   this.zoomVal_Label = new Label( this );
   this.zoomVal_Label.text = "1:1";

   this.Xlabel_Label = new Label( this );
   this.Xlabel_Label.text = "X:";
   this.Xval_Label = new Label( this );
   this.Xval_Label.text = "---";

   this.Ylabel_Label = new Label( this );
   this.Ylabel_Label.text = "Y:";
   this.Yval_Label = new Label( this );
   this.Yval_Label.text = "---";

   this.coords_Frame = new Frame( this );
   this.coords_Frame.styleSheet = this.scaledStyleSheet(
      "QLabel { font-family: Hack; background: white; }" );
   this.coords_Frame.backgroundColor = 0xffffffff;
   this.coords_Frame.sizer = new HorizontalSizer;
   this.coords_Frame.sizer.margin = 4;
   this.coords_Frame.sizer.spacing = 4;
   this.coords_Frame.sizer.add( this.zoomLabel_Label );
   this.coords_Frame.sizer.add( this.zoomVal_Label );
   this.coords_Frame.sizer.addSpacing( 20 );
   this.coords_Frame.sizer.add( this.Xlabel_Label );
   this.coords_Frame.sizer.add( this.Xval_Label );
   this.coords_Frame.sizer.addSpacing( 20 );
   this.coords_Frame.sizer.add( this.Ylabel_Label );
   this.coords_Frame.sizer.add( this.Yval_Label );
   this.coords_Frame.sizer.addStretch();

   // optional container for extra controls injected from outside
   this.bottomControl_Sizer = new HorizontalSizer;
   this.bottomControl_Sizer.margin = 4;
   this.bottomControl_Sizer.spacing = 4;

   this.sizer = new VerticalSizer;
   this.sizer.add( this.buttons_Sizer );
   this.sizer.add( this.scroll_Sizer );
   this.sizer.add( this.bottomControl_Sizer );
   this.sizer.add( this.coords_Frame );
}

PreviewControl.prototype = new Frame;

// ----------------------------------------------------------------------------
// EOF WeightedBatchPreprocessing-PreviewControl.js - Released 2023-06-15T09:00:12Z

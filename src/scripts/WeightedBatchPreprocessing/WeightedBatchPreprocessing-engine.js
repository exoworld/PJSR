// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// WeightedBatchPreprocessing-engine.js - Released 2023-06-15T09:00:12Z
// ----------------------------------------------------------------------------
//
// This file is part of Weighted Batch Preprocessing Script version 2.5.10
//
// Copyright (c) 2019-2023 Roberto Sartori
// Copyright (c) 2020-2021 Adam Block
// Copyright (c) 2019 Tommaso Rubechi
// Copyright (c) 2012 Kai Wiechen
// Copyright (c) 2012-2023 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/* beautify ignore:start */
#include <pjsr/Compression.jsh>
#include <pjsr/LinearDefectDetection.jsh>
#include <pjsr/LinearPatternSubtraction.jsh>
#include <pjsr/SourceCodeFlag.jsh>
#include <pjsr/StdDialogCode.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/CryptographicHash.jsh>
#include "WeightedBatchPreprocessing-consoleLogger.js"
#include "WeightedBatchPreprocessing-processLogger.js"
#include "WeightedBatchPreprocessing-OperationQueue.js"
#include "WeightedBatchPreprocessing-LNReferenceSelector.js"

#define USE_SOLVER_LIBRARY true
#define SETTINGS_MODULE "WBPP"
#define STAR_CSV_FILE   File.systemTempDirectory + "/stars.csv"
#include "../AdP/CommonUIControls.js"
#include "../AdP/AstronomicalCatalogs.jsh"
#include "../AdP/WCSmetadata.jsh"
#include "../AdP/ImageSolver.js"
#include "../AdP/SearchCoordinatesDialog.js"
/* beautify ignore:end */

// ----------------------------------------------------------------------------

/**
 * Keyword constructor: defines a keyword by name and mode mask.
 *
 * @param {String} name
 * @param {WBPPKeywordMode} mode
 */
function Keyword( name, mode )
{
   this.name = name;
   this.mode = mode;
}

// ----------------------------------------------------------------------------

/**
 * Manages the list of keywords and provides helpers for
 * adding/replacing/removing/sorting and searching keywords.
 * Duplicates and empty keywords are now allowed.
 */
function Keywords()
{
   this.list = [];

   /**
    * Safely add a new keyword and checks for duplicates and empty keyword.
    *
    * @param {String} name
    * @param {String?} mode keyword mode, pre-processing only as default
    * @returns undefined on success, error message in case of error
    */
   this.add = ( name, mode ) =>
   {
      // default mode is pre-processing only
      mode = mode || WBPPKeywordMode.PRE;
      if ( name.length == 0 )
         return "Unable to add an empty keyword."
      if ( this.contains( name ) )
         return "Keyword \"" + name + "\" is already in the list."
      this.list.push( new Keyword( name, mode ) );
      return undefined;
   };

   /**
    * Replace an exsistent keyword name.
    * If the new name to be assigned collides with an existing keywords then
    * the replacement does not occur and the function silently returns.
    *
    * @param {String} oldName
    * @param {String} newName
    */
   this.replace = ( oldName, newName ) =>
   {
      if ( this.contains( newName ) )
         return;
      this.list.forEach( kw =>
      {
         if ( kw.name == oldName )
            kw.name = newName
      } );
   };

   /**
    * Removes a keyword witht the given name.
    *
    * @param {String} name
    */
   this.remove = ( name ) =>
   {
      this.list = this.list.filter( k => k.name != name );
   };

   /**
    * Moves a keyword up or down in the list.
    *
    * @param {*} name
    * @param {*} up true to move keyword in the previous position, false to move it in the next
    * @return the new index of the keyword
    */
   this.move = ( name, up ) =>
   {
      let indx = this.list.reduce( ( obj, keyword, index ) =>
      {
         return keyword.name == name ? index : obj;
      }, -1 );
      if ( indx == -1 )
         return undefined;

      // do nothing if keyword is not found or if the keyword would move out of bounds
      if ( indx == -1 || ( indx == 0 && up ) || ( indx == this.list.length - 1 && !up ) )
         return undefined;
      let dstIndx = indx + ( up ? -1 : 1 );
      let tmp = this.list[ dstIndx ];
      this.list[ dstIndx ] = this.list[ indx ];
      this.list[ indx ] = tmp;
      return dstIndx;
   };

   /**
    * Checks if a keyword with the given name exists.
    *
    * @param {string} name
    * @returns Boolean
    */
   this.contains = ( name ) =>
   {
      return this.list.map( k => k.name ).indexOf( name ) > -1;
   };

   /**
    * Filters the given keywords object accordingly to the current keywords configuration.
    * Always returns an empty object if keywords are globally disabled.
    *
    * @param {{String:String}} keywords keywords to be filtered
    * @param {WBPPKeywordMode} mode filtering mode
    * @returns {String:Stting} filtered keywords
    */
   this.filterKeywordsForMode = ( keywords, mode ) =>
   {
      if ( !engine.groupingKeywordsEnabled || !keywords )
      {
         return {};
      }
      let namesForMode = this.list.reduce( ( obj, keyword ) =>
      {
         if ( keyword.mode & mode )
            obj.push( keyword.name );
         return obj;
      }, [] );

      // return the keywords enabled for the given mode
      return Object
         .keys( keywords )
         .filter( name => ( namesForMode.indexOf( name ) != -1 ) )
         .reduce( ( obj, name ) =>
         {
            obj[ name ] = keywords[ name ];
            return obj;
         },
         {} );
   };

   /**
    * Returns the kewyords object containing only the keywords enabled for the given mode.
    *
    * @param {WBPPGroupingMode} mode
    */
   this.keywordsForMode = ( mode ) =>
   {
      return this.list.filter( kw => kw.mode & mode );
   };

   /**
    * Switches the mode of the given keyword.
    *
    * @param {String} name
    */
   this.switchMode = ( name ) =>
   {
      let keyword = this.list.filter( kw => kw.name == name );
      if ( keyword.length == 0 )
         return;
      // manually control the loop cycle
      switch ( keyword[ 0 ].mode )
      {
         case WBPPKeywordMode.NONE:
            keyword[ 0 ].mode = WBPPKeywordMode.PRE;
            break;
         case WBPPKeywordMode.PRE:
            keyword[ 0 ].mode = WBPPKeywordMode.PREPOST;
            break;
         case WBPPKeywordMode.PREPOST:
            keyword[ 0 ].mode = WBPPKeywordMode.POST;
            break;
         case WBPPKeywordMode.POST:
            keyword[ 0 ].mode = WBPPKeywordMode.NONE;
            break;
      }
   };

   /**
    * Returns a flat list of all keyword names
    */
   this.names = () =>
   {
      return this.list.map( k => k.name );
   };

   /**
    * Returns the sorted list of keyword names.
    *
    * @param {*} mode the mode of the keywords
    * @return {*}
    */
   this.sortedNames = ( mode ) =>
   {
      let keywords = this.list.filter( k => k.mode == mode ).map( k => k.name );
      keywords.sort();
      return keywords;
   }
}

Keywords.prototype = new Object;

// ----------------------------------------------------------------------------

/**
 * File Item constructor. A File Item refefrences a file on disk along with all its processed versions.
 *
 * @param {String} filePath the file path
 * @param {ImageType} imageType type of image
 * @param {String} filter filter name
 * @param {Number} binning image's binning
 * @param {Number} exposureTime image's exposure time
 * @param {{String:String}?} fileKeywords name-value map containing the file header metadata
 * @param {Object} size {width:Numeric, height:Numeric} image's width and height
 * @param {Object} matchingSizes { mode: {width:Numeric, height:Numeric}} the size to use when mathing the group of the given mode
 * @param {Number} height image's height
 * @param {Boolean} isCFA true if the file contains a CFA image
 * @param {Boolean} isMaster true if the image is a master file
 * @param {{String:String}?} keywords the keywords key-value map
 * @param {{String:Any}?} solverParams an object containing the solver parameters for the files, like the observation date, the pixel size
 *                                     and the focal length
 * @param {Overscan?} overscan an Overscan object containing the overscan regions
 */
function FileItem( filePath, imageType, filter, binning, exposureTime, fileKeywords, size, matchingSizes, isCFA, isMaster, keywords, solverParams, overscan )
{
   this.__base__ = Object;
   this.__base__();

   let
   {
      smartNaming
   } = WBPPUtils.shared();

   this.filePath = filePath;
   this.imageType = imageType;
   this.binning = binning;
   this.filter = filter;
   this.exposureTime = exposureTime;
   this.fileKeywords = fileKeywords;
   this.enabled = true;
   // real image size
   this.size = size;
   this.matchingSizes = {};
   this.matchingSizes[ WBPPGroupingMode.PRE ] = size;
   this.matchingSizes[ WBPPGroupingMode.POST ] = size;
   this.createdWithSmartNamingEnabled = engine.smartNamingOverride;

   this.solverParams = {
      observationDate: "",
      ra: NaN,
      dec: NaN,
      pixelSize: NaN,
      focalLength: NaN
   }
   if ( solverParams != undefined )
   {
      let keys = Object.keys( solverParams );
      for ( let i = 0; i < keys.length; i++ )
         this.solverParams[ keys[ i ] ] = solverParams[ keys[ i ] ];
   }

   if ( matchingSizes != undefined )
   {
      let keys = Object.keys( matchingSizes );
      for ( let i = 0; i < keys.length; i++ )
         this.matchingSizes[ keys[ i ] ] = matchingSizes[ keys[ i ] ];
   }

   this.isCFA = isCFA;
   this.isMaster = isMaster;
   this.keywords = keywords ||
   {};

   this.overscan = new Overscan()
   if ( overscan != undefined )
      this.overscan.copyFrom( overscan )
   /**
    * Updates the keywords associated to the file accordingly to the
    *
    */
   this.updateKeywords = () =>
   {
      this.keywords = {};
      engine.keywords.names().forEach( keywordName =>
      {
         // get the keyword from the file path
         let value = smartNaming.getCustomKeyValueFromPath( keywordName, this.filePath );
         if ( value != undefined )
            this.keywords[ keywordName ] = smartNaming.sanitizeKeywordValue( value );
         else
         {
            // get the keyword from the file header
            value = this.fileKeywords[ keywordName ];
            if ( value != undefined )
               this.keywords[ keywordName ] = smartNaming.sanitizeKeywordValue( value );
         }
      } );
   }

   /**
    * Initializes the internal data referencing the file.
    */
   this.initForProcessing = () =>
   {
      this.processed = {};
      this.current = {};
      this.descriptor = {};
      this.localNormalizationFile = {};
      this.drizzleFile = {};
      this.isReference = {};
      this.current[ "default" ] = this.filePath;
      this.descriptor[ "default" ] = undefined;
      this.localNormalizationFile[ "default" ] = undefined;
      this.drizzleFile[ "default" ] = undefined;
      this.isReference[ "default" ] = false;
   };

   /**
    * Invoked when a processing step has been performed successfully.
    *
    * @param {WBPPFrameProcessingStep} step step executed
    * @param {String} filePath processed output file
    * @param {String} associatedID defines the ID that identifies an associated file, if not
    *                             provided or empty string then the default id is used
    */
   this.processingSucceeded = ( step, filePath, associatedID ) =>
   {
      if ( associatedID == null || associatedID == "" )
         associatedID = "default";
      if ( this.processed[ associatedID ] == null )
         this.processed[ associatedID ] = {};
      this.processed[ associatedID ][ step ] = filePath;
      this.current[ associatedID ] = filePath;
   };

   /**
    * Mark the file as failed to process. Indicates that an error occurred.
    *
    */
   this.processingFailed = ( associatedID ) =>
   {
      if ( associatedID == null || associatedID == "" )
         associatedID = "default";
      this.current[ associatedID ] = undefined;
   };

   /**
    * Checks if the file processing is failed.
    *
    */
   this.failed = ( associatedID ) =>
   {
      if ( associatedID == null || associatedID == "" )
         associatedID = "default";
      return this.current[ associatedID ] == undefined;
   };

   /**
    * Stores the associated local normalization file
    *
    * @param {*} filePath the local normalization file path
    * @param {*} associatedID the associated ID
    */
   this.addLocalNormalizationFile = ( filePath, associatedID ) =>
   {
      if ( associatedID == null || associatedID == "" )
         associatedID = "default";
      this.localNormalizationFile[ associatedID ] = filePath;
   };

   /**
    * Stores the associated drizzle file
    *
    * @param {*} filePath the drizzle file path
    * @param {*} associatedID the associated ID
    */
   this.addDrizzleFile = ( filePath, associatedID ) =>
   {
      if ( associatedID == null || associatedID == "" )
         associatedID = "default";
      this.drizzleFile[ associatedID ] = filePath;
   };

   /**
    * Marks the file item as reference. This information tracks that the file item
    * has been used as a reference frame to align other images.
    *
    * @param {*} associatedID
    */
   this.markAsReference = ( associatedID ) =>
   {
      if ( associatedID == null || associatedID == "" )
         associatedID = "default";
      this.isReference[ associatedID ] = true;
   };

   this.initForProcessing();
}

FileItem.prototype = new Object;

/**
 * Active Frame object. An Active Frame is a File Item wrapper that keeps a reference
 * to the associatedID version of the File Item and wraps some processing functions along
 * with sum supporting f
 *
 * @param {*} fileItem the file item the active frame refers to
 * @param {*} associatedID the associated id defining the FileItem version
 */
function ActiveFrame( fileItem, associatedID )
{
   // Store the references
   this.fileItem = fileItem;
   this.associatedID = associatedID || "default";

   /**
    * Invoked when a processing step has been performed successfully on this active item.
    * Internally, the corresponding FileItem version will be marked as succesfully processed.
    *
    * @param {WBPPFrameProcessingStep} step step executed
    * @param {String} filePath processed output file
    */
   this.processingSucceeded = ( step, filePath, associatedID ) =>
   {
      let id = associatedID && associatedID.length > 0 ? associatedID : this.associatedID;
      this.fileItem.processingSucceeded( step, filePath, id );
      this.sync();
   };

   /**
    * Mark the file as failed to process. Indicates that an error occurred.
    *
    */
   this.processingFailed = ( associatedID ) =>
   {
      let id = associatedID && associatedID.length > 0 ? associatedID : this.associatedID;
      this.fileItem.processingFailed( id );
      this.sync();
   };

   /**
    * Checks if the current active frame has been processed.
    *
    */
   this.isProcessed = () =>
   {
      let processed = this.fileItem.processed[ this.associatedID ];
      return processed != null && Object.keys( processed ).length > 0;
   };

   /**
    * Returns the file name of the current item.
    *
    * @param {*} suffix
    */
   this.currentFileName = () =>
   {
      let name = File.extractName( this.current );
      let ext = File.extractExtension( this.current );

      return name + ext;
   };

   /**
    * Stores the descriptor associated to the curent Active Frame.
    *
    * @param {*} descriptor the descriptor to be stored
    */
   this.setDescriptor = ( descriptor ) =>
   {
      this.fileItem.descriptor[ this.associatedID ] = descriptor;
      this.sync();
   };

   /**
    * Stores the local normalization file for the associated ID
    *
    * @param {*} filePath the local normalization file
    */
   this.addLocalNormalizationFile = ( filePath ) =>
   {
      this.fileItem.addLocalNormalizationFile( filePath, this.associatedID );
      this.sync();
   };

   /**
    * Stores the drizzle file for the associated ID
    *
    * @param {*} filePath the drizzle file
    */
   this.addDrizzleFile = ( filePath ) =>
   {
      this.fileItem.addDrizzleFile( filePath, this.associatedID );
      this.sync();
   };

   /**
    * Marks the current associated File Item as a reference frame.
    *
    */
   this.markAsReference = () =>
   {
      this.fileItem.markAsReference( this.associatedID );
      this.sync();
   };

   /**
    * Synchronize the active item with the corresponding fileItem version.
    *
    */
   this.sync = () =>
   {
      // extract and inject the descriptor and the current item in the active frame
      // current is used to refer to the current file path
      this.current = this.fileItem.current[ this.associatedID ];
      // binning and descriptor is used when searching for the best reference frame
      this.binning = this.fileItem.binning;
      this.descriptor = this.fileItem.descriptor[ this.associatedID ];
      // exposure time is used to compute the active frame's total integration time
      this.exposureTime = this.fileItem.exposureTime;
      // get the associated local normalization file
      this.localNormalizationFile = this.fileItem.localNormalizationFile[ this.associatedID ];
      // get the associated drizzle file
      this.drizzleFile = this.fileItem.drizzleFile[ this.associatedID ];
      // track if the current active item was used as reference
      this.isReference = this.fileItem.isReference[ this.associatedID ];
   };

   // sync
   this.sync();
}

/**
 * Creates a dummy active frames programmatically referencing a file on the disk.
 * This dummy active frame must be used for accessory operations only and must never
 * be used as a conrete instance referencing a real FileItem,
 *
 * @param {*} filePath
 */
ActiveFrame.dummy = function( filePath, cloneFrom )
{
   let imageType = ImageType.LIGHT;
   let filter = "NoFilter";
   let binning = 1;
   let exposure = 0;
   let fileKeywords = {};
   let size = {
      width: 0,
      height: 0
   };
   let isCFA = false;
   let isMaster = false;
   let keywords = {};
   let solverParams = {};
   let overscan;
   if ( cloneFrom == undefined )
   {
      imageType = cloneFrom.imageType;
      filter = cloneFrom.filter;
      binning = cloneFrom.binning;
      exposure = cloneFrom.exposure;
      fileKeywords = cloneFrom.fileKeywords;
      size = cloneFrom.size;
      isCFA = cloneFrom.isCFA;
      isMaster = cloneFrom.isMaster;
      keywords = cloneFrom.keywords;
      solverParams = cloneFrom.solverParams;
      overscan = cloneFrom.overscan;
   }

   let fileItem = new FileItem(
      filePath,
      imageType,
      filter,
      binning,
      exposure,
      fileKeywords,
      size,
      undefined /* matching sizes */ ,
      isCFA,
      isMaster,
      keywords,
      solverParams,
      overscan );

   return new ActiveFrame( fileItem );
}

ActiveFrame.prototype = new Object;

// ----------------------------------------------------------------------------

/**
 * Frame group object constructor.
 *
 * @param {ImageType} imageType the type of images in this group
 * @param {String} filter the filter name associated to this group
 * @param {Number} binning the binning of images in this group
 * @param {Number} exposureTime the initial exposure time set for this group
 * @param {Object} size {width: Numeric, height:Numeric} optional, the size of the images in the group. If firstItem is provided then
 *                      it overrides this value
 * @param {Boolean} isCFA true if this group contains CFA images
 * @param {FileItem} firstItem optional, initial first item to be added
 * @param {Boolean} hasMaster must set to true if the item provided is to be considered a master file
 * @param {{String:String}?} keywords the keywords key-value map
 * @param {WBPPGroupingMode} mode the group mode
 * @param {String} associatedRGBchannel the associated RGB channel if this group is created to support the RGB channel separation
 * @param {String} linkedGroupID the id of a linked group
 * @param {Object} drizzleData the required drizzle data {scale, dropShrink, function} if drizzle integration is required
 */
function FrameGroup( imageType, filter, binning, exposureTime, size, isCFA, firstItem, hasMaster, keywords, mode, associatedRGBchannel, linkedGroupID, drizzleData )
{
   this.__base__ = Object;
   this.__base__();

   let
   {
      isEmptyString
   } = WBPPUtils.shared();

   // commons
   this.imageType = imageType;
   this.binning = binning;
   this.hasMaster = ( imageType != ImageType.LIGHT ) && hasMaster;

   // reference exposure time for the group
   this.exposureTime = ( imageType == ImageType.BIAS ) ? 0 : exposureTime;
   // filter name of the group, used by flat and light frame groups
   this.filter = ( imageType == ImageType.BIAS || imageType == ImageType.DARK ) ? "" : filter;
   // list of all exposure times of the frames in the group
   this.exposureTimes = [ this.exposureTime ];
   // used by flat and light frames
   this.optimizeMasterDark = DEFAULT_OPTIMIZE_DARKS;
   // by default the group does not have a size when created
   this.size = size ||
   {
      width: 0,
      height: 0
   };
   // group CFA
   this.isCFA = isCFA;
   // used by light frames to define the debayer Pattern and Method
   this.CFAPattern = DEFAULT_CFA_PATTERN;
   this.debayerMethod = DEFAULT_DEBAYER_METHOD;
   // used by LIGHT frames when a Master Flat is involved during calibration
   this.separateCFAFlatScalingFactors = isCFA;
   // group keywords, use only the keywords enabled for the given mode
   this.keywords = engine.keywords.filterKeywordsForMode( keywords, mode );
   // the group mode
   this.mode = mode;
   // list of frames
   this.fileItems = new Array;
   // output pedestal mode
   this.lightOutputPedestalMode = DEFAULT_PEDESTAL_MODE;
   // output pedestal used for light frames
   this.lightOutputPedestal = DEFAULT_LIGHT_OUTPUT_PEDESTAL;
   // output pedestal limit
   this.lightOutputPedestalLimit = DEFAULT_PEDESTAL_LIMIT;
   // name of che RGB channel this group is associated to, used to manage the debayer option "Separate RGB channels"
   this.associatedRGBchannel = associatedRGBchannel;
   // the ID of a linked group
   this.linkedGroupID = linkedGroupID;
   // drizzle data
   this.drizzleData = {};
   // a hidden group is a group that is not visible in the control panel
   this.isHidden = false;
   // an active group is a group ready for processing
   this.isActive = true;
   // cosmetic correction template icon name
   this.CCTemplate = "";

   // internally stores the current header descriptor length, used to pretty print the
   // group header in diagnostic messages
   this.footerLengthForCurrentHeader = 0;

   // Stores the set of master file names generated for to the group
   this.masterFiles = {};

   /**
    *  PRepare the group for being processed.
    */
   this.initForProcessing = () =>
   {
      // clear the master files set.
      this.masterFiles = {};
   }
   /**
    * This is a convenient method to clone an existing group keepoing only the
    * given active items. Caller must ensure that active items are items of the
    * group otherwise an unexpected behaiour.
    *
    * @param {*} activeItems
    */
   this.cloneWithActiveItems = ( activeItems ) =>
   {
      let frameGroup = new FrameGroup(
         this.imageType,
         this.filter,
         this.binning,
         this.exposureTime,
         this.size,
         this.isCFA,
         undefined,
         this.hasMaster,
         this.keywords,
         this.mode,
         this.associatedRGBchannel,
         this.linkedGroupID,
         this.drizzleData );
      frameGroup.fileItems = activeItems.map( item => item.fileItem );
      return frameGroup;
   };

   /**
    * Returns true if group is virtual. A virtual group is a group that is programmatically
    * created to support the accomplishment of one or more tasks but does not correspond to
    * any concrete group that is created when file items are added to WBPP. An example are the
    * _R, _G and _B groups that refer to the separated RGB channels associated to a CFA group: these
    * groups are created programmatically to the groups list when CFA groups get debayered and the user
    * enabled the Separate RGB Channel debayer feature.
    *
    * @returns true if group is virtual
    */
   this.isVirtual = () =>
   {
      return this.associatedRGBchannel != null
   };

   // ------ DRIZZLE MANAGEMENT

   this.isDrizzleAvailable = () =>
   {

      // disabled for all virtual groups that are not one of the RBG associated channels
      if ( this.isVirtual() && !( this.associatedRGBchannel == WBPPAssociatedChannel.R || this.associatedRGBchannel == WBPPAssociatedChannel.G || this.associatedRGBchannel == WBPPAssociatedChannel.B ) )
         return false;

      // superpixel debayer is not compatible, so if the group is CFA and debayer mode is superpixel we disable drizzle
      if ( this.isCFA )
      {
         // we need to search the calibration group of all frames and ensure that no superpixel is selected as debayer mode
         let calibrationGroups = engine.groupsManager.groupsForMode( WBPPGroupingMode.PRE );
         let superpixelFrames = {};
         for ( let i = 0; i < calibrationGroups.length; i++ )
         {
            let cg = calibrationGroups[ i ];
            if ( cg.imageType == ImageType.LIGHT && cg.isCFA && cg.debayerMethod == Debayer.prototype.SuperPixel )
            {
               for ( let j = 0; j < cg.fileItems.length; j++ )
                  superpixelFrames[ cg.fileItems[ j ].filePath ] = true;
            }
         }

         // now check if the group contains at least one frame debayered with superpixel
         for ( let i = 0; i < this.fileItems.length; i++ )
         {
            if ( !!superpixelFrames[ this.fileItems[ i ].filePath ] )
            {
               return false;
            }
         }
      }

      return true;
   }

   this.isDrizzleEnabled = () =>
   {
      if ( this.drizzleData.enabled == undefined )
         return false;
      return this.drizzleData.enabled && this.isDrizzleAvailable();
   };

   this.enableDrizzle = () =>
   {
      // drizzle can be enabled only if available
      this.drizzleData.enabled = this.isDrizzleAvailable();
      this.syncDrizzleOnAssociatedRGBChannels( this.linkedGroupID, this.drizzleData );
   };

   this.disableDrizzle = () =>
   {
      this.drizzleData.enabled = false;
      this.syncDrizzleOnAssociatedRGBChannels( this.linkedGroupID, this.drizzleData );
   };

   this.setDrizzleData = ( data ) =>
   {
      // we don't set any drizzle data on associated channels (drizzle is not applicable)
      if ( data == undefined || !this.isDrizzleAvailable() ) return;

      if ( data.enabled )
         this.enableDrizzle()
      else
         this.disableDrizzle();
      if ( data.scale )
         this.setDrizzleScale( data.scale );
      if ( data.dropShrink )
         this.setDrizzleDropShrink( data.dropShrink );
      if ( data.function )
         this.setDrizzleFunction( data.function );
      if ( data.gridSize )
         this.setDrizzleGridSize( data.gridSize );
   };

   // SETTERS

   this.setDrizzleDataDefaults = () =>
   {
      this.drizzleData.enabled = this.isDrizzleEnabled();
      this.setDrizzleScale( DEFAULT_DRIZZLE_SCALE );
      if ( this.isCFA )
         this.setDrizzleDropShrink( DEFAULT_DRIZZLE_DROP_SHRINK_CFA );
      else
         this.setDrizzleDropShrink( DEFAULT_DRIZZLE_DROP_SHRINK_MONO );
      this.setDrizzleFunction( DEFAULT_DRIZZLE_FUNCTION );
      this.setDrizzleGridSize( DEFAULT_DRIZZLE_GRID_SIZE );
      this.syncDrizzleOnAssociatedRGBChannels( this.linkedGroupID, this.drizzleData );
   };

   this.syncDrizzleOnAssociatedRGBChannels = ( linkedGroupID, drizzleData ) =>
   {
      if ( linkedGroupID == undefined )
         return;

      // we keep the drizzle scale in sync to ensure that R, G and B channels can be combined
      let groups = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST )
         .filter( g => g.linkedGroupID == linkedGroupID )
         .filter( g => ( g.associatedRGBchannel == WBPPAssociatedChannel.R ||
            g.associatedRGBchannel == WBPPAssociatedChannel.G ||
            g.associatedRGBchannel == WBPPAssociatedChannel.B ) );

      for ( let i = 0; i < groups.length; i++ )
      {
         groups[ i ].drizzleData.enabled = drizzleData.enabled;
         groups[ i ].drizzleData.scale = drizzleData.scale;
      }
   };

   this.setDrizzleScale = ( scale ) =>
   {
      this.drizzleData.scale = scale;
      this.syncDrizzleOnAssociatedRGBChannels( this.linkedGroupID, this.drizzleData );
   };

   this.setDrizzleDropShrink = ( dropShrink ) =>
   {
      this.drizzleData.dropShrink = dropShrink;
      this.syncDrizzleOnAssociatedRGBChannels( this.linkedGroupID, this.drizzleData );
   };

   this.setDrizzleFunction = ( f ) =>
   {
      this.drizzleData.function = f;
      this.syncDrizzleOnAssociatedRGBChannels( this.linkedGroupID, this.drizzleData );
   };

   this.setDrizzleGridSize = ( gridSize ) =>
   {
      this.drizzleData.gridSize = gridSize;
      this.syncDrizzleOnAssociatedRGBChannels( this.linkedGroupID, this.drizzleData );
   };

   this.getDrizzleFunctionName = () =>
   {
      let kernels = [
         "Square",
         "Circular",
         "Gaussian",
         "VarShape, β = 1",
         "VarShape, β = 1.5",
         "VarShape, β = 3",
         "VarShape, β = 4",
         "VarShape, β = 5",
         "VarShape, β = 6"
      ];

      return this.isDrizzleEnabled() ? kernels[ this.drizzleFunction() ] : "";
   };

   this.getDrizzleGridSizeString = () =>
   {
      if ( this.drizzleFunction() < DrizzleIntegration.prototype.Kernel_Gaussian )
         return "";
      return "grid = " + this.drizzleData.gridSize;
   };

   // GETTERS

   this.drizzleScale = () =>
   {
      if ( this.drizzleData != undefined && this.drizzleData.scale != undefined )
         return this.drizzleData.scale;
      return DEFAULT_DRIZZLE_SCALE; // default
   };

   this.drizzleDropShrink = () =>
   {
      if ( this.drizzleData != undefined && this.drizzleData.dropShrink != undefined )
         return this.drizzleData.dropShrink;
      return this.isCFA ? DEFAULT_DRIZZLE_DROP_SHRINK_CFA : DEFAULT_DRIZZLE_DROP_SHRINK_MONO; // default
   };

   this.drizzleFunction = () =>
   {
      if ( this.drizzleData != undefined && this.drizzleData.function != undefined )
         return this.drizzleData.function;
      return DEFAULT_DRIZZLE_FUNCTION; // default
   };

   this.drizzleGridSize = () =>
   {
      if ( this.drizzleData != undefined && this.drizzleData.gridSize != undefined )
         return this.drizzleData.gridSize;
      return DEFAULT_DRIZZLE_GRID_SIZE; // default
   };

   this.drizzledFrameSize = () =>
   {
      return this.frameSize() * this.drizzleScale() * this.drizzleScale();
   }

   // ------ END DRIZZLE MANAGEMENT

   // ------ MASTER FILE MANAGEMENT
   /**
    * Stores the master file name for the group given the master type and the variant.
    * @param {String} name the name of the master light to associate
    * @param {WBPPMasterType} type enum, the type of master file, can be LIGHT, DRIZZLE or RECOMBINED
    * @param {WBPPMasterVariant} variant enum, the master variant, can be REGULAR or CROPPED
    */
   this.setMasterFileName = ( name, type, variant ) =>
   {
      if ( type == undefined )
         type = WBPPMasterType.MASTER_LIGHT;
      if ( variant == undefined )
         variant = WBPPMasterVariant.REGULAR;

      this.masterFiles[ type + "_" + variant ] = name;
   };

   /**
    *  Returns the master file name assocaited to the given type and variant
    * @param WBPPMasterType} type enum, the type of master file, can be LIGHT, DRIZZLE or RECOMBINED
    * @param {WBPPMasterVariant} variant enum, the master variant, can be REGULAR or CROPPED
    * @returns
    */
   this.getMasterFileName = ( type, variant ) =>
   {
      if ( type == undefined )
         type = WBPPMasterType.MASTER_LIGHT;
      if ( variant == undefined )
         variant = WBPPMasterVariant.REGULAR;
      return this.masterFiles[ type + "_" + variant ];
   }

   // ------ END MASTER FILE MANAGEMENT

   // ------ OVERSCAN  MANAGEMENT

   /**
    * Returns true if the group contains a master file with the overscan information.
    *
    */
   this.hasOverscanInfo = () =>
   {
      return this.hasMaster && this.fileItems[ 0 ].overscan.enabled && this.fileItems[ 0 ].overscan.hasOverscanRegions();
   }

   this.getOverscanDescription = () =>
   {
      if ( this.hasMaster )
         return this.fileItems[ 0 ].overscan.toString();
      return "";
   }

   this.getOverscanInfo = () =>
   {
      if ( this.hasMaster )
         return this.fileItems[ 0 ].overscan;
      return undefined;
   }

   // ------ END OVERSCAN  MANAGEMENT

   /**
    * Compute the total integration time for the group.
    *
    * @param {Boolean} onlyActiveFrames true if total time takes into account only active frames
    * @returns total exposure as numerica value in seconds.
    */
   this.totalExposureTime = ( onlyActiveFrames ) =>
   {
      return ( onlyActiveFrames ? this.activeFrames() : this.fileItems ).reduce( ( acc, fileItem ) => ( acc + fileItem.exposureTime ), 0 );
   };

   /**
    * Merge the keywords object into the group's keywords object filtering only
    * the keywords enabled for the group's mode.
    *
    * @param {{String:String}} keywords the keywords key-value map
    */
   this.mergeKeywords = function( keywords )
   {
      Object.keys( keywords ).forEach( k =>
      {
         this.keywords[ k ] = keywords[ k ];
      } );
      this.keywords = engine.keywords.filterKeywordsForMode( this.keywords, mode );
   };

   // MANUAL OVERRIDES

   this.forceNoDark = false;
   this.forceNoFlat = false;
   this.overrideDark = undefined;
   this.overrideFlat = undefined;

   if ( firstItem )
   {
      // we pass null from importParameters()
      this.fileItems.push( firstItem );
      this.mergeKeywords( firstItem.keywords );
      // the group takes the size of the first item
      this.size = firstItem.matchingSizes[ this.mode ];
   }

   //

   /**
    * Returns the group size string.
    *
    * @return {*} "width"x"height" string
    */
   this.sizeString = () =>
   {
      return this.size.width + "x" + this.size.height;
   };

   //

   /**
    * Generates the group ID.
    *
    * @returns
    */
   this.generateID = () =>
   {
      // WBPP 2.0.2 - do not change!
      let pre_id_items = [
         this.imageType,
         this.binning,
         this.filter,
         this.exposureTime,
         this.colorSpaceToString()
      ];

      // WBPP 2.1 and above
      // NB: **** migration to be update in case of changes! ****
      let post_id_items = [
         this.mode
      ];

      // WBPP 2.2.0
      post_id_items.push( this.associatedRGBchannel || "none" );

      // WBPP 2.4.2
      post_id_items.push( this.sizeString() );

      let ID = pre_id_items.join( "_" )
      ID = ID + "_" + Object.keys( this.keywords ).map( k => k + ":" + this.keywords[ k ] ).join( "_" );
      ID = ID + "_" + post_id_items.join( "_" )

      return ID;
   };

   /**
    * Returns the list of active items objects in the group.An active item is a frame that
    * has not yet been processed or has been successfully processed so far.
    *
    * @param {*} associatedID optionally defines a custom associated ID
    * @returns
    */
   this.activeFrames = ( associatedID ) =>
   {
      // returns the frames that have been processes succesfully, taking into account the
      // associated id to the separated RGB groups, if set to the group or if provided directly
      let id = "default";
      if ( this.isVirtual() )
      {
         id = this.associatedRGBchannel;
      }
      if ( associatedID && associatedID.length > 0 )
      {
         id = associatedID;
      }
      return this.fileItems.filter( item => !item.failed( id ) ).map( item =>
      {
         return new ActiveFrame( item, id );
      } );
   };

   /**
    * Determines if the group matches the provided parameters.
    *
    * @param {ImageType} imageType
    * @param {String} filter
    * @param {Number} binning
    * @param {Number} exposureTime
    * @param {Object} size {width:Numeric, height:Numeric}
    * @param {Boolean} isCFA
    * @param {Number} exposureTolerance
    * @param {Number} lightExposureTolerance
    * @param {WBPPGroupingMode} mode
    * @returns
    */
   this.sameParameters = ( imageType, filter, binning, exposureTime, size, isCFA, exposureTolerance, lightExposureTolerance, mode ) =>
   {
      if ( this.imageType != imageType || this.binning != binning || this.mode != mode )
         return false;

      // if group has a size and a size is provided then they must match
      if ( this.size.width != size.width )
         return false;
      if ( this.size.height != size.height )
         return false;

      switch ( imageType )
      {
         case ImageType.BIAS:
            return true
         case ImageType.DARK:
            return ( Math.abs( this.exposureTime - exposureTime ) <= Math.max( CONST_MIN_EXPOSURE_TOLERANCE, exposureTolerance ) );
         case ImageType.FLAT:
         case ImageType.UNKNOWN:
            return this.filter == filter;
         case ImageType.LIGHT:
            return this.isCFA == isCFA && this.filter == filter && Math.abs( this.exposureTime - exposureTime ) <= lightExposureTolerance;
      }
      return false;
   };

   /**
    * Returns the number of matches between the values of the current group keywords and
    * the values of the keywords object provided. If a mismatch occurs( values are different )
    * then -1 is returned otherwise the count of the matches is returend.
    * If keywordsMatchCount is true then return -1 if the group contains a keyword is not in the
    * matching keywords OR if keywords contain a keyword that is not in the group
    *
    * @param {{String:String}} keywords the keywords key-value map
    * @param {Boolean} strictDirectMatching skip a group if it has a keyword that is not provided
    * @param {Boolean} strictInverseMatching skip a group if it does not have a value for a given keyword
    *
    * @returns the number of matching keywords
    */
   this.keywordsMatchCount = function( keywords, strictDirectMatching, strictInverseMatching )
   {
      // scan the list of group's keywords and cound the number of keywords
      let keys = Object.keys( this.keywords );
      let count = 0;
      for ( let i = 0; i < keys.length; i++ )
      {
         let key = keys[ i ];
         if ( keywords[ key ] )
         {
            if ( this.keywords[ key ] == keywords[ key ] )
               count++;
            else
               return -1;
         }
         else if ( strictDirectMatching )
         {
            // the group contains a keyword that is not in the provided keywords, in this case
            // the group does not strict match
            return -1;
         }
      }

      if ( strictInverseMatching )
      {
         // do the inverse check i.e. check if the list of provided keywords contains a keyword
         // that is not in the group, ni that case the group is not strictly matching
         let kw = Object.keys( keywords );
         for ( let i = 0; i < kw.length; i++ )
            if ( !this.keywords[ kw[ i ] ] )
               return -1;
      }
      return count;
   };

   /**
    * Adds a new file item to the group.
    *
    * @param {FileItem} item
    * @param {Boolean} ignoreCFAMatching
    * @returns on success { success: true }, {sucecss: false, message: string} otherwise
    */
   this.addFileItem = ( item, ignoreCFAMatching ) =>
   {
      // a concrete file item cannot be added to a virtual group
      if ( this.isVirtual() )
      {
         return {
            success: false,
            message: "Attempt to add file " + item.filePath + " to the virtual group " +
               this.toString() + ", operation will be ignored."
         }
      }

      // image type and group CFA must be compatible
      if ( this.imageType == ImageType.LIGHT && !ignoreCFAMatching && item.isCFA != this.isCFA )
      {
         return {
            success: false,
            message: "File " + item.filePath + " has a different CFA with respect " +
               "the CFA of group " + this.toString() + " so it will be discarded."
         }
      }

      // set the file item size to the group if this is the first item added
      if ( this.fileItems.length == 0 )
         this.size = item.matchingSizes[ this.mode ];

      // add the file, if this is a new master then the exposure time is set to the master exposure time
      if ( item.isMaster && !this.hasMaster )
      {

         this.fileItems.unshift( item );
         this.hasMaster = true;
         this.setExposureTime( item.exposureTime );
      }
      else
      {
         this.fileItems.push( item );
         this.addExposureTime( item.exposureTime );
      }

      return {
         success: true
      }
   };

   /**
    * Removes a file item at index i.
    *
    * @param {Number} i
    */
   this.removeItem = ( i ) =>
   {
      this.fileItems.splice( i, 1 );
      // if the new on-top element is a master then update the group accordingly
      if ( this.fileItems.length > 0 )
      {
         if ( this.fileItems[ 0 ] )
            this.hasMaster = this.fileItems[ 0 ].isMaster;
      }
      else
      {
         this.hasMaster = false;
      }
   };

   //
   /**
    * Checks if the provided rejection type is good for the group.
    *
    * @param {*} rejection
    * @returns
    */
   this.rejectionIsGood = function( rejection )
   {
      if ( rejection == ImageIntegration.prototype.auto )
         return [ true, "" ];

      // Invariants
      switch ( rejection )
      {
         case ImageIntegration.prototype.NoRejection:
            return [ false, "No pixel rejection algorithm has been selected" ];
         case ImageIntegration.prototype.MinMax:
            return [ false, "Min/Max rejection should not be used for production work" ];
         case ImageIntegration.prototype.CCDClip:
            return [ false, "CCD clipping rejection has been deprecated" ];
         default:
            break;
      }
      let selectedRejection = ( rejection !== ImageIntegration.prototype.auto ) ? rejection : this.bestRejectionMethod();

      // Selections dependent on the number of frames
      let n = this.fileItems.length;
      switch ( selectedRejection )
      {
         case ImageIntegration.prototype.PercentileClip:
            if ( n > 8 )
               return [ false, "Percentile clipping should only be used for small sets of eight or less images" ];
            break;
         case ImageIntegration.prototype.SigmaClip:
            if ( n < 8 )
               return [ false, "Sigma clipping requires at least 8 images to provide minimally reliable results; consider using percentile clipping" ];
            if ( n > 15 )
               return [ false, "Winsorized sigma clipping will work better than sigma clipping for sets of 15 or more images" ];
            break;
         case ImageIntegration.prototype.WinsorizedSigmaClip:
            if ( n < 8 )
               return [ false, "Winsorized sigma clipping requires at least 8 images to provide minimally reliable results; consider using percentile clipping" ];
            break;
         case ImageIntegration.prototype.AveragedSigmaClip:
            if ( n < 8 )
               return [ false, "Averaged sigma clipping requires at least 8 images to provide minimally reliable results; consider using percentile clipping for less than 8 frames" ];
            if ( n > 10 )
               return [ false, "Sigma clipping or Winsorized sigma clipping will work better than averaged sigma clipping for sets of 10 or more images" ];
            break;
         case ImageIntegration.prototype.LinearFit:
            if ( n < 8 )
               return [ false, "Linear fit clipping requires at least 15 images to provide reliable results; consider using percentile clipping for less than 8 frames" ];
            if ( n < 20 )
               return [ false, "Linear fit clipping may not be better than Winsorized sigma clipping for sets of less than 15-20 images" ];
            break;
         case ImageIntegration.prototype.Rejection_ESD:
            if ( n < 8 )
               return [ false, "ESD requires at least 15 images to provide reliable results; consider using percentile clipping for less than 8 frames" ];
            if ( n < 20 )
               return [ false, "ESD may not be better than Winsorized sigma clipping for sets of less than 20 images" ];
            if ( n < 25 )
               return [ false, "ESD  may not be better than linear fit clipping for sets of less than 20-25 images" ];
         case ImageIntegration.prototype.Rejection_RCR:
            if ( n < 15 )
               return [ false, "RCR requires at least 15 images to provide reliable results; consider using percentile clipping for less than 8 frames" ];
         default: // ?!
            break;
      }

      return [ true, "" ];
   };

   /**
    * Returns the best rejection method for the group.
    * NOTE: the best rejection is based on the current number of active frames.
    *
    * @returns
    */
   this.bestRejectionMethod = function()
   {
      let n = this.activeFrames().length;
      if ( n < 6 )
         return ImageIntegration.prototype.PercentileClip;
      if ( n <= 15 || this.imageType == ImageType.BIAS || this.imageType == ImageType.DARK )
         return ImageIntegration.prototype.WinsorizedSigmaClip;
      return ImageIntegration.prototype.Rejection_ESD;
   };

   /**
    * Adds a new exposure time and update the exposure data accordingly.
    *
    * @param {Number} time
    */
   this.addExposureTime = function( time )
   {
      // check exposure with tolerance
      let hasExposure = false;
      for ( let i = 0; i < this.exposureTimes.length; i++ )
      {
         if ( Math.abs( time - this.exposureTimes[ i ] ) < CONST_MIN_EXPOSURE_TOLERANCE )
         {
            hasExposure = true;
            break;
         }
      }
      if ( !hasExposure )
      {
         this.exposureTimes.push( time );
         this.exposureTimes.sort( ( a, b ) => a > b )
      }
   };

   /**
    * Assign the nominal exposure time of the group.
    *
    * @param {Number} time
    */
   this.setExposureTime = function( time )
   {
      let sanitizedValue = ( this.imageType != ImageType.BIAS ) ? time : 0;
      this.exposureTimes = [ sanitizedValue ];
      this.exposureTime = sanitizedValue;
   };

   /**
    * Returns the formatted nominal exposure string.
    *
    * @returns
    */
   this.exposureToString = function()
   {
      return format( "%.2fs", this.exposureTime );
   };

   /**
    * Returns the formatted frames exposures string.
    *
    * @returns
    */
   this.exposuresToString = function()
   {
      if ( this.exposureTimes.length > 1 )
         return '[' + this.exposureTimes.map( exposure => format( "%.2fs", exposure ) ).join( ', ' ) + ']';
      return this.exposureToString();
   };

   /**
    * Returns an extended formatted string describing the nominal exposure plus the list of all the different
    * exposures of the frames in the group.
    *
    * @returns
    */
   this.exposuresToExtendedString = function()
   {
      if ( this.exposureTimes.length > 1 )
         return format( "%.2fs", this.exposureTimes[ this.exposureTimes.length - 1 ] ) +
            ' - [' + this.exposureTimes.map( ( exposure ) => format( "%.2fs", exposure ) ).join( ', ' ) + ']';
      return format( "%.2fs", this.exposureTime );
   };

   /**
    * Returns an extended formatted string describing the nominal exposure plus the list of all the different
    * exposures of the frames in the group.
    *
    * @returns
    */
   this.exposuresMinMaxRangeString = function()
   {
      let min = Math.min.apply( null, this.exposureTimes );
      let max = Math.max.apply( null, this.exposureTimes );
      if ( ( max - min ) < 1 )
         return format( "%.2fs", this.exposureTime );
      else
         return "[ " + this.exposureTimes.map( ( exposure ) => format( "%.2fs", exposure ) ).join( ', ' ) + " ]";
   };

   /**
    * Returns a short string representing the group color space
    *
    */
   this.colorSpaceToString = () =>
   {
      if ( this.isCFA )
      {
         if ( this.mode == WBPPGroupingMode.PRE )
            return "CFA";
         else
            return "RGB";
      }
      else return "mono";
   };

   /**
    * Injects the group data into the given object.
    *
    * @param {*} object
    */
   this.injectGroupData = ( object ) =>
   {
      object.group = {
         size: this.sizeString(),
         binnig: this.binning,
         filter: this.filter.length > 0 ? this.filter : 'NoFilter',
         exposure: this.exposure,
         color: this.colorSpaceToString(),
         mode: this.modeToString(),
         channel: this.associatedRGBchannel,
         keywords: this.keywords,
         frames: this.activeFrames()
      }

      return object;
   };

   /**
    * Logs the group information to eh console.
    */
   this.log = function()
   {
      // the number of frames is reported only if this is not an RGB combined image
      if ( this.associatedRGBchannel != WBPPAssociatedChannel.COMBINED_RGB )
         console.noteln( 'Group of ', this.fileItems.length, ' ', StackEngine.imageTypeToString( this.imageType ), ' frames (', this.activeFrames().length, ' active)' );
      console.noteln( 'SIZE  : ', this.sizeString() );
      console.noteln( 'BINNING  : ', this.binning );
      console.noteln( 'Filter   : ', this.filter.length > 0 ? this.filter : 'NoFilter' );
      console.noteln( 'Exposure : ', this.exposuresToString() );
      console.noteln( 'Keywords : [', this.keywordsToString(), ']' );
      console.noteln( 'Mode     : ', this.modeToString() );
      if ( this.imageType == ImageType.FLAT || this.imageType == ImageType.LIGHT )
         console.noteln( 'Color   : ', this.colorSpaceToString() );
      if ( this.associatedRGBchannel != null )
         console.noteln( 'Channel : ', this.associatedRGBchannel );
   };

   /**
    * Returns a rich text string to be the header of a group section in the process logged output.
    *
    * @param {String} title custom header title
    * @returns
    */
   this.logStringHeader = ( title ) =>
   {
      let activeCount = this.activeFrames().length;
      let header = '<b>********************</b> <i>' + title + '</i> <b>********************';
      this.footerLengthForCurrentHeader = header.length - '<b></b><i></i><b>'.length;
      let str = '<b>' + header + '\n';
      // the number of frames is reported only if this is not an RGB combined image
      if ( this.associatedRGBchannel != WBPPAssociatedChannel.COMBINED_RGB )
         str += 'Group of ' + this.fileItems.length + ' ' + StackEngine.imageTypeToString( this.imageType ) + ' frames (' + activeCount + ' active)\n';
      str += 'SIZE  : ' + this.sizeString();
      str += '\n' + 'BINNING  : ' + this.binning;
      if ( this.imageType != ImageType.BIAS && this.imageType != ImageType.DARK )
         str += '\n' + 'Filter   : ' + ( this.filter.length > 0 ? this.filter : 'NoFilter' );
      if ( this.imageType != ImageType.BIAS )
         str += '\n' + 'Exposure : ' + this.exposuresToString();
      str += '\n' + 'Keywords : [' + this.keywordsToString() + ']';
      str += '\n' + 'Mode     : ' + this.modeToString();
      if ( this.imageType == ImageType.FLAT || this.imageType == ImageType.LIGHT )
         str += '\n' + 'Color    : ' + this.colorSpaceToString();
      if ( this.associatedRGBchannel != null )
         str += '\n' + 'Channel  : ' + this.associatedRGBchannel;
      str += "</b>\n"
      return str;
   };

   /**
    * Returns a rich text string to be the footer of a group section in the process logger output.
    *
    * @returns
    */
   this.logStringFooter = function()
   {
      return '<b>' + '*'.repeat( this.footerLengthForCurrentHeader ) + '</b>\n';
   };

   /**
    * Returns a single line group information string.
    *
    * @returns
    */
   this.toString = function()
   {
      let a = [];
      a.push( "size = " + this.sizeString() );
      if ( !isEmptyString( this.filter ) )
         a.push( "filter = " + this.filter );
      else
         a.push( "filter = NoFilter" );
      a.push( "binning = " + this.binning.toString() );
      if ( this.exposureTimes.length == 1 )
         a.push( format( "exposure = %.2fs", this.exposureTime ) );
      else if ( this.exposureTimes.length > 1 )
         a.push( 'exposures = ', this.exposuresToString() );
      a.push( 'keywords = [' + this.keywordsToString() + "]" );
      a.push( 'mode = ' + this.modeToString() );
      if ( this.associatedRGBchannel != null )
         a.push( 'channel = ' + this.associatedRGBchannel );
      // the number of frames is reported only if this is not an RGB combined image
      if ( this.associatedRGBchannel != WBPPAssociatedChannel.COMBINED_RGB )
      {
         let activeCount = this.activeFrames().length;
         a.push( "frames = " + this.fileItems.length.toString() + " (" + activeCount + " active)" );
      }

      let s = StackEngine.imageTypeToString( this.imageType ) + " frames ";
      s += a[ 0 ];
      for ( let i = 1; i < a.length; ++i )
         s += ", " + a[ i ];
      return s;
   };

   this.toShortString = function( withActiveFrames, reportFileItems )
   {
      if ( withActiveFrames == undefined )
         withActiveFrames = true;
      if ( reportFileItems == undefined )
         reportFileItems = false;
      let a = [];
      a.push( this.sizeString() );
      if ( !isEmptyString( this.filter ) )
         a.push( this.filter );
      else
         a.push( "NoFilter" );
      a.push( this.colorSpaceToString() )
      a.push( this.binning.toString() + "x" + this.binning.toString() );
      if ( this.exposureTimes.length == 1 )
         a.push( format( "%.2fs", this.exposureTime ) );
      else if ( this.exposureTimes.length > 1 )
         a.push( this.exposuresToString() );
      let kw = this.keywordsToString();
      if ( kw.length > 0 )
         a.push( '[' + kw + "]" );
      if ( this.associatedRGBchannel != null )
         a.push( this.associatedRGBchannel );

      // the number of frames is reported only if this is not an RGB combined image
      let s = "";
      if ( withActiveFrames && this.associatedRGBchannel != WBPPAssociatedChannel.COMBINED_RGB )
      {
         let activeCount = this.activeFrames().length;
         a.push( "frames = " + this.fileItems.length.toString() + " (" + activeCount + " active)" );
      }
      if ( reportFileItems && this.associatedRGBchannel != WBPPAssociatedChannel.COMBINED_RGB )
         s = this.fileItems.length + " ";
      s += StackEngine.imageTypeToString( this.imageType ) + " frames, ";
      s += a[ 0 ];
      for ( let i = 1; i < a.length; ++i )
         s += ", " + a[ i ];
      if ( !s.endsWith( ")" ) )
         s += ")";
      return s;
   };

   /**
    * Returns a very short group information string to be used in a dropdown list.
    *
    * @returns
    */
   this.dropdownShortString = function()
   {
      let retVal = "";
      switch ( this.imageType )
      {
         case ImageType.BIAS:
            retVal = "BIN " + this.binning;
            break;
         case ImageType.DARK:
            retVal = "BIN " + this.binning + "   " + this.exposureToString();
            break;
         case ImageType.FLAT:
         case ImageType.LIGHT:
            retVal = this.filter + "  BIN " + this.binning + "  " + this.exposureToString();
            break;
      }
      return retVal;
   };

   /**
    * Returns the group's CFA pattern string.
    *
    * @returns
    */
   this.CFAPatternString = function()
   {
      let patterns = [ "Auto", "RGGB", "BGGR", "GBRG", "GRBG", "GRGB", "GBGR", "RGBG", "BGRG" ];
      return patterns[ this.CFAPattern ];
   };

   /**
    * Returns the group's debayer method string.
    *
    * @returns
    */
   this.debayerMethodString = function()
   {
      let methods = [ "SuperPixel", "Bilinear", "VNG" ];
      return methods[ this.debayerMethod ];
   };

   /**
    * Returns a string listing all the group's keywords and values.
    * The list follows the global keywords definition order.
    *
    * @param {String} nameValueSeparator separator string between keyword name and value
    * @param {String} blockSeparator sparator string between different key/values
    * @returns
    */
   this.keywordsToString = ( nameValueSeparator, blockSeparator ) =>
   {
      let keywordsString = "";
      let separator = "";
      nameValueSeparator = nameValueSeparator || ": ";
      blockSeparator = blockSeparator || ", ";

      engine.keywords.names().forEach( name =>
      {
         if ( this.keywords[ name ] )
         {
            keywordsString += separator + name + nameValueSeparator + this.keywords[ name ];
            separator = blockSeparator;
         }
      } );
      return keywordsString;
   };

   /**
    * Returns a string representing a unique folder name to be used for this group.
    *
    */
   this.folderName = ( sanitized ) =>
   {
      let
      {
         cleanFilterName
      } = WBPPUtils.shared();

      sanitized = sanitized != undefined ? sanitized : true;

      let imgType = StackEngine.imageTypeToString( this.imageType );
      let binning = "BIN-" + this.binning;
      let size = this.sizeString();
      let exposure = "EXPOSURE-" + format( "%.2f", this.exposureTime ) + "s";
      let filter = "FILTER-" + ( isEmptyString( this.filter ) ? "NoFilter" : cleanFilterName( this.filter ) );
      let colorSpace = this.colorSpaceToString();
      let keywordsPostFix = this.keywordsToString( "-" /* name/value separator */ , "_" /* block separator */ );

      let infos = [];
      switch ( this.imageType )
      {
         case ImageType.BIAS:
            infos = [ imgType, binning, size ];
            break;
         case ImageType.DARK:
            infos = [ imgType, binning, size, exposure ];
            break;
         case ImageType.FLAT:
            infos = [ imgType, binning, size, filter, colorSpace ];
            break;
         case ImageType.LIGHT:
            if ( this.associatedRGBchannel )
            {
               infos = [ imgType, binning, size, exposure, filter, this.associatedRGBchannel ];
            }
            else
            {
               infos = [ imgType, binning, size, exposure, filter, colorSpace ];
            }
            break;
      }

      let fname = infos.join( "_" );
      if ( keywordsPostFix.length > 0 )
         fname = fname + "_" + keywordsPostFix;

      // sanitize the file name
      if ( sanitized )
      {
         let sanitizeRegExp = new RegExp( "[^" + FOLDER_NAME_CHARSET + "]", "gi" );
         let sanitizedFName = fname.replace( sanitizeRegExp, '-' );
         console.writeln( "Sanitized group folder name: [", fname, "] -> [", sanitizedFName, "]" );
         return sanitizedFName;
      }
      else
      {
         return fname;
      }
   };

   /**
    * Returns a string describing the group's mode.
    *
    * @returns
    */
   this.modeToString = () =>
   {
      switch ( this.mode )
      {
         case WBPPGroupingMode.PRE:
            return "calibration";
         case WBPPGroupingMode.POST:
            return "post-calibration";
      }
      return "-";
   };

   /**
    * Returns the sie of a single frame in the group
    *
    */
   this.frameSize = () =>
   {
      if ( this.mode == WBPPGroupingMode.PRE )
      {
         // in PRE all images are mono
         return this.size.width * this.size.height * 4;
      }
      else
      {
         // in POST images are mono if isCFA is false
         return this.size.width * this.size.height * 4 * ( this.isCFA ? 3 : 1 );
      }
   };

   /**
    * Returns the estimation of the size of the group after a processing
    *
    * @return {*}
    */
   this.groupSize = () =>
   {
      return this.fileItems.length * this.frameSize();
   };

   /**
    * Performs all the group's sanity checks and returns an object optionally containing the warnings or errors found.
    *
    * @returns {Object} an empty object on success, { warnings?: String, errors?: String } otherwise
    */
   this.status = function()
   {
      if ( this.hasMaster && this.mode == WBPPGroupingMode.PRE )
      {
         return {};
      }

      let statusString = "";

      if ( this.mode == WBPPGroupingMode.PRE )
      {

         // CALIBRATION GROUP CHECKS

         let
         {
            validCCIconName
         } = WBPPUtils.shared();

         // get the calibration groups
         let cg = engine.getCalibrationGroupsFor( this );

         // ERROR CHECK: LIGHT
         if ( this.imageType == ImageType.LIGHT )
         {
            // check if for any reason a master flat has different isCFA value than light group
            if ( cg.masterFlat && cg.masterFlat.isCFA != this.isCFA )
            {
               if ( this.isCFA )
                  statusString +=
                  "<p><b>Light frames are marked as CFA images but the Master Flat is not.</b><br>" +
                  "<i>Light frames mosaiced with a CFA pattern cannot be calibrated using a monochromatic Master Flat.</i><p>"
               else
                  statusString +=
                  "<p><b>Light frames are marked as monochromatic but the Master Flat is marked as a colorized CFA image.</b><br>" +
                  "<i>Monochromatic Light frames must be calibrated with a monochromatic Master Flat.</i><p>"
            }
         }

         // Returns errors immediately
         if ( statusString.length > 0 )
            return {
               errors: statusString
            };

         // CHECK: calibration files for FLAT
         if ( this.imageType == ImageType.FLAT )
            if ( !cg.masterBias && !cg.masterDark )
               statusString +=
               "<p><b>Neither a Master Bias nor a Master Dark matches.</b><br>" +
               "<i>Flat frames will be integrated without being calibrated.</i><p>"

         // CHECK: calibration files for LIGHT
         if ( this.imageType == ImageType.LIGHT )
            if ( !cg.masterBias && !cg.masterDark )
            {
               if ( !cg.masterFlat )
                  statusString +=
                  "<p><b>No matching Master Bias, Master Dark and Master Flat found.</b><br>" +
                  "<i>Light frames will be integrated without being calibrated.</i><p>"
               else
                  statusString +=
                  "<p><b>Neither Bias nor Dark master file matches.</b><br>" +
                  "<i>Light frames will not be bias and/or dark subtracted before being calibrated by the Master Flat. This configuration may not completely calibrate Light frames.</i><p>"
            }

         // CHECK: current group is FLAT or LIGHT
         if ( this.imageType == ImageType.FLAT || this.imageType == ImageType.LIGHT )
         {
            // dark is optimized but bias is missing
            if ( !cg.masterBias && cg.masterDark && this.optimizeMasterDark )
               statusString +=
               "<p><b>Master Dark will be optimized but no Master Bias matches.</b><br>" +
               "<i>This configuration should be avoided since the optimization should be performed on a bias-subtracted Master Dark.</i><p>"

            // dark has different exposure time and it's not optimized
            if ( cg.masterDark )
            {
               let dT = Math.abs( cg.masterDark.exposureTime - this.exposureTime );
               if ( !this.optimizeMasterDark && dT > 5 )
               {
                  // main sentence
                  statusString +=
                     "<p><b>" + StackEngine.imageTypeToString( this.imageType ) + " frame's exposure differs from the Master Dark's exposure.</b><br>";
                  // suggestion:
                  if ( this.imageType == ImageType.FLAT )
                  {
                     statusString += "<i>You can disable the use \"Dark\" checkbox and this will subtract the Master Bias only.</i><p>"
                  }
                  else if ( this.imageType == ImageType.LIGHT )
                  {
                     if ( cg.masterBias )
                        statusString += "<i>You can optimize the Master Dark in order to achieve a better dark current estimation.</i><p>"
                     else
                        statusString += "<i>You can add a compatible Master Bias and optimize the Master Dark in order to achieve a better dark current estimation.</i><p>"
                  }
               }
            }
         }

         // CHECK: Cosmetic Correction icon name
         if ( this.imageType == ImageType.LIGHT && this.CCTemplate && this.CCTemplate.length > 0 )
         {
            if ( !validCCIconName( this.CCTemplate ) )
               statusString +=
               "<p><b>Reference to a non-existent Cosmetic Correction process icon name " + this.CCTemplate + ".</b><br>" +
               "<i>Ensure that the specified Cosmetic Correction proceess icon exists otherwise Cosmetic Correction will not be performed. " +
               "Alternatively, you may remove the reference.</i><p>"
         }
      }
      else
      {
         // POST CALIBRATION GROUP CHECKS

         // check if group is drizzied but registration or image integration is not enabled
         if ( this.isDrizzleEnabled() && ( !engine.imageRegistration || !engine.integrate ) )
         {
            statusString += "<p>Drizzle Integration requires Image Registration and Image Integration to be enabled.</p>";
         }
      }

      return statusString.length > 0 ?
      {
         warnings: statusString
      } :
      {};
   };

   // initialize the group ID and set the defaults
   this.id = this.generateID();
   this.setDrizzleDataDefaults();
   this.setDrizzleData( drizzleData );
}

FrameGroup.prototype = new Object;

// ----------------------------------------------------------------------------
/**
 * This object is responsible of managing multiple grouping startegies. In the current implementation only two
 * kind of grouping are implemented: pre-processing and post-processing grouping.
 * Any file should be added using this manager, in such way the file is added to all managed groupings in order
 * to quickly switch between the two later.
 * Any time the list of groups for a grouping strategy is needed you just need to use the groupsForMode function
 * providing the grouping mode and it will return the corresponding list of groups.
 */
function FrameGroupsManager()
{
   // groups
   this.groups = [];
   this.cache = {};

   /**
    * Initialize all data for processing
    *
    */
   this.initializeProcessing = () =>
   {
      this.groups.forEach( group =>
      {
         group.initForProcessing();
         group.fileItems.forEach( item => item.initForProcessing() );
      } );
   };

   /**
    * Adds a new group with the given properties.
    *
    * @param {ImageType} imageType
    * @param {String} filter
    * @param {Number} binning
    * @param {Number} exposureTime
    * @param {Object} size {width:Numeric, height:Numeric}
    * @param {Boolean} isCFA
    * @param {String} fileItem
    * @param {Boolean} isMaster
    * @param {{String:String}?} itemKeywords
    * @param {WBPPGroupingMode} mode
    */
   this.addGroup = ( imageType, filter, binning, exposureTime, size, isCFA, fileItem, isMaster, keywords, mode ) =>
   {
      // append the new group
      this.groups.push( new FrameGroup(
         imageType,
         filter,
         binning,
         exposureTime,
         size,
         isCFA,
         fileItem,
         isMaster,
         keywords,
         mode ) );
   };

   /**
    * Adds a new file item. customKeywoeds and customModes are used by Linear
    * Pattern Subtraction in order to regroup frames by binning, exposure and filter name
    * ignoring the keywords and keep the new generated groups separated form the pre and post
    * groups.
    *
    * @param {FileItem} fileItem
    * @param {*} customKeywords
    * @param {*} customModes
    * @param {*} customCFA
    * @returns
    */
   this.addFileItem = ( fileItem, customKeywords, customModes, customCFA ) =>
   {
      // only light frames are added to both pre and post process modes. Bias, Dark and Flat are added only to pre-process mode.
      let modes = customModes || ( fileItem.imageType == ImageType.LIGHT ? [ WBPPGroupingMode.PRE, WBPPGroupingMode.POST ] : [ WBPPGroupingMode.PRE ] );
      let isCFA = customCFA == undefined ? fileItem.isCFA : customCFA;

      // for all modes iterate and add the file
      modes.forEach( mode =>
      {
         let size = fileItem.matchingSizes[ mode ];

         let group = engine.findGroup(
            fileItem.imageType,
            fileItem.filter,
            fileItem.binning,
            fileItem.exposureTime,
            size,
            fileItem.isMaster,
            isCFA,
            engine.darkExposureTolerance,
            mode == WBPPGroupingMode.PRE ? engine.lightExposureTolerance : engine.lightExposureTolerancePost,
            customKeywords || fileItem.keywords,
            true, /* strictKeywordsMatching */
            mode
         );

         // add the item if a group has been found, create a new group otherwise
         if ( group )
            group.addFileItem( fileItem, true /* ignoreCFAMatching */ );
         else
            this.addGroup(
               fileItem.imageType,
               fileItem.filter,
               fileItem.binning,
               fileItem.exposureTime,
               size,
               isCFA,
               fileItem,
               fileItem.isMaster,
               customKeywords || fileItem.keywords,
               mode );
      } );

      return {
         success: true
      };
   };

   /**
    * Returns the group with the provided id.
    *
    * @param {String} id the id that the returned group has
    * @returns the group with the provided id
    */
   this.getGroupByID = ( id ) =>
   {
      let matching = this.groups.filter( g => g.id == id );
      return matching.length == 1 ? matching[ 0 ] : undefined
   };

   /**
    * Returns the list of group matching the given mode
    *
    * @param {WBPPKeywordMode} mode
    * @returns
    */
   this.groupsForMode = ( mode ) =>
   {
      return this.groups.filter( group =>
      {
         return group && group.mode == mode
      } )
   };

   /**
    * Returns the list of differnt binnings of the calibration groups.
    *
    * @returns an array of integers representing the differnet binnings of the calibration groups
    */
   this.binningsOfCalibrationGroups = () =>
   {
      let groups = this.groupsForMode( WBPPGroupingMode.PRE );
      return groups.reduce( ( acc, group ) =>
      {
         if ( group.imageType == ImageType.LIGHT && acc.indexOf( group.binning ) == -1 )
         {
            acc.push( group.binning );
         }
         return acc;
      }, [] );
   };

   /**
    * Removes a group given its index and mode.
    *
    * @param {Numeric} i group index
    */
   this.removeGroupAtIndex = ( i ) =>
   {

      // sanitize overrides i.e. clear any override that references the group
      if ( this.groups[ i ] && this.groups[ i ].mode == WBPPGroupingMode.PRE )
      {
         let id = this.groups[ i ].id;
         this.cache[ id ] = undefined;
         this.groups.forEach( group =>
         {
            if ( !group.overrideDark || group.overrideDark.id == id )
            {
               group.overrideDark = undefined;
            }
            if ( !group.overrideFlat || group.overrideFlat.id == id )
            {
               group.overrideFlat = undefined;
            }
         } );
      }

      // remove the group from the list
      this.groups.splice( i, 1 );
   };

   /**
    * Delete all groups of the given type and mode
    *
    * @param {ImageType} imageType
    */
   this.deleteFrameSet = ( imageType, mode ) =>
   {
      for ( let i = 0; i < this.groups.length; ++i )
         // use mode only if defined
         if ( this.groups[ i ].imageType == imageType && this.groups[ i ].mode == ( mode || this.groups[ i ].mode ) )
            this.removeGroupAtIndex( i-- );
   };

   /**
    * Removes all groups.
    *
    */
   this.clear = () =>
   {
      this.groups = [];
   };

   /**
    * Convenience checked for empty groups
    */
   this.isEmpty = () =>
   {
      return this.groups.length == 0;
   };

   /**
    * Clears the groups properties cache.
    *
    */
   this.clearCache = () =>
   {
      this.cache = {}
   };

   /**
    * Returns customizable options for all groups. The object returned has the group id
    * as value and an object containing the properties as value.
    *
    * @returns
    */
   this.cacheGroupsProperties = () =>
   {
      // store all cachable properties for each group by ID
      this.groups.forEach( group =>
      {
         if ( group && group.id ) // for compatibility with WBPP 1.x
         {
            this.cache[ group.id ] = {
               optimizeMasterDark: group.optimizeMasterDark,
               CCTemplate: group.CCTemplate,
               separateCFAFlatScalingFactors: group.separateCFAFlatScalingFactors,
               isCFA: group.isCFA,
               CFAPattern: group.CFAPattern,
               debayerMethod: group.debayerMethod,
               lightOutputPedestalMode: group.lightOutputPedestalMode,
               lightOutputPedestal: group.lightOutputPedestal,
               drizzleData: group.drizzleData,

               forceNoDark: group.forceNoDark,
               forceNoFlat: group.forceNoFlat,
               overrideDarkID: group.overrideDark && group.overrideDark.id,
               overrideFlatID: group.overrideFlat && group.overrideFlat.id
            };
         }
      } );
   };

   /**
    * Restores customizable groups options from an object. The object shoule be
    * generated by the getGroupsProperties function.
    *
    * @param {*} mode optional, the group mode to be restored
    */
   this.restoreGroupsPropertiesFromCache = ( mode ) =>
   {
      let groups = mode != undefined ? this.groupsForMode( mode ) : this.groups;
      groups.forEach( group =>
      {
         let properties = this.cache[ group.id ];
         if ( properties )
         {
            group.optimizeMasterDark = properties.optimizeMasterDark;
            group.CCTemplate = properties.CCTemplate;
            group.separateCFAFlatScalingFactors = properties.separateCFAFlatScalingFactors;
            group.isCFA = properties.isCFA;
            group.CFAPattern = properties.CFAPattern;
            group.debayerMethod = properties.debayerMethod;
            group.lightOutputPedestalMode = properties.lightOutputPedestalMode;
            group.lightOutputPedestal = properties.lightOutputPedestal;
            group.setDrizzleData( properties.drizzleData );

            group.forceNoDark = properties.forceNoDark;
            group.forceNoFlat = properties.forceNoFlat;
            group.overrideDark = this.getGroupByID( properties.overrideDarkID );
            group.overrideFlat = this.getGroupByID( properties.overrideFlatID );
         }
      } );
   };

   /**
    * Returns the total light frames integration time.
    *
    * @returns
    */
   this.totalIntegrationTime = () =>
   {
      return this.groupsForMode( WBPPGroupingMode.POST ).reduce( ( acc, group ) =>
      {
         // monochromatic groups always add
         if ( !group.isCFA && group.associatedRGBchannel == null )
         {
            return acc + group.totalExposureTime();
         }

         // if separate RGB channels only is active then we count only the _R channel
         if ( engine.debayerOutputMethod == WBPPDebayerOutputMode.SEPARATED && group.associatedRGBchannel == WBPPAssociatedChannel.R )
         {
            return acc + group.totalExposureTime();
         }

         // if combined RGB channels is active then we count only the RGB channel
         if ( engine.debayerOutputMethod != WBPPDebayerOutputMode.SEPARATED && group.associatedRGBchannel == null )
         {
            return acc + group.totalExposureTime();
         }

         // left the count unchanged
         return acc;

      }, 0 );
   };

   /**
    * Returns the whole list of file items in the session.
    *
    * @returns
    */
   this.allFileItems = () =>
   {
      return this.groupsForMode( WBPPGroupingMode.PRE ).reduce( ( acc, g ) =>
      {
         return acc.concat( g.fileItems );
      }, [] );
   };

   /**
    * Returns the fileItem in the session that matches the provided filePath.
    * If none or more than one item are found then returns undefined.
    *
    * @param {*} filePath
    */
   this.getReferenceFrameFileItem = ( filePath ) =>
   {
      let fileItem = this.allFileItems().filter( item => item.filePath == filePath );
      return fileItem[ 0 ] || undefined;
   };
}
FrameGroupsManager.prototype = new Object;

// ----------------------------------------------------------------------------

/**
 * Overscan region constructor
 *
 */
function OverscanRegions()
{
   this.__base__ = Object;
   this.__base__();

   this.enabled = false; // whether to apply this overscan correction
   this.sourceRect = new Rect( 0 ); // source overscan region
   this.targetRect = new Rect( 0 ); // image region to be corrected

   this.isValid = function()
   {
      if ( !this.enabled )
         return true;
      if ( !this.sourceRect.isNormal || !this.targetRect.isNormal )
         return false;
      if ( this.sourceRect.x0 < 0 || this.sourceRect.y0 < 0 ||
         this.targetRect.x0 < 0 || this.targetRect.y0 < 0 )
         return false;
      return true;
   };
}

OverscanRegions.prototype = new Object;

// ----------------------------------------------------------------------------

/**
 * Overscan object constructor
 *
 */
function Overscan()
{
   this.__base__ = Object;
   this.__base__();

   this.enabled = false; // whether overscan correction is globally enabled

   this.overscan = new Array; // four overscan source and target regions
   this.overscan.push( new OverscanRegions );
   this.overscan.push( new OverscanRegions );
   this.overscan.push( new OverscanRegions );
   this.overscan.push( new OverscanRegions );

   this.imageRect = new Rect( 0 ); // image region (i.e. the cropping rectangle)

   this.isValid = function()
   {
      if ( !this.enabled )
         return true;
      for ( let i = 0; i < 4; ++i )
         if ( !this.overscan[ i ].isValid() )
            return false;
      if ( !this.imageRect.isNormal )
         return false;
      if ( this.imageRect.x0 < 0 || this.imageRect.y0 < 0 )
         return false;
      return true;
   };

   this.hasOverscanRegions = function()
   {
      for ( let i = 0; i < 4; ++i )
         if ( this.overscan[ i ].enabled )
            return true;
      return false;
   };

   this.reset = function()
   {
      this.enabled = false;
      for ( let i = 0; i < 4; ++i )
         this.overscan[ i ].enabled = false;
   };

   this.updateWithKeyword = function( key, value )
   {
      let updateRect = function( rect, str, value )
      {
         switch ( str )
         {
            case "X0":
               rect.x0 = value;
               break;
            case "Y0":
               rect.y0 = value;
               break;
            case "X1":
               rect.x1 = value;
               break;
            case "Y1":
               rect.y1 = value;
               break;
         }
      };

      // quickly check of the keyword format
      if ( key.length != 7 )
         return;
      if ( key[ 0 ] != 'O' || key[ 1 ] != 'S' )
         return;
      if ( key[ 4 ] != '0' && key[ 4 ] != '1' )
         return;
      if ( key[ 5 ] != 'X' && key[ 5 ] != 'Y' )
         return;
      let intValue;
      let indx;
      let coord = key.substring( 5, 7 );
      try
      {
         intValue = parseInt( value );
         indx = parseInt( key.substring( 4, 5 ) );
      }
      catch ( _ )
      {

      }
      if ( intValue == undefined || indx == undefined )
         return;

      switch ( key.substring( 0, 4 ) )
      {
         case "OSIR": // image region
            updateRect( this.imageRect, coord, intValue );
            this.enabled = true;
            break;
         case "OSSR": // source region
            updateRect( this.overscan[ indx ].sourceRect, coord, intValue );
            this.overscan[ indx ].enabled = true;
            break;
         case "OSTR": // target region
            updateRect( this.overscan[ indx ].targetRect, coord, intValue );
            this.overscan[ indx ].enabled = true;
            break;
      }
   };

   this.copyFrom = function( source )
   {
      if ( source == undefined )
         return;
      this.enabled = source.enabled;
      this.imageRect = new Rect( source.imageRect );
      for ( let i = 0; i < 4; i++ )
      {
         this.overscan[ i ].enabled = source.overscan[ i ].enabled;
         this.overscan[ i ].sourceRect = new Rect( source.overscan[ i ].sourceRect );
         this.overscan[ i ].targetRect = new Rect( source.overscan[ i ].targetRect );
      }
   };

   this.toString = () =>
   {
      let rectToTooltip = function( R )
      {
         return "  x0: " + R.x0 + "<br>" +
            "  y0: " + R.x0 + "<br>" +
            "  x1: " + R.x0 + "<br>" +
            "  y1: " + R.x0 + "<br>";
      };

      let str = "<P><b>Image region</b><br>" + rectToTooltip( this.imageRect );

      for ( let i = 0; i < 4; i++ )
         if ( this.overscan[ i ].enabled )
            str += "<br>Region #" + i + ":<br>" +
            "[source rect]<br>" + rectToTooltip( this.overscan[ i ].sourceRect ) +
            "[target rect]<br>" + rectToTooltip( this.overscan[ i ].sourceRect );
      return str;
   }
}

Overscan.prototype = new Object;

// ----------------------------------------------------------------------------
// WBPP OERATION QUEUE
let WBPPOperationQueue = function()
{
   this.__base__ = OperationQueue;
   this.__base__();

   let
   {
      elapsedTimeToString
   } = WBPPUtils.shared();

   //

   this.onTerminationRequest = () =>
   {
      this.requestInterruption();
   }

   /**
    * This function saves the current pipeline status.
    * This information is handled by the test saving operation.
    */
   this.getExecutionStatus = () =>
   {
      let executionStatus = {
         totalExecutionTime: this.executionTime,
         ops: []
      };

      for ( let i = 0; i < this.operations.length; i++ )
      {
         if ( this.operations[ i ].operation.trackable )
         {
            executionStatus.ops[ i ] = {};
            executionStatus.ops[ i ].operation = this.operations[ i ].operation;
            executionStatus.ops[ i ].name = this.operations[ i ].operation.name;
         }
      }

      return executionStatus;
   }

   //

   this.stepCallback = () =>
   {
      switch ( this.status )
      {
         case OperationQueueStatus.RUNNING: //
            if ( !this.currentOperation() || this.executionMonitorDialog == undefined )
            {
               // INITIALIZED, READY TO RUN THE FIRST OPERATION
               // show the monitor view and start the refresh timer
               this.executionMonitorDialog = new ExecutionMonitorDialog( false /* as report */ , this.requestInterruption, undefined /* operations */ , this.onTerminationRequest );
               this.executionMonitorDialog.show();
               this.updateExecutionReport();
               this.executionMonitorDialog.adjustDialogFrame();
               // start the event and GUI refresh timer
               this.updaterTimer.start();
            }
            // flush the logs
            engine.consoleLogger.flush();
            break;

         case OperationQueueStatus.INACTIVE: //
            // EXECUTION COMPLETED, dismiss the execution monitor as tracker and show it as report
            this.updaterTimer.stop();
            this.hideExecutionMonitorDialog();
            // remove the temporary saved parameters
            engine.removeRunningConfiguration();

            // if we are in automation mode and
            //   - not on "LoadOnly"
            //   - not recording the test
            // return;
            if ( engine.automationMode && !engine.testLoadOnly && !engine.recordTest )
               return;

            // store the execution status to be eventually saved in the test export information
            engine.executionStatus = this.getExecutionStatus();

            // export the resulting test (with no cache) if we're recording
            if ( engine.automationMode && engine.recordTest && engine.testFile )
            {
               console.noteln( "STORE RECORDED TEST INTO ", engine.testFile )
               engine.executionCache.reset();
               let json = engine.exportParameters( true /* toJson */ );
               if ( File.exists( engine.testFile ) )
                  File.remove( engine.testFile );
               File.writeTextFile( engine.testFile, json );
               return;
            }

            this.executionMonitorDialog = new ExecutionMonitorDialog( true /* as report */ , undefined /* request interruption callback */ , this.trackabkeOperations() );
            this.executionMonitorDialog.noteLabel.text = "Executed in " + elapsedTimeToString( this.executionTime );

            // blocking execution
            this.executionMonitorDialog.execute();
            this.updateExecutionReport();
            break;
      }
      // RUNNING a task, refresh the status
      this.updateExecutionReport();
   };

   //

   // refresh timer
   this.updaterTimer = new Timer
   this.updaterTimer.interval = 1;
   this.updaterTimer.periodic = true;
   this.updaterTimer.dialog = this;
   this.updaterTimer.onTimeout = () =>
   {
      if ( this.executionMonitorDialog )
      {
         this.executionMonitorDialog.updateRunningOperation( this.trackabkeOperations() );
         this.executionMonitorDialog.noteLabel.text = "Elapsed: " + elapsedTimeToString( this.elapsed.value );
      }
      // refresh the progress report
      processEvents();
      gc();
   };
   this.updaterTimer.stop();

   //

   this.trackabkeOperations = () =>
   {
      return this.operations.filter( item => ( item.operation.trackable || false ) ).map( item => item.operation )
   };

   //

   this.updateExecutionReport = function()
   {
      if ( this.executionMonitorDialog )
         this.executionMonitorDialog.updateWithOperations( this.trackabkeOperations() );
   };

   //

   this.hideExecutionMonitorDialog = function()
   {
      // execution dialog
      if ( this.executionMonitorDialog )
      {
         this.executionMonitorDialog.ok();
         this.executionMonitorDialog = undefined;
      }
      // step termination box
      if ( this.waitingMsgBox )
      {
         this.waitingMsgBox.ok();
         this.waitingMsgBox = undefined;
      }
   };
}
WBPPOperationQueue.prototype = new OperationQueue;

// ----------------------------------------------------------------------------
// WBPP OERATIONS

/**
 * WBPP Operation Block is a specialization of an Operation Block providing supplementary
 * information to support the tracking and the estimation( space and time ) of execution.
 *
 * @param {String} name the opeartion name
 * @param {Object} group the group handled by the operation
 * @param {Bool} trackable TRUE if the operation is tracked in the execution monitor
 */
let WBPPOperationBlock = function( name, group, trackable )
{
   this.__base__ = OperationBlock;
   this.__base__();

   this.name = name;
   this.group = group;
   this.trackable = trackable;
   this.hasWarnings = false;
   this.statusMessage = "";

   // only trackable WBPP operations trigger the pipeline event script
   this.triggersEventScript = trackable;

   // operation execution time (in seconds)
   this.executionTime = 0;
   // average time per frame
   this.averageExecution = 0;

   this.run = ( environment, requestInterruption ) =>
   {
      return this._run( environment, requestInterruption );
   };

   this.updateGroupDescription = ( withActiveFrames, reportFileItems ) =>
   {
      this.groupDescription = ( group ? group.toShortString( withActiveFrames, reportFileItems ) : "" );
   };

   this.spaceRequired = () =>
   {
      return 0;
   };

   this.updateGroupDescription();
}
WBPPOperationBlock.prototype = new OperationBlock;

// ----------------------------------------------------------------------------

/**
 * This object is responsible of handling the WBPP execution cache.
 * The execution cache is the set of information that allows to check if a processs needs to be
 * executed or if its configuration is unchanged, the input files are unchanged and the output files
 * are already in the disk and unchanged since a previous execution. In this case, the process can be
 * skipped since it would produce exactly the already-existing output data.
 *
 */
function ExecutionCache()
{

   let hasher = new CryptographicHash( CryptographicHash_SHA1 );

   let
   {
      getLastModifiedDate
   } = WBPPUtils.shared();


   /**
    * Generates a cache key for the given string.
    *
    * @param {String} string
    * @return {String}
    */
   this.keyFor = ( string ) =>
   {
      return hasher.hash( ByteArray.stringToUTF8( string ) ).toHex();
   };

   /**
    * Clears the internal cache.
    *
    */
   this.reset = () =>
   {
      this.cache = {};
   };

   /**
    * Returns true if there is a cache data associated to the given key.
    *
    * @param {*} key
    * @return {*}
    */
   this.hasCacheForKey = ( key ) =>
   {
      return this.cache[ key ] != undefined;
   };

   /**
    * Retrieves the cached data associated to the given key.
    *
    * @param {*} key
    * @return {*}
    */
   this.cacheForKey = ( key ) =>
   {
      return this.cache[ key ];
   };

   /**
    * Sets the cache associated to the gicen key.
    *
    * @param {*} key
    * @param {*} value
    */
   this.setCache = ( key, value ) =>
   {
      this.cache[ key ] = value;
   };

   /**
    * Returns true if the file is unchanged since the last time its last modified date was cached for the given cache key
    *
    * @param {String} key
    * @param {String} filePath
    * @return {*}
    */
   this.isFileUnmodified = ( key, filePath ) =>
   {
      if ( arguments.length != 2 )
      {
         console.criticalln( "isFileUnmodified called with the wrong number of arguments ", arguments.length );
         return false;
      }
      if ( filePath == undefined || filePath.length == 0 )
         return false;
      let LMD = this.cache[ "LMD_" + key + filePath ];
      let currentLMD = getLastModifiedDate( filePath );
      let result = false;
      if ( currentLMD != null && LMD != undefined )
         result = currentLMD == LMD;
      console.writeln( "[cache] - file ", ( result ? "is unmodified" : "has changed" ), " ", File.extractNameAndExtension( filePath ), " [ ", key, " ] - cached [", LMD, "] - current [", currentLMD, "]" );
      return result;
   };

   /**
    * Stores the last modified date of the given file in the cache to the given cache key
    *
    * @param {String} key
    * @param {String} filePath
    */
   this.cacheFileLMD = ( key, filePath ) =>
   {
      if ( arguments.length != 2 )
      {
         console.criticalln( "cacheFileLMD called with the wrong number of arguments ", arguments.length );
         return;
      }
      // cached the data only if file exists
      let LMD = getLastModifiedDate( filePath );
      if ( LMD != null )
      {
         console.writeln( "[cache] - set LMD [", key, "] ", File.extractNameAndExtension( filePath ), ": ", LMD );
         let cacheKey = "LMD_" + key + filePath;
         this.cache[ cacheKey ] = LMD;
         this.__trackLMDKeys( filePath, cacheKey, LMD );
      }
   };

   /**
    * Invalidates the cached last modified date for the given file.
    *
    * @param {*} filePath
    */
   this.invalidateFileLMD = ( key, filePath ) =>
   {
      if ( arguments.length != 2 )
      {
         console.criticalln( "invalidateFileLMD called with the wrong number of arguments ", arguments.length );
         return;
      }
      console.writeln( "[cache] - invalidate LMD [", key, "] ", File.extractNameAndExtension( filePath ) );
      let cacheKey = "LMD_" + key + filePath;
      this.cache[ cacheKey ] = undefined;
      this.__trackLMDKeys( filePath, cacheKey, undefined );
   };

   /**
    * Exports the cache to a string representation.
    *
    */
   this.toString = () =>
   {
      return JSON.stringify( this.cache );
   };

   /**
    * Reconstruct the cache from a string representation.
    *
    * @param {*} string
    */
   this.fromString = ( string ) =>
   {
      try
      {
         let cache = JSON.parse( string );
         this.cache = cache;
      }
      catch ( e )
      {
         console.warningln( "*** Execution cache parsing failed." )
      }
   };

   /**
    * Approximates the size by returning the lenget of the JSON representation
    *
    */
   this.size = () =>
   {
      return JSON.stringify( this.cache ).length
   };

   this.reset();

   /**
    * Internal function that tracks the inverse mapping from a file LMD to the list of associated keys.
    * This is useful to update the LMD when a file gets changed but the changes should not impact the cached LMD.
    * Actually this occurs when the astrometric solution needs to be writtten: since only the header file changes
    * and the data remains the same we want to update the previously cached LMD with the updated LMD after we save
    * the file with the new astrometric solution.
    *
    * @param {*} filePath
    * @param {*} key
    * @param {*} LMD
    */
   this.__trackLMDKeys = ( filePath, key, LMD ) =>
   {
      let fileHistory = this.cache[ filePath ];
      if ( fileHistory == undefined || fileHistory.length == 0 )
      {
         this.cache[ filePath ] = [
         {
            lmd: LMD,
            keys: [ key ]
         } ];
      }
      else
      {
         if ( fileHistory[ fileHistory.length - 1 ].lmd == LMD )
            fileHistory[ fileHistory.length - 1 ].keys.push( key )
         else
         {
            fileHistory.push(
            {
               lmd: LMD,
               keys: [ key ]
            } )
         }
      }
   };

   /**
    * Updates the LMD to the new one for all cached files. This function could be called from outside to
    * explicitly modify a previously cached LMD for a given file.
    * This is used when we save the astrometric solution since the data regards the header file and not the image, so we
    * want to keep the file cached after it has been saved and its LMD changes (thus we need to update the cahced LMD
    * with the new LMD).
    *
    * @param {*} filePath
    * @param {*} previousLMD
    * @param {*} newLMD
    */
   this.updateLMD = ( filePath, previousLMD, newLMD ) =>
   {
      let fileHistory = this.cache[ filePath ];
      if ( fileHistory != undefined && fileHistory.length > 0 )
         for ( let i = 0; i < fileHistory.length; i++ )
            if ( fileHistory[ i ].lmd == previousLMD )
            {
               for ( let j = 0; j < fileHistory[ i ].keys.length; j++ )
               {
                  let key = fileHistory[ i ].keys[ j ];
                  this.cache[ key ] = newLMD;
               }
               fileHistory[ i ].lmd = newLMD;
               break;
            }
   };
}

// ----------------------------------------------------------------------------

/**
 * Main StackEngine object constructor
 *
 */
function StackEngine()
{
   this.__base__ = Object;
   this.__base__();

   this.diagnosticMessages = new Array;

   // allocate structures
   this.overscan = new Overscan;
   this.combination = new Array( 4 );
   this.rejection = new Array( 4 );
   this.percentileLow = new Array( 4 );
   this.percentileHigh = new Array( 4 );
   this.sigmaLow = new Array( 4 );
   this.sigmaHigh = new Array( 4 );
   this.linearFitLow = new Array( 4 );
   this.linearFitHigh = new Array( 4 );
   this.ESD_Outliers = new Array( 4 );
   this.ESD_Significance = new Array( 4 );
   this.RCR_Limit = new Array( 4 );

   this.groupsManager = new FrameGroupsManager();

   this.operationQueue = new WBPPOperationQueue();

   // generic execution cache object handled by pipeline steps
   this.executionCache = new ExecutionCache;

   // console logger
   this.consoleLogger = new ConsoleLogger();

   // process logger
   this.processLogger = new ProcessLogger();
}

StackEngine.prototype = new Object;

// ----------------------------------------------------------------------------
// OVERSCAN MANAGEMENT
// ----------------------------------------------------------------------------
/**
 * Sets the global overscan settings from a specific gropu settings.
 *
 * @param {*} group the group containing the overscan settings
 */
StackEngine.prototype.setOverscanInfoFromGroup = function( group )
{
   if ( group.hasMaster )
      this.overscan.copyFrom( group.fileItems[ 0 ].overscan );
}

// ----------------------------------------------------------------------------
// REFERENCE FRAME MANAGEMENT
// ----------------------------------------------------------------------------
StackEngine.prototype.getBestReferenceFrameModes = function()
{
   // store the current selected item
   let selections = [ "manual", "auto" ];
   let postProcessKeywords = engine.keywords.keywordsForMode( WBPPGroupingMode.POST );
   for ( let i = 0; i < postProcessKeywords.length; i++ )
      selections.push( "auto by " + postProcessKeywords[ i ].name );
   return selections;
};

StackEngine.prototype.setBestReferenceFrameMode = function( index )
{

   if ( index == WBPPBestRefernenceMethod.MANUAL || index == WBPPBestRefernenceMethod.AUTO_SINGLE )
   {
      this.bestFrameRefernceMethod = index;
      this.bestFrameReferenceKeyword = "";
   }
   else
   {
      this.bestFrameRefernceMethod = WBPPBestRefernenceMethod.AUTO_KEYWORD;
      let options = this.getBestReferenceFrameModes();
      this.bestFrameReferenceKeyword = options[ index ].replace( "auto by ", "" );
   }
};

StackEngine.prototype.bestReferenceFrameModeIndex = function()
{
   if ( this.bestFrameRefernceMethod == WBPPBestRefernenceMethod.MANUAL || this.bestFrameRefernceMethod == WBPPBestRefernenceMethod.AUTO_SINGLE )
      return this.bestFrameRefernceMethod;

   let options = this.getBestReferenceFrameModes().map( item => item.replace( "auto by ", "" ) );

   for ( let i = 0; i < options.length; i++ )
      if ( options[ i ] == this.bestFrameReferenceKeyword )
         return i;

   // error, not able to find the index. Change the method to AUTO SINGLE
   this.bestFrameRefernceMethod = WBPPBestRefernenceMethod.AUTO_SINGLE;
   return 1;
};

// ----------------------------------------------------------------------------
// GROUPS GETTERS
// ----------------------------------------------------------------------------

//
/**
 * Returns a sorted list of groups of a given type and mode.
 *
 * @param {ImageType} type type of gorups to be retrieved
 * @param {WBPPGroupingMode} mode the grouping mode
 * @returns the sorted list of groups
 */
StackEngine.prototype.getSortedGroupsOfType = function( type, mode )
{
   let groupsByType = [];
   let groups = this.groupsManager.groupsForMode( mode );
   for ( let i = 0; i < groups.length; i++ )
      if ( groups[ i ].imageType == type )
         groupsByType.push( groups[ i ] );

   // get the ordered keywords
   let keywords;

   // sort result
   groupsByType.sort( ( a, b ) =>
   {
      // sort image size first
      let asize = a.size.width * a.size.height;
      let bsize = b.size.width * b.size.height;
      if ( asize != bsize )
         return asize < bsize ? -1 : 1;

      // master file on bottom
      if ( a.hasMaster != b.hasMaster )
         return a.hasMaster ? -1 : 1;
      // filter by binning
      if ( a.binning != b.binning )
         return a.binning > b.binning ? -1 : 1;

      // for Flats, the filter is the only third sorting rule
      if ( type == ImageType.FLAT )
         return a.filter.localeCompare( b.filter );

      // filter by duration
      if ( a.exposureTime != b.exposureTime )
         return a.exposureTime > b.exposureTime ? -1 : 1;

      // filter by filter name
      if ( a.filter != b.filter )
         return a.filter.localeCompare( b.filter );

      // allocate the sorting keywords only one time if needed
      if ( keywords == undefined )
         keywords = engine.keywords.sortedNames( mode );

      // filter by keywords
      for ( let j = 0; j < keywords.length; j++ )
      {
         let aKeyword = a.keywords[ keywords[ j ] ];
         let bKeyword = b.keywords[ keywords[ j ] ];
         if ( aKeyword != undefined && bKeyword == undefined )
            return -1;
         if ( aKeyword == undefined && bKeyword != undefined )
            return 1;
         if ( aKeyword != bKeyword )
            return aKeyword.localeCompare( bKeyword );
      }
      // sorting not defined, groups have the same sorting precedence (this should never happen)
      return 0;
   } );
   // resort inserting associated channels and recombined just after the master groups
   let sorted = [];
   // first pass, extract non associated groups and map the associated ones
   // linkedGroupIDmap is a map [groupID] => {R: group?, B: group?, B: group?...} mapping the groups
   // that have linked groups and the linked groups mapped on the associated channels, for a quick second pass insertion
   let linkedGroupIDmap = {};
   let parentGroups = [];
   for ( let i = 0; i < groupsByType.length; i++ )
   {
      let linkedGroupID = groupsByType[ i ].linkedGroupID;
      if ( linkedGroupID == undefined )
         parentGroups.push( groupsByType[ i ] );
      else
      {
         if ( !linkedGroupIDmap[ linkedGroupID ] )
            linkedGroupIDmap[ linkedGroupID ] = {}
         linkedGroupIDmap[ linkedGroupID ][ groupsByType[ i ].associatedRGBchannel ] = groupsByType[ i ];
      }
   }
   // second pass, insert the parents and the associated immediatly after
   for ( let i = 0; i < parentGroups.length; i++ )
   {
      sorted.push( parentGroups[ i ] );
      let groupID = parentGroups[ i ].id;
      if ( linkedGroupIDmap[ groupID ] )
         for ( let j = 0; j < WBPPAssociatedChannel.sorted.length; j++ )
            if ( linkedGroupIDmap[ groupID ][ WBPPAssociatedChannel.sorted[ j ] ] )
               sorted.push( linkedGroupIDmap[ groupID ][ WBPPAssociatedChannel.sorted[ j ] ] );
   }

   return sorted;
};

// ----------------------------------------------------------------------------

/**
 * Search for the groups that are calibrated by the provided group.
 * NB: by default this function looks the calibration groups with mode WBPPGroupingMode = .PRE
 *
 * @param {FrameGroup} group the group that calibrates all the returned groups
 * @returns the array of groups that are calibrated by the provided group
 */
StackEngine.prototype.getGroupsCalibratedBy = function( group )
{
   let calibratedBy = [];

   // Scan all pre-processing groups, search for the calibration files for each group
   // of the same type of the provided group, if the ID matches then the looped group
   // is calibrated with the provided file
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );
   for ( let i = 0; i < groups.length; ++i )
   {
      let cg = groups[ i ];

      if ( cg == group )
         continue;

      let cf = this.getCalibrationGroupsFor( cg );

      if ( group.imageType == ImageType.BIAS && group == cf.masterBias )
      {
         calibratedBy.push( cg );
         continue
      }

      if ( group.imageType == ImageType.DARK && group == cf.masterDark )
      {
         calibratedBy.push( cg );
         continue
      }

      if ( group.imageType == ImageType.FLAT && group == cf.masterFlat )
      {
         calibratedBy.push( cg );
         continue
      }
   }
   return calibratedBy;
};

// ----------------------------------------------------------------------------

/**
 * Search for the groups that calibrate the provided group.
 *
 * @param {FrameGroup} group the group for which we want to retrieve the calibration groups
 * @returns the array of groups that calibrate the provided group
 */
StackEngine.prototype.getCalibrationGroupsFor = function( group )
{
   let calibrationGroups = {
      masterBias: undefined,
      masterDark: undefined,
      masterFlat: undefined,
   };
   // a master file is not calibrated
   if ( group.hasMaster )
      return calibrationGroups;

   let mb, md, mf;
   let size = group.size;
   let binning = group.binning;
   let exposureTime = group.exposureTime;
   let filter = group.filter;
   let exactDarkExposureTime = false;
   let isCFA = group.isCFA;

   switch ( group.imageType )
   {
      case ImageType.DARK:
         // dark frames are never calibrated
         break;
      case ImageType.FLAT:
         // look for compatible master dark and master bias
         mb = this.getMasterBiasGroup( binning, size, false /*isMaster*/ , group.keywords );
         if ( !group.forceNoDark )
            md = group.overrideDark || this.getMasterDarkGroup( ImageType.FLAT, binning, exposureTime, size, mb /* exactDarkExposureTime: yes if we have a master bias */ , group.keywords );
         break;
      case ImageType.LIGHT:
         mb = this.getMasterBiasGroup( binning, size, false, group.keywords );
         if ( !group.forceNoDark )
            md = group.overrideDark || this.getMasterDarkGroup( ImageType.LIGHT, binning, exposureTime, size, exactDarkExposureTime, group.keywords );
         if ( !group.forceNoFlat )
            mf = group.overrideFlat || this.getMasterFlatGroup( binning, size, filter, isCFA, false /*isMaster*/ , group.keywords );
         break;
   }

   // Advanced logic: remove master bias when it is not necessary
   // MasterBias is needed only when:
   // 1. master dark is NOT present
   // 2. if master dark is present and optimized
   // 3. master dark is present but does not contain the bias
   if ( mb )
   {
      let masterDarkISNotPResent = !md;
      let masterDarkIsOptimized = md && group.optimizeMasterDark;

      if ( masterDarkISNotPResent || masterDarkIsOptimized )
         calibrationGroups.masterBias = mb;
   }
   if ( md )
      calibrationGroups.masterDark = md;
   if ( mf )
      calibrationGroups.masterFlat = mf;

   return calibrationGroups;
};

// ----------------------------------------------------------------------------

/**
 * Returns an array of { group, count } objects containing a group and the number
 * of matching keywords, filtering by the image type provided.
 * For each group, the number of matching keywors is counted, groups with the same
 * count are grouped together and the groups with the same keyword matching count is
 * sorted by keyword precedence.
 *
 * @param {ImageType} imageType
 * @param {Object} size {width:Numeric, height:Numeric}
 * @param {{String:String}} keywords the keywords key-value map
 * @param {Boolean} onlyHighestMatches if true returns the list of groups with the
 *                                     highest number of matching keywords, othwerise
 *                                     it returns all the matching groups
 * @returns
 */
StackEngine.prototype.getCompatibleCalibrationGroups = function( imageType, size, keywords, onlyHighestMatches )
{
   // preselect the compatible groups
   let matchingGroups = {};
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );
   for ( let i = 0; i < groups.length; ++i )
      if ( groups[ i ].imageType == imageType )
      {
         // overscan handling: when we search for a group with overscan ebnabled we have to handle the case
         // where a flat group has a master into it, then the master size must match the current overscan
         // area size to match, otherwise the regular matching is applied
         let compatibleSize = groups[ i ].size.width == size.width && groups[ i ].size.height == size.height;
         if ( imageType == ImageType.FLAT &&
            groups[ i ].hasMaster )
         {
            if ( engine.overscan.enabled )
            {
               // in case of overscan is enabled then also the size of the master must match the size of the overscan area
               let W = engine.overscan.imageRect.x1 - engine.overscan.imageRect.x0;
               let H = engine.overscan.imageRect.y1 - engine.overscan.imageRect.y0;
               compatibleSize = compatibleSize && groups[ i ].fileItems[ 0 ].size.width == W && groups[ i ].fileItems[ 0 ].size.height == H;
            }
            else
            {
               // in case of overscan is not active then the size of the master must match the provided size
               compatibleSize = compatibleSize && groups[ i ].fileItems[ 0 ].size.width == size.width && groups[ i ].fileItems[ 0 ].size.height == size.height;
            }
         }

         if ( compatibleSize )
         {
            // enable the strict direct matching i.e. we exclude a group if it
            // has a keyword that is not in the set of the provided keywords
            let count = groups[ i ].keywordsMatchCount(
               keywords,
               true, /* strictDirectMatching */
               false /* strictInverseMatching */
            );
            if ( count >= 0 )
            {
               if ( !matchingGroups[ count ] )
                  matchingGroups[ count ] = [];
               matchingGroups[ count ].push( i );
            }
         }
      }

   // matchingGroups is a map between the count number and the array of compatible groups
   // we select the groups with the highest matching count
   let sortedCounts = Object.keys( matchingGroups );
   sortedCounts.sort();
   sortedCounts.reverse();

   // search for the best dark within the candidates
   let compatibleGroups = [];

   // extract the candidates
   if ( sortedCounts.length > 0 )
   {
      // keep only the highest matching if required
      if ( onlyHighestMatches )
         sortedCounts = [ sortedCounts[ 0 ] ];

      // internally sort groups by keyword precedence
      let keywords = engine.keywords.names();
      sortedCounts.forEach( count =>
      {
         // get the groups with the current count value
         let currentGroups = matchingGroups[ count ].map( i => groups[ i ] );
         // sort by keyword precedence
         currentGroups.sort( ( a, b ) =>
         {
            // process keywords respecting the order, if a keyword is found in group A
            // but not in group B then A has precedence and viceversa. If they have the same
            // keywords then the behavior is undefined
            for ( let j = 0; j < keywords.length; j++ )
            {
               let aKeyword = a.keywords[ keywords[ j ] ];
               let bKeyword = b.keywords[ keywords[ j ] ];
               if ( aKeyword != undefined && bKeyword == undefined )
                  return -1;
               if ( aKeyword == undefined && bKeyword != undefined )
                  return 1;
            }
            return 0;
         } );
         compatibleGroups = compatibleGroups.concat( currentGroups.map( g => (
         {
            group: g,
            count: count
         } ) ) );
      } );
   }

   return compatibleGroups;
};

// ----------------------------------------------------------------------------

/**
 * Returns the list of groups that matches the criteria provided.
 * This function accepts an object with key - value that must be matched by the returned group.
 *
 * @param {{}} properties key-value pair to be matched
 * @returns
 */
StackEngine.prototype.getCalibrationGroupsMatching = function( properties )
{
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );
   return groups.reduce( ( matchingGroups, group ) =>
   {
      let keys = Object.keys( properties );
      for ( let i = 0; i < keys.length; i++ )
      {
         let key = keys[ i ];
         // handle the special case of the 'size' property object
         if ( key == "size" )
         {
            if ( group[ key ] && ( group[ key ].width != properties[ key ].width || group[ key ].height != properties[ key ].height ) )
               return matchingGroups;
         }
         else if ( group[ key ] != properties[ key ] )
            return matchingGroups;
      }
      matchingGroups.push( group );
      return matchingGroups;
   }, [] );
};

// ----------------------------------------------------------------------------

/**
 * Search for the Master Bias Group matching the given parameters.
 *
 * @param {Numeric} binning binning to match
 * @param {Boolean} isMaster true if the group has to contain a mster file, false otherwise
 * @param {{String:String}} keywords the keywords key-value map
 * @returns the matching masterBias group on success, undefined otherwise
 */
StackEngine.prototype.getMasterBiasGroup = function( binning, size, isMaster, keywords )
{
   let compatibleBias = this.getCompatibleCalibrationGroups( ImageType.BIAS, size, keywords, true /* onlyHighestMatches */ );

   for ( let i = 0; i < compatibleBias.length; ++i )
      if ( !isMaster || compatibleBias[ i ].group.hasMaster )
         if ( compatibleBias[ i ].group.binning == binning )
            return compatibleBias[ i ].group;
   return undefined;
};

// ----------------------------------------------------------------------------

/**
 * Returns the group containing or generating the best matching master dark given the parameters.
 *
 * @param {ImageType} imageType the imate type for which we want the matching dark (flat or light frames)
 * @param {Numeric} binning binning to match
 * @param {Numeric} exposureTime exposure time to search for
 * @param {Object} size {width:Numeric, height:Numeric}
 * @param {Boolean} findExactExposureTime true if exposure time must match exactly, false otherwise
 * @param {{String: String}} keywords { key: value } keywords object
 * @param {Boolean} isMaster true if the group must already contain the master file
 * @param {Boolean} logResult true if the search result of the master dark needs to print some log
 *                      information on the console
 * @returns the matching master dark group
 */
StackEngine.prototype.getMasterDarkGroup = function( imageType, binning, exposureTime, size, findExactExposureTime, keywords, isMaster, logResult )
{
   // Assume no binning when binning is unknown.
   if ( binning <= 0 )
      binning = 1;

   // Ensure we get the most exposed master dark frame when the exposure time
   // is unknown. This favors scaling down dark current during optimization.
   let knownTime = exposureTime > 0;
   if ( !knownTime )
      exposureTime = 1.0e+10;

   // By default we do not search for exact duration darks.
   if ( findExactExposureTime === undefined )
      findExactExposureTime = false;

   // search for the best dark within the candidates
   let masterDarkGroup = undefined;
   let candidateDarks = this.getCompatibleCalibrationGroups( ImageType.DARK, size, keywords, imageType !== ImageType.LIGHT /* onlyHighestMatches */ );

   let foundTime = 1.0e+20;
   let bestSoFar = 1.0e+20;
   let bestMatchingCount = -1;
   for ( let i = 0; i < candidateDarks.length; ++i )
      if ( !isMaster || candidateDarks[ i ].group.hasMaster )
         if ( candidateDarks[ i ].group.imageType == ImageType.DARK )
            if ( candidateDarks[ i ].group.binning == binning )
            {
               let d = Math.abs( candidateDarks[ i ].group.exposureTime - exposureTime );
               if ( d <= bestSoFar && ( !findExactExposureTime || ( findExactExposureTime && d < CONST_FLAT_DARK_TOLERANCE ) ) )
               {
                  // if the best equals the current exposure then we keep it only if the new best has a higher number matching keywords
                  if ( d == bestSoFar )
                     if ( candidateDarks[ i ].length <= bestMatchingCount )
                        continue;

                  bestMatchingCount = candidateDarks[ i ].length;
                  masterDarkGroup = candidateDarks[ i ].group;
                  foundTime = candidateDarks[ i ].group.exposureTime;
                  bestSoFar = d;
               }
            }

   if ( masterDarkGroup && logResult )
      if ( foundTime > 0 )
      {
         if ( findExactExposureTime )
            console.noteln( "<end><cbr><br>* Searching for a master flat dark with exposure time = " +
               exposureTime + "s -- found." );
         else if ( knownTime )
            console.noteln( "<end><cbr><br>* Searching for a master dark frame with exposure time = ",
               exposureTime + "s -- best match is ", foundTime + "s" );
         else
            console.noteln( "<end><cbr><br>* Using master dark frame with exposure time = ",
               foundTime + "s to calibrate unknown exposure time frame(s)." );
      }
   else
   {
      if ( findExactExposureTime )
         console.noteln( "<end><cbr><br>* Searching for a master flat dark with exposure time = ",
            exposureTime + "s -- not found." );
      else if ( knownTime )
         console.noteln( "<end><cbr><br>* Searching for a master dark frame with exposure time = ",
            exposureTime + "s -- best match is a master dark frame of unknown exposure time." );
      else
         console.noteln( "<end><cbr><br>* Master dark match with an unknown exposure time." );
   }

   return masterDarkGroup;
};

// ----------------------------------------------------------------------------

/**
 * Returns the group containing or generating the best matching master flat given the parameters.
 *
 * @param {Numeric} binning
 * @param {Object} size {width:Numeric, height:Numeric}
 * @param {String} filter
 * @param {Boolean} isMaster true if the group must already contain the master file
 * @returns the matching master flat group
 */
StackEngine.prototype.getMasterFlatGroup = function( binning, size, filter, isCFA, isMaster, keywords )
{
   // search for the best flat group within the candidates
   let candidateFlats = this.getCompatibleCalibrationGroups( ImageType.FLAT, size, keywords, false /* onlyHighestMatches */ );

   for ( let i = 0; i < candidateFlats.length; ++i )
      if ( !isMaster || candidateFlats[ i ].group.hasMaster )
         if ( candidateFlats[ i ].group.imageType == ImageType.FLAT )
            if ( candidateFlats[ i ].group.binning == binning && candidateFlats[ i ].group.filter == filter && candidateFlats[ i ].group.isCFA == isCFA )
               return candidateFlats[ i ].group;
   return undefined;
};

// ----------------------------------------------------------------------------
// FRAME GROUP CEHCKS
// ----------------------------------------------------------------------------

/**
 * Returns true if groups of the given type and mode exist.
 *
 * @param {ImageType} imageType
 * @param {WBPPGroupingMode} mode
 * @returns
 */
StackEngine.prototype.hasFrames = function( imageType, mode )
{
   let groups = this.groupsManager.groupsForMode( mode );
   for ( let i = 0; i < groups.length; ++i )
      if ( groups[ i ].imageType == imageType )
         return true;
   return false;
};

// ----------------------------------------------------------------------------

/**
 * Returns true if bias frames exists.
 *
 * @returns
 */
StackEngine.prototype.hasBiasFrames = function()
{
   return this.hasFrames( ImageType.BIAS, WBPPGroupingMode.PRE );
};

/**
 * Returns true if dark frames exists.
 *
 * @returns
 */
StackEngine.prototype.hasDarkFrames = function()
{
   return this.hasFrames( ImageType.DARK, WBPPGroupingMode.PRE );
};

/**
 * Returns true if flat frames exists.
 *
 * @returns
 */
StackEngine.prototype.hasFlatFrames = function()
{
   return this.hasFrames( ImageType.FLAT, WBPPGroupingMode.PRE );
};

/**
 * Returns true if light frame groups with the given mode exists.
 *
 * @returns
 */
StackEngine.prototype.hasLightFrames = function( mode )
{
   return this.hasFrames( ImageType.LIGHT, mode );
};

// ----------------------------------------------------------------------------
// StackEngine Presets
// ----------------------------------------------------------------------------

/**
 * Constructor of the presets Dialog.
 */
function PresetsDialog()
{
   this.__base__ = Dialog;
   this.__base__();
   this.windowTitle = "Presets";

   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;
   this.sizer.spacing = 8;

   this.addPreset = ( title, description, preset ) =>
   {
      let control = new ParametersControl( title, this );
      control.setScaledFixedWidth( 400 );

      let label = new Label( control );
      label.wordWrapping = true;
      label.useRichText = true;
      label.text = description;

      let button = new PushButton( control );
      button.text = "APPLY";
      button.onClick = () =>
      {
         this.done( preset );
      }
      let sizer = new HorizontalSizer;
      sizer.addStretch();
      sizer.add( button );
      sizer.addStretch();

      control.add( label );
      control.add( sizer );
      this.sizer.add( control );
   };

   // MAX QUALITY
   this.addPreset(
      "Maximum quality",
      "<p><b>Maximum quality with no compromises.</b><br>" +
      "Local normalization is enabled with its default maximum number of stars used for scale evaluation, " +
      "the PSF type is set to <i>Auto</i>.</p>",
      WBPPPresets.BEST_QUALITY
   );

   // FASTER / MID QUALITY
   this.addPreset(
      "Faster with good quality",
      "<p><b>Faster with sub-optimal quality results.</b><br>" +
      "Local normalization is enabled with a reduced number of stars used for scale evaluation (500), " +
      "the PSF type is set to <i>Moffat 4</i>.</p>",
      WBPPPresets.MID
   );

   // FASTER / MID QUALITY
   this.addPreset(
      "Fastest with lower quality",
      "<p><b>Fastest method with lower quality results.</b><br>" +
      "Local normalization is disabled.</p>",
      WBPPPresets.FAST
   );

   //
   this.ensureLayoutUpdated();
   this.setFixedSize();
}
PresetsDialog.prototype = new Dialog;

/**
 * Configure the engine parameters by appliying the given preset.
 *
 * @param {*} preset
 */
StackEngine.prototype.applyPreset = function( preset )
{
   switch ( preset )
   {
      case WBPPPresets.BEST_QUALITY:
         // enable and configure local normalization
         this.localNormalization = true;
         this.localNormalizationPsfType = WBPPLocalNormalizationPsfType.AUTO;
         this.localNormalizationPsfMaxStars = DEFAULT_LOCALNORMALIZATION_PSF_MAX_STARS;
         break;
      case WBPPPresets.MID:
         this.localNormalization = true;
         this.localNormalizationPsfType = WBPPLocalNormalizationPsfType.MOFFAT_4;
         this.localNormalizationPsfMaxStars = 500;
         break;
      case WBPPPresets.FAST:
         this.localNormalization = false;
         break;
   }
};

// ----------------------------------------------------------------------------
// StackEngine Methods
// ----------------------------------------------------------------------------

/**
 * Retuns the image type given the keyword value. Invoked when IMAGETYP is found in FITS header.
 *
 * @param {String} value IMAGETYP keyword's value
 * @returns the image type, UNKNOWN if type cannot be inferred
 */
StackEngine.imageTypeFromKeyword = function( value )
{
   switch ( value.toLowerCase().replace( " ", "" ) )
   {
      case "biasframe":
      case "bias":
      case "masterbias":
         return ImageType.BIAS;
      case "darkframe":
      case "dark":
      case "masterdark":
      case "flatdark":
      case "darkflat":
         return ImageType.DARK;
      case "flatfield":
      case "flatframe":
      case "flat":
      case "masterflat":
         return ImageType.FLAT;
      case "lightframe":
      case "light":
      case "scienceframe":
      case "science":
      case "masterlight":
         return ImageType.LIGHT;
      default:
         return ImageType.UNKNOWN;
   }
};

// ----------------------------------------------------------------------------

/**
 * Checks if the file is to be considered a master file from the IMAGETYP keyword's value.
 *
 * @param {String} value IMAGETYP keyword's value
 * @returns true if file is to be used as master file
 */
StackEngine.isMasterFromKeyword = function( value )
{
   return value.toLowerCase().indexOf( "master" ) >= 0;
};

// ----------------------------------------------------------------------------

/**
 * Returns the string describing the image type.
 *
 * @param {ImageType} imageType
 * @returns
 */
StackEngine.imageTypeToString = function( imageType )
{
   return [ "Bias", "Dark", "Flat", "Light" ][ imageType ];
};

// ----------------------------------------------------------------------------

/**
 * Returns the string describing the master file associated to the providede image type.
 *
 * @param {ImageType} imageType
 * @returns
 */
StackEngine.imageTypeToMasterKeywordValue = function( imageType )
{
   return [ "Master Bias", "Master Dark", "Master Flat", "Master Light" ][ imageType ];
};

// ----------------------------------------------------------------------------
// WARNINGS AND DIAGNOSTICS
// ----------------------------------------------------------------------------

/**
 * Diagnostic messages generator.
 */
StackEngine.prototype.runDiagnostics = function()
{
   let
   {
      cleanFilterName,
      isEmptyString,
      paddedLabel,
      readableSize
   } = WBPPUtils.shared();

   this.messages = [ 0 ];

   this.pushTop = () =>
   {
      this.messages.push( this.diagnosticMessages.length );
   };
   this.top = () =>
   {
      return this.messages[ this.messages.length - 1 ];
   };
   this.popTop = () =>
   {
      if ( this.messages.length > 1 )
         this.messages.pop();
   };

   this.errorPrefix = "<span style=\"color:#DD1111\"><b>Error</b>: ";
   this.errorPostfix = "</span>";
   this.warningPrefix = "<span style=\"color:#CC00CC\"><b>Warning</b>: ";
   this.warningPostfix = "</span>";
   this.notePrefix = "<span style=\"color:#009900\"><b>Note</b></span>: <span style=\"white-space:break-spaces\">";
   this.notePostfix = "</span>";

   let preprocessGroups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );
   let postprocessGroups = this.groupsManager.groupsForMode( WBPPGroupingMode.POST );

   /**
    * Removes all messages that are not errors or warnings until an error or warning is retrieved or
    * the top pointer is reached.
    */
   this.cleanUp = () =>
   {
      while ( this.top() < this.diagnosticMessages.length && this.diagnosticMessages.length > 0 )
      {
         let msg = this.diagnosticMessages[ this.diagnosticMessages.length - 1 ];
         if ( !msg.startsWith( this.errorPrefix ) && !msg.startsWith( this.warningPrefix ) && !msg.startsWith( this.notePrefix ) )
            this.diagnosticMessages.pop();
         else
            return;
      }
   };

   /**
    * Pushes an error message
    *
    * @param {*} message the error message
    */
   this.error = function( message )
   {
      this.diagnosticMessages.push( this.errorPrefix + "<i>" + message + "</i>." + this.errorPostfix );
   };

   /**
    * Pushes a warning message.
    *
    * @param {*} message the warning message
    */
   this.warning = function( message )
   {
      this.diagnosticMessages.push( this.warningPrefix + "<i>" + message + "</i>." + this.warningPostfix );
   };

   /**
    * Pushes a note message.
    *
    * @param {*} message the note message
    */
   this.note = function( message )
   {
      this.diagnosticMessages.push( this.notePrefix + message + "." + this.notePostfix );
   };

   /**
    * Adds a generic text to the diagnostic, appending a period at the end.
    *
    * @param {*} text
    */
   this.genericText = function( text )
   {
      this.diagnosticMessages.push( text + "." );
   };

   /**
    * Adds a header
    *
    * @param {*} text the header title
    */
   this.headerText = function( text )
   {
      this.diagnosticMessages.push( "<br><br><b>==== " + text + "</b>" );
   };

   // initial clean up
   this.clearDiagnosticMessages();

   // ........................................................................
   this.pushTop();
   this.headerText( "Check for long file path limitation" );
   if ( this.outputDirectory != "" && File.directoryExists( this.outputDirectory ) )
   {
      let foldersList = [ this.outputDirectory ];
      while ( true )
      {
         foldersList.push( "WBPP_64_characters_folder_name_to_test_long_path_accessibility_" );
         let curDir = foldersList.join( "/" );
         try
         {
            File.createDirectory( curDir, true /* create intermediate dir */ );
         }
         catch ( e )
         {
            this.warning( "Cannot create files with a <b>path longer than 256 characters</b>. This is a limitation imposed by the " +
               "operating system and can cause the failure of one or more steps.<br>Ensure that you remove this limitation by " +
               "properly configuring your operating system settings" );
            break;
         }
         if ( curDir.length > 260 )
            break;
      }

      // clean up
      while ( foldersList.length > 1 )
      {
         let curDir = foldersList.join( "/" );
         try
         {
            File.removeDirectory( curDir );
         }
         catch ( e )
         {};
         foldersList.pop();
      }
   }
   this.cleanUp();
   this.popTop();

   // ........................................................................

   this.pushTop();
   this.headerText( "Check .xisf writer" );
   try
   {
      let F = new FileFormat( ".xisf", false /*toRead*/ , true /*toWrite*/ );
      if ( F == null )
         throw '';
      if ( !F.canStoreFloat )
         this.error( "The " + F.name + " format cannot store 32-bit floating point image data" );
      if ( !F.canStoreKeywords )
         this.warning( "The " + F.name + " format does not support keywords" );
      if ( !F.canStoreProperties || !F.supportsViewProperties )
         this.warning( "The " + F.name + " format does not support image properties" );
      if ( F.isDeprecated )
         this.warning( "Using a deprecated output file format: " + F.name );

   }
   catch ( x )
   {
      this.error( "No installed file format can write " + ".xisf" + " files" );
   }
   this.cleanUp();
   this.popTop();

   // ........................................................................

   this.pushTop();
   this.headerText( "Check output directory" );

   let hasOutputDirectoryIssue = false;

   if ( isEmptyString( this.outputDirectory ) )
   {
      this.error( "No output directory specified" );
      hasOutputDirectoryIssue = true;
   }
   else if ( !File.directoryExists( this.outputDirectory ) )
   {
      this.error( "The specified output directory does not exist: " + this.outputDirectory );
      hasOutputDirectoryIssue = true;
   }
   else
   {
      try
      {
         let f = new File;
         let n = this.outputDirectory + "/__pixinsight_checking__";
         for ( let u = 1;; ++u )
         {
            let nu = File.appendToName( n, u.toString() );
            if ( !File.exists( nu ) )
            {
               n = nu;
               break;
            }
         }
         f.createForWriting( n );
         f.close();
         File.remove( n );
      }
      catch ( x )
      {
         this.error( "Cannot access the output directory for writing: " + this.outputDirectory );
         hasOutputDirectoryIssue = true;
      }
   }

   this.cleanUp();
   this.popTop();

   // ........................................................................

   this.pushTop();
   this.headerText( "Check bias/dark/flat/light groups" );

   // global configuration
   if ( preprocessGroups.length == 0 && postprocessGroups.length == 0 )
      this.error( "No input frames have been provided" );
   else
   {
      if ( !this.hasBiasFrames() )
         this.note( "No bias frames have been provided" );

      if ( !this.hasDarkFrames() )
         this.note( "No dark frames have been provided" );

      if ( !this.hasFlatFrames() )
         this.note( "No flat frames have been provided" );

      if ( !this.hasLightFrames( WBPPGroupingMode.PRE ) )
         this.note( "No light frames have been provided" );
   }

   this.cleanUp();
   this.popTop();

   // ........................................................................

   // Diagnostic pre-processes BIAS, DARK, FLAT and LIGHT frames
   let groupsOrder = [ ImageType.BIAS, ImageType.DARK, ImageType.FLAT, ImageType.LIGHT ];

   for ( let k = 0; k < groupsOrder.length; k++ )
   {
      for ( let i = 0; i < preprocessGroups.length; ++i )
      {
         if ( preprocessGroups[ i ].imageType != groupsOrder[ k ] )
            continue;

         this.pushTop();
         this.headerText( preprocessGroups[ i ].toString() );

         // check file existence
         for ( let j = 0; j < preprocessGroups[ i ].fileItems.length; ++j )
            if ( !File.exists( preprocessGroups[ i ].fileItems[ j ].filePath ) )
               this.error( "Nonexistent input file: " + preprocessGroups[ i ].fileItems[ j ].filePath );

         // filter name character set
         if ( !isEmptyString( preprocessGroups[ i ].filter ) )
            if ( cleanFilterName( preprocessGroups[ i ].filter ) != preprocessGroups[ i ].filter )
               this.warning( "Invalid file name characters will be replaced with dashes " +
                  "in filter name: \'" + preprocessGroups[ i ].filter + "\'" );

         // various checks for flat and light frames
         if ( ( preprocessGroups[ i ].imageType == ImageType.FLAT || preprocessGroups[ i ].imageType == ImageType.LIGHT ) &&
            !preprocessGroups[ i ].hasMaster )
         {
            let cf = this.getCalibrationGroupsFor( preprocessGroups[ i ] )
            let masterDarkExposureDifferenceIsHigh = cf.masterDark && Math.abs( cf.masterDark.exposureTime - preprocessGroups[ i ].exposureTime ) > 5;

            // check if neither bias nor dark are available
            if ( !cf.masterBias && !cf.masterDark && !cf.masterFlat )
            {
               this.note( "No Master Bias, Master Dark and Maser Flat have been provided to calibrate this group. Frames will not be calibrated" );
               continue;
            }

            // check if neither bias nor dark are available
            if ( !cf.masterBias && !cf.masterDark )
               this.note( "Neither Master Bias nor Master Dark will be used to calibrate the frames" );

            // check if only dark is found but it does not contain the master bias
            if ( !cf.masterBias && cf.masterDark && preprocessGroups[ i ].optimizeDarks )
               this.warning( "Frames will be calibrated using an optimized Master Dark but no Master Bias has been found. Optimizing a Master Dark without subtracting the Master Bias could (likely) lead to improper results" );

            // check if dark exposure difference is too much
            if ( masterDarkExposureDifferenceIsHigh )
            {
               if ( preprocessGroups[ i ].optimizeMasterDark )
                  this.note( 'Frames will be calibrated using an optimized Master Dark with an exposure time of ' + cf.masterDark.exposureTime + ' sec' );
               else
                  this.warning( 'Frames will be calibrated using a Master Dark with a non-matching exposure time of ' + cf.masterDark.exposureTime + ' sec' );
            }

            // check flats for light frames
            if ( preprocessGroups[ i ].imageType == ImageType.LIGHT && !cf.masterFlat )
               this.note( "No Master Flat will be used to calibrate the frames" );

         }

         // ----------------------------------------

         // Cosmetic correction check
         if ( preprocessGroups[ i ].CCTemplate && preprocessGroups[ i ].CCTemplate.length > 0 )
         {
            let CC = ProcessInstance.fromIcon( preprocessGroups[ i ].CCTemplate );
            if ( CC == null )
               this.warning( "No such process icon: " + preprocessGroups[ i ].CCTemplate );
            else
            {
               if ( !( CC instanceof CosmeticCorrection ) )
                  this.warning( "The specified process icon does not transport an instance " +
                     "of CosmeticCorrection: " + preprocessGroups[ i ].CCTemplate );
               else
               {
                  if ( !CC.useMasterDark && !CC.useAutoDetect && !CC.useDefectList )
                     this.warning( "The specified CosmeticCorrection instance does not define " +
                        "a valid correction operation: " + preprocessGroups[ i ].CCTemplate );
               }
            }
         }

         // check rejection for bias/dark/flat groups that do not have a master (so they will be integrated)
         if ( preprocessGroups[ i ].imageType != ImageType.LIGHT && !preprocessGroups[ i ].hasMaster )
         {
            let r = preprocessGroups[ i ].rejectionIsGood( this.rejection[ preprocessGroups[ i ].imageType ] );
            if ( !r[ 0 ] ) // if not good
               this.warning( "Integration of " + preprocessGroups[ i ].toString() + ": " + r[ 1 ] ); // reason
         }

         this.cleanUp();
         this.popTop();
      }
   }

   // ----------------------------------------

   if ( !engine.imageRegistration && engine.integrate )
   {
      this.warning( "You decided to integrate your light frames but registration is disabled. " +
         "Ensure that your light frames are already aligned or enable the registration to properly " +
         "align them before generating the master light frames" );
   }

   // ----------------------------------------

   if ( engine.subframesWeightsMethod == WBPPSubframeWeightsMethod.PSFScaleSNR &&
      engine.integrate &&
      !engine.localNormalization )
   {
      this.error( "The integration of light frames using the PSF Scale SNR weighting method requires Local Normalization to be enabled" )
   }

   for ( let i = 0; i < postprocessGroups.length; ++i )
   {
      if ( postprocessGroups[ i ].imageType != ImageType.LIGHT )
         continue;

      this.pushTop();
      this.headerText( postprocessGroups[ i ].toString() );

      let needsIntegration = postprocessGroups[ i ].associatedRGBchannel == undefined || postprocessGroups[ i ].associatedRGBchannel != WBPPAssociatedChannel.COMBINED_RGB;

      if ( this.integrate &&
         needsIntegration &&
         postprocessGroups[ i ].fileItems.length < 3 )
      {
         this.error( "Only " + postprocessGroups[ i ].fileItems.length + " frames provided. Cannot integrate less than 3 light frames" );
      }

      // check rejection for light post-processing groups if integration is enabled
      if ( this.integrate &&
         needsIntegration &&
         postprocessGroups[ i ].imageType == ImageType.LIGHT )
      {
         let r = postprocessGroups[ i ].rejectionIsGood( this.rejection[ postprocessGroups[ i ].imageType ] );
         if ( !r[ 0 ] ) // if not good
            this.warning( "Integration of " + postprocessGroups[ i ].toString() + ": " + r[ 1 ] ); // reason
      }

      // check if enough files are provided when drizzle integration is active
      if ( postprocessGroups[ i ].isDrizzleEnabled() &&
         needsIntegration && postprocessGroups[ i ].fileItems.length < 15 )
      {
         this.warning( "Drizzle Integration of " + postprocessGroups[ i ].toString() + " (" + postprocessGroups[ i ].fileItems.length + "): drizzle requires more than 15 frames to produce optimal results" );
      }
      this.cleanUp();
      this.popTop();
   }

   // ........................................................................

   // Check overscan
   if ( this.overscan.enabled )
   {
      this.pushTop();
      this.headerText( "Check Overscan settings" );

      if ( !this.overscan.isValid() )
         this.error( "Invalid overscan region(s) defined" );
      else if ( this.overscan.enabled && !this.overscan.hasOverscanRegions() )
         this.warning( "Overscan correction has been enabled, but no overscan regions have been defined" );

      this.cleanUp();
      this.popTop();
   }

   // ----------------------------------------

   // Reference frame
   if ( this.hasLightFrames( WBPPGroupingMode.POST ) )
   {
      this.pushTop();
      this.headerText( "Check reference frame settings" );

      // best reference frame checks
      if ( this.imageRegistration )
      {
         if ( this.bestFrameRefernceMethod == WBPPBestRefernenceMethod.MANUAL )
         {
            if ( isEmptyString( this.referenceImage ) )
               this.error( "No registration reference image has been specified." );
            else if ( !File.exists( this.referenceImage ) )
               this.error( "The specified registration reference file does not exist: " + this.referenceImage );
         }
         else
         {
            let keywords = engine.keywords.keywordsForMode( WBPPGroupingMode.POST );
            if ( keywords.length > 0 && this.bestFrameRefernceMethod == WBPPBestRefernenceMethod.AUTO_SINGLE )
            {
               let keywordsList = keywords.map( k => k.name ).join( ", " );
               let kwDesc = keywords.length > 1 ? "s " + keywordsList + "s <b>" : " <b>" + keywordsList + "</b>";
               this.warning( "Master Light frames will be grouped using the keyword" + kwDesc + " and the registration mode is <b>auto</b>: " +
                  "<u>All frames will be aligned on the same reference frame</u>. Consider selecting <b>\"auto by\"</b> registration mode " +
                  "if you want to register these groups separately by keyword" );
            }
         }
      }

      this.cleanUp();
      this.popTop();
   }

   // ----------------------------------------
   // report the required space

   this.pushTop();
   this.headerText( "Check disk space availability" );

   // non zero-size keys
   let keys = Object.keys( engine.WS ).filter( key => ( engine.WS[ key ].size > 0 ) );
   // total space
   let WStot = keys.reduce( ( acc, key ) => ( acc + engine.WS[ key ].size ), 0 );
   // length of the longes non null label
   let pad = keys.reduce( ( acc, key ) => ( Math.max( acc, engine.WS[ key ].label.length ) ), 0 ) + 3;
   // generate the size report for each non null sized key
   keys.forEach( key =>
   {
      this.note( paddedLabel( engine.WS[ key ].label, pad ) + readableSize( engine.WS[ key ].size ) );
   } );

   // check if available space is enough
   if ( !hasOutputDirectoryIssue )
   {
      let availableSpace = File.getAvailableSpace( this.outputDirectory );
      let readableWS = readableSize( WStot ).trim();
      let readableAvailableSpace = readableSize( availableSpace ).trim();
      if ( WStot > availableSpace )
         this.warning( "The required working space (" + readableWS + ") is more than the available space (" + readableAvailableSpace + ")" );
      else if ( WStot >= 0.95 * availableSpace )
         this.warning( "The required working space (" + readableWS + ") is close to the available space (" + readableAvailableSpace + ")" );
      else
         this.note( "The required working space is " + readableWS + " (available " + readableAvailableSpace + ")" );
   }

   this.cleanUp();
   this.popTop();
};

// ----------------------------------------------------------------------------

/**
 * Returns true if diagnostic is empty.
 *
 * @returns
 */
StackEngine.prototype.hasDiagnosticMessages = function()
{
   return this.diagnosticMessages.length > 0;
};

// ----------------------------------------------------------------------------

/**
 * Returns true if diagnostic contains error messages.
 *
 * @returns
 */
StackEngine.prototype.hasErrorMessages = function()
{
   for ( let i = 0; i < this.diagnosticMessages.length; ++i )
      if ( this.diagnosticMessages[ i ].contains( "<b>Error</b>: " ) )
         return true;
   return false;
};

// ----------------------------------------------------------------------------

/**
 * Removes all diagnostic messages.
 *
 */
StackEngine.prototype.clearDiagnosticMessages = function()
{
   this.diagnosticMessages = new Array;
};

// ----------------------------------------------------------------------------

/**
 * Constructor of the diagnostic message Dialog.
 *
 * @param {[String]} messages the messages array to be displayed
 * @param {Boolean} cancelButton true if cancel button should be displayed
 */
function DiagnosticInformationDialog( messages, cancelButton, generateScreenshots )
{
   this.__base__ = Dialog;
   this.__base__();

   let messagesCount = 0;
   let info = "<HTML>";
   info += "<style>";
   info += "body { font-family: DejaVu Sans Mono, Monospace; font-size: 10pt; background: #FFFFFF; border-style: solid; border-color: #777777; border-width: 1pt;}";
   info += "</style><body><div style=\"margin: 4pt;\"";
   for ( let i = 0; i < messages.length; ++i )
   {
      info += messages[ i ] + '<br>';
      if ( !messages[ i ].contains( "====" ) && messages[ i ].length > 0 )
         messagesCount += 1;
   }
   info += "</div></body></HTML>";

   this.infoLabel = new Label( this );
   this.infoLabel.text = format( "%d message(s):", messagesCount );
   this.infoLabel.adjustToContents();
   this.infoLabel.setMaxHeight( this.infoLabel.height );

   this.infoBoxContainer = new Control( this );
   this.infoBoxContainer.setVariableHeight();
   this.infoBoxContainer.setVariableWidth();

   this.infoBox = new WebView( this );
   this.infoBox.useRichText = true;
   this.infoBox.setScaledMinSize( 800, 300 );
   this.infoBox.setHTML( info );

   this.okButton = new PushButton( this );
   this.okButton.defaultButton = true;
   this.okButton.text = cancelButton ? "Continue" : "OK";
   this.okButton.icon = this.scaledResource( ":/icons/ok.png" );
   this.okButton.onClick = function()
   {
      this.dialog.done( StdDialogCode_Ok );
   };

   if ( cancelButton )
   {
      this.cancelButton = new PushButton( this );
      this.cancelButton.defaultButton = true;
      this.cancelButton.text = "Cancel";
      this.cancelButton.icon = this.scaledResource( ":/icons/cancel.png" );
      this.cancelButton.onClick = function()
      {
         this.dialog.done( StdDialogCode_Cancel );
      };
   }

   if ( generateScreenshots )
   {
      this.screenshotsButton = new PushButton( this );
      this.screenshotsButton.defaultButton = true;
      this.screenshotsButton.text = "Generate Screenshots";
      this.screenshotsButton.icon = this.scaledResource( ":/icons/picture-export.png" );
      this.screenshotsButton.onClick = function()
      {
         this.dialog.done( StdDialogCode_GenerateScreenshots );
      };
   }

   this.buttonsSizer = new HorizontalSizer;
   this.buttonsSizer.addStretch();
   this.buttonsSizer.add( this.okButton );
   if ( cancelButton )
   {
      this.buttonsSizer.addSpacing( 8 );
      this.buttonsSizer.add( this.cancelButton );
   }
   if ( generateScreenshots )
   {
      this.buttonsSizer.addSpacing( 8 );
      this.buttonsSizer.add( this.screenshotsButton );
   }

   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;
   this.sizer.add( this.infoLabel );
   this.sizer.addSpacing( 4 );
   this.sizer.add( this.infoBox );
   this.sizer.addSpacing( 8 );
   this.sizer.add( this.buttonsSizer );

   this.ensureLayoutUpdated();
   this.adjustToContents();
   this.width = 200;
   this.height = 100;
   this.setMinSize();

   this.windowTitle = "Diagnostic Messages";
};
DiagnosticInformationDialog.prototype = new Dialog;

function ModalDialog( content, title )
{
   this.__base__ = Dialog;
   this.__base__();

   this.infoBox = new WebView( this );
   this.infoBox.useRichText = true;
   this.infoBox.setScaledMinSize( 400, 200 );
   let info = "<HTML><style>" +
      "body { font-family: DejaVu Sans Mono, Monospace; font-size: 10pt; background: #FFFFFF; border-style: solid; border-color: #777777; border-width: 0pt;}" +
      "</style><body><div style=\"margin: 4pt;\">" + content + "</div></body></HTML>";
   this.infoBox.setHTML( info );

   this.ensureLayoutUpdated();
   this.adjustToContents();
   this.setMinSize();

   this.windowTitle = title;
}
ModalDialog.prototype = new Dialog;

// ----------------------------------------------------------------------------
/**
 * Shows the diagnostic messages dialog, optinoally with a cancel button.
 *
 * @param {Boolean} cancelButton true if a cancel button needs to be shown
 * @returns StdDialogCode_Ok if OK button is pressed, StdDialogCode_Cancel
 *          dialog is closed or cancel button is pressed
 */
StackEngine.prototype.showDiagnosticMessages = function( cancelButton, generateScreenshots )
{
   if ( this.hasErrorMessages() )
   {
      ( new DiagnosticInformationDialog( this.diagnosticMessages, false /*cancelButton*/ , generateScreenshots ) ).execute();
      return false;
   }

   return ( new DiagnosticInformationDialog( this.diagnosticMessages, cancelButton, generateScreenshots ) ).execute();
};

StackEngine.prototype.postGroupStatus = () =>
{

};

// ----------------------------------------------------------------------------

/**
 * Constructor of the Process Logger dialog.
 *
 * @param {ProcessLogger} processLogger the instance of the process logger containing the messages
 */
function ProcessLogDialog( processLogger )
{
   this.__base__ = Dialog;
   this.__base__();

   let info = processLogger.toString();

   this.infoLabel = new Label( this );
   this.infoLabel.text = format( "WBPP steps:" );

   this.infoBox = new TextBox( this );
   this.infoBox.useRichText = true;
   this.infoBox.readOnly = true;
   this.infoBox.styleSheet = this.scaledStyleSheet( "QWidget { font-family: Hack, DejaVu Sans Mono, monospace; font-size: 10pt; color: #0066ff; padding: 4px;" );
   this.infoBox.setScaledMinSize( 800, 300 );
   this.infoBox.text = info;

   this.saveButton = new PushButton( this );
   this.saveButton.defaultButton = true;
   this.saveButton.text = "Save";
   this.saveButton.icon = this.scaledResource( ":/icons/save.png" );
   this.saveButton.onClick = () =>
   {
      // save content to a text file
      var save = new SaveFileDialog;
      save.caption = "Process Dialog Output File";
      save.initialPath = engine.outputDirectory + "/logs/ProcessLogger.txt";
      save.overwritePrompt = true;
      save.filters = [
         [ "*.txt", "*.*" ]
      ];

      if ( save.execute() )
         processLogger.writeToFile( save.fileName );
   };

   this.okButton = new PushButton( this );
   this.okButton.defaultButton = true;
   this.okButton.text = "DONE";
   this.okButton.icon = this.scaledResource( ":/icons/ok.png" );
   this.okButton.onClick = function()
   {
      this.dialog.ok();
   };

   this.buttonsSizer = new HorizontalSizer;
   this.buttonsSizer.addStretch();
   this.buttonsSizer.add( this.saveButton );
   this.buttonsSizer.addScaledSpacing( 8 );
   this.buttonsSizer.add( this.okButton );

   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;
   this.sizer.add( this.infoLabel );
   this.sizer.addSpacing( 4 );
   this.sizer.add( this.infoBox );
   this.sizer.addSpacing( 8 );
   this.sizer.add( this.buttonsSizer );

   this.ensureLayoutUpdated();
   this.adjustToContents();
   this.setMinSize();

   this.windowTitle = "Smart Report";
}

ProcessLogDialog.prototype = new Dialog;

// ----------------------------------------------------------------------------

/**
 * Shows the process logger dialog.
 */
StackEngine.prototype.showProcessLogs = function()
{
   let dialog = new ProcessLogDialog( this.processLogger );
   dialog.execute();
};

// ----------------------------------------------------------------------------

/**
 * Removes all messages from the process logger.
 */
StackEngine.prototype.cleanProcessLog = function()
{
   this.processLogger.clean();
};

// ----------------------------------------------------------------------------

/**
 * Constructor of the execution monitor Dialog.
 */
let ExecutionMonitorDialog = function( asReport, executionCancelRequestCallback, operations, onTerminationRequest )
{
   this.__base__ = Dialog;
   this.__base__();

   let
   {
      elapsedTimeToString
   } = WBPPUtils.shared();

   this.columnSizesAdjusted = false
   this.cancelRequested = false;

   let readyIcon = ":/bullets/bullet-circle-void.svg";
   let doneIcon = ":/bullets/bullet-check-green.svg";
   let doneWithWarningsIcon = ":/bullets/bullet-circle-purple.svg";
   let runningIcon = ":/bullets/bullet-triangle-blue.svg";
   let failedIcon = ":/bullets/bullet-circle-red.svg";
   let cancelIcon = ":/bullets/bullet-cross.svg";

   this.updateWithOperations = ( operations ) =>
   {
      // ensure we're not invoking this function after the interruption of a timer
      if ( this.updatingWithOperations )
         return;
      this.updatingWithOperations = true;

      let activeNode;
      this.infoTreeBox.clear();
      for ( let i = 0; i < operations.length; i++ )
      {
         let node = new TreeBoxNode;
         // operation index
         node.setText( 0, operations[ i ].name );
         // group info
         node.setText( 1, operations[ i ].groupDescription );
         // elapsed
         if ( operations[ i ].status == OperationBlockStatus.READY )
            node.setText( 2, "" );
         else if ( operations[ i ].status == OperationBlockStatus.RUNNING )
            node.setText( 2, elapsedTimeToString( operations[ i ].elapsed.value ) );
         else
            node.setText( 2, elapsedTimeToString( operations[ i ].executionTime ) );
         // status
         switch ( operations[ i ].status )
         {
            case OperationBlockStatus.READY:
               operations[ i ].updateGroupDescription();
               node.setIcon( 3, readyIcon );
               break;
            case OperationBlockStatus.RUNNING:
               activeNode = node;
               if ( this.cancelRequested )
                  node.setText( 3, "canceling..." );
               else
                  node.setText( 3, "running" );
               node.setIcon( 3, runningIcon );
               break;
            case OperationBlockStatus.DONE:
               node.setText( 3, "success" );
               if ( operations[ i ].hasWarnings )
                  node.setIcon( 3, doneWithWarningsIcon );
               else
                  node.setIcon( 3, doneIcon );
               break;
            case OperationBlockStatus.FAILED:
               node.setText( 3, "failed" );
               node.setIcon( 3, failedIcon );
               break;
            case OperationBlockStatus.CANCELED:
               node.setText( 3, "canceled" );
               node.setIcon( 3, cancelIcon );
               break;
         }
         // notes
         if ( operations[ i ].status != OperationBlockStatus.RUNNING )
         {
            node.setText( 4, operations[ i ].statusMessage );
         }

         this.infoTreeBox.add( node );

         if ( activeNode )
         {
            activeNode.setTextColor( 0, 0x0000BB );
            activeNode.setTextColor( 1, 0x0000BB );
            activeNode.setTextColor( 2, 0x0000BB );
            if ( this.cancelRequested )
               activeNode.setTextColor( 3, 0xBB0000 );
            else
               activeNode.setTextColor( 3, 0x0000BB );
         }
      }
      if ( !this.columnSizesAdjusted )
      {
         this.columnSizesAdjusted = true;
         this.infoTreeBox.adjustColumnWidthToContents( 0 );
         this.infoTreeBox.adjustColumnWidthToContents( 1 );

      }
      // elapsed always get adjusted
      this.infoTreeBox.adjustColumnWidthToContents( 2 );
      // status always get adjusted
      this.infoTreeBox.adjustColumnWidthToContents( 3 );


      // scroll follows the current active node in automation mode
      if ( activeNode )
         this.infoTreeBox.setNodeIntoView( activeNode );

      this.updatingWithOperations = false;
   };

   this.updateRunningOperation = ( operations ) =>
   {
      if ( this.updatingWithOperations )
         return;
      this.updatingWithOperations = true;

      for ( let i = 0; i < operations.length; i++ )
         if ( operations[ i ].status == OperationBlockStatus.RUNNING )
            if ( i < this.infoTreeBox.numberOfChildren )
            {
               let node = this.infoTreeBox.child( i );
               node.setText( 2, elapsedTimeToString( operations[ i ].elapsed.value ) );
               if ( this.cancelRequested )
               {
                  node.setText( 3, "canceling..." );
                  node.setTextColor( 3, 0xBB0000 );
                  this.infoTreeBox.adjustColumnWidthToContents( 3 );
               }
               break;
            }
      this.updatingWithOperations = false;
   };

   this.infoBoxContainer = new Control( this );
   this.infoBoxContainer.setVariableHeight();
   this.infoBoxContainer.setVariableWidth();

   this.infoTreeBox = new TreeBox( this );
   this.infoTreeBox.rootDecoration = false;
   this.infoTreeBox.headerVisible = true;
   this.infoTreeBox.headerSorting = false;
   this.infoTreeBox.alternateRowColor = true;
   this.infoTreeBox.numberOfColumns = 5;

   this.infoTreeBox.setHeaderText( 0, "Operation" );
   this.infoTreeBox.setHeaderText( 1, "Group involved" );
   this.infoTreeBox.setHeaderText( 2, "Elapsed" );
   this.infoTreeBox.setHeaderText( 3, "Status" );
   this.infoTreeBox.setHeaderText( 4, "Note" );

   if ( executionCancelRequestCallback )
   {
      this.cancelButton = new PushButton( this );
      this.cancelButton.defaultButton = true;
      this.cancelButton.text = "Cancel";
      this.cancelButton.icon = this.scaledResource( ":/icons/cancel.png" );
      this.cancelButton.onClick = () =>
      {
         if ( this.cancelRequested )
         {
            this.ok();
         }
         else
         {
            if ( onTerminationRequest )
               onTerminationRequest();
            executionCancelRequestCallback();
            this.cancelRequested = true;
         }
      };
   }

   if ( asReport )
   {
      this.okButton = new PushButton( this );
      this.okButton.defaultButton = true;
      this.okButton.text = "DONE";
      this.okButton.onClick = () =>
      {
         this.ok();
      };
   }

   //

   this.noteLabel = new Label( this );
   let f = this.noteLabel.font;
   f.bold = true;
   this.noteLabel.font = f;
   this.noteLabel.textAlignment = TextAlign_Left | TextAlign_VertCenter;

   //

   this.buttonsSizer = new HorizontalSizer;
   this.buttonsSizer.add( this.noteLabel );
   this.buttonsSizer.addStretch();
   if ( executionCancelRequestCallback )
   {
      this.buttonsSizer.addSpacing( 8 );
      this.buttonsSizer.add( this.cancelButton );
   }
   if ( asReport )
   {
      this.buttonsSizer.addSpacing( 8 );
      this.buttonsSizer.add( this.okButton );
   }

   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;
   this.sizer.add( this.infoTreeBox );
   this.sizer.addSpacing( 8 );
   this.sizer.add( this.buttonsSizer );

   this.ensureLayoutUpdated();
   this.adjustToContents();
   this.setMinSize();

   this.windowTitle = "WBPP Execution Monitor";

   // init with operations
   if ( operations )
      this.updateWithOperations( operations );

   // -

   this.onReturn = ( retVal ) =>
   {
      this.storeDialogFrame();
      if ( retVal == 0 && executionCancelRequestCallback )
      {
         /* dialog dismissed without pressing CANCEL button */
         if ( onTerminationRequest )
            onTerminationRequest();
         executionCancelRequestCallback();
      }
   };

   this.adjustDialogFrame = () =>
   {
      // frame has already been adjusted once
      if ( ExecutionMonitorDialog.prototype.__frame__ )
         return;

      // get the rect of the last node
      let width = 680;
      let height = 300;
      if ( this.infoTreeBox.numberOfChildren > 0 )
      {
         let lastNode = this.infoTreeBox.child( this.infoTreeBox.numberOfChildren - 1 );
         let frame = this.infoTreeBox.nodeRect( lastNode );
         width = frame.x1;
         height = frame.y1;
      }

      width = Math.min( this.availableScreenRect.width - 100, width + 120 );
      height = Math.min( this.availableScreenRect.height - 200, height + 200 );

      let x = ( this.availableScreenRect.width - width ) / 2;
      let y = ( this.availableScreenRect.height - height ) / 2;
      this.position = new Point( x, y );
      this.width = width;
      this.height = height;

      this.storeDialogFrame();
   };

   this.storeDialogFrame = () =>
   {
      let frame = {
         position: new Point( this.position ),
         width: this.width,
         height: this.height
      };
      ExecutionMonitorDialog.prototype.__frame__ = frame;
   };

   this.restoreDialogFrame = () =>
   {
      let frame = ExecutionMonitorDialog.prototype.__frame__;
      if ( frame )
      {
         this.position = new Point( frame.position );
         this.width = frame.width;
         this.height = frame.height;
      }
   };

   this.restoreDialogFrame();
};

ExecutionMonitorDialog.prototype = new Dialog;

// ----------------------------------------------------------------------------
// FILE ITEM MANAGEMENT
// ----------------------------------------------------------------------------

/**
 *  Finds the group matching the provided criterias. Virtual groups are excluded from the result.
 *
 * @param {ImageType} imageType
 * @param {String} filter
 * @param {Numeric} binning
 * @param {Numeric} exposureTime
 * @param {Object} size {width:Numeric, height:Numeric}
 * @param {Boolean} isMaster
 * @param {Boolean} isCFA
 * @param {Numeric} darkExposureTolerance
 * @param {Numeric} lightExposureTolerance
 * @param {{String: String}} keywords { key: value } keywords object
 * @param {Boolean} strictKeywordsMatching
 * @param {WBPPGroupingMode} mode
 * @returns
 */
StackEngine.prototype.findGroup = function( imageType, filter, binning, exposureTime, size, isMaster, isCFA, darkExposureTolerance, lightExposureTolerance, keywords, strictKeywordsMatching, mode )
{
   // memoization check to speed up the match
   let keywordsForMode = engine.keywords.filterKeywordsForMode( keywords, mode );
   let memoizationKey = "" + imageType + filter + binning + exposureTime + size.width + size.height + isMaster + isCFA + darkExposureTolerance + lightExposureTolerance + JSON.stringify( keywordsForMode ) + JSON.stringify( keywords ) + strictKeywordsMatching + mode;
   let memoizedGroup = engine.findGroupMemoization[ memoizationKey ];
   if ( memoizedGroup != undefined )
      return memoizedGroup;

   // NOTE: since there could be more than one group matching the same parameters but a different
   // number of keywords, we lopp through all groups and we collect all groups that matches.
   // If there is more than one matching group then we do a final loop with the results and
   // we select the group that has the highest number of matching keywords
   let groupIndx = [];
   let groups = this.groupsManager.groupsForMode( mode );
   for ( let i = 0; i < groups.length; ++i )
   {
      // in case we're searching a group for a master frame then the tolerance is reduced to the maximum
      // precision in case the current group has a master too.
      // We do this because the tolerance has a meaning only when adding dark frames or matching
      // dark frame groups that contains only dark frames.
      let tolerance = isMaster ? CONST_MIN_EXPOSURE_TOLERANCE : darkExposureTolerance;
      if ( !groups[ i ].isVirtual() && groups[ i ].sameParameters( imageType, filter, binning, exposureTime, size, isCFA, tolerance, lightExposureTolerance, mode ) )
         groupIndx.push( i );
   }
   // return the group that best matches the keywords

   let bestgroupIndex = this.bestGroupMatchingKeywordsIndex( groups, groupIndx, keywordsForMode, strictKeywordsMatching );
   let group = bestgroupIndex == -1 ? undefined : groups[ bestgroupIndex ];
   engine.findGroupMemoization[ memoizationKey ] = group;
   return group;
};

// ----------------------------------------------------------------------------

/**
 * Implements the strategy to select the best candidate group acconrdingly to the
 * provided keywords. If more than one group is found that matches the same number of
 * keywords then the groups are sorted following the keywords order and the
 * first gets selected.
 *
 * @param {[FrameGroup]} groups
 * @param {Numeric} groupIndx indexes of candidate groups in groups array
 * @param {{String: String}} keywords { key: value } keywords object
 * @param {Boolean} strictKeywordsMatching if true keywords values must match exactly, including
 *                                         the keywords that have no values
 * @returns
 */
StackEngine.prototype.bestGroupMatchingKeywordsIndex = function( groups, groupIndx, keywords, strictKeywordsMatching )
{
   // return -1 if no group matches
   if ( groupIndx.length == 0 )
      return -1;
   // find the groups with the highest matching count
   let maxMatch = 0;
   let matchingIndexes = [];
   groupIndx.forEach( i =>
   {
      let matchCount = groups[ i ].keywordsMatchCount(
         keywords,
         strictKeywordsMatching, /* strictDirectMatching */
         strictKeywordsMatching /* strictInverseMatching */
      );

      if ( matchCount > maxMatch )
      {
         maxMatch = matchCount;
         matchingIndexes = [ i ];
      }
      else if ( matchCount == maxMatch )
         matchingIndexes.push( i );
   } );

   if ( matchingIndexes.length == 0 )
      return -1; // no matching groups

   if ( matchingIndexes.length == 1 )
      return matchingIndexes[ 0 ]; // one matching group

   // initialize an array with matching groups and index
   let groupsWithIndex = groups.map( ( group, index ) => (
   {
      priority: 0,
      keywords: Object.keys( group.keywords ),
      group: group
   } ) );
   // update the index of a group
   engine.keywords.names().forEach( ( name, index ) =>
   {
      groupsWithIndex.forEach( ( g ) =>
      {
         if ( g.keywords.indexOf( name ) != -1 )
         {
            group.priority += 1 << ( index + 1 );
         }
      } );
   } );
   // return the group with the lowest index
   groupsWithIndex.sort( ( a, b ) => a.priority - b.priority );
   return groupsWithIndex[ 0 ];
};

// ----------------------------------------------------------------------------
/**
 * Returns the list of groups linked to the given parent group ID. Optionally,
 * a specified group can be excluded from the list.
 * @param {*} parentGroupID the parent group ID that all returned groups are linked to
 * @param {*} excludedGroup the group to be excluded from the returned list
 * @returns
 */
StackEngine.prototype.getLinkedGroups = function( parentGroupID, excludedGroup )
{
   // expect to find 3 linked groups
   let groups = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST );

   return groups.reduce( ( acc, group ) =>
      {
         if ( group.linkedGroupID == parentGroupID )
            if ( excludedGroup == undefined || ( group.id != excludedGroup.id ) )
               acc.push( group );
         return acc;
      },
      [] );
}

// ----------------------------------------------------------------------------

/**
 * Performs a sanity check on the file at the given filePath.
 *
 * @param {String} filePath
 * @returns
 */
StackEngine.prototype.checkFile = function( filePath )
{
   let
   {
      isEmptyString
   } = WBPPUtils.shared();

   // path must not be an empty string
   if ( isEmptyString( filePath ) )
      return {
         success: false,
         message: "Empty file path"
      };

   // file must exist
   if ( !File.exists( filePath ) )
      return {
         success: false,
         message: "File not found: " + filePath
      }

   // file must not be already added. By default the grouping mode WBPPGoupingMode = .pre is used for this check
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );
   for ( let i = 0; i < groups.length; ++i )
      for ( let j = 0; j < groups[ i ].fileItems.length; ++j )
         if ( groups[ i ].fileItems[ j ].filePath == filePath )
            return {
               success: false,
               message: "File " + filePath + " has already been added as " + StackEngine.imageTypeToString( groups[ i ].imageType ) + " frame"
            }

   return {
      success: true
   };
};

// ----------------------------------------------------------------------------

/**
 * Adds a new file with the given properties.
 *
 * @param {String} filePath
 * @param {Imagetype} imageType
 * @param {String} filter
 * @param {Numeric} binning
 * @param {Numeric} exposureTime
 * @param {Object} overrideSize {width: Numeric, height: Numeric}, optional
 * @param {Boolean} overrideCFA
 * @param {*} customModes
 * @returns
 */
StackEngine.prototype.addFile = function( filePath, imageType, filter, binning, exposureTime, overrideSize, overrideCFA, customModes )
{
   let
   {
      getImageSize,
      keywords,
      smartNaming
   } = WBPPUtils.shared();

   filePath = filePath.trim();

   let checkResult = this.checkFile( filePath );
   if ( !checkResult.success )
      return checkResult;

   let forcedType = imageType != undefined && imageType != ImageType.UNKNOWN;
   if ( !forcedType )
      imageType = ImageType.UNKNOWN;

   if ( filter == "?" )
      filter = undefined;
   let forcedFilter = filter != undefined && filter != "?"; // ### see Add Custom Frames dialog

   let forcedBinning = binning != undefined && binning > 0;
   if ( !forcedBinning )
      binning = 0;

   let forcedExposureTime = imageType == ImageType.BIAS || exposureTime != undefined && exposureTime > 0;
   if ( !forcedExposureTime || imageType == ImageType.BIAS )
      exposureTime = 0;

   // set the item size
   let size = overrideSize != undefined ? overrideSize : getImageSize( filePath, this.inputHints() );
   if ( !size )
   {
      return {
         success: false,
         message: "Unable to detect file size"
      };
   }

   // assume image is NOT CFA unless the bayer pattern is found in the header
   let isCFA = false;

   // initially assume that the file is not a master file
   let isMaster = false;

   let result = keywords.readFileKeywords( filePath );
   if ( !result.success )
      return result;

   if ( engine.smartNamingOverride )
   {
      for ( let i = 0; i < result.keywords.length; ++i )
      {
         let keywordName = result.keywords[ i ].name;
         let value = smartNaming.getCustomKeyValueFromPath( keywordName, filePath );
         if ( value != undefined )
            result.keywords[ i ].value = value;
      }

      // append the relevant keywords that are found in the file
      let injectedKeywords = [ "IMAGETYP", "FILTER", "INSFLNAM", "XBINNING", "BINNING", "CCDBINX", "EXPTIME", "EXPOSURE", "BAYERPAT" ];
      for ( let i = 0; i < injectedKeywords.length; i++ )
      {
         let value = smartNaming.getCustomKeyValueFromPath( injectedKeywords[ i ], filePath );
         if ( value != undefined )
         {
            // a relevant keyword has found in the filePath, append it to the list
            result.keywords.push(
            {
               name: injectedKeywords[ i ],
               value: value,
               strippedValue: value
            } );
         }
      }
   }

   // initialize the custom overscan info
   let overscan = new Overscan();

   // initialize the keywords extracting them from the path
   let preovscw;
   let preovsch;
   let solverParams = {
      observationDate: "",
      ra: NaN,
      dec: NaN,
      pixelSize: NaN,
      focalLength: NaN
   }
   let fileKeywords = {};
   let parsedValue;
   for ( let i = 0; i < result.keywords.length; ++i )
   {
      let value = result.keywords[ i ].strippedValue.trim();
      let name = result.keywords[ i ].name;
      if ( name === "HISTORY" ) continue;

      // save the file keywords in the file item
      fileKeywords[ name ] = value;

      switch ( name )
      {
         case "IMAGETYP":
            if ( !forcedType )
               imageType = StackEngine.imageTypeFromKeyword( value );
            isMaster = StackEngine.isMasterFromKeyword( value );
            break;
         case "FILTER":
         case "INSFLNAM":
            if ( !forcedFilter )
               filter = value;
            break;
         case "XBINNING":
         case "BINNING":
         case "CCDBINX":
            if ( !forcedBinning )
               binning = parseInt( value );
            break;
         case "EXPTIME":
         case "EXPOSURE":
            if ( !forcedExposureTime && imageType != ImageType.BIAS )
               exposureTime = parseFloat( value );
            break;
         case "BAYERPAT":
            isCFA = true;
            break;
         case "PREOVSCW": // the master flat pre-overscan width
            preovscw = parseFloat( value );
            break;
         case "PREOVSCH": // the master flat pre-overscan height
            preovsch = parseFloat( value );
            break;
         case "DATE-OBS": // observation time
            {
               let d = new Date( value );
               if ( isFinite( d.getTime() ) ) // if the ISO 8601 representation is valid
                  solverParams.observationDate = format( "%04d-%02d-%02dT%02d:%02d:%06.3f",
                        d.getUTCFullYear(), d.getUTCMonth()+1, d.getUTCDate(),
                        d.getUTCHours(), d.getUTCMinutes(), d.getUTCSeconds()+d.getUTCMilliseconds()/1000 );
            }
            break;
         case "RA": // right ascension of the center of the image
            solverParams.ra = parseFloat( value );
            break;
         case "DEC": // declination of the center of the image
            solverParams.dec = parseFloat( value );
            break;
         case "OBJCTRA": // right ascension of the center of the image (compatibility)
            if ( isNaN( solverParams.ra ) ) // RA takes precedence
            {
               let angle = DMSangle.FromString( value, 0, 24 );
               if ( angle != null )
                  solverParams.ra = 15 * angle.GetValue();
            }
            break;
         case "OBJCTDEC": // declination of the center of the image (compatibility)
            if ( isNaN( solverParams.dec ) ) // DEC takes precedence
            {
               let angle = DMSangle.FromString( value, 0, 90 );
               if ( angle != null )
                  solverParams.dec = angle.GetValue();
            }
            break;
         case "XPIXSZ": // pixel size in micron, including binning
            parsedValue = parseFloat( value );
            if ( !isNaN( parsedValue ) && parsedValue > 0 )
               solverParams.pixelSize = parsedValue;
            break;
         case "FOCALLEN": // focal length in mm
            parsedValue = parseFloat( value );
            if ( !isNaN( parsedValue ) && parsedValue > 0 )
               solverParams.focalLength = parsedValue;
            break;
         default:
            // handle overscan
            overscan.updateWithKeyword( name, value );
      }
   }

   // smart naming: extract type binning, filter and duration from filePath if needed
   if ( imageType == ImageType.UNKNOWN )
      imageType = smartNaming.geImageTypeFromPath( filePath );

   if ( imageType == ImageType.UNKNOWN )
   {
      this.diagnosticMessages.push( "Unable to determine frame type [assuming LIGHT frame]: " + filePath );
      imageType = ImageType.LIGHT;
   }

   if ( !forcedBinning && binning == 0 )
      binning = smartNaming.getBinningFromPath( filePath );
   if ( !forcedFilter && filter == undefined )
      filter = smartNaming.getFilterFromPath( filePath ) || "NoFilter";
   if ( !forcedExposureTime && imageType !== ImageType.BIAS && exposureTime == 0 )
      exposureTime = smartNaming.getExposureTimeFromPath( filePath );

   // if master was not found in the FITS header then check if file is a master from the filePath
   if ( !isMaster )
   {
      let searchPath = this.detectMasterIncludingFullPath ? filePath : File.extractName( filePath );
      isMaster = smartNaming.isMasterFromPath( searchPath );
   }

   // check for CFA override
   if ( overrideCFA !== undefined )
      isCFA = overrideCFA;

   // light frames are never used as masters
   isMaster = isMaster && ( imageType != ImageType.LIGHT );

   // set the custom matching sizes for the master flat
   let matchingSizes = {};
   if ( isMaster && imageType == ImageType.FLAT && preovscw != undefined && preovsch != undefined )
   {
      matchingSizes[ WBPPGroupingMode.PRE ] = {
         width: preovscw,
         height: preovsch
      }
   }

   let item = new FileItem(
      filePath,
      imageType,
      filter,
      binning,
      exposureTime,
      fileKeywords,
      size,
      matchingSizes,
      isCFA,
      isMaster,
      undefined, /* item keywords are not defined yet */
      solverParams,
      overscan
   );
   // once created we can update the keywords
   item.updateKeywords();

   this.groupsManager.addFileItem( item, undefined /* custom keywords */ , customModes );

   return {
      success: true
   };
};

// ----------------------------------------------------------------------------

/**
 * Adds a bias Frame.
 *
 * @param {String} filePath
 * @returns
 */
StackEngine.prototype.addBiasFrame = function( filePath )
{
   return this.addFile( filePath, ImageType.BIAS );
};

// ----------------------------------------------------------------------------

/**
 * Adds a dark frame.
 *
 * @param {String} filePath
 * @returns
 */
StackEngine.prototype.addDarkFrame = function( filePath )
{
   return this.addFile( filePath, ImageType.DARK );
};

// ----------------------------------------------------------------------------

/**
 * Adds a flat frame.
 *
 * @param {String} filePath
 * @returns
 */
StackEngine.prototype.addFlatFrame = function( filePath )
{
   return this.addFile( filePath, ImageType.FLAT );
};

// ----------------------------------------------------------------------------

/**
 * Adds a light frame.
 *
 * @param {String} filePath
 * @returns
 */
StackEngine.prototype.addLightFrame = function( filePath )
{
   return this.addFile( filePath, ImageType.LIGHT );
};

// ----------------------------------------------------------------------------
/**
 * Rebuilds groups and regenerate the execution pipeline.
 *
 */
StackEngine.prototype.rebuild = function()
{
   // engine rebuild is always performed with in-memory data
   let et = new ElapsedTime();
   this.reconstructGroups( true );
   console.noteln();
   console.noteln( "reconstruct: ", et.text );
   et.reset();
   this.buildExecutionPipeline();
   console.noteln( "pipeline: ", et.text );
   console.noteln();
};

/**
 * Reconstruction is performed by scanning all files in pre-processing groups
 * and re-add them one by one. Before reconsructing, all properties for each
 * group is saved and after the reconstruction the properties are restored
 * if groups with same ID have been created.
 *
 * @param {*} formCache true if file item properties have to be read from memory
 *                      instead of being read from disk.
 */
StackEngine.prototype.reconstructGroups = function( formCache )
{
   // reset the memoization cache
   engine.findGroupMemoization = {};

   // clean up null values
   this.removePurgedElements();

   // save the current group properties to be restored for the unchanged groups
   this.groupsManager.cacheGroupsProperties();

   // get all groups in pre-processing state, these contain all files added to WBPP
   let fileItems = {};
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   // flatten files, ensure uniquness
   let filePaths = [];
   for ( let i = 0; i < groups.length; ++i )
      for ( let j = 0; j < groups[ i ].fileItems.length; ++j )
      {
         let fileItem = groups[ i ].fileItems[ j ];
         // ensure uniquness
         if ( fileItems[ fileItem.filePath ] == undefined )
         {
            fileItems[ fileItem.filePath ] = fileItem;
            filePaths.push( fileItem.filePath );
         }
      }

   // remove all groups
   this.groupsManager.clear();

   // re-add files one by one
   for ( let i = 0; i < filePaths.length; ++i )
   {
      let filePath = filePaths[ i ];
      let fileItem = fileItems[ filePath ];
      if ( fileItem )
      {
         if ( formCache )
         {
            // update the keywords in case they have changed
            fileItem.updateKeywords();
            this.groupsManager.addFileItem( fileItem, undefined /* custom keywords */ , [ WBPPGroupingMode.PRE ] /* customModes */ );
         }
         else
         {
            this.addFile(
               fileItem.filePath,
               fileItem.imageType,
               fileItem.filter,
               fileItem.binning,
               fileItem.exposureTime,
               fileItem.size,
               undefined, /* override CFA */
               [ WBPPGroupingMode.PRE ] /* customModes */
            );
         }
      }
   }

   // ready to restore the group properties
   this.groupsManager.restoreGroupsPropertiesFromCache( WBPPGroupingMode.PRE );

   // reconstruct post process groups after PRE has been completed
   this.reconstructPostProcessGroups();

   // ready to restore the group properties
   this.groupsManager.restoreGroupsPropertiesFromCache( WBPPGroupingMode.POST );

   // sort files by name
   this.groupsManager.groups.forEach( g =>
   {
      if ( g.mode == WBPPGroupingMode.PRE )
      {
         // sort but keep the master file in first position
         let master;
         if ( g.hasMaster )
         {
            master = g.fileItems[ 0 ];
            g.fileItems.shift();
         }
         g.fileItems.sort( ( a, b ) =>
         {
            return a.filePath.localeCompare( b.filePath );
         } )
         if ( master )
         {
            g.fileItems.unshift( master );
         }
      }
   } );

   // aux: returns the list of groups with the given type and mode, adding the
   // counter property
   let _getGroups = ( type, mode ) =>
   {
      // get the list of sorted groups
      let groups = engine.getSortedGroupsOfType( type, mode );
      // add the counter property (GUI purposes)
      groups.forEach( ( g, i ) =>
      {
         g.__counter__ = i + 1;
      } );
      return groups;
   }

   // sorted groups
   let bias = _getGroups( ImageType.BIAS, WBPPGroupingMode.PRE );
   let dark = _getGroups( ImageType.DARK, WBPPGroupingMode.PRE );
   let flat = _getGroups( ImageType.FLAT, WBPPGroupingMode.PRE );
   let light_pre = _getGroups( ImageType.LIGHT, WBPPGroupingMode.PRE );
   let light_post = _getGroups( ImageType.LIGHT, WBPPGroupingMode.POST );
   // clear and re-add groups in sorted order
   this.groupsManager.clear();
   bias.forEach( g => this.groupsManager.groups.push( g ) );
   dark.forEach( g => this.groupsManager.groups.push( g ) );
   flat.forEach( g => this.groupsManager.groups.push( g ) );
   light_pre.forEach( g => this.groupsManager.groups.push( g ) );
   light_post.forEach( g => this.groupsManager.groups.push( g ) );
}; /** StackEngine.prototype.reconstructGroups */

/**
 * Reconstruction is performed by scanning all files in pre-processing groups
 * and re-add them one by one. Before reconsructing, all properties for each
 * group is saved and after the reconstruction the properties are restored
 * if groups with same ID have been created.
 */
StackEngine.prototype.reconstructPostProcessGroups = function()
{
   // clean up null values
   this.removePurgedElements();

   // get all groups in pre-processing state, these contain all files added to WBPP
   let fileItems = [];
   let preGroups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   // flatten files and store the overridden CFA property
   for ( let i = 0; i < preGroups.length; ++i )
      if ( preGroups[ i ].imageType == ImageType.LIGHT )
      {
         for ( let j = 0; j < preGroups[ i ].fileItems.length; ++j )
            fileItems.push(
            {
               fileItem: preGroups[ i ].fileItems[ j ],
               isCFA: preGroups[ i ].isCFA
            } );
      }

   // purge post-process groups
   let postGroups = this.groupsManager.groupsForMode( WBPPGroupingMode.POST );
   postGroups.forEach( g =>
   {
      g.__purged__ = true;
   } );
   this.removePurgedElements();

   // re-add file items one by one.
   for ( let i = 0; i < fileItems.length; ++i )
   {
      let fileItem = fileItems[ i ].fileItem;
      if ( fileItem )
      {
         // override the POST matching size depending on the overscan settings
         if ( this.overscan.enabled )
         {
            fileItem.matchingSizes[ WBPPGroupingMode.POST ] = {
               width: engine.overscan.imageRect.x1 - engine.overscan.imageRect.x0,
               height: engine.overscan.imageRect.y1 - engine.overscan.imageRect.y0
            }
         }
         else
            fileItem.matchingSizes[ WBPPGroupingMode.POST ] = fileItem.matchingSizes[ WBPPGroupingMode.PRE ];

         let isCFA = fileItems[ i ].isCFA;
         this.groupsManager.addFileItem(
            fileItem,
            undefined /* custom keywords */ ,
            [ WBPPGroupingMode.POST ],
            isCFA /* custom CFA */
         );
      }
   }

   // retrieve the groups again and insert the associated groups for the separated RGB channels
   let updatedPostGroups = this.groupsManager.groups.reduce( ( acc, group ) =>
   {
      // let preprocessing groups unchanged
      if ( group.mode == WBPPGroupingMode.PRE )
      {
         acc.push( group );
         return acc;
      }

      // POST processing group: if the group is CFA then it remains if the debayer mode is combined RGB channels or both
      if ( !group.isCFA || this.debayerOutputMethod == WBPPDebayerOutputMode.COMBINED )
      {
         acc.push( group );
         return acc;
      }

      // hide and deactivate the post RGB group if only the separated RGB channels are generated
      if ( this.debayerOutputMethod == WBPPDebayerOutputMode.SEPARATED )
      {
         group.isHidden = true;
         group.isActive = false;
      }
      acc.push( group );
      let associatedChannels = [];
      if ( engine.debayerActiveChannelR )
         associatedChannels.push( WBPPAssociatedChannel.R );
      if ( engine.debayerActiveChannelG )
         associatedChannels.push( WBPPAssociatedChannel.G );
      if ( engine.debayerActiveChannelB )
         associatedChannels.push( WBPPAssociatedChannel.B );

      associatedChannels.forEach( associatedRGBchannel =>
      {
         let associatedGroup = new FrameGroup(
            group.imageType,
            group.filter,
            group.binning,
            group.exposureTime,
            group.size,
            false, /* isCFA: group is managed as MONO */
            null, /* firstItem: group is initially empty */
            false, /* hasMaster */
            group.keywords,
            group.mode,
            associatedRGBchannel,
            group.id, /* the parent linked group */
            undefined /* drizzle data */
         );
         associatedGroup.exposureTimes = group.exposureTimes.splice();
         // associate the same files, the function "activeFrames()" will return the associated file paths
         // for the given RGB channel
         associatedGroup.fileItems = group.fileItems;
         acc.push( associatedGroup );
      } );

      // recombination is performed only if all channels are active
      if ( engine.recombineRGB && associatedChannels.length == 3 && engine.integrate )
      {
         let associatedGroup = new FrameGroup(
            group.imageType,
            group.filter,
            group.binning,
            group.exposureTime,
            group.size,
            true, /* isCFA: group is managed as CFA */
            null, /* firstItem: group is initially empty */
            false, /* hasMaster */
            group.keywords,
            group.mode,
            WBPPAssociatedChannel.COMBINED_RGB,
            group.id /* the parent linked group */
         );
         associatedGroup.isHidden = false;
         associatedGroup.exposureTimes = group.exposureTimes.splice();
         acc.push( associatedGroup );
      }

      return acc;
   }, [] )

   this.groupsManager.groups = updatedPostGroups;
};

// ----------------------------------------------------------------------------

/**
 * Apply the provided CFA settings to the pre-processing groups.
 *
 * @param {ImageType} imageType
 * @param {Boolean} isCFA
 * @param {Debayer.prototype} CFAPattern
 * @param {Debayer.prototype} debayerMethod
 * @param {Boolean} separateCFAFlatScalingFactors
 */
StackEngine.prototype.applyCFASettings = function( imageType, isCFA, CFAPattern, debayerMethod, separateCFAFlatScalingFactors )
{
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   for ( let i = 0; i < groups.length; ++i )
      if ( groups[ i ].imageType == imageType )
      {
         groups[ i ].isCFA = isCFA;
         groups[ i ].CFAPattern = CFAPattern;
         groups[ i ].debayerMethod = debayerMethod;

         if ( imageType == ImageType.FLAT )
            groups[ i ].separateCFAFlatScalingFactors = separateCFAFlatScalingFactors;
      }
};

// ----------------------------------------------------------------------------

/**
 * Apply the provided output pedestal value to the pre-processing light frame groups.
 *
 * @param {Float} limit the output pedestal limit
 */
StackEngine.prototype.applyOutputPedestalLimit = function( mode, pedestal, limit )
{
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   for ( let i = 0; i < groups.length; ++i )
      if ( groups[ i ].imageType == ImageType.LIGHT )
      {
         groups[ i ].lightOutputPedestalMode = mode;
         groups[ i ].lightOutputPedestal = pedestal;
         groups[ i ].lightOutputPedestalLimit = limit;
      }
};

// ----------------------------------------------------------------------------

/**
 * Apply the provided Cosmetic Correction template icon to all pre-processing light frame groups.
 *
 * @param {String} templateIconName CosmeticCorrection template icon
 */
StackEngine.prototype.applyCCTemplate = function( templateIconName )
{
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   for ( let i = 0; i < groups.length; ++i )
      if ( groups[ i ].imageType == ImageType.LIGHT )
         groups[ i ].CCTemplate = templateIconName;
};

// ----------------------------------------------------------------------------

/**
 * Apply the drizzle configuration of the given group to all post-caliobration groups
 *
 * @param {String} templateIconName CosmeticCorrection template icon
 */
StackEngine.prototype.applyDrizzleConfiguration = function( referenceGroup )
{
   let groups = this.groupsManager.groupsForMode( WBPPGroupingMode.POST );

   for ( let i = 0; i < groups.length; ++i )
   {
      // skip virtual groups since they are
      groups[ i ].setDrizzleData( referenceGroup.drizzleData )
   }
};

// ----------------------------------------------------------------------------

/**
 * Returns the FITS coordinate convention hints given the current global option status
 *
 * @return {*}
 */
StackEngine.prototype.coordinateConventionHints = function()
{
   let coordinateConvention = ""
   if ( this.fitsCoordinateConvention != 0 )
      coordinateConvention = ( this.fitsCoordinateConvention == 1 ? " up-bottom" : " bottom-up" )
   return coordinateConvention;
};

/**
 * Returns the input hits to keep the white balance convention if the global setting is set
 * to do so.
 */
StackEngine.prototype.whiteBalanceHints = function()
{
   if ( this.preserveWhiteBalance )
      return " camera-white-balance";
   return "";
}

/**
 * Provides default input file hints.
 *
 * @returns
 */
StackEngine.prototype.inputHints = function()
{
   // Input format hints:
   // * XISF: fits-keywords normalize
   // * FITS: signed-is-physical use-roworder-keywords up-bottom|bottom-up
   // * DSLR_RAW: raw cfa
   return "fits-keywords normalize raw cfa use-roworder-keywords signed-is-physical" + this.coordinateConventionHints() + this.whiteBalanceHints();
};

// ----------------------------------------------------------------------------

/**
 * Provides default output file hints.
 *
 * @returns
 */
StackEngine.prototype.outputHints = function()
{
   // Output format hints:
   // * XISF: properties fits-keywords no-compress-data block-alignment 4096 max-inline-block-size 3072 no-embedded-data no-resolution
   // * FITS: up-bottom|bottom-up

   return "properties fits-keywords no-compress-data block-alignment 4096 max-inline-block-size 3072 no-embedded-data no-resolution " +
      this.coordinateConventionHints();
};

// ----------------------------------------------------------------------------

/**
 * Opens the image at filePath and returns the generated Window object.
 *
 * @param {String} filePath
 * @returns
 */
StackEngine.prototype.readImage = function( filePath )
{
   let
   {
      isEmptyString
   } = WBPPUtils.shared();

   let ext = File.extractExtension( filePath );
   let F = new FileFormat( ext, true /*toRead*/ , false /*toWrite*/ );
   if ( F.isNull )
      throw new Error( "No installed file format can read \'" + ext + "\' files." ); // shouldn't happen

   let f = new FileFormatInstance( F );
   if ( f.isNull )
      throw new Error( "Unable to instantiate file format: " + F.name );

   let d = f.open( filePath, this.inputHints() );
   if ( d.length < 1 )
      throw new Error( "Unable to open file: " + filePath );
   if ( d.length > 1 )
      throw new Error( "Multi-image files are not supported by this script: " + filePath );

   let window = new ImageWindow( 1, 1, 1, /*numberOfChannels*/ 32, /*bitsPerSample*/ true /*floatSample*/ );

   let view = window.mainView;
   view.beginProcess( UndoFlag_NoSwapFile );

   if ( !f.readImage( view.image ) )
      throw new Error( "Unable to read file: " + filePath );

   if ( F.canStoreImageProperties )
      if ( F.supportsViewProperties )
      {
         let info = view.importProperties( f );
         if ( !isEmptyString( info ) )
            console.criticalln( "<end><cbr>*** Error reading image properties:\n", info );
      }

   if ( F.canStoreKeywords )
      window.keywords = f.keywords;

   view.endProcess();

   f.close();

   return window;
};

// ----------------------------------------------------------------------------

/**
 * Writes an iamge to file.
 *
 * @param {String} filePath
 * @param {[ImageWindow]} images an array of images
 * @param {[String]} identifiers an optional array of identifiers
 */
StackEngine.prototype.writeImage = function( filePath, images, identifiers )
{
   if ( images.length == 0 )
   {
      console.warning( "*** write image: empty image array provided, no image will be saved at path ", filePath );
      return;
   }

   let F = new FileFormat( ".xisf", false /*toRead*/ , true /*toWrite*/ );
   if ( F.isNull )
      throw new Error( "No installed file format can write " + ".xisf" + " files." ); // shouldn't happen

   let f = new FileFormatInstance( F );
   if ( f.isNull )
      throw new Error( "Unable to instantiate file format: " + F.name );

   if ( !f.create( filePath, this.outputHints() ) )
      throw new Error( "Error creating output file: " + filePath );

   let d = new ImageDescription;
   d.bitsPerSample = 32;
   d.ieeefpSampleFormat = true;
   if ( !f.setOptions( d ) )
      throw new Error( "Unable to set output file options: " + filePath );

   // the first image determine the saved image properties
   if ( F.canStoreImageProperties )
      if ( F.supportsViewProperties )
         images[ 0 ].mainView.exportProperties( f );

   for ( let i = 0; i < images.length; i++ )
   {
      if ( images[ i ] != null )
      {
         if ( identifiers && identifiers[ i ] != undefined )
            f.setImageId( identifiers[ i ] );
         if ( F.canStoreKeywords )
            f.keywords = images[ i ].keywords;
         if ( !f.writeImage( images[ i ].mainView.image ) )
            throw new Error( "Error writing output file: " + filePath );
      }
   }

   f.close();
};

// ----------------------------------------------------------------------------
StackEngine.rejectionMethods = [
{
   name: "Percentile Clipping",
   rejection: ImageIntegration.prototype.PercentileClip
},
{
   name: "Winsorized Sigma Clipping",
   rejection: ImageIntegration.prototype.WinsorizedSigmaClip
},
{
   name: "Linear Fit Clipping",
   rejection: ImageIntegration.prototype.LinearFit
},
{
   name: "Generalized Extreme Studentized Deviate",
   rejection: ImageIntegration.prototype.Rejection_ESD
},
{
   name: "Robust Chauvenet Rejection",
   rejection: ImageIntegration.prototype.Rejection_RCR
},
{
   name: "Auto",
   rejection: ImageIntegration.prototype.auto
} ];

// ----------------------------------------------------------------------------

StackEngine.prototype.rejectionNames = function()
{
   return StackEngine.rejectionMethods.map( item => item.name );
};

// ----------------------------------------------------------------------------

StackEngine.prototype.rejectionFromIndex = function( index )
{
   return StackEngine.rejectionMethods[ index ].rejection;
};

// ----------------------------------------------------------------------------

StackEngine.prototype.rejectionName = function( rejection )
{
   for ( let i = 0; i < StackEngine.rejectionMethods.length; ++i )
      if ( StackEngine.rejectionMethods[ i ].rejection === rejection )
         return StackEngine.rejectionMethods[ i ].name;
   return StackEngine.rejectionMethods[ StackEngine.rejectionMethods.length - 1 ].name;
};

// ----------------------------------------------------------------------------

StackEngine.prototype.rejectionIndex = function( rejection )
{
   for ( let i = 0; i < StackEngine.rejectionMethods.length; ++i )
      if ( StackEngine.rejectionMethods[ i ].rejection === rejection )
         return i;
   return StackEngine.rejectionMethods.length - 1;
};

// ----------------------------------------------------------------------------

/*
 * min/max values ot FWHM, eccentricity and SNR are computed.
 * These values will be used to compute the final weights of the light images.
 */
StackEngine.prototype.getMinMaxDescriptorsValues = function( imagesDescriptors )
{
   let FWHM = imagesDescriptors.map( descriptor => descriptor.FWHM );
   let eccentricity = imagesDescriptors.map( descriptor => descriptor.eccentricity );
   let SNR = imagesDescriptors.map( descriptor => descriptor.SNR );
   let noise = imagesDescriptors.map( descriptor => descriptor.noise );
   let stars = imagesDescriptors.map( descriptor => descriptor.numberOfStars );
   let PSFSignalWeight = imagesDescriptors.map( descriptor => descriptor.PSFSignalWeight );
   let PSFSNR = imagesDescriptors.map( descriptor => descriptor.PSFSNR );

   let FWHM_min = Math.min.apply( null, FWHM );
   let FWHM_max = Math.max.apply( null, FWHM );
   let eccentricity_min = Math.min.apply( null, eccentricity );
   let eccentricity_max = Math.max.apply( null, eccentricity );
   let SNR_min = Math.min.apply( null, SNR );
   let SNR_max = Math.max.apply( null, SNR );
   let noise_min = Math.min.apply( null, noise );
   let noise_max = Math.max.apply( null, noise );
   let stars_min = Math.min.apply( null, stars );
   let stars_max = Math.max.apply( null, stars );
   let PSFSignalWeight_min = Math.min.apply( null, PSFSignalWeight );
   let PSFSignalWeight_max = Math.max.apply( null, PSFSignalWeight );
   let PSFSNR_min = Math.min.apply( null, PSFSNR );
   let PSFSNR_max = Math.max.apply( null, PSFSNR );

   return {
      FWHM_min: FWHM_min,
      FWHM_max: FWHM_max,
      eccentricity_min: eccentricity_min,
      eccentricity_max: eccentricity_max,
      SNR_min: SNR_min,
      SNR_max: SNR_max,
      noise_min: noise_min,
      noise_max: noise_max,
      stars_min: stars_min,
      stars_max: stars_max,
      PSFSignalWeight_min: PSFSignalWeight_min,
      PSFSignalWeight_max: PSFSignalWeight_max,
      PSFSNR_min: PSFSNR_min,
      PSFSNR_max: PSFSNR_max
   };
};

// ----------------------------------------------------------------------------

/*
 * Find the best frame as reference for registration across all images
 */
StackEngine.prototype.findRegistrationReferenceFileItem = function( groups )
{
   // extract descriptors for all active frames (double check that a descriptor exists)
   let fileItems = groups.reduce( ( acc, g ) =>
      {
         return acc.concat( g.activeFrames() );
      },
      [] ).filter( fileItem => fileItem.descriptor != undefined )

   // find the lowest binning of frames
   let binningForRegistration = fileItems.reduce( ( acc, fileItem ) =>
   {
      return Math.min( acc, fileItem.binning );
   }, 256 );

   // extract only the frames with the lowest binning
   fileItems = fileItems.filter( item => ( item.binning == binningForRegistration ) );

   // get the best frame
   let descriptors = fileItems.map( fileItem => fileItem.descriptor );
   let maxVal = 0;
   let bestFrame = undefined;

   // compute the absolute mix/max ranges
   let flatDescriptorsMinMax = this.getMinMaxDescriptorsValues( descriptors );

   // compute all images weight and find the best
   for ( let i = 0; i < fileItems.length; ++i )
   {
      // select the frame with the highest number of stars
      let weight = this.computeWeightForLight(
         fileItems[ i ].descriptor,
         flatDescriptorsMinMax,
         0, /*FWHMWeight*/
         0, /*eccentricityWeight*/
         0, /*SNRWeight*/
         1, /*starsWeight*/
         0, /*PSF Signal Weight*/
         0, /*PSF SNR Weight*/
         1 /*pedestal*/
      );
      if ( weight && isFinite( weight ) )
         if ( weight > maxVal )
         {
            maxVal = weight;
            bestFrame = fileItems[ i ];
         }
   }

   // check in case light images are all the same
   return bestFrame;
};

// ----------------------------------------------------------------------------

StackEngine.prototype.computeWeightForLight = function( descriptor, descriptorMinMax, FWHMWeight, eccentricityWeight, SNRWeight, starsWeight, PSFSignalWeight, PSFSNRWeight, pedestal, normalizationFactor, printToConsole )
{
   if ( descriptor == undefined )
      return undefined;

   normalizationFactor = normalizationFactor || 1;
   printToConsole = printToConsole || false;

   let
   {
      paddedStringNumber
   } = WBPPUtils.shared();

   let FWHM = descriptor.FWHM;
   let FWHM_min = descriptorMinMax.FWHM_min;
   let FWHM_max = descriptorMinMax.FWHM_max;
   let eccentricity = descriptor.eccentricity;
   let eccentricity_min = descriptorMinMax.eccentricity_min;
   let eccentricity_max = descriptorMinMax.eccentricity_max;
   let SNR = descriptor.SNR;
   let SNR_min = descriptorMinMax.SNR_min;
   let SNR_max = descriptorMinMax.SNR_max;
   let noise = descriptor.noise;
   let noise_min = descriptorMinMax.noise_min;
   let noise_max = descriptorMinMax.noise_max;
   let stars = descriptor.numberOfStars;
   let stars_min = descriptorMinMax.stars_min;
   let stars_max = descriptorMinMax.stars_max;
   let PSFSignal = descriptor.PSFSignalWeight;
   let PSFSignal_min = descriptorMinMax.PSFSignalWeight_min;
   let PSFSignal_max = descriptorMinMax.PSFSignalWeight_max;
   let PSFSNR = descriptor.PSFSNR;
   let PSFSNR_min = descriptorMinMax.PSFSNR_min;
   let PSFSNR_max = descriptorMinMax.PSFSNR_max;

   let a = FWHM_max - FWHM_min == 0 ? 0 : 1 - ( FWHM - FWHM_min ) / ( FWHM_max - FWHM_min );
   let b = eccentricity_max - eccentricity_min == 0 ? 0 : 1 - ( eccentricity - eccentricity_min ) / ( eccentricity_max - eccentricity_min );
   let c = SNR_max - SNR_min == 0 ? 0 : ( SNR - SNR_min ) / ( SNR_max - SNR_min );
   let s = stars_max - stars_min == 0 ? 0 : ( stars - stars_min ) / ( stars_max - stars_min );
   let ps = PSFSignal_max != 0 ? PSFSignal / PSFSignal_max : 0;
   let psfsnr = PSFSNR_max != 0 ? PSFSNR / PSFSNR_max : 0;
   let score = pedestal + a * FWHMWeight + b * eccentricityWeight + c * SNRWeight + s * starsWeight + ps * PSFSignalWeight + psfsnr * PSFSNRWeight;
   let weight = score / normalizationFactor;
   if ( printToConsole )
   {
      console.noteln( 'Weights of image: ', descriptor.filePath );
      console.noteln( "--------------------------------" );
      console.noteln( 'FWHM         : ', isFinite( a ) ? paddedStringNumber( format( "%.02f %%", a * 100 ), 4 ) + " [ " + paddedStringNumber( format( "%i", FWHMWeight ), 3 ) + " ] " : '-' );
      console.noteln( 'eccentricity : ', isFinite( b ) ? paddedStringNumber( format( "%.02f %%", b * 100 ), 4 ) + " [ " + paddedStringNumber( format( "%i", eccentricityWeight ), 3 ) + " ] " : '-' );
      console.noteln( 'SNR          : ', isFinite( c ) ? paddedStringNumber( format( "%.02f %%", c * 100 ), 4 ) + " [ " + paddedStringNumber( format( "%i", SNRWeight ), 3 ) + " ] " : '-' );
      console.noteln( 'stars        : ', isFinite( s ) ? paddedStringNumber( format( "%.02f %%", s * 100 ), 4 ) + " [ " + paddedStringNumber( format( "%i", starsWeight ), 3 ) + " ] " : '-' );
      console.noteln( 'PSF Signal   : ', isFinite( ps ) ? paddedStringNumber( format( "%.02f %%", ps * 100 ), 4 ) + " [ " + paddedStringNumber( format( "%i", PSFSignalWeight ), 3 ) + " ] " : '-' );
      console.noteln( 'PSF SNR      : ', isFinite( psfsnr ) ? paddedStringNumber( format( "%.02f %%", psfsnr * 100 ), 4 ) + " [ " + paddedStringNumber( format( "%i", PSFSNRWeight ), 3 ) + " ] " : '-' );
      console.noteln( 'Pedestal     : ', paddedStringNumber( format( "%i", pedestal ), 4 ) );
      console.noteln();
      console.noteln( 'Score        : ', isFinite( score ) ? paddedStringNumber( format( "%.03f", score ), 4 ) : '-' );
      console.noteln( 'Image weight : ', isFinite( weight ) ? paddedStringNumber( format( "%.03f", weight ), 4 ) : '-' );
      console.noteln( "--------------------------------" );
      console.flush();
   }
   return weight;
};

// -------------------------------------------------------- --------------------

/**
 * Writes the weights for each provided groups,
 *
 * @param {*} groups
 */
StackEngine.prototype.writeWeightsWithDescriptors = function( groups )
{
   let
   {
      existingDirectory,
      getLastModifiedDate
   } = WBPPUtils.shared();

   // compute weights for all groups
   for ( let i = 0; i < groups.length; ++i )
   {
      let activeFrames = groups[ i ].activeFrames();
      let virtualGroup = groups[ i ].isVirtual();

      for ( let j = 0; j < activeFrames.length; ++j )
      {
         processEvents();
         gc();

         let descriptor = activeFrames[ j ].descriptor;
         if ( descriptor && descriptor.formulaWeight != undefined && descriptor.imageWeight != undefined )
         {
            // avoid to overwrite unprocessed files (potentially original data)
            let targetFname;
            if ( virtualGroup || activeFrames[ j ].isProcessed() )
            {
               targetFname = activeFrames[ j ].current;
            }
            else
            {
               let subfolder = groups[ i ].folderName();
               let measuredDirectory = existingDirectory( this.outputDirectory + "/measured/" + subfolder );
               targetFname = measuredDirectory + "/" + activeFrames[ j ].currentFileName();
            }

            // check if this weight has already been cached for the source and target frames
            let key = activeFrames[ j ].current + "_" + targetFname + "_" + JSON.stringify( descriptor );
            let cacheKey = engine.executionCache.keyFor( key );
            if (
               engine.executionCache.hasCacheForKey( cacheKey ) &&
               engine.executionCache.isFileUnmodified( cacheKey, activeFrames[ j ].current ) &&
               engine.executionCache.isFileUnmodified( cacheKey, targetFname )
            )
            {
               console.noteln( "descriptor, source and target frame are unchanged, no need to write the weights into the target file" )
            }
            else
            {

               let imageWindow = this.readImage( activeFrames[ j ].current, "" );
               if ( imageWindow === null )
               {
                  console.warningln( "** Warning: Unable to open file to write weights: " + activeFrames[ j ].current + ", light frame will be discarded." );
                  this.processLogger.addWarning( "Unable to open file to write weights: " + activeFrames[ j ].current + ", light frame will be discarded." );
                  // can't read the file, discard it
                  activeFrames[ j ].processingFailed();
               }
               else
               {

                  if ( isFinite( descriptor.formulaWeight ) )
                  {
                     imageWindow.keywords = imageWindow.keywords.filter( keyword =>
                     {
                        return keyword.name != CONST_MEASUREMENT_FWHM &&
                           keyword.name != CONST_MEASUREMENT_ECCENTRICITY &&
                           keyword.name != CONST_MEASUREMENT_NOISE &&
                           keyword.name != CONST_MEASUREMENT_SNRWEIGHT &&
                           keyword.name != CONST_MEASUREMENT_STARS &&
                           keyword.name != CONST_MEASUREMENT_PSFSIGNAL &&
                           keyword.name != CONST_MEASUREMENT_PSFSNR &&
                           keyword.name != CONST_MEASUREMENT_SCORE &&
                           keyword.name != WEIGHT_KEYWORD;
                     } ).concat(
                        new FITSKeyword(
                           CONST_MEASUREMENT_FWHM,
                           format( "%.5e", descriptor.FWHM ).replace( "e", "E" ),
                           "WBPP Measurement: FWHM"
                        ) ).concat(
                        new FITSKeyword(
                           CONST_MEASUREMENT_ECCENTRICITY,
                           format( "%.5e", descriptor.eccentricity ).replace( "e", "E" ),
                           "WBPP Measurement: eccentricity"
                        ) ).concat(
                        new FITSKeyword(
                           CONST_MEASUREMENT_NOISE,
                           format( "%.5e", descriptor.noise ).replace( "e", "E" ),
                           "WBPP Measurement: noise"
                        ) ).concat(
                        new FITSKeyword(
                           CONST_MEASUREMENT_SNRWEIGHT,
                           format( "%.5e", descriptor.SNR ).replace( "e", "E" ),
                           "WBPP Measurement: SNR Weight"
                        ) ).concat(
                        new FITSKeyword(
                           CONST_MEASUREMENT_STARS,
                           format( "%i", descriptor.numberOfStars ),
                           "WBPP Measurement: number of stars found"
                        ) ).concat(
                        new FITSKeyword(
                           CONST_MEASUREMENT_PSFSIGNAL,
                           format( "%.5e", descriptor.PSFSignalWeight ).replace( "e", "E" ),
                           "WBPP Measurement: PSF Signal Weight"
                        ) ).concat(
                        new FITSKeyword(
                           CONST_MEASUREMENT_PSFSNR,
                           format( "%.5e", descriptor.PSFSNR ).replace( "e", "E" ),
                           "WBPP Measurement: PSF SNR"
                        ) ).concat(
                        new FITSKeyword(
                           CONST_MEASUREMENT_SCORE,
                           format( "%.5e", descriptor.formulaWeight ).replace( "e", "E" ),
                           "WBPP Measurement: Image score"
                        ) ).concat(
                        new FITSKeyword(
                           WEIGHT_KEYWORD,
                           format( "%.3e", descriptor.imageWeight ).replace( "e", "E" ),
                           "Subframe weight"
                        ) );

                     // store the cache, a flag reporting that the given target file is the source with the addition of the descriptor data
                     engine.executionCache.setCache( cacheKey, true )

                     // get the current LMD of the source file before writing, file could be overwritten so we need to update the
                     // current LMD value in the cache
                     let previousLMD = getLastModifiedDate( activeFrames[ j ].current );

                     // write the file
                     imageWindow.saveAs( targetFname, false, false, false, false );
                     activeFrames[ j ].processingSucceeded( WBPPFrameProcessingStep.WRITE_WEIGHTS, targetFname );
                     imageWindow.forceClose();

                     // handle the overwriting of the file by updating the target frame LMD value in the cache
                     let newLMD = getLastModifiedDate( targetFname );
                     engine.executionCache.updateLMD( targetFname, previousLMD, newLMD );
                     engine.executionCache.cacheFileLMD( cacheKey, activeFrames[ j ].current );
                     engine.executionCache.cacheFileLMD( cacheKey, targetFname );
                  }
                  else
                  {
                     console.warningln( "** Warning: Unable to open file to write weights: " + activeFrames[ j ].current + ", light frame will be discarded." );
                     this.processLogger.addWarning( "Unable to open file to write weights: " + activeFrames[ j ].current + ", light frame will be discarded." );
                     // can't read the file, discard it
                     activeFrames[ j ].processingFailed();
                  }
               }
            }
         }
         else
         {
            console.warningln( "** Warning: Measurement not found for image: " + activeFrames[ j ].current + ", light frame will be discarded." );
            this.processLogger.addWarning( "Measurement not found for image: " + activeFrames[ j ].current + ", light frame will be discarded." );
            // can't find the descriptor for the file item
            activeFrames[ j ].processingFailed();
         }
      }
   }
};

// ----------------------------------------------------------------------------

/**
 * Measure the provided file items and store the measurements into each
 * measured item. The measure is performed by means of the SubframeSelector
 * Process.
 *
 * @param {[FileItem]} fileItems
 * @returns the number of frames successfully measured
 */
StackEngine.prototype.computeDescriptors = function( fileItems )
{
   let nCached = 0;
   let nMeasured = 0;
   let nFailed = 0;

   if ( !fileItems || fileItems.length == 0 )
      return {
         nCached: nCached,
         nMeasured: nMeasured,
         nFailed: nFailed
      };

   let
   {
      enableTargetFrames,
      paddedStringNumber
   } = WBPPUtils.shared();

   let subframes = fileItems.map( item => item.current );

   var SS = new SubframeSelector;
   SS.routine = SubframeSelector.prototype.MeasureSubframes;
   SS.nonInteractive = true; // ### since core version 1.8.8-8
   SS.cameraResolution = SubframeSelector.prototype.Bits16;
   SS.scaleUnit = SubframeSelector.prototype.ArcSeconds;
   SS.dataUnit = SubframeSelector.prototype.DataNumber;
   SS.fileCache = false;

   /**
    * Use the cached data for the unchanged files
    */
   let subframesToMeasure = [];
   let cachedDescriptors = {};

   let isDescriptorValid = function( descriptor )
   {
      let failed = !isFinite( descriptor.FWHM ) ||
         !isFinite( descriptor.eccentricity ) ||
         !isFinite( descriptor.numberOfStars ) ||
         !isFinite( descriptor.PSFSignalWeight ) ||
         !isFinite( descriptor.PSFSNR ) ||
         !isFinite( descriptor.SNR ) ||
         !isFinite( descriptor.median ) ||
         !isFinite( descriptor.mad ) ||
         !isFinite( descriptor.Mstar ) ||
         descriptor.numberOfStars <= 0;
      return !failed;
   };

   // The cache is a map between the filePath and its descriptor
   let SSCache = {};
   let SScacheKey = engine.executionCache.keyFor( "SubframeSelector" );
   console.writeln();
   console.noteln( "SS check for cached data" );
   if ( engine.executionCache.hasCacheForKey( SScacheKey ) )
   {
      console.noteln( "SS has cached data for key ", SScacheKey );
      SSCache = engine.executionCache.cacheForKey( SScacheKey );
   }
   else
      console.noteln( "SS no cached data available for key ", SScacheKey );

   for ( let i = 0; i < subframes.length; i++ )
   {
      let filePath = subframes[ i ];
      let cachedDescriptor = SSCache[ filePath ];
      if ( cachedDescriptor != undefined && this.executionCache.isFileUnmodified( SScacheKey, filePath ) )
      {
         // we have a valid cached data for that file
         console.noteln( "SS will use cached descriptor for ", filePath );
         cachedDescriptors[ filePath ] = cachedDescriptor;
         continue;
      }
      console.noteln( "SS will measure ", filePath );
      // non existent or old cache found, we need to measure the file
      subframesToMeasure.push( filePath );
   }

   // Set the frames to be measured and perform the measurements
   let success = true;
   SS.subframes = enableTargetFrames( subframesToMeasure, 2 );
   if ( subframesToMeasure.length > 0 )
      success = SS.executeGlobal();

   // Put the process into the process container if needed
   // We set the full list of files to be measured, disregarding the ones that have not been measured because of
   // the cache

   SS.nonInteractive = false;
   SS.subframes = enableTargetFrames( subframes, 2 );
   engine.processContainer.add( SS );

   // NB: fixed indexes that need to be aligned with the process implementation
   let iIndex = 0;
   let iFilePath = 3;
   let iFWHM = 5;
   let iEccentricity = 6;
   let iPSFSignalWeight = 7;
   let iSNREstimate = 9;
   let iMedian = 10;
   let iMad = 11;
   let iNoise = 12;
   let iStars = 14;
   let iMstar = 26;
   let iPSFSNR = 28;

   // create a table of  descriptors, the cached descriptors are stored immediately,
   // the others are filled by scanning the SS results
   let descriptors = [];
   for ( let i = 0; i < subframes.length; ++i )
   {
      if ( cachedDescriptors[ subframes[ i ] ] != undefined )
      {
         descriptors.push( cachedDescriptors[ subframes[ i ] ] );
         nCached++;
      }
      else
      {
         descriptors.push(
         {
            filePath: subframes[ i ],
            failed: true
         } );
      }
   }

   if ( success )
   {
      // extract successful measurements
      for ( let i = 0; i < SS.measurements.length; ++i )
      {
         let index = SS.measurements[ i ][ iIndex ];
         let filePath = SS.measurements[ i ][ iFilePath ];
         let FWHM = SS.measurements[ i ][ iFWHM ];
         let eccentricity = SS.measurements[ i ][ iEccentricity ];
         let noise = SS.measurements[ i ][ iNoise ];
         let numberOfStars = SS.measurements[ i ][ iStars ];
         let PSFSignalWeight = SS.measurements[ i ][ iPSFSignalWeight ];
         let PSFSNR = SS.measurements[ i ][ iPSFSNR ];
         let SNR = SS.measurements[ i ][ iSNREstimate ];
         let median = SS.measurements[ i ][ iMedian ];
         let mad = SS.measurements[ i ][ iMad ];
         let Mstar = SS.measurements[ i ][ iMstar ];

         let descriptor = {
            filePath: filePath,
            FWHM: FWHM,
            eccentricity: eccentricity,
            noise: noise,
            numberOfStars: numberOfStars,
            PSFSignalWeight: PSFSignalWeight,
            PSFSNR: PSFSNR,
            SNR: SNR,
            median: median,
            mad: mad,
            Mstar: Mstar
         };

         descriptor.failed = !isDescriptorValid( descriptor );

         // find the index of the corresponding descriptor data in the descriptor array
         let j = 0;
         while ( descriptors[ j ].filePath != filePath )
            j++;

         // update the descriptor
         descriptors[ j ] = descriptor;

         if ( descriptor.failed )
            nFailed++;
         else
            nMeasured++;
      }
   }
   else
   {
      // SS failed, no measure is returned
      nFailed = subframesToMeasure.length;
   }

   // store the result in the execution cache, only valid descriptors are saved
   for ( let i = 0; i < descriptors.length; i++ )
   {
      if ( !descriptors[ i ].failed )
      {
         let filePath = descriptors[ i ].filePath;
         engine.executionCache.cacheFileLMD( SScacheKey, filePath );
         SSCache[ descriptors[ i ].filePath ] = descriptors[ i ];
      }
   }
   engine.executionCache.setCache( SScacheKey, SSCache );

   // gather measurements in file items
   for ( let i = 0; i < descriptors.length; ++i )
   {
      let descriptor = descriptors[ i ];
      console.writeln();
      console.writeln( "<end><cbr><raw>", descriptor.filePath, "</raw>" );
      if ( descriptor.failed )
      {
         console.noteln( "Descriptor failed: " );
         console.noteln( "FWHM            : ", descriptor.FWHM );
         console.noteln( "eccentricity    : ", descriptor.eccentricity );
         console.noteln( "numberOfStars   : ", descriptor.numberOfStars );
         console.noteln( "PSFSignalWeight : ", descriptor.PSFSignalWeight );
         console.noteln( "PSFSNR          : ", descriptor.PSFSNR );
         console.noteln( "SNR             : ", descriptor.SNR );
         console.noteln( "median          : ", descriptor.median );
         console.noteln( "mad             : ", descriptor.mad );
         console.noteln( "Mstar           : ", descriptor.Mstar );

         fileItems[ i ].processingFailed();
         console.warningln( "** Warning: Failed to measure frame - image will be ignored." );
         this.processLogger.addWarning( "Failed to measure " + descriptor.filePath + ", image will be discarded." );
         continue;
      }

      fileItems[ i ].setDescriptor( descriptor );

      let padding = descriptor.failed ? 10 : Math.floor( Math.max( 1, Math.log10( Math.max.apply( null, [ descriptor.FWHM, descriptor.eccentricity, descriptor.SNR, descriptor.numberOfStars, descriptor.PSFSignalWeight, descriptor.PSFSNR, descriptor.median * 65535, descriptor.Mstar * 65535 ] ) ) ) ) + 1;

      console.noteln( "--------------------------" + "-".repeat( padding ) );
      console.noteln( "FWHM              : ", isFinite( descriptor.FWHM ) ? paddedStringNumber( format( "%0.3f", descriptor.FWHM ), padding ) + ' [px]' : "NaN" );
      console.noteln( "Eccentricity      : ", isFinite( descriptor.eccentricity ) ? paddedStringNumber( format( "%0.3f", descriptor.eccentricity ), padding ) : "NaN" );
      console.noteln( "Number of stars   : ", isFinite( descriptor.numberOfStars ) ? paddedStringNumber( format( "%i", descriptor.numberOfStars ), padding ) : "NaN" );
      console.noteln( "PSF Signal Weight : ", isFinite( descriptor.PSFSignalWeight ) ? paddedStringNumber( format( "%0.3f", descriptor.PSFSignalWeight ), padding ) : "NaN" );
      console.noteln( "PSF SNR           : ", isFinite( descriptor.PSFSNR ) ? paddedStringNumber( format( "%0.3f", descriptor.PSFSNR ), padding ) : "NaN" );
      console.noteln( "SNR               : ", isFinite( descriptor.SNR ) ? paddedStringNumber( format( "%0.3f", descriptor.SNR ), padding ) : "NaN" );
      console.noteln( "Median (ADU)      : ", isFinite( descriptor.median ) ? paddedStringNumber( format( "%0.3f", descriptor.median * 65535 ), padding ) : "NaN" );
      console.noteln( "MAD (ADU)         : ", isFinite( descriptor.mad ) ? paddedStringNumber( format( "%0.3f", descriptor.mad * 65535 ), padding ) : "NaN" );
      console.noteln( "Mstar (ADU)       : ", isFinite( descriptor.Mstar ) ? paddedStringNumber( format( "%0.3f", descriptor.Mstar * 65535 ), padding ) : "NaN" );
      console.noteln( "--------------------------" + "-".repeat( padding ) );
   }
   console.flush();
   processEvents();
   gc();

   // return the file count
   return {
      nCached: nCached,
      nMeasured: nMeasured,
      nFailed: nFailed
   };
};

// ----------------------------------------------------------------------------
/**
 * Returns the auto crop region for an image.
 * The autocrop region is computed analyzing the low rejection map and the crop region represents the
 * the largest rectangle that includes pixels with low rejection values greather than 0.5.
 * The method assumes to find the low rejection map store in the image file, if this is not the case
 * it attempts to load the file with the postfix "_low_rejection". If none is found then no crop region
 * is computed.
 *
 * @param {*} filePath the file path of the image
 * @param {*} keepTheImageOpen if TRUE, the main image window remains open once the function returns
 * @returns the Rect defining the crop region of the image, undefined in case of errors.
 */

StackEngine.prototype.getAutocropRegion = function( filePath, returnTheWorkingImages )
{
   if ( !File.exists( filePath ) )
      return {
         success: false,
         message: "unable to compute the autocrop region, file not found at path " + filePath
      };;

   let windows = ImageWindow.open( filePath );
   // ensure that data has been loaded
   if ( windows == undefined || windows.length == 0 )
      return {
         success: false,
         message: "unable to compute the autocrop region, file not loaded at path " + filePath
      };

   // expect to find the main window and, optionally, the rejection high and low ones
   let mainWindow = windows[ 0 ];
   let rejLowWindow;
   let rejHighWindow;

   // search the low rejection map within the loaded images
   for ( let i = 1; i < windows.length; i++ )
   {
      if ( windows[ i ].mainView.id.indexOf( "_rejection_low" ) != -1 )
         rejLowWindow = windows[ i ];
      else if ( windows[ i ].mainView.id.indexOf( "_rejection_high" ) != -1 )
         rejHighWindow = windows[ i ];
   }

   // if rejection low is not present then serach for the external file that should have been
   // saved for autocrop purposes
   if ( rejLowWindow == undefined )
   {
      return {
         success: false,
         message: "unable to find the external low rejection map at path " + externalRejectionLowFile
      }
   }

   // compute the crop region
   let image = rejLowWindow.mainView.image;
   // memoization of the bottom and top Y coordinate available for each x coordinate
   let bottomY = new Array( image.width );
   let upperY = new Array( image.width );

   // crop coordinates
   let cx0 = 0;
   let cx1 = 0;
   let cy0 = 0;
   let cy1 = 0;

   // ROWS SCAN
   let maxArea = 0;
   let TOLERANCE = 0.25;

   for ( let row = 0; row < image.height; row++ )
   {

      // find the (x0,x1) extremes of the row
      let x0 = 0;
      while ( x0 < image.width && image.sample( x0, row ) > TOLERANCE )
         x0++;
      if ( x0 == image.width )
         continue;

      let x1 = image.width - 1;
      while ( image.sample( x1, row ) > TOLERANCE )
         x1--;

      // skip if the max possible area is lower than the current max
      if ( ( x1 - x0 ) * ( image.height - row ) <= maxArea && ( x1 - x0 ) * row <= maxArea )
         continue;

      // find the bottom Y values for each column
      let Ybottom = [ x0, x1 ].map( x =>
      {
         // check memoization
         if ( bottomY[ x ] != undefined )
            return bottomY[ x ];
         else
         {
            let y = image.height - 1;
            while ( y > row && image.sample( x, y ) > TOLERANCE )
               y--;
            bottomY[ x ] = y;
            return y;
         }
      } );
      // the bottom Y coordinate is the lower one
      let yb = Math.min( Ybottom[ 0 ], Ybottom[ 1 ] );

      // find the top Y values for each column
      let Ytop = [ x0, x1 ].map( x =>
      {
         // check memoization
         if ( upperY[ x ] != undefined )
            return upperY[ x ];
         else
         {
            let y = 0;
            while ( y < row && image.sample( x, y ) > TOLERANCE )
               y++;
            upperY[ x ] = y;
            return y;
         }
      } );
      // the top Y coordinate is the higher one
      let yt = Math.max( Ytop[ 0 ], Ytop[ 1 ] );

      // check the bottom area
      let area = ( x1 - x0 ) * ( yb - row );
      if ( area > maxArea )
      {
         maxArea = area;
         cx0 = x0;
         cy0 = row;
         cx1 = x1;
         cy1 = yb;
      }
      // check the top area
      area = ( x1 - x0 ) * ( row - yt );
      if ( area > maxArea )
      {
         maxArea = area;
         cx0 = x0;
         cy0 = yt;
         cx1 = x1;
         cy1 = row;
      }
   }

   // build the returned object
   let retVal = {
      success: true,
      rect: new Rect( cx0, cy0, cx1, cy1 )
   };

   if ( returnTheWorkingImages )
   {
      retVal.mainWindow = mainWindow;
      retVal.rejLowWindow = rejLowWindow;
      retVal.rejHighWindow = rejHighWindow;
   }
   else
   {
      if ( mainWindow )
         mainWindow.forceClose();
      if ( rejLowWindow )
         rejLowWindow.forceClose();
      if ( rejHighWindow )
         rejHighWindow.forceClose();
   }
   return retVal;
}

// ----------------------------------------------------------------------------

StackEngine.prototype.doLinearPatternSubtraction = function( groups )
{
   let
   {
      existingDirectory
   } = WBPPUtils.shared();

   // first step: we need to generate the reference image. This image needs to be generated
   // for each pair binning/image size amongst the calibration groups.
   // For each binning/size we use as reference the group that has the
   // longest total exposure time since it is supposed to have the highest SNR once integrated.

   // identify the set of binning/size pairs
   let groupsByBinningAndSize = groups.filter( g =>
   {
      let isLight = g.imageType == ImageType.LIGHT;
      let isMono = !g.isCFA;
      let isPRE = g.mode == WBPPGroupingMode.PRE;
      return isLight && isMono && isPRE;
   } ).reduce( ( acc, group ) =>
   {
      // generate an unique key that is based on binning and image size
      let key = group.binning + " (" + group.sizeString() + ")";
      if ( !acc[ key ] )
         acc[ key ] = [ group ];
      else
         acc[ key ].push( group );

      return acc;
   },
   {} );

   // iterate through all binning/size values
   let binningAndSizes = Object.keys( groupsByBinningAndSize );

   let nCached = 0;
   let nGenerated = 0;
   let nFailed = 0;

   if ( binningAndSizes.length == 0 )
   {
      console.warning( "** Warning: No monochrmoatic groups have been found, linear defects correction will be skipped." );
      this.processLogger.addWarning( "No monochrmoatic groups have been found, linear defects correction will be skipped." );
   }
   else
   {
      // configure the LDD and LPS engines

      let LDD = new LDDEngine();
      LDD.detectionThreshold = engine.linearPatternSubtractionRejectionLimit;
      LDD.closeFormerWorkingImages = true;

      let LPS = new LPSEngine();
      LPS.targetIsActiveImage = false;
      LPS.rejectionLimit = engine.linearPatternSubtractionRejectionLimit;
      LPS.globalRejectionLimit = Math.max( 5.0, engine.linearPatternSubtractionRejectionLimit );
      LPS.closeFormerWorkingImages = true;

      let detectModes;
      switch ( engine.linearPatternSubtractionMode )
      {
         case 0: // columns
            detectModes = [
            {
               correctColumns: true,
               postfix: "_lps"
            } ];
            break;
         case 1: // rows
            detectModes = [
            {
               correctColumns: false,
               postfix: "_lps"
            } ];
            break;
         case 2: // columns and rows
            detectModes = [
            {
               correctColumns: true,
               postfix: "_lps"
            },
            {
               correctColumns: false,
               postfix: "" // on the second execution we overwrite existing files
            } ];
            break;
      }

      // execute a correction cycle for each binning/size
      for ( let i = 0; i < binningAndSizes.length; ++i )
      {
         let binning = binningAndSizes[ i ];
         let groups = groupsByBinningAndSize[ binning ];

         // sort groups by the total active duration
         let groupsSortedByDuration = groups
            .filter( g => ( g.activeFrames().length >= 3 ) )
            .map( g => (
            {
               group: g,
               tet: g.totalExposureTime( true /* only active frames */ )
            } ) )
            .sort( ( a, b ) =>
            {
               if ( b.tet > a.tet )
                  return 1;
               if ( a.tet > b.tet )
                  return -1;
               return 0;
            } )
            .map( g => ( g.group ) );

         // check if a reference group has been found
         if ( groupsSortedByDuration.length == 0 )
         {
            console.warningln( "** Warning: unable generate the Linear Defect Detection reference frame." +
               " No groups with at least 3 grayscale light frames found.Light frames with binning " + binning +
               " will not be corrected." );
            this.processLogger.addError( "Uable generate the Linear Defect Detection reference frame." +
               " No groups with at least 3 grayscale light frames found. Light frames with binning " + binning +
               " will not be corrected." );
            // count the number of failed frames for the current binning
            nFailed += groups.reduce( ( acc, group ) => ( acc + group.activeFrames().length ), 0 );
            continue;
         }

         // the reference group has the longest duration
         let referenceGroup = groupsSortedByDuration[ 0 ];

         console.noteln( "LDD generate the reference frame for binning ", binning );
         // integrate the reference group to generate the reference frame for the current binninb/size value
         let
         {
            masterFilePath
         } = this.doIntegrate(
            referenceGroup,
            undefined, /* custom prefix */
            "_LDD_REFERENCE_FRAME", /* custom postfix */
            false, /* custom generate rejection maps */
            false, /* custom generate drizzle files */
            undefined, /* desired master file name */
            {
               /* II overridden parameters */
               combination: ImageIntegration.prototype.Average,
               weightMode: ImageIntegration.prototype.DontCare,
               evaluateSNR: false
            },
            undefined, /* FITS keywords */
            false /* handle astrometric solution */
         );

         // integration check
         if ( !masterFilePath )
         {
            console.warningln( "** Warning: failed to generate the Linear Defect Detection reference frame." +
               " Light frames with binning " + binning + " will not be corrected." );
            this.processLogger.addError( "Warning: failed to generate the Linear Defect Detection reference frame for" +
               " binning " + binning + ", light frames with this binning will not be corrected." );
            // count the number of failed frames for the current binning
            nFailed += groups.reduce( ( acc, group ) => ( acc + group.activeFrames().length ), 0 );
            continue;
         }

         // perform the detection for the linear defects for each mode (rows, columns or both) and generate the corresponding
         // defects list files
         let LDDdefectsFileNames = [];
         let imageReferenceWidth = 0;
         let imageReferenceHeight = 0;
         let LDDsuccess = true;
         for ( let d = 0; d < detectModes.length && LDDsuccess; d++ )
         {
            let dm = detectModes[ d ];
            LDD.detectColumns = dm.correctColumns;

            let columnOrRow = LDD.detectColumns ? "Col" : "Row";
            let LDDdefectsFileName = File.appendToName( masterFilePath, "_defects_list_" + columnOrRow );
            LDDdefectsFileName = File.changeExtension( LDDdefectsFileName, ".txt" );
            LDDdefectsFileNames[ d ] = LDDdefectsFileName;

            // generate the list of defects
            console.writeln();
            console.noteln( "Perform Linear Defect Detection on " + columnOrRow + "s for binning ", binning );
            this.processLogger.addSuccess( "Linear Defect Detection reference frame", masterFilePath );

            console.noteln( "LDD.columnOrRow  ................... ", columnOrRow );
            console.noteln( "LDD.detectColumns .................. ", LDD.detectColumns );
            console.noteln( "LDD.detectPartialLines ............. ", LDD.detectPartialLines );
            console.noteln( "LDD.imageShift ..................... ", LDD.imageShift );
            console.noteln( "LDD.closeFormerWorkingImages ....... ", LDD.closeFormerWorkingImages );
            console.noteln( "LDD.layersToRemove ................. ", LDD.layersToRemove );
            console.noteln( "LDD.rejectionLimit ................. ", LDD.rejectionLimit );
            console.noteln( "LDD.detectionThreshold ............. ", LDD.detectionThreshold );
            console.noteln( "LDD.partialLineDetectionThreshold .. ", LDD.partialLineDetectionThreshold );

            // Check if valid cache is already present for this configuration
            let LDDcacheKey = engine.executionCache.keyFor( "LDD_" + engine.linearPatternSubtractionRejectionLimit + "_" + masterFilePath );

            // cached data must exist for the current configuration
            // the reference image must be unchanged
            // the defect list file must exist and be unchanged
            console.writeln();
            console.noteln( "LDD check for cached data" );
            if ( engine.executionCache.hasCacheForKey( LDDcacheKey ) &&
               engine.executionCache.isFileUnmodified( LDDcacheKey, masterFilePath ) &&
               engine.executionCache.isFileUnmodified( LDDcacheKey, LDDdefectsFileName ) )
            {
               // valid cache exists
               let cachedData = engine.executionCache.cacheForKey( LDDcacheKey );
               imageReferenceWidth = cachedData.imageReferenceWidth;
               imageReferenceHeight = cachedData.imageReferenceHeight;
               console.noteln( "LDD valid cached data exists, defect list file is ", LDDdefectsFileName );
               console.writeln();
            }
            else
            {
               // no valid cache exists, generate the defect list
               if ( !engine.executionCache.hasCacheForKey( LDDcacheKey ) )
                  console.noteln( "LDD has no cached data for key ", LDDcacheKey );
               else if ( !engine.executionCache.isFileUnmodified( LDDcacheKey, masterFilePath ) )
                  console.noteln( "LDD has cache but the reference frame has changed: ", masterFilePath );
               else
                  console.noteln( "LDD has cache but the defect list file has changed: ", LDDdefectsFileName );
               console.writeln();

               // open the reference frame
               let refrenceFrameImageWindow = ImageWindow.open( masterFilePath );
               if ( refrenceFrameImageWindow.length > 0 )
               {
                  refrenceFrameImageWindow = refrenceFrameImageWindow[ 0 ];
                  refrenceFrameImageWindow.show();
               }
               else
               {
                  LDDsuccess = false;
                  console.warningln( "** Warning: failed to open the Linear Defect Detection reference frame." +
                     " Light frames with binning " + binning + " will not be corrected." );
                  this.processLogger.addError( "Warning: failed to open the Linear Defect Detection reference frame for" +
                     " binning " + binning + ", light frames with this binning will not be corrected." );
                  break;
               }

               imageReferenceWidth = refrenceFrameImageWindow.mainView.image.width;
               imageReferenceHeight = refrenceFrameImageWindow.mainView.image.height;

               // perform LDD on the active frame
               LDD.execute();
               refrenceFrameImageWindow.forceClose();
               console.noteln( "Linear Defect Detection completed" );

               // write the defective lines
               let LDDDefectFile = File.createFileForWriting( LDDdefectsFileName );
               if ( LDDDefectFile )
               {
                  console.noteln( "Linear Defect Detection writing defect file at ", LDDdefectsFileName );
                  for ( let i = 0; i < LDD.detectedColumnOrRow.length; ++i )
                  {
                     let line = columnOrRow + " " +
                        LDD.detectedColumnOrRow[ i ] + " " +
                        LDD.detectedStartPixel[ i ] + " " +
                        LDD.detectedEndPixel[ i ];
                     LDDDefectFile.outTextLn( line );
                     console.noteln( line );
                  }
                  LDDDefectFile.close();

                  // update the cache. The cache is epmty but we need to save it anyway to remember that LDD with this configuratio has
                  // already been performed
                  let cachedData = {
                     imageReferenceWidth: imageReferenceWidth,
                     imageReferenceHeight: imageReferenceHeight
                  };
                  console.writeln();
                  engine.executionCache.setCache( LDDcacheKey, cachedData );
                  engine.executionCache.cacheFileLMD( LDDcacheKey, masterFilePath );
                  engine.executionCache.cacheFileLMD( LDDcacheKey, LDDdefectsFileName );
                  console.writeln();
               }
               else
               {
                  LDDsuccess = false;
                  console.warningln( "Linear Defect Detection error creating the defect file at ", LDDdefectsFileName );
                  this.processLogger.addError( "Warning: Linear Defect Detection error for binning " + binning + ", cannot create " +
                     "the defect file at " + LDDdefectsFileName );
               }
            }
         }

         // we ignore the correction if LDD has not been succesfully executed
         if ( !LDDsuccess )
         {
            // count the number of failed frames for the current binning
            nFailed += groups.reduce( ( acc, group ) => ( acc + group.activeFrames().length ), 0 );
            continue;
         }

         // now that the list of defects has been succesfully generated for the current modes, we proceed correcting all frames of
         // all groups with the current binning/size
         for ( let ig = 0; ig < groups.length; ig++ )
         {
            // common mode LPS parameters
            LPS.outputDir = existingDirectory( this.outputDirectory + "/ldd_lps/" + groups[ ig ].folderName() );
            LPS.backgroundReferenceWidth = imageReferenceWidth;
            LPS.backgroundReferenceHeight = imageReferenceHeight;

            // generate the list of active frames associated to the current group
            let activeFrames = groups[ ig ].activeFrames();

            // check for cached data
            let LPScacheKey = engine.executionCache.keyFor(
               "LPS_" +
               engine.linearPatternSubtractionRejectionLimit + "_" +
               LPS.backgroundReferenceWidth + "_" +
               LPS.backgroundReferenceHeight + "_" +
               LPS.outputDir + "_" +
               // include the defects file names in the key
               LDDdefectsFileNames.join( "_" )
            );

            // check the cache and skip the files that are unmodified
            let filesToCorrect = activeFrames;
            // inject an auxiliary property to track the execution of LPS
            filesToCorrect = filesToCorrect.map( item =>
            {
               // assume files are processed succesfully by LPS
               item.__lps_success__ = true;
               item.__lps_input__ = item.current;
               return item;
            } );
            let LPScacheData = {};
            // prerequisite before checking: we must have a saved cache and the defect files must me unmodified
            console.writeln();
            console.noteln( "LPS check for cached data" );
            if ( engine.executionCache.hasCacheForKey( LPScacheKey ) )
            {
               console.noteln( "LPS has cache data for key ", LPScacheKey );

               // check if defect files are unmodified
               let defectFilesUnchanged = LDDdefectsFileNames.reduce( ( acc, defectFilePath ) =>
               {
                  // skip the check if a file has already changes
                  if ( !acc )
                     return acc;
                  // check the current defect file for modifications
                  let unmodified = engine.executionCache.isFileUnmodified( LPScacheKey, defectFilePath );
                  if ( !unmodified )
                     console.noteln( "LPS cache found but the defect file has changed: ", defectFilePath );
                  return acc && unmodified;
               }, true );

               if ( defectFilesUnchanged )
               {
                  // cached data is valid
                  // the cache is a map between input files and corrected files
                  LPScacheData = engine.executionCache.cacheForKey( LPScacheKey );
                  filesToCorrect = [];

                  // get the list of cached files
                  for ( let j = 0; j < activeFrames.length; j++ )
                  {
                     let activeFrame = activeFrames[ j ];
                     let outputFile = LPScacheData[ activeFrame.current ];
                     if ( outputFile != undefined &&
                        engine.executionCache.isFileUnmodified( LPScacheKey, activeFrame.current ) &&
                        engine.executionCache.isFileUnmodified( LPScacheKey, outputFile ) )
                     {
                        // input and output files are unmodified, proceed without processing the input file again
                        activeFrame.processingSucceeded( WBPPFrameProcessingStep.LPS, outputFile );
                        nCached++;
                        console.noteln( "LPS cache found for ", activeFrame.current );
                     }
                     else
                     {
                        // put the file to be corrected into the list and store its LMD
                        filesToCorrect.push( activeFrame );
                        engine.executionCache.cacheFileLMD( LPScacheKey, activeFrame.current );
                        if ( outputFile == undefined )
                           console.noteln( "LPS no cached data for ", File.extractNameAndExtension( activeFrame.current ) );
                        else if ( !engine.executionCache.isFileUnmodified( LPScacheKey, activeFrame.current ) )
                           console.noteln( "LPS input file is modified ", File.extractNameAndExtension( activeFrame.current ) );
                        else
                           console.noteln( "LPS output file is modified ", outputFile );
                        console.noteln( "LPS will correct ", activeFrame.current );
                     }
                  }
               }
               else
               {
                  console.noteln( "LPS defect files changed, LPS will be executed on all frames" );
               }
            }
            else
            {
               console.noteln( "LPS has no cache data for key ", LPScacheKey );
            }
            console.writeln();

            if ( filesToCorrect.length > 0 )
            {

               // execute the correction of the group for each correction mode
               // perform the correction passes (rows, columns or both)
               let LPSsuccess = true;
               for ( let d = 0; d < detectModes.length && LPSsuccess; d++ )
               {
                  let dm = detectModes[ d ];
                  let columnOrRow = LDD.detectColumns ? "Col" : "Row";

                  // get the defects list file name for the given mode
                  let LDDdefectsFileName = LDDdefectsFileNames[ d ];

                  // keep only the files that has been succesfully processed or not yet processed
                  filesToCorrect = filesToCorrect.filter( item => item.__lps_success__ );

                  // configure LPS
                  LPS.postfix = dm.postfix;
                  LPS.correctColumns = dm.correctColumns;
                  LPS.postfix = dm.postfix;
                  LPS.defectTableFilePath = LDDdefectsFileName;

                  console.noteln( "Executing Linear Pattern Subtraction for group ", groups[ ig ].toString() );
                  console.noteln( "LPS.defectTableFilePath ........ ", LPS.defectTableFilePath );
                  console.noteln( "LPS.postfix .................... ", LPS.postfix );
                  console.noteln( "LPS.targetIsActiveImage ........ ", LPS.targetIsActiveImage );
                  console.noteln( "LPS.correctColumns ............. ", LPS.correctColumns );
                  console.noteln( "LPS.correctEntireImage ......... ", LPS.correctEntireImage );
                  console.noteln( "LPS.layersToRemove ............. ", LPS.layersToRemove );
                  console.noteln( "LPS.rejectionLimit ............. ", LPS.rejectionLimit );
                  console.noteln( "LPS.globalRejection ............ ", LPS.globalRejection );
                  console.noteln( "LPS.globalRejectionLimit ....... ", LPS.globalRejectionLimit );
                  console.noteln( "LPS.closeFormerWorkingImages ... ", LPS.closeFormerWorkingImages );
                  console.noteln( "LPS.backgroundReferenceLeft .... ", LPS.backgroundReferenceLeft );
                  console.noteln( "LPS.backgroundReferenceTop ..... ", LPS.backgroundReferenceTop );
                  console.noteln( "LPS.backgroundReferenceWidth ... ", LPS.backgroundReferenceWidth );
                  console.noteln( "LPS.backgroundReferenceHeight .. ", LPS.backgroundReferenceHeight );

                  LPS.inputFiles = filesToCorrect.map( item => item.current );
                  LPS.execute();
                  console.noteln( "Linear Pattern Subtraction completed for group ", groups[ ig ].toString() );
                  // cache the defects list LMD
                  engine.executionCache.cacheFileLMD( LPScacheKey, LDDdefectsFileName );

                  // check output results
                  if ( LPS.output.length == 0 )
                  {
                     LPSsuccess = false;
                     filesToCorrect = filesToCorrect.map( item =>
                     {
                        item.__lps_success__ = false;
                        return item;
                     } );
                     nFailed += filesToCorrect.length;
                     // report the issue
                     console.warningln( "** Warning: Linear Pattern Subtraction " + columnOrRow + "s failed. Light frames for group " + groups[ ig ].toString() + " will not be corrected." );
                     this.processLogger.addError( "Linear Pattern Subtraction " + columnOrRow + "s failed. Light frames for group " + groups[ ig ].toString() + " will not be corrected." );
                  }
                  else
                  {
                     // create a support for output file matching
                     let LPSFilesData = LPS.output.reduce( ( acc, filePath ) =>
                     {
                        acc[ File.extractName( filePath ) ] = filePath;
                        return acc;
                     },
                     {} );

                     console.writeln();
                     for ( let c = 0; c < filesToCorrect.length; ++c )
                     {
                        let lpsFileName = File.extractName( filesToCorrect[ c ].current ) + dm.postfix;
                        let outputFilePath = LPSFilesData[ lpsFileName ];

                        if ( outputFilePath != undefined && outputFilePath.length > 0 )
                        {
                           if ( File.exists( outputFilePath ) )
                           {
                              // success
                              filesToCorrect[ c ].processingSucceeded( WBPPFrameProcessingStep.LPS, outputFilePath );
                              nGenerated++;
                           }
                           else
                           {
                              filesToCorrect[ c ].__lps_success__ = false;
                              console.warningln( "** Warning: File does not exist after Linear Pattern Subtraction: " + filesToCorrect[ c ].current );
                              this.processLogger.addWarning( "File does not exist after Linear Pattern Subtraction: " + filesToCorrect[ c ].current );
                              nFailed++;
                           }
                        }
                        else
                        {
                           filesToCorrect[ c ].__lps_success__ = false;
                           console.warningln( "** Warning: Linear Pattern Subtraction failed for frame: " + filesToCorrect[ c ].current );
                           this.processLogger.addWarning( "Linear Pattern Subtraction failed for frame: " + filesToCorrect[ c ].current );
                           nFailed++;
                        }
                     }
                  }
               }

               // cache the successfully processed files
               filesToCorrect = filesToCorrect.filter( item => item.__lps_success__ );
               for ( let c = 0; c < filesToCorrect.length; ++c )
               {
                  // update the cached input/output map
                  LPScacheData[ filesToCorrect[ c ].__lps_input__ ] = filesToCorrect[ c ].current;
                  // cache the final output file LMD
                  engine.executionCache.cacheFileLMD( LPScacheKey, filesToCorrect[ c ].current );
                  engine.executionCache.cacheFileLMD( LPScacheKey, filesToCorrect[ c ].__lps_input__ );
               }
            }
            else if ( activeFrames.length > 0 )
            {
               console.noteln( "LPS is skipped since all frames are already cached" );
            }
            else
            {
               console.noteln( "LPS no files to correct found" );
            }

            // end processing the groups
            engine.executionCache.setCache( LPScacheKey, LPScacheData );
         }
      }
   }

   return {
      nCached: nCached,
      nGenerated: nGenerated,
      nFailed: nFailed
   };
};

/**
 *  Generic function to plagesolve the image window.
 *
 * @param {*} imageWindow the image to be solved
 * @param {*} optimizeOnlyAtFirst TRUE if the first attempt is only for optimization
 * @param {*} overrides an optional object containing a set of key/value metadata values to override
 * @returns
 */
StackEngine.prototype.solveImage = function( referenceImageWindow, onlyOptimizeAtFirst, overrides )
{
   let
   {
      computeSTF
   } = WBPPUtils.shared();

   let solver = new ImageSolver();
   let solverInitializationSuccess = InitializeSolver( solver, referenceImageWindow );
   if ( overrides != undefined )
   {
      let keys = Object.keys( overrides );
      keys.forEach( k =>
      {
         solver.metadata[ k ] = overrides[ k ];
      } );
   }
   let stfApplied = false;

   if ( solverInitializationSuccess )
   {
      resetImageSolverConfiguration( solver );
      solver.solverCfg.onlyOptimize = onlyOptimizeAtFirst;

      for ( ;; )
      {
         if ( solver.error )
            console.warningln( solver.error );

         if ( solver.solverCfg.distortionCorrection == false )
         {
            // warning: distortion correction must be enabled
            ( new MessageBox( "<p>Distortion correction must be enabled to compute an accurate astrometric solution.</p>",
               "Plate Solving",
               StdIcon_Warning,
               StdButton_Ok ) ).execute();
         }
         else
         {
            if ( SolveImage( solver, referenceImageWindow ) )
               // solved
               break;
         }

         if ( !engine.platesolveFallbackManual )
         {
            // if manual fallback is not checked then we do not attempt to present the interactive GUI
            return false;
         }

         // solver failed, prepare the interactive mode
         engine.operationQueue.hideExecutionMonitorDialog();
         if ( !stfApplied )
         {
            referenceImageWindow.mainView.stf = computeSTF( referenceImageWindow.mainView, undefined /* descriptor */ , true /* for STF */ );
            referenceImageWindow.fitWindow();
            referenceImageWindow.position = new Point( 0, 0 );
            stfApplied = true;
         }

         referenceImageWindow.show();
         console.hide();
         let dialog = new ImageSolverDialog( solver.solverCfg, solver.metadata, false /*showTargetImage*/ );
         let res = dialog.execute();
         referenceImageWindow.hide();
         console.show();
         if ( res == 0 )
            return false;

         solver.solverCfg = dialog.solverCfg;
         solver.metadata = dialog.metadata;
      }

      return true;
   }
   return false;
}

// ----------------------------------------------------------------------------

/**
 * Initialize the processing.
 * To be invoked before starting the pipeline.
 */
StackEngine.prototype.initializeExecution = function()
{
   let
   {
      existingDirectory,
      timestampString
   } = WBPPUtils.shared();

   this.groupsManager.initializeProcessing();
   this.processContainer = new ProcessContainer;
   this.executionTimestamp = timestampString();
   this.logsFileName = existingDirectory( this.outputDirectory + "/logs" ) + "/" + this.executionTimestamp + ".log";
   this.consoleLogger.initialize( this.logsFileName )
};

/**
 * Performs the post-execution actions.
 *
 */
StackEngine.prototype.postExecutionActions = function()
{
   let
   {
      existingDirectory
   } = WBPPUtils.shared();

   // store the log timestamp
   let filePath = existingDirectory( this.outputDirectory + "/logs" ) + "/" + this.executionTimestamp + "_ProcessContainer.js";
   let str = this.processContainer.toSource( "JavaScript", "processContainer" /*varId*/ , 0 /*indent*/ ,
      SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();
   str += "processContainer.launchInterface();"

   File.writeTextFile( filePath, str );
   console.writeln();
   console.writeln( "Smart report result" );
   console.writeln();
   console.writeln( this.processLogger.toString() );
};

// ----------------------------------------------------------------------------
//                                  OPERATIONS
// ----------------------------------------------------------------------------

StackEngine.prototype.calibrationOperation = function( frameGroup )
{
   this.__base__ = WBPPOperationBlock;
   this.__base__( "Calibration", frameGroup, true /* trackable */ );

   this.spaceRequired = () =>
   {
      return frameGroup.groupSize();
   };

   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => ( frameGroup.injectGroupData(
   {
      name: "Calibration",
      status: this.status,
      statusMessage: this.statusMessage
   } ) );

   let
   {
      resultCountToString
   } = WBPPUtils.shared();

   this._run = () =>
   {
      let groupType = StackEngine.imageTypeToString( frameGroup.imageType )

      let activeFrames = frameGroup.activeFrames();

      let
      {
         calibratedFiles,
         nCached,
         nGenerated
      } = engine.doCalibrate( frameGroup );


      let nFailed = 0;
      if ( !calibratedFiles )
      {
         // calibration skipped
         return OperationBlockStatus.CANCELED;
      }
      else if ( nFailed == activeFrames.length )
      {
         // mark all processed frames as failed
         activeFrames.forEach( activeFrame => activeFrame.processingFailed() );
         // report the issue
         console.warningln( "** Warning: " + groupType + " frames calibration failed." );
         engine.processLogger.addError( groupType + " frames calibration failed." );
         this.statusMessage = "calibration failed";
         return OperationBlockStatus.FAILED;
      }
      else
      {
         // check which active frame has been successfully processed
         for ( let c = 0; c < activeFrames.length; ++c )
         {
            let inputFile = activeFrames[ c ].current
            let outputFile = calibratedFiles[ c ];

            if ( outputFile != undefined && outputFile.length > 0 )
            {
               if ( File.exists( outputFile ) )
               {
                  activeFrames[ c ].processingSucceeded( WBPPFrameProcessingStep.CALIBRATION, outputFile );
                  console.writeln( "Calibration frame " + c + ": " + inputFile + " ---> " + outputFile );
               }
               else
               {
                  // non-existing file can occur only with processed files
                  nFailed++;
                  nGenerated--;
                  console.warningln( "Calibration frame " + c + ": " + inputFile + " ---> [ FAILED: Calibrated file not found ]" );
                  engine.processLogger.addWarning( "File does not exist after image calibration: " + outputFile );
                  activeFrames[ c ].processingFailed();
               }
            }
            else
            {
               console.warningln( "Calibration frame " + c + ": " + inputFile + " ---> [ FAILED ]" );
               engine.processLogger.addWarning( "Calibration failed for image: " + inputFile );
               activeFrames[ c ].processingFailed();
               nGenerated--;
               nFailed++;
            }
         }

         // report how many flat frames exist after calibration
         let calibratedActiveFrames = frameGroup.activeFrames();
         if ( calibratedActiveFrames.length < 1 )
         {
            console.warningln( "** Warning: No " + groupType + " frames found after calibration." );
            engine.processLogger.addError( "No " + groupType + " frames found after calibration." );
            this.statusMessage = "no frames found after calibration";
            return OperationBlockStatus.FAILED;
         }

         engine.processLogger.addSuccess( "Calibration completed", calibratedActiveFrames.length + " " + groupType + " frame" + ( calibratedActiveFrames.length == 1 ? "" : "s" ) + " calibrated." );
         this.statusMessage = resultCountToString( nCached, nGenerated, nFailed, "calibrated" );
         this.hasWarnings = nFailed > 0;
         return OperationBlockStatus.DONE;
      }
   };
};
StackEngine.prototype.calibrationOperation.prototype = new WBPPOperationBlock;

StackEngine.prototype.LPSOperation = function()
{
   this.__base__ = WBPPOperationBlock;
   this.__base__( "Linear Defects Correction ", undefined, true /* trackable */ );

   this.spaceRequired = () =>
   {
      let preprocessGroups = engine.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

      let size = 0;
      let bins = preprocessGroups.reduce( ( acc, group ) =>
      {
         acc[ group.binning ] = true;
         return acc
      },
      {} );

      Object.keys( bins ).forEach( bin =>
      {
         let firstFound = false;
         for ( let i = 0; i < preprocessGroups.length; i++ )
            if ( preprocessGroups[ i ].binning == bin && !preprocessGroups[ i ].isCFA )
            {
               size += preprocessGroups[ i ].groupSize();
               if ( !firstFound )
               {
                  // include the master generated for LPS execution
                  firstFound = true;
                  size += preprocessGroups[ i ].frameSize();
               }
            }
      } );

      return size;
   };

   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "LPS",
      status: this.status,
      statusMessage: this.statusMessage
   } );

   let
   {
      resultCountToString
   } = WBPPUtils.shared();

   this._run = function()
   {
      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Apply linear defects correction to light frames" );
      console.noteln( SEPARATOR );

      let groupsPRE = engine.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

      let
      {
         nCached,
         nGenerated,
         nFailed
      } = engine.doLinearPatternSubtraction( groupsPRE );

      let resultString = resultCountToString( nCached, nGenerated, nFailed, "corrected" );
      this.statusMessage = ( nCached + nGenerated + nFailed ) + " frame(s)" + ( resultString.length > 0 ? ", " + resultString : "" );
      this.hasWarnings = nFailed > 0;

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* End of linear defects correction process" );
      console.noteln( SEPARATOR );

      return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.LPSOperation.prototype = new WBPPOperationBlock;

StackEngine.prototype.CosmeticCorrectionOperation = function( frameGroup )
{
   this.__base__ = WBPPOperationBlock;
   this.__base__( "Cosmetic Correction", frameGroup, true /* trackable */ );

   this.spaceRequired = () =>
   {
      return frameGroup.groupSize();
   }

   this.envForScript = () => ( frameGroup.injectGroupData(
   {
      name: "Cosmetic Correction",
      status: this.status,
      statusMessage: this.statusMessage
   } ) );

   let
   {
      enableTargetFrames,
      existingDirectory,
      resultCountToString
   } = WBPPUtils.shared();

   this._run = function()
   {
      let canceled = false;

      let cosmeticCorrectionTemplateId = frameGroup.CCTemplate;
      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Begin cosmetic correction of light frames" );
      console.noteln( SEPARATOR );

      frameGroup.log();

      let activeFrames = frameGroup.activeFrames();

      let filePaths = [];
      let nCached = 0;
      let nGenerated = 0;
      let nFailed = 0;

      let CC = ProcessInstance.fromIcon( cosmeticCorrectionTemplateId );
      if ( CC == null )
      {
         console.warningln( "** Warning: No such process icon: " + cosmeticCorrectionTemplateId + ", cosmetic correction will be skipped." );
         engine.processLogger.addWarning( "No such process icon: " + cosmeticCorrectionTemplateId + ", cosmetic correction will be skipped." );
         this.statusMessage = "process icon not found";
         canceled = true;
      }
      else if ( !( CC instanceof CosmeticCorrection ) )
      {
         console.warningln( "** Warning: The specified icon does not transport an instance " +
            "of CosmeticCorrection: " + cosmeticCorrectionTemplateId + ", cosmetic correction will be skipped." );
         engine.processLogger.addWarning( "The specified icon does not transport an instance " +
            "of CosmeticCorrection: " + cosmeticCorrectionTemplateId + ", cosmetic correction will be skipped." );
         this.statusMessage = "wrong process icon found";
         canceled = true;
      }
      else if ( activeFrames.length == 0 )
      {
         console.warningln( "** Warning: no active frames." );
         engine.processLogger.addWarning( "No active frames." );
         this.statusMessage = "no active frames";
         canceled = true;
      }
      else
      {

         console.noteln( "Cosmetic Correction: applying " + cosmeticCorrectionTemplateId + " process icon." );
         engine.processLogger.addMessage( "Running <b>" + cosmeticCorrectionTemplateId + "</b> Cosmetic Correction process icon." );

         let subfolder = frameGroup.folderName();
         let cosmetizedDirectory = existingDirectory( engine.outputDirectory + "/cosmetized/" + subfolder );

         filePaths = activeFrames.map( activeFrame => activeFrame.current );
         CC.outputDir = cosmetizedDirectory;
         CC.outputExtension = ".xisf";
         CC.prefix = "";
         CC.postfix = "_cc";
         CC.overwrite = true;
         CC.cfa = frameGroup.isCFA;

         let CCSource = CC.toSource( "JavaScript", "CC" /*varId*/ , 0 /*indent*/ ,
            SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();
         console.writeln( SEPARATOR2 );
         console.writeln( CCSource );
         console.writeln( SEPARATOR2 );

         /**
          * Skip CC for files that have valid cached data. Cache is a map between an input file and an
          * output file:
          * [inputFile]: outputFile
          */
         let filesToProcess = filePaths;
         let cachedCount = 0;
         let cached = {};
         let CCCache = {};
         let CCcacheKey = engine.executionCache.keyFor( CCSource );
         if ( engine.executionCache.hasCacheForKey( CCcacheKey ) )
         {

            console.noteln( "Cosmetic correction has cached data for key ", CCcacheKey );

            if ( ( !CC.useMasterDark || ( CC.useMasterDark && engine.executionCache.isFileUnmodified( CCcacheKey, CC.masterDarkPath ) ) ) )
            {
               // Cosmetic Correction has already been executed with such configuration and the (eventually used) master
               // dark is unchanged.
               // We avoid to correct frames that have already been processed by this configuration.
               filesToProcess = [];
               CCCache = engine.executionCache.cacheForKey( CCcacheKey );
               for ( let i = 0; i < filePaths.length; i++ )
               {
                  let inputFile = filePaths[ i ];
                  let outputFile = CCCache[ inputFile ];

                  if ( outputFile != undefined &&
                     engine.executionCache.isFileUnmodified( CCcacheKey, inputFile ) &&
                     engine.executionCache.isFileUnmodified( CCcacheKey, outputFile )
                  )
                  {
                     console.noteln( "Cosmatic Correction: cache ", inputFile, " --> ", outputFile );
                     cached[ inputFile ] = outputFile;
                     cachedCount++;
                  }
                  else
                  {
                     console.noteln( "Cosmetic Correction: process ", inputFile );
                     filesToProcess.push( inputFile );
                  }

               }
            }
         }
         else
         {
            console.noteln( "Cosmetic correction has no cached data for key ", CCcacheKey );
         }

         // in process container we store the full CC files
         CC.targetFrames = enableTargetFrames( filePaths, 2 );
         engine.processContainer.add( CC );

         if ( filesToProcess.length > 0 )
         {
            CC.targetFrames = enableTargetFrames( filesToProcess, 2 );
            CC.executeGlobal();
         }
         else if ( cachedCount > 0 )
            console.noteln( "Cosmetic Correction: all ", filePaths.length, " files are cached, execution is skipped." )

         /*
          * ### FIXME: CosmeticCorrection should provide read-only output
          * data, including the full file path of each output image.
          */

         for ( let c = 0; c < filePaths.length; ++c )
         {
            let filePath = filePaths[ c ];
            let ccFilePath = cosmetizedDirectory + '/' + File.extractName( filePath ) + "_cc" + ".xisf";

            if ( cached[ filePath ] )
            {
               activeFrames[ c ].processingSucceeded( WBPPFrameProcessingStep.CC, ccFilePath );
               nCached++;
            }
            else
            {

               // we mark as successful the frames that succeeded but we don't mark as failed frames for which CC failed such that wa can continue
               // using the uncosmetized versions

               if ( File.exists( ccFilePath ) )
               {
                  activeFrames[ c ].processingSucceeded( WBPPFrameProcessingStep.CC, ccFilePath );
                  // cache the result
                  CCCache[ filePath ] = ccFilePath;
                  engine.executionCache.cacheFileLMD( CCcacheKey, filePath );
                  engine.executionCache.cacheFileLMD( CCcacheKey, ccFilePath );
                  nGenerated++;
               }
               else
               {
                  console.warningln( "** Warning: File does not exist after cosmetic correction: " + ccFilePath + ", the uncosmetized frame " + activeFrames[ c ].current + "will be used." );
                  engine.processLogger.addWarning( "File does not exist after cosmetic correction: " + ccFilePath + ", the uncosmetized frame " + activeFrames[ c ].current + "will be used." );
                  nFailed++;
               }
            }
         }
         if ( nFailed == activeFrames.length )
         {
            console.warningln( "** Warning: All cosmetic corrected light frame files have been removed or cannot be accessed. Uncosmetized frames will be used." );
            engine.processLogger.addWarning( "All cosmetic corrected light frame files have been removed or cannot be accessed. Uncosmetized frames will be used." );
            this.statusMessage = "no corrected frames found";
            this.hasWarnings = true;
         }
         else if ( nFailed == 0 )
         {
            engine.processLogger.addSuccess( "Cosmetic correction completed", activeFrames.length + " light frame" + ( activeFrames.length > 1 ? "s have" : "has" ) + " been calibrated." );
         }
         else
         {
            let successCount = activeFrames.length - nFailed;
            engine.processLogger.addSuccess( "Cosmetic correction completed", successCount + " light frame" + ( successCount > 1 ? "s have" : "has" ) + " been cosmetized." );
            this.statusMessage = nFailed + " frame" + ( nFailed > 1 ? "s" : "" ) + " failed";
            this.hasWarnings = true;
         }
         // update the cache
         engine.executionCache.setCache( CCcacheKey, CCCache );
         if ( CC.useMasterDark )
            engine.executionCache.cacheFileLMD( CCcacheKey, CC.masterDarkPath );

         console.noteln( "<end><cbr><br>", SEPARATOR );
         console.noteln( "* End cosmetic correction of light frames" );
         console.noteln( SEPARATOR );
      }

      this.statusMessage = resultCountToString( nCached, nGenerated, nFailed, "corrected" );
      this.hasWarnings = this.hasWarnings || nFailed > 0;

      // return the proper status
      if ( canceled )
         return OperationBlockStatus.CANCELED;
      else
         return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.CosmeticCorrectionOperation.prototype = new WBPPOperationBlock;

StackEngine.prototype.DebayerOperation = function( frameGroup )
{
   this.__base__ = WBPPOperationBlock;
   this.__base__( "Debayer", frameGroup, true /* trackable */ );

   this.spaceRequired = () =>
   {
      let groupsize = frameGroup.groupSize();
      let size = 0;
      switch ( engine.debayerOutputMethod )
      {
         case WBPPDebayerOutputMode.COMBINED:
            size = groupsize * 3; // from mono to RGB
            break;
         case WBPPDebayerOutputMode.BOTH:
            size = groupsize * 3; // consider the combined RGB output
         case WBPPDebayerOutputMode.SEPARATED || WBPPDebayerOutputMode.BOTH:
            if ( engine.debayerActiveChannelR )
               size += groupsize; // R only is Gray
            if ( engine.debayerActiveChannelG )
               size += groupsize; // G only is Gray
            if ( engine.debayerActiveChannelB )
               size += groupsize; // B only is Gray
      }

      return size;
   };

   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => ( frameGroup.injectGroupData(
   {
      name: "Debayer",
      status: this.status,
      statusMessage: this.statusMessage
   } ) );

   let
   {
      enableTargetFrames,
      existingDirectory,
      resultCountToString
   } = WBPPUtils.shared();

   this._run = function()
   {
      let failed = false;

      if ( frameGroup.isCFA )
      {

         console.noteln( "<end><cbr><br>", SEPARATOR );
         console.noteln( "* Begin demosaicing of light frames" );
         console.noteln( SEPARATOR );

         let activeFrames = frameGroup.activeFrames();

         if ( activeFrames.length == 0 )
         {
            // report the issue
            console.warningln( "** Warning: No active light frames in the group." );
            engine.processLogger.addError( "No active light frames in the group." );
            this.statusMessage = "no active frames";
         }
         else
         {

            engine.processLogger.addSuccess( "Demosaicing with pattern", frameGroup.CFAPatternString() );
            frameGroup.log();
            console.writeln( "CFA Pattern: ", frameGroup.CFAPatternString() );
            console.writeln( "Debayer Method: ", frameGroup.debayerMethodString() );

            let DB = new Debayer;

            let subfolder = frameGroup.folderName();
            let debayerDirectory = existingDirectory( engine.outputDirectory + "/debayered/" + subfolder );

            let filePaths = activeFrames.map( activeFrame => activeFrame.current );
            DB.inputHints = engine.inputHints();
            DB.cfaPattern = frameGroup.CFAPattern;
            DB.debayerMethod = frameGroup.debayerMethod;
            // N.B. For CFAs, evaluate noise and signal with Debayer instead of ImageCalibration
            DB.evaluateNoise = DB.evaluateSignal = true;
            DB.outputDirectory = debayerDirectory;
            DB.outputExtension = ".xisf";
            DB.outputPostfix = "_d";
            DB.overwriteExistingFiles = false;

            /**
             * Check if some files are already cached and can be skipped
             */
            let DBSource = DB.toSource( "JavaScript", "DB" /*varId*/ , 0 /*indent*/ ,
               SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();

            // we set the output configuration after creating the caching keys to share the cached files amongst all configurations
            DB.outputRGBImages = engine.debayerOutputMethod != WBPPDebayerOutputMode.SEPARATED;
            DB.outputSeparateChannels = engine.debayerOutputMethod != WBPPDebayerOutputMode.COMBINED;

            // define the channels involved
            let channels;
            switch ( engine.debayerOutputMethod )
            {
               case WBPPDebayerOutputMode.COMBINED:
                  channels = [ "" ];
                  break;
               case WBPPDebayerOutputMode.SEPARATED:
                  channels = [ WBPPAssociatedChannel.R, WBPPAssociatedChannel.G, WBPPAssociatedChannel.B ];
                  break;
               default:
                  channels = [ "", WBPPAssociatedChannel.R, WBPPAssociatedChannel.G, WBPPAssociatedChannel.B ];
            }

            let filesToDebayer = filePaths;
            let cachedCount = 0;
            let cached = {};
            let DBCache = {};
            let DBcacheKey = engine.executionCache.keyFor( DBSource );
            if ( engine.executionCache.hasCacheForKey( DBcacheKey ) )
            {
               console.noteln( "Debayer has cached data for key ", DBcacheKey );

               // The cache is a map with the input file and the array of output files previously generated
               DBCache = engine.executionCache.cacheForKey( DBcacheKey );
               filesToDebayer = [];

               for ( let i = 0; i < filePaths.length; i++ )
               {
                  let inputFile = filePaths[ i ];
                  let outputFiles = DBCache[ inputFile ];

                  // output files is always an array of 4 file paths, RGB, R, G and B channels.
                  // depending on the debayer output mode, only the proper output files are checked

                  if ( outputFiles == undefined )
                     // we don't have any cached result for this input file
                     filesToDebayer.push( inputFile );
                  else
                  {
                     // the original file must be unchanged
                     let useCache = engine.executionCache.isFileUnmodified( DBcacheKey, inputFile );

                     // all output channel files must be unchanged
                     for ( let j = 0;
                        ( j < channels.length ) && useCache; j++ )
                     {
                        let channel = channels[ j ];
                        let outputFile = outputFiles[ channel ];
                        useCache = engine.executionCache.isFileUnmodified( DBcacheKey, outputFile );
                     }

                     if ( useCache )
                     {
                        cached[ inputFile ] = outputFiles;
                        cachedCount++;
                        console.noteln( "Debayer will use cache for file: ", inputFile );
                     }
                     else
                     {
                        filesToDebayer.push( inputFile );
                        console.noteln( "Debayer will debayer: ", inputFile );
                     }
                  }
               }
            }
            else
            {
               console.noteln( "Debayer has no cached data for key ", DBcacheKey );
            }

            // process is saved in container with the full list of files to be debayered
            {
               DB.targetItems = enableTargetFrames( filePaths, 2 );

               let DBSource = DB.toSource( "JavaScript", "DB" /*varId*/ , 0 /*indent*/ ,
                  SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();

               console.writeln( SEPARATOR2 );
               console.writeln( DBSource );
               console.writeln( SEPARATOR2 );
               engine.processContainer.add( DB );
            }

            let debayeredFiles = [];
            let success = true;
            if ( filesToDebayer.length > 0 )
            {
               DB.targetItems = enableTargetFrames( filesToDebayer, 2 );
               success = DB.executeGlobal();
            }
            else
               console.noteln( "Debayer has no files to process" );

            let nCached = 0;
            let nGenerated = 0;
            let nFailed = 0;

            // track which output files are referenced by the cache
            let cachedInputFiles = {};

            // populate the array of result descriptors.
            // For each active frame a map between the channel and the output file is created.
            // debayeredFiles = [
            //    {  // output data for input file #0
            //       "": "output file for RGB channel"
            //       "_R": "output file for R channel"
            //       "_G": "output file for G channel"
            //       "_B": "output file for B channel"
            //    },
            //    {  // output data for input file #1
            //       "": "output file for RGB channel"
            //       "_R": "output file for R channel"
            //       "_G": "output file for G channel"
            //       "_B": "output file for B channel"
            //    },
            //    ...
            // ]
            let j = 0;
            filePaths.forEach( filePath =>
            {
               let fileMap = {};

               if ( cached[ filePath ] != undefined )
               {
                  let cachedData = cached[ filePath ];
                  for ( let k = 0; k < channels.length; k++ )
                  {
                     let channel = channels[ k ];
                     fileMap[ channel ] = cachedData[ channel ];
                  }
                  cachedInputFiles[ filePath ] = true;
               }
               else
               {
                  let outputRow = DB.outputFileData[ j++ ];
                  let lastIndx = outputRow.length - 1;
                  let DBoutputMap = {};
                  DBoutputMap[ "" ] = outputRow[ 0 ];
                  DBoutputMap[ WBPPAssociatedChannel.R ] = outputRow[ lastIndx - 2 ];
                  DBoutputMap[ WBPPAssociatedChannel.G ] = outputRow[ lastIndx - 1 ];
                  DBoutputMap[ WBPPAssociatedChannel.B ] = outputRow[ lastIndx ];

                  // update the cached data of the only the relevant channels
                  for ( let k = 0; k < channels.length; k++ )
                  {
                     let channel = channels[ k ];
                     fileMap[ channel ] = DBoutputMap[ channel ];
                  }
                  cachedInputFiles[ filePath ] = false;
               }

               debayeredFiles.push( fileMap );
            } );

            if ( ( !success || debayeredFiles.length == 0 ) && cachedCount == 0 )
            {
               // mark all active frames as failed
               activeFrames.forEach( activeFrame => activeFrame.processingFailed() );
               // report the issue
               console.warningln( "** Warning: Light frames debayering failed." );
               engine.processLogger.addError( "Light frames debayering failed." );
               this.statusMessage = "debayering failed";
               failed = true;
            }
            else
            {

               // check which acive frame has been successfully processed
               for ( let c = 0; c < activeFrames.length; ++c )
               {
                  let inputFilePath = activeFrames[ c ].current;
                  let currentBaseName = File.extractName( inputFilePath );
                  let outputFiles = debayeredFiles[ c ];
                  let isCached = cachedInputFiles[ inputFilePath ];

                  // process the results for each active channel
                  let frameFailed = false;
                  channels.forEach( channel =>
                  {

                     let outputFilePath = outputFiles[ channel ];

                     if ( outputFilePath != undefined && outputFilePath.length > 0 )
                     {
                        if ( File.exists( outputFilePath ) )
                        {
                           activeFrames[ c ].processingSucceeded( WBPPFrameProcessingStep.DEBAYER, outputFilePath, channel );
                           engine.executionCache.cacheFileLMD( DBcacheKey, inputFilePath );
                           engine.executionCache.cacheFileLMD( DBcacheKey, outputFilePath );
                           outputFiles[ channel ] = outputFilePath;
                        }
                        else
                        {
                           let channelName = channel.length > 0 ? " channel " + channel : "";
                           console.warningln( "** Warning: File does not exist after image debayering" + channelName + ": " + currentBaseName );
                           engine.processLogger.addWarning( "File does not exist after image debayering" + channelName + ": " + currentBaseName );
                           activeFrames[ c ].processingFailed( channel );
                           frameFailed = true;
                           // invalidate the cache and the LMD for that file
                           outputFiles[ channel ] = undefined;
                           engine.executionCache.invalidateFileLMD( DBcacheKey, inputFilePath );
                           engine.executionCache.invalidateFileLMD( DBcacheKey, outputFilePath );
                        }
                     }
                     else
                     {
                        let channelName = channel.length > 0 ? "channel " + channel + " of " : "";
                        console.warningln( "** Warning: Debayer failed for " + channelName + "frame: " + currentBaseName );
                        engine.processLogger.addWarning( "Debayer failed for " + channelName + "frame: " + currentBaseName );
                        activeFrames[ c ].processingFailed( channel );
                        frameFailed = true;
                        // invalidate the cache and  the LMD for that file
                        outputFiles[ channel ] = undefined;
                        engine.executionCache.invalidateFileLMD( DBcacheKey, inputFilePath );
                        engine.executionCache.invalidateFileLMD( DBcacheKey, outputFilePath );
                     }
                  } );

                  if ( frameFailed )
                     nFailed++;
                  else if ( isCached )
                     nCached++;
                  else
                     nGenerated++;

                  // update the list of output files for the given input file
                  DBCache[ inputFilePath ] = outputFiles;
               }
               // save the DB cache
               engine.executionCache.setCache( DBcacheKey, DBCache );

               // set the status message
               this.statusMessage = resultCountToString( nCached, nGenerated, nFailed, "debayered" );
               this.hasWarnings = nFailed > 0;

               // report how many light frames exist after calibration
               let emptyChannels = [];
               channels.forEach( channel =>
               {
                  let debayerChannelName;
                  switch ( channel )
                  {
                     case "":
                        debayerChannelName = "combined";
                        break;
                     case WBPPAssociatedChannel.R:
                        debayerChannelName = "separated red";
                        break;
                     case WBPPAssociatedChannel.G:
                        debayerChannelName = "separated green";
                        break;
                     case WBPPAssociatedChannel.B:
                        debayerChannelName = "separated blue";
                  }

                  let debayeredActiveFrames = frameGroup.activeFrames( channel );
                  if ( debayeredActiveFrames.length < 1 )
                  {
                     msg = "** Warning: No frames found for " + debayerChannelName + " channel after debayering.";
                     console.warningln( msg );
                     engine.processLogger.addError( msg );
                     emptyChannels.push( debayerChannelName );
                  }
                  else
                  {
                     engine.processLogger.addSuccess( "Debayer completed", debayeredActiveFrames.length + " Light frame" + ( debayeredActiveFrames.length == 1 ? "" : "s" ) + "  debayered for " + debayerChannelName + " channel." );
                  }
               } )

               if ( emptyChannels.length > 0 )
               {
                  let last = emptyChannels.pop();
                  let commaSeparatedList = emptyChannels.join( ", " ) + ( emptyChannels.length > 0 ? " and " : "" ) + last;
                  this.statusMessage += "\nno frames generated for " + commaSeparatedList + " channel";
               }
            }
         }

         console.noteln( "<end><cbr><br>", SEPARATOR );
         console.noteln( "* End demosaicing of light frames" );
         console.noteln( SEPARATOR );
      }

      if ( failed )
         return OperationBlockStatus.FAILED;
      else
         return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.DebayerOperation.prototype = new WBPPOperationBlock;

StackEngine.prototype.ReferenceFrameDataPreparationOperation = function()
{
   this.__base__ = WBPPOperationBlock;
   this.__base__( "Reference frame data preparation", undefined, false /* trackable */ );

   /**
    * data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Reference frame data preparation",
      status: this.status,
      statusMessage: this.statusMessage
   } );

   this._run = function( environment, requestInterruption )
   {
      // here we continue using the post-process groups
      let groups = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST );

      // remove non-active groups
      groups = groups.filter( g => g.isActive );

      // returns immediatly if no post-process groups are given (this could be the case when WBPP is used to generate the master
      // bias / dark / flat only).
      if ( groups.length == 0 )
      {
         // no POST calibration groups, we can interrupt the whole processing
         requestInterruption();
         return OperationBlockStatus.CANCELED;
      }

      // ----------------------------------------------------------------------------
      // MEASURING FRAMES
      // ----------------------------------------------------------------------------

      // List of cases which requires to measure the images:
      // 1. if I choose to generate the weights
      // 2. if I do the registration and I want the best reference image to be auto-selcted
      // 3. local normalization is enabled
      let referenceFrameData = {};

      // assign the manual reference frame
      if ( engine.bestFrameRefernceMethod == WBPPBestRefernenceMethod.MANUAL )
      {
         // assume that the file is not added to the session
         let currentReferenceImage = engine.referenceImage;
         // check if the reference frame is one frame in the session
         let referenceFrameFileItem = engine.groupsManager.getReferenceFrameFileItem( engine.referenceImage );
         if ( referenceFrameFileItem )
         {
            // precedence is given to the green channel if separate debayer channels has been generated for the reference frame
            if ( referenceFrameFileItem.current[ WBPPAssociatedChannel.G ] )
            {
               currentReferenceImage = referenceFrameFileItem.current[ WBPPAssociatedChannel.G ]
               referenceFrameFileItem.markAsReference( WBPPAssociatedChannel.G );
            }
            else if ( referenceFrameFileItem.current[ "default" ] )
            {
               currentReferenceImage = referenceFrameFileItem.current[ "default" ]
               referenceFrameFileItem.markAsReference( "default" );
            }
            else
            {
               currentReferenceImage = referenceFrameFileItem.filePath;
            }
         }
         // by convention assume to configure the "auto_single" mode with the manual reference file
         referenceFrameData[ "__manual__" ] = {
            /* no groups to be measured */
            groups: [],
            referenceImage: currentReferenceImage
         }
      }
      else if ( engine.bestFrameRefernceMethod == WBPPBestRefernenceMethod.AUTO_SINGLE )
      {
         referenceFrameData[ "__single__" ] = {
            /* measure all groups  */
            groups: groups
         }
      }
      else
      {
         // aggregate groups by keyword
         referenceFrameData = groups.reduce( ( acc, group ) =>
         {
            let value = group.keywords[ engine.bestFrameReferenceKeyword ] || "__undefined__";
            if ( acc[ value ] )
               acc[ value ].groups.push( group );
            else
               acc[ value ] = {
                  groups: [ group ]
               };
            return acc;
         },
         {} );
      }

      // for each keyword store the lowest binning
      Object.keys( referenceFrameData ).forEach( key =>
      {
         let groups = referenceFrameData[ key ].groups;
         let lowestBinning = groups.reduce( ( acc, group ) =>
         {
            return Math.min( group.binning, acc );
         }, 256 );
         referenceFrameData[ key ].lowestBinning = lowestBinning;
      } );

      // store the reference frame data for further processing
      environment.referenceFrameData = referenceFrameData;
      return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.ReferenceFrameDataPreparationOperation.prototype = new WBPPOperationBlock;

StackEngine.prototype.MeasurementOperation = function()
{
   this.__base__ = WBPPOperationBlock;
   this.__base__( "Measurements", undefined, true /* trackable */ );

   /**
    * data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Measurements",
      status: this.status,
      statusMessage: this.statusMessage
   } );

   let
   {
      resultCountToString
   } = WBPPUtils.shared();

   this._run = function( environment )
   {
      let failed = false;

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Perform image measurements" );
      console.noteln( SEPARATOR );
      console.flush();
      engine.processLogger.newLine();
      engine.processLogger.addMessage( "<b>*****************</b> <i>IMAGE MEASUREMENTS</i> <b>**************</b>" );

      // get the reference frame data from the environment
      let referenceFrameData = environment.referenceFrameData;

      // using the active post-process groups
      let groups = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST ).filter( g => g.isActive );

      // retrieve the whole list of frames to be measured
      let framesToMeasure = [];

      let generateSubframesWeights = engine.subframeWeightingEnabled && ( engine.subframesWeightsMethod == WBPPSubframeWeightsMethod.FORMULA );
      if ( generateSubframesWeights || engine.localNormalization )
      {
         // all frames must be measured
         for ( let i = 0; i < groups.length; i++ )
            framesToMeasure = framesToMeasure.concat( groups[ i ].activeFrames() );
      }
      else
      {
         // measurement is used only to find the reference frame
         // measure only the groups with the lowest binning for each keyword
         framesToMeasure = Object.keys( referenceFrameData ).reduce( ( acc, key ) =>
         {
            let lowBinningGroups = referenceFrameData[ key ].groups.filter( group => group.binning == referenceFrameData[ key ].lowestBinning );
            return acc.concat( lowBinningGroups );
         }, [] ).reduce( ( acc, group ) =>
         {
            return acc.concat( group.activeFrames() );
         }, [] );
      }

      if ( framesToMeasure.length == 0 )
      {
         console.criticalln( "No active frames to be measured." );
         engine.processLogger.addError( "No active frames to be measured." );
         this.statusMessage = "No active frames to be measured";
         this.hasWarnings = true;
      }

      // measure the frames
      let
      {
         nCached,
         nMeasured,
         nFailed
      } = engine.computeDescriptors( framesToMeasure );

      console.noteln( "SS nCached   : ", nCached );
      console.noteln( "SS nMeasured : ", nMeasured );
      console.noteln( "SS nFailed   : ", nFailed );

      // report
      if ( ( nMeasured + nCached ) == framesToMeasure.length )
      {
         console.noteln( "All frame measurements completed successfully" );
         engine.processLogger.addSuccess( "Frames measurement:", "completed successfully" );
      }
      else if ( nFailed < framesToMeasure.length )
      {
         let measuredCount = nMeasured + nCached;
         console.warningln( "Measurements completed successfully for " + measuredCount + " light frame" + ( measuredCount == 1 ? "" : "s" ) + " over " + framesToMeasure.length + "." );
         engine.processLogger.addSuccess( "Measurements completed successfully", measuredCount + " light frame" + ( measuredCount == 1 ? "" : "s" ) + " over " + framesToMeasure.length + "." );
         this.statusMessage = "measurement failed on " + nFailed + " frame" + ( nFailed > 1 ? "s" : "" );
         this.hasWarnings = true;
      }
      else
      {
         console.criticalln( "Frame measurement failed for all light frames." );
         engine.processLogger.addError( "Frame measurement failed for all light frames." );
         this.statusMessage = "measurement failed on all frames";
         failed = true;
      }

      let countString = resultCountToString( nCached, nMeasured, nFailed, "measured" );
      this.statusMessage = countString.length > 0 ? countString : framesToMeasure.length + " measured";
      this.hasWarnings = this.hasWarnings || nFailed > 0;

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* End generation of image descriptors" );
      console.noteln( SEPARATOR );
      console.flush();
      engine.processLogger.addMessage( "<b>" + SEPARATOR + "</b>" );
      engine.processLogger.newLine();

      if ( failed )
         return OperationBlockStatus.FAILED;
      else
         return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.MeasurementOperation.prototype = new WBPPOperationBlock;

StackEngine.prototype.CustomFormulaWeightsGenerationOperation = function()
{
   this.__base__ = WBPPOperationBlock;
   this.__base__( "Weights generation", undefined, true /* trackable */ );

   /**
    * data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Weights generation",
      status: this.status,
      statusMessage: this.statusMessage
   } );

   let
   {
      resultCountToString
   } = WBPPUtils.shared();

   this._run = function()
   {
      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Generate the custom formula weights" );
      console.noteln( SEPARATOR );
      console.flush();
      engine.processLogger.newLine();
      engine.processLogger.addMessage( "<b>*****************</b> <i>CUSTOM WEIGHTS GENERATION</i> <b>**************</b>" );

      // scan all groups and generate the formula weights
      let groups = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST ).filter( g => g.isActive );

      let nGenerated = 0;
      let nFailed = 0;
      for ( let i = 0; i < groups.length; ++i )
      {
         let activeFrames = groups[ i ].activeFrames().filter( af => af.descriptor );
         let descriptors = activeFrames.map( frame => frame.descriptor ).filter( Boolean );
         let descriptorMinMax = engine.getMinMaxDescriptorsValues( descriptors );

         // pre-compute non normalized weights
         let scores = activeFrames.map( af =>
         {
            return engine.computeWeightForLight(
               af.descriptor,
               descriptorMinMax,
               engine.FWHMWeight,
               engine.eccentricityWeight,
               engine.SNRWeight,
               engine.starsWeight,
               engine.PSFSignalWeight,
               engine.PSFSNRWeight,
               engine.pedestal,
               1 /*normalization factor*/ ,
               false /* print to console */ );
         } );

         let normalizationFactor = Math.max.apply( null, scores.filter( score => isFinite( score ) ) );

         for ( let j = 0; j < activeFrames.length; ++j )
         {

            let descriptor = activeFrames[ j ].descriptor;

            let weight = engine.computeWeightForLight(
               descriptor,
               descriptorMinMax,
               engine.FWHMWeight,
               engine.eccentricityWeight,
               engine.SNRWeight,
               engine.starsWeight,
               engine.PSFSignalWeight,
               engine.PSFSNRWeight,
               engine.pedestal,
               normalizationFactor,
               true /* print to console */ );


            if ( isFinite( weight ) )
            {
               descriptor.imageWeight = weight;
               descriptor.formulaWeight = weight * normalizationFactor;
               nGenerated++;
            }
            else
            {
               console.warningln( "Custom formula failed for frame ", activeFrames[ j ].current );
               activeFrames[ j ].processingFailed();
               nFailed++;
            }
         }
      }

      this.statusMessage = resultCountToString( 0 /* nCached */ , nGenerated, nFailed, "weight(s) generated" );
      this.hasWarnings = nFailed > 0;

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* End generation of image weights" );
      console.noteln( SEPARATOR );
      console.flush();


      if ( nFailed == 0 )
         engine.processLogger.addSuccess( "Success", nGenerated + " weight(s) generated" );
      else
         engine.processLogger.addWarning( this.statusMessage );
      engine.processLogger.addMessage( "<b>" + SEPARATOR + "</b>" );
      engine.processLogger.newLine();

   };
};
StackEngine.prototype.CustomFormulaWeightsGenerationOperation.prototype = new WBPPOperationBlock;

StackEngine.prototype.BadFramesRejectionOperation = function()
{
   this.__base__ = WBPPOperationBlock;
   this.__base__( "Bad frames rejection", undefined, true /* trackable */ );

   /**
    * data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Bad frames rejection",
      status: this.status,
      statusMessage: this.statusMessage
   } );

   let _weightForFrame = function( activeFrame )
   {
      let descriptor = activeFrame.descriptor;
      if ( descriptor == undefined )
         return undefined;

      switch ( engine.subframesWeightsMethod )
      {
         case WBPPSubframeWeightsMethod.PSFSignal:
            return descriptor.PSFSignalWeight;
         case WBPPSubframeWeightsMethod.PSFSNR:
            return descriptor.PSFSNR;
         case WBPPSubframeWeightsMethod.PSFScaleSNR:
            return undefined;
         case WBPPSubframeWeightsMethod.SNREstimate:
            return descriptor.SNR;
         case WBPPSubframeWeightsMethod.FORMULA:
            return descriptor.imageWeight;
      }
      return undefined;
   }

   this._run = function()
   {

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Bad frames rejection" );
      console.noteln( SEPARATOR );
      engine.processLogger.newLine();
      engine.processLogger.addMessage( "<b>*****************</b> <i>BAD FRAMES REJECTION</i> <b>**************</b>" );

      let discardedFrames = 0;
      let groups = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST ).filter( g => g.isActive );

      let totalFrames = groups.reduce( ( acc, group ) => ( acc + group.activeFrames().length ), 0 );
      if ( engine.subframesWeightsMethod != WBPPSubframeWeightsMethod.PSFScaleSNR )
      {

         // using the active post-process groups


         for ( let i = 0; i < groups.length; i++ )
         {
            let group = groups[ i ];
            let activeFrames = group.activeFrames();

            // find the max value of the current weighting method
            let maxValue = activeFrames.reduce( ( value, frame ) =>
            {
               let weight = _weightForFrame( frame );

               if ( weight == undefined )
                  return value;

               return Math.max( weight, value );
            }, 0 );

            // if all weights are invalid then skip the group filtering
            if ( maxValue == 0 )
               continue;

            for ( let j = 0; j < activeFrames.length; j++ )
            {
               let normalizedWeight = ( _weightForFrame( activeFrames[ j ] ) || 0 ) / maxValue;
               let info = File.extractNameAndExtension( activeFrames[ j ].current ) +
                  " - " +
                  format( "%.3f", normalizedWeight ) +
                  ( normalizedWeight < engine.minWeight ? " < " : " > " ) +
                  format( "%.3f", engine.minWeight ) +
                  " | " +
                  ( normalizedWeight < engine.minWeight ? "rejected" : "accepted" );

               console.noteln( "[Frames rejection] ", info );
               if ( normalizedWeight < engine.minWeight )
               {
                  engine.processLogger.addWarning( "frame rejected [" + info + "]: " + activeFrames[ j ].current );
                  activeFrames[ j ].processingFailed();
                  discardedFrames++;
               }
            }
         }
      }

      this.statusMessage = discardedFrames + " rejected";

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* End of bad frames rejection" );
      console.noteln( SEPARATOR );

      engine.processLogger.addMessage( this.statusMessage );
      engine.processLogger.addMessage( "<b>" + SEPARATOR + "</b>" );
      engine.processLogger.newLine();
   };
};
StackEngine.prototype.BadFramesRejectionOperation.prototype = new WBPPOperationBlock;

StackEngine.prototype.WriteWeightsOperation = function()
{
   this.__base__ = WBPPOperationBlock;
   this.__base__( "Write weights", undefined, true /* trackable */ );

   this.spaceRequired = () =>
   {
      let preprocessGroups = engine.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

      // we duplicate the original files if they are not processed
      let size = 0;
      for ( let i = 0; i < preprocessGroups.length; ++i )
      {
         if ( preprocessGroups[ i ].imageType == ImageType.LIGHT )
         {

            let cg = engine.getCalibrationGroupsFor( preprocessGroups[ i ] );
            let doCalibrate = cg.masterBias || cg.masterDark || cg.masterFlat || false;
            let doCC = preprocessGroups[ i ].CCTemplate || false;
            let doDebayer = preprocessGroups[ i ].isCFA;

            if ( !doCalibrate && !doCC && !doDebayer && !preprocessGroups[ i ].isVirtual() && !engine.linearPatternSubtraction && engine.subframesWeightsMethod == WBPPSubframeWeightsMethod.FORMULA )
               size += preprocessGroups[ i ].groupSize();
         }
      }

      return size;
   };

   /**
    * data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Write weights",
      status: this.status,
      statusMessage: this.statusMessage
   } );

   this._run = function()
   {
      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Embed measurements and weights into FITS headers" );
      console.noteln( SEPARATOR );
      console.flush();
      engine.processLogger.newLine();
      engine.processLogger.addMessage( "<b>**************</b> <i>WRITE WEIGHTS AND MEASUREMENTS</i> <b>***********</b>" );

      // using the active post-process groups
      let groups = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST ).filter( g => g.isActive );
      engine.writeWeightsWithDescriptors( groups );

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* End embedding measurements and weight into FITS headers" );
      console.noteln( SEPARATOR );
      console.flush();
      engine.processLogger.addMessage( "Embedding measurements and weight completed." );
      engine.processLogger.addMessage( "<b>" + SEPARATOR + "</b>" );
      engine.processLogger.newLine();
   };
};
StackEngine.prototype.WriteWeightsOperation.prototype = new WBPPOperationBlock;

StackEngine.prototype.ReferenceFrameSelectionOperation = function()
{
   this.__base__ = WBPPOperationBlock;
   this.__base__( "Reference frame selection", undefined, true /* trackable */ );

   /**
    * data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Reference frame selection",
      status: this.status,
      statusMessage: this.statusMessage
   } );

   this._run = function( environment, requestInterruption )
   {
      let criticalErrorOccurred = false;

      engine.processLogger.newLine();
      engine.processLogger.addMessage( "<b>**********</b> <i>BEST REFERENCE FRAME FOR REGISTRATION</i> <b>***********</b>" );

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Begin selection of the best reference frame" );
      console.noteln( SEPARATOR );
      console.flush();

      // get the reference frame data from the environment
      let referenceFrameData = environment.referenceFrameData;

      // using the active post-process groups
      let groups = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST ).filter( g => g.isActive );

      // initialize the reference frame per group
      for ( let i = 0; i < groups.length; i++ )
         groups[ i ].__reference_frame__ = undefined;

      let nSelected = 0;
      let nFailed = 0;

      if ( engine.bestFrameRefernceMethod == WBPPBestRefernenceMethod.MANUAL )
      {
         // search in all files the selected frame, in case take as reference the
         // current processed file name
         console.noteln( "Best reference frame, manually selected: " + referenceFrameData[ "__manual__" ].referenceImage );
         engine.processLogger.addSuccess( "Best reference frame", " manually selected " + referenceFrameData[ "__manual__" ].referenceImage );

         // save reference frame for all groups
         for ( let i = 0; i < groups.length; i++ )
            groups[ i ].__reference_frame__ = referenceFrameData[ "__manual__" ].referenceImage;
         this.statusMessage = "Manual"
      }
      else
      {
         if ( engine.bestFrameRefernceMethod == WBPPBestRefernenceMethod.AUTO_SINGLE )
         {
            let actualReferenceImage = engine.findRegistrationReferenceFileItem( referenceFrameData[ "__single__" ].groups );
            if ( actualReferenceImage )
            {
               console.noteln( "Best reference frame for registration - auto selection completed: " + actualReferenceImage.current );
               engine.processLogger.addSuccess( "Best reference frame", " (auto selection) " + actualReferenceImage.current );

               // save reference frame for all groups
               for ( let i = 0; i < groups.length; i++ )
                  groups[ i ].__reference_frame__ = actualReferenceImage.current;

               // mark the frame as reference
               actualReferenceImage.markAsReference();
               nSelected++;
            }
            else
            {
               // mark all frames as failed
               for ( let i = 0; i < groups.length; i++ )
               {
                  let activeFrames = groups[ i ].activeFrames();
                  for ( let j = 0; j < activeFrames.length; j++ )
                     activeFrames[ j ].processingFailed();
               }

               console.criticalln( "Unable to detect the best reference frame." );
               engine.processLogger.addError( "Unable to detect the best reference frame." );
               this.statusMessage = "failed to detect the reference frame"
               criticalErrorOccurred = true;
            }
         }
         else
         {
            let keywordsFailed = [];
            let referenceFrameDataKeys = Object.keys( referenceFrameData );
            referenceFrameData = referenceFrameDataKeys.reduce( ( acc, keywordValue ) =>
            {
               let data = referenceFrameData[ keywordValue ];
               let actualReferenceImage = engine.findRegistrationReferenceFileItem( data.groups );
               console.noteln( "<end><cbr><br>" );
               if ( actualReferenceImage )
               {
                  console.noteln( "Best reference frame for " + engine.bestFrameReferenceKeyword + " = " + keywordValue + " : " + actualReferenceImage.current );
                  engine.processLogger.addSuccess( "Best reference frame for " + engine.bestFrameReferenceKeyword + " = " + keywordValue, actualReferenceImage.current );

                  // store the reference image in the current groups per keyword
                  for ( let i = 0; i < data.groups.length; i++ )
                     data.groups[ i ].__reference_frame__ = actualReferenceImage.current;

                  // mark the frame as reference
                  actualReferenceImage.markAsReference();
                  nSelected++;
               }
               else
               {
                  // mark all frames in the current goups per keyword as failed
                  for ( let i = 0; i < data.groups.length; i++ )
                  {
                     let activeFrames = data.groups[ i ].activeFrames();
                     for ( let j = 0; j < activeFrames.length; j++ )
                        activeFrames[ j ].processingFailed();
                  }

                  let msg = "Unable to detect the best reference frame for " + engine.bestFrameReferenceKeyword + " = " + keywordValue +
                     ". Groups with this key/value will not be registered.";
                  console.criticalln( msg );
                  engine.processLogger.addError( msg );
                  keywordsFailed.push( keywordValue );
                  nFailed++;
               }
               return acc;
            },
            {} );

            if ( keywordsFailed.length == referenceFrameDataKeys.length )
            {
               this.statusMessage = "failed to detect all reference frames";
               criticalErrorOccurred = true;
            }
            else if ( keywordsFailed.length > 0 )
            {
               let last = keywordsFailed.pop();
               let commaSeparatedList = keywordsFailed.join( ", " ) + ( keywordsFailed.length > 0 ? " and " : "" ) + last;
               this.statusMessage = "failed to assign a reference frame for " + commaSeparatedList + " keyword" + ( keywordsFailed.length > 1 ? "s" : "" );
               this.hasWarnings = true;
            }

            console.noteln( "<end><cbr><br>", SEPARATOR );
            console.noteln( "* End selection of the best reference frame" );
            console.noteln( "<end><cbr><br>", SEPARATOR );
            console.flush();

            this.statusMessage = nSelected + " frame(s) selected";
            if ( nFailed > 0 )
               this.statusMessage += ", " + nFailed + " failed";
         }
      }

      engine.processLogger.addMessage( "<b>" + SEPARATOR + "</b>" );
      engine.processLogger.newLine();

      if ( criticalErrorOccurred )
      {
         requestInterruption();
         return OperationBlockStatus.CANCELED;
      }

      return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.ReferenceFrameSelectionOperation.prototype = new WBPPOperationBlock;

function resetImageSolverConfiguration( solver )
{
   /*
    * Default parameters for ImageSolver version 6.0 (core version 1.8.9-2)
    */
   solver.solverCfg.autoMagnitude = true;
   solver.solverCfg.autoPSF = false;
   solver.solverCfg.brightThreshold = 3.0;
   solver.solverCfg.catalog = "PPMXL";
   solver.solverCfg.catalogMode = CatalogMode.prototype.Automatic;
   solver.solverCfg.distortedCorners = false;
   solver.solverCfg.distortionCorrection = true;
   solver.solverCfg.distortionModelPath = null;
   solver.solverCfg.enableSimplifier = true;
   solver.solverCfg.files = [];
   solver.solverCfg.generateDistortModel = false;
   solver.solverCfg.generateErrorImg = false;
   solver.solverCfg.hotPixelFilterRadius = 1;
   solver.solverCfg.magnitude = 12;
   solver.solverCfg.maxIterations = 100;
   solver.solverCfg.maxStarDistortion = 0.6;
   solver.solverCfg.minStructureSize = 0;
   solver.solverCfg.noiseReductionFilterRadius = 0;
   solver.solverCfg.onlyOptimize = false;
   solver.solverCfg.optimizeSolution = true;
   solver.solverCfg.outSuffix = "_WCS";
   solver.solverCfg.peakResponse = 0.5;
   solver.solverCfg.projection = 0;
   solver.solverCfg.projectionOriginMode = 0;
   solver.solverCfg.psfSearchRadius = 2;
   solver.solverCfg.restrictToHQStars = false;
   solver.solverCfg.sensitivity = 0.5;
   solver.solverCfg.showDistortion = false;
   solver.solverCfg.showSimplifiedSurfaces = false;
   solver.solverCfg.showStars = false;
   solver.solverCfg.simplifierRejectFraction = 0.10;
   solver.solverCfg.simplifierTolerance = 0.05;
   solver.solverCfg.splineSmoothing = 0.010;
   solver.solverCfg.structureLayers = 5;
   solver.solverCfg.useActive = true;
   solver.solverCfg.useDistortionModel = false;
   solver.solverCfg.vizierServer = "http://vizier.u-strasbg.fr/";
}

function InitializeSolver( solver, imageWindow )
{
   function defaults()
   {

      solver.metadata.dec = engine.imageSolverDec;
      solver.metadata.epoch = engine.imageSolverEpoch;
      solver.metadata.focal = engine.imageSolverFocalLength;
      solver.metadata.ra = engine.imageSolverRa;
      solver.metadata.referenceSystem = "ICRS";
      solver.metadata.topocentric = false;
      solver.metadata.useFocal = true;
      solver.metadata.xpixsz = engine.imageSolverPixelSize;
      // derived
      solver.metadata.resolution = isNaN( solver.metadata.focal ) ? 0 : solver.metadata.xpixsz / solver.metadata.focal * 0.18 / Math.PI;
      solver.metadata.SaveParameters();
   }

   try
   {
      // always init the default values
      defaults();
      solver.Init( imageWindow, engine.imageSolverForceDefaults );
      return true;
   }
   catch ( e )
   {
      console.warningln( e );
      return false;
   }
}

function SolveImage( solver, image )
{
   try
   {
      let result = solver.SolveImage( image );
      if ( result )
      {
         // save configuration and metadata on success
         solver.solverCfg.SaveSettings();
         solver.solverCfg.SaveParameters();
         solver.metadata.SaveSettings();
         solver.metadata.SaveParameters();

         engine.imageSolverEpoch = solver.metadata.epoch;
         engine.imageSolverDec = solver.metadata.dec;
         engine.imageSolverRa = solver.metadata.ra;
         engine.imageSolverFocalLength = solver.metadata.focal;
         engine.imageSolverPixelSize = solver.metadata.xpixsz;
      }
      return result;
   }
   catch ( e )
   {
      console.warningln( e );
      return false;
   }
}

StackEngine.prototype.PlatesolvingOperation = function()
{

   this.__base__ = WBPPOperationBlock;
   this.__base__( "Plate solving reference frames", undefined, true /* trackable */ );

   /**
    * data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Plate solving reference frames",
      status: this.status,
      statusMessage: this.statusMessage
   } );

   let
   {
      getLastModifiedDate,
      hasAstrometricSolution,
      resultCountToString,
      hasSplineAstrometricSolutionMetadata
   } = WBPPUtils.shared();

   this._run = function()
   {
      engine.processLogger.newLine();
      engine.processLogger.addMessage( "<b>**********</b> <i>PLATESOLVING REFERENCE FRAMES</i> <b>***********</b>" );

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Platesolve the reference frames" );
      console.noteln( SEPARATOR );

      // using the active post-process groups
      let groups = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST ).filter( g => g.isActive );

      // save reference frames in a map
      let referenceFrames = {}

      for ( let i = 0; i < groups.length; i++ )
         if ( groups[ i ].__reference_frame__ != undefined )
            referenceFrames[ groups[ i ].__reference_frame__ ] = i;

      // map the reference frame descriptors
      let referenceFrameDescriptors = {};
      for ( let i = 0; i < groups.length; i++ )
      {
         if ( groups[ i ].imageType != ImageType.LIGHT )
            continue;

         let activeFrames = groups[ i ].activeFrames();
         for ( let j = 0; j < activeFrames.length; j++ )
            if ( referenceFrames[ activeFrames[ j ].current ] != undefined )
               referenceFrameDescriptors[ activeFrames[ j ].current ] = activeFrames[ j ].descriptor;
      }

      let nCached = 0;
      let nSuccess = 0;
      let nFailed = 0;

      // loop through the frames and attempt to platesolve
      let filePaths = Object.keys( referenceFrames );
      for ( let i = 0; i < filePaths.length; i++ )
      {
         let filePath = filePaths[ i ];

         // find the reference frame descriptor

         let IScacheKey = engine.executionCache.keyFor( "plateSolve_" + filePath );
         if ( engine.executionCache.isFileUnmodified( IScacheKey, filePath ) )
         {
            engine.processLogger.addSuccess( "Astrometric solution cached for file ", filePath );
            console.noteln( "<end><cbr><br>" + "=".repeat( 79 ) );
            console.noteln( "*** Astrometric solution cached for file ", filePath );
            nCached++;
            continue;
         }

         // open the image and check for the astrometric solution
         let referenceImageWindow = ImageWindow.open( filePath );
         if ( referenceImageWindow.length > 0 )
            referenceImageWindow = referenceImageWindow[ 0 ];

         try
         {
            if ( hasAstrometricSolution( referenceImageWindow ) )
            {
               if ( hasSplineAstrometricSolutionMetadata( referenceImageWindow ) )
               {
                  // image already has an astrometric solution, close and continue
                  engine.processLogger.addSuccess( "Astrometric solution found for file ", filePath );
                  nSuccess++;
               }
               else
               {
                  engine.processLogger.addSuccess( "Improving the astrometric solution by enabling distortion corrections for file ", filePath );
                  if ( engine.solveImage( referenceImageWindow, true /* optimize at first */ ) )
                  {
                     nSuccess++;
                     console.noteln( "Astrometric solution improved." );
                     let previousLMD = getLastModifiedDate( filePath );
                     referenceImageWindow.save( false /* allow messages */ );
                     let newLMD = getLastModifiedDate( filePath );
                     engine.executionCache.updateLMD( filePath, previousLMD, newLMD );
                     engine.executionCache.cacheFileLMD( IScacheKey, filePath );
                  }
               }

               referenceImageWindow.forceClose();
               continue;
            }
         }
         catch ( e )
         {
            console.warningln( e );
            console.noteln( "* An error occurred handling the astrometric solution, attempting to solve manually..." );
         }

         let success = engine.solveImage( referenceImageWindow );

         if ( success )
         {
            nSuccess++;
            // log
            console.noteln( "<end><cbr><br>" + "=".repeat( 79 ) );
            console.noteln( "*** Astrometric solution found for file " + filePath );
            engine.processLogger.addSuccess( "Astrometric solution found", filePath );
            console.writeln( referenceImageWindow.astrometricSolutionSummary() );
            // update the cache
            let previousLMD = getLastModifiedDate( filePath );
            referenceImageWindow.save( false /* allow maessages */ );
            let newLMD = getLastModifiedDate( filePath );
            engine.executionCache.updateLMD( filePath, previousLMD, newLMD );
            engine.executionCache.cacheFileLMD( IScacheKey, filePath );
         }
         else
         {
            nFailed++;
            // log
            console.warningln( "*** Astrometric solution failed for file " + filePath );
            engine.processLogger.addWarning( "Astrometric solution failed for file " + filePath );
            // invalidate the cache
            engine.executionCache.invalidateFileLMD( IScacheKey, filePath );
         }

         referenceImageWindow.forceClose();
      }

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* End platesolving reference frames" );
      console.noteln( SEPARATOR );
      engine.processLogger.addMessage( "<b>" + SEPARATOR + "</b>" );
      engine.processLogger.newLine();

      // report the result
      this.hasWarnings = nFailed > 0;
      if ( filePaths.length > 1 )
      {
         if ( this.hasWarnings )
            this.statusMessage = resultCountToString( nCached, nSuccess, nFailed, "solved" );
      }

      if ( nFailed == filePaths.length )
         return OperationBlockStatus.FAILED;
      else
         return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.PlatesolvingOperation.prototype = new WBPPOperationBlock;

StackEngine.prototype.RegistrationOperation = function( frameGroup )
{
   this.__base__ = WBPPOperationBlock;
   this.__base__( "Registration", frameGroup, true /* trackable */ );

   this.spaceRequired = () =>
   {
      return frameGroup.groupSize();
   };

   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => ( frameGroup.injectGroupData(
   {
      name: "Registration",
      status: this.status,
      statusMessage: this.statusMessage
   } ) );

   let
   {
      enableTargetFrames,
      existingDirectory,
      resultCountToString
   } = WBPPUtils.shared();

   this._run = function()
   {
      engine.processLogger.newLine();
      engine.processLogger.addMessage( frameGroup.logStringHeader( 'IMAGE REGISTRATION' ) );

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Begin registration of light frames" );
      console.noteln( SEPARATOR );
      frameGroup.log();
      console.flush();

      let activeFrames = frameGroup.activeFrames();

      if ( activeFrames.length == 0 )
      {
         console.warningln( "** Warning: No active light frames to be registered." );
         engine.processLogger.addError( "No active light frames to be registered." );
         this.statusMessage = "no active frames";
         return OperationBlockStatus.CANCELED;
      }

      console.noteln( ' ' );
      console.noteln( 'Reference image: ', frameGroup.__reference_frame__ );
      engine.processLogger.addSuccess( 'Reference image', frameGroup.__reference_frame__ );
      let SA = new StarAlignment;

      let subfolder = frameGroup.folderName();
      let registerDirectory = engine.outputDirectory + "/registered/" + subfolder;
      registerDirectory = existingDirectory( registerDirectory );

      let filePaths = activeFrames.map( item => item.current );
      SA.inputHints = engine.inputHints();
      SA.outputHints = engine.outputHints();
      SA.referenceImage = frameGroup.__reference_frame__;
      SA.referenceIsFile = true;
      SA.outputDirectory = registerDirectory;
      SA.generateDrizzleData = true;
      SA.pixelInterpolation = engine.pixelInterpolation;
      SA.clampingThreshold = engine.clampingThreshold;
      SA.structureLayers = engine.structureLayers;
      SA.hotPixelFilterRadius = engine.hotPixelFilterRadius;
      SA.noiseReductionFilterRadius = engine.noiseReductionFilterRadius;
      SA.minStructureSize = engine.minStructureSize;
      SA.sensitivity = engine.sensitivity;
      SA.peakResponse = engine.peakResponse;
      SA.brightThreshold = engine.brightThreshold;
      SA.maxStarDistortion = engine.maxStarDistortion;
      SA.allowClusteredSources = engine.allowClusteredSources;
      SA.useTriangles = engine.useTriangleSimilarity;
      SA.outputExtension = ".xisf";
      SA.outputPrefix = "";
      SA.outputPostfix = "_r";
      SA.outputSampleFormat = StarAlignment.prototype.f32;
      SA.overwriteExistingFiles = false;
      SA.inheritAstrometricSolution = engine.platesolve;

      if ( engine.distortionCorrection )
         SA.distortionCorrection = true;
      else
         SA.maxStars = engine.maxStars;

      // Override star alignment parameters if the current group is a separated RGB channel
      if ( frameGroup.associatedRGBchannel != undefined )
      {
         SA.distortionCorrection = true;
         SA.noiseReductionFilterRadius = Math.max( 2, engine.noiseReductionFilterRadius );
      }

      let SASource = SA.toSource( "JavaScript", "SA" /*varId*/ , 0 /*indent*/ ,
         SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();
      console.writeln( SEPARATOR2 );
      console.writeln( SASource );
      console.writeln( SEPARATOR2 );

      /**
       * Skip alignment for files that have valid cached data. Cache is a map between an input file and an
       * output file:
       * [inputFile]: outputFile
       */
      let filesToRegister = filePaths;
      let cachedCount = 0;
      let cached = {};
      let SACache = {};
      let SAcacheKey = engine.executionCache.keyFor( SASource + "_" + engine.platesolve );
      if ( engine.executionCache.hasCacheForKey( SAcacheKey ) )
      {
         console.noteln( "StarAlignment: cache data for key ", SAcacheKey );

         if ( engine.executionCache.isFileUnmodified( SAcacheKey, frameGroup.__reference_frame__ ) )
         {
            // Star Alignment has already been executed with such configuration and the registered frame is unchanged.
            // We avoid to align frames that have already been processed by this configuration.
            // The cache data is a map between input and output (registered) file, if both are unchanged then
            // we remove them from the list of the files to be aligned.
            filesToRegister = [];
            SACache = engine.executionCache.cacheForKey( SAcacheKey );
            for ( let i = 0; i < filePaths.length; i++ )
            {
               let inputFile = filePaths[ i ];
               let outputFile = SACache[ inputFile ];
               let drizzleFilePath = outputFile != undefined ? File.changeExtension( outputFile, ".xdrz" ) : "";

               if ( outputFile != undefined &&
                  engine.executionCache.isFileUnmodified( SAcacheKey, inputFile ) &&
                  engine.executionCache.isFileUnmodified( SAcacheKey, outputFile ) &&
                  engine.executionCache.isFileUnmodified( SAcacheKey, drizzleFilePath )
               )
               {
                  console.noteln( "StarAlignment: cache ", inputFile, " --> ", outputFile );
                  cached[ inputFile ] = outputFile;
                  cachedCount++;
               }
               else
               {
                  console.noteln( "StarAlignment: align ", inputFile );
                  filesToRegister.push( inputFile );
               }
            }
         }
         else
         {
            console.noteln( "StarAlignment: reference frame has changed, ignore cache and register all frames." );
         }
      }
      else
      {
         console.noteln( "StarAlignment: no cache data for key ", SAcacheKey );
      }

      // in process container we store the full SA measured files
      SA.targets = enableTargetFrames( filePaths, 3 );
      engine.processContainer.add( SA );

      // set the files to be measured and proceed
      let success = true;
      if ( filesToRegister.length > 0 )
      {
         SA.targets = enableTargetFrames( filesToRegister, 3 );
         success = SA.executeGlobal();
         engine.executionCache.cacheFileLMD( SAcacheKey, frameGroup.__reference_frame__ );
      }

      if ( !success && cachedCount == 0 )
      {
         // critical error, the output data must always have the same size of the input data
         // mark all active frames as failed
         activeFrames.forEach( activeFrame => activeFrame.processingFailed() );
         // report the issue
         console.warningln( "** Warning: Error registering light frames. This group will be skipped." );
         engine.processLogger.addError( "Error registering light frames. This group will be skipped." );
         this.statusMessage = "registration failed";
         return OperationBlockStatus.FAILED;
      }

      if ( filesToRegister.length > 0 && cachedCount == 0 )
         if ( !SA.outputData || SA.outputData.length != filesToRegister.length )
         {
            // critical error, the output data must always have the same size of the input data
            // mark all active frames as failed
            activeFrames.forEach( activeFrame => activeFrame.processingFailed() );
            // report the issue
            console.warningln( "** Warning: Light frames registration failed, the output data size mismatches the input data size." );
            engine.processLogger.addError( "Light frames registration failed, output data size mismatch." );
            this.statusMessage = "a critical error occurred (data size mismatch).";
            return OperationBlockStatus.FAILED;
         }

      // create a support for output file matching
      let registeredFiles = SA.outputData.map( outputItem => outputItem[ 0 ] );

      let nCached = 0;
      let nRegistered = 0;
      let nFailed = 0;

      // scan all input files and detect the corresponding registered version
      let j = 0;
      for ( let c = 0; c < activeFrames.length; ++c )
      {
         let inputFile = filePaths[ c ];
         let outputFile;
         let cachedFile = false;

         // input file may be mapped in the cache or picked from the SA output list (if not cached)
         if ( cached[ inputFile ] != undefined )
         {
            cachedFile = true;
            outputFile = cached[ inputFile ];
         }
         else
            // just aligned
            outputFile = registeredFiles[ j++ ];

         let errorMsg = "";
         let success = false;

         if ( outputFile )
         {
            if ( outputFile.length > 0 )
            {
               if ( File.exists( outputFile ) )
               {
                  success = true;
                  activeFrames[ c ].processingSucceeded( WBPPFrameProcessingStep.REGISTRATION, outputFile );

                  // store the drizzle file if present
                  let drizzleFilePath = File.changeExtension( outputFile, ".xdrz" );
                  if ( File.exists( drizzleFilePath ) )
                     activeFrames[ c ].addDrizzleFile( drizzleFilePath );

                  // cache the input and output file data if not already cached
                  if ( !cachedFile )
                  {
                     SACache[ inputFile ] = outputFile;
                     engine.executionCache.cacheFileLMD( SAcacheKey, inputFile );
                     engine.executionCache.cacheFileLMD( SAcacheKey, outputFile );
                     engine.executionCache.cacheFileLMD( SAcacheKey, drizzleFilePath );
                  }

                  if ( cachedFile )
                     nCached++;
                  else
                     nRegistered++;
               }
               else
               {
                  errorMsg = ": Registered frame not found " + outputFile;
                  engine.processLogger.addWarning( "File does not exist after image registration: " + outputFile );
                  activeFrames[ c ].processingFailed();
                  nFailed++;
               }
            }
            else
            {
               errorMsg = ": output file name is an empty string";
               engine.processLogger.addWarning( "Registration failed for image: " + inputFile );
               activeFrames[ c ].processingFailed();
               nFailed++;
            }
         }
         else
         {
            errorMsg = ": Empty output file name";
            engine.processLogger.addWarning( "Registration failed for image: " + inputFile );
            activeFrames[ c ].processingFailed();
            nFailed++;
         }

         if ( success )
            console.writeln( "Registered frame " + c + ": " + inputFile + " ---> " + outputFile );
         else
            console.warningln( "Registered frame " + c + ": " + inputFile + " ---> [ FAILED" + errorMsg + " ]" );
      }

      // update the cache for this Star Alignment configuration
      engine.executionCache.setCache( SAcacheKey, SACache );

      let registeredFrames = frameGroup.activeFrames();
      if ( registeredFrames.length < 1 )
      {
         console.warningln( "** Warning: No light frames found after registration." );
         engine.processLogger.addError( "No light frames found after registration." );
         this.statusMessage = "no light frames found after registration";
         return OperationBlockStatus.FAILED;
      }

      engine.processLogger.addSuccess( "Registration completed", registeredFrames.length + " images out of " + activeFrames.length + " successfully registered." );

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* End registration of light frames" );
      console.noteln( SEPARATOR );
      console.flush();

      this.statusMessage = resultCountToString( nCached, nRegistered, nFailed, "registered" );
      this.hasWarnings = nFailed > 0;

      return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.RegistrationOperation.prototype = new WBPPOperationBlock;

StackEngine.prototype.LocalNormalizationReferenceFrameSelectionOperation = function( frameGroup )
{
   this.__base__ = WBPPOperationBlock;
   let title = "";
   let trackable = false;
   if ( engine.localNormalizationInteractiveMode )
   {
      title = "LN reference [interactive]";
      trackable = true;
   }
   else if ( engine.localNormalizationRerenceFrameGenerationMethod == WBPPLocalNormalizationReferenceFrameMethod.INTEGRATION_BEST_FRAMES )
   {
      title = "LN reference generation";
      trackable = true;
   }
   else
   {
      trackable = false;
   }
   this.__base__( title, frameGroup, trackable );

   this.spaceRequired = () =>
   {
      return frameGroup.frameSize();
   };

   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => ( frameGroup.injectGroupData(
   {
      name: title,
      status: this.status,
      statusMessage: this.statusMessage
   } ) );

   this._run = function( environment )
   {
      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Local normalization reference frame selection" );
      console.noteln( SEPARATOR );
      frameGroup.log();
      console.flush();

      let activeFrames = frameGroup.activeFrames();

      if ( activeFrames.length == 0 )
      {
         console.warningln( "** Warning: No active light frames to be locally normalized." );
         engine.processLogger.addError( "No active light frames to be locally normalized." );
         this.statusMessage = "no active frames";
         return OperationBlockStatus.FAILED;
      }

      if ( engine.localNormalizationInteractiveMode )
      {
         console.noteln( "-" );
         console.noteln( "STARTING Local Normalization Interactive Session" );
         console.noteln( "-" );
         engine.operationQueue.hideExecutionMonitorDialog();
         let LNReferenceFrameSelectionWindow = new WBPPLocalNormalizationReferenceSelector( frameGroup );
         LNReferenceFrameSelectionWindow.execute();
         console.noteln( "-" );
         console.noteln( "Local Normalization Interactive Session TERMINATED" );
         console.noteln( "-" );
         let lnReferenceFrame = LNReferenceFrameSelectionWindow.referenceFrame;


         if ( lnReferenceFrame == undefined )
         {
            console.warningln( "** Warning: Unable to find the reference frame generated during the interactive mode." );
            engine.processLogger.addError( "Unable to find the reference frame generated during the interactive mode." );
            this.statusMessage = "no reference frame defined";
            return OperationBlockStatus.FAILED;
         }

         console.noteln( "<end><cbr><br>", SEPARATOR );
         console.noteln( "* End selection of local normalization reference frame" );
         console.noteln( SEPARATOR );
         console.flush();

         // inject the reference frame in the environment
         environment[ frameGroup.id + "_LNReferenceFrame" ] = lnReferenceFrame;
         return OperationBlockStatus.DONE;
      }
      else
      {
         // select the reference frame
         let
         {
            lnReferenceFilePath,
            cached
         } = engine.generateLNReference( frameGroup );

         if ( lnReferenceFilePath == undefined )
         {
            console.warningln( "** Warning: Unable to determine the local normalization reference frame. " +
               "Local normalization will be skipped for this group." );
            engine.processLogger.addError( "Unable to determine the local normalization reference frame. " +
               "Local normalization will be skipped for this group." );
            this.statusMessage = "unable to find a local normalization reference frame";
            return OperationBlockStatus.FAILED;
         }

         console.noteln( "<end><cbr><br>", SEPARATOR );
         console.noteln( "* End selection of local normalization reference frame" );
         console.noteln( SEPARATOR );
         console.flush();

         // inject the reference frame in the environment
         environment[ frameGroup.id + "_LNReferenceFrame" ] = lnReferenceFilePath;
         if ( cached )
            this.statusMessage = "cached";
         return OperationBlockStatus.DONE;
      }
   };
};
StackEngine.prototype.LocalNormalizationReferenceFrameSelectionOperation.prototype = new WBPPOperationBlock;

StackEngine.prototype.LocalNormalizationOperation = function( frameGroup )
{
   this.__base__ = WBPPOperationBlock;
   this.__base__( "Local Normalization", frameGroup, true /* trackable */ );

   this.spaceRequired = () =>
   {
      if ( engine.localNormalizationGenerateImages )
      {
         return frameGroup.groupSize();
      }
      else
      {
         return 0;
      }
   };

   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => ( frameGroup.injectGroupData(
   {
      name: "Local Normalization",
      status: this.status,
      statusMessage: this.statusMessage
   } ) );

   let
   {
      enableTargetFrames,
      getImageSize,
      resultCountToString
   } = WBPPUtils.shared();

   this._run = function( environment )
   {
      let failed = false;

      let activeFrames = frameGroup.activeFrames();

      if ( activeFrames.length == 0 )
      {
         console.warningln( "** Warning: No active light frames to be locally normalized." );
         engine.processLogger.addError( "No active light frames to be locally normalized." );
         this.statusMessage = "no active frames";
         return OperationBlockStatus.FAILED;
      }

      // select the reference frame
      let lnReferenceFrame = environment[ frameGroup.id + "_LNReferenceFrame" ];

      if ( lnReferenceFrame == undefined )
      {
         console.warningln( "** Warning: Unable to determine the local normalization reference frame. " +
            "Local normalization will be skipped for this group." );
         engine.processLogger.addError( "Unable to determine the local normalization reference frame. " +
            "Local normalization will be skipped for this group." );
         this.statusMessage = "unable to find a local normalization reference frame";
         return OperationBlockStatus.FAILED;
      }

      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* Begin local normalization of light frames" );
      console.noteln( SEPARATOR );
      frameGroup.log();
      console.flush();

      console.noteln( "* Local normalization reference image: " + lnReferenceFrame );
      engine.processLogger.addSuccess( "Local normalization reference image: ", lnReferenceFrame );

      let referenceImageSize = getImageSize( lnReferenceFrame );
      let imageRefrenceDimension = Math.max( referenceImageSize.width, referenceImageSize.height );

      let LN = new LocalNormalization;
      LN.overwriteExistingFiles = true;
      let filePaths = activeFrames.map( item => item.current );
      LN.referencePathOrViewId = lnReferenceFrame;
      LN.referenceIsView = false;
      LN.scale = imageRefrenceDimension / engine.localNormalizationGridSize;
      LN.referenceRejectionThreshold = 3.00;
      LN.targetRejectionThreshold = 3.20;
      LN.psfMaxStars = engine.localNormalizationPsfMaxStars;
      LN.psfMinSNR = engine.localNormalizationPsfMinSNR;
      LN.psfAllowClusteredSources = engine.localNormalizationPsfAllowClusteredSources;
      LN.lowClippingLevel = engine.localNormalizationLowClippingLevel;
      LN.highClippingLevel = engine.localNormalizationHighClippingLevel;
      LN.scaleEvaluationMethod = ( engine.localNormalizationMethod == 0 ) ?
         LocalNormalization.prototype.ScaleEvaluationMethod_PSFSignal :
         LocalNormalization.prototype.ScaleEvaluationMethod_MultiscaleAnalysis;
      LN.psfType = [
         LocalNormalization.prototype.PSFType_Gaussian,
         LocalNormalization.prototype.PSFType_Moffat15,
         LocalNormalization.prototype.PSFType_Moffat4,
         LocalNormalization.prototype.PSFType_Moffat6,
         LocalNormalization.prototype.PSFType_Moffat8,
         LocalNormalization.prototype.PSFType_MoffatA,
         LocalNormalization.prototype.PSFType_Auto
      ][ engine.localNormalizationPsfType ];
      LN.psfGrowth = engine.localNormalizationPsfGrowth;

      if ( engine.localNormalizationGenerateImages )
         LN.generateNormalizedImages = LocalNormalization.prototype.GenerateNormalizedImages_Always;

      // Since core version 1.8.9-1, LocalNormalization can generate .xnml
      // tagged as invalid when relative scale factor evaluation fails. These
      // special files are recognized by ImageIntegration and the corresponding
      // images are excluded from the integration. This allows us to be
      // tolerant of normalization errors.
      LN.generateInvalidData = true;

      let LNSource = LN.toSource( "JavaScript", "LN" /*varId*/ , 0 /*indent*/ ,
         SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();

      // Check if valid cached data is present
      let filesToNormalize = filePaths;
      let cachedCount = 0;
      let cached = {};
      let LNCache = {};
      let LNcacheKey = engine.executionCache.keyFor( LNSource );
      if ( engine.executionCache.hasCacheForKey( LNcacheKey ) )
      {
         console.noteln( "Local Normalization has cached data for key ", LNcacheKey );

         if ( engine.executionCache.isFileUnmodified( LNcacheKey, LN.referencePathOrViewId ) )
         {
            // valid cache found. The cache is a map between the input file and the output LN file
            console.noteln( "Local Normalization has cached data for key ", LNcacheKey );

            LNCache = engine.executionCache.cacheForKey( LNcacheKey );
            filesToNormalize = [];

            for ( let i = 0; i < filePaths.length; i++ )
            {
               let inputFile = filePaths[ i ];
               let lnFile = LNCache[ inputFile ];
               if ( lnFile != undefined &&
                  engine.executionCache.isFileUnmodified( LNcacheKey, inputFile ) &&
                  engine.executionCache.isFileUnmodified( LNcacheKey, lnFile ) )
               {
                  cached[ inputFile ] = lnFile;
                  cachedCount++;
                  console.noteln( "Local Normalization cache for input found: ", inputFile, " --> ", lnFile );
               }
               else
               {
                  filesToNormalize.push( inputFile );
                  console.noteln( "Local Normalization input file will be normalized: ", inputFile );
               }
            }
         }
      }
      else
      {
         console.noteln( "Local Normalization has no cached data for key ", LNcacheKey );
      }

      // write the process into the console
      console.writeln( SEPARATOR2 );
      console.writeln( LNSource );
      console.writeln( SEPARATOR2 );

      // process is saved in container with the full list of files to be normalized
      LN.targetItems = enableTargetFrames( filePaths, 2 );
      engine.processContainer.add( LN );

      // perform LN if there are files to normalize
      let success = true;
      if ( filesToNormalize.length > 0 )
      {
         LN.targetItems = enableTargetFrames( filesToNormalize, 2 );
         success = LN.executeGlobal();
         engine.executionCache.cacheFileLMD( LNcacheKey, LN.referencePathOrViewId );
      }

      let nCached = 0;
      let nSuccess = 0;
      let nFailed = 0;

      if ( !success && cachedCount == 0 )
      {
         console.warningln( "** Warning: Error applying local normalization to light frames. This group will be skipped." );
         engine.processLogger.addError( "Error applying local normalization to light frames. This group will be skipped." );
         failed = true;
      }
      else if ( filesToNormalize.length > 0 && cachedCount == 0 && ( !LN.outputData || LN.outputData.length != filesToNormalize.length ) )
      {
         // skip local normalization
         console.warningln( "** Warning: Local normalization issue occurred. Local normalization will not be applied." );
         engine.processLogger.addWarning( "Local normalization issue occurred. Local normalization will not be applied" );
         failed = true;
      }
      else
      {
         let lnFiles = [];
         let j = 0;
         for ( let i = 0; i < filePaths.length; i++ )
         {
            let inputFile = filePaths[ i ];
            if ( cached[ inputFile ] != undefined )
            {
               lnFiles.push(
               {
                  path: cached[ inputFile ],
                  valid: true,
                  cached: true
               } );
            }
            else
            {
               let lnFile = LN.outputData[ j++ ];
               lnFiles.push(
               {
                  path: lnFile[ 0 ] || "",
                  valid: lnFile[ 5 ] || false,
                  cached: false
               } );
            }
         }

         // ensure that valid LN files have been created for each file
         for ( let k = 0; k < lnFiles.length; ++k )
         {
            if ( lnFiles[ k ].path.length == 0 )
            {
               console.warningln( "** Warning: Local normalization generation failed for file ", activeFrames[ k ].current );
               engine.processLogger.addWarning( "Local normalization failed for file " + activeFrames[ k ].current );
               activeFrames[ k ].processingFailed();
               nFailed++;
            }
            else if ( !File.exists( lnFiles[ k ].path ) )
            {
               console.warningln( "** Warning: Local normalization data file not found for file ", activeFrames[ k ].current );
               engine.processLogger.addWarning( "Local normalization data file not found for file " + activeFrames[ k ].current );
               activeFrames[ k ].processingFailed();
               nFailed++;
            }
            else
            {
               if ( !lnFiles[ k ].valid )
               {
                  console.warningln( "** Warning: Invalid local normalization data generated for file ", activeFrames[ k ].current );
                  engine.processLogger.addWarning( "Invalid local normalization data generated for file " + activeFrames[ k ].current );
                  activeFrames[ k ].processingFailed();
                  nFailed++;
               }
               else
               {
                  // valid LN files are cached
                  LNCache[ filePaths[ k ] ] = lnFiles[ k ].path;
                  engine.executionCache.cacheFileLMD( LNcacheKey, filePaths[ k ] );
                  engine.executionCache.cacheFileLMD( LNcacheKey, lnFiles[ k ].path );
                  activeFrames[ k ].addLocalNormalizationFile( lnFiles[ k ].path );
                  if ( lnFiles[ k ].cached )
                     nCached++;
                  else
                     nSuccess++;
               }
            }
         }
         // save the updated cache
         engine.executionCache.setCache( LNcacheKey, LNCache );

      }

      engine.processLogger.addSuccess( "Local normalization", "completed." );
      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* End local normalization of light frames" );
      console.noteln( SEPARATOR );
      console.flush();

      this.statusMessage = resultCountToString( nCached, nSuccess, nFailed, "completed" );
      this.hasWarnings = nFailed > 0;
      if ( failed )
         return OperationBlockStatus.FAILED;
      else
         return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.LocalNormalizationOperation.prototype = new WBPPOperationBlock;

StackEngine.prototype.ImageIntegrationOperation = function( frameGroup )
{
   this.__base__ = WBPPOperationBlock;
   this.__base__( "Integration", frameGroup, true /* trackable */ );

   this.spaceRequired = () =>
   {
      return frameGroup.frameSize();
   };

   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => ( frameGroup.injectGroupData(
   {
      name: "Integration",
      status: this.status,
      statusMessage: this.statusMessage
   } ) );

   let
   {
      isEmptyString
   } = WBPPUtils.shared();

   this._run = function()
   {
      let activeFrames = frameGroup.activeFrames();

      if ( activeFrames.length == 0 )
      {
         console.warningln( "** Warning: No active frames to be integrated." );
         engine.processLogger.addError( "No active frames to be integrated." );
         this.statusMessage = "no active frames";
         return OperationBlockStatus.CANCELED;
      }

      let groupType = StackEngine.imageTypeToString( frameGroup.imageType );

      // do integrate
      // overscan-specific intervention: when we integrate a master flat and the overscan is enabled we
      // store two custon keywords to remind the original frame sizes, this will help the matching and the addition
      // of this special cropped master
      let keywords = [];
      if ( frameGroup.imageType == ImageType.FLAT && engine.overscan.enabled )
      {
         keywords.push( new FITSKeyword( "PREOVSCW", format( "%d", frameGroup.size.width ), "Width of the original flat frames before overscan" ) );
         keywords.push( new FITSKeyword( "PREOVSCH", format( "%d", frameGroup.size.height ), "Height of the original flat frames before overscan" ) );
      }
      let
      {
         masterFilePath,
         cached
      } = engine.doIntegrate(
         frameGroup,
         undefined /* custom prefix */ ,
         undefined /* custom postfix */ ,
         undefined /* customGenerateRejectionMaps */ ,
         undefined /* customGenerateDrizzle */ ,
         undefined /* desiredFileName */ ,
         undefined /* overrideIIparameters */ ,
         keywords
      );

      // check the result
      if ( isEmptyString( masterFilePath ) || !File.exists( masterFilePath ) )
      {
         console.warningln( "** Warning: Master " + groupType + " file was not generated." );
         engine.processLogger.addError( "Warning: Master " + groupType + " file was not generated." );
         this.statusMessage = "master file not generated";
         return OperationBlockStatus.FAILED;
      }

      if ( frameGroup.imageType != ImageType.LIGHT )
      {
         // add the created master file
         console.writeln( "add the master file at path: ", masterFilePath );
         engine.addFile( masterFilePath );
      }
      else
      {
         // store the master file associated to the group ID into the environment for further processing
         // this info is used, for example, by channel recombination
         console.writeln( "set the group's master file at path: ", masterFilePath );
         frameGroup.setMasterFileName( masterFilePath );
      }

      engine.processLogger.addSuccess( "Integration completed", "master " + groupType + " saved at path " + masterFilePath );
      this.statusMessage = cached ? "cached" : "";
      return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.ImageIntegrationOperation.prototype = new WBPPOperationBlock;

StackEngine.prototype.DrizzleIntegrationOperation = function( frameGroup )
{
   this.__base__ = WBPPOperationBlock;
   this.__base__( "Drizzle Integration (" + frameGroup.drizzleScale() + "x)", frameGroup, true /* trackable */ );

   this.spaceRequired = () =>
   {
      return frameGroup.frameSize() * frameGroup.drizzleScale() * frameGroup.drizzleScale();
   };

   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => ( frameGroup.injectGroupData(
   {
      name: "Drizzle Integration",
      status: this.status,
      statusMessage: this.statusMessage
   } ) );

   let
   {
      isEmptyString
   } = WBPPUtils.shared();

   this._run = function( environment )
   {
      let activeFrames = frameGroup.activeFrames();

      if ( activeFrames.length == 0 )
      {
         console.warningln( "** Warning: No active frames, drizzle integration skipped." );
         engine.processLogger.addError( "No active frames, drizzle integration skipped." );
         this.statusMessage = "no active frames";
         return OperationBlockStatus.CANCELED;
      }
      else
      {
         // we proceed with drizzle integration only if image integration has succeeded
         let masterFileName = frameGroup.getMasterFileName();
         if ( masterFileName == undefined || !File.exists( masterFileName ) )
         {
            console.warningln( "** Warning: No active master light has been generated. Drizzle integration is skipped." );
            engine.processLogger.addError( "No active master light has been generated. Drizzle integration is skipped." );
            this.statusMessage = "no master light found";
            return OperationBlockStatus.CANCELED;
         }
      }

      let groupType = StackEngine.imageTypeToString( frameGroup.imageType );

      // apply drizzle integration

      let
      {
         masterFilePath,
         cached
      } = engine.doDrizzleIntegration(
         frameGroup,
         frameGroup.drizzleScale() /* scale */ ,
         frameGroup.drizzleDropShrink() /* shrink */ ,
         frameGroup.drizzleFunction() /* kernel */ ,
         undefined /* custom prefix */ ,
         "_drizzle_" + frameGroup.drizzleScale() + "x" /* custom postfix */ ,
         undefined /* desiredFileName */ ,
         undefined /* overrideDIparameters */ ,
         [] /* keywords */ ,
         true /* optimize the astrometric solution */
      );

      // check the result
      if ( isEmptyString( masterFilePath ) || !File.exists( masterFilePath ) )
      {
         console.warningln( "** Warning: Master " + groupType + " file was not generated." );
         engine.processLogger.addError( "Warning: Master " + groupType + " file was not generated." );
         this.statusMessage = "master file not generated";
         return OperationBlockStatus.FAILED;
      }

      if ( frameGroup.imageType != ImageType.LIGHT )
         // add the created master file
         engine.addFile( masterFilePath );
      else
         // store the master file associated to the group ID into the environment for further processing
         // this info is used, for example, by channel recombination
         frameGroup.setMasterFileName( masterFilePath, WBPPMasterType.DRIZZLE );

      engine.processLogger.addSuccess( "Drizzle Integration completed", "master " + groupType + " saved at path " + masterFilePath );
      this.statusMessage = cached ? "cached" : "";
      return OperationBlockStatus.DONE;
   };
};
StackEngine.prototype.DrizzleIntegrationOperation.prototype = new WBPPOperationBlock;

StackEngine.prototype.AutoCropOperation = function()
{
   this.__base__ = WBPPOperationBlock;
   this.__base__( "Autocrop", undefined, true /* trackable */ );

   let
   {
      hasAstrometricSolution,
      keywords,
      resultCountToString
   } = WBPPUtils.shared();

   this.spaceRequired = () =>
   {
      // in the average case, we expect the cropped version to have a slightly smaller size than the original file.
      // We assume 95 % as the heuristic factor.
      let groups = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST ).filter( g => !g.isHidden );
      return groups.reduce( ( a, g ) =>
      {
         let size = g.frameSize();
         if ( g.isDrizzleEnabled() )
            size += g.drizzledFrameSize();
         return a + size;
      }, 0 );
   };

   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => (
   {
      name: "Autocrop",
      status: this.status,
      statusMessage: this.statusMessage
   } );

   this._run = function( environment )
   {

      let totalToCrop = 0;
      let nSuccess = 0;
      let nCached = 0;
      let nFailed = 0;

      let allPostGroups = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST ).filter( g => g.isActive );
      console.writeln( "Autocrop: active groups to be processed = ", allPostGroups.length );

      // we collect groups that shares the same reference frame. For each reference frame, all
      // groups will be cropped on the same region
      let refGroupsMap = allPostGroups.reduce( ( a, g ) =>
      {
         // we ignore the recombined rgb channels group
         if ( g.__reference_frame__ != undefined )
         {
            if ( g.associatedRGBchannel != WBPPAssociatedChannel.COMBINED_RGB )
            {
               if ( a[ g.__reference_frame__ ] == undefined )
                  a[ g.__reference_frame__ ] = [];
               a[ g.__reference_frame__ ].push( g );
            }
         }
         else
         {
            console.writeln( "Autocrop: group has no reference frame ", g.toShortString() );
         }
         return a;
      },
      {} );
      let refFrames = Object.keys( refGroupsMap );
      console.writeln( "Autocrop: reference frames = ", refFrames.length );

      // the categories of master files to be cropped are
      // 1. ImageIntegration master files
      // 2. DrizzleIntegration master files
      // for each category a list of cropping groups are crated, in each cropping group all masters
      // shares the same reference frame
      let croppingGroups = [];

      // add the image integration masters files. We pick the master files names from the environment as they have to be
      // created by the execution of ImageIntegration first in the pipeline.
      // If ImageIntegration did fail, the group may remain active but no master file name is stored into the environment,
      // in that case we need to filter out these groups.
      [
      {
         desc: "image integration master light",
         isDrizzle: false
      },
      {
         desc: "drizzle master light",
         isDrizzle: true
      } ].forEach( item =>
      {
         refFrames.forEach( referenceFrame =>
         {
            let groups = refGroupsMap[ referenceFrame ];
            let filteredGroups = [];

            // we first filter the groups that has a valid master file name assocaited for both
            // master lights and drizzled master lights.
            groups.forEach( g =>
            {
               if ( !item.isDrizzle ||
                  ( item.isDrizzle && g.isDrizzleEnabled() ) )
               {
                  if ( g.getMasterFileName( item.isDrizzle ? WBPPMasterType.DRIZZLE : WBPPMasterType.MASTER_LIGHT ) != undefined )
                     filteredGroups.push( g );
                  else
                     console.warningln( "Autocrop: ", item.desc, " not available for group ", g.toShortString() );
               }
            } );

            if ( filteredGroups.length > 0 )
            {

               let type = item.isDrizzle ? WBPPMasterType.DRIZZLE : WBPPMasterType.MASTER_LIGHT;
               let filePaths = filteredGroups.map( g => ( g.getMasterFileName( type ) ) );
               totalToCrop += filteredGroups.length;
               croppingGroups.push(
               {
                  groups: filteredGroups,
                  filePaths: filePaths,
                  referenceFrame: referenceFrame,
                  isDrizzle: item.isDrizzle
               } );
            }
         } );
      } );
      croppingGroups.filter( cg => cg.groups.length > 0 );

      // log the crop groups
      croppingGroups.forEach( ( cg ) =>
      {
         console.noteln( "Autocrop: masters group for reference frame ", File.extractNameAndExtension( cg.referenceFrame ) );
         cg.filePaths.forEach( ( f, j ) =>
         {
            console.noteln( "Autocrop:     ", j, ". ", File.extractNameAndExtension( f ) );
         } );
      } );

      console.writeln( "Autocrop: active crop groups count is ", croppingGroups.length );

      // loop through all aggregated groups and
      //    determine the common crop region
      //    crop all frames in the same group using the common crop region
      for ( let i = 0; i < croppingGroups.length; i++ )
      {
         let groups = croppingGroups[ i ].groups;
         let filePaths = croppingGroups[ i ].filePaths;
         let referenceFrame = croppingGroups[ i ].referenceFrame;
         let isDrizzle = croppingGroups[ i ].isDrizzle;

         console.writeln();
         console.writeln( "Autocrop: ---------------------------------------------" );
         console.writeln( "Autocrop: crop group ", i );
         console.writeln( "Autocrop: crop the following masters on reference frame ", referenceFrame );
         filePaths.forEach( f =>
         {
            console.writeln( "Autocrop: ", f );
         } )
         console.writeln( "Autocrop: ---------------------------------------------" );
         console.writeln();

         // we check if the reference frame has already been cropped, if this is the case then
         // no crop needs to be applied.
         // This is the case where we selected an already cropped frame as reference frame; when this
         // happens the aligned frame already has the same frame size of the reference frame.
         let isAutocrop = keywords.readFileKeyword( referenceFrame, AUTOCROP_KEYWORD );
         if ( isAutocrop )
         {
            nSuccess == groups.length;
            console.writeln( "Autocrop: reference frame has already been cropped by WBPP, skip the cropping" );
            engine.processLogger.addMessage( "reference frame " + referenceFrame + " has already been cropped by WBPP, skip cropping " + filePaths.length + " master" + ( filePaths.length == 1 ? "" : "s" ) + " using it as the reference." );
            continue;
         }

         // check if cache exists
         // the cache key is generated from the concatenated sorted list of masters to be cropped
         let ACcacheKey = engine.executionCache.keyFor( filePaths.join( "|" ) );
         let ACCache = {
            previouslyFailed: false,
            cropRects:
            {}
         };
         let inputDataIsUnchanged = true;
         let outputDataIsUnchanged = true;

         // cache handling
         // we skip processing the whole grop if
         // - input files are unchanged
         // - the previous execution failed, so we already know that re-executing the autocrop will fail again
         // - the previous execution did not fail and the output files are unchanged
         if ( engine.executionCache.hasCacheForKey( ACcacheKey ) )
         {
            ACCache = engine.executionCache.cacheForKey( ACcacheKey );
            if ( ACCache.cropRects == undefined )
            {
               console.noteln( "Autocrop: missing crop rectangles cached data. Reprocess the master files." );
               ACCache.cropRects = {};
            }
            else
            {
               console.noteln( "Autocrop: has cached data for the key ", ACcacheKey );
               console.noteln( "Autocrop: check input files " );
               // all input files must be unchanged
               for ( let j = 0; inputDataIsUnchanged && j < filePaths.length; j++ )
               {
                  inputDataIsUnchanged = engine.executionCache.isFileUnmodified( ACcacheKey, filePaths[ j ] );
                  if ( !inputDataIsUnchanged )
                  {
                     console.noteln( "Autocrop: file has changed since last autocrop execution, ", filePaths[ j ] );
                     break;
                  }
                  else
                     console.noteln( "Autocrop: file is unmodified since last autocrop execution, ", filePaths[ j ] );
               }

               if ( inputDataIsUnchanged && ACCache.previouslyFailed )
               {
                  console.noteln( "Autocrop: input data is unchanged but the previous execution failed. Autocrop will be skipped." );
                  nFailed += groups.length;
                  continue;
               }
               else if ( inputDataIsUnchanged && !ACCache.previouslyFailed )
               {
                  console.noteln( "Autocrop: check output files " );
                  for ( let j = 0; outputDataIsUnchanged && j < filePaths.length; j++ )
                  {
                     let croppedFilePath = File.appendToName( filePaths[ j ], "_autocrop" );
                     outputDataIsUnchanged = engine.executionCache.isFileUnmodified( ACcacheKey, croppedFilePath );
                     if ( !outputDataIsUnchanged )
                     {
                        console.noteln( "Autocrop: file has changed since last autocrop execution, ", filePaths[ j ] );
                        break;
                     }
                     else
                        console.noteln( "Autocrop: file is unmodified since last autocrop execution, ", filePaths[ j ] );
                  }
               }

               // if data is unchanged if input files are unchanged and the previous execution failed or did not failed and output files are unchanged
               if ( inputDataIsUnchanged && outputDataIsUnchanged )
               {
                  // we have cached data with unchanged inputs and outputs that succeded in the previous execution.
                  // skip any opration and inject into the environment the crop rects to feed the drizzle integration in case.
                  let validCache = true;
                  let groupIDs = Object.keys( ACCache.cropRects );
                  groupIDs.forEach( key =>
                  {
                     let rect = ACCache.cropRects[ key ];
                     if ( rect != undefined )
                        try
                        {
                           environment[ key ] = new Rect( rect.x0, rect.y0, rect.x1, rect.y1 );
                        }
                     catch ( e )
                     {
                        validCache = false;
                     }
                     else
                        validCache = false;
                  } );
                  if ( !validCache )
                  {
                     console.noteln( "Autocrop: invalid crop rectangles cached data. Reprocess the master files." );
                  }
                  else
                  {
                     // set the autocrop master file into the groups
                     for ( let j = 0; j < groups.length; j++ )
                     {
                        let croppedFilePath = File.appendToName( filePaths[ j ], "_autocrop" );
                        groups[ j ].setMasterFileName( croppedFilePath, isDrizzle ? WBPPMasterType.DRIZZLE : WBPPMasterType.MASTER_LIGHT, WBPPMasterVariant.CROPPED );
                     }
                     console.noteln( "Autocrop: successfully cached cropped file found. Autocrop will be skipped." );
                     nCached += groups.length;
                     continue;
                  }
               }
            }
         }

         // detect the crop region for all masters first, then the unique crop
         // region applied to all of them will be the intersection of all individual crop regions
         let cropRectangles = [];
         for ( let j = 0; j < groups.length; j++ )
         {
            let masterFilePath = croppingGroups[ i ].filePaths[ j ];

            // cache the LMD for all input filepaths
            engine.executionCache.cacheFileLMD( ACcacheKey, masterFilePath );
            let result = {
               success: false
            };

            let group = groups[ j ];
            console.writeln( "Autocrop: process master ", masterFilePath );
            if ( isDrizzle )
            {
               // each group that is drizzled must have been processed once already to crop
               // the master light, so the crop region is available in the environment
               let cropRect = environment[ "crop_region_" + group.id ];
               if ( cropRect != undefined )
               {
                  let scale = group.drizzleScale();
                  cropRect.x0 = cropRect.x0 * scale;
                  cropRect.y0 = cropRect.y0 * scale;
                  cropRect.x1 = cropRect.x1 * scale;
                  cropRect.y1 = cropRect.y1 * scale;
                  result = {
                     success: true,
                     rect: cropRect
                  };
               }
               else
               {
                  console.warningln( "Autocrop: crop region not found for drizzle master ", group.toString() );
               }
            }
            else
            {
               // open the master light and compute the crop region
               result = engine.getAutocropRegion( masterFilePath, true /* returnTheWorkingImages */ );
               if ( !result.success )
               {
                  console.warningln( result.message );
                  engine.processLogger.addError( result.message );
                  console.warningln( "Autocrop: failed to determine the crop region for the file ", masterFilePath );
               }
               else
               {
                  let id = "crop_region_" + group.id;
                  environment[ id ] = result.rect;
                  ACCache.cropRects[ id ] = {
                     x0: result.rect.x0,
                     x1: result.rect.x1,
                     y0: result.rect.y0,
                     y1: result.rect.y1
                  };
               }
            }
            cropRectangles.push( result );
         }

         // report the result
         for ( let j = 0; j < groups.length; j++ )
         {
            let masterFilePath = filePaths[ j ];
            if ( cropRectangles[ j ].success )
            {
               let cropRect = cropRectangles[ j ].rect;
               let rectString = "(" + cropRect.x0 + "," + cropRect.y0 + "), (" + cropRect.x1 + "," + cropRect.y1 + ")";
               console.noteln( "Autocrop: crop region ", rectString, " successflly computed for the file ", masterFilePath );
            }
            else
            {
               console.warningln( "Autocrop: failed to compute crop region for the file ", masterFilePath );
            }
         }

         // -------------------------------------------------
         // ensure that at least one crop region is available
         let oneOreMoreCropRegionsAvailable = false;
         for ( let j = 0; j < cropRectangles.length; j++ )
            if ( cropRectangles[ j ].success )
            {
               oneOreMoreCropRegionsAvailable = true;
               break;
            }

         if ( !oneOreMoreCropRegionsAvailable )
         {
            nFailed += groups.length;
            // update the cache
            ACCache.previouslyFailed = true;
            engine.executionCache.setCache( ACcacheKey, ACCache );
            // log
            let msg = "No autocrop regions defined. Autocrop will be skipped for master frames " +
               "aligned on reference frame " + referenceFrame;
            console.warningln( "Autocrop: " + msg );
            engine.processLogger.addError( msg );
            continue;
         }

         // -------------------------------------------------
         // the final crop recion is computed as the intersection of all crop regions
         let cropRect = new Rect( -1, -1, 1e6, 1e6 );
         for ( let j = 0; j < cropRectangles.length; j++ )
         {
            if ( !cropRectangles[ j ].success )
               continue;
            let c = cropRectangles[ j ].rect;
            cropRect.intersect( new Rect( c.x0, c.y0, c.x1, c.y1 ) );
         }
         let rectString = "(" + cropRect.x0 + "," + cropRect.y0 + "), (" + cropRect.x1 + "," + cropRect.y1 + ")";
         console.noteln( "Autocrop: the applied crop region is ", rectString );

         // check the intersection result
         if ( cropRect.width == 0 || cropRect.height == 0 )
         {
            nFailed += groups.length;
            // update the cache
            ACCache.previouslyFailed = true;
            engine.executionCache.setCache( ACcacheKey, ACCache );
            // log
            console.warningln( "Autocrop: crop region has null size " + cropRect.width + "x" + cropRect.height + ", crop will be skipped." );
            continue;
         }

         // -------------------------------------------------
         // now we need to apply the crop to all master frames
         for ( let j = 0; j < groups.length; j++ )
         {
            let masterFilePath = filePaths[ j ];
            let croppedFilePath = File.appendToName( masterFilePath, "_autocrop" );

            console.writeln( "Autocrop: cropping file at ", masterFilePath );

            let windows = ImageWindow.open( masterFilePath );
            if ( windows == undefined || windows.length == 0 )
            {
               console.warningln( "Autocrop: failed to open the master file ", masterFilePath );
               nFailed++;
               continue;
            }
            let window = windows[ 0 ];
            // close the rejection maps
            let k = 1;
            while ( k < windows.length )
            {
               windows[ k ].forceClose();
               k++;
            }
            windows = [ window ];

            console.writeln( "Autocrop: generating the crop region image" );
            // generate the cropping mask
            let pm = new PixelMath;
            pm.expression = "iif(" +
               "(x()==" + cropRect.x0 + " && y()>=" + cropRect.y0 + " && y()<=" + cropRect.y1 + ") || " +
               "(x()==" + cropRect.x1 + " && y()>=" + cropRect.y0 + " && y()<=" + cropRect.y1 + ") || " +
               "(y()==" + cropRect.y0 + " && x()>=" + cropRect.x0 + " && x()<=" + cropRect.x1 + ") || " +
               "(y()==" + cropRect.y1 + " && x()>=" + cropRect.x0 + " && x()<=" + cropRect.x1 + ")" +
               ",1,0)";
            pm.createNewImage = true;
            pm.newImageId = "crop_mask";
            pm.newImageColorSpace = PixelMath.prototype.Gray;
            pm.executeOn( window.mainView );
            let maskImage = ImageWindow.windowById( "crop_mask" );
            maskImage.hide();
            // get the cropped center's RA DEC if available
            let hasAS = hasAstrometricSolution( window );
            let cRA;
            let cDEC;
            if ( hasAS )
            {
               let celestial = window.imageToCelestial( cropRect.center );
               cRA = celestial.x;
               cDEC = celestial.y;
            }
            // crop the image
            window.mainView.beginProcess();
            window.mainView.image.cropTo( cropRect );
            window.mainView.endProcess();

            // append the crop mask to the set of images stored into the xisf file
            windows.push( maskImage );

            // now we have to refine the astrometric solution if needed
            if ( hasAstrometricSolution( window ) && engine.platesolve )
            {
               console.writeln( "Autocrop: adjusting the astrometric solution after being cropped" );
               if ( !engine.solveImage(
                     window,
                     false /* do not optimize at first since we want to entirely recompute the solution */ ,
                     {
                        ra: cRA /* use the cropped ra center coordinates */ ,
                        dec: cDEC /* use the cropped dec center coordinates */
                     } ) )
               {
                  console.warningln( "Autocrop: failed to adjust the astrometric solution after being cropped. Astrometric solution is removed" );
                  window.clearAstrometricSolution();
               }
            }

            window.keywords = window.keywords.concat(
               new FITSKeyword(
                  AUTOCROP_KEYWORD,
                  format( "(%d,%d)x(%d,%d)", cropRect.x0, cropRect.y0, cropRect.x1, cropRect.y1 ),
                  "WBPP Autocrop" )
            );

            engine.writeImage(
               croppedFilePath,
               windows,
               [ "integration_autocrop",
                  "crop_mask"
               ] );

            // store the autocrop file name into the environment
            groups[ j ].setMasterFileName( croppedFilePath, isDrizzle ? WBPPMasterType.DRIZZLE : WBPPMasterType.MASTER_LIGHT, WBPPMasterVariant.CROPPED );

            windows.forEach( win =>
            {
               if ( win )
                  win.forceClose();
            } );
            console.noteln( "Autocrop: cropped master file saved at ", croppedFilePath );
            engine.executionCache.cacheFileLMD( ACcacheKey, croppedFilePath );
            nSuccess++;
         }

         // update the cache
         ACCache.previouslyFailed = false;
         engine.executionCache.setCache( ACcacheKey, ACCache );

      }

      engine.processLogger.addSuccess( "Autocrop completed", totalToCrop + " master file" + ( totalToCrop > 1 ? "s" : "" ) + " processed, " + ( nSuccess + nCached ) + " cropped." );
      this.statusMessage = resultCountToString( nCached, nSuccess, nFailed, "cropped" );
      if ( this.statusMessage == "" && nSuccess > 0 )
         this.statusMessage = nSuccess + " cropped";
      this.hasWarnings = nFailed > 0;
      if ( nFailed == totalToCrop )
         return OperationBlockStatus.FAILED;
      else
         return OperationBlockStatus.DONE;
   }
}
StackEngine.prototype.AutoCropOperation.prototype = new WBPPOperationBlock;

StackEngine.prototype.RGBRecombinationOperation = function( frameGroup )
{
   this.__base__ = WBPPOperationBlock;
   this.__base__( "RGB Combination", frameGroup, true /* trackable */ );

   let
   {
      resultCountToString
   } = WBPPUtils.shared();

   this.spaceRequired = () =>
   {
      // the group size if autocrop is not ebabled, otherwise both the uncropped and cropped files
      // will be recombined
      return frameGroup.frameSize() * ( engine.autocrop ? 2 : 1 );
   };
   /**
    * Standard group data for the event script
    *
    */
   this.envForScript = () => ( frameGroup.injectGroupData(
   {
      name: "RGB Combination",
      status: this.status,
      statusMessage: this.statusMessage
   } ) );

   this._run = function()
   {
      // get the recombined parent group, the R, G and B associated groups must have the same parent
      let parentGroupID = frameGroup.linkedGroupID;
      let singleChannelGroups = engine.getLinkedGroups( parentGroupID, frameGroup );

      if ( singleChannelGroups.length != 3 )
      {
         // something is wrong, the number of associated channels must be 3 at this point
         this.statusMessage = "Number of assocaited channels is " + singleChannelGroups.length + ", expected 3.";
         this.hasWarnings = true;
         return OperationBlockStatus.FAILED;
      }

      // check if drizzle is enabled for the first group, this means that it should be enabled for all groups and we have to
      // recombine the drizzle channels too
      let recombineDrizzle = singleChannelGroups[ 0 ].isDrizzleEnabled();
      let drizzlePostfix = "_" + singleChannelGroups[ 0 ].drizzleScale() + "x";

      // construct the list of master variants to recombined. Ignore the drizzle for separated channel since it;s currently disabled.
      let types = [
      {
         type: WBPPMasterType.MASTER_LIGHT,
         variant: WBPPMasterVariant.REGULAR,
         postfix: ""
      } ];

      if ( recombineDrizzle )
      {
         types.push(
         {
            type: WBPPMasterType.DRIZZLE,
            variant: WBPPMasterVariant.REGULAR,
            postfix: "_drizzle" + drizzlePostfix
         } );
      }

      if ( engine.autocrop )
      {
         types.push(
         {
            type: WBPPMasterType.MASTER_LIGHT,
            variant: WBPPMasterVariant.CROPPED,
            postfix: "_autocrop"
         } );
         if ( recombineDrizzle )
         {
            types.push(
            {
               type: WBPPMasterType.DRIZZLE,
               variant: WBPPMasterVariant.CROPPED,
               postfix: "_autocrop_drizzle" + drizzlePostfix
            } );
         }
      }

      let nSuccess = 0;
      let nFailed = 0;
      let nCached = 0;
      for ( let i = 0; i < types.length; i++ )
      {
         let type = types[ i ].type;
         let variant = types[ i ].variant;
         let postfix = types[ i ].postfix;

         // exctract the linkned groups ensuring that the correspondend master file is generated
         let linkedGroups = singleChannelGroups.reduce( ( acc, group ) =>
         {
            let filePath = group.getMasterFileName( type, variant );
            if ( filePath != undefined )
            {
               // store the file name that has been found in the environment associated to the group ID
               acc[ group.associatedRGBchannel ] = filePath;
            }
            return acc;
         },
         {} );

         if ( Object.keys( linkedGroups ).length < 3 )
         {
            let missingChannels = [ WBPPAssociatedChannel.R,
               WBPPAssociatedChannel.G,
               WBPPAssociatedChannel.B
            ].reduce( ( acc, ch ) =>
            {
               if ( linkedGroups[ ch ] == undefined )
                  acc.push( ch );
               return acc;
            }, [] );

            let message = "RGB combination not possible, " + postfix + " channel" +
               ( missingChannels.length > 1 ? "s " : " " ) + missingChannels.join( ", " ) + " not found.";
            console.warningln( "** Warning: " + message );
            engine.processLogger.addError( message );
            this.statusMessage = "missing " + missingChannels.join( ", " );
            nFailed++;
            continue;
         }

         // combine the channels
         let recombinedFileName = "master" + frameGroup.folderName().replace( " ", "_" ) + postfix + ".xisf";
         let
         {
            success,
            error,
            filePath,
            cached
         } = engine.combineRGB(
            linkedGroups[ WBPPAssociatedChannel.R ],
            linkedGroups[ WBPPAssociatedChannel.G ],
            linkedGroups[ WBPPAssociatedChannel.B ],
            recombinedFileName );

         // check the result
         if ( !success )
         {
            console.warningln( "** Warning: " + error );
            engine.processLogger.addError( "Warning: " + error );
            this.statusMessage = "combined RGB file not generated";
            nFailed++;
            continue;
         }

         // success
         frameGroup.setMasterFileName( filePath, type, variant );
         if ( cached )
            nCached++;
         else
            nSuccess++;

         engine.processLogger.addSuccess( "RGB Combination completed", "combined file saved at path " + filePath );
      };

      // done
      this.statusMessage = resultCountToString( nCached, nSuccess, nFailed, "recombined" );
      this.hasWarnings = nFailed > 0;
      return nCached + nSuccess > 0 ? OperationBlockStatus.DONE : OperationBlockStatus.FAILED;
   };
};
StackEngine.prototype.RGBRecombinationOperation.prototype = new WBPPOperationBlock;

/**
 * Generates the pipeline steps to process the bias frames.
 */
StackEngine.prototype.buildPipelineForBias = function()
{
   // process only pre-process groups
   let groupsPRE = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   for ( let i = 0; i < groupsPRE.length; ++i )
      if ( groupsPRE[ i ].imageType == ImageType.BIAS && !groupsPRE[ i ].hasMaster )
      {
         let logEnv = {
            processLogger: this.processLogger,
            group: groupsPRE[ i ]
         };

         // log header
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.addMessage( env.params.group.logStringHeader( 'MASTER BIAS GENERATION' ) );
         }, logEnv )
         // integration step
         let integrationOperation = new this.ImageIntegrationOperation( groupsPRE[ i ] );
         this.WS.masterBias.size += integrationOperation.spaceRequired();
         this.operationQueue.addOperation( integrationOperation );
         // log footer
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.addMessage( env.params.group.logStringFooter() );
            env.params.processLogger.newLine();
         }, logEnv )
      }
};

// ----------------------------------------------------------------------------

/**
 * Generates the pipeline steps to process the dark frames.
 */
StackEngine.prototype.buildPipelineForDark = function()
{
   // process only pre-process groups
   let groupsPRE = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   for ( let i = 0; i < groupsPRE.length; ++i )
      if ( groupsPRE[ i ].imageType == ImageType.DARK && !groupsPRE[ i ].hasMaster )
      {
         let logEnv = {
            processLogger: this.processLogger,
            group: groupsPRE[ i ]
         };

         // log header
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.addMessage( env.params.group.logStringHeader( 'MASTER DARK GENERATION' ) );
         }, logEnv );
         // integration step
         let integrationOperation = new this.ImageIntegrationOperation( groupsPRE[ i ] );
         this.WS.masterDark.size += integrationOperation.spaceRequired();
         this.operationQueue.addOperation( integrationOperation );
         // log footer
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.addMessage( env.params.group.logStringFooter() );
            env.params.processLogger.newLine();
         }, logEnv );
      }
};

// ----------------------------------------------------------------------------

/**
 * Generates the pipeline steps to process the flat frames.
 */
StackEngine.prototype.buildPipelineForFlat = function()
{

   // process only pre-process groups
   let groupsPRE = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   // flats calibration operations
   for ( let i = 0; i < groupsPRE.length; ++i )
      if ( groupsPRE[ i ].imageType == ImageType.FLAT && !groupsPRE[ i ].hasMaster )
      {
         let logEnv = {
            processLogger: this.processLogger,
            group: groupsPRE[ i ]
         };

         // log header
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.addMessage( env.params.group.logStringHeader( 'MASTER FLAT GENERATION' ) );
         }, logEnv );
         // calibration step (only if calibration masters are found)
         let cg = this.getCalibrationGroupsFor( groupsPRE[ i ] );
         if ( cg.masterBias || cg.masterDark || engine.overscan.enabled )
         {
            let calibrationOperation = new this.calibrationOperation( groupsPRE[ i ] );
            this.WS.calibrationFlat.size += calibrationOperation.spaceRequired();
            this.operationQueue.addOperation( calibrationOperation );
         }
         // integration step
         let integrationOperation = new this.ImageIntegrationOperation( groupsPRE[ i ] );
         this.WS.masterFlat.size += integrationOperation.spaceRequired();
         this.operationQueue.addOperation( integrationOperation );
         // log footer
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.addMessage( env.params.group.logStringFooter() );
            env.params.processLogger.newLine();
         }, logEnv );
      }
};

// ----------------------------------------------------------------------------

StackEngine.prototype.readableLNReferenceSelectionMethod = function( sanitized )
{
   sanitized = sanitized != undefined ? sanitized : false;
   switch ( this.localNormalizationBestReferenceSelectionMethod )
   {
      case WBPPLocalNormalizationReferenceFrameMetric.PSFSW:
         return sanitized ? "the_highest_PSF_Signal_Weight" : "the highest PSF Signal Weight";
      case WBPPLocalNormalizationReferenceFrameMetric.PSFSNR:
         return sanitized ? "the_highest_PSF_SNR" : "the highest PSF SNR"
      case WBPPLocalNormalizationReferenceFrameMetric.MSTAR:
         return sanitized ? "the_lowers_Mstar" : "the lowest M*"
      case WBPPLocalNormalizationReferenceFrameMetric.MEDIAN:
         return sanitized ? "the_lowest_median" : "the lowest median"
      case WBPPLocalNormalizationReferenceFrameMetric.STARS:
         return sanitized ? "the_highest_number_of-stars" : "the highest number of stars"
   }

   return "";
};

/**
 * Returns the best active frames or the set of best active frames to be integrated to
 * generate the local normalization reference frame accordingly
 *
 * @param {*} group
 * @return {*}
 */
StackEngine.prototype.sortFramesForLocalNormalizationReference = function( group )
{
   let activeFrames = group.activeFrames();

   // determine the measuring criteria
   let descriptorKey = "PSFSignalWeight";
   let sortByMaxVal = true;
   switch ( this.localNormalizationBestReferenceSelectionMethod )
   {
      case WBPPLocalNormalizationReferenceFrameMetric.PSFSW:
         descriptorKey = "PSFSignalWeight";
         break;
      case WBPPLocalNormalizationReferenceFrameMetric.PSFSNR:
         descriptorKey = "PSFSNR";
         break;
      case WBPPLocalNormalizationReferenceFrameMetric.MSTAR:
         descriptorKey = "Mstar";
         sortByMaxVal = false;
         break;
      case WBPPLocalNormalizationReferenceFrameMetric.MEDIAN:
         descriptorKey = "median";
         sortByMaxVal = false;
         break;
      case WBPPLocalNormalizationReferenceFrameMetric.STARS:
         descriptorKey = "numberOfStars";
         break;
   }

   // sort by measuring criteria
   activeFrames.sort( ( a, b ) =>
   {
      let aVal;
      let bVal;

      aVal = a.descriptor[ descriptorKey ];
      bVal = b.descriptor[ descriptorKey ];
      return sortByMaxVal ? bVal - aVal : aVal - bVal;
   } );

   let N = 1;
   // we exclude from the count the frames with the __integrated__ property used in LN interactive mode
   // to properly compute the number of best frmes to be integrated
   let filteredActiveFrames = activeFrames.filter( f => ( f.__integrated__ == undefined ) )
   if ( filteredActiveFrames.length >= 3 &&
      engine.localNormalizationRerenceFrameGenerationMethod != WBPPLocalNormalizationReferenceFrameMethod.SINGLE_BEST )
      // determine the number of frames to be integrated
      N = Math.max( 3, Math.min( engine.localNormalizationMaxIntegratedFrames, Math.floor( filteredActiveFrames.length / 3 ) ) );

   return {
      descriptorKey: descriptorKey,
      N: N,
      activeFrames: activeFrames
   };
};

/**
 * Generates the local normalization reference frame and returns the file path.
 *
 * @param {*} group
 * @param {*} bestFrames
 * @param {*} logEnabled
 * @param {*} desiredFileName
 */
StackEngine.prototype.generateLNReference = function( group, bestFrames, logEnabled, desiredFileName )
{
   let activeFrames = group.activeFrames();
   if ( logEnabled == undefined )
      logEnabled = true;

   let
   {
      enableTargetFrames,
      existingDirectory,
      getImageSize
   } = WBPPUtils.shared();

   let lbl = this.readableLNReferenceSelectionMethod();
   let dk;

   if ( bestFrames == undefined )
   {
      let
      {
         descriptorKey,
         N,
         activeFrames
      } = engine.sortFramesForLocalNormalizationReference( group );
      dk = descriptorKey;
      bestFrames = activeFrames.slice( 0, N );
      console.noteln( "Selecting the best reference frames for Local Normalization using ", dk, " metric." );
   }

   // if 1 frame hes been selected then return it, otherwise proceed with the integration
   if ( engine.localNormalizationRerenceFrameGenerationMethod == WBPPLocalNormalizationReferenceFrameMethod.SINGLE_BEST )
   {
      console.noteln( "Local normalization: using the single best frame as reference." );
      if ( logEnabled )
         this.processLogger.addSuccess( "Local normalization: ", "using the single frame with ", dk, " as reference." );
      return {
         lnReferenceFilePath: bestFrames[ 0 ].current,
         cached: false
      };
   }
   else if ( bestFrames.length < 3 )
   {
      console.warningln( "** Warning: Local normalization, not enough best frames found, using the single best frame as reference." );
      if ( logEnabled )
         this.processLogger.addWarning( "Local normalization: ", "not enough best frames found, using the single best frame as reference." );
      return {
         lnReferenceFilePath: bestFrames[ 0 ].current,
         cached: false
      };
   }

   console.noteln( "Local normalization: generate the reference frame selecting " + bestFrames.length + " frames with " + lbl + " amongst " + activeFrames.length + " frames" );
   // do LN and Integration on a temporary group overriding the method to MEDIAN
   let integrationGroup = group.cloneWithActiveItems( bestFrames );

   // perform local normalization using the best frame as reference
   let integratedFrames = integrationGroup.activeFrames();
   let LN = new LocalNormalization;

   let subfolder = integrationGroup.folderName();
   LN.outputDirectory = existingDirectory( engine.outputDirectory + "/registered/" + subfolder + "/ln_reference_frame_data" );

   // read the current reference frame size

   let referenceImageSize = getImageSize( integratedFrames[ 0 ].current );
   let imageRefrenceDimension = Math.max( referenceImageSize.width, referenceImageSize.height );

   LN.referencePathOrViewId = integratedFrames[ 0 ].current;
   LN.referenceIsView = false;
   LN.scale = imageRefrenceDimension / engine.localNormalizationGridSize;
   LN.referenceRejection = true;
   LN.referenceRejectionThreshold = 3.00;
   LN.targetRejectionThreshold = 3.20;
   LN.psfMaxStars = engine.localNormalizationPsfMaxStars;
   LN.psfMinSNR = engine.localNormalizationPsfMinSNR;
   LN.psfAllowClusteredSources = engine.localNormalizationPsfAllowClusteredSources;
   LN.lowClippingLevel = engine.localNormalizationLowClippingLevel;
   LN.highClippingLevel = engine.localNormalizationHighClippingLevel;
   LN.scaleEvaluationMethod = this.localNormalizationMethod == 0 ?
      LocalNormalization.prototype.ScaleEvaluationMethod_PSFSignal :
      LocalNormalization.prototype.ScaleEvaluationMethod_MultiscaleAnalysis;
   LN.psfType = [
      LocalNormalization.prototype.PSFType_Gaussian,
      LocalNormalization.prototype.PSFType_Moffat15,
      LocalNormalization.prototype.PSFType_Moffat4,
      LocalNormalization.prototype.PSFType_Moffat6,
      LocalNormalization.prototype.PSFType_Moffat8,
      LocalNormalization.prototype.PSFType_MoffatA,
      LocalNormalization.prototype.PSFType_Auto
   ][ this.localNormalizationPsfType ];
   LN.psfGrowth = engine.localNormalizationPsfGrowth;
   LN.overwriteExistingFiles = false;

   let LNSource = LN.toSource( "JavaScript", "LN" /*varId*/ , 0 /*indent*/ ,
      SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();

   // Check if valid cached data is present
   let fileITemsToNormalize = integratedFrames;
   let LNCache = {};
   let LNcacheKey = engine.executionCache.keyFor( "LNReference" + LNSource );
   if ( engine.executionCache.hasCacheForKey( LNcacheKey ) )
   {

      console.noteln( "LN Reference frame generation has cached data for key ", LNcacheKey );

      // thge cache is the map between each input file and the correspondent output xnml file
      LNCache = engine.executionCache.cacheForKey( LNcacheKey );
      fileITemsToNormalize = [];

      // check if the reference file is unchanged
      if ( engine.executionCache.isFileUnmodified( LNcacheKey, LN.referencePathOrViewId ) )
      {
         // check which input file is unchanged
         for ( let i = 0; i < integratedFrames.length; i++ )
         {
            let inputFile = integratedFrames[ i ].current;
            let lnFile = LNCache[ inputFile ];
            if ( engine.executionCache.isFileUnmodified( LNcacheKey, inputFile ) &&
               engine.executionCache.isFileUnmodified( LNcacheKey, lnFile ) )
            {
               // the cached ln file is valid, associate it
               integratedFrames[ i ].addLocalNormalizationFile( lnFile );
               console.noteln( "LN file is cached: [", inputFile, "] -> [", lnFile, "]" );
            }
            else
            {
               fileITemsToNormalize.push( integratedFrames[ i ] );
               console.noteln( "LN file will be generated for: [", inputFile, "]" );
            }
         }
      }
   }
   else
   {
      console.noteln( "LN Reference frame generation has no cache date for key ", LNcacheKey );
   }

   let filePaths = fileITemsToNormalize.map( item => item.current );
   // process is saved in container with the full list of files to be normalized
   LN.targetItems = enableTargetFrames( filePaths, 2 );
   engine.processContainer.add( LN );

   // perform LN if there are files to normalize
   let lnSuccess = true;
   if ( filePaths.length > 0 )
   {
      LN.targetItems = enableTargetFrames( filePaths, 2 );
      lnSuccess = LN.executeGlobal();
   }

   // ignore any result if something went wrong. We accept LN data only if normalized files have been gerenrated for
   // all input files provided
   let useLN = true;

   /* AUX: delete all normalized files generated by the provided LN instance */
   let cleanLNFiles = function( LN )
   {
      if ( !LN.outputData )
         return

      let lnFiles = LN.outputData.map( item => ( item[ 0 ] || "" ) );
      for ( let k = 0; k < lnFiles.length; k++ )
         File.remove( lnFiles[ k ] )
   }

   // clean the generated ln files and disable LN if something went wrong
   let lnFiles = [];
   if ( !lnSuccess )
      useLN = false;
   else if ( fileITemsToNormalize.length > 0 && ( !LN.outputData || LN.outputData.length != fileITemsToNormalize.length ) )
   {
      useLN = false;
   }
   else
   {
      lnFiles = LN.outputData.map( item => ( item[ 0 ] || "" ) );
      // ensure that LN files have been created for each file
      for ( let k = 0; k < lnFiles.length; k++ )
         if ( lnFiles[ k ].length == 0 || !File.exists( lnFiles[ k ] ) )
         {
            useLN = false;
            break;
         }
   }

   // in case of success then integrate the input files along with the corresponding local normalization files
   if ( useLN )
   {
      // merge cached and generated normalized files
      for ( let i = 0; i < filePaths.length; i++ )
      {
         let inputFile = filePaths[ i ];
         let lnFile = lnFiles[ i ];
         fileITemsToNormalize[ i ].addLocalNormalizationFile( lnFile );
         console.noteln( "associate the local normalization file: [" + fileITemsToNormalize[ i ].current + "] -> [" + lnFile + "]" );

         // cache the result
         LNCache[ inputFile ] = lnFile;
         engine.executionCache.cacheFileLMD( LNcacheKey, inputFile );
         engine.executionCache.cacheFileLMD( LNcacheKey, lnFile );
         LNCache[ inputFile ] = lnFile;
      }
      // save the updated cache
      engine.executionCache.setCache( LNcacheKey, LNCache );

      // integrate the reference frame
      let
      {
         masterFilePath,
         cached
      } = this.doIntegrate(
         integrationGroup, /* frameGroup */
         "LN_Reference_", /* customPrefix */
         "", /* customPostfix */
         undefined, /* customGenerateRejectionMaps */
         false, /* customGenerateDrizzle */
         desiredFileName, /* desired master file name */
         {
            /* II overridden parameters */
            combination: ImageIntegration.prototype.Average,
            rejection: integrationGroup.bestRejectionMethod(),
            normalization: ImageIntegration.prototype.AdditiveWithScaling,
            rejectionNormalization: ImageIntegration.prototype.LocalRejectionNormalization,
            weightMode: ImageIntegration.prototype.PSFSignalWeight,
            rangeClipLow: true,
            rangeLow: 0,
            generateRejectionMaps: false
         },
         undefined, /* FITS keywords */
         false /* handleAstrometricSolution */
      );

      // if integration failed then return the best frame
      if ( masterFilePath.length == 0 )
      {
         console.warningln( "** Warning: Local normalization, integration failed, using the best frame as reference" );
         if ( logEnabled )
            this.processLogger.addWarning( "Local normalization: ", "integration failed, using the best frame as reference" );
         return {
            lnReferenceFilePath: integratedFrames[ 0 ].current,
            cached: false
         };
      }
      console.noteln( "Local normalization: " + "reference frame generated by integrating " + integratedFrames.length + " frames" );
      if ( logEnabled )
         this.processLogger.addSuccess( "Local normalization: ", "reference frame generated by integrating " + integratedFrames.length + " frames" );
      return {
         lnReferenceFilePath: masterFilePath,
         cached: cached
      };
   }
   else
   {
      cleanLNFiles( LN );
      console.warningln( "** Warning: Local normalization, local normalizaton of best frames failed, using the best frame as reference" );
      if ( logEnabled )
         this.processLogger.addWarning( "Local normalization: ", "local normalizaton of best frames failed, using the best frame as reference" );
      return {
         lnReferenceFilePath: integratedFrames[ 0 ].current,
         cached: false
      };
   }
};

// ----------------------------------------------------------------------------

/**
 * Generates the pipeline steps to process the light frames.
 */
StackEngine.prototype.buildPipelineForLight = function()
{
   // pre-process groups first
   let groupsPRE = this.groupsManager.groupsForMode( WBPPGroupingMode.PRE );

   // CALIBRATION
   for ( let i = 0; i < groupsPRE.length; ++i )
      if ( groupsPRE[ i ].imageType == ImageType.LIGHT )
      {
         let logEnv = {
            processLogger: this.processLogger,
            group: groupsPRE[ i ]
         };

         // log header
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.newLine();
            env.params.processLogger.addMessage( env.params.group.logStringHeader( 'LIGHT FRAMES CALIBRATION' ) );
         }, logEnv );

         // calibration step (only if calibration masters are found)
         let cg = this.getCalibrationGroupsFor( groupsPRE[ i ] );
         if ( cg.masterBias || cg.masterDark || cg.masterFlat )
         {
            let calibrationOperation = new this.calibrationOperation( groupsPRE[ i ] );
            this.WS.calibrationLight.size += calibrationOperation.spaceRequired();
            this.operationQueue.addOperation( calibrationOperation );
         }

         // log footer
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.addMessage( env.params.group.logStringFooter() );
            env.params.processLogger.newLine();
         }, logEnv );
      }

   // LINEAR PATTERN SUBTRACTION
   if ( this.linearPatternSubtraction )
   {
      let logEnv = {
         processLogger: this.processLogger
      };

      // log header
      this.operationQueue.addOperationBlock( ( env ) =>
      {
         env.params.processLogger.newLine();
         env.params.processLogger.addMessage( "<b>********************</b> <i>LINEAR DEFECTS CORRECTION</i> <b>********************</b>" );
      }, logEnv );

      let LPSOperation = new this.LPSOperation();
      this.WS.LPS.size += LPSOperation.spaceRequired();
      this.operationQueue.addOperation( LPSOperation );

      // log footer
      this.operationQueue.addOperationBlock( ( env ) =>
      {
         env.params.processLogger.addMessage( "<b>" + SEPARATOR + "</b>" );
         env.params.processLogger.newLine();
      }, logEnv );
   }

   // COSMETIC CORRECTON AND DEBAYER
   for ( let i = 0; i < groupsPRE.length; ++i )
      if ( groupsPRE[ i ].imageType == ImageType.LIGHT )
      {
         let logEnv = {
            processLogger: this.processLogger,
            group: groupsPRE[ i ]
         };

         let needsCC = groupsPRE[ i ].CCTemplate && groupsPRE[ i ].CCTemplate.length > 0;
         let needsDebayer = groupsPRE[ i ].isCFA;

         if ( !needsCC && !needsDebayer )
            continue;

         // log header
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.newLine();
            env.params.processLogger.addMessage( env.params.group.logStringHeader( 'COSMETIZATION / DEBAYERING' ) );
         }, logEnv );

         // cosmetic correction
         if ( needsCC )
         {
            let cosmeticCorretcionOperation = new this.CosmeticCorrectionOperation( groupsPRE[ i ] );
            this.WS.cosmeticCorrection.size += cosmeticCorretcionOperation.spaceRequired();
            this.operationQueue.addOperation( cosmeticCorretcionOperation );
         }
         else
         {
            // log CC disabled
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addSuccess( "Cosmetic Correction", "disabled." );
            }, logEnv );
         }

         // debayer
         if ( needsDebayer )
         {
            let debayerOperation = new this.DebayerOperation( groupsPRE[ i ] );
            this.WS.debayer.size += debayerOperation.spaceRequired();
            this.operationQueue.addOperation( debayerOperation );
         }
         else
         {
            // log CC disabled
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addSuccess( "Debayer", "Grayscale frames, no demosaicing is needed." );
            }, logEnv );
         }

         // log footer
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.addMessage( env.params.group.logStringFooter() );
            env.params.processLogger.newLine();
         }, logEnv );
      }

   // REFERENCE FRAME DATA PREPARATION
   this.operationQueue.addOperation( new this.ReferenceFrameDataPreparationOperation() );

   // MEASUREMENT OPERATION
   let writeWeights = this.subframeWeightingEnabled && this.subframesWeightsMethod == WBPPSubframeWeightsMethod.FORMULA
   let generateSubframesWeights = this.subframeWeightingEnabled && ( ( this.subframesWeightsMethod == WBPPSubframeWeightsMethod.FORMULA ) || this.integrate );
   let measureImages = generateSubframesWeights || ( this.imageRegistration && ( this.bestFrameRefernceMethod != WBPPBestRefernenceMethod.MANUAL ) ) || this.localNormalization;
   if ( !engine.groupsManager.isEmpty() && measureImages )
   {
      this.operationQueue.addOperation( new this.MeasurementOperation() );
      if ( engine.subframeWeightingEnabled && ( engine.subframesWeightsMethod == WBPPSubframeWeightsMethod.FORMULA ) )
         this.operationQueue.addOperation( new this.CustomFormulaWeightsGenerationOperation() );
      if ( !engine.groupsManager.isEmpty() && ( engine.subframesWeightsMethod != WBPPSubframeWeightsMethod.PSFScaleSNR ) && this.integrate )
         this.operationQueue.addOperation( new this.BadFramesRejectionOperation() );
   }

   // SET THE REFERENCE FRAME
   if ( !engine.groupsManager.isEmpty() && this.imageRegistration )
      this.operationQueue.addOperation( new this.ReferenceFrameSelectionOperation() );

   // exit if no registration, local normalization and integration needs to be performed
   if ( !generateSubframesWeights && !this.imageRegistration && !this.localNormalization && !this.integrate )
      return;

   // POST-PROCESS GROUPS
   let groupsPOST = engine.groupsManager.groupsForMode( WBPPGroupingMode.POST ).filter( g => g.isActive );

   // POST-PROCESS GROUPS - PLATE SOLVE
   if ( !engine.groupsManager.isEmpty() && this.imageRegistration && this.platesolve )
      this.operationQueue.addOperation( new this.PlatesolvingOperation() );

   // WRITE WEIGHTS
   if ( !engine.groupsManager.isEmpty() && writeWeights )
   {
      let writeWeightsOperation = new this.WriteWeightsOperation();
      this.WS.imageWeighting.size += writeWeightsOperation.spaceRequired();
      this.operationQueue.addOperation( writeWeightsOperation );
   }

   // POST-PROCESS GROUPS - REGISTRATION
   if ( this.imageRegistration )
      for ( let i = 0; i < groupsPOST.length; i++ )
      {
         let standardPostCalibrationProcessing = groupsPOST[ i ].associatedRGBchannel != WBPPAssociatedChannel.COMBINED_RGB;

         if ( standardPostCalibrationProcessing )
         {
            let logEnv = {
               processLogger: this.processLogger,
               group: groupsPOST[ i ]
            };

            // log header
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringHeader( 'IMAGE REGISTRATION' ) );
            }, logEnv )

            let registrationOperation = new this.RegistrationOperation( groupsPOST[ i ] );
            this.WS.registration.size += registrationOperation.spaceRequired();
            this.operationQueue.addOperation( registrationOperation );

            // log footer
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringFooter() );
               env.params.processLogger.newLine();
            }, logEnv )
         }
      }

   // POST-PROCESS GROUPS - LOCAL NORMALIZATION REFERENCE FRAME SELECTION
   if ( this.localNormalization )
      for ( let i = 0; i < groupsPOST.length; i++ )
      {
         let standardPostCalibrationProcessing = groupsPOST[ i ].associatedRGBchannel != WBPPAssociatedChannel.COMBINED_RGB;

         if ( standardPostCalibrationProcessing )
         {
            let logEnv = {
               processLogger: this.processLogger,
               group: groupsPOST[ i ]
            };

            // log header
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringHeader( 'LOCAL NORMALIZATION - REFERENCE FRAME SELECTION' ) );
            }, logEnv );

            let lnReferenceFrameSelectionOperation = new this.LocalNormalizationReferenceFrameSelectionOperation( groupsPOST[ i ] );
            this.WS.localNormalization.size += lnReferenceFrameSelectionOperation.spaceRequired();
            this.operationQueue.addOperation( lnReferenceFrameSelectionOperation );

            // log footer
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringFooter() );
               env.params.processLogger.newLine();
            }, logEnv );
         }
      }

   // POST-PROCESS GROUPS - LOCAL NORMALIZATION
   if ( this.localNormalization )
      for ( let i = 0; i < groupsPOST.length; i++ )
      {
         let standardPostCalibrationProcessing = groupsPOST[ i ].associatedRGBchannel != WBPPAssociatedChannel.COMBINED_RGB;

         if ( standardPostCalibrationProcessing )
         {
            let logEnv = {
               processLogger: this.processLogger,
               group: groupsPOST[ i ]
            };

            // log header
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringHeader( 'LOCAL NORMALIZATION' ) );
            }, logEnv );

            let lnOperation = new this.LocalNormalizationOperation( groupsPOST[ i ] );
            this.WS.localNormalization.size += lnOperation.spaceRequired();
            this.operationQueue.addOperation( lnOperation );

            // log footer
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringFooter() );
               env.params.processLogger.newLine();
            }, logEnv );
         }
      }

   // POST-PROCESS GROUPS - IMAGE INTGEGRATION
   if ( this.integrate )
   {
      for ( let i = 0; i < groupsPOST.length; i++ )
      {
         let standardPostCalibrationProcessing = groupsPOST[ i ].associatedRGBchannel != WBPPAssociatedChannel.COMBINED_RGB;

         if ( standardPostCalibrationProcessing )
         {
            let logEnv = {
               processLogger: this.processLogger,
               group: groupsPOST[ i ]
            };

            // log header
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringHeader( 'IMAGE INTEGRATION' ) );
            }, logEnv );

            let imageIntegrationOperation = new this.ImageIntegrationOperation( groupsPOST[ i ] );
            this.WS.integration.size += imageIntegrationOperation.spaceRequired();
            this.operationQueue.addOperation( imageIntegrationOperation );

            // drizzle (requires both image integratino and registration to be enabled)
            if ( groupsPOST[ i ].isDrizzleEnabled() && this.imageRegistration )
            {
               let drizzleIntegrationOperation = new this.DrizzleIntegrationOperation( groupsPOST[ i ] );
               this.WS.integration.size += drizzleIntegrationOperation.spaceRequired();
               this.operationQueue.addOperation( drizzleIntegrationOperation );
            }

            // log footer
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringFooter() );
               env.params.processLogger.newLine();
            }, logEnv );
         }
      }

      if ( this.integrate && this.autocrop )
      {
         let logEnv = {
            processLogger: this.processLogger
         };

         // log header
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            let msg = "";
            console.writeln();
            env.params.processLogger.addMessage( "<b>*********************** <i>AUTO CROP</i> ***********************</b>" );
            console.writeln( SEPARATOR );
            console.writeln( "* Begin autocrop of master light frames" );
            console.writeln( SEPARATOR );
         }, logEnv );

         let autocropOperation = new this.AutoCropOperation();
         this.WS.integration.size += autocropOperation.spaceRequired();
         this.operationQueue.addOperation( autocropOperation );

         // log footer
         this.operationQueue.addOperationBlock( ( env ) =>
         {
            env.params.processLogger.addMessage( "<b>" + SEPARATOR + "</b>" );
            env.params.processLogger.newLine();
            console.writeln( SEPARATOR );
            console.writeln( "* End autocrop of master light frames" );
            console.writeln( SEPARATOR );
         }, logEnv );
      }

      // process the RGB recombination groups, this needs to be done at the end to ensure that all
      // gray R,G and B masters have been integrated
      for ( let i = 0; i < groupsPOST.length; i++ )
      {
         let logEnv = {
            processLogger: this.processLogger,
            group: groupsPOST[ i ]
         };

         // RGB RECOMBINATION
         if ( groupsPOST[ i ].associatedRGBchannel == WBPPAssociatedChannel.COMBINED_RGB )
         {
            // log header
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringHeader( 'RGB COMBINATION' ) );
            }, logEnv );

            // RGB Recombination
            let recombinationOperation = new this.RGBRecombinationOperation( groupsPOST[ i ] );
            this.WS.recombination.size += recombinationOperation.spaceRequired();
            this.operationQueue.addOperation( recombinationOperation );

            // log footer
            this.operationQueue.addOperationBlock( ( env ) =>
            {
               env.params.processLogger.addMessage( env.params.group.logStringFooter() );
               env.params.processLogger.newLine();
            }, logEnv );
         }
      }
   }
};

StackEngine.prototype.resetRequiredSpace = function()
{
   // Space required
   this.WS = {
      masterBias:
      {
         label: "Master bias",
         size: 0
      },
      masterDark:
      {
         label: "Master darks",
         size: 0
      },
      masterFlat:
      {
         label: "Master flats",
         size: 0
      },
      calibrationFlat:
      {
         label: "Calibrated flats",
         size: 0
      },
      calibrationLight:
      {
         label: "Calibrated lights",
         size: 0
      },
      LPS:
      {
         label: "Linear pattern subtraction",
         size: 0
      },
      cosmeticCorrection:
      {
         label: "Cosmetic correction",
         size: 0
      },
      debayer:
      {
         label: "Debayer",
         size: 0
      },
      imageWeighting:
      {
         label: "Image Weighting",
         size: 0,
      },
      registration:
      {
         label: "Registration",
         size: 0
      },
      localNormalization:
      {
         label: "Local normalization",
         size: 0
      },
      integration:
      {
         label: "Master lights",
         size: 0
      },
      recombination:
      {
         label: "Channel recombination",
         size: 0
      }
   }
};

// ----------------------------------------------------------------------------

StackEngine.prototype.buildExecutionPipeline = function()
{
   this.operationQueue.clear();
   this.resetRequiredSpace();
   this.buildPipelineForBias();
   this.buildPipelineForDark();
   this.buildPipelineForFlat();
   this.buildPipelineForLight();
};

StackEngine.prototype.runPipeline = function()
{
   this.saveRunningConfiguration();

   if ( engine.usePipelineScript )
      this.operationQueue.installEventScript( engine.pipelineScriptFile );
   else
      this.operationQueue.uninstallEventScript();

   this.operationQueue.run();
};

// ----------------------------------------------------------------------------

/**
 * Integrates the provided group.
 *
 * @param {*} frameGroup
 * @param {String} customPrefix custom prefix to be added at the end of the master frame (default is "master")
 * @param {String} customPostfix custom postfix to be added at the end of the master frame
 * @param {Boolean} customGenerateRejectionMaps optionally overrides the rejection maps setting
 * @param {Boolean} customGenerateDrizzle optionally override the drizzle files generation
 * @param {String} desiredFileName optionally requires a master file name
 * @param {Boolean} overrideIIparameters to override ImageIntegration's parameters, needs to be an object with key/values
 * @param {Array} FITSKeywords optional list of FITS keyword to inject into the integrated image before saving
 * @param {Boolean} handleAstrometricSolution optional, TRUE by default, FALSE if astrometric solution has not to be integrated
 */
StackEngine.prototype.doIntegrate = function(
   frameGroup,
   customPrefix,
   customPostfix,
   customGenerateRejectionMaps,
   customGenerateDrizzle,
   desiredFileName,
   overrideIIparameters,
   FITSKeywords,
   handleAstrometricSolution )
{
   let
   {
      copyAstrometricSolution,
      enableTargetFrames,
      existingAndUniqueFileName,
      getLastModifiedDate,
      hasAstrometricSolution
   } = WBPPUtils.shared();

   let filePath = "";
   let imageType = frameGroup.imageType;

   console.noteln( "<end><cbr><br>", SEPARATOR );
   console.noteln( "* Begin integration of ", StackEngine.imageTypeToString( imageType ) + " frames" );
   console.noteln( SEPARATOR );
   frameGroup.log();

   if ( handleAstrometricSolution == undefined )
      handleAstrometricSolution = engine.platesolve;

   let useCache = false;

   let activeFrames = frameGroup.activeFrames();
   if ( activeFrames.length < 3 )
   {
      console.warningln( "** Warning: Cannot integrate less than three frames." );
      this.processLogger.addWarning( "Cannot integrate less than three frames." );
   }
   else
   {

      let selectedRejection = ( this.rejection[ imageType ] == ImageIntegration.prototype.auto ) ?
         frameGroup.bestRejectionMethod() : this.rejection[ imageType ];

      if ( this.rejection[ imageType ] == ImageIntegration.prototype.auto )
      {
         console.noteln( "Rejection method auto-selected: ", engine.rejectionName( selectedRejection ) );
         this.processLogger.addMessage( "<b>Rejection method auto-selected:</b> " + engine.rejectionName( selectedRejection ) );
      }
      else
      {
         console.noteln( "<b>Rejection method:</b> ", engine.rejectionName( selectedRejection ) );
      }

      // Drizzle is generated only for Light frames
      let generateDrizzle = imageType == ImageType.LIGHT && ( ( customGenerateDrizzle != undefined ) ? customGenerateDrizzle : true );

      // ensure that drizzle files exists otherwise we disable the drizzle handling
      if ( imageType == ImageType.LIGHT )
         for ( let i = 0; i < activeFrames.length; i++ )
         {
            let sanitizedFileName = activeFrames[ i ].drizzleFile || "";
            let valid = sanitizedFileName.length > 0;

            if ( !valid || !File.exists( sanitizedFileName ) )
            {
               generateDrizzle = false;
               console.warningln( "** Warning: Drizzle file not found: ", activeFrames[ i ].drizzleFile );
               console.warningln( "** Warning: disabling the update of the drizzle files." );
               break;
            }
         }


      // add local normalization files if xnml files are found for all frames
      let useLN = activeFrames.reduce( ( acc, item ) => ( acc && ( item.localNormalizationFile != undefined ) ), true );
      let embedRejectionMaps = ( customGenerateRejectionMaps != undefined ) ? customGenerateRejectionMaps : this.generateRejectionMaps;

      let II = new ImageIntegration;
      II.inputHints = this.inputHints();
      II.bufferSizeMB = 16;
      II.stackSizeMB = 1024;
      II.autoMemorySize = true;
      II.autoMemoryLimit = 0.75;
      II.images = enableTargetFrames( activeFrames, 2, generateDrizzle, useLN );
      // FIX: local normalization files are associated to the file item but can have a differnt path, we need ot override the file path
      // generated by enableTargetFrames function
      II.combination = this.combination[ imageType ];
      II.rejection = selectedRejection;
      II.generateRejectionMaps = embedRejectionMaps || this.autocrop;
      II.generateDrizzleData = generateDrizzle;
      II.pcClipLow = this.percentileLow[ imageType ];
      II.pcClipHigh = this.percentileHigh[ imageType ];
      II.sigmaLow = this.sigmaLow[ imageType ];
      II.sigmaHigh = this.sigmaHigh[ imageType ];
      II.winsorizationCutoff = 5.0;
      II.linearFitLow = this.linearFitLow[ imageType ];
      II.linearFitHigh = this.linearFitHigh[ imageType ];
      II.esdOutliersFraction = this.ESD_Outliers[ imageType ];
      II.esdAlpha = this.ESD_Significance[ imageType ];
      II.rcrLimit = this.RCR_Limit[ imageType ];
      II.clipLow = true;
      II.clipHigh = true;
      II.largeScaleClipLow = false;
      II.largeScaleClipHigh = false;
      II.subtractPedestals = false;
      II.truncateOnOutOfRange = true;
      II.generate64BitResult = false;
      II.showImages = false; // since core 1.8.8-6
      II.useFileThreads = true;
      II.fileThreadOverload = 1.00;
      II.weightScale = ImageIntegration.prototype.WeightScale_BWMV;

      switch ( imageType )
      {
         case ImageType.LIGHT:
            II.minWeight = this.minWeight; // since core 1.8.9-1
            II.normalization = ImageIntegration.prototype.AdditiveWithScaling;
            II.rejectionNormalization = ImageIntegration.prototype.Scale;
            II.largeScaleClipHigh = this.lightsLargeScaleRejectionHigh;
            II.largeScaleClipHighProtectedLayers = this.lightsLargeScaleRejectionLayersHigh;
            II.largeScaleClipHighGrowth = this.lightsLargeScaleRejectionGrowthHigh;
            II.largeScaleClipLow = this.lightsLargeScaleRejectionLow;
            II.largeScaleClipLowProtectedLayers = this.lightsLargeScaleRejectionLayersLow;
            II.largeScaleClipLowGrowth = this.lightsLargeScaleRejectionGrowthLow;
            II.subtractPedestals = true;
            break;
         case ImageType.FLAT:
            II.normalization = ImageIntegration.prototype.Multiplicative;
            II.rejectionNormalization = ImageIntegration.prototype.EqualizeFluxes;
            II.largeScaleClipHigh = this.flatsLargeScaleRejection;
            II.largeScaleClipHighProtectedLayers = this.flatsLargeScaleRejectionLayers;
            II.largeScaleClipHighGrowth = this.flatsLargeScaleRejectionGrowth;
            break;
         default:
            II.normalization = ImageIntegration.prototype.NoNormalization;
            II.rejectionNormalization = ImageIntegration.prototype.NoRejectionNormalization;
            break;
      }

      switch ( imageType )
      {
         case ImageType.LIGHT:
            if ( this.subframeWeightingEnabled )
            {
               II.weightMode = [
                  ImageIntegration.prototype.PSFSignalWeight,
                  ImageIntegration.prototype.PSFSNR,
                  ImageIntegration.prototype.PSFScaleSNR,
                  ImageIntegration.prototype.SNREstimate,
                  ImageIntegration.prototype.KeywordWeight
               ][ this.subframesWeightsMethod ];
               II.weightKeyword = WEIGHT_KEYWORD;
            }
            else
            {
               II.weightMode = ImageIntegration.prototype.DontCare;
            }

            II.evaluateSNR = true;
            II.rangeClipLow = true;
            II.rangeLow = 0;
            II.rangeClipHigh = false;
            II.truncateOnOutOfRange = false;
            II.useCache = true;
            break;
         default:
            II.weightMode = ImageIntegration.prototype.DontCare;
            II.evaluateSNR = false;
            II.rangeClipLow = false;
            II.rangeClipHigh = false;
            II.useCache = false;
            break;
      }

      // finally enable local normalization if normalization files have been provided
      if ( useLN )
      {
         II.normalization = ImageIntegration.prototype.LocalNormalization;
         II.rejectionNormalization = ImageIntegration.prototype.LocalRejectionNormalization;
         // ### N.B. LN is incompatible with subtraction of pedestals. This is
         // because the LN functions have been computed with the pedestals
         // added. This applies to both rejection and output normalizations.
         II.subtractPedestals = false;
      }

      // override the II parameters
      if ( overrideIIparameters )
         Object.keys( overrideIIparameters ).forEach( key =>
         {
            // must be a valid key
            if ( II[ key ] != undefined )
               II[ key ] = overrideIIparameters[ key ];
         } );

      /**
       * Check if valid cached data can be used, generate rejection maps is a key and has to be chcked separately since
       * the masters will change depending on this value while the II.generateRejectionMaps property may have not changed
       * since it also depends on the autocrop option.
       */

      let IISource = II.toSource( "JavaScript", "II" /*varId*/ , 0 /*indent*/ ,
         SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();

      let IIcacheKey = engine.executionCache.keyFor( IISource + "_" + this.generateRejectionMaps );
      console.writeln();
      if ( engine.executionCache.hasCacheForKey( IIcacheKey ) )
      {
         console.noteln( "ImageIntegration has cached data for key ", IIcacheKey )
         // Image Integration cache consists in the integrated image filePath
         let IICacheOutputFilePath = engine.executionCache.cacheForKey( IIcacheKey );

         useCache = IICacheOutputFilePath != undefined;

         // the cache is valid only if all input files and the integrated file are unchanged
         for ( let i = 0;
            ( i < activeFrames.length ) && useCache; i++ )
         {
            // source images must be unchanged
            if ( !engine.executionCache.isFileUnmodified( IIcacheKey, activeFrames[ i ].current ) )
               useCache = false;
            if ( useCache && generateDrizzle && !engine.executionCache.isFileUnmodified( IIcacheKey, activeFrames[ i ].drizzleFile ) )
               useCache = false;
            // if using the local normalization then the source images must be unchanged
            if ( useCache && useLN && !engine.executionCache.isFileUnmodified( IIcacheKey, activeFrames[ i ].localNormalizationFile ) )
               useCache = false;
         }

         // source images must be unchanged
         if ( useCache )
            useCache = engine.executionCache.isFileUnmodified( IIcacheKey, IICacheOutputFilePath )

         // determine if using cache or not
         if ( useCache )
         {
            filePath = IICacheOutputFilePath;
            console.noteln( "ImageIntegration the cache is valid, skip the integration and use the cached result at path ", IICacheOutputFilePath );
         }
         else
            console.noteln( "ImageIntegration the cache is not valid, proceed with the integration" );
      }
      else
      {
         console.noteln( "ImageIntegration has no cache data for key ", IIcacheKey );
      }
      console.writeln();

      // cache the drizzle file LMD to be udated if needed
      let drizzleFileLMD = useCache ?
      {} : activeFrames.reduce( ( acc, item ) =>
      {
         if ( item.drizzleFile != undefined && item.drizzleFile.length > 0 )
            acc[ item.drizzleFile ] = getLastModifiedDate( item.drizzleFile );
         return acc;
      },
      {} );

      // PROCEED
      console.writeln( SEPARATOR2 );
      console.writeln( IISource );
      console.writeln( SEPARATOR2 );

      let ok = true;
      if ( useCache )
      {
         console.noteln( "** Using cached data for ImageIntegration." );
         console.noteln( "<end><cbr><br>* master " + StackEngine.imageTypeToString( imageType ) + " frame:" );
         console.noteln( "<raw>" + filePath + "</raw>" );
      }
      else
      {
         ok = II.executeGlobal();
      }

      II.showImages = true;
      engine.processContainer.add( II );

      if ( !ok )
      {
         console.warningln( "** Warning: ImageIntegration failed." );
         this.processLogger.addWarning( "ImageIntegration failed." );
      }
      else if ( !useCache )
      {

         // Write master frame FITS keywords
         // Build the file name postfix

         let keywords = new Array;
         if ( FITSKeywords )
            for ( let i = 0; i < FITSKeywords.length; i++ )
               keywords.push( FITSKeywords[ i ] );

         keywords.push( new FITSKeyword( "COMMENT", "", "PixInsight image preprocessing pipeline" ) );
         keywords.push( new FITSKeyword( "COMMENT", "", "Master frame generated with " + TITLE + " v" + VERSION ) );

         keywords.push( new FITSKeyword( "IMAGETYP", StackEngine.imageTypeToMasterKeywordValue( imageType ), "Type of image" ) );

         keywords.push( new FITSKeyword( "XBINNING", format( "%d", frameGroup.binning ), "Binning factor, horizontal axis" ) );
         keywords.push( new FITSKeyword( "YBINNING", format( "%d", frameGroup.binning ), "Binning factor, vertical axis" ) );

         keywords.push( new FITSKeyword( "FILTER", frameGroup.filter, "Filter used when taking image" ) );

         keywords.push( new FITSKeyword( "EXPTIME", format( "%.2f", frameGroup.exposureTime ), "Exposure time in seconds" ) );

         //  inject the overscan area configuration to any master BIAS, DARK and FLATS
         if ( imageType != ImageType.LIGHT && engine.overscan.enabled )
         {
            keywords.push( new FITSKeyword( "OSIR0X0", format( "%d", this.overscan.imageRect.x0 ), "Custom WBPP Info: overscan image rect x0" ) );
            keywords.push( new FITSKeyword( "OSIR0Y0", format( "%d", this.overscan.imageRect.y0 ), "Custom WBPP Info: overscan image rect y0" ) );
            keywords.push( new FITSKeyword( "OSIR0X1", format( "%d", this.overscan.imageRect.x1 ), "Custom WBPP Info: overscan image rect x1" ) );
            keywords.push( new FITSKeyword( "OSIR0Y0", format( "%d", this.overscan.imageRect.y1 ), "Custom WBPP Info: overscan image rect y1" ) );

            for ( let i = 0; i < 4; i++ )
               if ( this.overscan.overscan[ i ].enabled )
               {
                  keywords.push( new FITSKeyword( "OSSR" + i + "X0", format( "%d", this.overscan.overscan[ i ].sourceRect.x0 ), "Custom WBPP Info: overscan source rect x0" ) );
                  keywords.push( new FITSKeyword( "OSSR" + i + "Y0", format( "%d", this.overscan.overscan[ i ].sourceRect.y0 ), "Custom WBPP Info: overscan source rect y0" ) );
                  keywords.push( new FITSKeyword( "OSSR" + i + "X1", format( "%d", this.overscan.overscan[ i ].sourceRect.x1 ), "Custom WBPP Info: overscan source rect x1" ) );
                  keywords.push( new FITSKeyword( "OSSR" + i + "Y1", format( "%d", this.overscan.overscan[ i ].sourceRect.y1 ), "Custom WBPP Info: overscan source rect y1" ) );
                  keywords.push( new FITSKeyword( "OSTR" + i + "X0", format( "%d", this.overscan.overscan[ i ].targetRect.x0 ), "Custom WBPP Info: overscan target rect x0" ) );
                  keywords.push( new FITSKeyword( "OSTR" + i + "Y0", format( "%d", this.overscan.overscan[ i ].targetRect.y0 ), "Custom WBPP Info: overscan target rect y0" ) );
                  keywords.push( new FITSKeyword( "OSTR" + i + "X1", format( "%d", this.overscan.overscan[ i ].targetRect.x1 ), "Custom WBPP Info: overscan target rect x1" ) );
                  keywords.push( new FITSKeyword( "OSTR" + i + "Y1", format( "%d", this.overscan.overscan[ i ].targetRect.y1 ), "Custom WBPP Info: overscan target rect y1" ) );
               }
         }

         // concatenate the image keywords filtering out the keywords already added that have to remain unique
         let uniqueKeywords = [ "IMAGETYP", "XBINNING", "YBINNING", "FILTER", "EXPTIME" ];
         let window = ImageWindow.windowById( II.integrationImageId );
         window.keywords = keywords.concat( window.keywords.filter( k => uniqueKeywords.indexOf( k.name ) == -1 ) );

         // propagate and optimize the astrometric solution for light frames
         if ( imageType == ImageType.LIGHT && handleAstrometricSolution )
         {
            // check if the first integrated frame contains an astrometric solution, in that case we transfer the solution to the
            // integrated image
            let referenceWindows = ImageWindow.open( activeFrames[ 0 ].current );
            if ( referenceWindows.length > 0 )
            {
               let referenceWindow = referenceWindows[ 0 ];
               try
               {
                  // we copy the solution of the first frame
                  console.noteln( "* Copying and optimizing the astrometric solution from the first frame" );
                  copyAstrometricSolution( referenceWindow, window );
                  engine.solveImage( window, hasAstrometricSolution( window ) /** Optimize only */ );
                  console.noteln( window.astrometricSolutionSummary() );
               }
               catch ( e )
               {
                  console.warningln( e );
                  console.noteln( "An issue occurred while handling the astrometric solution; no astrometric solution will be generated." );
               }
               referenceWindow.forceClose();
            }
         }

         // for masterFlat if overscan is enabled we temporarily set the group size to the overscan region to generate the proper master file
         let fileName = "";
         if ( desiredFileName == undefined &&
            ( frameGroup.imageType == ImageType.FLAT || frameGroup.imageType == ImageType.LIGHT ) &&
            engine.overscan.enabled )
         {
            let W = frameGroup.size.width;
            let H = frameGroup.size.height;
            frameGroup.size.width = engine.overscan.imageRect.x1 - engine.overscan.imageRect.x0;
            frameGroup.size.height = engine.overscan.imageRect.y1 - engine.overscan.imageRect.y0;
            fileName = desiredFileName || frameGroup.folderName( false /* sanitized */ );
            frameGroup.size.width = W;
            frameGroup.size.height = H;
         }
         else
         {
            fileName = desiredFileName || frameGroup.folderName( false /* sanitized */ );
         }
         let prefix = customPrefix != undefined ? customPrefix : "master";
         let fullFileName = prefix + fileName + ( customPostfix || "" ) + ".xisf";
         // ensure file name uniqueness
         filePath = existingAndUniqueFileName( this.outputDirectory + "/master", fullFileName );

         console.noteln( "<end><cbr><br>* Writing master " + StackEngine.imageTypeToString( imageType ) + " frame:" );
         console.noteln( "<raw>" + filePath + "</raw>" );

         // extract the rejection map windows
         let rejectionLowWindow = null;
         let rejectionHighWindow = null;
         let slopeMapWindow = null;

         if ( II.generateRejectionMaps )
         {
            if ( II.clipLow )
               rejectionLowWindow = ImageWindow.windowById( II.lowRejectionMapImageId );
            if ( II.clipHigh && embedRejectionMaps )
               rejectionHighWindow = ImageWindow.windowById( II.highRejectionMapImageId );
            if ( II.rejection == ImageIntegration.prototype.LinearFit && embedRejectionMaps )
               slopeMapWindow = ImageWindow.windowById( II.slopeMapImageId );

            this.writeImage( filePath,
               [ window, rejectionLowWindow, rejectionHighWindow, slopeMapWindow ],
               [ "integration", "rejection_low", "rejection_high", "slope_map" ] );

            if ( rejectionLowWindow != null && !rejectionLowWindow.isNull )
               rejectionLowWindow.forceClose();
            if ( rejectionHighWindow != null && !rejectionHighWindow.isNull )
               rejectionHighWindow.forceClose();
            if ( slopeMapWindow != null && !slopeMapWindow.isNull )
               slopeMapWindow.forceClose();
         }
         else
         {
            this.writeImage( filePath, [ window ], [ "integration" ] );
         }

         window.forceClose();

         // store the cached data
         if ( File.exists( filePath ) )
         {
            console.writeln();
            engine.executionCache.setCache( IIcacheKey, filePath );
            engine.executionCache.cacheFileLMD( IIcacheKey, filePath );
            for ( let i = 0; i < activeFrames.length; i++ )
            {
               // cache the current input frames
               engine.executionCache.cacheFileLMD( IIcacheKey, activeFrames[ i ].current );
               if ( generateDrizzle )
                  engine.executionCache.cacheFileLMD( IIcacheKey, activeFrames[ i ].drizzleFile );
               if ( useLN )
                  engine.executionCache.cacheFileLMD( IIcacheKey, activeFrames[ i ].localNormalizationFile );

               // update the cache for the drizzle file since ImageIntegration modified it (registration will keep it cached)
               let lastLMD = drizzleFileLMD[ activeFrames[ i ].drizzleFile ];
               if ( lastLMD )
                  engine.executionCache.updateLMD( activeFrames[ i ].drizzleFile, lastLMD, getLastModifiedDate( activeFrames[ i ].drizzleFile ) );
            }
            console.writeln();
         }
      }
   }

   console.noteln( "<end><cbr><br>", SEPARATOR );
   console.noteln( "* End integration of " + StackEngine.imageTypeToString( imageType ) + " frames" );
   console.noteln( SEPARATOR );

   return {
      masterFilePath: filePath,
      cached: useCache
   };
};

// ----------------------------------------------------------------------------

/**
 * Performs the drizzle integration.
 *
 * @param {*} frameGroup
 * @param {*} scale
 * @param {*} shrink
 * @param {*} kernel
 * @param {*} customPrefix custom prefix to be added at the end of the master frame (default is "master")
 * @param {*} customPostfix custom postfix to be added at the end of the master frame
 * @param {*} desiredFileName
 * @param {*} overrideDIparameters
 * @param {*} FITSKeywords optional list of FITS keyword to inject into the integrated image before saving
 * @return {*}
 */
StackEngine.prototype.doDrizzleIntegration = function( frameGroup, scale, shrink, kernel, customPrefix, customPostfix, customGenerateDrizzle, desiredFileName, overrideIIparameters, FITSKeywords )
{
   let
   {
      copyAstrometricSolution,
      existingAndUniqueFileName,
      hasAstrometricSolution,
      hasSplineAstrometricSolutionMetadata
   } = WBPPUtils.shared();

   let filePath = "";
   let imageType = frameGroup.imageType;

   console.noteln( "<end><cbr><br>", SEPARATOR );
   console.noteln( "* Begin drizzle integration of ", StackEngine.imageTypeToString( imageType ) + " frames" );
   console.noteln( SEPARATOR );
   frameGroup.log();

   let useCache = false;

   let activeFrames = frameGroup.activeFrames().filter( f => ( f.drizzleFile != undefined && f.drizzleFile.length > 0 ) );
   if ( activeFrames.length < 3 )
   {
      console.warningln( "** Warning: Cannot apply drizzle integration to less than three frames." );
      this.processLogger.addWarning( "Cannot apply drizzle integration to less than three frames." );

      console.noteln( "active frames:\n" )
      console.noteln( JSON.stringify( activeFrames, null, 2 ) );
   }
   else
   {
      if ( activeFrames.length < 15 )
      {
         let msg = "** Warning: it is recommended to perform drizzle integration with a set of frames higher than 15 frames " +
            "(current is " + activeFrames.length + ")."
         console.warningln( "** Warning: " + msg );
         this.processLogger.addWarning( msg );
      }

      let useLN = activeFrames.reduce( ( acc, item ) => ( acc && ( item.localNormalizationFile != undefined ) ), true );

      let DI = new DrizzleIntegration;

      DI.inputData = activeFrames.map( f => ( [ true, f.drizzleFile, useLN ? f.localNormalizationFile : "" ] ) )
      DI.scale = scale;
      DI.dropShrink = shrink;
      DI.kernelFunction = kernel;
      DI.enableCFA = frameGroup.isCFA;

      // override the II parameters
      if ( overrideIIparameters )
         Object.keys( overrideIIparameters ).forEach( key =>
         {
            // must be a valid key
            if ( DI[ key ] != undefined )
               DI[ key ] = overrideIIparameters[ key ];
         } );

      /**
       * Check if valid cached data can be used
       */

      let DISource = DI.toSource( "JavaScript", "DI" /*varId*/ , 0 /*indent*/ ,
         SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();

      let DIcacheKey = engine.executionCache.keyFor( DISource );
      console.writeln();
      if ( engine.executionCache.hasCacheForKey( DIcacheKey ) )
      {
         console.noteln( "DrizzleIntegration has cached data for key ", DIcacheKey )
         // Drizzle Integration cache consists in the integrated image filePath
         let DICacheOutputFilePath = engine.executionCache.cacheForKey( DIcacheKey );

         useCache = DICacheOutputFilePath != undefined;

         // the cache is valid only if all input files and the integrated file are unchanged
         for ( let i = 0;
            ( i < activeFrames.length ) && useCache; i++ )
         {
            // source images must be unchanged
            if ( !engine.executionCache.isFileUnmodified( DIcacheKey, activeFrames[ i ].current ) )
               useCache = false;
            // if using the local normalization then the source images must be unchanged
            if ( useCache && useLN && !engine.executionCache.isFileUnmodified( DIcacheKey, activeFrames[ i ].localNormalizationFile ) )
               useCache = false;
         }

         // source images must be unchanged
         if ( useCache )
            useCache = engine.executionCache.isFileUnmodified( DIcacheKey, DICacheOutputFilePath )

         // determine if using cache or not
         if ( useCache )
         {
            filePath = DICacheOutputFilePath;
            console.noteln( "Drizzle Integration the cache is valid, skip the drizzle integration and use the cached result" );
         }
         else
            console.noteln( "Drizzle Integration the cache is not valid, proceed with the drizzle integration" );
      }
      else
      {
         console.noteln( "Drizzle Integration has no cache data for key ", DIcacheKey );
      }
      console.writeln();

      // PROCEED
      console.writeln( SEPARATOR2 );
      console.writeln( DISource );
      console.writeln( SEPARATOR2 );

      let ok = true;
      if ( useCache )
      {
         console.noteln( "** Using cached data for Drizzle Integration." );
         console.noteln( "<end><cbr><br>* master " + StackEngine.imageTypeToString( imageType ) + " frame:" );
         console.noteln( "<raw>" + filePath + "</raw>" );
      }
      else
      {
         ok = DI.executeGlobal();
      }

      engine.processContainer.add( DI );

      if ( !ok )
      {
         console.warningln( "** Warning: Drizzle Integration failed." );
         this.processLogger.addWarning( "Drizzle Integration failed." );
      }
      else if ( !useCache )
      {
         // Write master frame FITS keywords
         // Build the file name postfix

         let keywords = new Array;
         if ( FITSKeywords )
            for ( let i = 0; i < FITSKeywords.length; i++ )
               keywords.push( FITSKeywords[ i ] );

         keywords.push( new FITSKeyword( "COMMENT", "", "PixInsight image preprocessing pipeline" ) );
         keywords.push( new FITSKeyword( "COMMENT", "", "Master frame generated with " + TITLE + " v" + VERSION ) );

         keywords.push( new FITSKeyword( "IMAGETYP", StackEngine.imageTypeToMasterKeywordValue( imageType ), "Type of image" ) );

         keywords.push( new FITSKeyword( "XBINNING", format( "%d", frameGroup.binning ), "Binning factor, horizontal axis" ) );
         keywords.push( new FITSKeyword( "YBINNING", format( "%d", frameGroup.binning ), "Binning factor, vertical axis" ) );

         keywords.push( new FITSKeyword( "FILTER", frameGroup.filter, "Filter used when taking image" ) );

         keywords.push( new FITSKeyword( "EXPTIME", format( "%.2f", frameGroup.exposureTime ), "Exposure time in seconds" ) );

         // concatenate the image keywords filtering out the keywords already added that have to remain unique
         let uniqueKeywords = [ "IMAGETYP", "XBINNING", "YBINNING", "FILTER", "EXPTIME" ];
         let window = ImageWindow.windowById( DI.integrationImageId );
         let weightImage = ImageWindow.windowById( DI.weightImageId );
         if ( window )
            window.hide();
         if ( weightImage )
            weightImage.hide();
         window.keywords = keywords.concat( window.keywords.filter( k => uniqueKeywords.indexOf( k.name ) == -1 ) );

         // check if the first integrated frame contains an astrometric solution, in that case we transfer the solution to the
         // integrated image
         if ( engine.platesolve )
         {
            let referenceWindows = ImageWindow.open( activeFrames[ 0 ].current );
            if ( referenceWindows.length > 0 )
            {
               let referenceWindow = referenceWindows[ 0 ];

               try
               {
                  // propagate and optimize the astrometric solution for drizzle 1x
                  if ( hasAstrometricSolution( referenceWindow ) )
                  {
                     let propagateTheSolution = hasSplineAstrometricSolutionMetadata( referenceWindow ) && scale == 1;
                     if ( propagateTheSolution )
                     {
                        console.noteln( "Copying the astrometric solution from the first frame" );
                        // copy the solution to the integrated image
                        copyAstrometricSolution( referenceWindow, window );
                     }
                     else
                     {
                        console.noteln( "Propagating astrometric metadata with scaled resolution of ", scale, "x" );
                        // propagate the scaled WCS and perform the solution
                        let wcs = new ImageMetadata();
                        wcs.ExtractMetadata( referenceWindow );
                        wcs.resolution /= scale;
                        wcs.xpixsz /= scale;
                        wcs.SaveProperties( window );
                        wcs.UpdateBasicKeywords( window.keywords );
                     }

                     // plate solve again to adapt the solution to the drizzle scale
                     {
                        let solver = new ImageSolver;
                        InitializeSolver( solver, window );
                        resetImageSolverConfiguration( solver );
                        solver.solverCfg.onlyOptimize = propagateTheSolution;
                        SolveImage( solver, window );

                        console.noteln( window.astrometricSolutionSummary() );
                     }
                  }
                  else
                     console.noteln( "No astrometric solution found in the first frame" );
               }
               catch ( e )
               {
                  console.warningln( e );
                  console.noteln( "issue occurred while handling the astrometric solution, no astrometric solution will be generated." );
               }
               referenceWindow.forceClose();
            }
         }

         // for masterFlat if overscan is enabled we temporarily set the group size to the overscan region to generate the proper master file
         let fileName = desiredFileName || frameGroup.folderName( false /* sanitized */ );
         let prefix = customPrefix != undefined ? customPrefix : "master";
         let fullFileName = prefix + fileName + ( customPostfix || "" ) + ".xisf";
         // ensure file name uniqueness
         filePath = existingAndUniqueFileName( this.outputDirectory + "/master", fullFileName );

         console.noteln( "<end><cbr><br>* Writing master " + StackEngine.imageTypeToString( imageType ) + " frame:" );
         console.noteln( "<raw>" + filePath + "</raw>" );

         this.writeImage( filePath, [ window, weightImage ], [ "integration", "weightImage" ] );

         window.forceClose();
         if ( weightImage )
            weightImage.forceClose();

         // store the cached data
         if ( File.exists( filePath ) )
         {
            console.writeln();
            engine.executionCache.setCache( DIcacheKey, filePath );
            engine.executionCache.cacheFileLMD( DIcacheKey, filePath );
            for ( let i = 0; i < activeFrames.length; i++ )
            {
               engine.executionCache.cacheFileLMD( DIcacheKey, activeFrames[ i ].current );
               if ( useLN )
               {
                  engine.executionCache.cacheFileLMD( DIcacheKey, activeFrames[ i ].localNormalizationFile );
               }
            }
            console.writeln();
         }
      }
   }

   console.noteln( "<end><cbr><br>", SEPARATOR );
   console.noteln( "* End drizzle integration of " + StackEngine.imageTypeToString( imageType ) + " frames" );
   console.noteln( SEPARATOR );

   return {
      masterFilePath: filePath,
      cached: useCache
   };
};

// ----------------------------------------------------------------------------

/**
 * Calibrate the provided frame group.
 *
 * @param {*} frameGroup
 * @returns
 */
StackEngine.prototype.doCalibrate = function( frameGroup )
{
   let
   {
      enableTargetFrames,
      existingDirectory,
      isEmptyString
   } = WBPPUtils.shared();

   console.noteln( "<end><cbr><br>", SEPARATOR );
   console.noteln( "* Begin calibration of " + StackEngine.imageTypeToString( frameGroup.imageType ) + " frames" );
   console.noteln( SEPARATOR );

   frameGroup.log();

   let activeFrames = frameGroup.activeFrames();
   let cg = this.getCalibrationGroupsFor( frameGroup );

   // -------------------------------
   // get the matching MASTER BIAS
   // -------------------------------
   let masterBias = cg.masterBias;
   let masterBiasPath = masterBias ? masterBias.fileItems[ 0 ].filePath : "";
   let masterBiasEnabled = !isEmptyString( masterBiasPath );

   // -------------------------------
   // get the matching MASTER DARK
   // -------------------------------
   let masterDark = cg.masterDark;
   let masterDarkPath = masterDark ? masterDark.fileItems[ 0 ].filePath : "";
   let masterDarkEnabled = !isEmptyString( masterDarkPath );

   if ( !frameGroup.forceNoDark )
   {
      if ( frameGroup.overrideDark )
      {
         this.processLogger.addMessage( 'Master Dark manually assigned.' )
      }
      else
      {
         console.noteln( "Master Dark automatic match" );
         this.processLogger.addMessage( 'Master Dark automatic match.' );
      }
   }
   else
   {
      this.processLogger.addMessage( 'Master Dark manually disabled.' );
   }

   // -------------------------------
   // get the matching MASTER FLAT
   // -------------------------------
   // flats are enabled only when calibrating light frames
   let masterFlat = cg.masterFlat;
   let masterFlatPath = masterFlat ? masterFlat.fileItems[ 0 ].filePath : "";
   let masterFlatEnabled = !isEmptyString( masterFlatPath );

   if ( !frameGroup.forceNoFlat )
   {
      if ( frameGroup.overrideFlat )
      {
         console.noteln( "Master Flat manually assigned" );
         this.processLogger.addMessage( 'Master Flat manually assigned.' );
      }
      else if ( frameGroup.imageType == ImageType.LIGHT )
      {

         console.noteln( "Master Flat automatic match" );
         this.processLogger.addMessage( 'Master Flat automatic match.' );
      }
   }
   else
   {
      console.noteln( "Master Flat manually disabled" );
      this.processLogger.addMessage( 'Master Flat manually disabled.' );
   }

   // LOG
   this.processLogger.addMessage( '<ul>' );
   if ( masterBiasEnabled )
   {
      console.noteln( "Master bias: " + masterBiasPath );
      this.processLogger.addMessage( "<li>Master bias: " + masterBiasPath + '</li>' );
   }
   else
   {
      console.noteln( "Master bias: none" );
      this.processLogger.addMessage( "<li>Master bias: none</li>" );
   }

   if ( masterDarkEnabled )
   {
      console.noteln( "* Master dark: " + masterDarkPath );
      this.processLogger.addMessage( "<li>Master dark: " + masterDarkPath + '</li>' );
   }
   else
   {
      console.noteln( " Master dark: none" );
      this.processLogger.addMessage( "<li>Master dark: none</li>" );
   }

   if ( masterFlatEnabled )
   {
      console.noteln( "* Master flat: " + masterFlatPath );
      this.processLogger.addMessage( "<li>Master flat: " + masterFlatPath + '</li>' );
   }
   else
   {
      console.noteln( " Master flat: none" );
      this.processLogger.addMessage( "<li>Master flat: none</li>" );
   }

   this.processLogger.addMessage( '<ul>' );

   if ( !engine.overscan.enabled && !masterBiasEnabled && !masterDarkEnabled && !masterFlatEnabled )
   {
      console.warningln( " ** Warning: Image Calibration skipped for " + StackEngine.imageTypeToString( frameGroup.imageType ) + " of duration " + frameGroup.exposureTime + 's.' );
      this.processLogger.addWarning( "Image Calibration skipped for " + StackEngine.imageTypeToString( frameGroup.imageType ) + " of duration " + frameGroup.exposureTime + 's.' );
      console.noteln( "<end><cbr><br>", SEPARATOR );
      console.noteln( "* End calibration of " + StackEngine.imageTypeToString( frameGroup.imageType ) + " frames" );
      console.noteln( SEPARATOR );
      return undefined; /* mark the calibration skipped by returning undefined */
   }

   if ( frameGroup.optimizeMasterDark )
      this.processLogger.addMessage( "Master Dark is optimized." );

   // PREPARE CALIBRATION
   let pedestalMode = function( mode )
   {
      if ( mode >= 0 && mode < WBPPPedestalModeIC.length )
         return WBPPPedestalModeIC[ mode ];
      return ImageCalibration.prototype.OutputPedestal_Auto;
   };

   let IC = new ImageCalibration;

   IC.enableCFA = frameGroup.isCFA
   if ( frameGroup.isCFA )
      IC.cfaPattern = frameGroup.CFAPattern; // ### N.B. Debayer and IC define compatible enumerated parameters for CFA patterns
   IC.inputHints = this.inputHints();
   IC.outputHints = this.outputHints();
   IC.masterBiasEnabled = false;
   IC.masterDarkEnabled = false;
   IC.masterFlatEnabled = false;
   IC.calibrateBias = true; // relevant if we define overscan areas
   IC.calibrateDark = this.overscan.enabled || masterBiasEnabled; // compatibility with pre-calibrated master dark has been removed
   IC.calibrateFlat = false; // assume we have calibrated each individual flat frame
   IC.optimizeDarks = frameGroup.optimizeMasterDark;
   IC.darkOptimizationLow = this.darkOptimizationLow;
   IC.darkOptimizationWindow = this.darkOptimizationWindow;
   IC.separateCFAFlatScalingFactors = masterFlat ? masterFlat.separateCFAFlatScalingFactors : false;
   IC.flatScaleClippingFactor = 0.05;
   IC.outputExtension = ".xisf";
   IC.outputPrefix = "";
   IC.outputPostfix = "_c";
   // N.B. For CFAs, evaluate noise and signal with Debayer instead of ImageCalibration
   IC.evaluateNoise = IC.evaluateSignal = frameGroup.imageType == ImageType.LIGHT && !frameGroup.isCFA;
   IC.outputSampleFormat = ImageCalibration.prototype.f32;
   IC.overwriteExistingFiles = false;
   IC.onError = ImageCalibration.prototype.Continue;

   if ( frameGroup.imageType == ImageType.LIGHT )
   {
      let lightOutputPedestalLogMessage;
      if ( frameGroup.lightOutputPedestalMode == WBPPPedestalMode.AUTO )
         lightOutputPedestalLogMessage = format( "Light Output Pedestal: auto" );
      else
         lightOutputPedestalLogMessage = format( "Light Output Pedestal: %.0f", frameGroup.lightOutputPedestal );
      this.processLogger.addMessage( lightOutputPedestalLogMessage );
      console.noteln( lightOutputPedestalLogMessage );
      IC.outputPedestal = frameGroup.lightOutputPedestal;
      IC.outputPedestalMode = pedestalMode( frameGroup.lightOutputPedestalMode );
      IC.autoPedestalLimit = frameGroup.lightOutputPedestalLimit;
   }

   if ( this.overscan.enabled )
   {
      IC.overscanEnabled = true;
      IC.overscanImageX0 = this.overscan.imageRect.x0;
      IC.overscanImageY0 = this.overscan.imageRect.y0;
      IC.overscanImageX1 = this.overscan.imageRect.x1;
      IC.overscanImageY1 = this.overscan.imageRect.y1;
      IC.overscanRegions = [ // enabled, sourceX0, sourceY0, sourceX1, sourceY1, targetX0, targetY0, targetX1, targetY1
         [ false, 0, 0, 0, 0, 0, 0, 0, 0 ],
         [ false, 0, 0, 0, 0, 0, 0, 0, 0 ],
         [ false, 0, 0, 0, 0, 0, 0, 0, 0 ],
         [ false, 0, 0, 0, 0, 0, 0, 0, 0 ]
      ];

      for ( let i = 0; i < 4; ++i )
         if ( this.overscan.overscan[ i ].enabled )
         {
            let M = IC.overscanRegions;
            M[ i ] = [
               true,
               this.overscan.overscan[ i ].sourceRect.x0,
               this.overscan.overscan[ i ].sourceRect.y0,
               this.overscan.overscan[ i ].sourceRect.x1,
               this.overscan.overscan[ i ].sourceRect.y1,
               this.overscan.overscan[ i ].targetRect.x0,
               this.overscan.overscan[ i ].targetRect.y0,
               this.overscan.overscan[ i ].targetRect.x1,
               this.overscan.overscan[ i ].targetRect.y1
            ];
            IC.overscanRegions = M;
         }
   }

   // Set master files
   IC.masterBiasEnabled = masterBiasEnabled;
   IC.masterBiasPath = masterBiasPath

   IC.masterDarkEnabled = masterDarkEnabled;
   IC.masterDarkPath = masterDarkPath;

   IC.masterFlatEnabled = masterFlatEnabled;
   IC.masterFlatPath = masterFlatPath;

   // Set output directories
   let subfolder = frameGroup.folderName();
   IC.outputDirectory = existingDirectory( this.outputDirectory + "/calibrated/" + subfolder );

   let calibratedFiles = [];
   let ICSource = IC.toSource( "JavaScript", "IC" /*varId*/ , 0 /*indent*/ ,
      SourceCodeFlag_NoTimeInfo | SourceCodeFlag_NoReadOnlyParams | SourceCodeFlag_NoDescription ).trim();
   console.writeln( SEPARATOR2 );
   console.writeln( ICSource );
   console.writeln( SEPARATOR2 );

   /*
    * Check if valid cached data can be used. Cache must exist for the same Image Calibration configuration and bias, dark and flat masters
    * must be unchanged (if provided)
    */
   let inputFiles = activeFrames.map( item => item.current );
   let filesToCalibrate = inputFiles;

   let cached = {};
   let ICCache = {};
   let ICcacheKey = engine.executionCache.keyFor( ICSource );
   if ( engine.executionCache.hasCacheForKey( ICcacheKey ) )
   {
      console.noteln( "Image Calibration has cached data for key ", ICcacheKey );

      // the precondition for using the cache is that the calibratino masters have not changed
      let useCache = true;
      if ( masterBiasEnabled && !engine.executionCache.isFileUnmodified( ICcacheKey, masterBiasPath ) )
      {
         console.noteln( "Image Calibration master bias has changed, recalibrate all frames" );
         useCache = false;
      }
      if ( masterDarkEnabled && !engine.executionCache.isFileUnmodified( ICcacheKey, masterDarkPath ) )
      {
         console.noteln( "Image Calibration master dark has changed, recalibrate all frames" );
         useCache = false;
      }
      if ( masterFlatEnabled && !engine.executionCache.isFileUnmodified( ICcacheKey, masterFlatPath ) )
      {
         console.noteln( "Image Calibration master flat has changed, recalibrate all frames" );
         useCache = false;
      }

      if ( useCache )
      {
         ICCache = engine.executionCache.cacheForKey( ICcacheKey );
         filesToCalibrate = [];

         for ( let i = 0; i < inputFiles.length; i++ )
         {
            let inputFile = inputFiles[ i ];
            let outputFile = ICCache[ inputFile ];

            if ( outputFile != undefined &&
               engine.executionCache.isFileUnmodified( ICcacheKey, inputFile ) &&
               engine.executionCache.isFileUnmodified( ICcacheKey, outputFile ) )
            {
               cached[ inputFile ] = outputFile;
               console.noteln( "Image Calibration will use cache for file: ", File.extractNameAndExtension( inputFile ) );
            }
            else
            {
               console.noteln( "Image Calibration will calibrate: ", File.extractNameAndExtension( inputFile ) );
               filesToCalibrate.push( inputFile );
            }
         }
      }
   }
   else
      console.noteln( "Image Calibration has no cached data for key ", ICcacheKey );

   // in process container we store the full calibrated files
   IC.targetFrames = enableTargetFrames( inputFiles, 2 );
   engine.processContainer.add( IC );

   // set the files to be calibrated and proceed
   let success = true;
   if ( filesToCalibrate.length > 0 )
   {
      IC.targetFrames = enableTargetFrames( filesToCalibrate, 2 );
      success = IC.executeGlobal();
   }
   let nCached = 0;
   let nGenerated = 0;
   let nFailed = 0;

   // iterate through all input files and store cached data or the new generated ones
   let j = 0;
   for ( let i = 0; i < inputFiles.length; i++ )
   {
      let inputFile = inputFiles[ i ];
      if ( cached[ inputFile ] != undefined )
      {
         calibratedFiles.push( cached[ inputFile ] );
         nCached++;
      }
      else if ( success )
      {
         let outputFile = IC.outputData[ j++ ][ 0 ];

         console.noteln( "IC outputFile: [" + outputFile + "]" );

         if ( outputFile != undefined && outputFile.length > 0 )
         {
            if ( File.exists( outputFile ) )
            {
               calibratedFiles.push( outputFile );

               // update the LMD for the new generated files
               ICCache[ inputFile ] = outputFile;
               engine.executionCache.cacheFileLMD( ICcacheKey, inputFile );
               engine.executionCache.cacheFileLMD( ICcacheKey, outputFile );
               nGenerated++;
            }
            else
            {
               calibratedFiles.push( undefined );
               nFailed++;
            }
         }
         else
         {
            calibratedFiles.push( undefined );
            nFailed++;
         }

         console.noteln( "IC outputFile checked" );
      }
      else
      {
         calibratedFiles.push( undefined );
         nFailed++;
      }
   }
   // update the used master files LMD
   if ( masterBiasEnabled )
      engine.executionCache.cacheFileLMD( ICcacheKey, masterBiasPath );
   if ( masterDarkEnabled )
      engine.executionCache.cacheFileLMD( ICcacheKey, masterDarkPath );
   if ( masterFlatEnabled )
      engine.executionCache.cacheFileLMD( ICcacheKey, masterFlatPath );
   engine.executionCache.setCache( ICcacheKey, ICCache );
   console.noteln( nCached, " cached, ", nGenerated, " generated, ", nFailed, " failed." );

   processEvents();
   gc();

   console.noteln( "<end><cbr><br>", SEPARATOR );
   console.noteln( "* End calibration of " + StackEngine.imageTypeToString( frameGroup.imageType ) + " frames" );
   console.noteln( SEPARATOR );

   return {
      calibratedFiles: calibratedFiles,
      nCached: nCached,
      nGenerated: nGenerated,
      nFailed: nFailed
   };
};

// ----------------------------------------------------------------------------

StackEngine.prototype.combineRGB = function( RfilePath, GfilePath, BfilePath, fileName )
{
   let
   {
      existingAndUniqueFileName
   } = WBPPUtils.shared();

   let filePath = this.outputDirectory + "/master/" + fileName;

   // Check if cached data exists
   let idString = "CombinedRBG_" + RfilePath + "_" + GfilePath + "_" + BfilePath;
   let CRGBcacheKey = engine.executionCache.keyFor( idString );
   let CRBGCache = {};
   if ( engine.executionCache.hasCacheForKey( CRGBcacheKey ) )
   {

      CRBGCache = engine.executionCache.cacheForKey( CRGBcacheKey );
      if ( engine.executionCache.isFileUnmodified( CRGBcacheKey, RfilePath ) &&
         engine.executionCache.isFileUnmodified( CRGBcacheKey, GfilePath ) &&
         engine.executionCache.isFileUnmodified( CRGBcacheKey, BfilePath ) &&
         CRBGCache.outputFName != undefined && engine.executionCache.isFileUnmodified( CRGBcacheKey, CRBGCache.outputFName ) )
      {
         console.noteln( "RGB Combination success with cached data." );
         return {
            success: true,
            filePath: CRBGCache.outputFName,
            cached: true
         };
      }
   }

   // no cached data, create a unique file name
   filePath = existingAndUniqueFileName( this.outputDirectory + "/master", fileName );

   // load the imagaes
   let loopData = [
   {
      c: "R",
      p: RfilePath
   },
   {
      c: "G",
      p: GfilePath
   },
   {
      c: "B",
      p: BfilePath
   } ];
   let windows = [];
   for ( let i = 0; i < 3; i++ )
   {
      let w = ImageWindow.open( loopData[ i ].p );
      if ( w.length == 0 )
      {
         for ( j = 0; j, windows.length; j++ )
            windows[ j ].forceClose();

         return {
            error: "Unable to load " + loopData[ i ].c + " channel image at path " + loopData[ i ].p,
            success: false
         }
      }
      else
         windows.push( w[ 0 ] );
   }

   // run channel combination
   var CC = new ChannelCombination;
   CC.colorSpace = ChannelCombination.prototype.RGB;
   CC.channels = [ // enabled, id
      [ true, windows[ 0 ].mainView.id ],
      [ true, windows[ 1 ].mainView.id ],
      [ true, windows[ 2 ].mainView.id ]
   ];

   let res = CC.executeGlobal();
   engine.processContainer.add( CC );

   if ( !res )
   {
      res = {
         error: "Channel combination failed.",
         success: false
      };
   }
   else
   {
      let rgbWindow = ImageWindow.activeWindow;

      // inject all keywords from R image to the combined image
      rgbWindow.keywords = windows[ 0 ].keywords;

      // save the file
      this.writeImage( filePath, [ rgbWindow ], [ "RGB_combination" ] );

      rgbWindow.forceClose();

      // check the result
      if ( !File.exists( filePath ) )
         res = {
            error: "Error savning the combined RGB file.",
            success: false
         };
      else
      {
         CRBGCache.outputFName = filePath;
         engine.executionCache.setCache( CRGBcacheKey, CRBGCache );
         engine.executionCache.cacheFileLMD( CRGBcacheKey, RfilePath );
         engine.executionCache.cacheFileLMD( CRGBcacheKey, GfilePath );
         engine.executionCache.cacheFileLMD( CRGBcacheKey, BfilePath );
         engine.executionCache.cacheFileLMD( CRGBcacheKey, filePath );
         res = {
            success: true,
            filePath: filePath,
            cached: false
         };
      }
   }

   // close R,G,B images
   windows[ 0 ].forceClose();
   windows[ 1 ].forceClose();
   windows[ 2 ].forceClose();

   return res;
};

// ----------------------------------------------------------------------------

/**
 * Cleans up elements that has been deallocated but that are still in the groups
 * or file items lists.
 *
 */
StackEngine.prototype.removePurgedElements = function()
{
   let groups = this.groupsManager.groups;
   for ( let i = groups.length; --i >= 0; )
   {
      if ( !groups[ i ] || groups[ i ].__purged__ )
         this.groupsManager.removeGroupAtIndex( i );
      else
      {
         for ( let j = groups[ i ].fileItems.length; --j >= 0; )
            if ( !groups[ i ].fileItems[ j ] || groups[ i ].fileItems[ j ].__purged__ )
               groups[ i ].removeItem( j );
         if ( groups[ i ].fileItems.length == 0 )
            this.groupsManager.removeGroupAtIndex( i );
      }
   }
};

// ----------------------------------------------------------------------------

/**
 * Encode the JSON string of the list of groups.
 *
 * @returns
 */
StackEngine.prototype.groupsToStringData = function()
{
   // from version 2.1.3 manual groups matching overrides references
   // are replaced by the group ID before saving and restored once reloaded
   this.groupsManager.groups.forEach( ( group ) =>
   {
      if ( group.overrideDark && group.overrideDark.id )
         group.overrideDark = group.overrideDark.id;
      if ( group.overrideFlat && group.overrideFlat.id )
         group.overrideFlat = group.overrideFlat.id;
   } );
   let stringData = JSON.stringify( this.groupsManager.groups, null, 2 );
   this.relinkManualOverrides();
   // save files structure
   return stringData;
};

// ----------------------------------------------------------------------------

/**
 * Decode the list of groups from a JSON string.
 *
 * @param {*} data
 */
StackEngine.prototype.groupsFromStringData = function( data, version )
{
   try
   {
      let groupsData = JSON.parse( data );

      // save files structure
      if ( groupsData )
      {
         this.removePurgedElements();
         this.migrateGroupsData( groupsData, version );
         this.relinkManualOverrides();
         this.reconstructGroups();
      }
   }
   catch ( e )
   {
      console.noteln( "Error occurred while loading saved groups. Group list will be cleared." );
      this.groupsManager.clear();
   }
};

// ----------------------------------------------------------------------------

/**
 * Migrates old data versions to the current version
 *
 * @param {*} groupsData groups data to be migrated
 */
StackEngine.prototype.migrateGroupsData = function( groupsData, version )
{
   let
   {
      versionLT
   } = WBPPUtils.shared();

   if ( version == undefined || versionLT( version, "2.5.0" ) )
   {
      // braking change: group ID changed (fixed)
      console.noteln( "WBPP v2.5.0 group data is not compatible with earlier versions (", version, "). All group properties will be reset to default values." );

      let fileItems = [];
      groupsData.forEach( group =>
      {
         if ( group.mode == WBPPGroupingMode.PRE )
            for ( let j = 0; j < group.fileItems.length; j++ )
               fileItems.push( group.fileItems[ j ] );
      } );

      this.groupsManager.clear();
      this.groupsManager.clearCache();

      // re-add files one by one
      console.show();
      for ( let i = 0; i < fileItems.length; ++i )
      {
         // show progressing
         console.noteln( "reading [", i, "/", fileItems.length, "] ", fileItems[ i ].filePath );
         this.addFile( fileItems[ i ].filePath, fileItems[ i ].imageType );
      }
      console.hide();
   }

   this.groupsManager.groups = groupsData;
};

/**
 * Reconstruct the links between the groups that has been manually assigned.
 * When groups are saved, the links are replaced by the the group id string,
 * once reloaded these strings must be replaced by the link to the group.
 *
 * This function must be called when groups has been loaded, migrated and
 * finally assigned to the groups manager.
 */
StackEngine.prototype.relinkManualOverrides = function()
{
   this.groupsManager.groups.forEach( ( group ) =>
   {
      if ( typeof group.overrideDark == "string" )
         group.overrideDark = this.groupsManager.getGroupByID( group.overrideDark );
      if ( typeof group.overrideFlat == "string" )
         group.overrideFlat = this.groupsManager.getGroupByID( group.overrideFlat );
   } );
};

// ----------------------------------------------------------------------------

StackEngine.prototype.migrateKeywords = function( keywords, version )
{
   let migrated = keywords;

   // ---------------------------
   // n okeywords are suppored before version [2.0.2]
   if ( version == undefined )
   {
      return [];
   }

   return keywords;
};

// ----------------------------------------------------------------------------

StackEngine.prototype.migrateFrom = function( dataVersion )
{
   if ( dataVersion == undefined || typeof dataVersion != typeof "" )
      dataVersion = "0.0.0";

   let
   {
      versionLT
   } = WBPPUtils.shared();

   // migration versions
   let migrationVersions = [
   {
      version: "2.4.0", // migrate from versions < 2.4.0
      migration: () =>
      {
         // PSF power has been replaced by PSFSNR, set it to PSFSignal in case
         if ( this.subframesWeightsMethod == undefined || this.subframesWeightsMethod == WBPPSubframeWeightsMethod.PSFSNR )
         {
            if ( this.subframesWeightsMethod )
               console.writeln( "* Weighting method PSF Power Weight has been removed and will be replaced by PSF Signal" );
            this.subframesWeightsMethod = WBPPSubframeWeightsMethod.PSFSignal;
         }
         // Rejection methods list has been shortened, map unavailable rejection methods to auto
         for ( let i = 0; i < 4; i++ )
         {
            // index correspond to auto if the rejection method is not listed
            let index = this.rejectionIndex( this.rejection[ i ] )
            this.rejection[ i ] = this.rejectionFromIndex( index )
         }
      }
   },
   {
      version: "2.4.1", // migrate from versions < 2.4.1
      migration: () =>
      {
         // PSFScaleSNR has been introduced after PSFSNR, update the old enumeration
         if ( this.subframesWeightsMethod == undefined || this.subframesWeightsMethod > WBPPSubframeWeightsMethod.PSFSNR )
            this.subframesWeightsMethod = this.subframesWeightsMethod + 1;
      }
   },
   {
      version: "2.4.3", // migrate from versions < 2.4.3
      migration: () =>
      {
         // New StarDetector engine V2 in core 1.8.9-1: reset star detection parameters to default values.
         this.sensitivity = DEFAULT_SA_SENSITIVITY;
         this.peakResponse = DEFAULT_SA_PEAK_RESPONSE;
         this.maxStarDistortion = DEFAULT_SA_MAX_STAR_DISTORTION;
      }
   },
   {
      version: "2.5.0", // migrate from versions < 2.5.0
      migration: () =>
      {
         // fix the issue with the LN max stars out of range (set the default value if OOR)
         if ( this.localNormalizationPsfMaxStars > DEFAULT_LOCALNORMALIZATION_PSF_MAX_STARS ||
            this.localNormalizationPsfMaxStars < DEFAULT_LOCALNORMALIZATION_PSF_MIN_STARS )
            this.localNormalizationPsfMaxStars = DEFAULT_LOCALNORMALIZATION_PSF_MAX_STARS;
         // we always clear the cache when new version is installed
         this.executionCache.reset();
      }
   } ];

   // perform the migrations
   for ( let i = 0; i < migrationVersions.length; i++ )
      if ( versionLT( dataVersion, migrationVersions[ i ].version ) )
      {
         console.noteln( "* Migrating data from WBPP v", dataVersion, " to v", migrationVersions[ i ].version );
         // perform the migration
         migrationVersions[ i ].migration();
         dataVersion = migrationVersions[ i ].version;
      }
};

// ----------------------------------------------------------------------------
/**
 * Saves the current running configuratoin.
 *
 */
StackEngine.prototype.saveRunningConfiguration = function()
{
   // temporary save the parameters
   if ( !this.automationMode )
   {
      let JSON = this.exportParameters( true /* toJSON*/ );
      Settings.write( SETTINGS_KEY_BASE + "runningConfiguration", DataType_UCString, JSON );
      Settings.write( SETTINGS_KEY_BASE + "runningConfiguration_VERSION", DataType_UCString, VERSION );
   }
}

/**
 * Checks if a running configuration is available. The presence of this information measnt hat WBPP has most probably crashed or PI has been
 * terminated during the previous execution.
 *
 */
StackEngine.prototype.hasRunningConfiguration = function()
{
   let hasConfiguration = Settings.read( SETTINGS_KEY_BASE + "runningConfiguration", DataType_UCString );
   let version = Settings.read( SETTINGS_KEY_BASE + "runningConfiguration_VERSION", DataType_UCString );
   // only configurations from the current version will be restored
   if ( version !== VERSION )
   {
      this.removeRunningConfiguration();
      return false;
   }
   return hasConfiguration != undefined;
}

/**
 * Restores the saved running configuration. This operation will update all paremeters, rebuild the engine and removes the saved
 * configuraiton.
 *
 */
StackEngine.prototype.restoreRunningConfiguration = function()
{
   if ( !this.hasRunningConfiguration() )
      return;

   let JSON = Settings.read( SETTINGS_KEY_BASE + "runningConfiguration", DataType_UCString );
   if ( JSON )
   {
      this.importParameters( JSON );
      engine.rebuild();
   }
   this.removeRunningConfiguration();
}

/**
 * Removes the running configuration.
 *
 */
StackEngine.prototype.removeRunningConfiguration = function()
{
   Settings.remove( SETTINGS_KEY_BASE + "runningConfiguration" );
   Settings.remove( SETTINGS_KEY_BASE + "runningConfiguration_VERSION" );
}


/**
 * Load the persisted WBPP settings.
 *
 * @returns
 */
StackEngine.prototype.loadSettings = function()
{
   let
   {
      elapsedTimeToString,
      versionLT
   } = WBPPUtils.shared();

   function load( key, type )
   {
      return Settings.read( SETTINGS_KEY_BASE + key, type );
   }

   function loadIndexed( key, index, type )
   {
      return load( key + '_' + index.toString(), type );
   }

   let o;

   let dataVersion = undefined;
   if ( ( o = load( "VERSION", DataType_String ) ) != null )
      dataVersion = o;

   if ( ( o = load( "outputDirectory", DataType_String ) ) != null )
   {
      // from version 2.50 and above we use base64 representation to handle non utf8 characters
      if ( dataVersion == undefined || versionLT( dataVersion, "2.5.0" ) )
         this.outputDirectory = o;
      else
      {
         try
         {
            this.outputDirectory = ByteArray.fromBase64( o ).toString();
         }
         catch ( e )
         {
            this.outputDirectory = "";
         }
      }
   }

   if ( ( o = load( "saveFrameGroups", DataType_Boolean ) ) != null )
      this.saveFrameGroups = o;
   if ( ( o = load( "smartNamingOverride", DataType_Boolean ) ) != null )
      this.smartNamingOverride = o;
   if ( ( o = load( "fitsCoordinateConvention", DataType_Int32 ) ) != null )
      this.fitsCoordinateConvention = o;
   if ( ( o = load( "detectMasterIncludingFullPath", DataType_Boolean ) ) != null )
      this.detectMasterIncludingFullPath = o;
   if ( ( o = load( "detectMasterIncludingFullPath", DataType_Boolean ) ) != null )
      this.detectMasterIncludingFullPath = o;
   if ( ( o = load( "generateRejectionMaps", DataType_Boolean ) ) != null )
      this.generateRejectionMaps = o;
   if ( ( o = load( "preserveWhiteBalance", DataType_Boolean ) ) != null )
      this.preserveWhiteBalance = o;
   if ( ( o = load( "groupingKeywordsEnabled", DataType_Boolean ) ) != null )
      this.groupingKeywordsEnabled = o;
   if ( ( o = load( "showAstrometricInfo", DataType_Boolean ) ) != null )
      this.showAstrometricInfo = o;

   if ( ( o = load( "darkOptimizationThreshold", DataType_Float ) ) != null )
      this.darkOptimizationThreshold = o;
   if ( ( o = load( "darkOptimizationLow", DataType_Float ) ) != null )
      this.darkOptimizationLow = o;
   if ( ( o = load( "darkExposureTolerance", DataType_Float ) ) != null )
      this.darkExposureTolerance = o;
   if ( ( o = load( "lightExposureTolerance", DataType_Float ) ) != null )
      this.lightExposureTolerance = o;
   if ( ( o = load( "lightExposureTolerancePost", DataType_Float ) ) != null )
      this.lightExposureTolerancePost = o;

   if ( ( o = load( "overscanEnabled", DataType_Boolean ) ) != null )
      this.overscan.enabled = o;
   for ( let i = 0; i < 4; ++i )
   {
      if ( ( o = loadIndexed( "overscanRegionEnabled", i, DataType_Boolean ) ) != null )
         this.overscan.overscan[ i ].enabled = o;
      if ( ( o = loadIndexed( "overscanSourceX0", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].sourceRect.x0 = o;
      if ( ( o = loadIndexed( "overscanSourceY0", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].sourceRect.y0 = o;
      if ( ( o = loadIndexed( "overscanSourceX1", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].sourceRect.x1 = o;
      if ( ( o = loadIndexed( "overscanSourceY1", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].sourceRect.y1 = o;
      if ( ( o = loadIndexed( "overscanTargetX0", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].targetRect.x0 = o;
      if ( ( o = loadIndexed( "overscanTargetY0", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].targetRect.y0 = o;
      if ( ( o = loadIndexed( "overscanTargetX1", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].targetRect.x1 = o;
      if ( ( o = loadIndexed( "overscanTargetY1", i, DataType_Int32 ) ) != null )
         this.overscan.overscan[ i ].targetRect.y1 = o;
   }
   if ( ( o = load( "overscanImageX0", DataType_Int32 ) ) != null )
      this.overscan.imageRect.x0 = o;
   if ( ( o = load( "overscanImageY0", DataType_Int32 ) ) != null )
      this.overscan.imageRect.y0 = o;
   if ( ( o = load( "overscanImageX1", DataType_Int32 ) ) != null )
      this.overscan.imageRect.x1 = o;
   if ( ( o = load( "overscanImageY1", DataType_Int32 ) ) != null )
      this.overscan.imageRect.y1 = o;

   if ( ( o = load( "minWeight", DataType_Float ) ) != null )
      this.minWeight = o;

   for ( let i = 0; i < 4; ++i )
   {
      if ( ( o = loadIndexed( "combination", i, DataType_Int32 ) ) != null )
         this.combination[ i ] = o;
      if ( ( o = loadIndexed( "rejection", i, DataType_Int32 ) ) != null )
         this.rejection[ i ] = o;
      // compatibility from PI 1.8.7 and above
      if ( this.rejection[ i ] == ImageIntegration.CCDClip )
         this.rejection[ i ] = ImageIntegration.auto;
      if ( ( o = loadIndexed( "percentileLow", i, DataType_Float ) ) != null )
         this.percentileLow[ i ] = o;
      if ( ( o = loadIndexed( "percentileHigh", i, DataType_Float ) ) != null )
         this.percentileHigh[ i ] = o;
      if ( ( o = loadIndexed( "sigmaLow", i, DataType_Float ) ) != null )
         this.sigmaLow[ i ] = o;
      if ( ( o = loadIndexed( "sigmaHigh", i, DataType_Float ) ) != null )
         this.sigmaHigh[ i ] = o;
      if ( ( o = loadIndexed( "linearFitLow", i, DataType_Float ) ) != null )
         this.linearFitLow[ i ] = o;
      if ( ( o = loadIndexed( "linearFitHigh", i, DataType_Float ) ) != null )
         this.linearFitHigh[ i ] = o;
      if ( ( o = loadIndexed( "ESD_Outliers", i, DataType_Float ) ) != null )
         this.ESD_Outliers[ i ] = o;
      if ( ( o = loadIndexed( "ESD_Significance", i, DataType_Float ) ) != null )
         this.ESD_Significance[ i ] = o;
      if ( ( o = loadIndexed( "RCR_Limit", i, DataType_Float ) ) != null )
         this.RCR_Limit[ i ] = o;
   }

   if ( ( o = load( "flatsLargeScaleRejection", DataType_Boolean ) ) != null )
      this.flatsLargeScaleRejection = o;
   if ( ( o = load( "flatsLargeScaleRejectionLayers", DataType_Int32 ) ) != null )
      this.flatsLargeScaleRejectionLayers = o;
   if ( ( o = load( "flatsLargeScaleRejectionGrowth", DataType_Int32 ) ) != null )
      this.flatsLargeScaleRejectionGrowth = o;
   if ( ( o = load( "lightsLargeScaleRejectionHigh", DataType_Boolean ) ) != null )
      this.lightsLargeScaleRejectionHigh = o;
   if ( ( o = load( "lightsLargeScaleRejectionLayersHigh", DataType_Int32 ) ) != null )
      this.lightsLargeScaleRejectionLayersHigh = o;
   if ( ( o = load( "lightsLargeScaleRejectionGrowthHigh", DataType_Int32 ) ) != null )
      this.lightsLargeScaleRejectionGrowthHigh = o;
   if ( ( o = load( "lightsLargeScaleRejectionLow", DataType_Boolean ) ) != null )
      this.lightsLargeScaleRejectionLow = o;
   if ( ( o = load( "lightsLargeScaleRejectionLayersLow", DataType_Int32 ) ) != null )
      this.lightsLargeScaleRejectionLayersLow = o;
   if ( ( o = load( "lightsLargeScaleRejectionGrowthLow", DataType_Int32 ) ) != null )
      this.lightsLargeScaleRejectionGrowthLow = o;
   if ( ( o = load( "imageRegistration", DataType_Boolean ) ) != null )
      this.imageRegistration = o;

   if ( ( o = load( "linearPatternSubtraction", DataType_Boolean ) ) != null )
      this.linearPatternSubtraction = o;
   if ( ( o = load( "linearPatternSubtractionRejectionLimit", DataType_Int32 ) ) != null )
      this.linearPatternSubtractionRejectionLimit = o;
   if ( ( o = load( "linearPatternSubtractionMode", DataType_Int32 ) ) != null )
      this.linearPatternSubtractionMode = o;

   if ( ( o = load( "subframeWeightingEnabled", DataType_Boolean ) ) != null )
      this.subframeWeightingEnabled = o;
   if ( ( o = load( "subframeWeightingPreset", DataType_Int32 ) ) != null )
      this.subframeWeightingPreset = o;
   if ( ( o = load( "subframesWeightsMethod", DataType_Int32 ) ) != null )
      this.subframesWeightsMethod = o;

   if ( ( o = load( "FWHMWeight", DataType_Int32 ) ) != null )
      this.FWHMWeight = o;
   if ( ( o = load( "eccentricityWeight", DataType_Int32 ) ) != null )
      this.eccentricityWeight = o;
   if ( ( o = load( "SNRWeight", DataType_Int32 ) ) != null )
      this.SNRWeight = o;
   if ( ( o = load( "starsWeight", DataType_Int32 ) ) != null )
      this.starsWeight = o;
   if ( ( o = load( "PSFSignalWeight", DataType_Int32 ) ) != null )
      this.PSFSignalWeight = o;
   if ( ( o = load( "PSFSNRWeight", DataType_Int32 ) ) != null )
      this.PSFSNRWeight = o;
   if ( ( o = load( "pedestal", DataType_Int32 ) ) != null )
      this.pedestal = o;

   if ( ( o = load( "localNormalization", DataType_Boolean ) ) != null )
      this.localNormalization = o;
   if ( ( o = load( "localNormalizationInteractiveMode", DataType_Boolean ) ) != null )
      this.localNormalizationInteractiveMode = o;
   if ( ( o = load( "localNormalizationGenerateImages", DataType_Boolean ) ) != null )
      this.localNormalizationGenerateImages = o;
   if ( ( o = load( "localNormalizationMethod", DataType_Int32 ) ) != null )
      this.localNormalizationMethod = o;
   if ( ( o = load( "localNormalizationMaxIntegratedFrames", DataType_Int32 ) ) != null )
      this.localNormalizationMaxIntegratedFrames = o;
   if ( ( o = load( "localNormalizationBestReferenceSelectionMethod", DataType_Int32 ) ) != null )
      this.localNormalizationBestReferenceSelectionMethod = o;
   if ( ( o = load( "localNormalizationGridSize", DataType_Int32 ) ) != null )
      this.localNormalizationGridSize = o;
   if ( ( o = load( "localNormalizationRerenceFrameGenerationMethod", DataType_Int32 ) ) != null )
      this.localNormalizationRerenceFrameGenerationMethod = o;
   if ( ( o = load( "localNormalizationPsfType", DataType_Int32 ) ) != null )
      this.localNormalizationPsfType = o;
   if ( ( o = load( "localNormalizationPsfGrowth", DataType_Float ) ) != null )
      this.localNormalizationPsfGrowth = o;
   if ( ( o = load( "localNormalizationPsfMaxStars", DataType_Int32 ) ) != null )
      this.localNormalizationPsfMaxStars = o;
   if ( ( o = load( "localNormalizationPsfMinSNR", DataType_Float ) ) != null )
      this.localNormalizationPsfMinSNR = o;
   if ( ( o = load( "localNormalizationPsfAllowClusteredSources", DataType_Boolean ) ) != null )
      this.localNormalizationPsfAllowClusteredSources = o;
   if ( ( o = load( "localNormalizationLowClippingLevel", DataType_Float ) ) != null )
      this.localNormalizationLowClippingLevel = o;
   if ( ( o = load( "localNormalizationHighClippingLevel", DataType_Float ) ) != null )
      this.localNormalizationHighClippingLevel = o;

   if ( ( o = load( "platesolve", DataType_Boolean ) ) != null )
      this.platesolve = o;
   if ( ( o = load( "platesolveFallbackManual", DataType_Boolean ) ) != null )
      this.platesolveFallbackManual = o;
   if ( ( o = load( "imageSolverRa", DataType_Double ) ) != null )
      this.imageSolverRa = o;
   if ( ( o = load( "imageSolverDec", DataType_Double ) ) != null )
      this.imageSolverDec = o;
   if ( ( o = load( "imageSolverEpoch", DataType_Double ) ) != null )
      this.imageSolverEpoch = o;
   if ( ( o = load( "imageSolverFocalLength", DataType_Float ) ) != null )
      this.imageSolverFocalLength = o;
   if ( ( o = load( "imageSolverPixelSize", DataType_Float ) ) != null )
      this.imageSolverPixelSize = o;
   if ( ( o = load( "imageSolverForceDefaults", DataType_Boolean ) ) != null )
      this.imageSolverForceDefaults = o;

   if ( ( o = load( "pixelInterpolation", DataType_Int32 ) ) != null )
      this.pixelInterpolation = o;
   if ( ( o = load( "clampingThreshold", DataType_Float ) ) != null )
      this.clampingThreshold = o;
   if ( ( o = load( "maxStars", DataType_Int32 ) ) != null )
      this.maxStars = o;
   if ( ( o = load( "distortionCorrection", DataType_Boolean ) ) != null )
      this.distortionCorrection = o;
   if ( ( o = load( "structureLayers", DataType_Int32 ) ) != null )
      this.structureLayers = o;
   if ( ( o = load( "hotPixelFilterRadius", DataType_Int32 ) ) != null )
      this.hotPixelFilterRadius = o;
   if ( ( o = load( "noiseReductionFilterRadius", DataType_Int32 ) ) != null )
      this.noiseReductionFilterRadius = o;
   if ( ( o = load( "minStructureSize", DataType_Int32 ) ) != null )
      this.minStructureSize = o;
   if ( ( o = load( "sensitivity", DataType_Float ) ) != null )
      this.sensitivity = o;
   if ( ( o = load( "peakResponse", DataType_Float ) ) != null )
      this.peakResponse = o;
   if ( ( o = load( "brightThreshold", DataType_Float ) ) != null )
      this.brightThreshold = o;
   if ( ( o = load( "maxStarDistortion", DataType_Float ) ) != null )
      this.maxStarDistortion = o;
   if ( ( o = load( "allowClusteredSources", DataType_Boolean ) ) != null )
      this.allowClusteredSources = o;
   if ( ( o = load( "useTriangleSimilarity", DataType_Boolean ) ) != null )
      this.useTriangleSimilarity = o;

   if ( ( o = load( "integrate", DataType_Boolean ) ) != null )
      this.integrate = o;
   if ( ( o = load( "autocrop", DataType_Boolean ) ) != null )
      this.autocrop = o;
   if ( ( o = load( "usePipelineScript", DataType_Boolean ) ) != null )
      this.usePipelineScript = o;
   if ( ( o = load( "pipelineScriptFile", DataType_String ) ) != null )
      this.pipelineScriptFile = o;

   if ( ( o = load( "referenceImage", DataType_String ) ) != null )
      this.referenceImage = o;
   if ( ( o = load( "bestFrameReferenceKeyword", DataType_String ) ) != null )
      this.bestFrameReferenceKeyword = o;
   if ( ( o = load( "bestFrameRefernceMethod", DataType_Int32 ) ) != null )
      this.bestFrameRefernceMethod = o;

   if ( ( o = load( "debayerOutputMethod", DataType_Int32 ) ) != null )
      this.debayerOutputMethod = o;
   if ( ( o = load( "recombineRGB", DataType_Boolean ) ) != null )
      this.recombineRGB = o;
   if ( ( o = load( "debayerActiveChannelR", DataType_Boolean ) ) != null )
      this.debayerActiveChannelR = o;
   if ( ( o = load( "debayerActiveChannelG", DataType_Boolean ) ) != null )
      this.debayerActiveChannelG = o;
   if ( ( o = load( "debayerActiveChannelB", DataType_Boolean ) ) != null )
      this.debayerActiveChannelB = o;

   if ( ( o = load( "enableCompactGUI", DataType_Boolean ) ) != null )
      this.enableCompactGUI = o;

   if ( ( o = load( "keywords", DataType_String ) ) != null )
   {
      let keywords = JSON.parse( o );
      this.keywords.list = this.migrateKeywords( keywords, dataVersion );
   }
   if ( this.saveFrameGroups )
   {
      if ( ( o = load( "groups", DataType_String ) ) != null )
         this.groupsFromStringData( o, dataVersion );
      else if ( ( o = load( "frameGroups", DataType_String ) ) != null ) /* WBPP 2.0.2 */
         this.groupsFromStringData( o, dataVersion );
   }

   if ( ( o = load( "executionCache", DataType_String ) ) != null )
   {
      // measure parsing performance
      let elapsed = new ElapsedTime;
      try
      {
         this.executionCache.fromString( o );
      }
      catch ( e )
      {
         this.executionCache.reset();
      }

      console.noteln( "* Parsed cache data in ", elapsed.text );
   }

   if ( dataVersion )
      this.migrateFrom( dataVersion );
};

// ----------------------------------------------------------------------------

/**
 * Persist the WBPP settings.
 *
 */
StackEngine.prototype.saveSettings = function()
{
   function save( key, type, value )
   {
      try
      {
         Settings.write( SETTINGS_KEY_BASE + key, type, value );
      }
      catch ( e )
      {
         console.warningln( "Unable to save [", key, "] for type ", type, " with value ", value );
      }
   }

   function saveIndexed( key, index, type, value )
   {
      try
      {
         save( key + '_' + index.toString(), type, value );
      }
      catch ( e )
      {
         console.warningln( "Unable to save [", key, "] for type", type, " with value ", value, " at index ", index );
      }
   }

   // base64 encoding to handle non utf8 chars
   save( "outputDirectory", DataType_String, new ByteArray( this.outputDirectory ).toBase64() );

   save( "saveFrameGroups", DataType_Boolean, this.saveFrameGroups );
   save( "smartNamingOverride", DataType_Boolean, this.smartNamingOverride );
   save( "detectMasterIncludingFullPath", DataType_Boolean, this.detectMasterIncludingFullPath );
   save( "fitsCoordinateConvention", DataType_Int32, this.fitsCoordinateConvention );
   save( "generateRejectionMaps", DataType_Boolean, this.generateRejectionMaps );
   save( "preserveWhiteBalance", DataType_Boolean, this.preserveWhiteBalance );
   save( "groupingKeywordsEnabled", DataType_Boolean, this.groupingKeywordsEnabled );
   save( "showAstrometricInfo", DataType_Boolean, this.showAstrometricInfo );

   save( "darkOptimizationLow", DataType_Float, this.darkOptimizationLow );
   save( "darkExposureTolerance", DataType_Float, this.darkExposureTolerance );
   save( "lightExposureTolerance", DataType_Float, this.lightExposureTolerance );
   save( "lightExposureTolerancePost", DataType_Float, this.lightExposureTolerancePost );

   save( "overscanEnabled", DataType_Boolean, this.overscan.enabled );
   for ( let i = 0; i < 4; ++i )
   {
      saveIndexed( "overscanRegionEnabled", i, DataType_Boolean, this.overscan.overscan[ i ].enabled );
      saveIndexed( "overscanSourceX0", i, DataType_Int32, this.overscan.overscan[ i ].sourceRect.x0 );
      saveIndexed( "overscanSourceY0", i, DataType_Int32, this.overscan.overscan[ i ].sourceRect.y0 );
      saveIndexed( "overscanSourceX1", i, DataType_Int32, this.overscan.overscan[ i ].sourceRect.x1 );
      saveIndexed( "overscanSourceY1", i, DataType_Int32, this.overscan.overscan[ i ].sourceRect.y1 );
      saveIndexed( "overscanTargetX0", i, DataType_Int32, this.overscan.overscan[ i ].targetRect.x0 );
      saveIndexed( "overscanTargetY0", i, DataType_Int32, this.overscan.overscan[ i ].targetRect.y0 );
      saveIndexed( "overscanTargetX1", i, DataType_Int32, this.overscan.overscan[ i ].targetRect.x1 );
      saveIndexed( "overscanTargetY1", i, DataType_Int32, this.overscan.overscan[ i ].targetRect.y1 );
   }
   save( "overscanImageX0", DataType_Int32, this.overscan.imageRect.x0 );
   save( "overscanImageY0", DataType_Int32, this.overscan.imageRect.y0 );
   save( "overscanImageX1", DataType_Int32, this.overscan.imageRect.x1 );
   save( "overscanImageY1", DataType_Int32, this.overscan.imageRect.y1 );

   save( "minWeight", DataType_Float, this.minWeight );

   for ( let i = 0; i < 4; ++i )
   {
      saveIndexed( "combination", i, DataType_Int32, this.combination[ i ] );
      saveIndexed( "rejection", i, DataType_Int32, this.rejection[ i ] );
      saveIndexed( "percentileLow", i, DataType_Float, this.percentileLow[ i ] );
      saveIndexed( "percentileHigh", i, DataType_Float, this.percentileHigh[ i ] );
      saveIndexed( "sigmaLow", i, DataType_Float, this.sigmaLow[ i ] );
      saveIndexed( "sigmaHigh", i, DataType_Float, this.sigmaHigh[ i ] );
      saveIndexed( "linearFitLow", i, DataType_Float, this.linearFitLow[ i ] );
      saveIndexed( "linearFitHigh", i, DataType_Float, this.linearFitHigh[ i ] );
      saveIndexed( "ESD_Outliers", i, DataType_Float, this.ESD_Outliers[ i ] );
      saveIndexed( "ESD_Significance", i, DataType_Float, this.ESD_Significance[ i ] );
      saveIndexed( "RCR_Limit", i, DataType_Float, this.RCR_Limit[ i ] );
   }

   save( "flatsLargeScaleRejection", DataType_Boolean, this.flatsLargeScaleRejection );
   save( "flatsLargeScaleRejectionLayers", DataType_Int32, this.flatsLargeScaleRejectionLayers );
   save( "flatsLargeScaleRejectionGrowth", DataType_Int32, this.flatsLargeScaleRejectionGrowth );
   save( "lightsLargeScaleRejectionHigh", DataType_Boolean, this.lightsLargeScaleRejectionHigh );
   save( "lightsLargeScaleRejectionLayersHigh", DataType_Int32, this.lightsLargeScaleRejectionLayersHigh );
   save( "lightsLargeScaleRejectionGrowthHigh", DataType_Int32, this.lightsLargeScaleRejectionGrowthHigh );
   save( "lightsLargeScaleRejectionLow", DataType_Boolean, this.lightsLargeScaleRejectionLow );
   save( "lightsLargeScaleRejectionLayersLow", DataType_Int32, this.lightsLargeScaleRejectionLayersLow );
   save( "lightsLargeScaleRejectionGrowthLow", DataType_Int32, this.lightsLargeScaleRejectionGrowthLow );
   save( "imageRegistration", DataType_Boolean, this.imageRegistration );

   save( "platesolve", DataType_Boolean, this.platesolve );
   save( "platesolveFallbackManual", DataType_Boolean, this.platesolveFallbackManual );
   save( "imageSolverRa", DataType_Double, this.imageSolverRa );
   save( "imageSolverDec", DataType_Double, this.imageSolverDec );
   save( "imageSolverEpoch", DataType_Double, this.imageSolverEpoch );
   save( "imageSolverFocalLength", DataType_Float, this.imageSolverFocalLength );
   save( "imageSolverPixelSize", DataType_Float, this.imageSolverPixelSize );
   save( "imageSolverForceDefaults", DataType_Boolean, this.imageSolverForceDefaults );

   save( "pixelInterpolation", DataType_Int32, this.pixelInterpolation );
   save( "clampingThreshold", DataType_Float, this.clampingThreshold );
   save( "maxStars", DataType_Int32, this.maxStars );
   save( "distortionCorrection", DataType_Boolean, this.distortionCorrection );

   save( "linearPatternSubtraction", DataType_Boolean, this.linearPatternSubtraction );
   save( "linearPatternSubtractionRejectionLimit", DataType_Int32, this.linearPatternSubtractionRejectionLimit );
   save( "linearPatternSubtractionMode", DataType_Int32, this.linearPatternSubtractionMode );

   save( "subframeWeightingEnabled", DataType_Boolean, this.subframeWeightingEnabled );
   save( "subframeWeightingPreset", DataType_Int32, this.subframeWeightingPreset );
   save( "subframesWeightsMethod", DataType_Int32, this.subframesWeightsMethod );
   save( "FWHMWeight", DataType_Int32, this.FWHMWeight );
   save( "eccentricityWeight", DataType_Int32, this.eccentricityWeight );
   save( "SNRWeight", DataType_Int32, this.SNRWeight );
   save( "starsWeight", DataType_Int32, this.starsWeight );
   save( "PSFSignalWeight", DataType_Int32, this.PSFSignalWeight );
   save( "PSFSNRWeight", DataType_Int32, this.PSFSNRWeight );
   save( "pedestal", DataType_Int32, this.pedestal );

   save( "localNormalization", DataType_Boolean, this.localNormalization );
   save( "localNormalizationInteractiveMode", DataType_Boolean, this.localNormalizationInteractiveMode );
   save( "localNormalizationGenerateImages", DataType_Boolean, this.localNormalizationGenerateImages );
   save( "localNormalizationMethod", DataType_Int32, this.localNormalizationMethod );
   save( "localNormalizationMaxIntegratedFrames", DataType_Int32, this.localNormalizationMaxIntegratedFrames );
   save( "localNormalizationBestReferenceSelectionMethod", DataType_Int32, this.localNormalizationBestReferenceSelectionMethod );
   save( "localNormalizationGridSize", DataType_Int32, this.localNormalizationGridSize );
   save( "localNormalizationRerenceFrameGenerationMethod", DataType_Int32, this.localNormalizationRerenceFrameGenerationMethod );
   save( "localNormalizationPsfType", DataType_Int32, this.localNormalizationPsfType );
   save( "localNormalizationPsfGrowth", DataType_Float, this.localNormalizationPsfGrowth );
   save( "localNormalizationPsfMaxStars", DataType_Int32, this.localNormalizationPsfMaxStars );
   save( "localNormalizationPsfMinSNR", DataType_Float, this.localNormalizationPsfMinSNR );
   save( "localNormalizationPsfAllowClusteredSources", DataType_Boolean, this.localNormalizationPsfAllowClusteredSources );
   save( "localNormalizationLowClippingLevel", DataType_Float, this.localNormalizationLowClippingLevel );
   save( "localNormalizationHighClippingLevel", DataType_Float, this.localNormalizationHighClippingLevel );

   save( "structureLayers", DataType_Int32, this.structureLayers );
   save( "hotPixelFilterRadius", DataType_Int32, this.hotPixelFilterRadius );
   save( "noiseReductionFilterRadius", DataType_Int32, this.noiseReductionFilterRadius );
   save( "minStructureSize", DataType_Int32, this.minStructureSize );
   save( "sensitivity", DataType_Float, this.sensitivity );
   save( "peakResponse", DataType_Float, this.peakResponse );
   save( "brightThreshold", DataType_Float, this.brightThreshold );
   save( "maxStarDistortion", DataType_Float, this.maxStarDistortion );
   save( "allowClusteredSources", DataType_Boolean, this.allowClusteredSources );
   save( "useTriangleSimilarity", DataType_Boolean, this.useTriangleSimilarity );

   save( "integrate", DataType_Boolean, this.integrate );
   save( "autocrop", DataType_Boolean, this.autocrop );

   save( "usePipelineScript", DataType_Boolean, this.usePipelineScript );
   save( "pipelineScriptFile", DataType_String, this.pipelineScriptFile );

   save( "referenceImage", DataType_String, this.referenceImage );
   save( "bestFrameReferenceKeyword", DataType_String, this.bestFrameReferenceKeyword );
   save( "bestFrameRefernceMethod", DataType_Int32, this.bestFrameRefernceMethod );

   save( "debayerOutputMethod", DataType_Int32, this.debayerOutputMethod );
   save( "recombineRGB", DataType_Boolean, this.recombineRGB );
   save( "debayerActiveChannelR", DataType_Boolean, this.debayerActiveChannelR );
   save( "debayerActiveChannelG", DataType_Boolean, this.debayerActiveChannelG );
   save( "debayerActiveChannelB", DataType_Boolean, this.debayerActiveChannelB );

   save( "enableCompactGUI", DataType_Boolean, this.enableCompactGUI );

   save( "VERSION", DataType_String, VERSION );

   save( "keywords", DataType_String, JSON.stringify( this.keywords.list ) );
   if ( this.saveFrameGroups )
      save( "groups", DataType_String, this.groupsToStringData() );
   save( "executionCache", DataType_String, this.executionCache.toString() );
};

// ----------------------------------------------------------------------------

StackEngine.prototype.defaults = {
   globals: () =>
   {

   },
   overscan: () =>
   {

      for ( let i = 0; i < 4; ++i )
      {
         engine.overscan.overscan[ i ].enabled = false;
         engine.overscan.overscan[ i ].sourceRect.assign( 0 );
         engine.overscan.overscan[ i ].targetRect.assign( 0 );
      }
      engine.overscan.imageRect.assign( 0 );
   },
   customFormulaWeights: () =>
   {
      engine.subframeWeightingPreset = DEFAULT_SUBFRAMEWEIGHTING_PRESET;
      engine.FWHMWeight = DEFAULT_SUBFRAMEWEIGHTING_FWHM_WEIGHT;
      engine.eccentricityWeight = DEFAULT_SUBFRAMEWEIGHTING_ECCENTRICITY_WEIGHT;
      engine.starsWeight = DEFAULT_SUBFRAMEWEIGHTING_STARS_WEIGHT;
      engine.PSFSignalWeight = DEFAULT_SUBFRAMEWEIGHTING_PSF_SIGNAL_WEIGHT;
      engine.PSFSNRWeight = DEFAULT_SUBFRAMEWEIGHTING_PSF_SNR_WEIGHT;
      engine.SNRWeight = DEFAULT_SUBFRAMEWEIGHTING_SNR_WEIGHT;
      engine.pedestal = DEFAULT_SUBFRAMEWEIGHTING_PEDESTAL;
   },
   imageRegistration: () =>
   {
      engine.pixelInterpolation = DEFAULT_SA_PIXEL_INTERPOLATION;
      engine.clampingThreshold = DEFAULT_SA_CLAMPING_THRESHOLD;
      engine.maxStars = DEFAULT_SA_MAX_STARS;
      engine.distortionCorrection = DEFAULT_SA_DISTORTION_CORRECTION;
      engine.structureLayers = DEFAULT_SA_STRUCTURE_LAYERS;
      engine.minStructureSize = DEFAULT_SA_MIN_STRUCTURE_SIZE;
      engine.hotPixelFilterRadius = DEFAULT_SA_HOT_PIXEL_FILTER_RADIUS;
      engine.noiseReductionFilterRadius = DEFAULT_SA_NOISE_REDUCTION;
      engine.sensitivity = DEFAULT_SA_SENSITIVITY;
      engine.peakResponse = DEFAULT_SA_PEAK_RESPONSE;
      engine.brightThreshold = DEFAULT_SA_BRIGHT_THRESHOLD;
      engine.maxStarDistortion = DEFAULT_SA_MAX_STAR_DISTORTION;
      engine.allowClusteredSources = DEFAULT_SA_ALLOW_CLUSTERED_SOURCES;
      engine.useTriangleSimilarity = DEFAULT_SA_USE_TRIANGLE_SIMILARITY;
   },
   imageSolver: () =>
   {
      engine.imageSolverRa = DEFAULT_IMAGE_SOLVER_RA;
      engine.imageSolverDec = DEFAULT_IMAGE_SOLVER_DEC;
      engine.imageSolverEpoch = DEFAULT_IMAGE_SOLVER_EPOCH;
      engine.imageSolverFocalLength = DEFAULT_IMAGE_SOLVER_FOCAL_LENGTH;
      engine.imageSolverPixelSize = DEFAULT_IMAGE_SOLVER_PIXEL_SIZE;
      engine.imageSolverForceDefaults = DEFAULT_IMAGE_SOLVER_FORCE_DEFAULTS;
   },
   localNormalization: () =>
   {
      engine.localNormalization = DEFAULT_LOCALNORMALIZATION;
      engine.localNormalizationInteractiveMode = DEFAULT_LOCALNORMALIZATION_INTERACTIVE_MODE;
      engine.localNormalizationGenerateImages = DEFAULT_LOCALNORMALIZATION_GENERATE_IMAGES;
      engine.localNormalizationMethod = DEFAULT_LOCALNORMALIZATION_METHOD;
      engine.localNormalizationMaxIntegratedFrames = DEFAULT_LOCALNORMALIZATION_INTEGRATED_FRAMES;
      engine.localNormalizationBestReferenceSelectionMethod = DEFAULT_LOCALNORMALIZATION_BEST_REFERENCE_METHOD;
      engine.localNormalizationGridSize = DEFAULT_LOCALNORMALIZATION_GRID_SIZE;
      engine.localNormalizationRerenceFrameGenerationMethod = DEFAULT_LOCALNORMALIZATION_REF_FRAME_METHOD;
      engine.localNormalizationPsfType = DEFAULT_LOCALNORMALIZATION_PSF_TYPE;
      engine.localNormalizationPsfGrowth = DEFAULT_LOCALNORMALIZATION_PSF_GROWTH;
      engine.localNormalizationPsfMaxStars = DEFAULT_LOCALNORMALIZATION_PSF_MAX_STARS;
      engine.localNormalizationPsfMinSNR = DEFAULT_LOCALNORMALIZATION_PSF_MIN_SNR;
      engine.localNormalizationPsfAllowClusteredSources = DEFAULT_LOCALNORMALIZATION_PSF_ALLOW_CLUSTERED;
      engine.localNormalizationLowClippingLevel = DEFAULT_LOCALNORMALIZATION_LOW_CLIPPING_LEVEL;
      engine.localNormalizationHighClippingLevel = DEFAULT_LOCALNORMALIZATION_HIGH_CLIPPING_LEVEL;
   },
   imageIntegration: ( imageType ) =>
   {
      engine.combination[ imageType ] = ImageIntegration.prototype.Average;
      engine.rejection[ imageType ] = DEFAULT_REJECTION_METHOD;
      engine.percentileLow[ imageType ] = 0.2;
      engine.percentileHigh[ imageType ] = 0.1;
      engine.sigmaLow[ imageType ] = 4.0;
      engine.sigmaHigh[ imageType ] = 3.0;
      engine.linearFitLow[ imageType ] = 5.0;
      engine.linearFitHigh[ imageType ] = 3.5;
      engine.ESD_Outliers[ imageType ] = 0.3;
      engine.ESD_Significance[ imageType ] = 0.05;
      engine.RCR_Limit[ imageType ] = 0.1;

      if ( imageType == ImageType.FLAT )
      {
         engine.flatsLargeScaleRejection = DEFAULT_LARGE_SCALE_REJECTION;
         engine.flatsLargeScaleRejectionLayers = DEFAULT_LARGE_SCALE_LAYERS;
         engine.flatsLargeScaleRejectionGrowth = DEFAULT_LARGE_SCALE_GROWTH;
      }

      if ( imageType == ImageType.LIGHT )
      {
         engine.minWeight = DEFAULT_MIN_WEIGHT;
         engine.lightsLargeScaleRejectionHigh = DEFAULT_LARGE_SCALE_REJECTION;
         engine.lightsLargeScaleRejectionLayersHigh = DEFAULT_LARGE_SCALE_LAYERS;
         engine.lightsLargeScaleRejectionGrowthHigh = DEFAULT_LARGE_SCALE_GROWTH;
         engine.lightsLargeScaleRejectionLow = DEFAULT_LARGE_SCALE_REJECTION;
         engine.lightsLargeScaleRejectionLayersLow = DEFAULT_LARGE_SCALE_LAYERS;
         engine.lightsLargeScaleRejectionGrowthLow = DEFAULT_LARGE_SCALE_GROWTH;
      }
   }
};

StackEngine.prototype.setDefaultParameters = function()
{
   setDefaultParameters.apply( this );
};

// ----------------------------------------------------------------------------

/**
 * Sets the WBPP's engine default parameters.
 *
 */
function setDefaultParameters()
{
   // General options
   this.detectMasterIncludingFullPath = DEFAULT_MASTER_DETECTION_USES_FULL_PATH;
   this.smartNamingOverride = DEFAULT_SMART_NAMING_OVERRIDE;
   this.saveFrameGroups = DEFAULT_SAVE_FRAME_GROUPS;
   this.outputDirectory = DEFAULT_OUTPUT_DIRECTORY;
   this.fitsCoordinateConvention = DEFAULT_FITS_COORDINATE_CONVENTION;
   this.generateRejectionMaps = DEFAULT_GENERATE_REJECTION_MAPS;
   this.preserveWhiteBalance = DEFAULT_PRESERVE_WHITE_BALANCE;
   this.groupingKeywordsEnabled = DEFAULT_GROUPING_KEYWORDS_ACTIVE;
   this.showAstrometricInfo = DEFAULT_SHOW_ASTROMETRIC_INFO;

   // Calibration parameters
   this.darkOptimizationThreshold = 0; // ### deprecated - retained for compatibility
   this.darkOptimizationLow = DEFAULT_DARK_OPTIMIZATION_LOW; // in sigma units from the central value
   this.darkOptimizationWindow = DEFAULT_DARK_OPTIMIZATION_WINDOW;
   this.darkExposureTolerance = DEFAULT_DARK_EXPOSURE_TOLERANCE; // in seconds

   // Image integration parameters
   for ( let imageType = 0; imageType < 4; ++imageType )
      this.defaults.imageIntegration( imageType );

   // Overscan
   this.overscan.enabled = false;
   this.defaults.overscan();

   this.minWeight = DEFAULT_MIN_WEIGHT;

   // Light
   this.lightExposureTolerance = DEFAULT_LIGHT_EXPOSURE_TOLERANCE; // in seconds
   this.lightExposureTolerancePost = DEFAULT_LIGHT_EXPOSURE_TOLERANCE_POST; // in seconds
   this.imageRegistration = DEFAULT_IMAGE_REGISTRATION;

   // Linear Pattern Subtraction
   this.linearPatternSubtraction = DEFAULT_LINEAR_PATTERN_SUBTRACTION;
   this.linearPatternSubtractionRejectionLimit = DEFAULT_LINEAR_PATTERN_SUBTRACTION_SIGMA;
   this.linearPatternSubtractionMode = DEFAULT_LINEAR_PATTERN_SUBTRACTION_MODE;

   // Subframe weights
   this.subframeWeightingEnabled = DEFAULT_SUBFRAMEWEIGHTING_ENABLED;
   this.subframesWeightsMethod = DEFAULT_SUBFRAMEWEIGHTING_METHOD;
   this.defaults.customFormulaWeights();

   // Image Solver
   this.platesolve = DEFAULT_SA_PLATESOLVE;
   this.platesolveFallbackManual = DEFAULT_SA_PLATESOLVE_FALLBACK_MANUAL;
   this.defaults.imageSolver();

   // Local normalization
   this.defaults.localNormalization();

   // Image registration
   this.referenceImage = "";
   this.bestFrameReferenceKeyword = "";
   this.bestFrameRefernceMethod = DEFAULT_BEST_REFERENCE_METHOD;
   this.debayerOutputMethod = DEFAULT_DEBAYER_OUTPUT_METHOD;
   this.defaults.imageRegistration();

   // post-calibration
   this.debayerActiveChannelR = true;
   this.debayerActiveChannelG = true;
   this.debayerActiveChannelB = true;
   this.recombineRGB = DEFAULT_RECOMBINE_RGB;

   // pipeline scripting
   this.usePipelineScript = false;
   this.pipelineScriptFile = "";

   // GUI
   this.enableCompactGUI = false;
   this.GUIBiasPageOverscanControlCollapsed = true;
   this.GUIBiasPageImageIntegrationControlCollapsed = true;
   this.GUIDarkPageImageIntegrationControlCollapsed = true;
   this.GUIFlatPageImageIntegrationControlCollapsed = true;
   this.GUILightPageLPSControlCollapsed = true;
   this.GUILightPageSubframeWeightingControlCollapsed = true;
   this.GUILightPageRegistrationControlCollapsed = true;
   this.GUIPlatesolveControlCollapsed = true;
   this.GUILightPageNormalizationControlCollapsed = true;
   this.GUILightPageIntegrationControlCollapsed = true;

   this.GUIPresetsCollapsed = true;
   this.GUIKeywordsCollapsed = true;
   this.GUIGlobalOptionsCollapsed = true;
   this.GUIReferenceFrameCollapsed = true;
   this.GUIOutputDirCollapsed = true;

   this.GUIGroupOverscanSettingsCollapsed = true;
   this.GUIGroupCalibrationSettingsCollapsed = true;
   this.GUIGroupPedestalSettingsCollapsed = true;
   this.GUIGroupCCSettingsCollapsed = true;
   this.GUIGroupDebayerSettingsCollapsed = true;

   this.globalOptionsHidden = false;

   this.integrate = DEFAULT_INTEGRATE;
   this.autocrop = DEFAULT_AUTOCROP;

   this.viewMode = WBPPGroupingMode.PRE;

   this.keywords = new Keywords();

   // automation mode is disabled by default
   this.automationMode = false;
}

// ----------------------------------------------------------------------------

/**
 * Import the WBPP settings from an instance.
 *
 */
StackEngine.prototype.importParameters = function( fromJSON )
{
   let
   {
      parameters,
      JSONParameters
   } = WBPPUtils.shared();

   let P = Parameters;
   if ( fromJSON )
   {
      P = JSONParameters;
      parameters = JSONParameters;
      P.clear();
      P.fromJSONtest( fromJSON );
   }
   else if ( !this.automationMode )
   {
      this.setDefaultParameters();
      this.loadSettings();
   }

   if ( P.has( "saveFrameGroups" ) )
      this.saveFrameGroups = P.getBoolean( "saveFrameGroups" );

   if ( P.has( "smartNamingOverride" ) )
      this.smartNamingOverride = P.getBoolean( "smartNamingOverride" );

   if ( P.has( "detectMasterIncludingFullPath" ) )
      this.detectMasterIncludingFullPath = P.getBoolean( "detectMasterIncludingFullPath" );

   if ( P.has( "outputDirectory" ) )
      this.outputDirectory = P.getString( "outputDirectory" );

   if ( P.has( "fitsCoordinateConvention" ) )
      this.fitsCoordinateConvention = P.getInteger( "fitsCoordinateConvention" );

   if ( P.has( "generateRejectionMaps" ) )
      this.generateRejectionMaps = P.getBoolean( "generateRejectionMaps" );

   if ( P.has( "preserveWhiteBalance" ) )
      this.preserveWhiteBalance = P.getBoolean( "preserveWhiteBalance" );

   if ( P.has( "groupingKeywordsEnabled" ) )
      this.groupingKeywordsEnabled = P.getBoolean( "groupingKeywordsEnabled" );

   if ( P.has( "showAstrometricInfo" ) )
      this.showAstrometricInfo = P.getBoolean( "showAstrometricInfo" );

   if ( P.has( "darkOptimizationThreshold" ) )
      this.darkOptimizationThreshold = P.getReal( "darkOptimizationThreshold" );

   if ( P.has( "darkOptimizationLow" ) )
      this.darkOptimizationLow = P.getReal( "darkOptimizationLow" );

   if ( P.has( "darkExposureTolerance" ) )
      this.darkExposureTolerance = P.getReal( "darkExposureTolerance" );

   if ( P.has( "lightExposureTolerance" ) )
      this.lightExposureTolerance = P.getReal( "lightExposureTolerance" );

   if ( P.has( "lightExposureTolerancePost" ) )
      this.lightExposureTolerancePost = P.getReal( "lightExposureTolerancePost" );

   if ( P.has( "overscanEnabled" ) )
      this.overscan.enabled = P.getBoolean( "overscanEnabled" );

   for ( let i = 0; i < 4; ++i )
   {
      if ( P.has( "overscanRegionEnabled" ) )
         this.overscan.overscan[ i ].enabled = parameters.getBooleanIndexed( "overscanRegionEnabled", i );

      if ( P.has( "overscanSourceX0" ) )
         this.overscan.overscan[ i ].sourceRect.x0 = parameters.getIntegerIndexed( "overscanSourceX0", i );

      if ( P.has( "overscanSourceY0" ) )
         this.overscan.overscan[ i ].sourceRect.y0 = parameters.getIntegerIndexed( "overscanSourceY0", i );

      if ( P.has( "overscanSourceX1" ) )
         this.overscan.overscan[ i ].sourceRect.x1 = parameters.getIntegerIndexed( "overscanSourceX1", i );

      if ( P.has( "overscanSourceY1" ) )
         this.overscan.overscan[ i ].sourceRect.y1 = parameters.getIntegerIndexed( "overscanSourceY1", i );

      if ( P.has( "overscanTargetX0" ) )
         this.overscan.overscan[ i ].targetRect.x0 = parameters.getIntegerIndexed( "overscanTargetX0", i );

      if ( P.has( "overscanTargetY0" ) )
         this.overscan.overscan[ i ].targetRect.y0 = parameters.getIntegerIndexed( "overscanTargetY0", i );

      if ( P.has( "overscanTargetX1" ) )
         this.overscan.overscan[ i ].targetRect.x1 = parameters.getIntegerIndexed( "overscanTargetX1", i );

      if ( P.has( "overscanTargetY1" ) )
         this.overscan.overscan[ i ].targetRect.y1 = parameters.getIntegerIndexed( "overscanTargetY1", i );
   }

   if ( P.has( "overscanImageX0" ) )
      this.overscan.imageRect.x0 = P.getInteger( "overscanImageX0" );

   if ( P.has( "overscanImageY0" ) )
      this.overscan.imageRect.y0 = P.getInteger( "overscanImageY0" );

   if ( P.has( "overscanImageX1" ) )
      this.overscan.imageRect.x1 = P.getInteger( "overscanImageX1" );

   if ( P.has( "overscanImageY1" ) )
      this.overscan.imageRect.y1 = P.getInteger( "overscanImageY1" );

   if ( P.has( "minWeight" ) )
      this.minWeight = P.getReal( "minWeight" );

   for ( let i = 0; i < 4; ++i )
   {
      if ( parameters.hasIndexed( "combination", i ) )
         this.combination[ i ] = parameters.getIntegerIndexed( "combination", i );

      if ( parameters.hasIndexed( "rejection", i ) )
         this.rejection[ i ] = parameters.getIntegerIndexed( "rejection", i );

      if ( parameters.hasIndexed( "percentileLow", i ) )
         this.percentileLow[ i ] = parameters.getRealIndexed( "percentileLow", i );

      if ( parameters.hasIndexed( "percentileHigh", i ) )
         this.percentileHigh[ i ] = parameters.getRealIndexed( "percentileHigh", i );

      if ( parameters.hasIndexed( "sigmaLow", i ) )
         this.sigmaLow[ i ] = parameters.getRealIndexed( "sigmaLow", i );

      if ( parameters.hasIndexed( "sigmaHigh", i ) )
         this.sigmaHigh[ i ] = parameters.getRealIndexed( "sigmaHigh", i );

      if ( parameters.hasIndexed( "linearFitLow", i ) )
         this.linearFitLow[ i ] = parameters.getRealIndexed( "linearFitLow", i );

      if ( parameters.hasIndexed( "linearFitHigh", i ) )
         this.linearFitHigh[ i ] = parameters.getRealIndexed( "linearFitHigh", i );

      if ( parameters.hasIndexed( "ESD_Outliers", i ) )
         this.ESD_Outliers[ i ] = parameters.getRealIndexed( "ESD_Outliers", i );

      if ( parameters.hasIndexed( "ESD_Significance", i ) )
         this.ESD_Significance[ i ] = parameters.getRealIndexed( "ESD_Significance", i );

      if ( parameters.hasIndexed( "RCR_Limit", i ) )
         this.RCR_Limit[ i ] = parameters.getRealIndexed( "RCR_Limit", i );
   }

   if ( P.has( "flatsLargeScaleRejection" ) )
      this.flatsLargeScaleRejection = P.getBoolean( "flatsLargeScaleRejection" );

   if ( P.has( "flatsLargeScaleRejectionLayers" ) )
      this.flatsLargeScaleRejectionLayers = P.getInteger( "flatsLargeScaleRejectionLayers" );

   if ( P.has( "flatsLargeScaleRejectionGrowth" ) )
      this.flatsLargeScaleRejectionGrowth = P.getInteger( "flatsLargeScaleRejectionGrowth" );

   if ( P.has( "lightsLargeScaleRejectionHigh" ) )
      this.lightsLargeScaleRejectionHigh = P.getBoolean( "lightsLargeScaleRejectionHigh" );

   if ( P.has( "lightsLargeScaleRejectionLayersHigh" ) )
      this.lightsLargeScaleRejectionLayersHigh = P.getInteger( "lightsLargeScaleRejectionLayersHigh" );

   if ( P.has( "lightsLargeScaleRejectionGrowthHigh" ) )
      this.lightsLargeScaleRejectionGrowthHigh = P.getInteger( "lightsLargeScaleRejectionGrowthHigh" );

   if ( P.has( "lightsLargeScaleRejectionLow" ) )
      this.lightsLargeScaleRejectionLow = P.getBoolean( "lightsLargeScaleRejectionLow" );

   if ( P.has( "lightsLargeScaleRejectionLayersLow" ) )
      this.lightsLargeScaleRejectionLayersLow = P.getInteger( "lightsLargeScaleRejectionLayersLow" );

   if ( P.has( "lightsLargeScaleRejectionGrowthLow" ) )
      this.lightsLargeScaleRejectionGrowthLow = P.getInteger( "lightsLargeScaleRejectionGrowthLow" );

   if ( P.has( "imageRegistration" ) )
      this.imageRegistration = P.getBoolean( "imageRegistration" );

   if ( P.has( "linearPatternSubtraction" ) )
      this.linearPatternSubtraction = P.getBoolean( "linearPatternSubtraction" );

   if ( P.has( "linearPatternSubtractionRejectionLimit" ) )
      this.linearPatternSubtractionRejectionLimit = P.getInteger( "linearPatternSubtractionRejectionLimit" );

   if ( P.has( "linearPatternSubtractionMode" ) )
      this.linearPatternSubtractionMode = P.getInteger( "linearPatternSubtractionMode" );

   if ( P.has( "subframeWeightingEnabled" ) )
      this.subframeWeightingEnabled = P.getBoolean( "subframeWeightingEnabled" );

   if ( P.has( "subframeWeightingPreset" ) )
      this.subframeWeightingPreset = P.getInteger( "subframeWeightingPreset" );

   if ( P.has( "FWHMWeight" ) )
      this.FWHMWeight = P.getInteger( "FWHMWeight" );

   if ( P.has( "eccentricityWeight" ) )
      this.eccentricityWeight = P.getInteger( "eccentricityWeight" );

   if ( P.has( "SNRWeight" ) )
      this.SNRWeight = P.getInteger( "SNRWeight" );

   if ( P.has( "starsWeight" ) )
      this.starsWeight = P.getInteger( "starsWeight" );

   if ( P.has( "PSFSignalWeight" ) )
      this.PSFSignalWeight = P.getInteger( "PSFSignalWeight" );

   if ( P.has( "PSFSNRWeight" ) )
      this.PSFSNRWeight = P.getInteger( "PSFSNRWeight" );

   if ( P.has( "pedestal" ) )
      this.pedestal = P.getInteger( "pedestal" );

   if ( P.has( "localNormalization" ) )
      this.localNormalization = P.getBoolean( "localNormalization" );

   if ( P.has( "localNormalizationInteractiveMode" ) )
      this.localNormalizationInteractiveMode = P.getBoolean( "localNormalizationInteractiveMode" );

   if ( P.has( "localNormalizationGenerateImages" ) )
      this.localNormalizationGenerateImages = P.getBoolean( "localNormalizationGenerateImages" );

   if ( P.has( "localNormalizationMethod" ) )
      this.localNormalizationMethod = P.getInteger( "localNormalizationMethod" );

   if ( P.has( "localNormalizationMaxIntegratedFrames" ) )
      this.localNormalizationMaxIntegratedFrames = P.getInteger( "localNormalizationMaxIntegratedFrames" );

   if ( P.has( "localNormalizationBestReferenceSelectionMethod" ) )
      this.localNormalizationBestReferenceSelectionMethod = P.getInteger( "localNormalizationBestReferenceSelectionMethod" );

   if ( P.has( "localNormalizationGridSize" ) )
      this.localNormalizationGridSize = P.getInteger( "localNormalizationGridSize" );

   if ( P.has( "localNormalizationRerenceFrameGenerationMethod" ) )
      this.localNormalizationRerenceFrameGenerationMethod = P.getInteger( "localNormalizationRerenceFrameGenerationMethod" );

   if ( P.has( "localNormalizationPsfType" ) )
      this.localNormalizationPsfType = P.getInteger( "localNormalizationPsfType" );

   if ( P.has( "localNormalizationPsfGrowth" ) )
      this.localNormalizationPsfGrowth = P.getReal( "localNormalizationPsfGrowth" );

   if ( P.has( "localNormalizationPsfMaxStars" ) )
      this.localNormalizationPsfMaxStars = P.getInteger( "localNormalizationPsfMaxStars" );

   if ( P.has( "localNormalizationPsfMinSNR" ) )
      this.localNormalizationPsfMinSNR = P.getReal( "localNormalizationPsfMinSNR" );

   if ( P.has( "localNormalizationPsfAllowClusteredSources" ) )
      this.localNormalizationPsfAllowClusteredSources = P.getBoolean( "localNormalizationPsfAllowClusteredSources" );

   if ( P.has( "localNormalizationLowClippingLevel" ) )
      this.localNormalizationLowClippingLevel = P.getReal( "localNormalizationLowClippingLevel" );

   if ( P.has( "localNormalizationHighClippingLevel" ) )
      this.localNormalizationHighClippingLevel = P.getReal( "localNormalizationHighClippingLevel" );

   if ( P.has( "subframesWeightsMethod" ) )
      this.subframesWeightsMethod = P.getInteger( "subframesWeightsMethod" );

   if ( P.has( "platesolve" ) )
      this.platesolve = P.getBoolean( "platesolve" );

   if ( P.has( "platesolveFallbackManual" ) )
      this.platesolveFallbackManual = P.getBoolean( "platesolveFallbackManual" );

   if ( P.has( "imageSolverRa" ) )
      this.imageSolverRa = P.getReal( "imageSolverRa" );

   if ( P.has( "imageSolverDec" ) )
      this.imageSolverDec = P.getReal( "imageSolverDec" );

   if ( P.has( "imageSolverEpoch" ) )
      this.imageSolverEpoch = P.getReal( "imageSolverEpoch" );

   if ( P.has( "imageSolverFocalLength" ) )
      this.imageSolverFocalLength = P.getReal( "imageSolverFocalLength" );

   if ( P.has( "imageSolverPixelSize" ) )
      this.imageSolverPixelSize = P.getReal( "imageSolverPixelSize" );

   if ( P.has( "imageSolverForceDefaults" ) )
      this.imageSolverForceDefaults = P.getBoolean( "imageSolverForceDefaults" );

   if ( P.has( "pixelInterpolation" ) )
      this.pixelInterpolation = P.getInteger( "pixelInterpolation" );

   if ( P.has( "clampingThreshold" ) )
      this.clampingThreshold = P.getReal( "clampingThreshold" );

   if ( P.has( "maxStars" ) )
      this.maxStars = P.getInteger( "maxStars" );

   if ( P.has( "distortionCorrection" ) )
      this.distortionCorrection = P.getBoolean( "distortionCorrection" );

   if ( P.has( "structureLayers" ) )
      this.structureLayers = P.getInteger( "structureLayers" );

   if ( P.has( "hotPixelFilterRadius" ) )
      this.hotPixelFilterRadius = P.getInteger( "hotPixelFilterRadius" );

   if ( P.has( "noiseReductionFilterRadius" ) )
      this.noiseReductionFilterRadius = P.getInteger( "noiseReductionFilterRadius" );

   if ( P.has( "minStructureSize" ) )
      this.minStructureSize = P.getInteger( "minStructureSize" );

   if ( P.has( "sensitivity" ) )
      this.sensitivity = P.getReal( "sensitivity" );

   if ( P.has( "peakResponse" ) )
      this.peakResponse = P.getReal( "peakResponse" );

   if ( P.has( "brightThreshold" ) )
      this.brightThreshold = P.getReal( "brightThreshold" );

   if ( P.has( "maxStarDistortion" ) )
      this.maxStarDistortion = P.getReal( "maxStarDistortion" );

   if ( P.has( "allowClusteredSources" ) )
      this.allowClusteredSources = P.getBoolean( "allowClusteredSources" );

   if ( P.has( "useTriangleSimilarity" ) )
      this.useTriangleSimilarity = P.getBoolean( "useTriangleSimilarity" );

   if ( P.has( "referenceImage" ) )
      this.referenceImage = P.getString( "referenceImage" );

   if ( P.has( "bestFrameReferenceKeyword" ) )
      this.bestFrameReferenceKeyword = P.getString( "bestFrameReferenceKeyword" );

   if ( P.has( "bestFrameRefernceMethod" ) )
      this.bestFrameRefernceMethod = P.getInteger( "bestFrameRefernceMethod" );

   if ( P.has( "debayerOutputMethod" ) )
      this.debayerOutputMethod = P.getInteger( "debayerOutputMethod" );

   if ( P.has( "integrate" ) )
      this.integrate = P.getBoolean( "integrate" );

   if ( P.has( "autocrop" ) )
      this.autocrop = P.getBoolean( "autocrop" );

   if ( P.has( "recombineRGB" ) )
      this.recombineRGB = P.getBoolean( "recombineRGB" );

   if ( P.has( "debayerActiveChannelR" ) )
      this.debayerActiveChannelR = P.getBoolean( "debayerActiveChannelR" );

   if ( P.has( "debayerActiveChannelG" ) )
      this.debayerActiveChannelG = P.getBoolean( "debayerActiveChannelG" );

   if ( P.has( "debayerActiveChannelB" ) )
      this.debayerActiveChannelB = P.getBoolean( "debayerActiveChannelB" );

   if ( P.has( "usePipelineScript" ) )
      this.usePipelineScript = P.getBoolean( "usePipelineScript" );

   if ( P.has( "enableCompactGUI" ) )
      this.enableCompactGUI = P.getBoolean( "enableCompactGUI" );

   if ( P.has( "useTriangleSimilarity" ) )
      this.useTriangleSimilarity = P.getBoolean( "useTriangleSimilarity" );

   let dataVersion = undefined;
   if ( P.has( "VERSION" ) )
      dataVersion = P.getString( "VERSION" );

   if ( P.has( "keywords" ) )
   {
      let dataStr = ByteArray.fromBase64( P.getString( "keywords" ) ).toString();
      let keywords = JSON.parse( dataStr );
      this.keywords.list = this.migrateKeywords( keywords, dataVersion );
   }

   if ( fromJSON && P.has( "testExecutionStatus" ) )
      this.executionStatus = JSON.parse( P.getString( "testExecutionStatus" ) );

   if ( !this.automationMode || fromJSON )
   {
      if ( P.has( "groups" ) )
         this.groupsFromStringData( ByteArray.fromBase64( P.getString( "groups" ) ).toString(), dataVersion );
      if ( dataVersion == VERSION || fromJSON )
         if ( P.has( "executionCache" ) )
         {
            console.noteln( "has execution cache" );
            try
            {
               this.executionCache.fromString( ByteArray.fromBase64( P.getString( "executionCache" ) ) );
            }
            catch ( e )
            {
               this.executionCache.reset();
            }
         }
      else
      {
         console.noteln( "execution cache not found" );
      }

      if ( dataVersion )
         this.migrateFrom( dataVersion );
   }
};

// ----------------------------------------------------------------------------

/**
 * Prepare the export of the WBPP parameters to an instance.
 *
 * @param {Boolean} toJSON if true, the functino will returna json object containing the saved parameters
 */
StackEngine.prototype.exportParameters = function( toJSON )
{
   let
   {
      parameters,
      JSONParameters
   } = WBPPUtils.shared();

   let P = Parameters;
   if ( toJSON )
   {
      P = JSONParameters;
      parameters = JSONParameters;
   }

   P.clear();

   P.set( "VERSION", VERSION );

   P.set( "saveFrameGroups", this.saveFrameGroups );
   P.set( "smartNamingOverride", this.smartNamingOverride );
   P.set( "detectMasterIncludingFullPath", this.detectMasterIncludingFullPath );
   P.set( "outputDirectory", this.outputDirectory );
   P.set( "fitsCoordinateConvention", this.fitsCoordinateConvention );
   P.set( "generateRejectionMaps", this.generateRejectionMaps );
   P.set( "preserveWhiteBalance", this.preserveWhiteBalance );

   P.set( "groupingKeywordsEnabled", this.groupingKeywordsEnabled );
   P.set( "showAstrometricInfo", this.showAstrometricInfo );

   P.set( "darkOptimizationLow", this.darkOptimizationLow );
   P.set( "darkExposureTolerance", this.darkExposureTolerance );

   P.set( "overscanEnabled", this.overscan.enabled );

   for ( let i = 0; i < 4; ++i )
   {
      parameters.setIndexed( "overscanRegionEnabled", i, this.overscan.overscan[ i ].enabled );
      parameters.setIndexed( "overscanSourceX0", i, this.overscan.overscan[ i ].sourceRect.x0 );
      parameters.setIndexed( "overscanSourceY0", i, this.overscan.overscan[ i ].sourceRect.y0 );
      parameters.setIndexed( "overscanSourceX1", i, this.overscan.overscan[ i ].sourceRect.x1 );
      parameters.setIndexed( "overscanSourceY1", i, this.overscan.overscan[ i ].sourceRect.y1 );
      parameters.setIndexed( "overscanTargetX0", i, this.overscan.overscan[ i ].targetRect.x0 );
      parameters.setIndexed( "overscanTargetY0", i, this.overscan.overscan[ i ].targetRect.y0 );
      parameters.setIndexed( "overscanTargetX1", i, this.overscan.overscan[ i ].targetRect.x1 );
      parameters.setIndexed( "overscanTargetY1", i, this.overscan.overscan[ i ].targetRect.y1 );
   }

   P.set( "overscanImageX0", this.overscan.imageRect.x0 );
   P.set( "overscanImageY0", this.overscan.imageRect.y0 );
   P.set( "overscanImageX1", this.overscan.imageRect.x1 );
   P.set( "overscanImageY1", this.overscan.imageRect.y1 );

   P.set( "minWeight", this.minWeight );

   for ( let i = 0; i < 4; ++i )
   {
      parameters.setIndexed( "combination", i, this.combination[ i ] );
      parameters.setIndexed( "rejection", i, this.rejection[ i ] );
      parameters.setIndexed( "percentileLow", i, this.percentileLow[ i ] );
      parameters.setIndexed( "percentileHigh", i, this.percentileHigh[ i ] );
      parameters.setIndexed( "sigmaLow", i, this.sigmaLow[ i ] );
      parameters.setIndexed( "sigmaHigh", i, this.sigmaHigh[ i ] );
      parameters.setIndexed( "linearFitLow", i, this.linearFitLow[ i ] );
      parameters.setIndexed( "linearFitHigh", i, this.linearFitHigh[ i ] );
      parameters.setIndexed( "ESD_Outliers", i, this.ESD_Outliers[ i ] );
      parameters.setIndexed( "ESD_Significance", i, this.ESD_Significance[ i ] );
      parameters.setIndexed( "RCR_Limit", i, this.RCR_Limit[ i ] );
   }

   P.set( "flatsLargeScaleRejection", this.flatsLargeScaleRejection );
   P.set( "flatsLargeScaleRejectionLayers", this.flatsLargeScaleRejectionLayers );
   P.set( "flatsLargeScaleRejectionGrowth", this.flatsLargeScaleRejectionGrowth );

   P.set( "lightsLargeScaleRejectionHigh", this.lightsLargeScaleRejectionHigh );
   P.set( "lightsLargeScaleRejectionLayersHigh", this.lightsLargeScaleRejectionLayersHigh );
   P.set( "lightsLargeScaleRejectionGrowthHigh", this.lightsLargeScaleRejectionGrowthHigh );

   P.set( "lightsLargeScaleRejectionLow", this.lightsLargeScaleRejectionLow );
   P.set( "lightsLargeScaleRejectionLayersLow", this.lightsLargeScaleRejectionLayersLow );
   P.set( "lightsLargeScaleRejectionGrowthLow", this.lightsLargeScaleRejectionGrowthLow );

   P.set( "imageRegistration", this.imageRegistration );
   P.set( "lightExposureTolerance", this.lightExposureTolerance );
   P.set( "lightExposureTolerancePost", this.lightExposureTolerancePost );

   P.set( "linearPatternSubtraction", this.linearPatternSubtraction );
   P.set( "linearPatternSubtractionRejectionLimit", this.linearPatternSubtractionRejectionLimit );
   P.set( "linearPatternSubtractionMode", this.linearPatternSubtractionMode );

   P.set( "platesolve", this.platesolve );
   P.set( "platesolveFallbackManual", this.platesolveFallbackManual );
   P.set( "imageSolverRa", this.imageSolverRa );
   P.set( "imageSolverDec", this.imageSolverDec );
   P.set( "imageSolverEpoch", this.imageSolverEpoch );
   P.set( "imageSolverFocalLength", this.imageSolverFocalLength );
   P.set( "imageSolverPixelSize", this.imageSolverPixelSize );
   P.set( "imageSolverForceDefaults", this.imageSolverForceDefaults );

   P.set( "pixelInterpolation", this.pixelInterpolation );
   P.set( "clampingThreshold", this.clampingThreshold );
   P.set( "maxStars", this.maxStars );
   P.set( "distortionCorrection", this.distortionCorrection );
   P.set( "structureLayers", this.structureLayers );
   P.set( "hotPixelFilterRadius", this.hotPixelFilterRadius );
   P.set( "noiseReductionFilterRadius", this.noiseReductionFilterRadius );
   P.set( "minStructureSize", this.minStructureSize );
   P.set( "sensitivity", this.sensitivity );
   P.set( "peakResponse", this.peakResponse );
   P.set( "brightThreshold", this.brightThreshold );
   P.set( "maxStarDistortion", this.maxStarDistortion );
   P.set( "allowClusteredSources", this.allowClusteredSources );
   P.set( "useTriangleSimilarity", this.useTriangleSimilarity );
   P.set( "referenceImage", this.referenceImage );
   P.set( "bestFrameReferenceKeyword", this.bestFrameReferenceKeyword );
   P.set( "bestFrameRefernceMethod", this.bestFrameRefernceMethod );
   P.set( "debayerOutputMethod", this.debayerOutputMethod );

   P.set( "subframeWeightingEnabled", this.subframeWeightingEnabled );
   P.set( "subframeWeightingPreset", this.subframeWeightingPreset );
   P.set( "subframesWeightsMethod", this.subframesWeightsMethod );
   P.set( "FWHMWeight", this.FWHMWeight );
   P.set( "eccentricityWeight", this.eccentricityWeight );
   P.set( "SNRWeight", this.SNRWeight );
   P.set( "starsWeight", this.starsWeight );
   P.set( "PSFSignalWeight", this.PSFSignalWeight );
   P.set( "PSFSNRWeight", this.PSFSNRWeight );
   P.set( "pedestal", this.pedestal );

   P.set( "localNormalization", this.localNormalization );
   P.set( "localNormalizationInteractiveMode", this.localNormalizationInteractiveMode );
   P.set( "localNormalizationGenerateImages", this.localNormalizationGenerateImages );
   P.set( "localNormalizationMethod", this.localNormalizationMethod );
   P.set( "localNormalizationMaxIntegratedFrames", this.localNormalizationMaxIntegratedFrames );
   P.set( "localNormalizationBestReferenceSelectionMethod", this.localNormalizationBestReferenceSelectionMethod );
   P.set( "localNormalizationGridSize", this.localNormalizationGridSize );
   P.set( "localNormalizationRerenceFrameGenerationMethod", this.localNormalizationRerenceFrameGenerationMethod );
   P.set( "localNormalizationPsfType", this.localNormalizationPsfType );
   P.set( "localNormalizationPsfGrowth", this.localNormalizationPsfGrowth );
   P.set( "localNormalizationPsfMaxStars", this.localNormalizationPsfMaxStars );
   P.set( "localNormalizationPsfMinSNR", this.localNormalizationPsfMinSNR );
   P.set( "localNormalizationPsfAllowClusteredSources", this.localNormalizationPsfAllowClusteredSources );
   P.set( "localNormalizationLowClippingLevel", this.localNormalizationLowClippingLevel );
   P.set( "localNormalizationHighClippingLevel", this.localNormalizationHighClippingLevel );

   P.set( "integrate", this.integrate );
   P.set( "autocrop", this.autocrop );
   P.set( "recombineRGB", this.recombineRGB );
   P.set( "debayerActiveChannelR", this.debayerActiveChannelR );
   P.set( "debayerActiveChannelG", this.debayerActiveChannelG );
   P.set( "debayerActiveChannelB", this.debayerActiveChannelB );

   P.set( "usePipelineScript", this.usePipelineScript );
   P.set( "pipelineScriptFile", this.pipelineScriptFile );

   P.set( "enableCompactGUI", this.enableCompactGUI );

   P.set( "keywords", new ByteArray( JSON.stringify( this.keywords.list ) ).toBase64() );

   P.set( "groups", new ByteArray( this.groupsToStringData() ).toBase64() );
   P.set( "executionCache", new ByteArray( this.executionCache.toString() ).toBase64() );

   if ( toJSON )
      P.set( "testExecutionStatus", JSON.stringify( this.executionStatus ||
      {} ) );

   if ( toJSON )
      return JSON.stringify( P, null, 2 );
   return undefined;
};

var engine = new StackEngine;
// default parameters
engine.setDefaultParameters();

// ----------------------------------------------------------------------------
// EOF WeightedBatchPreprocessing-engine.js - Released 2023-06-15T09:00:12Z

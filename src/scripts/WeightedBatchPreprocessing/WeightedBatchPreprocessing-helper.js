// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// WeightedBatchPreprocessing-helper.js - Released 2023-06-15T09:00:12Z
// ----------------------------------------------------------------------------
//
// This file is part of Weighted Batch Preprocessing Script version 2.5.10
//
// Copyright (c) 2019-2023 Roberto Sartori
// Copyright (c) 2020-2021 Adam Block
// Copyright (c) 2019 Tommaso Rubechi
// Copyright (c) 2012 Kai Wiechen
// Copyright (c) 2012-2023 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/*
 * Helper routines
 */

// ----------------------------------------------------------------------------
// Extensions to the Array object
//
// NB: Define new methods of the Array object as nonenumerable properties with
//     Object.defineProperty() to prevent problems with "for...in" constructs.
// ----------------------------------------------------------------------------

var helperFunctions = () => (
{
   /**
    * Returns true if the version string a is lower than b.
    *
    * @param {*} a
    * @param {*} b
    * @return {*}
    */
   versionLT: ( a, b ) =>
   {
      let at = a.split( "." ).map( i => parseInt( i ) );
      let bt = b.split( "." ).map( i => parseInt( i ) );
      for ( let i = 0; i < Math.max( at.length, bt.length ); i++ )
      {
         let ai = ( at[ i ] || 0 );
         let bi = ( bt[ i ] || 0 );
         if ( bi > ai )
            return true;
         if ( bi < ai )
            return false;
      }
      return false;
   },

   /**
    * Returns an array of enabled target frames.
    * Used to build the input for ImageCalibration/ImageIntegration/StarAlignment
    */
   enableTargetFrames: function( array, ncolumns, enableDrizzle, enableLN )
   {
      let target = new Array;
      for ( let i = 0; i < array.length; ++i )
      {
         target[ i ] = new Array( ncolumns );
         for ( let j = 0; j < ncolumns - 1; ++j )
            target[ i ][ j ] = true;

         if ( typeof array[ i ] == 'string' )
         {
            target[ i ][ ncolumns - 1 ] = array[ i ];
            if ( enableDrizzle )
               target[ i ][ ncolumns ] = File.changeExtension( array[ i ], '.xdrz' );
            if ( enableLN )
               target[ i ][ ncolumns + 1 ] = File.changeExtension( array[ i ], '.xnml' );
         }
         else // assume file item
         {
            target[ i ][ ncolumns - 1 ] = array[ i ].current;
            if ( enableDrizzle )
               target[ i ][ ncolumns ] = File.changeExtension( array[ i ].current, '.xdrz' );
            if ( enableLN )
            {
               if ( array[ i ].localNormalizationFile != undefined )
                  target[ i ][ ncolumns + 1 ] = array[ i ].localNormalizationFile
               else
                  target[ i ][ ncolumns + 1 ] = File.changeExtension( array[ i ].current, '.xnml' );
            }
         }


      }
      return target;
   },

   /**
    * Checks if a valid Cosmetic Corection icon name is provided by checking the
    * existence of a correspondent valid icon on the workspace.
    *
    * @param {*} name the Cosmetic Corerction icon name to be checked
    * @return {*} true if a valid Cosmetic Correction icon exists, false otherwise
    */
   validCCIconName: function( name )
   {
      let icons = ProcessInstance.iconsByProcessId( "CosmeticCorrection" );
      for ( let i = 0; i < icons.length; ++i )
         if ( name == icons[ i ] )
            return true;
      return false;
   },

   // ----------------------------------------------------------------------------
   // String Utils
   // ----------------------------------------------------------------------------

   /**
    * Returns a string describing the count of resulted files.
    *
    * @param {Number} nCached the number of cached files
    * @param {Number} nCreated the number of created files
    * @param {Number} nFailed the number of failed files
    * @param {String} generatedLabel the optional label to be used for generated files
    * @return {String} the readable string
    */
   resultCountToString: function( nCached, nCreated, nFailed, generatedLabel )
   {
      if ( nCached == 0 && nFailed == 0 )
         return "";

      let result = "";
      let separator = "";

      if ( nCreated > 0 )
      {
         result += nCreated + " " + ( generatedLabel || "created" );
         separator = ", ";
      }
      if ( nCached > 0 )
      {
         result += separator + nCached + " cached";
         separator = ", ";
      }
      if ( nFailed > 0 )
         result += separator + nFailed + " failed";

      return result;
   },
   /**
    * Returns a string representing the current time stamp.
    *
    */
   timestampString: function()
   {
      let logDate = new Date;
      return format( "%04d%02d%02d%02d%02d%02d",
         logDate.getUTCFullYear(), logDate.getUTCMonth() + 1, logDate.getUTCDate(),
         logDate.getUTCHours(), logDate.getUTCMinutes(), logDate.getUTCSeconds() );
   },
   /**
    * Returns a h:mm:s formatted string representing the value as a duration
    *
    * @param {*} value
    */
   formatTimeDuration: function( duration )
   {
      let hours = Math.floor( duration / 3600 );
      let min = Math.floor( ( duration - hours * 3600 ) / 60 );
      let sec = duration % 60;
      return format( "%4i", hours ) + "h " + format( "%2i", min ) + "min " + format( "%2i", sec ) + "sec";
   },

   /**
    * Returns a string representing a human-readable elapsed time string.
    *
    * @param {Float} elapsed elapsed seconds
    */
   elapsedTimeToString: function( value )
   {
      let T = Math.trunc( value );
      let ms = value - Math.floor( value );
      let sec = T % 60;
      let min = Math.trunc( ( T - sec ) / 60 ) % 60;
      let hours = Math.trunc( ( T - sec - min * 60 ) / 3600 );

      // short times below 1 second are represented in ms
      if ( hours == 0 && min == 0 && sec == 0 )
         return Math.trunc( ms * 1000 ) + " ms";

      if ( hours == 0 )
      {
         if ( min == 0 )
            // less than 1 minute is represented in simple seconds onlym with the given precision
            return sec.toFixed( 0 ) + " s";
         else
            // minutes are represented with fixed mm:ss (rounded)
            return format( "%02i:", min ) + format( "%02i", sec );
      }
      else
         // hour is represented as hh:mm:ss
         return format( "%02i:", hours ) + format( "%02i:", min ) + format( "%02i:", sec );

   },

   /**
    * Returns the given size (in bytes) in a readable format string
    *
    * @param {*} _size size in bytes
    * @param {*} _padding the padding to be used
    * @return {*} the readable string
    */
   readableSize: function( _size, _padding )
   {
      let padding = _padding != undefined ? _padding : 12;
      let size = _size * 1.0;
      let Kb = 1024.0;
      let Mb = Kb * 1024.0;
      let Gb = Mb * 1024.0;
      let str;
      if ( size < Mb )
         str = format( "%.2f KiB", size / Kb );
      else if ( size < Gb )
         str = format( "%.2f MiB", size / Mb );
      else
         str = format( "%.2f GiB", size / Gb );
      return " ".repeat( Math.max( 0, padding - str.length ) ) + str;
   },

   /*
    * Returns a clean filter name.
    */
   cleanFilterName: function( str )
   {
      let sanitizeRegExp = new RegExp( "[^" + KEYWORD_VALUE_CHARSET + "]", "gi" );
      let returnStr;
      try
      {
         returnStr = str.replace( sanitizeRegExp, '-' );
      }
      catch ( e )
      {
         console.warningln( "*** clean filter name raised an exception on string <", str, "> with regexp ", sanitizeRegExp );
         returnStr = str;
      }
      return returnStr;
   },

   /*
    * Returns true if this string is empty.
    */
   isEmptyString: function( str )
   {
      return str == undefined || str.length <= 0;
   },

   /*
    * Returns true if this string contains the specified substring.
    */
   stringHas: function( str, s )
   {
      return str.indexOf( s ) > -1;
   },

   /*
    * Add the given padding to a string
    */
   paddedStringNumber: function( str, padding )
   {
      let intPart = str.split( '.' );
      let P = Math.max( 0, padding - intPart[ 0 ].length );
      let Pad = " ".repeat( P );
      return Pad + str;
   },

   /**
    * Append a sequence of chars to a label to span the given length
    *
    * @param {*} label the main label aligned on the left
    * @param {*} N the total length to reach
    * @param {*} char the filler char
    * @return {*} the padded label
    */
   paddedLabel: function( label, N, char )
   {
      return label + ( char || "." ).repeat( Math.max( 0, N - label.length ) );
   },

   /**
    * Copy the astrometric solution from a source image to a target image.
    *
    * Only copies valid solutions that include spline-based world
    * transformations with distortion corrections.
    *
    * @param {ImageWindow} source the image window containing the astrometric solution to copy.
    * @param {ImageWindow} target the target image window the astrometric solution is copied to.
    */
   copyAstrometricSolution: function( source, target )
   {
      if ( source.mainView.hasProperty( "PCL:AstrometricSolution:SplineWorldTransformation" ) ||
           source.mainView.hasProperty( "AstrometricSolution:SplineWorldTransformation" ) )
         target.copyAstrometricSolution( source );
   },

   /**
    * Returns true iff the specified image \a window has a valid astrometric
    * solution (linear or spline-based).
    */
   hasAstrometricSolution: function( window )
   {
      return window.astrometricSolutionSummary().length > 0;
   },

   /**
    * Checks if the image has metadata for a spline-based world transformation
    * with distortion corrections.
    *
    * @param {*} source
    * @return {*}
    */
   hasSplineAstrometricSolutionMetadata: function( window )
   {
      return (window.mainView.hasProperty( "PCL:AstrometricSolution:SplineWorldTransformation" ) ||
              window.mainView.hasProperty( "AstrometricSolution:SplineWorldTransformation" ));
   },

   // ----------------------------------------------------------------------------
   // File Utils
   // ----------------------------------------------------------------------------

   /**
    * Creates a directory if it does not exist.
    * Returns the directory path.
    */
   existingDirectory: function( dir )
   {
      if ( !File.directoryExists( dir ) )
         File.createDirectory( dir );
      return dir;
   },

   getLastModifiedDate: function( files )
   {
      if ( files == undefined )
         return null;
      let singleFile = false;
      if ( typeof files == typeof "" )
      {
         files = [ files ];
         singleFile = true;
      }
      let result = [];
      let fileInfo = new FileInfo();
      for ( let i = 0; i < files.length; i++ )
      {
         let lmd = null;
         if ( files[ i ].length > 0 && File.exists( files[ i ] ) )
         {
            fileInfo.refresh( files[ i ] );
            lmd = fileInfo.lastModified.toISOString();
         }
         result.push( lmd );
      }
      return singleFile ? result[ 0 ] : result;
   },

   /**
    * Returns the image width, height.
    *
    * @param {*} filePath
    * @return {*}
    */
   getImageSize: function( filePath, inputHints )
   {
      try
      {
         let suffix = File.extractExtension( filePath ).toLowerCase();
         let F = new FileFormat( suffix, true /*toRead*/ , false /*toWrite*/ );
         if ( F.isNull )
            throw new Error( "No installed file format can read \'" + suffix + "\' files." );

         let f = new FileFormatInstance( F );
         if ( f.isNull )
            throw new Error( "Unable to instantiate file format: " + F.name );

         let description = f.open( filePath, inputHints || "" /*inputHints*/ );
         if ( description.length < 1 )
            throw new Error( "Unable to open file: " + filePath );
         let width = description[ 0 ].width;
         let height = description[ 0 ].height;
         f.close();

         return {
            width: width,
            height: height
         }

      }
      catch ( error )
      {
         //
         return undefined;
      }

   },

   keywords:
   {
      readFileKeywords: function( filePath )
      {
         let ext = File.extractExtension( filePath ).toLowerCase();
         let F = new FileFormat( ext, true /*toRead*/ , false /*toWrite*/ );
         if ( F.isNull ) // shouldn't happen
            return {
               success: false,
               message: "No installed file format can read \'" + ext + "\' files."
            };
         let f = new FileFormatInstance( F );
         if ( f.isNull )
            return {
               success: false,
               message: "Unable to instantiate file format: " + F.name
            };

         let info = f.open( filePath, "verbosity 0" ); // do not fill the console with useless messages
         if ( !info || ( info && info.length <= 0 ) )
            return {
               success: false,
               message: "Unable to open input file: " + filePath
            };

         let keywords = [];
         if ( F.canStoreKeywords )
            keywords = f.keywords;

         f.close();

         return {
            success: true,
            keywords: keywords
         };
      },

      readFileKeyword: function( filePath, keyword )
      {
         let result = this.readFileKeywords( filePath );
         if ( result.success )
         {
            let upKey = keyword.toUpperCase();
            for ( let i = 0; i < result.keywords.length; ++i )
            {
               if ( result.keywords[ i ].name.toUpperCase() == upKey )
                  return result.keywords[ i ].value;
            }
         }
         return undefined;
      },
   },

   /**
    * Returns the name and extension components of a path specification.
    */
   extractNameAndExtension: function( path )
   {
      return File.extractName( path ) + File.extractExtension( path );
   },

   /**
    * Returns an existing and unique file path given the desired file name and the output folder.
    * In order not to overwrite existing files, incremental indeces are appended to the file name
    * until uniquess is gauranteed.
    *
    * @param {String} outputFolder the folder containing the file name, must be non empty.
    * @param {String} fileName the desired file name including the file extension.
    */
   existingAndUniqueFileName: function( outputFolder, fileName )
   {
      if ( !File.directoryExists( outputFolder ) )
         File.createDirectory( outputFolder );
      let filePath = outputFolder + "/" + fileName;
      let uniqueFilePath;
      let j = 0;
      let postFix = "";
      do {
         uniqueFilePath = File.appendToName( filePath, postFix );
         j++;
         postFix = "_(" + j + ")";
      }
      while ( File.exists( uniqueFilePath ) )
      return uniqueFilePath;
   },

   // ----------------------------------------------------------------------------
   // Smart Naming Helpers
   // ----------------------------------------------------------------------------

   smartNaming:
   {
      lastMatching: function( text, regexp )
      {
         let matches = text.match( regexp )
         if ( matches != null && matches.length > 0 )
         {
            return matches[ matches.length - 1 ];
         }
         return undefined;
      },

      /*
       * Extract the master property from the file Path.
       *
       * fileName must contain the "master" string in its name in order to be recognized
       * as a master file from SmartNaming.
       *
       * NOTE: The check is case-insensitive
       */
      isMasterFromPath: function( filePath )
      {
         let regexp = /(MASTER)/gi;
         return this.lastMatching( filePath, regexp ) != undefined;
      },

      /*
       * Extract the image type from the last matching pattern occurrence in its
       * filePath.
       *
       * filePath must contain one pr more of BIAS, DARK, FLAT, LIGHT.
       * The last of the sequence will be taken. It is useful to check the
       * whole path since instead of renaming single files it's possible to put all
       * light files into an enclosing folder with the word "lights" in the name.
       *
       * NOTE: Negative look behind is not supported in JS so the first char
       * before the keywords is matched and removed in the switch.
       */
      geImageTypeFromPath: function( filePath )
      {
         let regexp = /(?![a-z0-9]).{0,1}(BIAS|DARK|DARKS|FLAT|FLATS|LIGHT|LIGHTS)(?![a-z0-9])/gi;
         let fileType = this.lastMatching( filePath, regexp );
         if ( fileType )
         {
            switch ( fileType.substr( 1 ).toUpperCase() )
            {
               case 'BIAS':
                  return ImageType.BIAS;
               case 'DARK':
               case 'DARKS':
                  return ImageType.DARK;
               case 'FLAT':
               case 'FLATS':
                  return ImageType.FLAT;
               case 'LIGHT':
               case 'LIGHTS':
                  return ImageType.LIGHT;
            }
         }

         return ImageType.UNKNOWN;
      },

      sanitizeKeywordValue: function( value )
      {
         let sanitizeRegExp = new RegExp( "[^" + KEYWORD_VALUE_CHARSET + "]", "gi" );
         return value.replace( sanitizeRegExp, '-' );
      },

      /*
       * Extract the exposure time from the last matching pattern occurrence in its filePath.
       * Exposure can be specified by means of he
       */
      getExposureTimeFromPath: function( filePath )
      {
         // match the format EXPTIME_10.0 or EXPOSURE_2
         let regexp = /(EXPTIME|EXPOSURE)(_|-| )[0-9]+(\.[0-9]*)?/gi;
         let match = this.lastMatching( filePath, regexp );
         if ( match )
         {
            let sanitizedStrValue = match.replace( /(EXPTIME|EXPOSURE)(_|-| )/gi, '' );
            let value = Number( sanitizedStrValue );
            return value !== NaN ? value : 0
         }
         // find any number followed by 's' or 'sec' ore '_secs' like 2s, 2.1_secs or 2.2sec
         let postfixes = [ 's', 'sec', '_secs' ];
         regexp = /[0-9]+(\.[0-9]*)?(?=(s|sec|_secs)[^a-zA-Z0-9])/gi;
         let matches = regexp.exec( filePath );
         if ( matches !== null )
         {
            let sanitizedStr = matches[ 0 ];
            postfixes.forEach( postfix =>
            {
               sanitizedStr = sanitizedStr.replace( postfix, '' );
            } );
            let value = Number( sanitizedStr );
            return value !== NaN ? value : 0
         }
         return 0;
      },

      /*
       * Extract the binning from the last matching pattern occurrence in its filePath.
       */
      getBinningFromPath: function( filePath )
      {
         let regexp = /(XBINNING|CCDBINX|BINNING)(_|-| )[0-9]+/gi;
         let match = this.lastMatching( filePath, regexp );
         if ( match )
         {
            let sanitizedStrValue = match.replace( /(XBINNING|CCDBINX|BINNING)(_|-| )/gi, '' );
            let value = Number( sanitizedStrValue );
            return value !== NaN ? value : 1
         }
         return 1
      },

      /*
       * Extract the filter name from the last matching pattern occurrence in its filePath.
       * Poissible valid combinations within the path are:
       * - FILTER NebulaBooster
       * - FILTER-Ha
       * - INSFLNAM_L
       */
      getFilterFromPath: function( filePath )
      {
         let regexp = /(FILTER|INSFLNAM)(_|-| )[- a-zA-Z0-9]+/gi;
         let match = this.lastMatching( filePath, regexp );
         if ( match )
            return match.replace( /(FILTER|INSFLNAM)(_|-| )/gi, '' );
         return undefined;
      },

      /*
       * Extract a custom key-value from the last matching pattern occurrence in its filePath.
       * Value is limited to contiguous alphanumeric characters wrapped by by -,_ or space:
       */
      getCustomKeyValueFromPath: function( key, filePath )
      {
         // remove the extension
         let extension = File.extractExtension( filePath );
         let sanitizedFilePath = filePath.slice( 0, -extension.length );
         // search the existence of the key.value pair
         let regexp = new RegExp( "[^a-zA-Z0-9](" + key + ")(_|-| )[" + KEYWORD_VALUE_CHARSET + "]+", "gi" );
         let match = this.lastMatching( sanitizedFilePath, regexp );
         if ( match )
         {
            let subsRegExp = new RegExp( "[^a-zA-Z0-9](" + key + ")(_|-| )", "gi" );
            let sanitizeRegExp = new RegExp( "[^" + KEYWORD_VALUE_CHARSET + "]", "gi" );
            return match.replace( subsRegExp, '' ).replace( sanitizeRegExp, "-" );
         }
         return undefined;
      },
   },

   computeSTF: function( view, descriptor, forSTF, c0Add, mAdd )
   {

      // default c0Coeff value
      if ( c0Add == undefined )
         c0Add = 0;

      // default mCoeff value
      if ( mAdd == undefined )
         mAdd = 0;
      if ( descriptor == undefined )
         descriptor = {};

      // Shadows clipping point in (normalized) MAD units from the median.
      let shadowsClipping = -2.80;
      // Target mean background in the [0,1] range.
      let targetBackground = 0.25;
      // Apply the same STF to all nominal channels (true), or treat each channel
      // separately (false).
      let rgbLinked = false;

      var n = view.image.isColor ? 3 : 1;
      let median = [];
      let mad = [];
      if ( descriptor.median != undefined && descriptor.mad != undefined && n == 1 )
      {
         median = [ descriptor.median ];
         mad = [ descriptor.mad ];
      }
      // pick the arrays from the descriptor if present
      if ( descriptor.rgbMad != undefined && descriptor.rgbMedian != undefined )
      {
         median = descriptor.rgbMedian;
         mad = descriptor.rgbMad;
      }
      else
      {
         let medianValues = view.computeOrFetchProperty( "Median" );
         let madValues = view.computeOrFetchProperty( "MAD" );
         for ( let c = 0; c < n; c++ )
         {
            median[ c ] = medianValues.at( c );
            mad[ c ] = madValues.at( c );
         }
         // store the arrays in the descriptor for a future reuse
         descriptor.rgbMedian = median;
         descriptor.rgbMad = mad;
      }

      // ensure that the media is never negative
      median = median.map( ( item ) => Math.max( 0.00001, item ) );

      let stf = new Array;

      if ( rgbLinked || n == 1 )
      {
         /*
          * Noninverted image
          */
         var c0 = 0,
            m = 0;

         for ( var c = 0; c < n; ++c )
         {
            if ( 1 + mad[ c ] != 1 )
               c0 += median[ c ] + shadowsClipping * mad[ c ];
            m += median[ c ];
         }
         c0 = Math.range( c0 / n + c0Add, 0.0, 1.0 );
         m = Math.range( Math.mtf( targetBackground, m / n - c0 ) + mAdd, 0.0, 1.0 );

         if ( n == 1 )
         {
            stf = [ // c0, m, c1, r0, r1
               [ 0, 0.5, 1, 0, 1 ],
               [ 0, 0.5, 1, 0, 1 ],
               [ 0, 0.5, 1, 0, 1 ],
               [ c0, m, 1, 0, 1 ],
               [ 0, 0.5, 1, 0, 1 ]
            ];
         }
         else
         {
            stf = [ // c0, m, c1, r0, r1
               [ c0, m, 1, 0, 1 ],
               [ c0, m, 1, 0, 1 ],
               [ c0, m, 1, 0, 1 ],
               [ 0, 0.5, 1, 0, 1 ],
               [ 0, 0.5, 1, 0, 1 ]
            ];
         }
      }
      else
      {
         /*
          * Unlinked RGB channnels: Compute automatic stretch functions for
          * individual RGB channels separately.
          */
         var A = [ // c0, m, c1, r0, r1
            [ 0, 0.5, 1, 0, 1 ],
            [ 0, 0.5, 1, 0, 1 ],
            [ 0, 0.5, 1, 0, 1 ],
            [ 0, 0.5, 1, 0, 1 ],
            [ 0, 0.5, 1, 0, 1 ]
         ];

         for ( var c = 0; c < n; ++c )
         {
            /*
             * Noninverted channel
             */
            var c0 = ( 1 + mad[ c ] != 1 ) ? Math.range( median[ c ] + shadowsClipping * mad[ c ], 0.0, 1.0 ) : 0.0;
            var m = Math.mtf( targetBackground, median[ c ] - c0 );
            c0 = Math.range( c0 + c0Add, 0.0, 1.0 );
            m = Math.range( m + mAdd, 0.0, 1.0 );
            A[ c ] = [ c0, m, 1, 0, 1 ];
         }

         stf = A;
      }

      if ( forSTF )
      {
         for ( let i = 0; i < 5; i++ )
         {
            let tmp = stf[ i ][ 0 ];
            stf[ i ][ 0 ] = stf[ i ][ 1 ];
            stf[ i ][ 1 ] = tmp;
         }
      }

      return stf;
   },

   // ----------------------------------------------------------------------------
   // Extensions to the Parameters object
   // ----------------------------------------------------------------------------

   JSONParameters:
   {
      clear: function()
      {
         this.data = {};
      },
      set: function( key, object )
      {
         this.data[ key ] = object;
      },
      setIndexed: function( key, index, object )
      {
         if ( this.data[ key ] == undefined )
            this.data[ key ] = [];
         this.data[ key ][ index ] = object;
      },
      fromJSONtest: function( json )
      {
         let data = JSON.parse( json );
         if ( data == undefined )
            return;
         this.data = {};
         Object.keys( data.data ).forEach( key =>
         {
            this.data[ key ] = data.data[ key ];
         } );
      },
      has: function( key )
      {
         return this.data[ key ] != undefined
      },
      hasIndexed: function( key, index )
      {
         return this.data[ key ] != undefined && typeof( this.data[ key ] ) == "array" && this.data[ key ].length > index;
      },
      getBoolean: function( key )
      {
         return !!this.data[ key ];
      },
      getInteger: function( key )
      {
         return parseInt( this.data[ key ] );
      },
      getReal: function( key )
      {
         return parseFloat( this.data[ key ] );
      },
      getString: function( key )
      {
         return this.data[ key ];
      },
      getBooleanIndexed: function( key, index )
      {
         return !!this.data[ key ][ index ];
      },
      getIntegerIndexed: function( key, index )
      {
         return parseInt( this.data[ key ][ index ] );
      },
      getRealIndexed: function( key, index )
      {
         return parseFloat( this.data[ key ][ index ] );
      }
   },
   parameters:
   {
      indexedId: function( id, index )
      {
         return id + '_' + ( index + 1 ).toString(); // make indexes one-based
      },
      hasIndexed: function( id, index )
      {
         return Parameters.has( this.indexedId( id, index ) );
      },
      getBooleanIndexed: function( id, index )
      {
         return Parameters.getBoolean( this.indexedId( id, index ) );
      },
      getIntegerIndexed: function( id, index )
      {
         return Parameters.getInteger( this.indexedId( id, index ) );
      },
      getRealIndexed: function( id, index )
      {
         return Parameters.getReal( this.indexedId( id, index ) );
      },
      getStringIndexed: function( id, index )
      {
         return Parameters.getString( this.indexedId( id, index ) );
      },
      getUIntIndexed: function( id, index )
      {
         return Parameters.getUInt( this.indexedId( id, index ) );
      },
      getStringList: function( id )
      {
         let list = new Array();
         if ( Parameters.has( id ) )
         {
            let s = Parameters.getString( id );
            list = s.split( ':' );
            for ( let i = 0; i < list.length; ++i )
               list[ i ] = list[ i ].trim();
         }
         return list;
      },
      setIndexed: function( id, index, value )
      {
         return Parameters.set( this.indexedId( id, index ), value );
      },
   },
   factory:
   {
      CPTreeBox: function( parent )
      {
         let treeBox = new TreeBox( parent );
         treeBox.rootDecoration = false;
         treeBox.headerVisible = true;
         treeBox.alternateRowColor = true;
         treeBox.numberOfColumns = 1;
         return treeBox;
      },
      CPHeader: function( parent, title )
      {
         let label = new Label( parent );
         label.text = "<b>" + title + "</b>";
         label.margin = 8;
         label.spacing = 8;
         label.useRichText = true;
         return label;
      },
      CPMasterSelectionControl: function( parent, title )
      {
         let control = new Control( parent );
         let label = new Label( control );
         label.useRichText = true;
         label.text = title;
         label.setFixedWidth( 34 );
         label.textAlignment = TextAlign_VertCenter;
         let combo = new ComboBox( control );
         combo.setVariableWidth();
         control.sizer = new HorizontalSizer;
         control.sizer.add( label );
         control.sizer.add( combo );
         return combo;
      }
   }
} );

var WBPPUtils = ( function()
{
   this.instance = null;

   function createInstance()
   {
      return helperFunctions();
   }

   return {
      shared: function()
      {
         if ( !this.instance )
            this.instance = createInstance();
         return this.instance;
      }
   };
} )();

/*
 * Secure directory removal routine.
 */
function removeDirectoryAndContent( dirPath )
{
   function removeDirectory_recursive( dirPath, baseDir )
   {
      if ( dirPath.indexOf( ".." ) >= 0 )
         throw new Error( "removeDirectory(): Attempt to climb up the filesystem." );
      if ( dirPath.indexOf( baseDir ) != 0 )
         throw new Error( "removeDirectory(): Attempt to redirect outside the base directory." );
      if ( !File.directoryExists( dirPath ) )
         throw new Error( "removeDirectory(): Attempt to remove a nonexistent directory." );

      let currentDir = dirPath;
      if ( currentDir[ currentDir.length - 1 ] != '/' )
         currentDir += '/';

      let f = new FileFind;
      if ( f.begin( currentDir + "*" ) )
         do {
            let itemPath = currentDir + f.name;
            if ( f.isDirectory )
            {
               if ( f.name != "." && f.name != ".." )
               {
                  removeDirectory_recursive( itemPath, baseDir );
                  File.removeDirectory( itemPath );
               }
            }
            else
            {
               File.remove( itemPath );
            }
         }
         while ( f.next() );
   }

   if ( dirPath.indexOf( '/' ) != 0 )
      throw new Error( "removeDirectory(): Relative directory." );
   if ( !File.directoryExists( dirPath ) )
      throw new Error( "removeDirectory(): Nonexistent directory." );

   // Remove all files and subdirectories recursively
   removeDirectory_recursive( dirPath, dirPath );

   File.removeDirectory( dirPath );
}

/*
 * FileList
 *
 * Recursively search a directory tree for all existing files with the
 * specified file extensions.
 */
function FileList( dirPath, extensions, verbose )
{
   /*
    * Regenerate this file list for the specified base directory and file
    * extensions.
    */
   this.regenerate = function( dirPath, extensions, verbose )
   {
      // Security check: Do not allow climbing up a directory tree.
      if ( dirPath.indexOf( ".." ) >= 0 )
         throw new Error( "FileList: Attempt to redirect outside the base directory: " + dirPath );

      // The base directory is the root of our search tree.
      this.baseDirectory = File.fullPath( dirPath );
      if ( this.baseDirectory.length == 0 )
         throw new Error( "FileList: No base directory has been specified." );

      // The specified directory can optionally end with a separator.
      if ( this.baseDirectory[ this.baseDirectory.length - 1 ] == '/' )
         this.baseDirectory.slice( this.baseDirectory.length - 1, -1 );

      // Security check: Do not try to search on a nonexisting directory.
      if ( !File.directoryExists( this.baseDirectory ) )
         throw new Error( "FileList: Attempt to search a nonexistent directory: " + this.baseDirectory );

      // If no extensions have been specified we'll look for all existing files.
      if ( extensions == undefined || extensions == null || extensions.length == 0 )
         extensions = [ '' ];

      if ( verbose )
      {
         console.writeln( "<end><cbr><br>==> Finding files from base directory:" );
         console.writeln( this.baseDirectory );
      }

      // Find all files with the required extensions in our base tree recursively.
      this.files = [];
      for ( let i = 0; i < extensions.length; ++i )
         this.files = this.files.concat( searchDirectory( this.baseDirectory + "/*" + extensions[ i ], true /*recursive*/ ) );
   };

   this.baseDirectory = "";
   this.files = [];
   this.index = [];

   if ( dirPath != undefined )
      this.regenerate( dirPath, extensions, verbose );

   if ( verbose )
   {
      console.writeln( "<end><cbr>" + this.files.length + " file(s) found:" );
      for ( let i = 0; i < this.files.length; ++i )
         console.writeln( this.files[ i ] );
   }
}
FileList.prototype = new Object;

// ----------------------------------------------------------------------------
// EOF WeightedBatchPreprocessing-helper.js - Released 2023-06-15T09:00:12Z

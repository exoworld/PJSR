// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// WeightedBatchPreprocessing-testsRunner.js - Released 2023-06-15T09:00:12Z
// ----------------------------------------------------------------------------
//
// This file is part of Weighted Batch Preprocessing Script version 2.5.10
//
// Copyright (c) 2019-2023 Roberto Sartori
// Copyright (c) 2020-2021 Adam Block
// Copyright (c) 2019 Tommaso Rubechi
// Copyright (c) 2012 Kai Wiechen
// Copyright (c) 2012-2023 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/* beautify ignore:start */
#include <pjsr/DataType.jsh>
#include <pjsr/Sizer.jsh>
#include "WeightedBatchPreprocessing-helper.js"

#define TESTS_RUNNER_SETTINGS_KEY_BASE  "WeightedBatchPreprocessing/TestsRunner/"

/* beautify ignore:end */

/**
 * Main window dialog.
 *
 * @param {*} engine the WBPP test runner engine
 */
let WBPPTestRunnerDialog = function( engine )
{
   this.__base__ = Dialog;
   this.__base__();

   let
   {
      elapsedTimeToString,
      smartNaming
   } = WBPPUtils.shared();

   this.windowTitle = "WBPP Tests Runner";

   // ===============
   // MAIN PARAMETERS
   // ===============

   this.currentNode = undefined; // the currently selected node
   this.waitingTest = false; // true when the GUI is monitoring the execution of a test and waiting it to complete

   // ==================
   // HANDLING FUNCTIONS
   // ==================

   /**
    * Redraws the GUI controls state, filling the list of trees and the directory locations.
    *
    */
   this.redraw = () =>
   {
      let tests = engine.tests;
      this.filesTreeBox.clear();

      let selectedIndex = this.currentNode && this.currentNode.__i__ || -1;

      for ( let i = 0; i < tests.length; i++ )
      {
         let test = tests[ i ];
         let node = new TreeBoxNode( this.filesTreeBox );

         node.setText( 0, test.filePath );
         node.checkable = true;
         node.checked = test.enabled;
         node.__test__ = test;
         node.__i__ = i;

         if ( selectedIndex == i )
         {
            node.selected = true;
            this.currentNode = node;
         }

         switch ( test.status )
         {
            case engine.STATUS_READY:
               node.setIcon( 1, this.scaledResource( ":/bullets/bullet-ball-glass-grey.png" ) );
               break;
            case engine.STATUS_SUCCECSS:
               node.setIcon( 1, this.scaledResource( ":/bullets/bullet-ball-glass-green.png" ) );
               break;
            case engine.STATUS_FAILED:
               node.setIcon( 1, this.scaledResource( ":/icons/warning.png" ) );
               break;
            case engine.STATUS_RUNNING:
               node.setIcon( 1, this.scaledResource( ":/icons/forward.png" ) );
               break;
         }

         node.setText( 2, elapsedTimeToString( test.eta || 0 ) );

         if ( test.status == engine.STATUS_RUNNING )
            node.setText( 3, test.progressInfo );
         else
            node.setText( 3, test.notes );
      }

      for ( let i = 0; i < this.filesTreeBox.numberOfColumns; ++i )
         this.filesTreeBox.adjustColumnWidthToContents( i );

      // update the settings
      this.workingDirectoryEdit.text = engine.workingDirectory;
      this.sourcesDirectoryEdit.text = engine.sourcesDirectory;
      this.executableEdit.text = engine.executable;
      this.testCheckerScriptEdit.text = engine.testCheckerScript;
      this.keepPIOpenCheckBox.checked = engine.keepPIOpen;
      this.updateNotes();
      processEvents();
      gc();
   };

   /**
    * Updates the state of the GUI depending on the running state.
    *
    * @param {*} running
    */
   this.setRunningStatus = ( running ) =>
   {
      let enabled = !running;
      this.workingFolderButton.enabled = enabled;
      this.workingDirectoryEdit.enabled = enabled;
      this.sourcesDirectoryButton.enabled = enabled;
      this.sourcesDirectoryEdit.enabled = enabled;
      this.executableButton.enabled = enabled;
      this.executableEdit.enabled = enabled;
      this.testCheckerScriptButton.enabled = enabled;
      this.testCheckerScriptEdit.enabled = enabled;

      this.addFolderButton.enabled = enabled;
      this.reloadAllButton.enabled = enabled;
      this.clearButton.enabled = enabled;
      this.runAllButton.enabled = enabled;
      this.resetStatusButton.enabled = enabled;
      this.loadButton.enabled = enabled;
      this.runSelectedButton.enabled = enabled;
      this.resetButton.enabled = enabled;
   };

   /**
    * Launches a test in a separate PixInsight instance. Optinoally, the test can be launched with
    * the instruction to close the PixInsight instance once the test is terminated and to load the
    * test only without running it.
    *
    * @param {*} test
    * @param {*} terminatePIOnComplete
    * @param {*} loadOnly
    * @param {*} recordTest true if the test must be executed and recorded
    * @return {*}
    */
   this.runTest = ( test, terminatePIOnComplete, loadOnly, recordTest ) =>
   {
      engine.prepareTestForExecution( test );
      this.redraw();
      terminatePIOnComplete = terminatePIOnComplete == undefined ? true : terminatePIOnComplete;
      loadOnly = loadOnly == undefined ? false : loadOnly;
      // construct the execution command, valid for Linux only
      let parameters = [
         "-n",
         terminatePIOnComplete ? "--force-exit" : "",
         "--automation-mode",
         "-r=\"" +
         engine.sourcesDirectory +
         "/scripts/WeightedBatchPreprocessing/WeightedBatchPreprocessing.js,automationMode=true,testFile=" +
         test.filePath +
         ",outputDir=" +
         engine.workingDirectory +
         ",usePipelineScript=true,pipelineScriptFile=" +
         engine.testCheckerScript +
         ( loadOnly ? ",loadOnly=true" : "" ) +
         ( recordTest ? ",recordTest" : "" ) +
         "\""
      ];
      console.noteln( "=== test command ===" );
      console.writeln( engine.executable + " " + parameters.join( " " ) );
      console.noteln( "====================" );

      if ( !loadOnly )
         test.status = engine.STATUS_RUNNING;
      test.recording = recordTest;
      let process = new ExternalProcess();
      process.__command__ = engine.executable + " " + parameters.join( " " );
      process.start( engine.executable, parameters );
      return process;
   };

   /**
    * Waits for a test to terminate. This function monitors the execution status of the given process
    * and interpret the information printed to its cout stream to gather information about the
    * current execution and the results.
    *
    * @param {*} process
    * @param {*} test
    */
   this.waitTest = ( process, test ) =>
   {
      let updateConsole = function( msg )
      {
         console.clear();
         console.noteln( "=== test command ===" );
         console.writeln( process.__command__ );
         console.noteln( "====================" );
         console.writeln();
         if ( msg )
            console.noteln( msg );
      };

      updateConsole();
      this.waitingTest = true;
      this.setRunningStatus( true );

      let currCout = "";
      test.cout = "";
      while ( !process.waitForFinished( 1000 ) )
      {
         // update the progress information
         currCout += process.stdout.toString();
         if ( test.cout != currCout )
         {
            test.cout = currCout;
            updateConsole( test.cout );
         }
         let strings = test.cout.split( "\n" );
         for ( let i = strings.length - 1; i >= 0; i-- )
            if ( strings[ i ].indexOf( "PROGRESS" ) != -1 )
            {
               test.progressInfo = strings[ i ];
               break;
            }
         this.redraw();
      }
      test.progressInfo = "";
      // first ensure that the test completed
      updateConsole( test.cout );
      test.cout = test.cout.split( "\n" ).join( "<br>" );

      function saveLogs( test )
      {
         let test_fname = File.extractNameAndExtension( test.filePath );
         let srcLog = smartNaming.lastMatching( test.cout, /:LOGFILE:.*:LOGFILE:/g );
         srcLog = srcLog && srcLog.replace( /:LOGFILE:/g, "" );
         console.noteln( "test log file: ", srcLog );
         if ( srcLog && File.exists( srcLog ) )
         {
            let destLog = engine.workingDirectory + "/" + test_fname + ".log";
            if ( File.exists( destLog ) )
               File.remove( destLog );
            console.noteln( "copy to: ", destLog );
            File.copyFile( destLog, srcLog );
            test.logfile = destLog;
         }
      }

      if ( test.cout.indexOf( "TEST EXECUTION COMPLETED" ) == -1 )
      {
         // if this is not present then the execution didn't complete
         test.status = engine.STATUS_FAILED;
         test.notes = "Test execution crashed";
         saveLogs( test );
      }
      else if ( test.cout.indexOf( "TEST MANUALLY INTERRUPTED" ) != -1 )
      {
         // if this is present then the test was manually interrupted
         test.status = engine.STATUS_SUCCECSS;
         test.notes = "";
         saveLogs( test );
      }
      else if ( test.cout.indexOf( "TEST FAILED" ) != -1 )
      {
         // a test has failed
         test.status = engine.STATUS_FAILED;
         test.notes = test.recording ? "Failed while recording" : "Failed";
         saveLogs( test );
      }
      else
      {
         test.status = engine.STATUS_SUCCECSS;
         test.notes = "";
         saveLogs( test );
      }
      test.recording = false;
      this.setRunningStatus( false );
      this.redraw();
   };

   //

   this.updateNotes = () =>
   {
      let content = ( this.currentNode && this.currentNode.__test__ && this.currentNode.__test__.cout || "" ).replace( "\n", "<br>" )
      let info = "<HTML><style>" +
         "body { font-family: DejaVu Sans Mono, Monospace; font-size: 10pt; background: #FFFFFF; border-style: solid; border-color: #777777; border-width: 0pt;}" +
         "</style><body><div style=\"margin: 4pt;\">" + content + "</div></body></HTML>";
      this.coutView.setHTML( info );
   };

   // ================
   // GUI CONSTRUCTION
   // ================

   this.filesTreeBox = new TreeBox( this );
   this.filesTreeBox.setScaledMinWidth( 1024 );
   this.filesTreeBox.setScaledMinHeight( 400 );
   this.filesTreeBox.headerVisible = true;
   this.filesTreeBox.headerSorting = false;
   this.filesTreeBox.numberOfColumns = 3;
   this.filesTreeBox.setHeaderText( 0, "File" );
   this.filesTreeBox.setHeaderText( 1, "Status" );
   this.filesTreeBox.setHeaderText( 2, "ETA" );
   this.filesTreeBox.setHeaderText( 3, "Note" );

   this.filesTreeBox.onNodeSelectionUpdated = () =>
   {
      if ( this.filesTreeBox.selectedNodes.length > 0 )
      {
         this.currentNode = this.filesTreeBox.selectedNodes[ 0 ];
      }
      this.updateNotes();
   };

   this.filesTreeBox.onNodeUpdated = ( node, column ) =>
   {
      if ( column != o )
         return;
      let i = this.filesTreeBox.childIndex( node );
      engine.tests[ i ].enabled = node.checked;
   };

   //

   this.coutView = new WebView( this );
   this.coutView.useRichText = true;
   this.coutView.setScaledMinHeight( 200 );

   //

   this.reloadAllButton = new PushButton( this );
   this.reloadAllButton.text = "Reload tests";
   this.reloadAllButton.toolTip = "<p>Reload all tests.</p>";

   this.reloadAllButton.onClick = () =>
   {
      engine.reload();
      this.redraw();
   };

   //

   this.addFolderButton = new PushButton( this );
   this.addFolderButton.text = "Directory";
   this.addFolderButton.icon = this.scaledResource( ":/icons/add.png" );
   this.addFolderButton.toolTip = "<p>Load all tests from a selected folder.</p>";

   this.addFolderButton.onClick = () =>
   {
      let gdd = new GetDirectoryDialog;
      gdd.caption = "Select Root Directory";
      if ( gdd.execute() )
      {
         let rootDir = gdd.directory;
         let filters = [ ".wbpptest" ];
         let L = new FileList( rootDir, filters, false /*verbose*/ );
         L.files.forEach( ( filePath, i ) =>
         {
            engine.addTestFile( filePath );
         } );
         engine.sort();
         this.redraw();
      }
   };

   //

   this.clearButton = new PushButton( this );
   this.clearButton.text = "Clear list";
   this.clearButton.icon = this.scaledResource( ":/browser/delete-multiple.png" );
   this.clearButton.toolTip = "<p>Clear all tests</p>";
   this.clearButton.onMousePress = () =>
   {
      engine.clear();
      this.redraw();
   };

   //

   this.resetStatusButton = new PushButton( this );
   this.resetStatusButton.text = "Reset results";
   this.resetStatusButton.toolTip = "<p>Resets the tests status</p>";
   this.resetStatusButton.onMousePress = () =>
   {
      engine.prepareForExecution();
      this.redraw();
   };

   //

   this.runAllButton = new PushButton( this );
   this.runAllButton.text = "Run all";
   this.runAllButton.toolTip = "<p>Run all active tests</p>";
   this.runAllButton.onMousePress = () =>
   {
      engine.prepareForExecution();
      this.redraw();
      let testsToRun = engine.activeTests();

      // Loop through tests and run each one in background
      for ( let i = 0; i < testsToRun.length; i++ )
      {
         engine.clearWorkingDirectory();
         let process = this.runTest( testsToRun[ i ] );
         this.waitTest( process, testsToRun[ i ] )
      }
   };

   //

   this.runSelectedButton = new PushButton( this );
   this.runSelectedButton.text = "Run selected";
   this.runSelectedButton.toolTip = "<p>Launch the selected test without monitoring result</p>";
   this.runSelectedButton.onMousePress = () =>
   {
      if ( this.currentNode )
      {
         engine.prepareTestForExecution( this.currentNode.__test__ );
         this.redraw();
         let process = this.runTest( this.currentNode.__test__, !engine.keepPIOpen /* close PI when completed  */ );
         this.waitTest( process, this.currentNode.__test__ )
      }
   };

   //

   this.recordAllButton = new PushButton( this );
   this.recordAllButton.text = "Record all";
   this.recordAllButton.toolTip = "<p>Run all active tests</p>";
   this.recordAllButton.onMousePress = () =>
   {
      engine.prepareForExecution();
      this.redraw();
      let testsToRun = engine.activeTests();

      // Loop through tests and run each one in background
      for ( let i = 0; i < testsToRun.length; i++ )
      {
         engine.clearWorkingDirectory();
         let process = this.runTest( testsToRun[ i ], true /* close PI when completed  */ , false /* load only */ , true /* record */ );
         this.waitTest( process, testsToRun[ i ] )
      }
   };

   //

   this.recordSelectedButton = new PushButton( this );
   this.recordSelectedButton.text = "Record selected";
   this.recordSelectedButton.toolTip = "<p>Launch the selected test without monitoring result</p>";
   this.recordSelectedButton.onMousePress = () =>
   {
      if ( this.currentNode )
      {
         engine.prepareTestForExecution( this.currentNode.__test__ );
         this.redraw();
         let process = this.runTest( this.currentNode.__test__, !engine.keepPIOpen /* close PI when completed  */ , false /* load only */ , true /* record */ );
         this.waitTest( process, this.currentNode.__test__ )
      }
   };

   //

   this.loadButton = new PushButton( this );
   this.loadButton.text = "Load selected";
   this.loadButton.toolTip = "<p>Opens a PixInsight instance loading the test</p>";
   this.loadButton.onMousePress = () =>
   {
      if ( this.currentNode )
      {
         engine.prepareTestForExecution( this.currentNode.__test__ );
         this.redraw();
         this.runTest( this.currentNode.__test__, false /* close PI when completed  */ , true /* load only */ );
      }
   };

   //

   this.openLogsButton = new PushButton( this );
   this.openLogsButton.text = "Open logs";
   this.openLogsButton.toolTip = "<p>Opens the klogfile for the test</p>";
   this.openLogsButton.onMousePress = () =>
   {
      if ( this.currentNode && this.currentNode.__test__.logfile && this.currentNode.__test__.logfile.length > 0 )
      {
         let vscode = new ExternalProcess();
         vscode.start( "code", [ this.currentNode.__test__.logfile ] );
      }
   };

   //

   this.keepPIOpenCheckBox = new CheckBox( this );
   this.keepPIOpenCheckBox.text = "Keep PI open"
   this.keepPIOpenCheckBox.onCheck = ( checked ) =>
   {
      engine.keepPIOpen = checked;
      this.redraw();
   };

   //

   this.resetButton = new PushButton( this );
   this.resetButton.text = "Reset";
   this.resetButton.icon = this.scaledResource( ":/icons/reload.png" );
   this.resetButton.toolTip = "<p>Perform optional reset and clearing actions.</p>";
   this.resetButton.onClick = () =>
   {
      engine.defaults();
      this.redraw();
   };

   // ---------- PARAMETERS

   let buttonsSizer = new VerticalSizer;
   buttonsSizer.spacing = 6;
   let editSizer = new VerticalSizer;
   editSizer.spacing = 6;

   //

   this.workingFolderButton = new PushButton( this );
   this.workingFolderButton.text = "Working Directory";
   this.workingFolderButton.toolTip = "<p>Select the working directory.</p>";
   this.workingFolderButton.onClick = () =>
   {
      let gdd = new GetDirectoryDialog;
      gdd.caption = "Select Root Directory";
      if ( gdd.execute() )
      {
         engine.workingDirectory = gdd.directory;
         this.redraw();
      }
   };

   this.workingDirectoryEdit = new Edit( this );
   this.workingDirectoryEdit.toolTip = "<p>The working directory.</p>";

   buttonsSizer.add( this.workingFolderButton );
   editSizer.add( this.workingDirectoryEdit );

   //

   this.sourcesDirectoryButton = new PushButton( this );
   this.sourcesDirectoryButton.text = "Sources Directory";
   this.sourcesDirectoryButton.toolTip = "<p>Select the sources directory.</p>";
   this.sourcesDirectoryButton.onClick = () =>
   {
      let gdd = new GetDirectoryDialog;
      gdd.caption = "Select the sources directory";
      if ( gdd.execute() )
      {
         engine.sourcesDirectory = gdd.directory;
         this.redraw();
      }
   };

   this.sourcesDirectoryEdit = new Edit( this );
   this.sourcesDirectoryEdit.toolTip = "<p>The sources directory.</p>";

   buttonsSizer.add( this.sourcesDirectoryButton );
   editSizer.add( this.sourcesDirectoryEdit );

   //

   this.executableButton = new PushButton( this );
   this.executableButton.text = "Executable";
   this.executableButton.toolTip = "<p>Select the pixinsight executable.</p>";
   this.executableButton.onClick = () =>
   {
      let ofd = new OpenFileDialog;
      ofd.multipleSelections = false;
      ofd.caption = "Select the PixInsight executable file";
      if ( ofd.execute() )
      {
         engine.executable = ofd.fileName;
         this.redraw();
      }
   };

   this.executableEdit = new Edit( this );
   this.executableEdit.toolTip = "<p>The PixIsnight executable file.</p>";

   buttonsSizer.add( this.executableButton );
   editSizer.add( this.executableEdit );

   //

   this.testCheckerScriptButton = new PushButton( this );
   this.testCheckerScriptButton.text = "Test checker script";
   this.testCheckerScriptButton.toolTip = "<p>Select the test checker script.</p>";
   this.testCheckerScriptButton.onClick = () =>
   {
      let ofd = new OpenFileDialog;
      ofd.multipleSelections = false;
      ofd.caption = "Select the test checked script";
      ofd.filters = [
         [ "Javascript", ".js" ]
      ];
      if ( ofd.execute() )
      {
         engine.testCheckerScript = ofd.fileName;
         this.redraw();
      }
   };

   this.testCheckerScriptEdit = new Edit( this );
   this.testCheckerScriptEdit.toolTip = "<p>The test checker pipeline script.</p>";

   buttonsSizer.add( this.testCheckerScriptButton );
   editSizer.add( this.testCheckerScriptEdit );

   // LAYOUT

   this.parametersSizer = new HorizontalSizer;
   this.parametersSizer.spacing = 6;
   this.parametersSizer.add( buttonsSizer );
   this.parametersSizer.add( editSizer );

   //
   let vSpacing = 20;
   this.controlsSizer = new VerticalSizer;
   this.controlsSizer.spacing = 6;
   this.controlsSizer.add( this.addFolderButton );
   this.controlsSizer.add( this.clearButton );
   this.controlsSizer.addSpacing( vSpacing );
   this.controlsSizer.add( this.runAllButton );
   this.controlsSizer.add( this.recordAllButton );
   this.controlsSizer.add( this.reloadAllButton );
   this.controlsSizer.add( this.resetStatusButton );
   this.controlsSizer.addSpacing( vSpacing );
   this.controlsSizer.add( this.loadButton );
   this.controlsSizer.add( this.runSelectedButton );
   this.controlsSizer.add( this.recordSelectedButton );
   this.controlsSizer.add( this.keepPIOpenCheckBox );
   this.controlsSizer.addSpacing( vSpacing );
   this.controlsSizer.add( this.openLogsButton );
   this.controlsSizer.addStretch();
   this.controlsSizer.add( this.resetButton );

   //

   this.leftSizer = new VerticalSizer;
   this.leftSizer.spacing = 6;
   this.leftSizer.add( this.filesTreeBox );
   this.leftSizer.add( this.coutView );

   //

   this.treeAndControlSizer = new HorizontalSizer;
   this.treeAndControlSizer.spacing = 6;
   this.treeAndControlSizer.add( this.leftSizer );
   this.treeAndControlSizer.add( this.controlsSizer );

   //

   this.sizer = new VerticalSizer;
   this.sizer.spacing = 6;
   this.sizer.margin = 6;
   this.sizer.add( this.parametersSizer );
   this.sizer.add( this.treeAndControlSizer );

   this.redraw();
};
WBPPTestRunnerDialog.prototype = new Dialog;

/**
 * The WBPP test runner engine.
 *
 */
let WBPPTestRunnerEngine = function()
{

   // STATUS codes
   this.STATUS_READY = 0;
   this.STATUS_SUCCECSS = 1;
   this.STATUS_FAILED = 2;
   this.STATUS_RUNNING = 3;

   this.keepPIOpen = false;

   // defaults management
   this.defaults = () =>
   {
      // set the initial working directory
      this.workingDirectory = File.currentWorkingDirectory;

      // set the initial sources folder
      this.sourcesDirectory = coreSrcDirPath;

      // set the initial executable file path
      if ( corePlatform == "Linux" )
         this.executable = coreFilePath + ".sh";
      else
         this.executable = coreFilePath;

      // set the initial test checked script path
      this.testCheckerScript = coreSrcDirPath + "/scripts/WeightedBatchPreprocessing/WeightedBatchPreprocessing-testChecker.js";
   };

   this.defaults();

   this.addTestFile = ( filePath ) =>
   {
      try
      {
         let data = JSON.parse( File.readTextFile( filePath ) );
         if ( data != undefined )
         {
            let test = {
               filePath: filePath,
               logfile: "",
               content: data,
               eta: data.testExecutionStatus && data.testExecutionStatus.executionTime || 0,
               enabled: true,
               status: this.STATUS_READY,
               recording: false,
               cout: "",
               notes: "",
               progressInfo: ""
            };
            this.tests.push( test );
         }
      }
      catch ( _ )
      {

      }
   };

   this.activeTests = () =>
   {
      return this.tests.filter( t => t.enabled );
   };

   this.prepareTestForExecution = ( test ) =>
   {
      test.status = this.STATUS_READY;
      test.notes = "";
      test.progressInfo = "";
      test.cout = "";
      test.logfile = "";
      test.recording = false;
   };

   this.prepareForExecution = () =>
   {
      this.tests.forEach( t =>
      {
         this.prepareTestForExecution( t );
      } );
   };

   this.clear = () =>
   {
      this.tests = [];
   };

   this.sort = () =>
   {
      this.tests.sort( ( a, b ) =>
      {
         return a.filePath.localeCompare( b.filePath );
      } );
   };

   this.clearWorkingDirectory = () =>
   {
      let foldersToRemove = [
         "logs", "calibrated", "debayered", "cosmetized", "measured", "ldd_lps", "registered", "master"
      ];
      for ( let i = 0; i < foldersToRemove.length; i++ )
      {
         let path = this.workingDirectory + "/" + foldersToRemove[ i ];
         if ( File.directoryExists( path ) )
         {
            console.noteln( "Deleting the working subfolder ", path );
            removeDirectoryAndContent( path );
         }
      }
   };

   this.reload = () =>
   {
      let fnames = this.tests.map( t => t.filePath );
      this.clear();
      fnames.forEach( fname => this.addTestFile( fname ) );
   };

   this.load = () =>
   {
      function load( key, type )
      {
         return Settings.read( TESTS_RUNNER_SETTINGS_KEY_BASE + key, type );
      }
      let o;
      if ( ( o = load( "workingDirectory", DataType_String ) ) != null )
         this.workingDirectory = o;
      if ( ( o = load( "sourcesDirectory", DataType_String ) ) != null )
         this.sourcesDirectory = o;
      if ( ( o = load( "executable", DataType_String ) ) != null )
         this.executable = o;
      if ( ( o = load( "testCheckerScript", DataType_String ) ) != null )
         this.testCheckerScript = o;
      if ( ( o = load( "keepPIOpen", DataType_Boolean ) ) != null )
         this.keepPIOpen = o;
      if ( ( o = load( "tests", DataType_String ) ) != null )
      {
         o = JSON.parse( o );
         if ( o != undefined )
         {
            this.tests = o;
         }
      }
   };

   this.save = () =>
   {
      function save( key, type, value )
      {
         try
         {
            Settings.write( TESTS_RUNNER_SETTINGS_KEY_BASE + key, type, value );
         }
         catch ( e )
         {
            console.warningln( "Unable to save [", key, "] for type ", type, " with value ", value );
         }
      }

      save( "workingDirectory", DataType_String, this.workingDirectory );
      save( "sourcesDirectory", DataType_String, this.sourcesDirectory );
      save( "executable", DataType_String, this.executable );
      save( "testCheckerScript", DataType_String, this.testCheckerScript );
      save( "keepPIOpen", DataType_Boolean, this.keepPIOpen );
      save( "tests", DataType_String, JSON.stringify( this.tests ) );
   };

   this.clear();
};

// scoped execution
{
   let wbppTestRunnerEngine = new WBPPTestRunnerEngine();

   wbppTestRunnerEngine.load();
   let dialog = new WBPPTestRunnerDialog( wbppTestRunnerEngine );
   dialog.execute();
   wbppTestRunnerEngine.save();
}

// ----------------------------------------------------------------------------
// EOF WeightedBatchPreprocessing-testsRunner.js - Released 2023-06-15T09:00:12Z

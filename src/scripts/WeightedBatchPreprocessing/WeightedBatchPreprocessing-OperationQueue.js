// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// WeightedBatchPreprocessing-OperationQueue.js - Released 2023-06-15T09:00:12Z
// ----------------------------------------------------------------------------
//
// This file is part of Weighted Batch Preprocessing Script version 2.5.10
//
// Copyright (c) 2019-2023 Roberto Sartori
// Copyright (c) 2020-2021 Adam Block
// Copyright (c) 2019 Tommaso Rubechi
// Copyright (c) 2012 Kai Wiechen
// Copyright (c) 2012-2023 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

let OperationQueueStatus = {
   INACTIVE: 0,
   RUNNING: 1
};

/**
 * Operation Queue. This object handles the serial execution of the queued operational tasks.
 *
 */
let OperationQueue = function()
{
   this.status = OperationQueueStatus.INACTIVE;
   this.currentOperationIndex = -1;
   this.interruptionRequested = false;
   this.stepCallback = undefined;
   this.executionTime = 0;
   this.elapsed = new ElapsedTime;
   this.eventScriptCode = undefined;

   /// the environment shared across the operations
   this.environment = {};

   /// the list of operations
   this.operations = [];

   /**
    * Adds an operation. The operation is appended to the existing operations.
    *
    * @param {*} operation
    * @param {*} params an object accessible at execution time at env.params
    */
   this.addOperation = ( operation, params ) =>
   {
      // queue the operation and the external params
      operation.__index__ = this.operations.length;
      this.operations.push(
      {
         operation: operation,
         params: params
      } );
   };

   /**
    * Adds an operation block. This function receives a function and creates an operation
    * block out of it.
    *
    * @param {*} operationBlock function with (environment, requestInterruption) parameters
    * @param {*} params an object accessible at execution time at env.params
    * @return {OperationBlock} the added OperationBlock
    */
   this.addOperationBlock = ( operationBlock, params ) =>
   {
      let operation = new OperationBlock;
      operation.run = function( env, requestInterruption )
      {
         operationBlock( env, requestInterruption );
      };
      this.addOperation( operation, params );
      return operation;
   };

   /**
    * Returns the operation at given index.
    *
    * @param {*} i the index of the operation to be retrieved
    * @return {*} the operation if index is valid, undefined otherwise
    */
   this.operationAtIndex = ( i ) =>
   {
      if ( i < 0 || i >= this.operations.length )
         return undefined;
      return this.operations[ i ].operation;
   };

   /**
    * Returns the current active operation.
    *
    * @return {*} the active operation, undefined if no active operation exists.
    */
   this.currentOperation = () =>
   {
      return this.operationAtIndex( this.currentOperationIndex );
   };

   /**
    * Clears the queue by removing all operations and clearing the environment
    *
    */
   this.clear = () =>
   {
      // clear the environment
      this.environment = {};
      this.operations = [];
   };

   /**
    * Executes the provided event script code injecting the environment object.
    *
    * @param {*} scriptCode
    * @param {*} environment
    * @return {*}
    */
   this.executeEventScriptCode = ( scriptCode, environment ) =>
   {
      let code = "let success = false; let env = " + JSON.stringify( environment ) + ";try { if (!env.test) { " + scriptCode + " }; success = true; } catch (e) {console.noteln(\"event script error: \", e);}; success"
      let success = false;
      try
      {
         success = eval( code );
      }
      catch ( e )
      {
         console.noteln( "pipeline event script failed: ", e );
      }
      return success;
   }

   /**
    * Installs the event script code loading it from a filepath. The filepath must point to a valid JS file and
    * the JS code is tested before being installed.
    *
    * @param {*} filePath
    */
   this.installEventScript = ( filePath ) =>
   {
      if ( File.exists( filePath ) )
      {
         let scriptCode = File.readTextFile( filePath );

         // encapsulate the script code and test its execution
         let testEnvironment = {
            test: true
         };
         let success = this.executeEventScriptCode( scriptCode, testEnvironment );

         // we store the code only if it's successfully executed
         if ( success )
         {
            this.eventScriptCode = scriptCode;
            console.noteln( "succesfully installed the pipeline event script ", filePath )
         }
         else
            this.eventScriptCode = undefined;
      }
   }

   /**
    * Ununstalls the script code.
    *
    */
   this.uninstallEventScript = () =>
   {
      this.eventScriptCode = undefined;
   }

   /**
    * Sync method that runs the operations in the queue.
    *
    */
   this.run = ( env ) =>
   {
      // initialization
      this.executionTime = 0;
      this.elapsed.reset();
      this.environment = ( typeof env == "object" ) ? env :
      {};
      this.status = OperationQueueStatus.RUNNING;
      this.interruptionRequested = false;
      this.currentOperationIndex = -1;
      if ( typeof this.stepCallback == 'function' )
         this.stepCallback();

      // invoke the event script at the beginning of the execution
      this.triggerOperationWithEvent( undefined, "pipeline start" );

      // processing the queue
      for ( let i = 0; i < this.operations.length; i++ )
      {
         this.currentOperationIndex = i;
         this.executeTask( i );
         if ( this.interruptionRequested )
         {
            for ( let j = i + 1; j < this.operations.length; j++ )
               this.operations[ j ].operation.status = OperationBlockStatus.CANCELED;
            break;
         }
         processEvents();
         gc();
      }
      this.executionTime = this.elapsed.value;

      // clean up
      this.currentOperationIndex = -1;
      this.status = OperationQueueStatus.INACTIVE;

      // invoke the event script at the beginning of the execution
      this.triggerOperationWithEvent( undefined, "pipeline end" );

      if ( typeof this.stepCallback == 'function' )
         this.stepCallback();
   };

   this.triggerOperationWithEvent = ( op, eventName ) =>
   {
      // operation is performed only if
      // - a pipeline event script is installed
      // - no operation is provided (generic event)
      // - an operation is given and the operation triggers an event
      if ( this.eventScriptCode != undefined && ( op == undefined || op.operation.triggersEventScript ) )
      {
         // get the data to be injected into the environment from the operation
         let env = {
            test: false,
            requestInterruption: this.requestInterruption
         };
         // if an operatio is provided then inject the operation information
         if ( op )
         {
            if ( op.operation.envForScript )
            {
               let scriptEnv = op.operation.envForScript();
               let keys = Object.keys( scriptEnv );
               for ( let i = 0; i < keys.length; i++ )
                  env[ keys[ i ] ] = scriptEnv[ keys[ i ] ];
            }
            // operation details
            env.operationIndex = op.operation.__index__;
            env.operation = op.operation;
         }
         env.event = eventName;
         env.operationsCount = this.operations.length;
         this.executeEventScriptCode( this.eventScriptCode, env )
      }
   }

   /**
    * Hanelst the task execution.
    * A task can be executed only if it's in READY mode.
    *
    * @param {*} index the task index in the queue
    */
   this.executeTask = ( index ) =>
   {
      // a task can run only if it's ready
      if ( this.operations[ index ].operation.status == OperationBlockStatus.READY )
      {
         this.operations[ index ].operation.status = OperationBlockStatus.RUNNING;
         this.operations[ index ].operation.elapsed.reset();

         // starting an operation
         if ( typeof this.stepCallback == 'function' )
            this.stepCallback();

         // operation start handling
         this.triggerOperationWithEvent( this.operations[ index ], "start" );

         // operation execution
         this.environment.params = this.operations[ index ].params;
         this.operations[ index ].operation._perform( this.environment, this.requestInterruption );

         // operation dona handling
         this.triggerOperationWithEvent( this.operations[ index ], "done" );

         // operation done
         if ( typeof this.stepCallback == 'function' )
            this.stepCallback();
      }
   };

   /**
    * Requests the interruption of the queue execution. All pending tasks will be marked
    * as CANCELED.
    */
   this.requestInterruption = () =>
   {
      this.interruptionRequested = true;
   };
};

let OperationBlockStatus = {
   READY: 0,
   RUNNING: 1,
   DONE: 2,
   FAILED: 3,
   CANCELED: 4
};

/**
 * This object represents an operation block that can be queued in an operation queue.
 * Any application-level operation block must extend this object and implement the run()
 * method that concretely performs the task. The run() method receives an environment object
 * which contains shared data across operations and the interruptQueue callback to be called
 * if the whole operation queue execution needs to be canceled.
 */
let OperationBlock = function()
{
   // a block is always initialized as ready
   this.status = OperationBlockStatus.READY;

   // by design, an execution block does not trigger the event script
   this.triggersEventScript = false;

   // measurement of the elapsed time while the operatio nis ongoing
   this.elapsed = new ElapsedTime;

   // final execution time once the operation has completed
   this.executionTime = 0;

   /**
    * Internal execution of the operation.
    *
    * @param {*} environment the shared environment object
    * @param {*} interruptQueue the callback invoked to cancel the queue execution
    * @return {*}
    */
   this._perform = ( environment, interruptQueue ) =>
   {
      this.elapsed.reset();
      this.executionTime = 0;
      // safely handle the operation execution
      try
      {
         let status = this.run( environment, interruptQueue )
         this.status = status != undefined ? status : OperationBlockStatus.DONE;
      }
      catch ( e )
      {
         console.warningln( "Operation queue error: ", e )
         // a critical error occurred, mark the operation as failed and return
         this.status = OperationBlockStatus.FAILED;
         return;
      }
      this.executionTime = this.elapsed.value;
   };
};

// ----------------------------------------------------------------------------
// EOF WeightedBatchPreprocessing-OperationQueue.js - Released 2023-06-15T09:00:12Z

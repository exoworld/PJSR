// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// NoiseEvaluationCFA.js - Released 2021-05-31T07:25:09Z
// ----------------------------------------------------------------------------
//
// This file is part of PixInsight Noise Evaluation Script version 2.1.1
//
// Copyright (c) 2006-2021 Pleiades Astrophoto S.L.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

#feature-id    NoiseEvaluation : Image Analysis > NoiseEvaluation (CFA Bayer)

#feature-icon  @script_icons_dir/NoiseEvaluationCFABayer.svg

#feature-info \
Automatic estimation of Gaussian noise by the iterative multiresolution \
support and k-sigma thresholding algorithms (CFA Bayer version).<br>\
<br>\
Written by Juan Conejero (PTeam).<br>\
<br>\
References:<br>\
<br>\
Jean-Luc Starck, Fionn Murtagh, <i>Automatic Noise Estimation from the \
Multiresolution Support</i>, Publications of the Royal Astronomical Society \
of the Pacific, vol. 110, February 1998, pp. 193-199.<br>\
<br>\
J.L. Starck, F. Murtagh, <i>Astronomical Image and Data Analysis</i>, Springer, \
1st ed., 2002, pp. 37-38.<br>\
<br>\
Copyright (c) 2006-2021 Pleiades Astrophoto S.L.

#define VERSION "2.1.1"

if ( CoreApplication === undefined ||
     CoreApplication.versionRevision === undefined ||
     CoreApplication.versionMajor*1e11
   + CoreApplication.versionMinor*1e8
   + CoreApplication.versionRelease*1e5
   + CoreApplication.versionRevision*1e2 < 100800800600 )
{
   throw new Error( "This script requires PixInsight core version 1.8.8-6 or higher." );
}

/**
 * Estimation of the standard deviation of the noise, assuming a Gaussian
 * noise distribution (CFA version).
 *
 * - Use MRS noise evaluation when the algorithm converges for 4 >= J >= 2
 *
 * - Use k-sigma noise evaluation when either MRS doesn't converge or the
 *   length of the noise pixels set is below a 1% of the image area.
 *
 * - Automatically iterate to find the highest layer where noise can be
 *   successfully evaluated, in the [1,3] range.
 *
 * Returned noise estimates are scaled by the Sn robust scale estimator of
 * Rousseeuw and Croux.
 *
 * Copyright (C) 2017-2021 Pleiades Astrophoto. All Rights Reserved.
 * Written by Juan Conejero (PTeam)
 */
function ScaledNoiseEvaluation( image )
{
   let scale = image.Sn();
   if ( 1 + scale == 1 )
      throw Error( "Zero or insignificant data." );

   let a, n = 4, m = 0.01*image.selectedRect.area;
   for ( ;; )
   {
      a = image.noiseMRS( n );
      if ( a[1] >= m )
         break;
      if ( --n == 1 )
      {
         console.writeln( "<end><cbr>** Warning: No convergence in MRS noise evaluation routine - using k-sigma noise estimate." );
         a = image.noiseKSigma();
         break;
      }
   }
   this.sigma = a[0]/scale; // estimated scaled stddev of Gaussian noise
   this.count = a[1];       // number of pixels in the noisy pixels set
   this.layers = n;         // number of layers used for noise evaluation
}

/*
 * Returns a Bayer CFA frame converted to a 4-channel image. Individual CFA
 * components are written to output channels 0, ..., 3 as follows:
 *
 * 0 | 1
 * --+--
 * 2 | 3
 *
 * Where CFA element #0 corresponds to the top left corner of the input frame.
 * The output image will have half the dimensions of the input frame.
 *
 * If specified, the k0, ..., k3 scalars will multiply their respective output
 * channels 0, ..., 3.
 */
function BayerCFAToFourChannel( image, k0, k1, k2, k3 )
{
   if ( k0 === undefined )
      k0 = 1;
   if ( k1 === undefined )
      k1 = 1;
   if ( k2 === undefined )
      k2 = 1;
   if ( k3 === undefined )
      k3 = 1;

   let w = image.width;
   let h = image.height;
   let w2 = w >> 1;
   let h2 = h >> 1;
   let rgb = new Image( w2, h2, 4 );
   for ( let y = 0, j = 0; y < h; y += 2, ++j )
      for ( let x = 0, i = 0; x < w; x += 2, ++i )
      {
         rgb.setSample( k0*image.sample( x,   y   ), i, j, 0 );
         rgb.setSample( k1*image.sample( x+1, y   ), i, j, 1 );
         rgb.setSample( k2*image.sample( x,   y+1 ), i, j, 2 );
         rgb.setSample( k3*image.sample( x+1, y+1 ), i, j, 3 );
      }
   return rgb;
}

#define K0 1.0
#define K1 1.0
#define K2 1.0
#define K3 1.0

function main()
{
   let window = ImageWindow.activeWindow;
   if ( window.isNull )
      throw new Error( "No active image" );

   console.show();
   console.writeln( "<end><cbr><br><b>" + window.currentView.fullId + "</b>" );
   console.writeln( "Scaled Noise Evaluation Script v" + VERSION + " - Bayer CFA Version" );
   console.writeln( "Calculating scaled noise standard deviation..." );
   console.flush();

   console.abortEnabled = true;

   let image = BayerCFAToFourChannel( window.currentView.image, K0, K1, K2, K3 );
   console.writeln( "<end><cbr><br>Ch |   noise   |  count(%) | layers |" );
   console.writeln(               "---+-----------+-----------+--------+" );
   for ( let c = 0; c < image.numberOfChannels; ++c )
   {
      console.flush();
      image.selectedChannel = c;
      let E = new ScaledNoiseEvaluation( image );
      console.writeln( format( "%2d | <b>%.3e</b> |  %6.2f   |    %d   |", c, E.sigma, 100*E.count/image.selectedRect.area, E.layers ) );
      console.flush();
   }
   console.writeln(               "---+-----------+-----------+--------+" );
}

main();

// ----------------------------------------------------------------------------
// EOF NoiseEvaluationCFA.js - Released 2021-05-31T07:25:09Z

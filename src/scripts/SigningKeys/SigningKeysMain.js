// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// SigningKeysMain.js - Released 2022-04-13T15:13:50Z
// ----------------------------------------------------------------------------
//
// This file is part of PixInsight Signing Keys Maintenance Utility Script 1.0
//
// Copyright (c) 2021-2022 Pleiades Astrophoto S.L.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/*
 * PixInsight Signing Keys Maintenance Utility
 *
 * Copyright (C) 2021-2022 Pleiades Astrophoto. All Rights Reserved.
 * Written by Juan Conejero (PTeam)
 *
 * Script entry point.
 */

#include <pjsr/StdButton.jsh>
#include <pjsr/StdIcon.jsh>

function main()
{
   g_workingData.loadSettings();

   let dialog = new SigningKeysDialog();
   for ( ;; )
   {
      try
      {
         console.show();

         if ( dialog.execute() )
         {
            if ( g_workingData.outputFilePath.length == 0 )
            {
               (new MessageBox( "No output signing keys file has been specified.",
                              TITLE, StdIcon_Error, StdButton_Ok )).execute();
               continue;
            }

            if ( File.extractExtension( g_workingData.outputFilePath ) != ".xssk" )
            {
               (new MessageBox( "Invalid signing keys file extension. Must be \'.xssk\'.",
                              TITLE, StdIcon_Error, StdButton_Ok )).execute();
               continue;
            }

            if ( File.exists( g_workingData.outputFilePath ) )
            {
               if ( (new MessageBox( "<p>The specified output file already exists:</p>" +
                                     "<p>" + g_workingData.outputFilePath + "</p>" +
                                     "<p><b>* Warning *</b> If you overwrite the specified output file and don't " +
                                     "have a backup copy, you may lose the corresponding code signing identity.</p>" +
                                     "<p><b>Do you really want to overwrite this file?<b></p>",
                              TITLE, StdIcon_Warning, StdButton_No, StdButton_Yes )).execute() != StdButton_Yes )
                  continue;
            }

            /*
             * N.B. Password ByteArray objects will be securely wiped out with
             * zeros by the Security methods invoked.
             */
            let outputPassword = dialog.outputPassword_Edit.utf8;
            if ( !Security.validPassword( outputPassword ) )
            {
               (new MessageBox( "<p>Invalid or insecure password specified.</p>" +
                                "<p>A valid password must have at least eight characters and contain at least " +
                                "one numeric digit or punctuation character, one uppercase and one lowercase letter. " +
                                "Only ASCII characters are allowed. White space characters are not allowed.</p>",
                        TITLE, StdIcon_Error, StdButton_Ok )).execute();
               continue;
            }

            try
            {
               if ( g_workingData.exportKeysFile )
               {
                  if ( g_workingData.inputFilePath.length == 0 )
                  {
                     outputPassword.secureFill();
                     (new MessageBox( "No input signing keys file has been specified.",
                                    TITLE, StdIcon_Error, StdButton_Ok )).execute();
                     continue;
                  }

                  if ( !File.exists( g_workingData.inputFilePath ) )
                  {
                     outputPassword.secureFill();
                     (new MessageBox( "The specified input signing keys file does not exist.",
                                    TITLE, StdIcon_Error, StdButton_Ok )).execute();
                     continue;
                  }

                  let inputPassword = dialog.inputPassword_Edit.utf8;
                  if ( inputPassword.isEmpty )
                  {
                     outputPassword.secureFill();
                     (new MessageBox( "No password has been specified for the input signing keys file.",
                                    TITLE, StdIcon_Error, StdButton_Ok )).execute();
                     continue;
                  }

                  Security.exportSigningKeysFile( g_workingData.outputFilePath, outputPassword,
                                                  g_workingData.inputFilePath, inputPassword );

                  console.noteln( "* Signing keys file exported:" );
                  console.noteln( "Input  : <raw>" + g_workingData.inputFilePath + "</raw>" );
                  console.noteln( "Output : <raw>" + g_workingData.outputFilePath + "</raw>" );
               }
               else
               {
                  if ( g_workingData.localSigningIdentity )
                  {
                     Security.generateLocalSigningKeysFile( g_workingData.outputFilePath, outputPassword );

                     console.noteln( "* Signing keys file generated, local signing identity:" );
                     console.noteln( "<raw>" + g_workingData.outputFilePath + "</raw>" );
                  }
                  else
                  {
                     if ( g_workingData.developerId.length == 0 )
                     {
                        outputPassword.secureFill();
                        (new MessageBox( "No developer identifier has been specified.",
                                       TITLE, StdIcon_Error, StdButton_Ok )).execute();
                        continue;
                     }

                     Security.generateSigningKeysFile( g_workingData.outputFilePath, g_workingData.developerId, outputPassword )

                     console.noteln( "* Signing keys file generated for developer \'" + g_workingData.developerId + "\':" );
                     console.noteln( "<raw>" + g_workingData.outputFilePath + "</raw>" );
                  }
               }
            }
            catch ( x )
            {
               console.criticalln( "<end><cbr><raw>" + x.toString() + "</raw>" );
            }

            outputPassword.secureFill();

            console.flush();

            g_workingData.saveSettings();

            if ( (new MessageBox( "Do you want to perform more key management tasks?",
                                 TITLE, StdIcon_Question, StdButton_Yes, StdButton_No )).execute() == StdButton_Yes )
               continue;
         }

         console.hide();

         break;
      }
      catch ( x )
      {
         console.criticalln( "<end><cbr><raw>" + x.toString() + "</raw>" );
      }
   }
}

// ----------------------------------------------------------------------------
// EOF SigningKeysMain.js - Released 2022-04-13T15:13:50Z

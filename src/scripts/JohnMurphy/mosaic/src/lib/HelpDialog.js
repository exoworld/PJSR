/* global Dialog, FrameStyle_Sunken */

// Version 1.0 (c) John Murphy 31st-Dec-2020
//
// ======== #license ===============================================================
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation, version 3 of the License.
//
// This program is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
// more details.
//
// You should have received a copy of the GNU General Public License along with
// this program.  If not, see <http://www.gnu.org/licenses/>.
// =================================================================================
//"use strict";

function HelpDialog( ){
    this.__base__ = Dialog;
    this.__base__();

    let titleLabel = new Label();
    titleLabel.frameStyle = FrameStyle_Sunken;
    titleLabel.margin = 4;
    titleLabel.wordWrapping = false;
    titleLabel.useRichText = true;
    titleLabel.text = 
            "<p>I provide the software for free (I'm not paid) in the hope that it will be useful, but I need your support.<br />" +
            "So if you find my software useful, please buy me a coffee! It will be really appreciated.</p>" +
            "<ul><li>27,000 lines of code (NormalizeScaleGradient 11,000; PhotometricMosaic 16,000).</li>" +
            "<li>I actively support these scripts on the PixInsight forum.</li>" + 
            "<li>Currently porting NSG to C++ to make it compatible with Drizzle processing.</li></ul>" +
            "<p><b>Copy and paste the link into your browser. Thanks for your support, John Murphy.</b></p>";

    let ok_Button = new PushButton(this);
    ok_Button.defaultButton = true;
    ok_Button.text = "OK";
    ok_Button.icon = this.scaledResource(":/icons/ok.png");
    ok_Button.onClick = function () {
        this.dialog.ok();
    };

    let buttons_Sizer = new HorizontalSizer;
    buttons_Sizer.spacing = 6;
    buttons_Sizer.addStretch();
    buttons_Sizer.add(ok_Button);

    let webpage = new TextBox( this );
    webpage.text = "https://ko-fi.com/jmurphy";
    
    // Global sizer
    this.sizer = new VerticalSizer();
    this.sizer.margin = 10;
    this.sizer.spacing = 10;
    
    this.sizer.add(titleLabel);
    this.sizer.add(webpage);
    this.sizer.add(buttons_Sizer);

    this.windowTitle = "Thank you for your help and support!";
    this.adjustToContents();
}

HelpDialog.prototype = new Dialog;

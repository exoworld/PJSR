// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// WeightsOptimizer-Plots.js - Released 2023-06-21T16:40:28Z
// ----------------------------------------------------------------------------
//
// This file is part of Weigfhts Optimizer Script version 1.1.0
//
// Copyright (c) 2022-2023 Roberto Sartori
// Copyright (c) 2022-2023 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/* beautify ignore:start */


/* beautify ignore:end */
function WeightsOptimizerPlots( cache )
{
   this.gnuPlotCommands = [];

   this.graphFilePath = () => ( this.outputFile || ( File.systemTempDirectory + "/WeightOptimizerGnuplot.svg" ) )

   this.saveTo = ( filePath, iterations, highlightIndex, plotGain, plotMeasure ) =>
   {
      this.outputFile = filePath;
      this.plotIterations( iterations, highlightIndex, undefined, plotGain, plotMeasure );
      this.outputFile = undefined;
   }

   // Prepare a new plot
   this.newPlot = () =>
   {
      this.gnuPlotCommands = [];
      this.command( "set terminal svg size 1920,1080 enhanced font 'Helvetica,24'" );
      this.command( "set output '" + this.graphFilePath() + "'" );
   }

   // add a gnuplot command line
   this.command = ( command ) =>
   {
      this.gnuPlotCommands.push( command );
   }

   // returns the dot size depending on the number of points to be plotted
   this.dotSize = ( N ) => ( 1 - 0.1 * N / 10 );

   // convert a normalizr floor in range [0,1] to an hex representation in the range 00xff;
   this.ftoh = ( value ) =>
   {
      value = Math.floor( value * 255 );
      if ( value == 0 )
      {
         return "00";
      }
      return ( value > 15 ? "" : "0" ) + value.toString( 16 );
   }

   this.rgbaHex = ( r, g, b, a ) =>
   {
      return "#" + this.ftoh( a ) + this.ftoh( r ) + this.ftoh( g ) + this.ftoh( b );
   }

   this.execute = ( previewControl, filename ) =>
   {
      // nominal gnuplot file
      let plotFile = File.systemTempDirectory + "/" + filename + ".gnu";

      // write the gnuplot file
      File.writeTextFile( plotFile, this.gnuPlotCommands.join( "\n" ) + "\n" );
      if ( cache )
         cache.addTemporaryFilePath( plotFile )

      // delete the pre-existing image if needed
      if ( File.exists( this.graphFilePath() ) )
      {
         File.remove( this.graphFilePath() )
      }
      if ( cache )
         cache.addTemporaryFilePath( this.graphFilePath() )

      // run gnuplot
      let gnuplotCommand = "\"" + coreBinDirPath + "/gnuplot\" \"" + plotFile + "\"";
      let p = new ExternalProcess( gnuplotCommand );
      p.waitForFinished();
      if ( p.exitCode != 0 )
      {
         console.warningln( "gnuplot error: ", p.stderr.length > 0 ? "\n" + p.stderr.length : "" );
      }

      // open the png plot file
      if ( previewControl && File.exists( this.graphFilePath() ) )
      {
         let imgWin = ImageWindow.open( this.graphFilePath() )
         if ( imgWin.length > 0 )
         {
            imgWin = imgWin[ 0 ];
            let bmpImage = ( imgWin && imgWin.mainView && imgWin.mainView.image ) ? imgWin.mainView.image : undefined;
            if ( bmpImage )
            {
               previewControl.setImage( bmpImage, imgWin.mainView.image.width, imgWin.mainView.image.height );
            }
            if ( imgWin != null )
            {
               imgWin.forceClose();
            }
         }
      }
   }

   // -------------------------------------
   // HIGH LEVEL FUNCTIONS
   // -------------------------------------

   this.weightsPlotTemplate = ( N ) =>
   {
      this.command( "set xlabel 'Image index'" );
      this.command( "set ylabel 'Weight'" );
      this.command( "set title 'Image Integration Weights'" );
      this.command( "set nokey" );
      this.command( "set tics nomirror out scale 0.75" );
      this.command( "set xtics " + Math.max( 1, Math.floor( N / 20 ) ) );
      this.command( "set pointintervalbox 3" );
      this.command( "set grid back" );
      this.command( "set yr [0:1.1]" );
      this.command( "set ytics 0.1" );
   }

   this.gainPlotTemplate = () =>
   {
      this.command( "set xlabel 'steps'" );
      this.command( "set ylabel 'Gain'" );
      this.command( "set title 'Optimization Gain (%)'" );
      this.command( "set nokey" );
      this.command( "set tics nomirror out " );
      this.command( "set xtics 1" );
      this.command( "set pointintervalbox 3" );
      this.command( "set grid back" );
      this.command( "set style line 1 lc rgb 'red'  lt 1 lw 1 pt 7 ps 1" );
   }

   this.measurePlotTemplate = ( measureLabel ) =>
   {
      this.command( "set xlabel 'steps'" );
      this.command( "set ylabel '" + measureLabel + "'" );
      this.command( "set title 'Metric: " + measureLabel + "'" );
      this.command( "set nokey" );
      this.command( "set tics nomirror out" );
      this.command( "set xtics 1" );
      this.command( "set pointintervalbox 3" );
      this.command( "set grid back" );
      this.command( "set style line 1 lc rgb 'red'  lt 1 lw 1 pt 7 ps 1" );
   }

   this.plotState = ( state, previewControl ) =>
   {
      // generate the data file
      let dataFilePath = File.systemTempDirectory + '/plotData.txt';
      let dataContent = "";
      state.forEach( ( value, i ) =>
      {
         dataContent += ( i + 1 ) + " " + value + "\n";
      } );
      File.writeTextFile( dataFilePath, dataContent );

      // keep track of temporary files
      if ( cache )
         cache.addTemporaryFilePath( dataFilePath );

      let N = state.length;
      this.newPlot();
      this.weightsPlotTemplate( N );
      this.command( "set style line 1 lc rgb '#0060ad' lt 1 lw 1.8 pt 7 ps " + format( "%1.2f", this.dotSize( N ) ) )
      this.command( "plot '" + dataFilePath + "' using 1:2 with linespoints ls 1" )

      this.execute( previewControl, "plotState" );
   }

   this.plotIterations = ( iterations, highlightIndex, previewControl, plotGain, plotMeasure ) =>
   {
      if ( iterations.length == 0 ) return;

      // ITERATIONS
      // data gets plotted by columns, each column is an iteration, each row is a weight
      let dataFilePath = File.systemTempDirectory + '/plotData_Iterations.txt';
      let Ns = iterations[ 0 ].state.length;
      let table = iterations[ 0 ].state.map( ( _, i ) => "" + ( i + 1 ) + " " );
      let separator = ""
      iterations.forEach( it =>
      {
         it.state.forEach( ( value, i ) =>
         {
            table[ i ] = table[ i ] + separator + value
         } )
         separator = " "
      } )

      let dataContent = table.join( "\n" );
      File.writeTextFile( dataFilePath, dataContent );
      // keep track of temporary files
      if ( cache )
         cache.addTemporaryFilePath( dataFilePath );

      // OPTIMIZATION FUNCTIONS
      let funcFilePath = File.systemTempDirectory + '/plotData_Functions.txt';
      table = [ "Measure Gain" ];
      iterations.forEach( ( it ) =>
      {
         table.push( "" + it.index + " " + [ it.cost, it.mvp * 100, it.measure ].join( " " ) );
      } );
      dataContent = table.join( "\n" );
      File.writeTextFile( funcFilePath, dataContent );
      // keep track of temporary files
      if ( cache )
         cache.addTemporaryFilePath( funcFilePath );

      // start a new plot
      this.newPlot();

      let numberOfPlots = 1 + ( plotGain ? 1 : 0 ) + ( plotMeasure ? 1 : 0 );
      this.command( "set multiplot layout " + numberOfPlots + ", 1" )

      // plot gain
      if ( plotGain )
      {
         this.gainPlotTemplate();
         this.command( "plot '" + funcFilePath + "' using 1:3 with linespoints ls 1" )
      }

      // plot measure
      if ( plotMeasure )
      {
         let labels = {};
         iterations.forEach( it =>
         {
            labels[ it.optLbl ] = true
         } )
         let titleMeasure = Object.keys( labels ).length > 1 ? "Multiple" : iterations[ 0 ].optLbl;
         this.measurePlotTemplate( titleMeasure );
         let command = "plot '" + funcFilePath + "' using 1:4 with linespoints ls 1";
         if ( highlightIndex >= 0 )
            command += ", \\\n '" + funcFilePath + "' every ::" + ( highlightIndex + 1 ) + "::" + ( highlightIndex + 1 ) + " using 1:4 with circles";
         this.command( command )
      }

      // plot the weights
      this.weightsPlotTemplate( Ns );

      let N = iterations.length;
      let plotCommand = "plot";
      let separator = "";
      for ( let i = 0; i < N; i++ )
      {
         let converged = ( iterations[ i ].type == ITERATION_TYPE.CONVERGED || iterations[ i ].type == ITERATION_TYPE.SPEED_REDUCTIONS_STOP );

         let contrast = this.iterationsContrastValue || 0.5;
         let c = 0.5 * Math.pow( ( 1 - i / ( N - 1 ) ), contrast ) + 0.2;
         let a = Math.pow( c, 1.5 );
         let lw = 1;

         if ( i == ( N - 1 ) )
         {
            c = 0;
            a = 0;
            lw = 2;
         }
         let rgb;

         if ( i == highlightIndex )
         {
            rgb = "#e28743";
            lw = 2;
         }
         else
         {
            if ( iterations[ i ].colorHex )
            {
               rgb = iterations[ i ].colorHex;
               lw = 2;
            }
            else
            {
               rgb = this.rgbaHex( c, c, c, a );
            }
         }
         let dotSize = this.dotSize( Ns );
         if ( i != ( N - 1 ) && !converged )
            dotSize = dotSize / 2;
         this.command( "set style line " + ( i + 1 ) + " lc rgb '" + rgb + "'  lt 1 lw " + lw + " pt 7 ps " + format( "%1.2f", dotSize ) );
         plotCommand += separator + " '" + dataFilePath + "' using 1:" + ( i + 2 ) + ": " + ( i + 1 ) + " with linespoints ls " + ( i + 1 )
         separator = ", \\\n"
      }
      this.command( plotCommand );
      this.execute( previewControl, "plotIterations" );
   }
}

// ----------------------------------------------------------------------------
// EOF WeightsOptimizer-Plots.js - Released 2023-06-21T16:40:28Z

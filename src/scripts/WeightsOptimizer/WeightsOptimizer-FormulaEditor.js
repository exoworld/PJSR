// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// WeightsOptimizer-FormulaEditor.js - Released 2023-06-21T16:40:28Z
// ----------------------------------------------------------------------------
//
// This file is part of Weigfhts Optimizer Script version 1.1.0
//
// Copyright (c) 2022-2023 Roberto Sartori
// Copyright (c) 2022-2023 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

/* beautify ignore:start */
#include "WeightsOptimizer-SSCustomFormula.js"
/* beautify ignore:end */

function FormulaEditor( formula )
{
   this.__base__ = Dialog;
   this.__base__();
   this.setScaledMinWidth( 600 );
   this.setScaledMinHeight( 300 );
   this.windowTitle = "Weights Optimizer - Optimization Formula Editor";
   this.formula = formula;

   // ------------------------------
   //        FORMULA EDIT BOX
   // ------------------------------
   this.editBox = new TextBox( this );
   this.editBox.font = new Font( "monospace", 14 );
   this.editBox.text = formula;

   // ------------------------------
   //           NOTES
   // ------------------------------
   this.notes = new Label( this );
   this.notes.useRichText = true;
   this.notes.text = "<p><b>NOTES</b></p>" +
      "<p>Ensure to include a significant number of stars in your image or preview area when " +
      "the formula uses stars-based terms <b>FWHM</b>, <b>Eccentricity</b>, <b>PSFSignal</b>, <b>PSFCount</b>, " +
      "<b>PSFFlux</b>, <b>PSFFluxPower</b>, <b>Stars</b> and <b>StarResidual</b>. All these terms are zero " +
      "in absence of stars.</p>" +
      "<p>The terms <b>MStar</b> and <b>NStar</b> estimators require a working image larger than 512 pixels, " +
      "if your images or preview size are smaller then the values of these estimators is zero."
   this.notes.setFixedWidth( 240 );
   this.notes.wordWrapping = true;
   // ------------------------------
   //           OK BUTTON
   // ------------------------------
   this.OkButton = new PushButton( this );
   this.OkButton.text = "OK";
   this.OkButton.onClick = () =>
   {
      if ( this.validateFormula() )
      {
         // trim and replace sequences of spaces with a singe space
         this.formula = this.editBox.text.replace( /( +)/g, " " ).trim();
         this.ok();
      }
   }

   /**
    * Aux function that configures the OK button.
    *
    * @param {*} enabled
    */
   this.setOKenabled = ( enabled ) =>
   {
      this.OkButton.enabled = enabled;
      this.OkButton.text = enabled ? " OK" : " INVALID";
      this.OkButton.icon = this.scaledResource( enabled ? ":/browser/enabled.png" : ":/browser/disabled.png" )
   }

   // ----------------------
   // LAYOUT
   // ----------------------
   let hSizer = new HorizontalSizer;
   hSizer.addStretch();
   hSizer.add( this.OkButton );

   let editHSizer = new HorizontalSizer;
   editHSizer.spacing = WO_DEFAULT_SPACING;
   editHSizer.add( this.editBox );
   editHSizer.add( this.notes );

   this.sizer = new VerticalSizer;
   this.sizer.spacing = WO_DEFAULT_SPACING;
   this.sizer.margin = WO_DEFAULT_MARGIN;
   this.sizer.add( editHSizer );
   this.sizer.add( hSizer );

   /**
    * Prepares the variables to be injected to the JS code as SS does.
    * This list must follow any SS change.
    */
   this.prepareJS = () =>
   {
      let variables = [
         "FWHM",
         "FWHMMeanDev",
         "Eccentricity",
         "EccentricityMeanDev",
         "PSFSignal",
         "PSFSignalWeight",
         "PSFCount",
         "PSFFlux",
         "PSFTotalMeanFlux",
         "PSFFluxPower",
         "PSFTotalMeanPowerFlux",
         "SNR",
         "SNRWeight",
         "Median",
         "MedianMeanDev",
         "Noise",
         "NoiseRatio",
         "Stars",
         "StarResidual",
         "StarResidualMeanDev",
         "Azimuth",
         "Altitude",
         "MStar",
         "NStar"
      ];

      let postFixesAndValues = [
      {
         lbl: "",
         value: () => ( 1 + 100 * Math.random() )
      },
      {
         lbl: "Min",
         value: () => ( 1 + 10 * Math.random() )
      },
      {
         lbl: "Max",
         value: () => ( 90 + 10 * Math.random() )
      },
      {
         lbl: "Median",
         value: () => ( 45 + 10 * Math.random() )
      },
      {
         lbl: "Sigma",
         value: () => ( 1 + 2 * Math.random() )
      } ];

      let JSrows = [ "{ \nlet TEST = function() {\nlet index = 1" ];

      for ( let i = 0; i < variables.length; i++ )
      {
         let name = variables[ i ];
         // build all names with postfixes and assign the value
         for ( let j = 0; j < postFixesAndValues.length; j++ )
         {
            let postfix = postFixesAndValues[ j ];
            let fullname = name + postfix.lbl;
            let value = postfix.value();
            JSrows.push( "let " + fullname + "=" + value );
         }
      }
      JSrows.push( "let Index = 1" );

      this.JSvars = JSrows.join( ";\n" ) + ";\n";
   }
   this.prepareJS();

   /**
    * Performs the formula validation, returns TRUE if the formula content is a valid JS code.
    *
    * @return {*}
    */
   this.validateFormula = () =>
   {
      let isValid = true;
      try
      {
         let JSCode = this.JSvars + "return (" + this.editBox.text + ")\n } \nlet result = TEST();\nif (typeof result != typeof 0.1) throw 'non numeric result'; \n} "
         eval( JSCode );
      }
      catch ( e )
      {
         isValid = false;
      }

      return isValid;
   }

   /**
    * Performs the validation and updated the OK button accordingly.
    *
    */
   this.performValidation = () =>
   {
      let isValid = SSCustomFormula.validate( this.editBox.text );
      this.setOKenabled( isValid );
   }

   // ------------------------------
   // validation timer
   this.validationTimer = new Timer;
   this.validationTimer.interval = 1;
   this.validationTimer.periodic = true;
   this.validationTimer.dialog = this;
   this.validationTimer.onTimeout = () =>
   {
      this.performValidation();
   }
   this.validationTimer.start();

   this.onReturn = () =>
   {
      this.validationTimer.stop();
   }

   this.performValidation();
}

FormulaEditor.prototype = new Dialog;

// ----------------------------------------------------------------------------
// EOF WeightsOptimizer-FormulaEditor.js - Released 2023-06-21T16:40:28Z

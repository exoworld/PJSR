// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// WeightsOptimizer-SSCustomFormula.js - Released 2023-06-21T16:40:28Z
// ----------------------------------------------------------------------------
//
// This file is part of Weigfhts Optimizer Script version 1.1.0
//
// Copyright (c) 2022-2023 Roberto Sartori
// Copyright (c) 2022-2023 Pleiades Astrophoto
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (https://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------

let WO_SSCustomFormula_variables = [
{
   name: "FWHM",
   index: 5
},
{
   name: "Eccentricity",
   index: 6
},
{
   name: "PSFSignalWeight",
   index: 7,
   alias: "PSFSignal"
},
{
   name: "SNRWeight",
   index: 9,
   alias: "SNR"
},
{
   name: "Median",
   index: 10
},
{
   name: "MedianMeanDev",
   index: 11
},
{
   name: "Noise",
   index: 12
},
{
   name: "NoiseRatio",
   index: 13
},
{
   name: "Stars",
   index: 14
},
{
   name: "StarResidual",
   index: 15
},
{
   name: "FWHMMeanDev",
   index: 16
},
{
   name: "EccentricityMeanDev",
   index: 17
},
{
   name: "StarResidualMeanDev",
   index: 18
},
{
   name: "Azimuth",
   index: 19
},
{
   name: "Altitude",
   index: 20
},
{
   name: "PSFFlux",
   index: 21
},
{
   name: "PSFFluxPower",
   index: 22
},
{
   name: "PSFTotalMeanFlux",
   index: 23
},
{
   name: "PSFTotalMeanPowerFlux",
   index: 24
},
{
   name: "PSFCount",
   index: 25
},
{
   name: "MStar",
   index: 26
},
{
   name: "NStar",
   index: 27
} ];


function SSCustomFormula( SSOutputTable, log )
{

   if ( log )
      this.log = log;
   else
      this.log = () =>
      {}

   /**
    * Returns the i-th column as an array
    *
    * @param {*} i column index (zero based)
    */
   this.getColumn = ( i ) =>
   {
      let result = [];
      for ( let j = 0; j < this.data.length; j++ )
      {
         result.push( this.data[ j ][ i ] );
      }
      return result;
   }

   /**
    * Computes all the variants and prepare the JS header code to be injected before the formula.
    */
   this.initialize = () =>
   {
      let jsrows = [ "\nlet WO_CUSTOM_FORMULA = function() {" ]

      // scan all variables and check for the variants, if found then compute and inject
      for ( let i = 0; i < WO_SSCustomFormula_variables.length; i++ )
      {
         let variable = WO_SSCustomFormula_variables[ i ];
         let value = this.data[ variable.index ];
         jsrows.push( "let " + variable.name + "=" + ( isNaN( value ) ? 0 : value ) + ";" );
         if ( variable.alias )
         {
            jsrows.push( "let " + variable.alias + "=" + ( isNaN( value ) ? 0 : value ) + ";" );
         }
      }
      this.JSHeader = jsrows.join( "\n" );
   }

   /**
    * Computes the formula, assumes that the formula has already been validated.
    *
    * @param {*} formula
    */
   this.computeFormula = ( formula ) =>
   {
      // inject the custom formula execution instruction
      try
      {
         let jsfooter = "\nreturn (" + formula + ")\n };\nWO_CUSTOM_FORMULA();"

         let JSCode = this.JSHeader + jsfooter;
         this.log( "Custom formula code: " );
         this.log( JSCode );
         let result = eval( JSCode );
         this.log( "Result: ", result );
         if ( isNaN( result ) )
            retuls = 0;
         if ( typeof result == typeof 0.0 )
         {
            return result
         }
         else
         {
            throw "Non numeric measurement returned"
         }

      }
      catch ( e )
      {
         console.warningln( "Formula applied with errors: ", e )
      }
      return 0;
   }

   // store the measurements of the first image
   this.data = SSOutputTable[ 0 ];
   this.initialize();
}

/**
 * Initialize the variables available in the custom formula
 *
 */
SSCustomFormula.prepareJS = () =>
{

   let JSValidationRows = [ "{ \nlet TEST = function() {\n" ];

   for ( let i = 0; i < WO_SSCustomFormula_variables.length; i++ )
   {
      let variable = WO_SSCustomFormula_variables[ i ];
      JSValidationRows.push( "let " + variable.name + "=" + ( 1 + Math.random() ) );
      if ( variable.alias )
      {
         JSValidationRows.push( "let " + variable.alias + "=" + variable.name );
      }
   }

   SSCustomFormula.JSValidationHeader = JSValidationRows.join( ";\n" ) + "\n";
}

/**
 * Returns TRUE if the custom formula is a valid expression to be computated
 *
 * @param {*} expression
 */
SSCustomFormula.validate = ( expression ) =>
{

   // instantiate the vars if needed
   if ( SSCustomFormula.JSValidationHeader == undefined )
      SSCustomFormula.prepareJS();

   // run validation
   let isValid = true;
   try
   {
      let JSCode = SSCustomFormula.JSValidationHeader + "return (" + expression + ")\n } \nlet result = TEST();\nif (typeof result != typeof 0.1) throw 'non numeric result'; \n} "
      eval( JSCode );
   }
   catch ( e )
   {
      isValid = false;
   }

   return isValid;
}

// ----------------------------------------------------------------------------
// EOF WeightsOptimizer-SSCustomFormula.js - Released 2023-06-21T16:40:28Z

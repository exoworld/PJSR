/*
 * Aperture Photometry script
 *
 * Script for measuring the flux of the known stars in astronomical images.
 *
 * Copyright (C) 2013-2023, Andres del Pozo, Vicent Peris (OAUV)
 * Contributions (C) 2019-2023, Juan Conejero (PTeam)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/* beautify ignore:start */

#feature-id    AperturePhotometry : Image Analysis > AperturePhotometry

#feature-icon  @script_icons_dir/AperturePhotometry.svg

#feature-info  Script for measuring the flux of known stars in astronomical images.<br/>\
               <br/>\
               Copyright &copy;2013-2023 Andr&eacute;s del Pozo / Vicent Peris (OAUV)<br/>\
               Contributions &copy; 2019-2023 Juan Conejero (PTeam)

if ( CoreApplication === undefined ||
     CoreApplication.versionRevision === undefined ||
     CoreApplication.versionMajor*1e11
   + CoreApplication.versionMinor*1e8
   + CoreApplication.versionRelease*1e5
   + CoreApplication.versionRevision*1e2 < 100800800800 )
{
   throw new Error( "This script requires PixInsight core version 1.8.8-8 or higher." );
}

#define VERSION "1.5.2"
#define TITLE "Aperture Photometry"
#define SETTINGS_MODULE "PHOT"
#ifndef STAR_CSV_FILE
#define STAR_CSV_FILE   File.systemTempDirectory + "/stars.csv"
#endif

//#define ACTIVATE_EXPERIMENTAL 1

#include <pjsr/DataType.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/TextAlign.jsh>

#include "WCSmetadata.jsh"
#include "AstronomicalCatalogs.jsh"
#include "SpectrophotometricCatalogs.js"
//#include "CollapsibleFrame.js"

#define USE_SOLVER_LIBRARY true
#include "ImageSolver.js"
#include "CircleSquareIntersect.js"
#include "SearchCoordinatesDialog.js"
#include "AutoStretch.js"

#define STARFLAG_MULTIPLE     0x01
#define STARFLAG_OVERLAPPED   0x02
#define STARFLAG_BADPOS       0x04
#define STARFLAG_LOWSNR       0x08
#define STARFLAG_SATURATED    0x10

#define BKGWINDOW_RING        0
#define BKGWINDOW_PHOTOMETRIC 1

#define BKGMODEL_SOURCE       0
#define BKGMODEL_MMT          1
#define BKGMODEL_ABE          2

#define EXTRACTMODE_IMAGE     0
#define EXTRACTMODE_REFERENCE 1
#define EXTRACTMODE_CATALOG   2

/* beautify ignore:end */

function StarCatalog()
{
   this.name = null;
   this.posEq = null;
}

function StarReference()
{
   this.orgPosPx = null; // Original position after aplying the referentiation matrix
   this.imgPosPx = null; // Position measured on the image
   this.flags = 0;
}

function StarImage()
{
   //   this.orgPosPx=null; // Original position after aplying the referentiation matrix
   this.imgPosPx = null; // Position measured on the image
   this.flux = null;
   //   this.centroidPx=null;
   this.flags = 0;
   this.hfd = null;
}

var ShowObject = function( msg, obj )
{
   console.write( msg, ":" );
   for ( let key in obj )
      console.write( " ", key );
   console.writeln();
};

// ----------------------------------------------------------------------------

function ImagesTab( parent )
{
   this.__base__ = Control;
   this.__base__( parent );

   this.Validate = function()
   {
      if ( this.dialog.engine.useActive && this.dialog.engine.currentWindow == null )
      {
         (new MessageBox( "There is no active image window", TITLE, StdIcon_Error, StdButton_Ok )).execute();
         return false;
      }
      if ( !this.dialog.engine.useActive && (this.dialog.engine.files == null || this.dialog.engine.files.length == 0) )
      {
         (new MessageBox( "The list of target files is empty", TITLE, StdIcon_Error, StdButton_Ok )).execute();
         return false;
      }
      return true;
   };

   this.selected_Radio = new RadioButton( this );
   this.selected_Radio.text = "Active image";
   this.selected_Radio.minWidth = parent.labelWidth;
   this.selected_Radio.toolTip = "<p>The photometry will only be computed for the active image.</p>";
   this.selected_Radio.onCheck = function()
   {
      this.dialog.engine.useActive = true;
      this.parent.parent.EnableFileControls();
   };

   this.files_Radio = new RadioButton( this );
   this.files_Radio.text = "Local files:";
   this.files_Radio.minWidth = parent.labelWidth;
   this.files_Radio.toolTip = "<p>The photometry will be computed for the selected files.</p>";
   this.files_Radio.onCheck = function()
   {
      this.dialog.engine.useActive = false;
      this.parent.parent.EnableFileControls();
   };

   this.targetSelection_Control = new Control( this );
   this.targetSelection_Control.sizer = new VerticalSizer;
   this.targetSelection_Control.sizer.spacing = 4;
   this.targetSelection_Control.sizer.add( this.selected_Radio );
   this.targetSelection_Control.sizer.add( this.files_Radio );

   this.selected_Radio.checked = this.dialog.engine.useActive;
   this.files_Radio.checked = !this.dialog.engine.useActive;

   // List of files
   this.files_List = new TreeBox( this );
   this.files_List.rootDecoration = false;
   this.files_List.alternateRowColor = true;
   this.files_List.multipleSelection = true;
   this.files_List.headerVisible = false;
   this.files_List.numberOfColumns = 2;
   this.files_List.showColumn( 1, false );
   this.files_List.toolTip = "<p>List of files for which the photometry will be computed.</p>";
   if ( this.dialog.engine.files )
   {
      for ( let i = 0; i < this.dialog.engine.files.length; ++i )
      {
         let node = new TreeBoxNode( this.files_List );
         node.setText( 0, this.dialog.engine.files[i] );
      }
   }
   else
      this.dialog.engine.files = new Array();

   // Add file button
   this.add_Button = new PushButton( this );
   this.add_Button.text = "Add files";
   this.add_Button.toolTip = "Add files to the list";
   this.add_Button.onMousePress = function()
   {
      let ofd = new OpenFileDialog;
      ofd.multipleSelections = true;
      ofd.caption = "Select files";
      //ofd.loadImageFilters();
      ofd.filters = [
         [ "All supported formats", ".xisf", ".fit", ".fits", ".fts" ],
         [ "XISF Files", ".xisf" ],
         [ "FITS Files", ".fit", ".fits", ".fts" ]
      ];
      if ( ofd.execute() )
      {
         for ( let i = 0; i < ofd.fileNames.length; ++i )
         {
            this.dialog.engine.files.push( ofd.fileNames[i] );
            let node = new TreeBoxNode( this.parent.files_List );
            node.checkable = false;
            node.setText( 0, ofd.fileNames[i] );
         }
         this.parent.files_List.adjustColumnWidthToContents( 1 );
      }
   };

   // Remove file button
   this.remove_Button = new PushButton( this );
   this.remove_Button.text = "Remove files";
   this.remove_Button.toolTip = "Removes the selected files from the list";
   this.remove_Button.onMousePress = function()
   {
      for ( let i = this.parent.files_List.numberOfChildren - 1; i >= 0; i-- )
         if ( this.parent.files_List.child( i ).selected )
         {
            this.dialog.engine.files.splice( i, 1 );
            this.parent.files_List.remove( i );
         }
   };

   // Clear files button
   this.clear_Button = new PushButton( this );
   this.clear_Button.text = "Clear files";
   this.clear_Button.toolTip = "Clears the list of files";
   this.clear_Button.onMousePress = function()
   {
      this.parent.files_List.clear();
      this.dialog.engine.files = new Array();
   };

   // Buttons for managing the list of files
   this.files_Buttons = new HorizontalSizer;
   this.files_Buttons.spacing = 8;
   this.files_Buttons.add( this.add_Button );
   this.files_Buttons.add( this.remove_Button );
   this.files_Buttons.add( this.clear_Button );
   this.files_Buttons.addStretch();

   this.files_Sizer = new VerticalSizer;
   this.files_Sizer.spacing = 8;
   this.files_Sizer.add( this.files_List, 100 );
   this.files_Sizer.add( this.files_Buttons );

   this.EnableFileControls = function()
   {
      this.files_List.enabled = !this.dialog.engine.useActive;
      this.add_Button.enabled = !this.dialog.engine.useActive;
      this.remove_Button.enabled = !this.dialog.engine.useActive;
      this.clear_Button.enabled = !this.dialog.engine.useActive;
   };

   this.EnableFileControls();

   this.EnableSolveControls = function()
   {
      this.saveSolve_Check.enabled = this.dialog.engine.autoSolve || this.dialog.engine.forceSolve;
      this.suffix_Edit.enabled = ( this.dialog.engine.autoSolve || this.dialog.engine.forceSolve ) && this.dialog.engine.saveSolve;
      this.configSolver_Button.enabled = this.dialog.engine.autoSolve || this.dialog.engine.forceSolve;
      this.UseImageMetadata_Radio.enabled = this.dialog.engine.autoSolve || this.dialog.engine.forceSolve;
      this.UseSolverMetadata_Radio.enabled = this.dialog.engine.autoSolve || this.dialog.engine.forceSolve;
   };

   //
   this.autoSolve_Check = new CheckBox( this );
   this.autoSolve_Check.text = "Plate-solve unsolved images";
   this.autoSolve_Check.toolTip = "<p>When this option is active, unsolved images are solved using the ImageSolver script.</p>";
   this.autoSolve_Check.checked = this.dialog.engine.autoSolve;
   this.autoSolve_Check.onCheck = function( checked )
   {
      this.dialog.engine.autoSolve = checked;
      this.dialog.images_Tab.EnableSolveControls();
   };

   this.configSolver_Button = new PushButton( this );
   this.configSolver_Button.text = "Configure solver";
   this.configSolver_Button.toolTip = "<p>Opens the configuration dialog for the ImageSolver script</p>";
   this.configSolver_Button.enabled = this.dialog.engine.autoSolve || this.dialog.engine.forceSolve;
   this.configSolver_Button.onClick = function()
   {
      let solver = new ImageSolver();

      for ( ;; )
      {
         let solverWindow = null;
         if ( this.dialog.engine.useActive )
            solverWindow = ImageWindow.activeWindow;
         else if ( this.dialog.engine.files != null && this.dialog.engine.files.length > 0 )
            solverWindow = ImageWindow.open( this.dialog.engine.files[0] )[0];
         if ( solverWindow == null )
            return new MessageBox( "There is no selected file or window.", TITLE, StdIcon_Error, StdButton_Ok ).execute();

         solver.Init( solverWindow );

         let dialog = new ImageSolverDialog( solver.solverCfg, solver.metadata, false );
         if ( dialog.execute() )
         {
            solver.solverCfg.SaveSettings();
            solver.metadata.SaveSettings();
            if ( !this.dialog.engine.useActive )
               solverWindow.close();
            break;
         }

         if ( !dialog.resetRequest )
            return null;

         solver = new ImageSolver();
      }

      return null;
   };

   this.solver_Sizer = new HorizontalSizer;
   this.solver_Sizer.spacing = 8;
   this.solver_Sizer.add( this.autoSolve_Check );
   this.solver_Sizer.add( this.configSolver_Button );
   this.solver_Sizer.addStretch();

   //
   this.UseImageMetadata_Radio = new RadioButton( this );
   this.UseImageMetadata_Radio.text = "Use image metadata";
   this.UseImageMetadata_Radio.checked = this.dialog.engine.solverUseImageMetadata == true;
   this.UseImageMetadata_Radio.toolTip = "<p>When solving an image, the solver will prioritize the image metadata (coordinates, focal length, epoch)." +
      " If any of the parameters is not available then the solver will replace them with the values specified in the solver dialog.</p>";
   this.UseImageMetadata_Radio.enabled = this.dialog.engine.autoSolve || this.dialog.engine.forceSolve;
   this.UseImageMetadata_Radio.onCheck = function( value )
   {
      this.dialog.engine.solverUseImageMetadata = true;
      this.dialog.images_Tab.EnableSolveControls();
   };

   this.UseSolverMetadata_Radio = new RadioButton( this );
   this.UseSolverMetadata_Radio.text = "Use solver configuration";
   this.UseSolverMetadata_Radio.checked = this.dialog.engine.solverUseImageMetadata != true;
   this.UseSolverMetadata_Radio.toolTip = "<p>When solving an image, the solver will use the configuration specified in the solver dialog, " +
      " ignoring the values (coordinates, focal length, epoch, ...) that the images could have.</p>";
   this.UseSolverMetadata_Radio.enabled = this.dialog.engine.autoSolve || this.dialog.engine.forceSolve;
   this.UseSolverMetadata_Radio.onCheck = function( value )
   {
      this.dialog.engine.solverUseImageMetadata = false;
      this.dialog.images_Tab.EnableSolveControls();
   };

   //
   this.forceSolve_Check = new CheckBox( this );
   this.forceSolve_Check.text = "Force plate solving already solved images";
   this.forceSolve_Check.toolTip = "<p>When this option is active, all of the images are solved using the ImageSolver script.<br/>" +
      "This option can be used when the current referentiation is not good enough and you want to improve it.</p>";
   this.forceSolve_Check.checked = this.dialog.engine.forceSolve != null && this.dialog.engine.forceSolve;
   this.forceSolve_Check.onCheck = function( checked )
   {
      this.dialog.engine.forceSolve = checked;
      this.dialog.images_Tab.EnableSolveControls();
   };

   //
   this.saveSolve_Check = new CheckBox( this );
   this.saveSolve_Check.text = "Save the astrometric solution";
   this.saveSolve_Check.toolTip = "<p>When this option is active, the astrometric solution is saved in the image file.</p>";
   this.saveSolve_Check.checked = this.dialog.engine.saveSolve;
   this.saveSolve_Check.enabled = this.dialog.engine.autoSolve || this.dialog.engine.forceSolve;
   this.saveSolve_Check.onCheck = function( checked )
   {
      this.dialog.engine.saveSolve = checked;
      this.dialog.images_Tab.EnableSolveControls();
   };

   //
   this.suffix_Label = new Label( this );
   this.suffix_Label.text = "Output file suffix:";
   this.suffix_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;

   this.suffix_Edit = new Edit( this );
   this.suffix_Edit.text = this.dialog.engine.solveSuffix ? this.dialog.engine.solveSuffix : "";
   this.suffix_Edit.minWidth = parent.editWidth;
   this.suffix_Edit.toolTip = "<p>This suffix will be appended to the filename when saving the astrometric solution.<br/>" +
      "If it is empty, the original file will be overwritten.</p>";
   this.suffix_Edit.enabled = ( this.dialog.engine.autoSolve || this.dialog.engine.forceSolve ) && this.dialog.engine.saveSolve;
   this.suffix_Edit.onTextUpdated = function( value )
   {
      this.dialog.engine.solveSuffix = value ? value.trim() : "";
   };

   this.suffix_Sizer = new HorizontalSizer;
   this.suffix_Sizer.spacing = 4;
   this.suffix_Sizer.add( this.suffix_Label );
   this.suffix_Sizer.add( this.suffix_Edit );
   this.suffix_Sizer.addStretch();

   this.plateSolver_Group = new GroupBox( this );
   this.plateSolver_Group.title = "Plate Solver Parameters";
   this.plateSolver_Group.sizer = new VerticalSizer;
   this.plateSolver_Group.sizer.margin = 8;
   this.plateSolver_Group.sizer.spacing = 4;
   this.plateSolver_Group.sizer.add( this.solver_Sizer );
   this.plateSolver_Group.sizer.add( this.forceSolve_Check );
   this.plateSolver_Group.sizer.add( this.UseImageMetadata_Radio );
   this.plateSolver_Group.sizer.add( this.UseSolverMetadata_Radio );
   this.plateSolver_Group.sizer.add( this.saveSolve_Check );
   this.plateSolver_Group.sizer.add( this.suffix_Sizer );

   // Global sizer
   this.sizer = new VerticalSizer();
   this.sizer.margin = 8;
   this.sizer.spacing = 4;
   this.sizer.add( this.targetSelection_Control );
   this.sizer.add( this.files_Sizer, 100 );
   this.sizer.addSpacing( 16 );
   this.sizer.add( this.plateSolver_Group );
}

ImagesTab.prototype = new Control;

// ----------------------------------------------------------------------------

function StarsTab( parent )
{
   this.__base__ = Control;
   this.__base__( parent );

   var FillFilterCombo = function( combo, catalog, filter )
   {
      combo.clear();
      for ( let f = 0; f < catalog.filters.length; ++f )
      {
         combo.addItem( catalog.filters[f] );
         if ( catalog.filters[f] == filter )
            combo.currentItem = combo.numberOfItems - 1;
      }
   };

   // Stars source

   //
   this.magnitudeFilter_Combo = new ComboBox( this );
   this.magnitudeFilter_Combo.editEnabled = false;
   this.magnitudeFilter_Combo.toolTip = "<p>Filter used in the magnitude test.</p>";
   this.magnitudeFilter_Combo.onItemSelected = function()
   {
      this.dialog.engine.catalogFilter = this.itemText( this.currentItem );
   };

   //
   this.catalog_label = new Label( this );
   this.catalog_label.text = "Catalog:";
   this.catalog_label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.catalog_label.minWidth = parent.labelWidth;

   this.catalog_Combo = new ComboBox( this );
   this.catalog_Combo.editEnabled = false;
   this.catalog_Combo.magnitudeCombo = this.magnitudeFilter_Combo;
   for ( let i = 0; i < __catalogRegister__.catalogs.length; ++i )
   {
      let catalog = __catalogRegister__.GetCatalog( i );
      if ( catalog.filters )
      {
         this.catalog_Combo.addItem( catalog.name );
         if ( this.dialog.engine.catalogName == catalog.name )
         {
            this.catalog_Combo.currentItem = this.catalog_Combo.numberOfItems - 1;
            FillFilterCombo( this.magnitudeFilter_Combo, catalog, this.dialog.engine.catalogFilter );
         }
      }
   }
   this.catalog_Combo.minWidth = parent.editWidth;
   this.catalog_Combo.toolTip = "<p>Catalog that contains the coordinates of the stars that will be measured.</p>";
   this.catalog_Combo.onItemSelected = function()
   {
      this.dialog.engine.catalogName = this.itemText( this.currentItem )
      let catalog = __catalogRegister__.GetCatalog( this.dialog.engine.catalogName );
      FillFilterCombo( this.magnitudeCombo, catalog, catalog.magnitudeFilter );
      this.dialog.engine.catalogFilter = catalog.magnitudeFilter;
   };

   this.catalog_Sizer = new HorizontalSizer;
   this.catalog_Sizer.spacing = 4;
   this.catalog_Sizer.add( this.catalog_label );
   this.catalog_Sizer.add( this.catalog_Combo );
   this.catalog_Sizer.addStretch();

   //
   this.server_label = new Label( this );
   this.server_label.text = "VizieR server:";
   this.server_label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.server_label.minWidth = parent.labelWidth;

   this.mirror_Combo = new ComboBox( this );
   this.mirror_Combo.editEnabled = false;
   this.mirror_Combo.toolTip = "<p>Select the best VizieR server for your location.</p>";
   this.mirror_Combo.setFixedWidth( this.font.width( "Mnananai" ) * 5 );
   for ( let m = 0; m < VizierCatalog.mirrors.length; ++m )
   {
      this.mirror_Combo.addItem( VizierCatalog.mirrors[ m ].name );
      if ( VizierCatalog.mirrors[ m ].address == this.dialog.engine.vizierServer )
         this.mirror_Combo.currentItem = parseInt( m );
   }
   this.mirror_Combo.onItemSelected = function()
   {
      this.dialog.engine.vizierServer = VizierCatalog.mirrors[ this.currentItem ].address;
   };
   this.server_Sizer = new HorizontalSizer;
   this.server_Sizer.spacing = 4;
   this.server_Sizer.add( this.server_label );
   this.server_Sizer.add( this.mirror_Combo );
   this.server_Sizer.addStretch();

   //
   this.maxMag_Label = new Label( this );
   this.maxMag_Label.text = "Maximum magnitude:";
   this.maxMag_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.maxMag_Label.minWidth = parent.labelWidth;

   this.maxMag_Edit = new Edit( this );
   if ( this.dialog.engine.maxMagnitude != null )
      this.maxMag_Edit.text = format( "%.3f", this.dialog.engine.maxMagnitude );
   this.maxMag_Edit.minWidth = parent.editWidth;
   this.maxMag_Edit.toolTip = "<p>Maximum magnitude extracted from the catalog.</p>";
   this.maxMag_Edit.onTextUpdated = function( value )
   {
      this.dialog.engine.maxMagnitude = parseFloat( value );
   };

   this.maxMag_Sizer = new HorizontalSizer;
   this.maxMag_Sizer.spacing = 4;
   this.maxMag_Sizer.add( this.maxMag_Label );
   this.maxMag_Sizer.add( this.maxMag_Edit );
   this.maxMag_Sizer.add( this.magnitudeFilter_Combo );
   this.maxMag_Sizer.addStretch();

   //
   this.autoMag_CheckBox = new CheckBox( this );
   this.autoMag_CheckBox.text = "Automatic limit magnitude (XPSD)";
   this.autoMag_CheckBox.toolTip = "<p>When an XPSD server catalog is selected " +
      "(such as an APASS or Gaia XPSD catalog), compute an optimal limit magnitude " +
      "automatically using fast XPSD server search operations. In such cases the " +
      "<i>Maximum magnitude</i> parameter will be ignored.</p>"
   this.autoMag_CheckBox.checked = this.dialog.engine.autoMagnitude;
   this.autoMag_CheckBox.onCheck = function( checked )
   {
      this.dialog.engine.autoMagnitude = checked;
   };

   this.autoMag_Sizer = new HorizontalSizer;
   this.autoMag_Sizer.addUnscaledSpacing( parent.labelWidth + this.logicalPixelsToPhysical( 4 ) );
   this.autoMag_Sizer.add( this.autoMag_CheckBox );
   this.autoMag_Sizer.addStretch();


   this.starsSource_Group = new GroupBox( this );
   this.starsSource_Group.title = "Star Catalog";
   this.starsSource_Group.sizer = new VerticalSizer;
   this.starsSource_Group.sizer.margin = 8;
   this.starsSource_Group.sizer.spacing = 4;
   this.starsSource_Group.sizer.add( this.catalog_Sizer );
   this.starsSource_Group.sizer.add( this.server_Sizer );
   this.starsSource_Group.sizer.add( this.maxMag_Sizer );
   this.starsSource_Group.sizer.add( this.autoMag_Sizer );

   // MANUAL OBJECTS

   this.AddObjectNode = function( object, node )
   {
      if ( node == null )
         node = new TreeBoxNode( this.manual_List );
      node.setText( 0, object.name ? object.name : "" );
      node.setText( 1, DMSangle.FromAngle( object.posEq.x / 15 ).ToString( true ) );
      node.setText( 2, DMSangle.FromAngle( object.posEq.y ).ToString() );
      return node;
   };

   //this.dialog.engine.manualObjects = [ {name:"Test1", posEq:{x:238.1234567890123456789,y:-17}},{name:"Test2", posEq:{x:18,y:88}}];
   this.manual_List = new TreeBox( this );
   this.manual_List.rootDecoration = false;
   this.manual_List.alternateRowColor = true;
   this.manual_List.multipleSelection = true;
   this.manual_List.headerVisible = true;
   this.manual_List.numberOfColumns = 3;
   this.manual_List.setScaledFixedHeight( 120 );
   this.manual_List.setHeaderText( 0, "Name" );
   this.manual_List.setHeaderText( 1, "RA" );
   this.manual_List.setHeaderText( 2, "Dec" );
   if ( this.dialog.engine.manualObjects )
   {
      for ( let i = 0; i < this.dialog.engine.manualObjects.length; ++i )
         this.AddObjectNode( this.dialog.engine.manualObjects[i], null );
   }
   else
      this.dialog.engine.manualObjects = [];
   this.manual_List.onNodeSelectionUpdated = function()
   {
      let numSelected = this.dialog.stars_Tab.manual_List.selectedNodes.length;
      this.dialog.stars_Tab.manualDelete_Button.enabled = numSelected > 0;
      this.dialog.stars_Tab.manualEdit_Button.enabled = numSelected == 1;
   };

   this.manualAdd_Button = new ToolButton( this );
   this.manualAdd_Button.icon = this.scaledResource( ":/icons/add.png" );
   this.manualAdd_Button.setScaledFixedSize( 20, 20 );
   this.manualAdd_Button.toolTip = "<p>Add a new object</p>";
   this.manualAdd_Button.onMousePress = function()
   {
      let coordsDlg = new SearchCoordinatesDialog( null, true, true );
      if ( coordsDlg.execute() )
      {
         let object = coordsDlg.object;
         if ( object == null )
            return;
         this.dialog.stars_Tab.AddObjectNode( object, null );
         this.dialog.engine.manualObjects.push( object );
      }
   };

   this.manualDelete_Button = new ToolButton( this );
   this.manualDelete_Button.icon = this.scaledResource( ":/icons/delete.png" );
   this.manualDelete_Button.setScaledFixedSize( 20, 20 );
   this.manualDelete_Button.toolTip = "<p>Delete the selected objects.</p>";
   this.manualDelete_Button.enabled = false;
   this.manualDelete_Button.onMousePress = function()
   {
      let manual_List = this.dialog.stars_Tab.manual_List;
      for ( let i = manual_List.numberOfChildren - 1; i >= 0; i-- )
      {
         if ( manual_List.child( i ).selected )
         {
            this.dialog.engine.manualObjects.splice( i, 1 );
            manual_List.remove( i );
         }
      }
   };

   this.manualEdit_Button = new ToolButton( this );
   this.manualEdit_Button.icon = this.scaledResource( ":/icons/list-edit.png" );
   this.manualEdit_Button.setScaledFixedSize( 20, 20 );
   this.manualEdit_Button.toolTip = "<p>Edit the coordinates of the selected object.</p>";
   this.manualEdit_Button.enabled = false;
   this.manualEdit_Button.onMousePress = function()
   {
      let frame = this.dialog.stars_Tab;
      if ( frame.manual_List.selectedNodes.length == 1 )
      {
         let objectIdx = frame.manual_List.childIndex( frame.manual_List.selectedNodes[0] );
         let coordsDlg = new SearchCoordinatesDialog( this.dialog.engine.manualObjects[objectIdx], true, true );
         if ( coordsDlg.execute() )
         {
            let object = coordsDlg.object;
            if ( object == null )
               return;
            this.dialog.stars_Tab.AddObjectNode( object, frame.manual_List.selectedNodes[0] );
            this.dialog.engine.manualObjects[ objectIdx ] = object;
         }
      }
   };

   this.manualButtons_Sizer = new HorizontalSizer;
   this.manualButtons_Sizer.spacing = 8;
   this.manualButtons_Sizer.add( this.manualAdd_Button );
   this.manualButtons_Sizer.add( this.manualDelete_Button );
   this.manualButtons_Sizer.add( this.manualEdit_Button );
   this.manualButtons_Sizer.addStretch();

   this.manualObjects_Group = new GroupBox( this );
   this.manualObjects_Group.title = "User Defined Objects";
   this.manualObjects_Group.sizer = new VerticalSizer;
   this.manualObjects_Group.sizer.margin = 8;
   this.manualObjects_Group.sizer.add( this.manual_List, 100 );
   this.manualObjects_Group.sizer.addSpacing( 8 );
   this.manualObjects_Group.sizer.add( this.manualButtons_Sizer );

   //
   this.image_Label = new Label( this );
   this.image_Label.text = "Extract stars from:";
   this.image_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;

   this.catalog_Radio = new RadioButton( this );
   this.catalog_Radio.text = "Catalog";
   this.catalog_Radio.styleSheet = "QRadioButton { padding-left: 24px;}";
   this.catalog_Radio.checked = this.dialog.engine.extractMode == EXTRACTMODE_CATALOG;
   //this.each_Radio.minWidth = parent.labelWidth;
   this.catalog_Radio.toolTip = "<p>The positions of the stars are calculated by projecting the catalog coordinates " +
      "to image coordinates using the referentiation stored in the image.<br/>" +
      "This option is only valid when the referentiation is very good. However, if the referentiation " +
      "is good enough, it allows to predict the positions of stars in the image with very low SNR.</p>";
   this.catalog_Radio.onCheck = function( value )
   {
      this.dialog.engine.extractMode = EXTRACTMODE_CATALOG;
      this.dialog.stars_Tab.reference_Combo.enabled = false;
   };

   this.each_Radio = new RadioButton( this );
   this.each_Radio.text = "Each image";
   this.each_Radio.styleSheet = "QRadioButton { padding-left: 24px;}";
   this.each_Radio.checked = this.dialog.engine.extractMode == EXTRACTMODE_IMAGE;
   //this.each_Radio.minWidth = parent.labelWidth;
   this.each_Radio.toolTip = "<p>The stars are extracted from each image searching for the best position, " +
      "using the catalog coordinates as a starting point.</p>";
   this.each_Radio.onCheck = function( value )
   {
      this.dialog.engine.extractMode = EXTRACTMODE_IMAGE;
      this.dialog.stars_Tab.reference_Combo.enabled = false;
   };

   this.reference_Radio = new RadioButton( this );
   this.reference_Radio.text = "Reference image (EXPERIMENTAL)";
   this.reference_Radio.styleSheet = "QRadioButton { padding-left: 24px;}";
   this.reference_Radio.checked = this.dialog.engine.extractMode == EXTRACTMODE_REFERENCE;
   //this.reference_Radio.minWidth = parent.labelWidth;
   this.reference_Radio.toolTip = "<p>The stars are extracted from a reference image and then mapped to each image.<br/>" +
      "If the reference image is the integration of several images, then the SNR is higher and the star extraction process " +
      "<i>should</i> be more precise.<br/><b>THIS OPTION IS EXPERIMENTAL</b></p>";
   this.reference_Radio.onCheck = function( value )
   {
      this.dialog.engine.extractMode = EXTRACTMODE_REFERENCE;
      this.dialog.stars_Tab.reference_Combo.enabled = true;
   };

   this.reference_Combo = new ComboBox( this );
   this.reference_Combo.styleSheet = "QComboBox { margin-left: 48px;}";
   this.reference_Combo.addItem( "<None>" );
   for ( let windowList = ImageWindow.windows, i = 0; i < windowList.length; ++i )
   {
      if ( windowList[i].mainView.image.numberOfChannels == 1
         /*&&
               windowList[i].mainView.image.width == this.dialog.engine.window.mainView.image.width &&
               windowList[i].mainView.image.height == this.dialog.engine.window.mainView.image.height*/
      )
      {
         this.reference_Combo.addItem( windowList[i].mainView.id );
         if ( this.dialog.engine.starsReference == windowList[i].mainView.id )
            this.reference_Combo.currentItem = this.reference_Combo.numberOfItems - 1;
      }
   }
   this.reference_Combo.enabled = this.dialog.engine.extractMode == EXTRACTMODE_REFERENCE;
   this.reference_Combo.toolTip = "<p>Reference image used in the star extraction process.</p>";
   this.reference_Combo.onItemSelected = function()
   {
      if ( this.currentItem == 0 )
         this.dialog.engine.starsReference = null;
      else
         this.dialog.engine.starsReference = this.itemText( this.currentItem );
   };

   //

   this.starExtraction_Group = new GroupBox( this );
   this.starExtraction_Group.title = "Source for Star Extraction";
   this.starExtraction_Group.sizer = new VerticalSizer;
   this.starExtraction_Group.sizer.margin = 8;
   this.starExtraction_Group.sizer.spacing = 4;
   this.starExtraction_Group.sizer.add( this.image_Label, 100, Align_Left );
   this.starExtraction_Group.sizer.add( this.catalog_Radio );
   this.starExtraction_Group.sizer.add( this.each_Radio );
   this.starExtraction_Group.sizer.add( this.reference_Radio );
   this.starExtraction_Group.sizer.add( this.reference_Combo );

   //

   this.starsOptions_Group = new GroupBox( this );
   this.starsOptions_Group.title = "Star Extraction Options";
   this.starsOptions_Group.sizer = new VerticalSizer;
   this.starsOptions_Group.sizer.margin = 8;
   this.starsOptions_Group.sizer.spacing = 4;

   //

   this.margin_Label = new Label( this );
   this.margin_Label.text = "Margin:";
   this.margin_Label.textAlignment = TextAlign_Left | TextAlign_VertCenter;

   this.margin_Edit = new Edit( this );
   if ( this.dialog.engine.margin != null )
      this.margin_Edit.text = format( "%d", this.dialog.engine.margin );
   this.margin_Edit.toolTip = "<p>Minimum distance in pixels to the image borders.</p>";
   this.margin_Edit.minWidth = parent.editWidth;
   this.margin_Edit.onTextUpdated = function( value )
   {
      let margin = parseInt( value );
      if ( margin > 0 )
         this.dialog.engine.margin = margin;
      else
         (new MessageBox( "The margin must be greater than 0", TITLE, StdIcon_Error, StdButton_Ok )).execute();
   };

   this.marginUnits_Label = new Label( this );
   this.marginUnits_Label.text = "pixels";
   this.marginUnits_Label.textAlignment = TextAlign_Left | TextAlign_VertCenter;

   this.margin_Sizer = new HorizontalSizer;
   this.margin_Sizer.spacing = 4;
   this.margin_Sizer.add( this.margin_Label );
   this.margin_Sizer.add( this.margin_Edit );
   this.margin_Sizer.add( this.marginUnits_Label );
   this.margin_Sizer.addStretch();
   this.starsOptions_Group.sizer.add( this.margin_Sizer );

   // Global sizer
   this.sizer = new VerticalSizer();
   this.sizer.margin = 8;
   this.sizer.spacing = 16;
   this.sizer.add( this.starsSource_Group );
   this.sizer.add( this.manualObjects_Group );
   this.sizer.add( this.starExtraction_Group );
   this.sizer.add( this.starsOptions_Group );
   this.sizer.addStretch();

}

StarsTab.prototype = new Control;

// ----------------------------------------------------------------------------

function FluxTab( parent )
{
   this.__base__ = Control;
   this.__base__( parent );

   this.Validate = function()
   {
      if ( this.dialog.engine.manualFilter && ( this.dialog.engine.filter == null || this.dialog.engine.filter.length == 0 ) )
      {
         new MessageBox( "The filter field is empty", TITLE, StdIcon_Error, StdButton_Ok ).execute();
         return false;
      }
      if ( !this.dialog.engine.manualFilter && ( this.dialog.engine.filterKeyword == null || this.dialog.engine.filterKeyword.length == 0 ) )
      {
         new MessageBox( "The filter keyword is empty", TITLE, StdIcon_Error, StdButton_Ok ).execute();
         return false;
      }

      if ( this.dialog.engine.minimumAperture <= 0 )
      {
         new MessageBox( "The aperture must be greater than 0.", TITLE, StdIcon_Error, StdButton_Ok ).execute();
         return false;
      }

      if ( this.dialog.engine.apertureSteps < 1 )
      {
         new MessageBox( "The number of aperture steps must be greater than zero.", TITLE, StdIcon_Error, StdButton_Ok ).execute();
         return false;
      }

      if ( this.dialog.engine.apertureSteps >= 2 && this.dialog.engine.apertureStepSize <= 0 )
      {
         new MessageBox( "The aperture step size must be greater than zero", TITLE, StdIcon_Error, StdButton_Ok ).execute();
         return false;
      }

      return true;
   };

   //
   this.filter_Label = new RadioButton( this );
   this.filter_Label.text = "Filter:";
   this.filter_Label.checked = this.dialog.engine.manualFilter;
   this.filter_Label.textAlignment = TextAlign_Left | TextAlign_VertCenter;
   this.filter_Label.minWidth = parent.labelWidth;
   this.filter_Label.onCheck = function( value )
   {
      this.dialog.engine.manualFilter = true;
      this.parent.EnableFilterControls();
   };

   this.filter_Combo = new ComboBox( this );
   this.filter_Combo.editEnabled = true;
   this.filter_Combo.addItem( "R" );
   this.filter_Combo.addItem( "G" );
   this.filter_Combo.addItem( "B" );
   this.filter_Combo.addItem( "Johnson U" );
   this.filter_Combo.addItem( "Johnson B" );
   this.filter_Combo.addItem( "Johnson V" );
   this.filter_Combo.addItem( "Johnson R" );
   this.filter_Combo.addItem( "Johnson I" );
   this.filter_Combo.addItem( "SDSS u'" );
   this.filter_Combo.addItem( "SDSS g'" );
   this.filter_Combo.addItem( "SDSS r'" );
   this.filter_Combo.addItem( "SDSS i'" );
   this.filter_Combo.addItem( "SDSS z'" );
   this.filter_Combo.minWidth = parent.editWidth;
   if ( this.dialog.engine.filter )
      this.filter_Combo.editText = this.dialog.engine.filter;

   this.filter_Combo.toolTip = "<p>Filter used to acquire the images.</p>";
   this.filter_Combo.onItemSelected = function()
   {
      this.dialog.engine.filter = this.itemText( this.currentItem )
   };
   this.filter_Combo.onEditTextUpdated = function()
   {
      this.dialog.engine.filter = this.editText;
   };

   this.filter_Sizer = new HorizontalSizer;
   this.filter_Sizer.spacing = 4;
   this.filter_Sizer.add( this.filter_Label );
   this.filter_Sizer.add( this.filter_Combo );
   this.filter_Sizer.addStretch();

   //
   this.filterKey_Label = new RadioButton( this );
   this.filterKey_Label.text = "Filter keyword:";
   this.filterKey_Label.checked = !this.dialog.engine.manualFilter;
   this.filterKey_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.filterKey_Label.minWidth = parent.labelWidth;
   this.filterKey_Label.onCheck = function( value )
   {
      this.dialog.engine.manualFilter = false;
      this.parent.EnableFilterControls();
   };

   this.filterKey_Combo = new ComboBox( this );
   this.filterKey_Combo.editEnabled = true;
   this.filterKey_Combo.addItem( "FILTER" );
   this.filterKey_Combo.minWidth = parent.editWidth;
   if ( this.dialog.engine.filterKeyword )
      this.filterKey_Combo.editText = this.dialog.engine.filterKeyword;

   this.filterKey_Combo.toolTip = "<p>FITS keyword that contains the filter used to acquire the images.</p>";
   this.filterKey_Combo.onItemSelected = function()
   {
      this.dialog.engine.filterKeyword = this.itemText( this.currentItem )
   };
   this.filterKey_Combo.onEditTextUpdated = function()
   {
      this.dialog.engine.filterKeyword = this.editText;
   };

   this.filterKey_Sizer = new HorizontalSizer;
   this.filterKey_Sizer.spacing = 4;
   this.filterKey_Sizer.add( this.filterKey_Label );
   this.filterKey_Sizer.add( this.filterKey_Combo );
   this.filterKey_Sizer.addStretch();

   this.filter_Frame = new Frame( this );
   this.filter_Frame.sizer = new VerticalSizer;
   this.filter_Frame.sizer.margin = 0;
   this.filter_Frame.sizer.spacing = 4;
   this.filter_Frame.style = FrameStyle_Flat;
   this.filter_Frame.sizer.add( this.filter_Sizer );
   this.filter_Frame.sizer.add( this.filterKey_Sizer );
   this.filter_Frame.tabControl = this;
   this.filter_Frame.EnableFilterControls = function()
   {
      this.tabControl.filter_Combo.enabled = this.dialog.engine.manualFilter;
      this.tabControl.filterKey_Combo.enabled = !this.dialog.engine.manualFilter;
   };

   this.filter_Frame.EnableFilterControls();

   //
   this.apertureShape_Label = new Label( this );
   this.apertureShape_Label.text = "Aperture shape:";
   this.apertureShape_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.apertureShape_Label.minWidth = parent.labelWidth;

   this.apertureSquare_Radio = new RadioButton( this );
   this.apertureSquare_Radio.text = "Square";
   this.apertureSquare_Radio.checked = this.dialog.engine.apertureShape == 0;
   //this.apertureSquare_Radio.minWidth = parent.labelWidth;
   this.apertureSquare_Radio.toolTip = "<p>The photometry is computed using an square window.<br/>The aperture size is the side of the square.</p>";
   this.apertureSquare_Radio.onCheck = function( value )
   {
      this.dialog.engine.apertureShape = 0;
   };

   this.apertureCircle_Radio = new RadioButton( this );
   this.apertureCircle_Radio.text = "Circle";
   this.apertureCircle_Radio.checked = this.dialog.engine.apertureShape == 1;
   //this.apertureCircle_Radio.minWidth = parent.labelWidth;
   this.apertureCircle_Radio.toolTip = "<p>The photometry is computed using a circular window.<br/>The aperture size is the diameter of the circle.</p>";
   this.apertureCircle_Radio.onCheck = function( value )
   {
      this.dialog.engine.apertureShape = 1;
   };

   this.apertureShape_Frame = new Frame( this );
   this.apertureShape_Frame.sizer = new HorizontalSizer;
   this.apertureShape_Frame.sizer.margin = 0;
   this.apertureShape_Frame.sizer.spacing = 4;
   this.apertureShape_Frame.style = FrameStyle_Flat;
   this.apertureShape_Frame.sizer.add( this.apertureShape_Label );
   this.apertureShape_Frame.sizer.add( this.apertureSquare_Radio );
   this.apertureShape_Frame.sizer.add( this.apertureCircle_Radio );
   this.apertureShape_Frame.sizer.addStretch();

   //
   this.minimumAperture_Label = new Label( this );
   this.minimumAperture_Label.text = "Minimum aperture:";
   this.minimumAperture_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.minimumAperture_Label.minWidth = parent.labelWidth;

   this.minimumAperture_Edit = new Edit( this );
   if ( this.dialog.engine.minimumAperture != null )
      this.minimumAperture_Edit.text = format( "%g", this.dialog.engine.minimumAperture );
   this.minimumAperture_Edit.toolTip = "<p>Minimum aperture used for computing flux.<br/>" +
      "It is the side of a square or the diameter of a circle centered on the star.</p>";
   this.minimumAperture_Edit.minWidth = parent.editWidth;
   this.minimumAperture_Edit.onTextUpdated = function( value )
   {
      this.dialog.engine.minimumAperture = parseFloat( value );
   };

   //this.minimumApertureUnits_Label = new Label(this);
   //this.minimumApertureUnits_Label.text = "pixels";
   //this.minimumApertureUnits_Label.textAlignment = TextAlign_Left | TextAlign_VertCenter;
   this.minimumApertureUnits_Combo = new ComboBox( this );
   this.minimumApertureUnits_Combo.editEnabled = false;
   this.minimumApertureUnits_Combo.addItem( "pixels" );
   this.minimumApertureUnits_Combo.addItem( "arcseconds" );
   this.minimumApertureUnits_Combo.currentItem = ( this.dialog.engine.apertureUnit != null && this.dialog.engine.apertureUnit != 0 ) ? 1 : 0;
   this.minimumApertureUnits_Combo.onItemSelected = function()
   {
      this.dialog.engine.apertureUnit = this.currentItem;
      this.dialog.SetApertureUnit();
   };

   this.minimumAperture_Sizer = new HorizontalSizer;
   this.minimumAperture_Sizer.spacing = 4;
   this.minimumAperture_Sizer.add( this.minimumAperture_Label );
   this.minimumAperture_Sizer.add( this.minimumAperture_Edit );
   this.minimumAperture_Sizer.add( this.minimumApertureUnits_Combo );
   this.minimumAperture_Sizer.addStretch();

   //
   this.apertureSteps_Label = new Label( this );
   this.apertureSteps_Label.text = "Aperture steps:";
   this.apertureSteps_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.apertureSteps_Label.minWidth = parent.labelWidth;

   this.apertureSteps_Edit = new Edit( this );
   if ( this.dialog.engine.apertureSteps != null )
      this.apertureSteps_Edit.text = format( "%d", this.dialog.engine.apertureSteps );
   this.apertureSteps_Edit.toolTip = "<p>Number of aperture steps used in the photometry calculation.</p>";
   this.apertureSteps_Edit.minWidth = parent.editWidth;
   this.apertureSteps_Edit.onTextUpdated = function( value )
   {
      let steps = parseInt( value );
      if ( steps > 0 )
         this.dialog.engine.apertureSteps = steps;
      else
         (new MessageBox( "The number of steps must be greater than zero.", TITLE, StdIcon_Error, StdButton_Ok )).execute();
   };

   this.apertureSteps_Sizer = new HorizontalSizer;
   this.apertureSteps_Sizer.spacing = 4;
   this.apertureSteps_Sizer.add( this.apertureSteps_Label );
   this.apertureSteps_Sizer.add( this.apertureSteps_Edit );
   this.apertureSteps_Sizer.addStretch();

   //
   this.apertureStepSize_Label = new Label( this );
   this.apertureStepSize_Label.text = "Aperture step size:";
   this.apertureStepSize_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.apertureStepSize_Label.minWidth = parent.labelWidth;

   this.apertureStepSize_Edit = new Edit( this );
   if ( this.dialog.engine.apertureStepSize != null )
      this.apertureStepSize_Edit.text = format( "%g", this.dialog.engine.apertureStepSize );
   this.apertureStepSize_Edit.toolTip = "<p>When doing multiaperture photometry (Aperture steps > 1), this field defines " +
      "the increment of aperture size applied in each step.</p>";
   this.apertureStepSize_Edit.minWidth = parent.editWidth;
   this.apertureStepSize_Edit.onTextUpdated = function( value )
   {
      this.dialog.engine.apertureStepSize = parseFloat( value );
   };

   this.apertureStepSizeUnits_Label = new Label( this );
   this.apertureStepSizeUnits_Label.text = "pixels";
   this.apertureStepSizeUnits_Label.textAlignment = TextAlign_Left | TextAlign_VertCenter;

   this.apertureStepSize_Sizer = new HorizontalSizer;
   this.apertureStepSize_Sizer.spacing = 4;
   this.apertureStepSize_Sizer.add( this.apertureStepSize_Label );
   this.apertureStepSize_Sizer.add( this.apertureStepSize_Edit );
   this.apertureStepSize_Sizer.add( this.apertureStepSizeUnits_Label );
   this.apertureStepSize_Sizer.addStretch();

   //
   this.gain_Label = new Label( this );
   this.gain_Label.text = "CCD gain:";
   this.gain_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.gain_Label.minWidth = parent.labelWidth;

   this.gain_Edit = new Edit( this );
   if ( this.dialog.engine.gain != null )
      this.gain_Edit.text = format( "%.3f", this.dialog.engine.gain );
   this.gain_Edit.minWidth = parent.editWidth;
   this.gain_Edit.toolTip = "<p>Gain of the camera in e-/ADU.</p>";
   this.gain_Edit.onTextUpdated = function( value )
   {
      this.dialog.engine.gain = parseFloat( value );
   };

   this.gainUnits_Label = new Label( this );
   this.gainUnits_Label.text = "e-/ADU";
   this.gainUnits_Label.textAlignment = TextAlign_Left | TextAlign_VertCenter;

   this.gainLoad_Button = new ToolButton( this );
   this.gainLoad_Button.icon = this.scaledResource( ":/icons/window-export.png" );
   this.gainLoad_Button.setScaledFixedSize( 20, 20 );
   this.gainLoad_Button.toolTip = "Get the gain value from the FITS keyword 'EGAIN'";
   this.gainLoad_Button.onMousePress = function()
   {
      let window = null;
      if ( this.dialog.engine.useActive )
         window = ImageWindow.activeWindow;
      else if ( this.dialog.engine.files && this.dialog.engine.files.length > 0 )
         window = ImageWindow.open( this.dialog.engine.files[0] )[0];
      if ( window )
      {
         let gainOk = false;
         let keywords = window.keywords;
         for ( let i = 0; i < keywords.length && !gainOk; ++i )
         {
            let key = keywords[i];
            if ( key && key.name == "EGAIN" )
            {
               this.dialog.engine.gain = parseFloat( key.value );
               this.dialog.flux_Tab.gain_Edit.text = format( "%f", this.dialog.engine.gain );
               gainOk = true;
            }
         }
         if ( !gainOk )
            new MessageBox( "The image has no 'EGAIN' keyword", TITLE, StdIcon_Error, StdButton_Ok ).execute();
         if ( !this.dialog.engine.useActive )
            window.close();
      }
   };

   this.gain_Sizer = new HorizontalSizer;
   this.gain_Sizer.spacing = 4;
   this.gain_Sizer.add( this.gain_Label );
   this.gain_Sizer.add( this.gain_Edit );
   this.gain_Sizer.add( this.gainUnits_Label );
   this.gain_Sizer.add( this.gainLoad_Button );
   this.gain_Sizer.addStretch();

   //
   this.minSNR_Label = new Label( this );
   this.minSNR_Label.text = "SNR threshold:";
   this.minSNR_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.minSNR_Label.minWidth = parent.labelWidth;

   this.minSNR_Edit = new Edit( this );
   if ( this.dialog.engine.minSNR != null )
      this.minSNR_Edit.text = format( "%g", this.dialog.engine.minSNR );
   this.minSNR_Edit.minWidth = parent.editWidth;
   this.minSNR_Edit.toolTip = "<p>Stars with a Signal to Noise Ratio (SNR) less than this threshold will be " +
      "measured but the value 4 (LOWSNR) will be added to the flags.</p>";
   this.minSNR_Edit.onTextUpdated = function( value )
   {
      this.dialog.engine.minSNR = parseFloat( value );
   };

   this.minSNR_Sizer = new HorizontalSizer;
   this.minSNR_Sizer.spacing = 4;
   this.minSNR_Sizer.add( this.minSNR_Label );
   this.minSNR_Sizer.add( this.minSNR_Edit );
   this.minSNR_Sizer.addStretch();

   //
   this.saturation_Label = new Label( this );
   this.saturation_Label.text = "Saturation threshold:";
   this.saturation_Label.textAlignment = TextAlign_Right | TextAlign_VertCenter;
   this.saturation_Label.minWidth = parent.labelWidth;

   this.saturation_Edit = new Edit( this );
   if ( this.dialog.engine.saturationThreshold != null )
      this.saturation_Edit.text = format( "%g", this.dialog.engine.saturationThreshold * 100 );
   this.saturation_Edit.minWidth = parent.editWidth;
   this.saturation_Edit.toolTip = "<p>Stars that have one or more pixels with a value above this threshold will be " +
      "measured but the value 8 (SATURATED) will be added to the flags.</p>";
   this.saturation_Edit.onTextUpdated = function( value )
   {
      this.dialog.engine.saturationThreshold = parseFloat( value ) / 100;
   };

   this.saturationUnits_Label = new Label( this );
   this.saturationUnits_Label.text = "%";
   this.saturationUnits_Label.textAlignment = TextAlign_Left | TextAlign_VertCenter;

   this.saturation_Sizer = new HorizontalSizer;
   this.saturation_Sizer.spacing = 4;
   this.saturation_Sizer.add( this.saturation_Label );
   this.saturation_Sizer.add( this.saturation_Edit );
   this.saturation_Sizer.add( this.saturationUnits_Label );
   this.saturation_Sizer.addStretch();

   /////
   this.photometry_Group = new GroupBox( this );
   this.photometry_Group.title = "Photometry";
   this.photometry_Group.sizer = new VerticalSizer;
   this.photometry_Group.sizer.margin = 8;
   this.photometry_Group.sizer.spacing = 8;
   this.photometry_Group.sizer.add( this.filter_Frame );
   this.photometry_Group.sizer.add( this.apertureShape_Frame );
   this.photometry_Group.sizer.add( this.minimumAperture_Sizer );
   this.photometry_Group.sizer.add( this.apertureSteps_Sizer );
   this.photometry_Group.sizer.add( this.apertureStepSize_Sizer );
   this.photometry_Group.sizer.add( this.gain_Sizer );
   this.photometry_Group.sizer.add( this.minSNR_Sizer );
   this.photometry_Group.sizer.add( this.saturation_Sizer );

   // Global sizer
   this.sizer = new VerticalSizer();
   this.sizer.margin = 8;
   this.sizer.spacing = 6;
   this.sizer.add( this.photometry_Group );
   this.sizer.addStretch();
}

FluxTab.prototype = new Control;

// ----------------------------------------------------------------------------

function BackgroundTab( parent )
{
   this.__base__ = Control;
   this.__base__( parent );

   var EnableBkgControls = function( frame )
   {
      frame.bkgRing_Edit1.enabled = frame.dialog.engine.bkgWindowMode == BKGWINDOW_RING;
      frame.bkgRing_Edit2.enabled = frame.dialog.engine.bkgWindowMode == BKGWINDOW_RING;
      frame.bkgMMTLayers_Combo.enabled = frame.dialog.engine.bkgModel == BKGMODEL_MMT;
      frame.backgroundSigmaLow_Edit.enabled = frame.dialog.engine.bkgModel == BKGMODEL_SOURCE;
      frame.backgroundSigmaHigh_Edit.enabled = frame.dialog.engine.bkgModel == BKGMODEL_SOURCE;
      frame.bkgSourceModel_Radio.enabled = frame.dialog.engine.bkgWindowMode != BKGWINDOW_PHOTOMETRIC;
      frame.bkgPhoto_Radio.enabled = frame.dialog.engine.bkgModel != BKGMODEL_SOURCE;
   };

   //
   this.window_Group = new GroupBox( this );
   this.window_Group.title = "Background Aperture";
   this.window_Group.sizer = new VerticalSizer();
   this.window_Group.sizer.margin = 8;
   this.window_Group.sizer.spacing = 4;

   //
   this.ring_Radio = new RadioButton( this );
   this.ring_Radio.text = "Square ring:";
   this.ring_Radio.checked = this.dialog.engine.bkgWindowMode == BKGWINDOW_RING;
   this.ring_Radio.minWidth = parent.labelWidth;
   this.ring_Radio.toolTip = "<p>The background is extracted from a square ring around each star.</p>";
   this.ring_Radio.onCheck = function( value )
   {
      if ( value )
      {
         this.dialog.engine.bkgWindowMode = BKGWINDOW_RING;
         EnableBkgControls( this.dialog.background_Tab );
      }
   };

   this.bkgRing_Label1 = new Label( this );
   this.bkgRing_Label1.text = "internal=";
   this.bkgRing_Label1.textAlignment = TextAlign_Left | TextAlign_VertCenter;

   this.bkgRing_Edit1 = new Edit( this );
   if ( this.dialog.engine.bkgAperture1 != null )
      this.bkgRing_Edit1.text = format( "%d", this.dialog.engine.bkgAperture1 );
   this.bkgRing_Edit1.setFixedWidth( parent.editWidth2 );
   this.bkgRing_Edit1.toolTip = "<p>Width of the internal hole in the background window.</p>";
   this.bkgRing_Edit1.onTextUpdated = function( value )
   {
      this.dialog.engine.bkgAperture1 = parseInt( value );
   };

   this.bkgRing_Label2 = new Label( this );
   this.bkgRing_Label2.text = "external=";
   this.bkgRing_Label2.textAlignment = TextAlign_Left | TextAlign_VertCenter;

   this.bkgRing_Edit2 = new Edit( this );
   if ( this.dialog.engine.bkgAperture2 != null )
      this.bkgRing_Edit2.text = format( "%d", this.dialog.engine.bkgAperture2 );
   this.bkgRing_Edit2.setFixedWidth( parent.editWidth2 );
   this.bkgRing_Edit2.toolTip = "<p>Width of the background window.</p>";
   this.bkgRing_Edit2.onTextUpdated = function( value )
   {
      this.dialog.engine.bkgAperture2 = parseInt( value );
   };

   this.bkgRingUnits_Label = new Label( this );
   this.bkgRingUnits_Label.text = "pixels";
   this.bkgRingUnits_Label.textAlignment = TextAlign_Left | TextAlign_VertCenter;

   this.bkgRing_Sizer = new HorizontalSizer;
   this.bkgRing_Sizer.spacing = 4;
   this.bkgRing_Sizer.add( this.ring_Radio );
   this.bkgRing_Sizer.add( this.bkgRing_Label1 );
   this.bkgRing_Sizer.add( this.bkgRing_Edit1 );
   this.bkgRing_Sizer.add( this.bkgRing_Label2 );
   this.bkgRing_Sizer.add( this.bkgRing_Edit2 );
   this.bkgRing_Sizer.add( this.bkgRingUnits_Label );
   this.bkgRing_Sizer.addStretch();
   this.window_Group.sizer.add( this.bkgRing_Sizer );

   //
   this.bkgPhoto_Radio = new RadioButton( this );
   this.bkgPhoto_Radio.text = "Photometric aperture";
   this.bkgPhoto_Radio.checked = this.dialog.engine.bkgWindowMode == BKGWINDOW_PHOTOMETRIC;
   this.bkgPhoto_Radio.minWidth = parent.labelWidth;
   this.bkgPhoto_Radio.toolTip = "<p>The background is extracted from the same window used for flux calculation.<br/>" +
      "This option requires an MMT or ABE model.</p>";
   this.bkgPhoto_Radio.onCheck = function( value )
   {
      if ( value )
      {
         this.dialog.engine.bkgWindowMode = BKGWINDOW_PHOTOMETRIC;
         if ( this.dialog.engine.bkgModel == BKGMODEL_SOURCE )
         {
            this.dialog.engine.bkgModel = BKGMODEL_MMT;
            this.dialog.background_Tab.bkgMMTModel_Radio.checked = true;
         }
         EnableBkgControls( this.dialog.background_Tab );
      }
   };
   this.window_Group.sizer.add( this.bkgPhoto_Radio );

   //
   this.bkgModel_Group = new GroupBox( this );
   this.bkgModel_Group.title = "Background Model";
   this.bkgModel_Group.sizer = new VerticalSizer();
   this.bkgModel_Group.sizer.margin = 8;
   this.bkgModel_Group.sizer.spacing = 4;

   //
   this.bkgSourceModel_Radio = new RadioButton( this );
   this.bkgSourceModel_Radio.text = "Source image";
   this.bkgSourceModel_Radio.checked = this.dialog.engine.bkgModel == BKGMODEL_SOURCE;
   this.bkgSourceModel_Radio.minWidth = parent.labelWidth;
   this.bkgSourceModel_Radio.toolTip = "<p>The background is extracted from the same window used for flux calculation.<br/>" +
      "This option cannot be used with the option 'Photometric window'.</p>";
   this.bkgSourceModel_Radio.onCheck = function( value )
   {
      if ( value )
      {
         this.dialog.engine.bkgModel = BKGMODEL_SOURCE;
         if ( this.dialog.engine.bkgWindowMode == BKGWINDOW_PHOTOMETRIC )
         {
            this.dialog.engine.bkgWindowMode = BKGWINDOW_RING;
            this.dialog.background_Tab.ring_Radio.checked = true;
         }
         EnableBkgControls( this.dialog.background_Tab );
      }
   };

   this.sigma1_Label = new Label( this );
   this.sigma1_Label.text = "Sigma low=";
   this.sigma1_Label.textAlignment = TextAlign_Left | TextAlign_VertCenter;

   this.backgroundSigmaLow_Edit = new Edit( this );
   if ( this.dialog.engine.backgroundSigmaLow != null )
      this.backgroundSigmaLow_Edit.text = format( "%g", this.dialog.engine.backgroundSigmaLow );
   this.backgroundSigmaLow_Edit.setFixedWidth( parent.editWidth2 );
   this.backgroundSigmaLow_Edit.toolTip = "<p>Rejection factor in the 'Median Sigma Clipping' process used for computing the background around a star.</p>";
   this.backgroundSigmaLow_Edit.onTextUpdated = function( value )
   {
      this.dialog.engine.backgroundSigmaLow = parseFloat( value );
   };

   this.sigma2_Label = new Label( this );
   this.sigma2_Label.text = "high=";
   this.sigma2_Label.textAlignment = TextAlign_Left | TextAlign_VertCenter;

   this.backgroundSigmaHigh_Edit = new Edit( this );
   if ( this.dialog.engine.backgroundSigmaHigh != null )
      this.backgroundSigmaHigh_Edit.text = format( "%g", this.dialog.engine.backgroundSigmaHigh );
   this.backgroundSigmaHigh_Edit.setFixedWidth( parent.editWidth2 );
   this.backgroundSigmaHigh_Edit.toolTip = "<p>Rejection factor in the 'Median Sigma Clipping' process used for computing the background around a star.</p>";
   this.backgroundSigmaHigh_Edit.onTextUpdated = function( value )
   {
      this.dialog.engine.backgroundSigmaHigh = parseFloat( value );
   };

   this.bkgSourceModel_Sizer = new HorizontalSizer;
   this.bkgSourceModel_Sizer.spacing = 4;
   this.bkgSourceModel_Sizer.add( this.bkgSourceModel_Radio );
   this.bkgSourceModel_Sizer.add( this.sigma1_Label );
   this.bkgSourceModel_Sizer.add( this.backgroundSigmaLow_Edit );
   this.bkgSourceModel_Sizer.add( this.sigma2_Label );
   this.bkgSourceModel_Sizer.add( this.backgroundSigmaHigh_Edit );
   this.bkgSourceModel_Sizer.addStretch();

   this.bkgModel_Group.sizer.add( this.bkgSourceModel_Sizer );

   //
   this.bkgMMTModel_Radio = new RadioButton( this );
   this.bkgMMTModel_Radio.text = "Multiscale Median Transform";
   this.bkgMMTModel_Radio.checked = this.dialog.engine.bkgModel == BKGMODEL_MMT;
   this.bkgMMTModel_Radio.minWidth = parent.labelWidth;
   this.bkgMMTModel_Radio.toolTip = "<p>The background is extracted from a model image generated using the " +
      "Multiscale Median Transform algorithm (MMT). This process removes small-scale layers in order to elminate the stars.</p>";
   this.bkgMMTModel_Radio.onCheck = function( value )
   {
      if ( value )
      {
         this.dialog.engine.bkgModel = BKGMODEL_MMT;
         EnableBkgControls( this.dialog.background_Tab );
      }
   };

   this.bkgMMTLayers_Label = new Label( this );
   this.bkgMMTLayers_Label.text = "Layers to remove:";
   this.bkgMMTLayers_Label.textAlignment = TextAlign_Left | TextAlign_VertCenter;

   this.bkgMMTLayers_Combo = new ComboBox( this );
   this.bkgMMTLayers_Combo.editEnabled = false;
   //this.bkgMMTLayers_Combo.setFixedWidth(this.font.width("8MMM"));
   this.bkgMMTLayers_Combo.toolTip = "<p>Select the number of layers removed to obtain the background.<br/>" +
      "This parameter can be fine tuned using the option <i>Show MultiscaleMedianTransform background</i> " +
      "in the Output tab.</p>";
   for ( let i = 1; i <= 8; ++i )
      this.bkgMMTLayers_Combo.addItem( i.toString() );
   this.bkgMMTLayers_Combo.currentItem = this.dialog.engine.bkgMMTlayers - 1;
   this.bkgMMTLayers_Combo.onItemSelected = function()
   {
      this.dialog.engine.bkgMMTlayers = this.currentItem + 1;
   };

   this.bkgMMT_Sizer = new HorizontalSizer;
   this.bkgMMT_Sizer.spacing = 4;
   this.bkgMMT_Sizer.add( this.bkgMMTModel_Radio );
   this.bkgMMT_Sizer.addSpacing( 6 );
   this.bkgMMT_Sizer.add( this.bkgMMTLayers_Label );
   this.bkgMMT_Sizer.add( this.bkgMMTLayers_Combo );
   this.bkgMMT_Sizer.addStretch();
   this.bkgModel_Group.sizer.add( this.bkgMMT_Sizer );

   //
   this.bkgABEModel_Radio = new RadioButton( this );
   this.bkgABEModel_Radio.text = "Automatic Background Extraction";
   this.bkgABEModel_Radio.checked = this.dialog.engine.bkgModel == BKGMODEL_ABE;
   this.bkgABEModel_Radio.minWidth = parent.labelWidth;
   this.bkgABEModel_Radio.toolTip = "<p>The background is extracted from a model image generated using the " +
      "AutomaticBackgroundExtraction (ABE) process.</p>";
   this.bkgABEModel_Radio.onCheck = function( value )
   {
      if ( value )
      {
         this.dialog.engine.bkgModel = BKGMODEL_ABE;
         EnableBkgControls( this.dialog.background_Tab );
      }
   };
   this.bkgModel_Group.sizer.add( this.bkgABEModel_Radio );

   EnableBkgControls( this );

   // Global sizer
   this.sizer = new VerticalSizer();
   this.sizer.margin = 8;
   this.sizer.spacing = 6;
   this.sizer.add( this.window_Group );
   this.sizer.add( this.bkgModel_Group );
   //this.sizer.add(this.background_Group);
   this.sizer.addStretch();
}

BackgroundTab.prototype = new Control;

// ----------------------------------------------------------------------------

function OutputTab( parent )
{
   this.__base__ = Control;
   this.__base__( parent );

   //
   this.outDir_Label = new Label( this );
   this.outDir_Label.text = "Output Directory:";
   this.outDir_Label.textAlignment = TextAlign_Left | TextAlign_VertCenter;
   //this.outDir_Label.minWidth = parent.labelWidth;

   this.outDir_Edit = new Edit( this );
   if ( this.dialog.engine.outputDir )
      this.outDir_Edit.text = this.dialog.engine.outputDir;
   this.outDir_Edit.setScaledMinWidth( 300 );
   this.outDir_Edit.toolTip = "<p>Path to the directory where the output tables will be written.<br/>" +
      "If it is empty, the tables will be written on the directory of the first image.</p>";
   this.outDir_Edit.onTextUpdated = function( value )
   {
      if ( value.trim().length > 0 )
         this.dialog.engine.outputDir = value.trim();
      else
         this.dialog.engine.outputDir = null;
   };

   this.outDir_Button = new ToolButton( this );
   this.outDir_Button.icon = this.scaledResource( ":/icons/select-file.png" );
   this.outDir_Button.setScaledFixedSize( 20, 20 );
   this.outDir_Button.toolTip = "<p>Select a directory for the output tables.</p>";
   this.outDir_Button.onClick = function()
   {
      let gdd = new GetDirectoryDialog();
      if ( this.dialog.engine.outputDir )
         gdd.initialPath = this.dialog.engine.outputDir;
      gdd.caption = "Select the output directory";
      if ( gdd.execute() )
      {
         this.dialog.engine.outputDir = gdd.directory;
         this.parent.outDir_Edit.text = gdd.directory;
      }
   };

   this.outDir_Sizer = new HorizontalSizer;
   this.outDir_Sizer.spacing = 4;
   this.outDir_Sizer.add( this.outDir_Label );
   this.outDir_Sizer.add( this.outDir_Edit, 100 );
   this.outDir_Sizer.addSpacing( 4 );
   this.outDir_Sizer.add( this.outDir_Button );

   //
   this.foundStars_Check = new CheckBox( this );
   this.foundStars_Check.text = "Generate images with detected stars";
   this.foundStars_Check.toolTip = "<p>Generates images with the detected stars.</p>" +
      "<p>The images are shown on new windows or saved to files, depending on the value of the <i>Save debug images</i> option.</p>";
   this.foundStars_Check.checked = this.dialog.engine.showFoundStars;
   this.foundStars_Check.onCheck = function( checked )
   {
      this.dialog.engine.showFoundStars = checked;
   };

   //
   this.backModel_Check = new CheckBox( this );
   this.backModel_Check.text = "Show background model";
   this.backModel_Check.toolTip = "<p>Generates images with the background model for each target image.</p>" +
      "<p>The images are shown on new windows or saved to files, depending on the value of the <i>Save debug images</i> option.</p>";
   this.backModel_Check.checked = this.dialog.engine.showBackgroundModel;
   this.backModel_Check.onCheck = function( checked )
   {
      this.dialog.engine.showBackgroundModel = checked;
   };

   //
   this.saveDiag_Check = new CheckBox( this );
   this.saveDiag_Check.text = "Save debug images";
   this.saveDiag_Check.toolTip = "<p>Saves the debug images (detected stars and background model).</p>" +
      "<p>The images are saved on the output directory or, if it is not set, on the directories of the images.</p>";
   this.saveDiag_Check.checked = this.dialog.engine.saveDiagnostic;
   this.saveDiag_Check.onCheck = function( checked )
   {
      this.dialog.engine.saveDiagnostic = checked;
   };

   // Debug images group
   this.debug_Group = new GroupBox( this );
   this.debug_Group.title = "Debug Images";
   this.debug_Group.sizer = new VerticalSizer;
   this.debug_Group.sizer.margin = 8;
   this.debug_Group.sizer.spacing = 4;
   this.debug_Group.sizer.add( this.foundStars_Check );
   this.debug_Group.sizer.add( this.backModel_Check );
   this.debug_Group.sizer.add( this.saveDiag_Check );

   //
   this.outPrefix_Label = new Label( this );
   this.outPrefix_Label.text = "Table Prefix:";
   this.outPrefix_Label.textAlignment = TextAlign_Left | TextAlign_VertCenter;

   this.outPrefix_Edit = new Edit( this );
   if ( this.dialog.engine.outputPrefix )
      this.outPrefix_Edit.text = this.dialog.engine.outputPrefix;
   this.outPrefix_Edit.minWidth = 20 * this.font.width( 'M' );
   this.outPrefix_Edit.toolTip = "<p>Prefix used in the names of the output tables.</p>";
   this.outPrefix_Edit.onTextUpdated = function( value )
   {
      if ( value.trim().length > 0 )
         this.dialog.engine.outputPrefix = value.trim();
      else
         this.dialog.engine.outputPrefix = null;
   };

   this.outPrefix_Sizer = new HorizontalSizer;
   this.outPrefix_Sizer.spacing = 4;
   this.outPrefix_Sizer.add( this.outPrefix_Label );
   this.outPrefix_Sizer.add( this.outPrefix_Edit );
   this.outPrefix_Sizer.addStretch();

   //
   this.fileTable_Check = new CheckBox( this );
   this.fileTable_Check.text = "Generate a table for each image with its information";
   this.fileTable_Check.toolTip = "<p>These tables contain all the information available for each image, including catalog data and photometry calculations.</p>";
   this.fileTable_Check.checked = this.dialog.engine.generateFileTable;
   this.fileTable_Check.onCheck = function( checked )
   {
      this.dialog.engine.generateFileTable = checked;
   };

   //
   this.fluxTable_Check = new CheckBox( this );
   this.fluxTable_Check.text = "Generate flux tables";
   this.fluxTable_Check.toolTip = "<p>Each flux table contains a summary of the measured fluxes for all the stars and images, computed for a given aperture.</p>" +
      "<p>These tables will be generated on the output directory.</p>";
   this.fluxTable_Check.checked = this.dialog.engine.generateFluxTable;
   this.fluxTable_Check.onCheck = function( checked )
   {
      this.dialog.engine.generateFluxTable = checked;
   };

   //
   this.psfFluxTable_Check = new CheckBox( this );
   this.psfFluxTable_Check.text = "Generate PSF flux table";
   this.psfFluxTable_Check.toolTip = "<p>The flux table contains a summary of the fluxes of all the stars and images, calculated from " +
      "the PSF fittings of the stars.</p>" +
      "<p>This table will be generated on the output directory.</p>";
   this.psfFluxTable_Check.checked = this.dialog.engine.generatePSFFluxTable;
   this.psfFluxTable_Check.onCheck = function( checked )
   {
      this.dialog.engine.generatePSFFluxTable = checked;
   };

   //
   this.backgTable_Check = new CheckBox( this );
   this.backgTable_Check.text = "Generate background table";
   this.backgTable_Check.toolTip = "<p>The background table contains a summary of the measured backgrounds of all the stars and images.</p>" +
      "<p>This table will be generated on the output directory.</p>";
   this.backgTable_Check.checked = this.dialog.engine.generateBackgTable;
   this.backgTable_Check.onCheck = function( checked )
   {
      this.dialog.engine.generateBackgTable = checked;
   };

   //
   this.snrTable_Check = new CheckBox( this );
   this.snrTable_Check.text = "Generate SNR table";
   this.snrTable_Check.toolTip = "<p>The SNR table contains a summary of the measured SNR (Signal to Noise Ratio) of all the stars and images.</p>" +
      "<p>This table will be generated on the output directory.</p>";
   this.snrTable_Check.checked = this.dialog.engine.generateSNRTable;
   this.snrTable_Check.onCheck = function( checked )
   {
      this.dialog.engine.generateSNRTable = checked;
   };

   //
   this.flagTable_Check = new CheckBox( this );
   this.flagTable_Check.text = "Generate flags table";
   this.flagTable_Check.toolTip = "<p>The flags table contains a summary of the flags of all the stars and images.</p>" +
      "<p>This table will be generated on the output directory.</p>";
   this.flagTable_Check.checked = this.dialog.engine.generateFlagTable;
   this.flagTable_Check.onCheck = function( checked )
   {
      this.dialog.engine.generateFlagTable = checked;
   };

   //
   this.errorLog_Check = new CheckBox( this );
   this.errorLog_Check.text = "Generate errors file";
   this.errorLog_Check.toolTip = "<p>The errors file contains all the errors that occurred during the process. " +
      "It is only written if there is at least one error.</p>";
   this.errorLog_Check.checked = this.dialog.engine.generateErrorLog;
   this.errorLog_Check.onCheck = function( checked )
   {
      this.dialog.engine.generateErrorLog = checked;
   };

   // Photometry tables group
   this.tables_Group = new GroupBox( this );
   this.tables_Group.title = "Photometry Tables";
   this.tables_Group.sizer = new VerticalSizer;
   this.tables_Group.sizer.margin = 8;
   this.tables_Group.sizer.spacing = 4;
   this.tables_Group.sizer.add( this.outPrefix_Sizer );
   this.tables_Group.sizer.add( this.fileTable_Check );
   this.tables_Group.sizer.add( this.fluxTable_Check );
   this.tables_Group.sizer.add( this.psfFluxTable_Check );
   this.tables_Group.sizer.add( this.backgTable_Check );
   this.tables_Group.sizer.add( this.snrTable_Check );
   this.tables_Group.sizer.add( this.flagTable_Check );
   this.tables_Group.sizer.add( this.errorLog_Check );

   // Global sizer
   this.sizer = new VerticalSizer();
   this.sizer.margin = 8;
   this.sizer.spacing = 6;
   this.sizer.add( this.outDir_Sizer );
   this.sizer.add( this.debug_Group );
   this.sizer.add( this.tables_Group );
   this.sizer.addStretch();
}

OutputTab.prototype = new Control;

// ----------------------------------------------------------------------------

function PhotometryDialog( engine )
{
   this.__base__ = Dialog;
   this.__base__();

   this.engine = engine;

   this.labelWidth = this.font.width( "Maximum magnitude:M" );
   this.editWidth = this.font.width( "MMMMMMMMMMM" );
   this.editWidth2 = this.font.width( "M2.5M" );

   this.SetApertureUnit = function()
   {
      let label = ( this.engine.apertureUnit != null && this.engine.apertureUnit != 0 ) ? "arcseconds" : "pixels";
      this.flux_Tab.apertureStepSizeUnits_Label.text = label;
      this.background_Tab.bkgRingUnits_Label.text = label;
   };

   this.helpLabel = new Label( this );
   this.helpLabel.frameStyle = FrameStyle_Box;
   this.helpLabel.minWidth = 45 * this.font.width( 'M' );
   this.helpLabel.margin = 6;
   this.helpLabel.wordWrapping = true;
   this.helpLabel.useRichText = true;
   this.helpLabel.text =
      "<p><b>" + TITLE + " v" + VERSION
      + "</b> &mdash; A script for measuring the flux of known stars in astronomical images.<br/>"
      + "<br/>"
      + "Copyright &copy; 2013-2023 Andr&eacute;s del Pozo / Vicent Peris (OAUV) | &copy; 2019-2023 Juan Conejero (PTeam)</p>";

   this.images_Tab = new ImagesTab( this, engine );
   this.stars_Tab = new StarsTab( this, engine );
   this.flux_Tab = new FluxTab( this, engine );
   this.background_Tab = new BackgroundTab( this, engine );
   this.output_Tab = new OutputTab( this, engine );

   // Tab control
   this.tabs = new TabBox( this );
   this.tabs.addPage( this.images_Tab, "Images" );
   this.tabs.addPage( this.stars_Tab, "Stars" );
   this.tabs.addPage( this.flux_Tab, "Star flux" );
   this.tabs.addPage( this.background_Tab, "Background" );
   this.tabs.addPage( this.output_Tab, "Output" );

   // usual control buttons

   this.newInstanceButton = new ToolButton( this );
   this.newInstanceButton.icon = this.scaledResource( ":/process-interface/new-instance.png" );
   this.newInstanceButton.setScaledFixedSize( 20, 20 );
   this.newInstanceButton.toolTip = "New Instance";
   this.newInstanceButton.onMousePress = function()
   {
      this.hasFocus = true;
      this.dialog.engine.SaveParameters();
      this.pushed = false;
      this.dialog.newInstance();
   };

   this.reset_Button = new ToolButton( this );
   this.reset_Button.icon = this.scaledResource( ":/icons/reload.png" );
   this.reset_Button.setScaledFixedSize( 20, 20 );
   this.reset_Button.toolTip = "<p>Resets all settings to default values.<br/>" +
      "This action closes the dialog, so the script has to be executed again for changes to take effect.</p>";
   this.reset_Button.onClick = function()
   {
      let msg = new MessageBox( "Do you really want to reset all settings to their default values?",
                                TITLE, StdIcon_Warning, StdButton_Yes, StdButton_No );
      if ( msg.execute() == StdButton_Yes )
      {
         Settings.remove( SETTINGS_MODULE );
         this.dialog.resetRequest = true;
         this.dialog.cancel();
      }
   };

   this.help_Button = new ToolButton( this );
   this.help_Button.icon = this.scaledResource( ":/process-interface/browse-documentation.png" );
   this.help_Button.setScaledFixedSize( 20, 20 );
   this.help_Button.toolTip = "<p>Browse Documentation</p>";
   this.help_Button.onClick = function()
   {
      Dialog.browseScriptDocumentation( "AperturePhotometry" );
   };

   this.ok_Button = new PushButton( this );
   this.ok_Button.text = "OK";
   this.ok_Button.icon = this.scaledResource( ":/icons/ok.png" );
   this.ok_Button.onClick = function()
   {
      if ( !this.parent.images_Tab.Validate() )
         return;
      if ( !this.parent.flux_Tab.Validate() )
         return;
      this.dialog.ok();
   };

   this.cancel_Button = new PushButton( this );
   this.cancel_Button.text = "Cancel";
   this.cancel_Button.icon = this.scaledResource( ":/icons/cancel.png" );
   this.cancel_Button.onClick = function()
   {
      this.dialog.cancel();
   };

   this.buttons_Sizer = new HorizontalSizer;
   this.buttons_Sizer.spacing = 8;
   this.buttons_Sizer.add( this.newInstanceButton );
   this.buttons_Sizer.add( this.reset_Button );
   this.buttons_Sizer.add( this.help_Button );
   this.buttons_Sizer.addStretch();
   this.buttons_Sizer.add( this.ok_Button );
   this.buttons_Sizer.add( this.cancel_Button );

   // Global sizer

   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;
   this.sizer.spacing = 8;
   this.sizer.add( this.helpLabel );
   this.sizer.add( this.tabs );
   this.sizer.addSpacing( 8 );
   this.sizer.add( this.buttons_Sizer );

   this.SetApertureUnit();

   this.windowTitle = TITLE;

   this.ensureLayoutUpdated();
   this.adjustToContents();
   this.setFixedSize();
}

PhotometryDialog.prototype = new Dialog;

// ----------------------------------------------------------------------------

/*
 * Estimation of the standard deviation of the noise, assuming a Gaussian
 * noise distribution.
 *
 * - Use MRS noise evaluation when the algorithm converges for 4 >= J >= 2
 *
 * - Use k-sigma noise evaluation when either MRS doesn't converge or the
 *   length of the noise pixels set is below a 1% of the image area.
 *
 * - Automatically iterate to find the highest layer where noise can be
 *   successfully evaluated, in the [1,3] range.
 */
function NoiseEvaluation( img )
{
   let a, n = 4, m = 0.01 * img.selectedRect.area;
   for ( ;; )
   {
      a = img.noiseMRS( n );
      if ( a[1] >= m )
         break;
      if ( --n == 1 )
      {
         //console.writeln( "<end><cbr>** Warning: No convergence in MRS noise evaluation routine - using k-sigma noise estimate." );
         a = img.noiseKSigma();
         break;
      }
   }
   this.sigma = a[0]; // estimated stddev of Gaussian noise
   this.count = a[1]; // number of pixels in the noise pixels set
   this.layers = n; // number of layers used for noise evaluation
}

// ----------------------------------------------------------------------------

function MedianSigmaClipping( values, nsigmaLow, nsigmaHigh )
{
   this.Iteration = function( median, stdDev, sigmaLow, sigmaHigh )
   {
      let rangeMin = median - stdDev * sigmaLow;
      let rangeMax = median + stdDev * sigmaHigh;
      let validValues = [];
      for ( let i = 0; i < values.length; ++i )
         if ( values[i] >= rangeMin && values[i] <= rangeMax )
            validValues.push( values[i] );
      let result;
      if ( validValues.length > 0 )
      {
         result = {};
         result.median = Math.median( validValues );
         result.stdDev = this.StdDev( validValues, result.median );
         result.rejection = ( values.length - validValues.length ) / values.length;
         // console.writeln("NumValid:",validValues.length," median:",result.median," stdDev:",result.stdDev);
      }
      return result;
   };

   this.StdDev = function( values, reference )
   {
      return Math.stdDev( values );
   };

   let state = {};
   state.median = Math.median( values );
   state.stdDev = this.StdDev( values, state.median );
   state.rejection = 0;
   // console.writeln("NumOrg:",values.length," median:",state.median," stdDev:",state.stdDev);
   for ( let it = 0; it < 5; ++it )
   {
      let result = this.Iteration( state.median, state.stdDev, nsigmaLow, nsigmaHigh );
      if ( result == null )
      {
         state.rejection = 1;
         return state;
      }

      if ( state.median == result.median && state.stdDev == result.stdDev )
         return state;
      state = result;
   }

   return state;
}

// ----------------------------------------------------------------------------

function PhotometryEngine( w )
{
   this.__base__ = ObjectWithSettings;
   this.__base__(
      SETTINGS_MODULE,
      "phot",
      new Array(
         [ "apertureShape", DataType_UInt32 ],
         [ "minimumAperture", DataType_Double ],
         [ "apertureSteps", DataType_UInt32 ],
         [ "apertureStepSize", DataType_Double ],
         [ "apertureUnit", DataType_UInt32 ],
         [ "bkgWindowMode", DataType_UInt32 ],
         [ "bkgModel", DataType_UInt32 ],
         [ "backgroundSigmaLow", DataType_Double ],
         [ "backgroundSigmaHigh", DataType_Double ],
         [ "bkgAperture1", DataType_UInt32 ],
         [ "bkgAperture2", DataType_UInt32 ],
         [ "bkgMMTlayers", DataType_UInt32 ],
         [ "minSNR", DataType_Double ],
         [ "showFoundStars", DataType_Boolean ],
         [ "maxMagnitude", DataType_Double ],
         [ "defaultFocal", DataType_Double ],
         [ "gain", DataType_Double ],
         [ "saturationThreshold", DataType_Double ],
         [ "manualFilter", DataType_Boolean ],
         [ "filter", DataType_UCString ],
         [ "filterKeyword", DataType_UCString ],
         [ "margin", DataType_UInt32 ],
         [ "catalogName", DataType_UCString ],
         [ "catalogFilter", DataType_UCString ],
         [ "vizierServer", DataType_UCString ],
         [ "autoMagnitude", DataType_Boolean ],

         [ "useActive", DataType_Boolean ],
         [ "extractMode", DataType_UInt32 ],
         [ "starsReference", DataType_UCString ],
         [ "manualObjects", Ext_DataType_JSON ],
         [ "outputDir", DataType_UCString ],
         [ "outputPrefix", DataType_UCString ],
         [ "generateFileTable", DataType_Boolean ],
         [ "generateFluxTable", DataType_Boolean ],
         [ "generatePSFFluxTable", DataType_Boolean ],
         [ "generateBackgTable", DataType_Boolean ],
         [ "generateSNRTable", DataType_Boolean ],
         [ "generateFlagTable", DataType_Boolean ],
         [ "saveDiagnostic", DataType_Boolean ],
         [ "autoSolve", DataType_Boolean ],
         [ "forceSolve", DataType_Boolean ],
         [ "solverUseImageMetadata", DataType_Boolean ],
         [ "saveSolve", DataType_Boolean ],
         [ "solveSuffix", DataType_UCString ],
         [ "generateErrorLog", DataType_Boolean ],
         [ "showBackgroundModel", DataType_Boolean ],
         [ "files", Ext_DataType_StringArray ]
      )
   );

   //this.backgroundMode = BKGMODE_FIXED;

   this.bkgWindowMode = BKGWINDOW_PHOTOMETRIC;
   this.bkgModel = BKGMODEL_MMT;
   this.minimumAperture = 8;
   this.apertureShape = 0;
   this.apertureSteps = 1;
   this.apertureStepSize = 1;
   this.apertureUnit = 0; // 0-Pixels, 1-ArcSeconds
   this.backgroundSigmaLow = 5;
   this.backgroundSigmaHigh = 2.5;
   this.bkgFlatten = false;
   this.bkgAperture1 = 30;
   this.bkgAperture2 = 60;
   this.bkgMMTlayers = 8;
   this.showBackgroundModel = false;
   this.outputDir = null;
   this.outputPrefix = "Table_";
   this.files = [];

   this.minSNR = 4;
   this.showFoundStars = false;
   this.window = null;
   this.catalogName = "PPMX";
   this.catalogFilter = "Rmag";
   this.vizierServer = "https://vizier.cds.unistra.fr/";
   this.autoMagnitude = true; // only used with XPSD server catalogs
   this.maxMagnitude = 10;
   this.defaultFocal = 105; // only used when the image is not solved
   this.gain = 1.45;
   this.saturationThreshold = 0.95;
   this.manualFilter = true;
   this.filter = "R";
   this.filterKeyword = "FILTER";
   this.margin = 200;
   this.manualObjects = [];

   this.useActive = true;
   this.extractMode = EXTRACTMODE_IMAGE;
   this.starsReference = null;
   this.generateFileTable = true;
   this.generateFluxTable = true;
   this.generatePSFFluxTable = true;
   this.generateBackgTable = false;
   this.generateSNRTable = false;
   this.generateFlagTable = false;
   this.generateErrorLog = true;
   this.saveDiagnostic = false;
   this.autoSolve = true;
   this.forceSolve = false;
   this.solverUseImageMetadata = true;
   this.saveSolve = false;
   this.solveSuffix = "_WCS";

   this.ConvertADUtoe = function( pixels )
   {
      if ( this.gain == null )
         return;
      for ( let i = 0; i < pixels.length; ++i )
         pixels[i] *= 65535 * this.gain;
   };

   this.FlattenBackground = function( window )
   {
      let ABE = new AutomaticBackgroundExtractor;
      ABE.polyDegree = 5;
      ABE.boxSize = 5;
      ABE.boxSeparation = 5;
      ABE.modelImageSampleFormat = AutomaticBackgroundExtractor.prototype.f32;
      ABE.abeDownsample = 2.00;
      ABE.targetCorrection = AutomaticBackgroundExtractor.prototype.Subtract;
      ABE.normalize = false;
      ABE.discardModel = true;
      ABE.replaceTarget = false;
      ABE.correctedImageId = window.mainView.id + "_Flattened";
      ABE.correctedImageSampleFormat = AutomaticBackgroundExtractor.prototype.SameAsTarget;
      ABE.executeOn( window.mainView, false );
      let flattenedWindow = ImageWindow.windowById( ABE.correctedImageId );
      let autoSTF = new AutoStretch();
      autoSTF.Apply( flattenedWindow.mainView, false );
      flattenedWindow.keywords = window.keywords;
      flattenedWindow.metadataWCS = window.metadataWCS;
      return flattenedWindow;
   };

   this.LoadStars = function( imgMetadata )
   {
      if ( this.autoMagnitude && this.catalog.hasXPSDServer )
      {
         let fov = imgMetadata.resolution * Math.max( imgMetadata.width, imgMetadata.height );
         // Empiric formula for 1000 stars at 20 deg of galactic latitude
         let autoLimitMagnitudeFactor = 14.5;
         let m = autoLimitMagnitudeFactor * Math.pow( fov, -0.179 );
         m = Math.round( 100*Math.min( 20, Math.max( 7, m ) ) )/100;

         let xpsd = this.catalog.newXPSDServer();
         xpsd.command = "get-info";
         xpsd.executeGlobal();

         if ( xpsd.isValid )
         {
            let magHigh = xpsd.databaseMagnitudeHigh;

            console.writeln( "<end><cbr><br>Searching for optimal magnitude limit..." );

            xpsd.command = "search";
            xpsd.centerRA = imgMetadata.ra;
            xpsd.centerDec = imgMetadata.dec;
            xpsd.radius = imgMetadata.resolution * Math.SQRT2 * Math.sqrt( imgMetadata.width * imgMetadata.height )/2;
            xpsd.magnitudeLow = 0;
            xpsd.sourceLimit = 0; // do not retrieve objects, just count them.
            xpsd.verbosity = 0; // work quietly

            for ( let i = 0;; ++i )
            {
               xpsd.magnitudeHigh = m;
               xpsd.executeGlobal();
               console.writeln( format( "<end><cbr>Iteration %d: m=%.2f, %u sources found.", i+1, xpsd.magnitudeHigh, xpsd.excessCount ) );
               let stop = true;
               if ( xpsd.excessCount < 2500 )
               {
                  stop = m > magHigh;
                  if ( !stop )
                     m *= 1.10;
               }
               else if ( xpsd.excessCount > 3000 )
               {
                  stop = m < 7;
                  if ( !stop )
                     m *= 0.95;
               }

               if ( stop )
                  break;
            }

            this.catalog.magMax = m;
            console.noteln( "<end><cbr>* Using an automatically calculated limit magnitude of " + format( "%.2f", m ) + "." );
         }
      }
      else
      {
         this.catalog.magMax = this.maxMagnitude;
         this.catalog.queryMargin = 1.5; // It loads all the stars in an area 50% bigger than the first image
      }

      this.catalog.magMin = 0;

      this.catalog.Load( imgMetadata, this.vizierServer );
      if ( this.catalog.objects == null )
         throw "Catalog error";
      let stars = new Array();
      let margin = Math.max( 0, this.margin - 5 );
      for ( let i = 0; i < this.catalog.objects.length; ++i )
      {
         let star = new StarCatalog();
         star.name = this.catalog.objects[i].name;
         for ( let f = 0; f < this.catalog.filters.length; ++f )
         {
            let filter = this.catalog.filters[f];
            if ( this.catalog.objects[i][filter] )
               star[filter] = parseFloat( this.catalog.objects[i][filter] );
         }
         star.posEq = this.catalog.objects[i].posRD;
         stars.push( star );
      }

      // Add manual objects
      for ( let i = 0; i < this.manualObjects.length; ++i )
      {
         let star = new StarCatalog();
         star.name = this.manualObjects[i].name;
         star.posEq = new Point( this.manualObjects[i].posEq.x, this.manualObjects[i].posEq.y );
         star.manual = true;
         stars.push( star );
      }

      return stars;
   };

   // Get stars
   this.FindStars = function( catalogStars, window )
   {
      let imageStars = [];
      let margin = Math.max( 0, this.margin - 5 );
      let metadata = window.metadataWCS;
      for ( let i = 0; i < catalogStars.length; ++i )
      {
         let posPx = metadata.Convert_RD_I( catalogStars[i].posEq );
         let star = null;
         if ( posPx && posPx.x >= margin && posPx.x <= metadata.width - margin && posPx.y >= margin && posPx.y <= metadata.height - margin )
         {
            star = new StarReference();
            star.orgPosPx = posPx;
         }
         imageStars.push( star );
         // if(stars.length>20) return stars;
      }
      return imageStars;
   };

   this.MultipleLinearRegression = function( coords1, coords2 )
   {
      if ( coords1.length != coords2.length )
         throw "Input arrays of different size in Multiple Linear Regression";

      // Uses independent multiple linear regression for x and y
      // The model is: Y = X * B + err
      // The regresand Y contains the x (or y) of the predicted coordinates coords2
      // The regresors X contains the vectors (x,y,1) with the source coordinates coords1
      // The parameter vector B contains the factors of the expression xc = xi*B0 + yi*B1 + B2
      let Y1 = new Matrix( coords1.length, 1 );
      let Y2 = new Matrix( coords1.length, 1 );
      let X1 = new Matrix( coords1.length, 3 );
      let X2 = new Matrix( coords1.length, 3 );
      for ( let i = 0; i < coords1.length; ++i )
      {
         Y1.at( i, 0, coords2[i].x );
         X1.at( i, 0, coords1[i].x );
         X1.at( i, 1, coords1[i].y );
         X1.at( i, 2, 1 );

         Y2.at( i, 0, coords2[i].y );
         X2.at( i, 0, coords1[i].x );
         X2.at( i, 1, coords1[i].y );
         X2.at( i, 2, 1 );
      }

      // Solve the two multiple regressions
      let X1T = X1.transpose();
      let B1 = ( X1T.mul( X1 ) ).inverse().mul( X1T ).mul( Y1 );

      let X2T = X2.transpose();
      let B2 = ( X2T.mul( X2 ) ).inverse().mul( X2T ).mul( Y2 );

      // Create the correction matrix that transform from image coordinates to catalog coordinates
      let ref_1_2 = new Matrix(
         B1.at( 0, 0 ), B1.at( 1, 0 ), B1.at( 2, 0 ),
         B2.at( 0, 0 ), B2.at( 1, 0 ), B2.at( 2, 0 ),
         0, 0, 1 );
      console.writeln( "Correction matrix:" );
      ref_1_2.Print();
      return ref_1_2;
   };

   this.RegisterStarsAgainstImage = function( imgWindow, refStars, imgStars )
   {
      // Find the position of the stars in imgWindow
      let refStars2 = this.FindStars( this.catalogStars, imgWindow );
      this.FindStarCentersDPSF( imgWindow, refStars2 );
      this.MarkDisplacedStars( refStars2 );

      // Find the transformation between refWindow and imgWindow using a Multiple Linear Regression
      let coordsReference = [];
      let coordsImage = [];
      for ( let i = 0; i < refStars.length; ++i )
      {
         if ( refStars[i] && refStars[i].flags == 0 && refStars2[i] && refStars2[i].flags == 0 )
         {
            coordsReference.push( refStars[i].imgPosPx );
            coordsImage.push( refStars2[i].imgPosPx );
         }
      }
      if ( coordsImage.length == 0 )
         throw "There are not enough valid stars";
      let ref_R_I = this.MultipleLinearRegression( coordsReference, coordsImage );

      // Get the predicted position of the stars in the image
      for ( let i = 0; i < imgStars.length; ++i )
      {
         if ( refStars[i] )
         {
            //console.writeln(refStars[i][refField], " -> ", ref_R_I.Apply(refStars[i][refField]));
            imgStars[i].imgPosPx = ref_R_I.Apply( refStars[i].imgPosPx );
         }
         if ( refStars2[i] )
            imgStars[i].psf = refStars2[i].psf;
      }
   };

   this.GenerateMMTBackgroundModel = function( referenceWindow )
   {
      let imageOrg = referenceWindow.mainView.image;
      let mmtWindow = new ImageWindow( imageOrg.width, imageOrg.height, imageOrg.numberOfChannels,
                                       32, true, imageOrg.isColor, referenceWindow.mainView.id + "_MMTbkg" );

      let mmt = new MultiscaleMedianTransform;
      let layers = [];
      for ( let i = 0; i < this.bkgMMTlayers; ++i )
         layers.push( [ false, true, 0.000, false, 1.0000, 1.00, 0.0000 ] );
      layers.push( [ true, true, 0.000, false, 1.0000, 1.00, 0.0000 ] );
      mmt.layers = layers;

      mmtWindow.mainView.beginProcess( UndoFlag_NoSwapFile );
      mmtWindow.mainView.image.apply( imageOrg );
      mmt.executeOn( mmtWindow.mainView, false );
      mmtWindow.mainView.endProcess();

      if ( this.showBackgroundModel )
      {
         mmtWindow.mainView.stf = referenceWindow.mainView.stf;

         if ( this.saveDiagnostic )
         {
            let outDir = this.GetOutputDir();
            let filename;
            if ( referenceWindow.filePath )
               filename = File.extractName( referenceWindow.filePath ) + "_MMTbkg.fit";
            else
               filename = mmtWindow.mainView.id + ".fit";
            let path = outDir + "/" + filename;
            mmtWindow.saveAs( path, false, false, true, false );
         }
         else
            mmtWindow.show();
      }
      return mmtWindow;
   };

   this.GenerateABEModel = function( window )
   {
      let ABE = new AutomaticBackgroundExtractor;
      ABE.polyDegree = 5;
      ABE.boxSize = 5;
      ABE.boxSeparation = 5;
      ABE.modelImageSampleFormat = AutomaticBackgroundExtractor.prototype.f32;
      ABE.abeDownsample = 1.00;
      ABE.targetCorrection = AutomaticBackgroundExtractor.prototype.None;
      ABE.executeOn( window.mainView, false );

      let abeWindow = ImageWindow.windowById( window.mainView.id + "_ABE_background" );
      if ( this.showBackgroundModel )
      {
         abeWindow.mainView.stf = window.mainView.stf;
         if ( this.saveDiagnostic )
         {
            let outDir = this.GetOutputDir();
            let filename;
            if ( window.filePath )
               filename = File.extractName( window.filePath ) + "_ABE_background.fit";
            else
               filename = abeWindow.mainView.id + ".fit";
            let path = outDir + "/" + filename;
            abeWindow.saveAs( path, false, false, true, false );
         }
         else
            abeWindow.show();
      }
      return abeWindow;
   };

   this.ExtractBackgroundAperture = function( metadata, pixels, aperture1, aperture2, pos )
   {
      let ox = Math.floor( pos.x );
      let oy = Math.floor( pos.y );
      let left = Math.max( ox - aperture2, 0 );
      let right = Math.min( ox + aperture2, metadata.width - 1 );
      let top = Math.max( oy - aperture2, 0 );
      let bottom = Math.min( oy + aperture2, metadata.height - 1 );
      let values = new Array();
      aperture1 = Math.ceil( aperture1 );
      let internalLeft = ox - aperture1;
      let internalRight = ox + aperture1;
      let internalTop = oy - aperture1;
      let internalBottom = oy + aperture1;
      for ( let y = top; y <= bottom; ++y )
      {
         let yOrg = y * metadata.width;
         if ( y <= internalTop || y >= internalBottom )
         {
            for ( let x = left; x <= right; ++x )
               values.push( pixels[x + yOrg] );
         }
         else
         {
            for ( let x = left; x <= internalLeft; ++x )
               values.push( pixels[x + yOrg] );
            for ( let x = internalRight; x <= right; ++x )
               values.push( pixels[ x + yOrg ] );
         }
      }
      return values;
   };

   this.FindMMTBackground = function( window, pixels, pos )
   {
      let metadata = window.metadataWCS;
      let ox = Math.floor( pos.x );
      let oy = Math.floor( pos.y );
      let left = Math.max( ox - this.bkgAperture2, 0 );
      let right = Math.min( ox + this.bkgAperture2, metadata.width - 1 );
      let top = Math.max( oy - this.bkgAperture2, 0 );
      let bottom = Math.min( oy + this.bkgAperture2, metadata.height - 1 );
      let sum = 0;
      let num = 0;
      for ( let y = top; y <= bottom; ++y )
      {
         let yOrg = y * metadata.width;
         for ( let x = left; x <= right; ++x, ++num )
            sum += pixels[ x + yOrg ];
      }
      return { median: (num > 0) ? sum/num : 0,
               stdDev: window.globalBackground.stdDev,
               rejection: 0 };
   };

   this.FindStarCentersDPSF = function( referenceWindow, referenceStars )
   {
      // Search aperture: use a wider aperture for small resolutions. It must be at least 2 pixels.
      let searchAperture = 4/(referenceWindow.metadataWCS.resolution * 3600);
      searchAperture = Math.max( 2, Math.min( 20, searchAperture ) );
      console.writeln( format( "Star search aperture: %.2f pixels", searchAperture ) );

      let DPSF = new DynamicPSF;
      DPSF.views = [
         // id
         [ referenceWindow.mainView.id ]
      ];
      let stars = [];
      let psf = [];
      let translateIdx = {};
      for ( let i = 0; i < referenceStars.length; ++i )
      {
         if ( referenceStars[i] )
         {
            stars.push(
               [ 0, 0, DynamicPSF.prototype.Star_DetectedOk,
                  referenceStars[i].orgPosPx.x - searchAperture, referenceStars[i].orgPosPx.y - searchAperture,
                  referenceStars[i].orgPosPx.x + searchAperture, referenceStars[i].orgPosPx.y + searchAperture,
                  referenceStars[i].orgPosPx.x, referenceStars[i].orgPosPx.y
               ] );
            translateIdx[ psf.length ] = i;
            psf.push(
               [ psf.length, DynamicPSF.prototype.Function_Moffat4, false, DynamicPSF.prototype.PSF_FittedOk,
                  0.022934, 1.190503,
                  referenceStars[i].orgPosPx.x, referenceStars[i].orgPosPx.y,
                  3.174, 3.043, 166.57, 4.00, 3.826e-003
               ] );
         }
      }
      DPSF.stars = stars;
      DPSF.psf = psf;
      DPSF.astrometry = false;
      DPSF.searchRadius = 8;
      DPSF.autoPSF = false;
      DPSF.gaussianPSF = true;
      DPSF.circularPSF = false;
      DPSF.moffatPSF = false;
      DPSF.moffat10PSF = false;
      DPSF.moffat8PSF = false;
      DPSF.moffat6PSF = false;
      DPSF.moffat4PSF = false;
      DPSF.moffat25PSF = false;
      DPSF.moffat15PSF = false;
      DPSF.lorentzianPSF = false;
      DPSF.executeGlobal();
      psf = DPSF.psf;
      for ( let i = 0; i < psf.length; ++i )
      {
         let starIdx = translateIdx[ psf[i][0] ];
         if ( psf[i][3] == DynamicPSF.prototype.PSF_FittedOk )
         {
            let center = new Point( psf[i][6], psf[i][7] );
            referenceStars[starIdx].imgPosPx = center;
            // starIndex, function, circular, status, B, A, cx, cy, sx, sy, theta, beta, mad
            referenceStars[starIdx].psf = {
               sigmaX: psf[i][ 8] * 2.354820045 * referenceWindow.metadataWCS.resolution * 3600,
               sigmaY: psf[i][ 9] * 2.354820045 * referenceWindow.metadataWCS.resolution * 3600,
               A:      psf[i][ 5],
               theta:  psf[i][10],
               MAD:    psf[i][12]
            };
            referenceStars[starIdx].centerDelta = referenceStars[starIdx].orgPosPx.distanceTo( center );
         }
      }

      // Remove stars that could not be fitted
      for ( let i = 0; i < referenceStars.length; ++i )
         if ( referenceStars[i] && !referenceStars[i].imgPosPx )
         {
            //console.writeln(referenceStars[i].orgPosPx);
            referenceStars[i] = null;
         }
   };

   this.MarkDisplacedStars = function( referenceStars )
   {
      let numBadPos = 0;
      for ( let i = 0; i < 5; ++i )
      {
         let deltas = [];
         for ( let s = 0; s < referenceStars.length; ++s )
         {
            if ( referenceStars[s] && referenceStars[s].flags == 0 )
               deltas.push( referenceStars[s].centerDelta );
         }
         if ( deltas.length < 3 )
            return;
         let stdDev = Math.stdDev( deltas );
         let tolerance = Math.max( 1, stdDev * 5 );
         for ( let s = 0; s < referenceStars.length; ++s )
            if ( referenceStars[s] && referenceStars[s].flags == 0 && referenceStars[s].centerDelta > tolerance )
            {
               referenceStars[s].flags |= STARFLAG_BADPOS;
               numBadPos++;
            }
      }
      console.writeln( format( "<end><clrbol>Found %d stars with invalid position", numBadPos ) );
   };

   this.DrawStars = function( window, imageStars, suffix, positionField )
   {
      console.writeln( "Generating detected stars image" );
      let width = window.mainView.image.width;
      let height = window.mainView.image.height;
      if ( width * height * 4 >= 2 * 1024 * 1024 * 1024 )
      {
         console.warningln( "** Warning: Cannot draw the image: The size is too big" );
         return;
      }

      let newid = window.mainView.fullId + suffix;
      let bmp = new Bitmap( width, height );
      let tmpW = new ImageWindow( width, height, 3, window.bitsPerSample, window.isFloatSample, true, newid );
      tmpW.mainView.beginProcess( UndoFlag_NoSwapFile );
      tmpW.mainView.image.selectedChannel = 0;
      tmpW.mainView.image.apply( window.mainView.image );
      tmpW.mainView.image.selectedChannel = 1;
      tmpW.mainView.image.apply( window.mainView.image );
      tmpW.mainView.image.selectedChannel = 2;
      tmpW.mainView.image.apply( window.mainView.image );
      ApplySTF( tmpW.mainView, window.mainView.stf );
      let conv = new SampleFormatConversion();
      conv.format = SampleFormatConversion.prototype.To8Bit;
      conv.executeOn( tmpW.mainView, false );
      tmpW.mainView.endProcess();
      bmp.assign( tmpW.mainView.image.render() );
      tmpW.close();

      let g = new VectorGraphics( bmp );
      g.antialiasing = true;
      g.textAntialiasing = true;
      g.transparentBackground = true;
      let normalPen = new Pen( 0xff00ff00, 1 );
      let multiplePen = new Pen( 0xffff0000, 1 );
      let overlappedPen = new Pen( 0xffffff00, 1 );
      let dimPen = new Pen( 0xff80ffff, 1 );
      let saturatedPen = new Pen( 0xffff8080, 1 );
      let backgroundPen = new Pen( 0xff0000A0, 1 );

      //console.writeln("Rendering");
      let radius = this.minimumAperture * this.apertureUnitFactor / 2 + 0.5;
      let radius2 = ( this.minimumAperture + this.apertureStepSize * ( this.apertureSteps - 1 ) ) * this.apertureUnitFactor / 2 + 0.5;
      for ( let s = 0; s < imageStars.length; ++s )
      {
         if ( imageStars[s] == null )
            continue;
         if ( imageStars[s].flags == null || imageStars[s].flags == 0 )
            g.pen = normalPen;
         else if ( imageStars[s].flags & STARFLAG_MULTIPLE )
            g.pen = multiplePen;
         else if ( ( imageStars[s].flags & STARFLAG_OVERLAPPED ) || ( imageStars[s].flags & STARFLAG_BADPOS ) )
            g.pen = overlappedPen;
         else if ( imageStars[s].flags & STARFLAG_LOWSNR )
            g.pen = dimPen;
         else
            g.pen = saturatedPen;

         let pos = imageStars[s][ positionField ];
         if ( this.apertureShape == 0 )
         {
            g.drawRect( pos.x - radius, pos.y - radius, pos.x + radius, pos.y + radius );
            if ( this.apertureSteps > 1 )
               g.drawRect( pos.x - radius2, pos.y - radius2, pos.x + radius2, pos.y + radius2 );
         }
         else
         {
            g.drawEllipse( pos.x - radius, pos.y - radius, pos.x + radius, pos.y + radius );
            if ( this.apertureSteps > 1 )
               g.drawEllipse( pos.x - radius2, pos.y - radius2, pos.x + radius2, pos.y + radius2 );
         }
         if ( this.catalogStars[s].manual )
         {
            g.drawLine( pos.x - radius2, pos.y, pos.x - radius2 - 6, pos.y );
            g.drawLine( pos.x + radius2, pos.y, pos.x + radius2 + 6, pos.y );
            g.drawLine( pos.x, pos.y - radius2, pos.x, pos.y - radius2 - 6 );
            g.drawLine( pos.x, pos.y + radius2, pos.x, pos.y + radius2 + 6 );
         }
         if ( this.bkgWindowMode == BKGWINDOW_RING )
         {
            g.pen = backgroundPen;
            g.drawRect( pos.x - this.bkgAperture1 / 2, pos.y - this.bkgAperture1 / 2,
               pos.x + this.bkgAperture1 / 2, pos.y + this.bkgAperture1 / 2 );
            g.drawRect( pos.x - this.bkgAperture2 / 2, pos.y - this.bkgAperture2 / 2,
               pos.x + this.bkgAperture2 / 2, pos.y + this.bkgAperture2 / 2 );
         }
      }
      g.end();

      let targetW = new ImageWindow( width, height, 3, 8, false, true, newid );

      targetW.mainView.beginProcess( UndoFlag_NoSwapFile );

      // Blend annotation with target image
      targetW.mainView.image.blend( bmp );

      // Copy keywords to target image
      targetW.keywords = window.keywords;

      targetW.mainView.endProcess();

      if ( this.saveDiagnostic )
      {
         let outDir = this.GetOutputDir();
         let filename = (window.filePath ? File.extractName( window.filePath ) + suffix : newid) + ".xisf";
         let path = outDir + "/" + filename;
         targetW.saveAs( path, false, false, true, false );
         targetW.close();
      }
      else
         targetW.show();
   };

   this.CreateBackgroundMap = function( window, imageStars )
   {
      let dbe = new DynamicBackgroundExtraction;

      let data = new Array();
      for ( let i = 0; i < imageStars.length; ++i )
      {
         if ( imageStars[i] == null )
            continue;
         let sample = new Array();
         sample[0] = imageStars[i].imgPosPx.x / window.metadataWCS.width;
         sample[1] = imageStars[i].imgPosPx.y / window.metadataWCS.height;
         sample[2] = imageStars[i].background.median / (65535 * this.gain);
         sample[3] = 1;
         sample[4] = 0;
         sample[5] = 1;
         sample[6] = 0;
         sample[7] = 1;
         data.push( sample );
      }
      dbe.data = data;

      dbe.numberOfChannels = 1;
      dbe.derivativeOrder = 2;
      dbe.smoothing = 0.001;
      dbe.ignoreWeights = true;
      dbe.modelId = window.mainView.id + "_BackgroundMap";
      // dbe.modelWidth = 0;
      // dbe.modelHeight = 0;
      dbe.downsample = 1;
      dbe.modelSampleFormat = DynamicBackgroundExtraction.prototype.f32;
      dbe.targetCorrection = DynamicBackgroundExtraction.prototype.None;
      dbe.normalize = false;
      dbe.discardModel = false;
      dbe.replaceTarget = false;
      dbe.imageWidth = window.metadataWCS.width;
      dbe.imageHeight = window.metadataWCS.height;
      dbe.symmetryCenterX = 0.5;
      dbe.symmetryCenterY = 0.5;

      dbe.executeOn( window.mainView, false );
      let dbeWindow = ImageWindow.windowById( dbe.modelId );
      dbeWindow.mainView.stf = window.mainView.stf;

      if ( this.saveDiagnostic )
      {
         let outDir = this.GetOutputDir();
         let filename = (window.filePath ? File.extractName( window.filePath ) + "_BackgroundMap" : dbeWindow.mainView.id) + ".xisf";
         let path = outDir + "/" + filename;
         dbeWindow.saveAs( path, false, false, true, false );
         dbeWindow.close();
      }
   };

   this.ValidateStars = function( imageStars )
   {
      let maxAperture = (this.minimumAperture + this.apertureStepSize*(this.apertureSteps - 1)) * this.apertureUnitFactor;
      for ( let i = 0; i < imageStars.length - 1; ++i )
      {
         let star1 = imageStars[i];
         if ( star1 == null )
            continue;

         for ( let j = i + 1; j < imageStars.length; ++j )
         {
            let star2 = imageStars[j];
            if ( star2 == null )
               continue;

            let dist2 = (star1.imgPosPx.x - star2.imgPosPx.x) * (star1.imgPosPx.x - star2.imgPosPx.x)
                      + (star1.imgPosPx.y - star2.imgPosPx.y) * (star1.imgPosPx.y - star2.imgPosPx.y);
            if ( dist2 < 1 )
            { // Two stars have been detected at the same point
               imageStars[i].flags = imageStars[i].flags | STARFLAG_MULTIPLE;
               imageStars[j].flags = imageStars[j].flags | STARFLAG_MULTIPLE;
            }
            else if ( dist2 < maxAperture * maxAperture )
            {
               imageStars[i].flags = imageStars[i].flags | STARFLAG_OVERLAPPED;
               imageStars[j].flags = imageStars[j].flags | STARFLAG_OVERLAPPED;
            }
         }
      }
      let numProblems = imageStars.reduce(
                           function( num, star )
                           {
                              return (star && star.flags != 0) ? num + 1 : num;
                           }, 0 );
      console.writeln( "Stars with problems: ", numProblems );
   };

   this.AperturePhotometry = function( pixels, pixelsBkg, star, intersection, imageWidth )
   {
      let saturation = this.saturationThreshold * 65535 * this.gain;
      let apertureRect = intersection.ApertureRect();
      let left = Math.floor( apertureRect.left );
      let top = Math.floor( apertureRect.top );
      let right = Math.floor( apertureRect.right );
      let bottom = Math.floor( apertureRect.bottom );
      let totalArea = 0;
      let rawFlux = 0;
      let backFlux = 0;
      for ( let y = top; y <= bottom; ++y )
         for ( let x = left; x <= right; ++x )
         {
            let area = intersection.Calculate( new Point( x, y ), 1 );

            let pixelValue = pixels[ x + y * imageWidth ];
            if ( pixelValue > saturation )
               star.flags = star.flags | STARFLAG_SATURATED;
            totalArea += area;
            rawFlux += pixelValue * area;
            //console.writeln(area," ",pixelValue);
            if ( this.bkgWindowMode == BKGWINDOW_PHOTOMETRIC )
               backFlux += pixelsBkg[ x + y * imageWidth ] * area;
         }
      let apertureArea = intersection.ApertureArea();
      // ASSERT: Checks that the square/circle intersection algorithm is OK.
      if ( Math.abs( totalArea - apertureArea ) > 1e-5 )
      {
         console.warningln( "** Warning: Precision problem in the aperture algorithm" );
         console.writeln( totalArea, " ", apertureArea );
         //console.writeln(left," ",top," ",right," ",bottom);
         //console.writeln(star.imgPosPx," ",aperture/2);
      }
      //console.writeln("TOTAL:",totalArea, " FLUX", flux, "\n");
      if ( this.bkgWindowMode == BKGWINDOW_PHOTOMETRIC )
         return { flux:       Math.max( 1e-20, rawFlux - backFlux ),
                  background: backFlux/totalArea };

      return { flux:       Math.max( 1e-20, rawFlux - apertureArea * star.background.median ),
               background: star.background.median };
   };

   this.CalculatePhotometry = function( window, imageStars )
   {
      let pixels = [];
      window.mainView.image.getPixels( pixels );
      this.ConvertADUtoe( pixels );

      console.write( "Calculating image noise: " );
      let noise = new NoiseEvaluation( window.mainView.image );
      window.globalNoise = noise.sigma * 65535 * this.gain;
      console.writeln( format( "sigma=%.3f count=%.2f%% layers=%d", window.globalNoise, noise.count * 100 / pixels.length, noise.layers ) );

      let bkgPixels = null;
      if ( this.bkgModel == BKGMODEL_SOURCE )
      {
         bkgPixels = pixels;
      }
      else
      {
         let bkgWindow;
         if ( this.bkgModel == BKGMODEL_MMT )
            bkgWindow = this.GenerateMMTBackgroundModel( window );
         else if ( this.bkgModel == BKGMODEL_ABE )
            bkgWindow = this.GenerateABEModel( window );
         else
            throw "Unknown background model type";
         bkgPixels = [];
         bkgWindow.mainView.image.getPixels( bkgPixels );
         this.ConvertADUtoe( bkgPixels );
         if ( !this.showBackgroundModel || this.saveDiagnostic )
            bkgWindow.close();
      }

      console.writeln( "Aperture Photometry:" );
      if ( this.apertureSteps > 1 )
      {
         console.writeln( format( "  Minimum aperture: %.2f px", this.minimumAperture * this.apertureUnitFactor ) );
         console.writeln( format( "  Maximum aperture: %.2f px", ( this.minimumAperture + this.apertureStepSize * ( this.apertureSteps - 1 ) ) * this.apertureUnitFactor ) );
      }
      else
         console.writeln( format( "  Aperture: %.2f px", this.minimumAperture * this.apertureUnitFactor ) );
      console.writeln( format( "  Image resolution: %.3f as/px", window.metadataWCS.resolution * 3600 ) );
      if ( this.minimumAperture * this.apertureUnitFactor < 2 )
         console.warningln( "The aperture is less than 2 pixels. The precision of the photometry is going to be low." );
      processEvents();

      let lastProcess = ( new Date ).getTime();
      let startTime = lastProcess;
      for ( let i = 0; i < imageStars.length; ++i )
      {
         let star = imageStars[i];
         if ( star == null )
            continue;

         if ( this.bkgWindowMode == BKGWINDOW_RING )
         {
            let bkgValues = this.ExtractBackgroundAperture( window.metadataWCS, bkgPixels, this.bkgAperture1 / 2, this.bkgAperture2 / 2, star.imgPosPx );
            if ( bkgValues.length == 0 )
            {
               imageStars[i] = null;
               continue;
            }
            if ( this.bkgModel == BKGMODEL_SOURCE )
               star.background = MedianSigmaClipping( bkgValues, this.backgroundSigmaLow, this.backgroundSigmaHigh );
            else
               star.background = { median:    Math.mean( bkgValues ),
                                   stdDev:    window.globalNoise,
                                   rejection: 1 - noise.count/pixels.length };
         }

         star.flux = new Array( this.apertureSteps );
         star.SNR = new Array( this.apertureSteps );
         for ( let a = 0; a < this.apertureSteps; ++a )
         {
            let aperture = ( this.minimumAperture + this.apertureStepSize * a ) * this.apertureUnitFactor;
            let backNoise2;
            let intersection;

            if ( this.apertureShape == 0 )
               intersection = new SquareSquareIntersection( star.imgPosPx, aperture );
            else
               intersection = new CircleSquareIntersection( star.imgPosPx, aperture / 2 );

            let apertureRect = intersection.ApertureRect();
            if ( apertureRect.left < 0 || apertureRect.top < 0 || apertureRect.right >= window.metadataWCS.width || apertureRect.bottom >= window.metadataWCS.height )
            {
               star.flags |= STARFLAG_BADPOS;
               continue;
            }

            let photometry = this.AperturePhotometry( pixels, bkgPixels, star, intersection, window.metadataWCS.width );
            star.flux[a] = photometry.flux;
            if ( this.bkgWindowMode == BKGWINDOW_RING )
               backNoise2 = intersection.ApertureArea() * star.background.stdDev * star.background.stdDev;
            else
            {
               backNoise2 = intersection.ApertureArea() * window.globalNoise * window.globalNoise;
               if ( a == 0 )
                  star.background = {
                     median: photometry.background,
                     stdDev: window.globalNoise,
                     rejection: 1 - noise.count / pixels.length
                  };
            }
            star.SNR[a] = star.flux[a] / Math.sqrt( star.flux[a] + backNoise2 );
            if ( star.SNR[a] < this.minSNR )
               star.flags |= STARFLAG_LOWSNR;
         }

         let time = ( new Date ).getTime();
         if ( time - lastProcess > 1000 )
         {
            lastProcess = time;
            console.write( format( "<end><clrbol>Calculating photometry: Processing stars (%.2f%%)", i * 100 / imageStars.length ) );
            processEvents();
            if ( console.abortRequested )
               throw "Abort!!";
         }
      }
      let endTime = ( new Date ).getTime();
      let numProc = imageStars.reduce(
                        function( num, star )
                        {
                           return (star && star.flux) ? num + 1 : num;
                        }, 0 );
      console.writeln( format( "<end><clrbol>Calculating photometry: Processed %d stars in %.2f seconds", numProc, ( endTime - startTime ) / 1000 ) );
      gc();
   };

   this.FindBounds = function( catalogStars, imageStars )
   {
      let area = null;
      for ( let s = 0; s < imageStars.length; ++s )
      {
         if ( imageStars[s] )
         {
            let posEq = catalogStars[s].posEq;
            if ( area )
               area = area.union( posEq.x, posEq.y, posEq.x, posEq.y );
            else
               area = new Rect( posEq.x, posEq.y, posEq.x, posEq.y );
         }
      }

      return area;
   };

   this.FormatFloat = function( field, number, size, decimals )
   {
      if ( typeof( number ) != "number" )
         console.writeln( "Error writing: ", field, " ", number );
      let str = format( "%" + size + "." + decimals + "f", number );
      if ( str.length > size )
         return str.substr( 0, size );
      return str;
   };

   this.GetWindowFilter = function( window )
   {
      if ( this.manualFilter )
         return this.filter;
      else
      {
         let keywords = window.keywords;
         for ( let i = 0; i < keywords.length; ++i )
         {
            let key = keywords[i];
            if ( key && key.name == this.filterKeyword )
            {
               let value = key.value;
               // Remove the enclosing '' if necessary
               if ( value[0] == "'" && value[ value.length - 1 ] == "'" )
                  value = value.substr( 1, value.length - 2 );
               return value.trim();
            }
         }
         return "UNK";
      }
   };

   this.GetWindowAirMass = function( window )
   {
      let keywords = window.keywords;
      for ( let i = 0; i < keywords.length; ++i )
      {
         let key = keywords[i];
         if ( key && key.name == "AIRMASS" )
         {
            let value = key.value;
            // Remove the enclosing '' if necessary
            if ( value[0] == "'" && value[ value.length - 1 ] == "'" )
               value = value.substr( 1, value.length - 2 );
            return value.trim();
         }
      }
      return "UNK";
   };

   this.WriteResult = function( window, metadataWCS, catalogStars, imageStars )
   {
      let filePath = window.filePath;
      if ( catalogStars.length != imageStars.length )
         throw "Internal error: catalogStars.length != imageStars.length";

      if ( filePath.length == 0 )
      {
         console.writeln( "Output file path empty" );
         return;
      }

      let outPath = null;
      if ( this.outputDir && this.outputDir.length > 0 )
      {
         let filename = File.extractName( filePath );
         outPath = this.outputDir + "/" + filename + ".csv";
      }
      else
         outPath = File.changeExtension( filePath, ".csv" );
      console.writeln( "Writing output file: ", outPath );

      let file = new File;
      let printHeader = true;
      file.createForWriting( outPath );

      // Find filter value
      let filter = this.GetWindowFilter( window );
      let filterColWidth = Math.max( 6, filter.length );

      let separator = ";";
      let warning = false;
      let lineLength = 195 + filterColWidth + 19 * this.apertureSteps;
      if ( printHeader )
      {
         let area = this.FindBounds( catalogStars, imageStars );
         let boundsStr = format( "Bounds;%.8f;%.8f;%.8f;%.8f\n", area.x0, area.y0, area.x1, area.y1 );
         let apertureStr = format( "Aperture;%g;%d;%g\n", this.minimumAperture * this.apertureUnitFactor, this.apertureSteps, this.apertureStepSize * this.apertureUnitFactor );
         let backgroundStr;
         if ( this.bkgWindowMode == BKGWINDOW_RING )
            backgroundStr = format( "Background ring window;%d;%d\n", this.bkgAperture1, this.bkgAperture2 );
         else
            backgroundStr = "Background photometric window\n";

         let airmasStr = format( "Airmass;%s\n", this.GetWindowAirMass( window ) );

         file.outText( format( "%04d;%04d\n", boundsStr.length + apertureStr.length + backgroundStr.length + airmasStr.length + lineLength, lineLength ) );


         let columns = "DATE_OBS     ;NAME                     ;";
         columns += format( "%-" + filterColWidth + "ls", "FILTER" );
         columns += ";CATRA     ;CATDEC    ;";
         columns += "IMGRA     ;IMGDEC    ;";
         columns += "IMGX    ;IMGY    ;";
         //for (let f = 0; f < this.catalog.filters.length; ++f)
         //   columns += format("%-6ls;", this.catalog.filters[f]);
         columns += format( "%-6ls;", this.catalogFilter );
         columns += "BKGROUND;BGSTDDEV;BGRJCT;";
         columns += "PSF_A    ;PSF_SIGMAX;PSF_SIGMAY;PSF_THETA;PSF_MAD     ;"
         for ( let a = 0; a < this.apertureSteps; ++a )
            columns += format( "%-9ls", "FLUX" + format( "%g", ( this.minimumAperture + this.apertureStepSize * a ) * this.apertureUnitFactor ) ) + separator;
         for ( let a = 0; a < this.apertureSteps; ++a )
            columns += format( "%-8ls", "SNR" + format( "%g", ( this.minimumAperture + this.apertureStepSize * a ) * this.apertureUnitFactor ) ) + separator;
         columns += "FLAG\n";

         file.outText( boundsStr );
         file.outText( apertureStr );
         file.outText( backgroundStr );
         file.outText( airmasStr );
         file.outText( columns );
         //console.writeln(lineLength, " ", columns.length);
      }
      for ( let i = 0; i < imageStars.length; ++i )
      {
         if ( imageStars[i] )
         {
            let line = "";
            if ( metadataWCS.epoch )
               line += this.FormatFloat( "DATE_OBS", metadataWCS.epoch, 13, 5 ) + separator;
            else
               line += "             " + separator;

            if ( typeof( catalogStars[i].name ) == "number" )
               line += format( "%-25ls", "_" + catalogStars[i].name.toString() ) + separator;
            else
               line += format( "%-25ls", catalogStars[i].name ) + separator;
            line += format( "%-" + filterColWidth + "ls", filter ) + separator;
            line += this.FormatFloat( "CATRA", catalogStars[i].posEq.x, 10, 6 ) + separator;
            line += this.FormatFloat( "CATDEC", catalogStars[i].posEq.y, 10, 6 ) + separator;
            let imgEq = metadataWCS.Convert_I_RD( imageStars[i].imgPosPx );
            line += this.FormatFloat( "IMGRA", imgEq.x, 10, 6 ) + separator;
            line += this.FormatFloat( "IMGDEC", imgEq.y, 10, 6 ) + separator;
            line += this.FormatFloat( "imgPosPx.x", imageStars[i].imgPosPx.x, 8, 3 ) + separator;
            line += this.FormatFloat( "imgPosPx.y", imageStars[i].imgPosPx.y, 8, 3 ) + separator;

            if ( catalogStars[i][ this.catalogFilter ] )
               line += this.FormatFloat( filter, catalogStars[i][ this.catalogFilter ], 6, 3 ) + separator;
            else
               line += "      " + separator;

            if ( imageStars[i].background )
            {
               line += this.FormatFloat( "median", imageStars[i].background.median, 8, 6 ) + separator;
               line += this.FormatFloat( "stdDev", imageStars[i].background.stdDev, 8, 6 ) + separator;
               line += this.FormatFloat( "rejection", imageStars[i].background.rejection, 6, 4 ) + separator;
            }
            else
               line += "        " + separator + "        " + separator + "      " + separator;

            if ( imageStars[i].psf )
            {
               line += this.FormatFloat( "psf.A", imageStars[i].psf.A, 9, 7 ) + separator;
               line += this.FormatFloat( "psf.sigmaX", imageStars[i].psf.sigmaX, 10, 8 ) + separator;
               line += this.FormatFloat( "psf.sigmaY", imageStars[i].psf.sigmaY, 10, 8 ) + separator;
               line += this.FormatFloat( "psf.theta", imageStars[i].psf.theta, 9, 6 ) + separator;
               //line += this.FormatFloat("psf.MAD", imageStars[i].psf.MAD, 9, 7) + separator;
               line += format( "%.5E", imageStars[i].psf.MAD ) + separator;
            }
            else
               line += "         ;          ;          ;         ;            ;";

            for ( let a = 0; a < this.apertureSteps; ++a )
               if ( imageStars[i].flux[a] )
                  line += this.FormatFloat( "flux" + a, imageStars[i].flux[a], 9, 6 ) + separator;
               else
                  line += separator;
            for ( let a = 0; a < this.apertureSteps; ++a )
               if ( imageStars[i].SNR[a] )
                  line += this.FormatFloat( "SNR" + a, imageStars[i].SNR[a], 8, 5 ) + separator;
               else
                  line += separator;
            line += format( "%04x\n", imageStars[i].flags );

            //            if (line.length != lineLength)
            //               warning = true;

            file.outText( line );
         }
      }
      file.close();
      if ( warning )
         console.writeln( "Invalid out format" );
   };

   this.ValidateWindow = function( window )
   {
      if ( window == null || !window.isWindow )
         throw "The image could not be opened";
      if ( window.mainView.image.numberOfChannels != 1 )
         throw "The image should have only one channel";
   };

   this.CreateImageData = function( window, imageStars )
   {
      let imageData = {};
      imageData.id = window.mainView.id;
      imageData.dateObs = window.metadataWCS.epoch;
      imageData.stars = imageStars;
      imageData.filter = this.GetWindowFilter( window );
      imageData.airmass = this.GetWindowAirMass( window );
      return imageData;
   };

   this.GetOutputDir = function()
   {
      // Get table path
      let outDir = this.outputDir;
      if ( outDir == null || outDir.length == 0 )
      {
         let firstFilePath = this.useActive ? this.currentWindow.filePath : this.files[0];
         if ( firstFilePath == null || firstFilePath.length == 0 )
            throw "The first file has no file path defined";
         outDir = File.extractDrive( firstFilePath ) + File.extractDirectory( firstFilePath );
      }
      return outDir;
   };

   this.WriteTable = function( field, fieldName, snrField )
   {
      // Get table path
      let prefix = this.outputPrefix == null ? "Table_" : this.outputPrefix;
      let outDir = this.GetOutputDir();
      let filename = prefix + fieldName + ".csv";
      let path = outDir + "/" + filename;
      console.writeln( "Writing table: ", path );

      try
      {
         // Create file
         let file = new File;
         file.createForWriting( path );
         let headerprefix = ";;;";

         // Calculate prefix - depends on number of filters
         for ( let f = 0; f < this.catalog.filters.length; ++f )
            headerprefix += ";";
         if ( snrField )
            headerprefix += ";";

         // Write image ID row
         file.outText( headerprefix + "ImageId" );
         for ( let i = 0; i < this.imagesData.length; ++i )
            file.outText( ";" + this.imagesData[i].id );
         file.outText( "\n" );

         // Write image DATE_OBS row
         file.outText( headerprefix + "DATE_OBS" );
         for ( let i = 0; i < this.imagesData.length; ++i )
            if ( this.imagesData[i].dateObs == null )
               file.outText( ";" );
            else
               file.outText( format( ";%.5f", this.imagesData[i].dateObs ) );
         file.outText( "\n" );

         // Write image FILTER row
         file.outText( headerprefix + "FILTER" );
         for ( let i = 0; i < this.imagesData.length; ++i )
            file.outText( format( ";%ls", this.imagesData[i].filter ) );
         file.outText( "\n" );

         // Write image AIRMASS row
         file.outText( headerprefix + "AIRMASS" );
         for ( let i = 0; i < this.imagesData.length; ++i )
            file.outText( format( ";%ls", this.imagesData[i].airmass ) );
         file.outText( "\n" );

         // Write stars header
         file.outText( "StarId;RA;Dec;Flags" );
         for ( let f = 0; f < this.catalog.filters.length; ++f )
            file.outText( ";Catalog_" + this.catalog.filters[f] );

         if ( snrField )
            file.outText( ";MinSNR" );

         for ( let i = 0; i < this.imagesData.length; ++i )
            file.outText( format( ";%ls_%d", fieldName, i + 1 ) );
         file.outText( "\n" );

         // Write stars info
         for ( let s = 0; s < this.catalogStars.length; ++s )
         {
            if ( this.catalogStars[s] )
            {
               // Check for numeric id
               if ( typeof( this.catalogStars[s].name ) == "number" )
                  file.outText( "_" + this.catalogStars[s].name + ";" );
               else if ( this.catalogStars[s].name.length > 0 )
                  file.outText( this.catalogStars[s].name + ";" );
               else
                  file.outText( "-;" );
               file.outText( format( "%.8f;", this.catalogStars[s].posEq.x ) );
               file.outText( format( "%.8f;", this.catalogStars[s].posEq.y ) );
               let flags = 0;
               let minSNR = null;
               for ( let i = 0; i < this.imagesData.length; ++i )
                  if ( this.imagesData[i].stars[s] )
                  {
                     if ( this.imagesData[i].stars[s].flags )
                        flags = flags | this.imagesData[i].stars[s].flags;
                     if ( snrField )
                     {
                        let snr = snrField( this.imagesData[i].stars[s] );
                        if ( snr !== null )
                           if ( minSNR == null )
                              minSNR = snr;
                           else
                              minSNR = Math.min( minSNR, snr );
                     }
                  }
                  else
                     flags |= STARFLAG_LOWSNR;
               file.outText( format( "%d", flags ) );
               for ( let f = 0; f < this.catalog.filters.length; ++f )
               {
                  let filter = this.catalog.filters[f];
                  if ( this.catalogStars[s][filter] )
                     file.outText( format( ";%.4f", this.catalogStars[s][filter] ) );
                  else
                     file.outText( ";" );
               }

               if ( snrField )
               {
                  file.outText( ";" );
                  if ( minSNR !== null )
                     file.outText( format( "%.6f", minSNR ) );
               }
               for ( let i = 0; i < this.imagesData.length; ++i )
               {
                  file.outText( ";" );
                  if ( this.imagesData[i].stars[s] )
                  {
                     let fieldVal = field( this.imagesData[i].stars[s] );
                     if ( fieldVal !== null )
                        file.outText( format( "%.6f", fieldVal ) );
                  }
               }
               file.outText( "\n" );
            }
         }
         file.close();
      }
      catch ( ex )
      {
         console.flush();
         console.criticalln( "*** Error: Failure to write output table." );
         console.flush();
         this.errorList.push(
         {
            id: filename,
            message: ex
         } );
      }
   };

   this.LoadMetadata = function( window )
   {
      if ( window.metadataWCS == null )
      {
         window.metadataWCS = new ImageMetadata();
         window.metadataWCS.ExtractMetadata( window );

         if ( ( window.metadataWCS.ref_I_G == null && this.autoSolve ) || this.forceSolve )
         {
            if ( window.metadataWCS.ref_I_G == null )
               console.writeln( "<end><cbr><br>* The image has no astrometric solution -> SOLVING" );
            else
               console.writeln( "<end><cbr><br>* Forced image solving" );
            let solver = new ImageSolver();

            solver.Init( window, !this.solverUseImageMetadata );
            solver.solverCfg.showStars = false;
            solver.solverCfg.showDistortion = false;
            solver.solverCfg.generateErrorImg = false;

            if ( solver.SolveImage( window ) )
            {
               // Print result
               console.writeln( "<end><cbr><br>" + "=".repeat( 79 ) );
               console.writeln( window.astrometricSolutionSummary() );

               window.metadataWCS.ExtractMetadata( window );

               if ( this.saveSolve )
               {
                  if ( this.solveSuffix.length == 0 )
                     window.save();
                  else
                  {
                     let newPath = File.extractDrive( window.filePath ) +
                        File.extractDirectory( window.filePath ) + "/" +
                        File.extractName( window.filePath ) +
                        this.solveSuffix +
                        File.extractCompleteSuffix( window.filePath );
                     window.saveAs( newPath, false, false, true, false );
                  }
               }
            }
            else
            {
               let errMsg = "The image could not be solved. It will be processed using the existing referentiation.";
               console.writeln( "<b>Warning: </b>" + errMsg );
               if ( window.metadataWCS.ref_I_G != null )
                  this.errorList.push(
                  {
                     id: window.filePath,
                     message: errMsg
                  } );
            }

            if ( window.metadataWCS.ref_I_G == null )
               throw "The image has no coordinates";

         }
         if ( window.metadataWCS.ref_I_G == null )
            throw "The image has no coordinates";
      }
   };

   this.WriteErrorList = function( errorList )
   {
      // Get log path
      let prefix = this.outputPrefix == null ? "Table_" : this.outputPrefix;
      let outDir = this.GetOutputDir();
      let path = outDir + "/" + prefix + "Errors.txt";
      console.noteln( "* Writing error log: ", path );

      // Create file
      let file = new File;
      file.createForWriting( path );

      // Write errors
      for ( let i = 0; i < errorList.length; ++i )
         file.outText( errorList[i].id + ": " + errorList[i].message + "\n" );

      file.close();
   };

   this.ComputeAstrometryError = function( window, imageStars )
   {
      let errX = 0;
      let errY = 0;
      let num = 0;
      for ( let i = 0; i < imageStars.length; ++i )
         if ( imageStars[i] && imageStars[i].flags == 0 )
         {
            let imgPosEq = window.metadataWCS.Convert_I_RD( imageStars[i].imgPosPx );
            let eX = ( imgPosEq.x - this.catalogStars[i].posEq.x ) * 3600 * Math.cos( imgPosEq.y );
            let eY = ( imgPosEq.y - this.catalogStars[i].posEq.y ) * 3600;
            errX += eX * eX;
            errY += eY * eY;
            num++;
         }
      if ( num > 0 )
         console.writeln( format( "Astrometric error: RMSx=%.4f\" RMSy=%.4f\" RMS=%.4f\"",
            Math.sqrt( errX ) / num, Math.sqrt( errY ) / num, Math.sqrt( errX + errY ) / num ) );
   };

   this.ProcessWindow = function( window )
   {
      this.LoadMetadata( window );

      // Calculates the conversion between aperture units and pixels
      if ( this.apertureUnit != null && this.apertureUnit != 0 )
         this.apertureUnitFactor = 1 / ( window.metadataWCS.resolution * 3600 );
      else
         this.apertureUnitFactor = 1;

      // Get reference image
      let referenceWindow = null;
      if ( this.extractMode != EXTRACTMODE_REFERENCE )
         referenceWindow = window;
      else
         referenceWindow = ImageWindow.windowById( this.starsReference );
      this.LoadMetadata( referenceWindow );

      console.writeln( "Reference image: " + referenceWindow.mainView.id );

      // Extract stars from reference image
      if ( this.catalogStars == null )
      {
         // Load stars from the catalog
         this.catalog = __catalogRegister__.GetCatalog( this.catalogName );
         if ( this.catalogFilter )
            this.catalog.magnitudeFilter = this.catalogFilter;
         this.catalogStars = this.LoadStars( referenceWindow.metadataWCS );
         if ( this.catalogStars.length == 0 )
            throw "Could not find any stars in the catalog";
         if ( this.catalogFilter )
         {
            let filter = this.catalogFilter;
            // Sort the stars by catalog ascending magnitude
            this.catalogStars.sort(
                     function( s1, s2 )
                     {
                        let v1 = s1[filter] ? s1[filter] : 0;
                        let v2 = s2[filter] ? s2[filter] : 0;
                        return v1 - v2;
                     } );
         }
         console.writeln( "Catalog stars: ", this.catalogStars.length );
      }

      // Find the centroids of the stars in the reference image
      if ( this.referenceStars == null )
      //      if (this.extractMode == EXTRACTMODE_IMAGE ||
      //         (this.extractMode == EXTRACTMODE_REFERENCE && this.referenceStars == null))
      {
         this.referenceStars = this.FindStars( this.catalogStars, referenceWindow );
         if ( this.extractMode == EXTRACTMODE_CATALOG )
         {
            for ( let i = 0; i < this.referenceStars.length; ++i )
               if ( this.referenceStars[i] )
                  this.referenceStars[i].imgPosPx = this.referenceStars[i].orgPosPx;
         }
         else
         {
            this.FindStarCentersDPSF( referenceWindow, this.referenceStars );
            this.MarkDisplacedStars( this.referenceStars );
         }

         let numImageStars = this.referenceStars.reduce(
                                       function( num, star )
                                       {
                                          return star ? num + 1 : num;
                                       }, 0 );
         if ( numImageStars == 0 )
            throw "Could not locate any stars in the image";
         console.writeln( "Reference stars: ", numImageStars );

         // Validate stars
         console.writeln( "Validating reference stars" );
         processEvents();
         this.ValidateStars( this.referenceStars );

         //this.DrawStars(referenceWindow, this.referenceStars, "_CAT", "orgPosPx");
         if ( this.extractMode == EXTRACTMODE_REFERENCE && this.showFoundStars )
            this.DrawStars( referenceWindow, this.referenceStars, "_DetectedStars", "imgPosPx" );
      }

      // Find the centroids of the stars in the target image
      let imageStars = new Array( this.referenceStars.length );
      if ( window.mainView.id == referenceWindow.mainView.id )
      {
         for ( let i = 0; i < imageStars.length; ++i )
            if ( this.referenceStars[i] )
            {
               imageStars[i] = new StarImage();
               imageStars[i].imgPosPx = this.referenceStars[i].imgPosPx;
               //imageStars[i].background = this.referenceStars[i].background;
               imageStars[i].flags = this.referenceStars[i].flags;
               imageStars[i].psf = this.referenceStars[i].psf;
            }
      }
      else
      {
         console.writeln( "Register image against reference" );
         for ( let i = 0; i < imageStars.length; ++i )
            if ( this.referenceStars[i] )
            {
               imageStars[i] = new StarImage();
               imageStars[i].flags = this.referenceStars[i].flags;
            }
         this.RegisterStarsAgainstImage( window, this.referenceStars, imageStars );

         // Validate stars
         console.writeln( "Validating Stars" );
         processEvents();
         this.ValidateStars( imageStars );
      }

      // The reference stars are not longer needed but when extractMode==EXTRACTMODE_REFERENCE
      if ( this.extractMode != EXTRACTMODE_REFERENCE )
         this.referenceStars = null;

      // Calculate photometry
      this.CalculatePhotometry( window, imageStars );

      if ( this.extractMode != EXTRACTMODE_CATALOG )
         this.ComputeAstrometryError( window, imageStars );

      if ( this.showFoundStars )
         this.DrawStars( window, imageStars, "_DetectedStars", "imgPosPx" );
      if ( this.showBackgroundModel && this.bkgModel == BKGMODEL_SOURCE )
         this.CreateBackgroundMap( window, imageStars );

      if ( this.generateFileTable )
      {
         this.WriteResult( window, window.metadataWCS, this.catalogStars, imageStars );
      }

      this.imagesData.push( this.CreateImageData( window, imageStars ) );
   };

   this.CloseTemporalWindows = function()
   {
      if ( this.temporalWindows )
      {
         for ( let i = 0; i < this.temporalWindows.length; ++i )
            if ( this.temporalWindows[i].isWindow )
               this.temporalWindows[i].forceClose();
         this.temporalWindows = [];
      }
   };

   this.RemoveEmptyStars = function()
   {
      for ( let s = 0; s < this.catalogStars.length; ++s )
      {
         if ( this.catalogStars[s] )
         {
            let empty = true;
            for ( let i = 0; empty && i < this.imagesData.length; ++i )
               if ( this.imagesData[i].stars[s] )
                  empty = false;
            if ( empty )
               this.catalogStars[s] = null;
         }
      }
   };

   this.Calculate = function()
   {
      this.imagesData = [];
      this.temporalWindows = [];
      let numImagesOK = 0;
      let numImages;
      this.errorList = [];

      if ( this.useActive )
      {
         try
         {
            numImages = 1;
            this.ProcessWindow( this.currentWindow );
            numImagesOK++;
            ++__PJSR_AdpPhotometry_SuccessCount;
         }
         catch ( ex )
         {
            console.writeln( "*******************************" )
            console.criticalln( "*** Error: Failure to process image " + this.currentWindow.mainView.id + ": " + ex );
            this.errorList.push(
            {
               id: this.currentWindow.mainView.id,
               message: ex
            } );
         }
         this.CloseTemporalWindows();
      }
      else
      {
         if ( this.files.length == 0 )
            throw "There is not any image selected";
         numImages = this.files.length;
         for ( let i = 0; i < this.files.length; ++i )
         {
            try
            {
               console.writeln( "\n\x1b[36m*******************************\x1b[0m" )
               console.writeln( "Processing image ", this.files[i] );
               let fileWindow = ImageWindow.open( this.files[i] )[0];
               this.temporalWindows.push( fileWindow );
               this.ValidateWindow( fileWindow );
               let autoSTF = new AutoStretch();
               autoSTF.Apply( fileWindow.mainView, false );
               this.ProcessWindow( fileWindow );
               fileWindow.forceClose();
               numImagesOK++;
               ++__PJSR_AdpPhotometry_SuccessCount;
            }
            catch ( ex )
            {
               console.criticalln( "*** Error: Failure to process image file <raw>'" + this.files[i] + "'</raw>: " + ex );
               this.errorList.push(
               {
                  id: this.files[i],
                  message: ex
               } );
            }
            this.CloseTemporalWindows();
            gc( true );
         }
      }

      this.imagesData.sort(
         function( i1, i2 )
         {
            if ( i1.dateObs && i2.dateObs )
               return i1.dateObs - i2.dateObs;
            else
               return i1.id < i2.id ? -1 : ( i1.id == i2.id ? 0 : 1 );
         }
      );

      if ( numImagesOK > 0 )
      {
         this.RemoveEmptyStars();

         if ( this.generatePSFFluxTable )
            this.WriteTable(
               function( star )
               {
                  return star.psf.A * star.psf.sigmaX * star.psf.sigmaY;
               },
               "PSF_Flux",
               function( star )
               {
                  return star.SNR[0] ? star.SNR[0] : null;
               }
            );
         if ( this.generateFluxTable )
            for ( let f = 0; f < this.apertureSteps; ++f )
               this.WriteTable(
                  function( star )
                  {
                     return star.flux[f] ? star.flux[f] : null;
                  },
                  format( "Flux_Ap%g", this.minimumAperture + f * this.apertureStepSize ),
                  function( star )
                  {
                     return star.SNR[f] ? star.SNR[f] : null;
                  }
               );
         if ( this.generateBackgTable )
            this.WriteTable( function( star )
            {
               return star.background.median;
            }, "Background" );
         if ( this.generateSNRTable )
            for ( let f = 0; f < this.apertureSteps; ++f )
               this.WriteTable(
                  function( star )
                  {
                     return star.SNR[f] ? star.SNR[f] : null;
                  },
                  format( "SNR_Ap%g", this.minimumAperture + f * this.apertureStepSize ) );
         if ( this.generateFlagTable )
            this.WriteTable( function( star )
            {
               return star.flags;
            }, "Flags" );
      }

      console.writeln( "\n*******************************" )
      console.noteln( "Photometry process finished:" );
      console.writeln( "   <b>" + numImagesOK, " of ", numImages, " images processed successfully.</b>" );
      console.writeln( "   <b>", numImages - numImagesOK, " images with errors.</b>" );
      if ( this.errorList.length > 0 )
         console.criticalln( "   <b>", this.errorList.length, " errors and warnings.</b>" );
      if ( this.generateErrorLog && this.errorList.length > 0 )
         this.WriteErrorList( this.errorList );
   };

   // Select image and get metadata
   this.Init = function( w )
   {
      this.currentWindow = w;

      this.LoadSettings();
      this.LoadParameters();
   };
}

PhotometryEngine.prototype = new ObjectWithSettings;

// ----------------------------------------------------------------------------

function main()
{
   console.abortEnabled = true;

   if ( Parameters.getBoolean( "resetSettingsAndExit" ) )
   {
      Settings.remove( SETTINGS_MODULE );
      return;
   }
   else if ( Parameters.getBoolean( "resetSettings" ) )
   {
      Settings.remove( SETTINGS_MODULE );
   }

   let engine = new PhotometryEngine;
   if ( Parameters.isViewTarget )
   {
      engine.Init( Parameters.targetView.window );

      // When executing on a target, the debug windows are not necessary ?
      //engine.showBackgroundModel = false;
      //engine.showFoundStars = false;
   }
   else
   {
      if ( Parameters.getBoolean( "non_interactive" ) )
         engine.Init( ImageWindow.activeWindow );
      else
      {
         for( ;; )
         {
            engine.Init( ImageWindow.activeWindow );

            let dialog = new PhotometryDialog( engine );
            if ( dialog.execute() )
               break;
            if ( !dialog.resetRequest )
               return;

            engine = new PhotometryEngine();
         }

         engine.SaveSettings();
      }
   }

   engine.Calculate();
}

// Global control variable for PCL invocation.
var __PJSR_AdpPhotometry_SuccessCount = 0;

main();


/*
 *
 * StarTrailsBlending
 *
 * 2021 (C) Enzo De Bernardini
 * PixInsight Resources - pixinsight.com.ar
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT THE AUTHOR
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
 * INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
 * DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * Changelog:
 * 2.0.0  - 2021.03.24 - Full code rewrite, new GUI.
 *
 */

#feature-id    StarTrailsBlending : Utilities > StarTrailsBlending
#feature-info  <p><b>StarTrailsBlending</b></p><p>Blends a sequence of images, generating a final blended image, with the option to save the incremental results.</p><p>2021 - Enzo De Bernardini</p><p>pixinsight.com.ar</p>

#include <pjsr/Sizer.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/TextAlign.jsh>
#include <pjsr/UndoFlag.jsh>

#define TITLE             "StarTrailsBlending"
#define VERSION           "2.0.0"
#define DEFAULT_BASEIMAGE "StarTrailsBlended"

//
// Global variable with script's parameters
//

var scriptParameters = {

   debug              : false,
   inputFiles         : [],
   settingBlendMode   : "Lighten",
   settingRescale     : false,
   settingReverse     : false,
   settingPersistence : 100,
   outputDirectory    : "",
   outputFileType     : "xisf",
   saveIntermediate   : false,
   outputOverWrite    : false,
   numberImages       : true,

   // Save parameters values for script instance icon
   save: function() {
      Parameters.set("debug",              scriptParameters.debug);
      Parameters.set("inputFiles",         scriptParameters.inputFiles.join(",")); // array to string
      Parameters.set("settingBlendMode",   scriptParameters.settingBlendMode);
      Parameters.set("settingRescale",     scriptParameters.settingRescale);
      Parameters.set("settingReverse",     scriptParameters.settingReverse);
      Parameters.set("settingPersistence", scriptParameters.settingPersistence);
      Parameters.set("outputDirectory",    scriptParameters.outputDirectory);
      Parameters.set("outputFileType",     scriptParameters.outputFileType);
      Parameters.set("saveIntermediate",   scriptParameters.saveIntermediate);
      Parameters.set("outputOverWrite",    scriptParameters.outputOverWrite);
      Parameters.set("numberImages",       scriptParameters.numberImages);
   },

   // Load script instance parameters
   load: function() {
      if(Parameters.has("debug"))              { scriptParameters.debug              = Parameters.getBoolean("debug"); }
      if(Parameters.has("inputFiles"))         { scriptParameters.inputFiles         = Parameters.getString("inputFiles").split(","); } // string to array
      if(Parameters.has("settingBlendMode"))   { scriptParameters.settingBlendMode   = Parameters.getString("settingBlendMode"); }
      if(Parameters.has("settingRescale"))     { scriptParameters.settingRescale     = Parameters.getBoolean("settingRescale"); }
      if(Parameters.has("settingReverse"))     { scriptParameters.settingReverse     = Parameters.getBoolean("settingReverse"); }
      if(Parameters.has("settingPersistence")) { scriptParameters.settingPersistence = Parameters.getReal("settingPersistence"); }
      if(Parameters.has("outputDirectory"))    { scriptParameters.outputDirectory    = Parameters.getString("outputDirectory"); }
      if(Parameters.has("outputFileType"))     { scriptParameters.outputFileType     = Parameters.getString("outputFileType"); }
      if(Parameters.has("saveIntermediate"))   { scriptParameters.saveIntermediate   = Parameters.getBoolean("saveIntermediate"); }
      if(Parameters.has("outputOverWrite"))    { scriptParameters.outputOverWrite    = Parameters.getBoolean("outputOverWrite"); }
      if(Parameters.has("numberImages"))       { scriptParameters.numberImages       = Parameters.getBoolean("numberImages"); }
   }
};

//
// Output Filetypes (array)
//

var fileTypes = [
   ['xisf', 1],
   ['fits', 2],
   ['jpg' , 3],
   ['png' , 4],
   ['tiff', 5]
];

//
// Blend Modes (array)
//

var blendModes = [
   ['Lighten', 1],
   ['Screen', 2]
];

//
// DIALOG
//

function mainDialog() {

   // -----------------------------------------------------------------------------------------
   // Adopted pattern to inject all Dialog properties
   this.__base__ = Dialog;
   this.__base__();
   // -----------------------------------------------------------------------------------------

   // Resizable by user? (only set false if scaledMinWidth has a value)
   this.userResizable = false;

   // dialog min width
   this.scaledMinWidth = 700;

   // Window Title
   this.windowTitle = TITLE + " Script";

   // Title
   this.titleLabel              = new Label(this);
   this.titleLabel.frameStyle   = FrameStyle_Sunken;
   this.titleLabel.margin       = 10;
   this.titleLabel.wordWrapping = true;
   this.titleLabel.useRichText  = true;
   this.titleLabel.text = "<p><b>" + TITLE + " " + VERSION + "</b> &mdash; " + "Blending for star-trails images.</p>";


   // Treebox
   // Input images
   this.filesTreeBox = new TreeBox(this);
   this.filesTreeBox.multipleSelection = true;
   this.filesTreeBox.rootDecoration    = false;
   this.filesTreeBox.alternateRowColor = true;
   this.filesTreeBox.setMinSize(500, 200);
   this.filesTreeBox.numberOfColumns   = 1;
   this.filesTreeBox.headerVisible     = false;

   if(scriptParameters.inputFiles.length > 0) {

      this.filesTreeBox.clear();

      for(var i = 0; i < scriptParameters.inputFiles.length; ++i) {

         var node = new TreeBoxNode(this.filesTreeBox);
         node.setText(0, scriptParameters.inputFiles[i]);

      } // for

   } // if


   // BUTTON - Add Files
   this.filesAddButton         = new PushButton(this);
   this.filesAddButton.text    = "Add Images";
   this.filesAddButton.icon    = this.scaledResource( ":/icons/add.png" );
   this.filesAddButton.toolTip = "<p>Add image files to the input images list.</p>";

   this.filesAddButton.onClick = () => {

      var ofd = new OpenFileDialog;

      ofd.multipleSelections = true;
      ofd.caption = "Select Images";
      ofd.loadImageFilters();

      if(ofd.execute()) {

         this.dialog.filesTreeBox.canUpdate = false;

         for(var i = 0; i < ofd.fileNames.length; ++i) {

            var node = new TreeBoxNode(this.dialog.filesTreeBox);
            node.setText(0, ofd.fileNames[i]);
            scriptParameters.inputFiles.push(ofd.fileNames[i]);
         }

         this.dialog.filesTreeBox.canUpdate = true;
      }
   };


   // BUTTON - Clear fILES
   this.filesClearButton         = new PushButton(this);
   this.filesClearButton.text    = "Clear";
   this.filesClearButton.icon    = this.scaledResource( ":/icons/remove.png" );
   this.filesClearButton.toolTip = "<p>Clear the list of input images.</p>";

   this.filesClearButton.onClick = () => {
      this.dialog.filesTreeBox.clear();
      scriptParameters.inputFiles.length = 0;
   };


   // BUTTON - Remove File
   this.filesRemoveButton         = new PushButton(this);
   this.filesRemoveButton.text    = "Remove Selected";
   this.filesRemoveButton.icon    = this.scaledResource( ":/icons/clear-inverted.png" );
   this.filesRemoveButton.toolTip = "<p>Remove all selected images from the input images list.</p>";

   this.filesRemoveButton.onClick = () => {

      scriptParameters.inputFiles.length = 0;

      for(var i = 0; i < this.dialog.filesTreeBox.numberOfChildren; ++i) {

         if(!this.dialog.filesTreeBox.child(i).selected) {
            scriptParameters.inputFiles.push(this.dialog.filesTreeBox.child(i).text(0));
         }

      }

      for(var i = this.dialog.filesTreeBox.numberOfChildren; --i >= 0;) {

         if (this.dialog.filesTreeBox.child(i).selected) {
            this.dialog.filesTreeBox.remove(i);
         } // if
      } // for
   }; // function


   // SIZER
   // Input files related buttons
   this.filesButtonsSizer         = new HorizontalSizer;
   this.filesButtonsSizer.spacing = 5;
   this.filesButtonsSizer.add(this.filesAddButton);
   this.filesButtonsSizer.addStretch();
   this.filesButtonsSizer.add(this.filesClearButton);
   this.filesButtonsSizer.add(this.filesRemoveButton);


   // GROUPBOX
   // Files tree, related buttons
   this.filesGroupBox               = new GroupBox(this);
   this.filesGroupBox.title         = "Input Images";
   this.filesGroupBox.sizer         = new VerticalSizer;
   this.filesGroupBox.sizer.margin  = 10;
   this.filesGroupBox.sizer.spacing = 10;
   this.filesGroupBox.sizer.add(this.filesTreeBox);
   this.filesGroupBox.sizer.add(this.filesButtonsSizer);


   // Label + Combobox
   // Blend Mode
   this.settingBlendModeLabel               = new Label(this);
   this.settingBlendModeLabel.text          = "Blend Mode";
   this.settingBlendModeLabel.textAlignment = TextAlign_Right|TextAlign_VertCenter;

   this.settingBlendModeCombo = new ComboBox(this);

   with(this.settingBlendModeCombo) {

      blendModes.forEach(type => {

         // Fill ComboBox
         addItem(type[0]);

         // Select Item
         if(scriptParameters.settingBlendMode == type[0]) {
            this.settingBlendModeCombo.currentItem = type[1]-1;
         }

      });

      onItemSelected = function(index) {

         if(scriptParameters.debug) {
            console.noteln("Blend Mode: " + blendModes[index][0]);
         }; // if debug

         scriptParameters.settingBlendMode = blendModes[index][0];

      };

   } // with


   // Checkbox
   // Rescale?
   this.settingRescaleCheckBox         = new CheckBox(this);
   this.settingRescaleCheckBox.text    = "Rescale";
   this.settingRescaleCheckBox.checked = scriptParameters.settingRescale;
   this.settingRescaleCheckBox.toolTip = "<p>If checked, PixelMath will rescale every resulting image.</p>";

   this.settingRescaleCheckBox.onClick = (checked) => {

      scriptParameters.settingRescale = checked;

      if(scriptParameters.debug) {
         Console.noteln("Rescale: " + checked);
      }

   };


   // Checkbox
   // Reverse?
   this.settingReverseCheckBox         = new CheckBox(this);
   this.settingReverseCheckBox.text    = "Reverse order";
   this.settingReverseCheckBox.checked = scriptParameters.settingReverse;
   this.settingReverseCheckBox.toolTip = "<p>If checked, images will be processed in reverse order.</p>";

   this.settingReverseCheckBox.onClick = (checked) => {

      scriptParameters.settingReverse = checked;

      if(scriptParameters.debug) {
         Console.noteln("Reverse: " + checked);
      }

   };




   // NumericControl
   // Opacity Slider

   this.settingPersistenceSlider = new NumericControl(this);
   this.settingPersistenceSlider.label.text = "Persistence (%)";
   this.settingPersistenceSlider.setRange(0, 100);
   this.settingPersistenceSlider.setPrecision(0);
   this.settingPersistenceSlider.toolTip = "<p>Persistence of frames prior to the current one (100% for no change)</p>";
   this.settingPersistenceSlider.slider.setRange(0, 100);
   this.settingPersistenceSlider.setValue(scriptParameters.settingPersistence);

   this.settingPersistenceSlider.onValueUpdated = function(value) {

      scriptParameters.settingPersistence = value;

      if(scriptParameters.debug) {
         Console.noteln("Persistence: " + value);
      }

   };


   // SIZER - Horizonal
   // Blend Settings
   this.blendSettingsGroupBoxH         = new HorizontalSizer;
   this.blendSettingsGroupBoxH.margin  = 0;
   this.blendSettingsGroupBoxH.spacing = 10;
   this.blendSettingsGroupBoxH.add(this.settingBlendModeLabel);
   this.blendSettingsGroupBoxH.add(this.settingBlendModeCombo);
   this.blendSettingsGroupBoxH.add(this.settingRescaleCheckBox);
   this.blendSettingsGroupBoxH.addSpacing(10);
   this.blendSettingsGroupBoxH.add(this.settingReverseCheckBox);
   this.blendSettingsGroupBoxH.addSpacing(10);
   this.blendSettingsGroupBoxH.add(this.settingPersistenceSlider);

   // GROUPBOX
   // Blend settings
   this.settingsGroupBox               = new GroupBox(this);
   this.settingsGroupBox.title         = "Blend Settings";
   this.settingsGroupBox.sizer         = new VerticalSizer;
   this.settingsGroupBox.sizer.margin  = 10;
   this.settingsGroupBox.sizer.spacing = 10;
   this.settingsGroupBox.sizer.add(this.blendSettingsGroupBoxH);


   // Edit
   // Output Directory
   this.outputDirEdit          = new Edit(this);
   this.outputDirEdit.readOnly = true;
   this.outputDirEdit.text     = scriptParameters.outputDirectory;
   this.outputDirEdit.toolTip  = "<p>Optional. Output directory for intermediate images. May be used to generate a time-lapse animation.</p>";


   // Label + Button
   // Select Output Directory Label + Button

   this.outputExtLabel                = new Label(this);
   this.outputExtLabel.text           = "Output Direcotry";
   this.outputExtLabel.textAlignment  = TextAlign_Right|TextAlign_VertCenter;

   this.outputDirSelectButton         = new PushButton( this );
   this.outputDirSelectButton.text    = "Select";
   this.outputDirSelectButton.icon    = this.scaledResource( ":/icons/folder.png" );
   this.outputDirSelectButton.toolTip = "<p>Select the output directory.</p>";

   this.outputDirSelectButton.onClick = function() {

      var gdd = new GetDirectoryDialog;
      gdd.caption = "Select Output Directory for Intermediate Files";

      if(gdd.execute()) {
         scriptParameters.outputDirectory = gdd.directory;
         this.dialog.outputDirEdit.text  = gdd.directory;
      }

   };

   if(!scriptParameters.saveIntermediate) {
      this.outputDirSelectButton.enabled = false;
   }


   // Label + Combobox
   // Output Filetype
   this.outputFileTypeLabel               = new Label(this);
   this.outputFileTypeLabel.text          = "Output Filetype";
   this.outputFileTypeLabel.textAlignment = TextAlign_Right|TextAlign_VertCenter;

   this.fileTypeOutputCombo = new ComboBox(this);

   with(this.fileTypeOutputCombo) {

      fileTypes.forEach(type => {

         // Fill ComboBox
         addItem(type[0]);

         // Select Item
         if(scriptParameters.outputFileType == type[0]) {
            this.fileTypeOutputCombo.currentItem = type[1]-1;
         };

      });

      onItemSelected = function(index) {

         if(scriptParameters.debug) {
            console.noteln("Output selected: " + fileTypes[index][0]);
         }; // if debug

         scriptParameters.outputFileType = fileTypes[index][0];

      };

   } // with

   if(!scriptParameters.saveIntermediate) {
      this.fileTypeOutputCombo.enabled = false;
   }


   // Checkbox
   // Number images?
   this.outputNumberImagesCheckBox         = new CheckBox(this);
   this.outputNumberImagesCheckBox.text    = "Number Images";
   this.outputNumberImagesCheckBox.checked = scriptParameters.numberImages;
   this.outputNumberImagesCheckBox.toolTip = "<p>Prefix all intermediate images with incremental numering (0001, 0002...)</p>";

   this.outputNumberImagesCheckBox.onClick = (checked) => {

      scriptParameters.numberImages = checked;

      if(scriptParameters.debug) {
         Console.noteln("Number Images: " + checked);
      }

   };

   if(!scriptParameters.saveIntermediate) {
      this.outputNumberImagesCheckBox.enabled = false;
   }


   // Checkbox
   // Overwrite Warning?
   this.outputOverwriteWarningCheckBox         = new CheckBox(this);
   this.outputOverwriteWarningCheckBox.text    = "Overwrite Warning";
   this.outputOverwriteWarningCheckBox.checked = scriptParameters.outputOverWrite;
   this.outputOverwriteWarningCheckBox.toolTip = "<p>If checked, a warning will appear for each image to be overwritten.</p>";

   this.outputOverwriteWarningCheckBox.onClick = (checked) => {

      scriptParameters.outputOverWrite = checked;

      if(scriptParameters.debug) {
         Console.noteln("Overwrite Warning: " + checked);
      }

   };

   if(!scriptParameters.saveIntermediate) {
      this.outputOverwriteWarningCheckBox.enabled = false;
   }


   // SIZER - Horizonal
   // Output Settings
   this.outputSettingsGroupBoxH         = new HorizontalSizer;
   this.outputSettingsGroupBoxH.margin  = 0;
   this.outputSettingsGroupBoxH.spacing = 10;
   this.outputSettingsGroupBoxH.add(this.outputFileTypeLabel);
   this.outputSettingsGroupBoxH.addSpacing(9); // align with upper input, aka design ; )
   this.outputSettingsGroupBoxH.add(this.fileTypeOutputCombo);
   this.outputSettingsGroupBoxH.add(this.outputOverwriteWarningCheckBox);
   this.outputSettingsGroupBoxH.addSpacing(10);
   this.outputSettingsGroupBoxH.add(this.outputNumberImagesCheckBox);
   this.outputSettingsGroupBoxH.addStretch();


   // GROUPBOX
   // Output Settings
   this.outputDirectoryGroupBoxH         = new HorizontalSizer;
   this.outputDirectoryGroupBoxH.margin  = 0;
   this.outputDirectoryGroupBoxH.spacing = 10;
   this.outputDirectoryGroupBoxH.add(this.outputExtLabel);
   this.outputDirectoryGroupBoxH.add(this.outputDirEdit);
   this.outputDirectoryGroupBoxH.add(this.outputDirSelectButton);


   // GROUPBOX
   // Output Settings Horizontal Organization
   this.outputDir_GroupBox               = new GroupBox(this);
   this.outputDir_GroupBox.title         = "Save Intermediate Images";
   this.outputDir_GroupBox.titleCheckBox = true;
   this.outputDir_GroupBox.checked       = scriptParameters.saveIntermediate;
   this.outputDir_GroupBox.sizer         = new VerticalSizer;
   this.outputDir_GroupBox.sizer.margin  = 10;
   this.outputDir_GroupBox.sizer.spacing = 10;
   this.outputDir_GroupBox.sizer.add(this.outputDirectoryGroupBoxH);
   this.outputDir_GroupBox.sizer.add(this.outputSettingsGroupBoxH);

   this.outputDir_GroupBox.onCheck = (checked) => {

      scriptParameters.saveIntermediate = checked;

      if(checked) {
         this.outputNumberImagesCheckBox.enabled     = true;
         this.fileTypeOutputCombo.enabled            = true;
         this.outputDirSelectButton.enabled          = true;
         this.outputOverwriteWarningCheckBox.enabled = true;
      } else {
         this.outputNumberImagesCheckBox.enabled     = false;
         this.fileTypeOutputCombo.enabled            = false;
         this.outputDirSelectButton.enabled          = false;
         this.outputOverwriteWarningCheckBox.enabled = false;
      }


      if(scriptParameters.debug) {
         Console.noteln("Save Intermediate: " + checked);
      }

   };


   // Button Execute
   this.execButton = new PushButton(this);
   this.execButton.text = "  Run  ";
   this.execButton.icon = this.scaledResource( ":/icons/power.png" );
   this.execButton.onClick = () => {
      this.ok();
   };


   // Button Cancel
   this.cancelButton = new PushButton(this);
   this.cancelButton.text = "  Exit  ";
   this.cancelButton.icon = this.scaledResource( ":/toolbar/file-exit.png" );
   this.cancelButton.onClick = () => {

      var msg = new MessageBox("Exit?", "Confirm Exit", StdIcon_Question, StdButton_Yes, StdButton_No);

      if(msg.execute() == StdButton_Yes) {
         this.cancel();
      }

   };


   // Instance button
   this.newInstanceButton         = new ToolButton(this);
   this.newInstanceButton.icon    = this.scaledResource(":/process-interface/new-instance.png");
   this.newInstanceButton.toolTip = "Create Instance Icon";
   this.newInstanceButton.setScaledFixedSize( 24, 24 );
   this.newInstanceButton.onMousePress = () => {
      // stores the parameters
      scriptParameters.save();
      // create the script instance
      this.newInstance();
   };


   // PixInsight Resources button
   this.PIRButton         = new ToolButton(this);
   this.PIRButton.icon    = this.scaledResource(":/toolbar/resources-pixinsight-resources.png");
   this.PIRButton.toolTip = "PixInsight Resources";
   this.PIRButton.setScaledFixedSize( 24, 24 );
   this.PIRButton.onMousePress = () => {

      Console.show();
      Console.warningln("\n    ____     ____    ____ \n   / __ \\\   /  _/   / __ \\\  \n  / /_/ /   / /    / /_/ /\n / ____/  _/ /    / _, _/ \n/_/      /___/   /_/ |_|  \n")
      Console.noteln("\nPixInsight Resources: https://pixinsight.com.ar");

   };


   //
   // SIZERS
   //

   // HORIZONTAL -- Bottom icons, buttons
   this.execButtonSizer = new HorizontalSizer;
   this.execButtonSizer.margin = 0;
   this.execButtonSizer.add(this.newInstanceButton);
   this.execButtonSizer.addSpacing(10);
   this.execButtonSizer.add(this.PIRButton);
   this.execButtonSizer.addStretch();
   this.execButtonSizer.add(this.execButton);
   this.execButtonSizer.addSpacing(10);
   this.execButtonSizer.add(this.cancelButton);


   // VERTICAL - All sizers organization
   this.sizer = new VerticalSizer;
   this.sizer.margin = 10;
   this.sizer.add(this.titleLabel);
   this.sizer.addSpacing(10);
   this.sizer.add(this.filesGroupBox);
   this.sizer.addSpacing(10);
   this.sizer.add(this.settingsGroupBox);
   this.sizer.addSpacing(10);
   this.sizer.add(this.outputDir_GroupBox);
   this.sizer.addSpacing(10);
   this.sizer.add(this.execButtonSizer);
   this.sizer.addStretch();

}; // mainDialog


mainDialog.prototype = new Dialog;


//
// PROCESS
//

function mainProcess() {

   if(scriptParameters.debug) {
      Console.noteln("mainProcess -- START");
      Console.noteln(JSON.stringify(scriptParameters, null, 4));
   }


   Console.show();


   // Reverse order?
   if(scriptParameters.settingReverse) {
      scriptParameters.inputFiles.reverse();
   } // if reverse order


   //
   // Main Loop
   //

   for(var i = 0; i < scriptParameters.inputFiles.length; ++i) {

      // Show Progress
      Console.noteln("\nImage " + ( i + 1 )  + " of " + scriptParameters.inputFiles.length);

      var currentFilePath = scriptParameters.inputFiles[i];

      var w = ImageWindow.open(currentFilePath);

      if(w.length == 0 && i + 1 < scriptParameters.inputFiles.length) {

         var msg = new MessageBox("<p>Unable to load input image file:</p><p>" + currentFilePath + "</p><p><b>Continue?</b></p>", "Open Error", StdIcon_Error, StdButton_Yes, StdButton_No);

         if(msg.execute() == StdButton_No) {
            break;
         };

      } // if error opening a file


      //
      // Create Base Image from first image
      // All images will be blended over this
      //

      var currentFileName         = File.extractName(scriptParameters.inputFiles[i]);
      var currentImgWindow        = ImageWindow.windowByFilePath(currentFilePath);
      var currentImgMainView      = currentImgWindow.mainView;
      var currentImgMainViewImage = currentImgWindow.mainView.image;

      var currentView   = new View(currentImgMainView);
      var currentViewId = currentView.id;


      if(i == 0) {

         // Base Image, with same properties of first image
         var baseImage     = new ImageWindow(currentImgMainViewImage.width, currentImgMainViewImage.height, currentImgMainViewImage.numberOfChannels, currentImgMainViewImage.bitsPerSample, false, true, DEFAULT_BASEIMAGE);
         var baseImageView = baseImage.mainView;

         var baseView   = new View(baseImageView);
         var baseViewId = baseView.id;

         // Only visible image, for nice dynamic creation effect ; )
         baseImage.visible = true;

      } // if first image


      if(i + 1 <= scriptParameters.inputFiles.length) {

         if(scriptParameters.debug) {
            Console.noteln("URI: " + currentFilePath);
            Console.noteln("ID:  " + currentViewId);
         }


         //
         // Blending
         // Each open image are blended in "lighten" mode: Max(a, b) with last result, on the same target image
         //

         var P = new PixelMath;

         with(P) {

            //
            // Persistence
            //

               var blendPersistence = (scriptParameters.settingPersistence) / 100;


            //
            // Blend Mode
            //

            switch(scriptParameters.settingBlendMode) {

               case "Lighten":
                  var blendexpression = "Max(( IMG_BASE * " + blendPersistence + " ), IMG_CURRENT )";
               break;

               case "Screen":
                  var blendexpression = "~( ~( IMG_BASE * " + blendPersistence + " ) * ~IMG_CURRENT )"
               break;

            }; // switch


            P.expression = "IMG_BASE = " + baseViewId + "; IMG_CURRENT = " + currentViewId + "; " + blendexpression;
            P.expression1 = "";
            P.expression2 = "";
            P.expression3 = "";
            P.useSingleExpression = true;
            P.symbols = "IMG_BASE, IMG_CURRENT";
            P.clearImageCacheAndExit = false;
            P.cacheGeneratedImages = false;
            P.generateOutput = true;
            P.singleThreaded = false;
            P.optimization = true;
            P.use64BitWorkingImage = false;
            P.rescale = scriptParameters.settingRescale;
            P.rescaleLower = 0;
            P.rescaleUpper = 1;
            P.truncate = true; // TRUE?
            P.truncateLower = 0;
            P.truncateUpper = 1;
            P.createNewImage = false;
            P.showNewImage = true;
            P.newImageId = "";
            P.newImageWidth = 0;
            P.newImageHeight = 0;
            P.newImageAlpha = false;
            P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
            P.newImageSampleFormat = PixelMath.prototype.i32;

         }

         baseImageView.beginProcess(UndoFlag_NoSwapFile);

         P.executeOn(baseView, false);

         baseImageView.endProcess();


         //
         // Save intermediate file?
         //

         if(scriptParameters.saveIntermediate && scriptParameters.outputDirectory != "") {

            // Validation: output directory final slash
            if(scriptParameters.outputDirectory.length > 0 && scriptParameters.outputDirectory.charAt(scriptParameters.outputDirectory.length - 1) != "/") {
               scriptParameters.outputDirectory += "/";
            } // if final slash


            //
            // Save current state
            //

            var currentFileNameSave = currentFileName;

            // Numering?
            if(scriptParameters.saveIntermediate) {
               currentFileNameSave = format("%04d", (i + 1)) + "_" + currentFileName;
            }


            // Path and Filename
            var outputFilePathName = scriptParameters.outputDirectory + currentFileNameSave + "." + scriptParameters.outputFileType;

            if(scriptParameters.debug) {
               Console.noteln("Output: " + outputFilePathName);
            };

            // Save
            baseImage.saveAs(outputFilePathName, false, false, false, scriptParameters.outputOverWrite, "");

         } // if save intermediate files


         //
         // Close opened image
         //

         currentImgWindow.forceClose();

      } // if

   } // for

}; // mainProcess


//
// MAIN
//

function main() {

   if(scriptParameters.debug) {
      Console.show();
      Console.noteln("*** DEBUG MODE ON ***");
   } else {
      Console.hide();
   } // if debug

   // is script started from an instance in global context?
   if(Parameters.isGlobalTarget) {
      // load the parameters from the instance
      scriptParameters.load();
   }

   // Create and show the dialog
   let dialog = new mainDialog;

   if(dialog.execute()) {

      if(scriptParameters.debug) {
         Console.noteln("Execute -- OK");
      }

      var runMainProcessFlag = true;

      //
      // Validations
      //

      // Input min image number validation
      if(scriptParameters.inputFiles.length < 2) {

         runMainProcessFlag = false;

         var msg = new MessageBox("This process requires at least 2 images.", "Number of Images", StdIcon_Error, StdButton_Ok);
         msg.execute();
         main();

      }

      // Output Directory Validation
      if(scriptParameters.saveIntermediate && scriptParameters.outputDirectory == "") {

         runMainProcessFlag = false;

         var msg = new MessageBox("Output directory is required if save intermediate images are selected.", "Output Directory", StdIcon_Error, StdButton_Ok);
         msg.execute();
         main();

      }


      //
      // RUN
      //

      if(runMainProcessFlag) {
         mainProcess();
      }


   } else {

      if(scriptParameters.debug) {
         Console.noteln("Execute -- CANCEL");
      }

   } // if execution

} // main


main();

// Resistance is Futile

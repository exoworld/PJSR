// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// VaryParams-tests.js
// ----------------------------------------------------------------------------
//
// This file is part of FITSFileManager script version 1.6
//
// Copyright (c) 2012-2021 Jean-Marc Lugrin.
// Copyright (c) 2003-2021 Pleiades Astrophoto S.L.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (http://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------
"use strict";

#define TITLE     "VaryParams"

#define VERSION "1.5-test"

#include "VaryParams-unit-tests-support.jsh"

//#define DEBUG true


#include "../../../scripts/VaryParams/VaryParams-helpers.jsh"
#include "../../../scripts/VaryParams/VaryParams-engine.jsh"
#include "../../../scripts/VaryParams/VaryParams-gui.jsh"
#include "../../../scripts/VaryParams/VaryParams-driver.jsh"


// ---------------------------------------------------------------------------------------------------------
// Check environment for tests
// ---------------------------------------------------------------------------------------------------------

// Required processes
var vPtest_requiredProcesses = ["anACDNR","aProcessContainer","aStarMask","aSecondStarMask","aNewImage","aDeconvolution","aDBE",
"aPixelMathDiv2","anABEgen1","anABEgen2"];

var vPtest_testProcessForOperateOnSource = "anACDNR";



// Required views
#define VP_TEST_STRETCHED "TestStarFieldStretched_L"
#define VP_TEST_LINEAR "TestStarFieldLinear_L"
var vPtest_requiredViews = [VP_TEST_STRETCHED,VP_TEST_LINEAR];



// ---------------------------------------------------------------------------------------------------------
// Unit tests
// ---------------------------------------------------------------------------------------------------------



var vP_allTests = {


   // ---------------------------------------------------------------------------------------------------------
   // Test of Engine
   // ---------------------------------------------------------------------------------------------------------
   testSetOperateGenerateNewView_base: function () {
      var view = vPtest_defaultStretchedView();
      var t = new Vp_VaryProcessEngine();
      vP_assertNull(t.sourceView);
      t.setOperateGenerateNewView(view);
      vP_assertEquals(view,t.sourceView);
   },
   testSetOperateGenerateNewView_prepareWorkView: function () {
      var view = vPtest_defaultStretchedView();
      var t = new Vp_VaryProcessEngine();
      vP_assertNull(t.workView);
      t.setOperateGenerateNewView(view);
      t.prepareWorkView()
      vP_assertEquals(view,t.workView);
   },
   testSetOperateOnCopyOfSourceView: function () {
      var view = vPtest_defaultStretchedView();
      var t = new Vp_VaryProcessEngine();
      vP_assertNull(t.sourceView);
      t.setOperateOnCopyOfSourceView(view);
      vP_assertEquals(view,t.sourceView);
   },



   // ---------------------------------------------------------------------------------------------------------
   // Test of environment
   // ---------------------------------------------------------------------------------------------------------
   testEnvironment: function () {

      var env = vP_makeEnvironment();
      // Check that we are in the workspace that contains the project
      if (env.initialView===null) {
         throw "Test workspace (testVaryParams) is not current or no image selected";
      }
      vP_assertTrue(env.initialView.fullId === env.getCurrentView().fullId);
      // Check that there is no new view
      var nv = env.getAdditionalMainViews();
      vP_assertEquals(0,nv.length);

      var P001 = new NewImage;
      P001.id = "aNewImage";
      P001.width = 256;
      P001.height = 256;
      P001.numberOfChannels = 3;
      P001.colorSpace = NewImage.prototype.RGB;
      P001.sampleFormat = NewImage.prototype.i8;
      P001.v0 = 0.00000000;
      P001.v1 = 0.26000000;
      P001.v2 = 0.00000000;
      P001.executeGlobal();

      // Check that the views are seen
      nv = env.getAdditionalMainViews();
      vP_assertEquals(1,nv.length);
      vP_assertEquals("aNewImage", nv[0].id);

      vP_assertEquals(1,env.getAdditionalMainViewsExcept([]).length);
      vP_assertEquals(0,env.getAdditionalMainViewsExcept([nv[0].uniqueId]).length);


      nv[0].window.close();
   },

   testEnvironmentDefaultDir: function () {
      var env = vP_makeEnvironment();
      // If this fails, for some reason the system default directory is invalid
      vP_assertTrue(File.directoryExists(env.defaultOutputDirectory));
   },

  testEnvironmentGetOperateOnList: function () {
      var env = vP_makeEnvironment();
      var view = vPtest_defaultLinearView();
      vP_assertEquals(0,view.window.previews.length);
      var rect = view.image.bounds;
      var preview = view.window.createPreview(rect, "previewForTestGetOpLIst");

#ifdef DEBUG
      var p;

      var getOpNames = function(c) {
         switch(c) {
            case VP_OPERATE_UNDEFINED : return "undefined";
            case VP_OPERATE_MODIFYING_SOURCE : return "modify source";
            case VP_OPERATE_PROCESS_GENERATING_NEW_WINDOW : return "generate new window";
            case VP_OPERATE_GLOBALLY : return "operates globally";
         }
         return "*BADOPNAME " + c;
       }

      for (var i=0; i<vPtest_requiredProcesses.length; i++) {
         p = vPtest_getOriginalProcessInstance(vPtest_requiredProcesses[i]);
         Console.writeln("  Process: " + vPtest_requiredProcesses[i]);
         Console.writeln("     null view: " + env.getOperateOnList(p, null).map(getOpNames));
         Console.writeln("     main view: " + env.getOperateOnList(p, view).map(getOpNames));
         Console.writeln("     preview:   " + env.getOperateOnList(p, preview).map(getOpNames));
      }

#endif

      var testProcess = function(name, nullView,mainView,preView) {
         var p = vPtest_getOriginalProcessInstance(name);
         vP_assertArrayEquals(nullView,env.getOperateOnList(p, null));
         vP_assertArrayEquals(mainView,env.getOperateOnList(p, view));
         vP_assertArrayEquals(preView,env.getOperateOnList(p, preview));
      }
      testProcess("anACDNR",[],[VP_OPERATE_MODIFYING_SOURCE],[VP_OPERATE_MODIFYING_SOURCE]);
      testProcess("aProcessContainer",[],[],[]);
      testProcess("aStarMask",[],[VP_OPERATE_PROCESS_GENERATING_NEW_WINDOW],[VP_OPERATE_PROCESS_GENERATING_NEW_WINDOW]);
      testProcess("aSecondStarMask",[],[VP_OPERATE_PROCESS_GENERATING_NEW_WINDOW],[VP_OPERATE_PROCESS_GENERATING_NEW_WINDOW]);
      testProcess("aNewImage",[VP_OPERATE_GLOBALLY],[VP_OPERATE_GLOBALLY],[VP_OPERATE_GLOBALLY]);
      testProcess("aDeconvolution",[],[VP_OPERATE_MODIFYING_SOURCE],[VP_OPERATE_MODIFYING_SOURCE]);
      testProcess("aDBE",[],[VP_OPERATE_PROCESS_GENERATING_NEW_WINDOW],[]);
      testProcess("aPixelMathDiv2",[],[VP_OPERATE_MODIFYING_SOURCE],[VP_OPERATE_MODIFYING_SOURCE]);
      testProcess("anABEgen1",[],[VP_OPERATE_PROCESS_GENERATING_NEW_WINDOW],[VP_OPERATE_PROCESS_GENERATING_NEW_WINDOW]);
      testProcess("anABEgen2",[],[VP_OPERATE_PROCESS_GENERATING_NEW_WINDOW],[VP_OPERATE_PROCESS_GENERATING_NEW_WINDOW]);

      view.window.deletePreviews();

   },

   testEnvironmentGetCurrentView: function () {
      var env = vP_makeEnvironment();
      vPtest_focusOnStretchedWindow();
      var vs = env.getCurrentView();
      vPtest_focusOnLinearWindow();
      var vl = env.getCurrentView();
      vP_assertEquals(vPtest_defaultStretchedView().uniqueId,vs.uniqueId);
      vP_assertEquals(vPtest_defaultLinearView().uniqueId,vl.uniqueId);
   },



   // ---------------------------------------------------------------------------------------------------------
   // Test of process helpers
   // ---------------------------------------------------------------------------------------------------------
   testGetListOfParametersOfAProcess: function() {
      var params =  vP_getListOfParameters("anACDNR");
      vP_assertEquals(48, params.length);
      vP_assertArrayEquals(["applyToLightness",true], params[0]);
      vP_assertArrayEquals(["maskMTF",0.5], params[39]);
   },
   testMakeParameterizedProcessInstance: function () {
      var process = vP_makeParameterizedProcessInstance("anACDNR", "amount", 0.25);
      vP_assertEquals(0.25, process.amount);
   },


   testIsBlank: function() {
      vP_assertTrue(vP_isBlank(null));
      vP_assertTrue(vP_isBlank(""));
      vP_assertTrue(vP_isBlank("    "));
      vP_assertFalse(vP_isBlank("a"));
      vP_assertFalse(vP_isBlank("    \\    "));
   },

   testFindMinColWidth: function () {
      vP_assertEquals(5,vP_findMinColWidth("abcde", [1,2,3], 6));
      vP_assertEquals(5,vP_findMinColWidth("abcde", [1,2,3], 5));
      vP_assertEquals(4,vP_findMinColWidth("abcde", [1,2,3], 4));

      vP_assertEquals(5,vP_findMinColWidth("abc", [1,12345,3], 10));
      vP_assertEquals(4,vP_findMinColWidth("abc", [1,12345,3], 4));
   },





   // ---------------------------------------------------------------------------------------------------------
   // Test of gui parameters and settings
   // ---------------------------------------------------------------------------------------------------------
   testSettings: function () {
      var environment = vP_makeEnvironment();
      var guiParameters = new Vp_GUIParameters(environment);
      // This ensures that there are settings in first execution
      guiParameters.loadSettings();
      guiParameters.saveSettings();
      // Will load and chane on for tests
      guiParameters.loadSettings();
      var originalDeleteOtherPreviews = guiParameters.deleteOtherPreviews;
      // Inverse to check
      guiParameters.deleteOtherPreviews = ! guiParameters.deleteOtherPreviews;
      guiParameters.loadSettings();
      // Reload must restore value
      vP_assertEquals(originalDeleteOtherPreviews, guiParameters.deleteOtherPreviews);
      guiParameters.deleteOtherPreviews = ! guiParameters.deleteOtherPreviews;
      guiParameters.saveSettings();
      guiParameters.deleteOtherPreviews = originalDeleteOtherPreviews;
      guiParameters.loadSettings();
      // After saving, loading should load the inversed value
      vP_assertEquals(!originalDeleteOtherPreviews, guiParameters.deleteOtherPreviews);
      // Restore original value
      guiParameters.deleteOtherPreviews = originalDeleteOtherPreviews;
      guiParameters.saveSettings();
   },





   // ---------------------------------------------------------------------------------------------------------
   // Test of name helpers
   // ---------------------------------------------------------------------------------------------------------
   testFindShortName1: function() {
      vP_assertArrayEquals(["A","B","ACF"], vP_findShortNames(["a","B","abCdeFg"]));
   },
   testFindShortName2: function() {
      vP_assertArrayEquals(["Abc","AD","A"], vP_findShortNames(["a b c","A_bc_De","a_1.3-5"]));
   },
   // Collision, return indexes
   testFindShortName3: function() {
      vP_assertArrayEquals(["1","2"], vP_findShortNames(["abC","AbCd"]));
   },
   testFindShortName4: function() {
      vP_assertArrayEquals(["1","2"], vP_findShortNames([new Object(), new String()]));
   },
   testFindShortName5: function() {
      vP_assertArrayEquals(["1","0_3","_4_","0_0","1_2"], vP_findShortNames([1,1/3,-4.23,1E-5,1235E23]));
   },
   testCheckUniquePrefix1: function() {
      vP_assertTrue(vP_checkUniquePrefix(["ab","c"],1));
   },
   testCheckUniquePrefix2: function() {
      vP_assertTrue(vP_checkUniquePrefix(["ab","c"],2));
   },
   testCheckUniquePrefix3: function() {
      vP_assertTrue(vP_checkUniquePrefix(["ab","ac","def"],2));
   },
   testCheckUniquePrefix4: function() {
      vP_assertTrue(vP_checkUniquePrefix(["ab"],2));
   },
   testCheckUniquePrefix5: function() {
      vP_assertFalse(vP_checkUniquePrefix(["abc","abd"],2));
   },




   // ---------------------------------------------------------------------------------------------------------
   // Test of vP_makeProcessEngine
   // ---------------------------------------------------------------------------------------------------------
   testMakeProcessEngineError: function () {
      vPtest_focusOnStretchedWindow();
      var environment = vP_makeEnvironment();
      var guiParameters = new Vp_GUIParameters(environment);
      var processEngineOrError = vP_makeProcessEngine(environment, guiParameters);
      vP_assertTrue(processEngineOrError.engineCreationError);
      vP_assertEquals("string", typeof processEngineOrError.engineCreationError);
   },

// ---------------------------------------------------------------------------------------------------------
   // Test of complete process
   // ---------------------------------------------------------------------------------------------------------

   // Test process operating on source window
   testACDNR: function () {
      vPtest_focusOnStretchedWindow();
      var environment = vP_makeEnvironment();
      var guiParameters = new Vp_GUIParameters(environment);
      guiParameters.processInstanceName = "anACDNR";
      guiParameters.parameterToVary="amountL";
      // Use two values, will check that we generate 2 windows
      guiParameters.parameterValuesText="0.8,0.9";
      guiParameters.operateOn=VP_OPERATE_MODIFYING_SOURCE;
      guiParameters.resultDestination=VP_RESULT_IN_WINDOWS;
      var processEngineOrError = vP_makeProcessEngine(environment, guiParameters);
      vP_assertEquals("object", typeof processEngineOrError);
      vP_assertFalse(processEngineOrError.engineCreationError);
      vP_assertFalse(vP_executeForAllValues(processEngineOrError));
      var newViews = environment.getAdditionalMainViews();
      vP_assertEquals(2, newViews.length);
      vP_assertEquals(2, processEngineOrError.generatedResults.length);
      for (var i=0; i<newViews.length; i++) {
         vP_assertTrue(newViews[i].isMainView)
         newViews[i].window.forceClose();
      }
   },

   // Test process generating a new window
   testStarMask: function () {
      vPtest_focusOnLinearWindow();
      var environment = vP_makeEnvironment();
      var guiParameters = new Vp_GUIParameters(environment);
      guiParameters.processInstanceName = "aStarMask";
      guiParameters.parameterToVary="waveletlayers";
      // Use three values, will check that we generate 3 windows
      guiParameters.parameterValuesText="3,4,5";
      guiParameters.operateOn=VP_OPERATE_PROCESS_GENERATING_NEW_WINDOW;
      guiParameters.resultDestination=VP_RESULT_IN_WINDOWS;
      var processEngineOrError = vP_makeProcessEngine(environment, guiParameters);
      vP_assertEquals("object", typeof processEngineOrError);
      vP_assertFalse(processEngineOrError.engineCreationError);
      vP_assertFalse(vP_executeForAllValues(processEngineOrError));
      var newViews = environment.getAdditionalMainViews();
      vP_assertEquals(3, newViews.length);
      vP_assertEquals(3, processEngineOrError.generatedResults.length);
      for (var i=0; i<newViews.length; i++) {
         vP_assertTrue(newViews[i].isMainView)
         newViews[i].window.forceClose();
      }
   },
   // Test process global execution (create new window)
   testNewImage: function () {
      // Focus should not matter
      vPtest_focusOnStretchedWindow();
      var environment = vP_makeEnvironment();
      var guiParameters = new Vp_GUIParameters(environment);
      guiParameters.processInstanceName = "aNewImage";
      guiParameters.parameterToVary="v1";
      // Use four values, will check that we generate 4 windows
      guiParameters.parameterValuesText="0.25,0.5,0.75,1.0";
      guiParameters.operateOn=VP_OPERATE_GLOBALLY;
      guiParameters.resultDestination=VP_RESULT_IN_WINDOWS;
      var processEngineOrError = vP_makeProcessEngine(environment, guiParameters);
      vP_assertEquals("object", typeof processEngineOrError);
      vP_assertFalse(processEngineOrError.engineCreationError);
      vP_assertFalse(vP_executeForAllValues(processEngineOrError));
      var newViews = environment.getAdditionalMainViews();
      vP_assertEquals(4, newViews.length);
      vP_assertEquals(4, processEngineOrError.generatedResults.length);
      for (var i=0; i<newViews.length; i++) {
         vP_assertTrue(newViews[i].isMainView)
         newViews[i].window.forceClose();
      }
   },
   // Test process generating a preview from the main view
   testDeconvolution: function () {
      vPtest_focusOnLinearWindow();
      var environment = vP_makeEnvironment();
      var guiParameters = new Vp_GUIParameters(environment);
      var targetView = guiParameters.sourceView;
      vP_assertEquals(0,targetView.window.previews.length);
      guiParameters.processInstanceName = "aDeconvolution";
      guiParameters.parameterToVary="psfSigma";
      // Generate two previews
      guiParameters.parameterValuesText="1.5,2.5";
      guiParameters.operateOn=VP_OPERATE_MODIFYING_SOURCE;
      guiParameters.resultDestination=VP_RESULT_IN_PREVIEWS;
      var processEngineOrError = vP_makeProcessEngine(environment, guiParameters);
      vP_assertEquals("object", typeof processEngineOrError);
      vP_assertFalse(processEngineOrError.engineCreationError);
      vP_assertFalse(vP_executeForAllValues(processEngineOrError));
      var newViews = environment.getAdditionalMainViews();
      vP_assertEquals(0, newViews.length);
      vP_assertEquals(2, targetView.window.previews.length);
      vP_assertEquals(2, processEngineOrError.generatedResults.length);
      targetView.window.deletePreviews();
   },
   // Test process generating a window saved to a file
   testDBE: function () {
      vPtest_focusOnLinearWindow();
      var environment = vP_makeEnvironment();
      var  viewsUniqueIdsAtStart =  environment.getAdditionalMainViews().map(function(v){return v.uniqueId;});
      var guiParameters = new Vp_GUIParameters(environment);
      var targetView = guiParameters.sourceView;
      vP_assertEquals(0,targetView.window.previews.length);
      guiParameters.processInstanceName = "aDBE";
      guiParameters.parameterToVary="smoothing";
      // Generate two files
      guiParameters.parameterValuesText="0.2,0.3";
      guiParameters.operateOn=VP_OPERATE_PROCESS_GENERATING_NEW_WINDOW;
      guiParameters.resultDestination=VP_RESULT_IN_FILES;
      guiParameters.overwriteOutputFiles=true;
      var processEngineOrError = vP_makeProcessEngine(environment, guiParameters);
      vP_assertEquals("object", typeof processEngineOrError);
      vP_assertFalse(processEngineOrError.engineCreationError);
      vP_assertFalse(vP_executeForAllValues(processEngineOrError));
      // No new visible windows (invisible window are unfortunately created)
      var newViews = environment.getAdditionalMainViewsExcept(viewsUniqueIdsAtStart).filter(function(v){return v.window.visible});
      vP_assertEquals(0, newViews.length);
      vP_assertEquals(2, processEngineOrError.generatedResults.length);
   },
   // Make a test on a window without mask for reference
   testPixelMathNotMasked: function () {
      vPtest_focusOnLinearWindow();
      var environment = vP_makeEnvironment();
      var guiParameters = new Vp_GUIParameters(environment);
      var targetView = guiParameters.sourceView;
      var originalAvgDev = targetView.image.avgDev();
      vP_assertEquals(0,targetView.window.previews.length);
      guiParameters.processInstanceName = "aPixelMathDiv2";
      guiParameters.parameterToVary="expression";
      guiParameters.parameterValuesText='"$T/2"';
      guiParameters.operateOn=VP_OPERATE_MODIFYING_SOURCE;
      guiParameters.resultDestination=VP_RESULT_IN_WINDOWS;
      var processEngineOrError = vP_makeProcessEngine(environment, guiParameters);
      vP_assertEquals("object", typeof processEngineOrError);
      vP_assertFalse(processEngineOrError.engineCreationError);
      vP_assertFalse(vP_executeForAllValues(processEngineOrError));
      var newViews = environment.getAdditionalMainViews();
      vP_assertEquals(1, newViews.length);
      var resultView = processEngineOrError.resultPrimaryView;
      var newAvgDev = resultView.image.avgDev();
      var variationPC  = 100*(newAvgDev- originalAvgDev)/originalAvgDev;
      Console.writeln("testPixelMathNotMasked: original avgDev: ", originalAvgDev, ", new view avgDev: ", newAvgDev, ", variation: ", variationPC);
      vP_assertTrue(variationPC<30);
      resultView.window.forceClose();

   },
   // Make test on mask and ensure result is different than for non masked one
   testPixelMathMasked: function () {
      // Prepare masked image
      var maskedWindow = vP_copyImageFromView(vPtest_defaultLinearView(), "VPTEst_MaskedLinear", false);
      maskedWindow.mask = vPtest_defaultStretchedView().window;
      maskedWindow.maskInverted = true;
      maskedWindow.maskEnabled = true;

      maskedWindow.bringToFront();

      var environment = vP_makeEnvironment();
      var guiParameters = new Vp_GUIParameters(environment);
      var targetView = guiParameters.sourceView;
      var originalAvgDev = targetView.image.avgDev();
      vP_assertEquals(0,targetView.window.previews.length);
      guiParameters.processInstanceName = "aPixelMathDiv2";
      guiParameters.parameterToVary="expression";
      guiParameters.parameterValuesText='"$T/2"';
      guiParameters.operateOn=VP_OPERATE_MODIFYING_SOURCE;
      guiParameters.resultDestination=VP_RESULT_IN_WINDOWS;
      var processEngineOrError = vP_makeProcessEngine(environment, guiParameters);
      vP_assertEquals("object", typeof processEngineOrError);
      vP_assertFalse(processEngineOrError.engineCreationError);
      vP_assertFalse(vP_executeForAllValues(processEngineOrError));
      var newViews = environment.getAdditionalMainViews();
      vP_assertEquals(1, newViews.length);
      var resultView = processEngineOrError.resultPrimaryView;
      var newAvgDev = resultView.image.avgDev();
      var variationPC  = 100*(newAvgDev- originalAvgDev)/originalAvgDev;
      Console.writeln("testPixelMathMasked: original avgDev: ", originalAvgDev, ", new view avgDev: ", newAvgDev, ", variation: ", variationPC);
      vP_assertTrue(variationPC>30);
      maskedWindow.forceClose();
      resultView.window.forceClose();
   }


}


// ---------------------------------------------------------------------------------------------------------
// Support methods
// ---------------------------------------------------------------------------------------------------------

function vPtest_checkEnvironment() {

   Console.writeln("vPtest_checkEnvironment: Check environment for tests, close non required windows and previews");
   vP_checkPresenceOfViews(vPtest_requiredViews);
   vP_closeNonTestWindows(vPtest_requiredViews);
   vP_closePreviewsOfTestWindows(vPtest_requiredViews);

   vP_checkPresenceOfProcesses(vPtest_requiredProcesses);
   Console.writeln("vPtest_checkEnvironment: Completed");


}

// Return a view for tests requiring an intensity stretched view parameter
function vPtest_defaultStretchedView() {
   var view = View.viewById(VP_TEST_STRETCHED);
   if (view.isNull || !(view.window.isValidView(view))) {
      throw "InvalidView";
   }
   return view;
}
// Return a view for tests requiring a linear view parameter
function vPtest_defaultLinearView() {
   var view = View.viewById(VP_TEST_LINEAR);
   if (view.isNull || !(view.window.isValidView(view))) {
      throw "InvalidView";
   }
   return view;
}

// Make the required view the active view
function vPtest_focusOnStretchedWindow() {
   vPtest_defaultStretchedView().window.bringToFront();
}
function vPtest_focusOnLinearWindow() {
   vPtest_defaultLinearView().window.bringToFront();
}

// Get a process instance by name (checked)
function vPtest_getOriginalProcessInstance(name) {
  var p = ProcessInstance.fromIcon(name);
  if (p === null) {
   throw "Unknown process instance '" + name + "'";
  }
  return p;
}

// ---------------------------------------------------------------------------------------------------------
// Auto execute tests on load
// ---------------------------------------------------------------------------------------------------------

Console.show();

try {

   vPtest_checkEnvironment();

   // Execute specified tests
   //vP_executeTests({testEnvironmentGetOperateOnList: vP_allTests.testEnvironmentGetOperateOnList});
   //vP_executeTests({testDBE: vP_allTests.testDBE});
   //vP_executeTests({testStarMask: vP_allTests.testStarMask,testACDNR: vP_allTests.testACDNR });

   // This trick is to test issue with window closing differences bteween 1.7 and 1.8
   //vP_executeTests({testEnvironment1: vP_allTests.testEnvironment,testEnvironment2: vP_allTests.testEnvironment });


   // Execute all tests
   vP_executeTests(vP_allTests);

   Console.writeln("Completed");
} catch(x)
{
   Console.criticalln("Test failed: " + x);
}


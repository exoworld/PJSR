// ----------------------------------------------------------------------------
// PixInsight JavaScript Runtime API - PJSR Version 1.0
// ----------------------------------------------------------------------------
// aryParams-unit-tests-support.jsh
// ----------------------------------------------------------------------------
//
// This file is part of FITSFileManager script version 1.6
//
// Copyright (c) 2012-2021 Jean-Marc Lugrin.
// Copyright (c) 2003-2021 Pleiades Astrophoto S.L.
//
// Redistribution and use in both source and binary forms, with or without
// modification, is permitted provided that the following conditions are met:
//
// 1. All redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//
// 2. All redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
//
// 3. Neither the names "PixInsight" and "Pleiades Astrophoto", nor the names
//    of their contributors, may be used to endorse or promote products derived
//    from this software without specific prior written permission. For written
//    permission, please contact info@pixinsight.com.
//
// 4. All products derived from this software, in any form whatsoever, must
//    reproduce the following acknowledgment in the end-user documentation
//    and/or other materials provided with the product:
//
//    "This product is based on software from the PixInsight project, developed
//    by Pleiades Astrophoto and its contributors (http://pixinsight.com/)."
//
//    Alternatively, if that is where third-party acknowledgments normally
//    appear, this acknowledgment must be reproduced in the product itself.
//
// THIS SOFTWARE IS PROVIDED BY PLEIADES ASTROPHOTO AND ITS CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL PLEIADES ASTROPHOTO OR ITS
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, BUSINESS
// INTERRUPTION; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; AND LOSS OF USE,
// DATA OR PROFITS) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ----------------------------------------------------------------------------


#define VP_TEST_OK "OK"

// ---------------------------------------------------------------------------------------------------------
// Micro unit testing support
// ---------------------------------------------------------------------------------------------------------

// Test results (a property for each test named as the test function and with the result as a String, VP_TEST_OK if OK
var vP_testResults={};



// List all test results
function vP_showTestResults() {
   Console.writeln();
   Console.writeln("------------------------------------------------------------");
   Console.writeln("Test results: ");
   Console.writeln("------------");
   var  successes =0, failures = 0;
   for (var t in vP_testResults) {
      if (vP_testResults.hasOwnProperty(t)) {
         var result = vP_testResults[t];
         if (result === VP_TEST_OK) {
            Console.writeln("    ", t, ": ", result);
            successes += 1;
         } else {
            Console.writeln(" ** ", t, ": ", result);
            failures += 1;
         }
      }
   }
   Console. writeln("" + (successes + failures) + " tests, " + successes + " sucesses, " + failures + ", failures");
   return failures===0;
}

// Execute all properties of 'tests', that must be funtions, in a protected environment

function vP_executeTests(tests) {
   vP_testResults = {};
   for (var f in tests) {
      if (tests.hasOwnProperty(f)) {
         Console.writeln("Starting test: ", f);
         try {
            tests[f]();
            vP_testPassed(f);
            Console.writeln("Test completed successfully: ", f);
         } catch (e) {
            vP_testFailed(f,"Exception: " + e);
            Console.writeln("** Test failed: ", f,", because: ", e);
         }
      }
   };
   Console.show();
   vP_showTestResults();
}

// Assert support
function vP_assertNull(t) {
   if (t!==null) {
      throw "'" +  t + "' !== null";
   }
}
function vP_assertEquals(e,a) {
   if (e!==a) {
     throw "'" + e + "' !== '" + a +"'";
   }
}
function vP_assertArrayEquals(e,a) {
   if (e.length!==a.length) {
      var received = "";
      try {
         received = ", received: " + a;
      } catch (e) {
         // ignore
      }
     throw "Lenghts " + e.length + " !== " + a.length + received;
   }
   for (var i=0; i<e.length; i++) {
      if (e[i]!==a[i]) {
        throw "'" + e + "' !== '" + a +"' (index " + i + ")";
      }
   }
}
function vP_assertTrue(e) {
   if (!e) {
      throw "'" + e + "' is not true" ;
   }
}
function vP_assertFalse(e) {
   if (e) {
      throw "'" + e + "' is not false" ;
   }
}

// ---------------------------------------------------------------------------------------------------------
// Support methods (private)
// ---------------------------------------------------------------------------------------------------------
// Mark a test passed if it does not exist (test must be a string)
function vP_testPassed(test) {
   if (!(vP_testResults.hasOwnProperty(test))) {
      vP_testResults[test] = VP_TEST_OK;
   }
}

// Support methods (private)
// Mark a test failed in all cases (test must be a string), display message about failure
function vP_testFailed(test, msg) {
   vP_testResults[test] = msg;
   Console.writeln("Test ", test, " failed: ", msg);
}

// ---------------------------------------------------------------------------------------------------------
// Methods to support checking the current project for prerequisite
// ---------------------------------------------------------------------------------------------------------

function vP_checkPresenceOfProcesses(requiredProcesses) {
   Console.noteln("The tests requires that the project 'TestVaryParams.pxiproject' is loaded.")
   Console.writeln("  Checking presence of required test processes in current workspace");
   var allProcessInstanceNames = ProcessInstance.icons();
#ifdef DEBUG
   Console.writeln("    vPtest_checkPresenceOfProcesses: All icons on project: ", allProcessInstanceNames);
   Console.writeln("    vPtest_checkPresenceOfProcesses: Required icons: ", requiredProcesses);
#endif

   for (var i=0; i<requiredProcesses.length; i++) {
      var processName = requiredProcesses[i];
      var isProcessPresent =  allProcessInstanceNames.indexOf(processName)>=0;
      if (!isProcessPresent) {
         throw "Process '" + processName + "' not present in current project, reload test project 'TestVaryParams.pxiproject'";
      }
   }
   Console.writeln("        The " + requiredProcesses.length + " processes were found, ok.");
}

// Check that all views named in the array of string requiredViews are main views of some window in the project
function vP_checkPresenceOfViews(requiredViews) {

   var thereAreMissingViews = false;

#ifdef DEBUG
   var allValidWindows = ImageWindow.windows.filter(function (w) {return !w.isNull && vP_isOpenWindow(w)});
   Console.writeln("  vP_checkPresenceOfViews: " , allValidWindows.length," Window main views: ", allValidWindows.map(function(w){return w.mainView.id}));
#endif
   Console.writeln("    Checking presence of required views in current workspace");

   // Check that all required views are present
   for (var i=0; i<requiredViews.length; i++) {
      var viewId = requiredViews[i];
      var view = View.viewById(viewId);
#ifdef DEBUG
      Console.writeln("      vP_checkPresenceOfViews: View ", viewId , " is ", view);
#endif
   if (view.isNull || !view.window.isValidView(view)) {
         thereAreMissingViews = true;
         Console.writeln("View '" + viewId + "' not present in current project");
      } else {
         Console.writeln("      view " + viewId + " present, uniqueId: " + view.uniqueId);
      }
   }
   if (thereAreMissingViews) {
      throw "Some views are missing in the current project (see log above), reload the test project 'TestVaryParams.pxiproject'.";
   }

}

// Close all windows whose main view is not in the list of required views, assume that all required views are present
function vP_closeNonTestWindows(requiredViews) {

   var requiredViewsUniqueId = [];
   var allValidWindows = ImageWindow.windows.filter(function (w) {return !w.isNull && vP_isOpenWindow(w)});

   // build a list of the uniqueId of all required main views
   for (var i=0; i<requiredViews.length; i++) {
      var viewId = requiredViews[i];
      var view = View.viewById(viewId);
      if (view.isNull || !view.window.isValidView(view)) {
         throw "View '" + viewId + "' not present in current project";
      }
      requiredViewsUniqueId.push(view.uniqueId);
   }


   for (var i=0; i<allValidWindows.length; i++) {
      var currentMainView = allValidWindows[i].mainView;
      var currentViewUniqueId = currentMainView.uniqueId;
      var isRelevantWindow =  requiredViewsUniqueId.indexOf(currentViewUniqueId)>=0;
#ifdef DEBUG
      Console.writeln("    vP_checkPresenceOfViews: Window ", currentMainView.id, " isRelevantWindow: " + isRelevantWindow +",  has index ", requiredViewsUniqueId.indexOf(currentViewUniqueId));
#endif
      if (!isRelevantWindow) {
         // The user will be requested to confirm
         Console.writeln("    Closing unused window of view '" + currentMainView.id, "', uniqueId: " + currentMainView.uniqueId);
         allValidWindows[i].close();
      }
   }

}
// Close all previews of the window required views, assume that the views are present
function vP_closePreviewsOfTestWindows(requiredViews) {
   Console.writeln("  Closing previews of test windows");
   for (var i=0; i<requiredViews.length; i++) {
      var viewId = requiredViews[i];
      var view = View.viewById(viewId);
      if (view.isNull || !view.window.isValidView(view)) {
         throw "View '" + viewId + "' not present in current project";
      }
      view.window.deletePreviews();
   }
}

